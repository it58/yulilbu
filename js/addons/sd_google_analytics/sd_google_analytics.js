(function (_, $) {
    var data = $('#sd-ga-data').data();

    var filterContaining = function (arr, elm) {
        var elementIsContained = function (elm) {
            return elm.contains(this);
        };
        return arr.filter(elementIsContained, elm)
    };

    function matchesUrl(url_str, clean_url) {
        return (url_str.length == clean_url.length)
            || isNaN(url_str.substr(clean_url.length)[0]);
    }

    function bindLinks(blockRoots) { //TODO: Better selection parsing, iteration optimisation.
        if (blockRoots.length && typeof productDataIndex !== 'undefined') {
            for (var product_url in productDataIndex) {
                if (productDataIndex[product_url].bound) continue;
                (function (url) {
                    $('a[href^="' + url + '"]').filter(
                        function (i) {
                            return matchesUrl(this.href, url);
                        }
                    ).click(function (e) {
                        var link_url = this.href;
                        var containingBlocks = filterContaining(blockRoots, this);
                        if (containingBlocks.length) {
                            ga('ec:addProduct', productDataIndex[url].product);
                            ga('ec:setAction', 'click', {list: productDataIndex[url].list});
                            ga('send', 'event', 'UX', 'click', productDataIndex[url].list);
                        }
                    });
                    //and also for quick view urls:
                    var quickViewFragment = 'products.quick_view&product_id=' + productDataIndex[url].product.id + '&';
                    $('a[href*="' + quickViewFragment + '"]').click(function (e) {
                            var link_url = this.href;
                            var containingBlocks = filterContaining(blockRoots, this);
                            if (containingBlocks.length) {
                                ga('ec:addProduct', productDataIndex[url].product);
                                ga('ec:setAction', 'click', {list: productDataIndex[url].list + ' (quick view)'});
                                ga('send', 'event', 'UX', 'click', productDataIndex[url].list + ' (quick view)', {
                                    nonInteraction: data.noninteractiveQuickView
                                });
                            }
                        });

                })(product_url);
                productDataIndex[product_url].bound = true;
            }
        }
    }

    function sendPageview()
    {
        var product_form = $('[name^=product_form_ajax]').first();
        if (product_form.length > 0) {
            ga('set', 'page', '/index.php?dispatch=products.quick_view&product_id='+product_form.prop('name').replace(/product_form_ajax/g, ''));
            ga('send', 'pageview');
        }
    }

    $(document).ready(function () {
        blockRoots = $('input[class^="ga-block-marker"]').next().toArray();
        bindLinks(blockRoots);
    });

    $.ceEvent('on', 'ce.history_load', function (url) {
        if (typeof(ga) != 'undefined') {
            //According google analytics documentation URL should start with / symbol
            ga('send', 'pageview', url.replace('!', ''));
        }
    });

    $.ceEvent('on', 'ce.ajaxdone', function (elms, inline_scripts, params, data) {
        if (data['sd_ga'] && typeof(ga) != 'undefined') {
            var is_add_tracker = false;
            var is_remove_tracker = false;
            var add_tracker = '';
            var remove_tracker = '';

            //adds the add-to-cart event to the details product page
            if (data['sd_ga']['added']) {
                for (var item in data['sd_ga']['added']) {
                    ga('ec:addProduct', data['sd_ga']['added'][item]);

                    if (data['sd_ga']['added'][item]['tracker']) {
                        is_add_tracker = true;
                        add_tracker = data['sd_ga']['added'][item]['tracker'];
                        ga(add_tracker + '.ec:addProduct', data['sd_ga']['added'][item]);
                    }
                }
                sendPageview();
                ga('ec:setAction', 'add');
                ga('send', 'event', 'UX', 'click', 'add to cart');
                if (is_add_tracker === true) {
                    ga(add_tracker + '.ec:setAction', 'add');
                    ga(add_tracker + '.send', 'event', 'UX', 'click', 'add to cart');
                }
            }

            //adds the remove-from-cart event to the minicart (to the block)
            if (data['sd_ga']['deleted']) {
                for (var item in data['sd_ga']['deleted']) {
                    ga('ec:addProduct', data['sd_ga']['deleted'][item]);

                    if (data['sd_ga']['deleted'][item]['tracker']) {
                        is_remove_tracker = true;
                        remove_tracker = data['sd_ga']['deleted'][item]['tracker'];
                        ga(remove_tracker + '.ec:addProduct', data['sd_ga']['deleted'][item]);
                    }
                }
                ga('ec:setAction', 'remove');
                ga('send', 'event', 'UX', 'click', 'remove to cart');
                if (is_remove_tracker === true) {
                    ga(remove_tracker + '.ec:setAction', 'remove');
                    ga(remove_tracker + '.send', 'event', 'UX', 'click', 'remove to cart');
                }
            }
        }
    });

    $.ceEvent('on', 'ce.ajaxdone', function (elms, inline_scripts, params, data) {
        if (params.result_ids) {
            if (data.ga_array_data) {
                for (var item in data['ga_array_data']) {
                    if (item == 'addImpression' || item == 'addProduct' || item == 'addPromo') {
                        for (var key in data['ga_array_data'][item]) {
                            if (item == 'addPromo') {
                                for (var promo in data['ga_array_data'][item][key]) {
                                    ga('ec:' + item, data['ga_array_data'][item][key][promo]);
                                }
                            } else {
                                ga('ec:' + item, data['ga_array_data'][item][key]);
                            }
                        }
                    }
                    if (item == 'addProductClick' && typeof productDataIndex !== 'undefined') {
                        for (prod in data['ga_array_data'][item]) {
                            productDataIndex[prod] = data['ga_array_data'][item][prod];
                        }
                    }
                }
            }
        }
    });

    $.ceEvent('on', 'ce.ajaxdone', function (elms, inline_scripts, params, data) {
        blockRoots = $('input[class^="ga-block-marker"]').next().toArray();
        bindLinks(blockRoots);
    });

    if (typeof(ga) != 'undefined') {
        $(document).on('click', 'a[data-ca-target-id^=comparison_list]', function() {
            sendPageview();
            ga('send', 'event', 'UX', 'click', 'add to comparison list', {
                nonInteraction: data.noninteractiveComparisonList
            });
        });

        $(document).on('click', '.cm-submit[id^="button_wishlist"]', function() {
            $.ceEvent('one', 'ce.formajaxpost_' + $(this).parents('form').prop('name'), function(form, elm) {
                sendPageview();
                ga('send', 'event', 'UX', 'click', 'add to wishlist', {
                    nonInteraction: data.noninteractiveWishlist
                });
            });
        });

        $.ceEvent('on', 'ce.formajaxpost_call_requests_form', function(form, elm) {
            sendPageview();
            ga('send', 'event', 'UX', 'click', 'buy in one click', {
                nonInteraction: data.noninteractiveBuyOneClick
            });

            var product_id = elm['data']['call_data[product_id]'];
            var tracker = elm['data']['product_data[' + product_id + '][tracker]'];

            if (tracker) {
                ga(tracker + '.send', 'event', 'UX', 'click', 'buy in one click');
            }
        });

        $.ceEvent('on', 'ce.formajaxpost_call_requests_form_main', function() {
            ga('send', 'event', 'UX', 'click', 'call request');
        });
    }

}(Tygh, Tygh.$));
