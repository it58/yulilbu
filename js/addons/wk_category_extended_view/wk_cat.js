(function(_, $) {
    // list category click
    $(document).on('click','.categories_container li',function(event){
        var category_path = {};
        var category_id = $(this).data('category-id');
        var parent_id = $(this).data('parent-id');
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $(this).parents('div.categories_container').nextAll().remove();
        if($(this).hasClass('child')){
            var html = '<span class="fa fa-chevron-right icon_root"></span><div  class="categories_container" id="child_categories_'+category_id+'"></div>';
            $(html).insertAfter('#child_categories_'+parent_id);
            fn_add_subcategory_tree(category_id,category_path);
        }else{
            $('#selected_category_id').val(category_id);
        }
        var title_html = '';
        if(parent_id !== 0){
            title_html += '<span class="parent_title" id="category_title_'+category_id+'">>&nbsp;'+$(this).text()+'</span>';
        }else{
            $('#category_title_'+parent_id).empty();
            title_html += '<span id="category_title_'+category_id+'" class="parent_title">'+$(this).text()+'</span>';
        }
        $('#category_title_'+parent_id).nextAll().remove();
        $(title_html).insertAfter('#category_title_'+parent_id);
        if($(this).hasClass('no_child')){
            $('.toggle_category_box').click();
        }
    });

    //search on list category
    $(document).on('input','.category_search',function(){
        var parent_id = $(this).data('parent-id');
        $('#category_title_'+parent_id).nextAll().remove();
        $('#child_categories_'+parent_id).nextAll().remove();
        if(parent_id == 0){
            $('#category_title_'+parent_id).empty();
            var select_a_category_title = _.tr("select_a_category");
            $('.category_title_container').append('<span id="category_title_0" class="select_a_category_title">'+select_a_category_title+'</span>');
        }
        
        fn_search_category(parent_id,$(this).val());
    });

    //ajax Done hook when on search complete then focus the particular seerch filed
    $.ceEvent('on', 'ce.ajaxdone', function(elms, scripts, params, response_data, response_text) {
        if(params.result_ids == 'container_category_list_'+params.data.parent_category_id){
            $('#wk_search_category_'+params.data.parent_category_id).focus();
        }
    });
    
    //Event for toggling the view of category container box
    $(document).on('click','.toggle_category_box',function(){
        if($('.toggle_category_box').text() == 'close'){
            var last_category_id = $(".category_title_container span:last").attr('id');
            if(last_category_id != null && last_category_id != undefined){
                last_category_id = last_category_id.replace('category_title_','');
                if($('#wk_category_id_' + last_category_id).hasClass('child') || last_category_id == 0){
                    var error_msg = _.tr("please_select_a_leaf_category");
                    if(last_category_id == 0){
                        error_msg = _.tr('please_select_a_category');
                    }
                    $.ceNotification('show', {
                        type: 'E',
                        title: _.tr("error"),
                        message: error_msg
                    });
                    return false;
                }else{
                    var success_msg = _.tr('category_selected');
                    $.ceNotification('show', {
                        type: 'N',
                        title:  _.tr("success"),
                        message: success_msg
                    });
                }
            }
        }
        $('.category_container_box').toggle();
        if($('.toggle_category_box').text() == 'close'){
            $('.toggle_category_box').text('change'); 
        }else{
            $('.toggle_category_box').text('close');
        }
    });

}(Tygh, Tygh.$));

/**
 * for search category tree for any parent category id
 * @param Integer category_id       parent id in which subcategory searching need to be perform
 * @param String  search_string     query string to be search
*/
function fn_search_category(category_id,search_string){
    var form_values1 = {};
    form_values1['parent_category_id'] = category_id;
    form_values1['search_string'] = search_string;
    data = form_values1;
    $.ceAjax('request', fn_url('products.wk_search_category'),{
        result_ids: 'container_category_list_'+category_id,
        method: 'post',
        full_render: false,
        data: data,
    });
}

/**
 * function for loading subcategories tree of any categories
 * @param Integer category_id       parent id for which subcategory loading need to be perform
 * @param String  category_path     this is path of any category for selected category tree path
*/
function fn_add_subcategory_tree(category_id,category_path){
    var form_values1 = {};
    form_values1['parent_category_id'] = category_id;
    data = form_values1;
    $.ceAjax('request', fn_url('products.wk_get_child_categories'),{
        result_ids: 'child_categories_'+category_id,
        method: 'post',
        full_render: false,
        data: data,
        callback: function(data1){
            fn_print_selected_categories(category_id,category_path);  
        },
    });
} 

/**
 *Fuction for loading content of selected categories
 * @param Integer category_id       parent category id
 * @param String  category_path     this is path of any category for selected category tree path
*/
function fn_print_selected_categories(category_id,category_path){
    var categories = category_path;
    if(categories != null && categories != {} && categories.length > 0){
        for(i = 0;i<categories.length;i++){
            if(categories[i] == category_id){
                categories.splice(i, 1);
                break;
            }
        }
        if(categories.length > 1){
            var html = '<span class="fa fa-chevron-right icon_root"></span><div class="categories_container" id="child_categories_'+categories[0]+'"></div>';
            $(html).insertAfter('#child_categories_'+category_id);
            $('#wk_category_id_'+categories[0]).addClass('active');
            fn_add_subcategory_tree(categories[0],categories);
        }else{
            $('#wk_category_id_'+categories[0]).addClass('active');
        }
        var title_html = '<span class="parent_title" id="category_title_'+categories[0]+'">>&nbsp;'+$('#wk_category_id_'+categories[0]).text()+'</span>';
        if(category_path[0] == category_id || category_id == 0){
            $('#category_title_0').empty();
            title_html = '<span class="parent_title" id="category_title_'+categories[0]+'">'+$('#wk_category_id_'+categories[0]).text()+'</span>';
        }
        $(title_html).insertAfter('#category_title_'+category_id);
    }
}


