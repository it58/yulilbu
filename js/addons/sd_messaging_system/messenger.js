(function(_, $) {

    function Messenger() {

        var RECONNECT_DELAY_TIME = 3000;
        var RECONENCT_MAX_ATTEMPT = 10;

        var websocket_url = $('input[name="websocket_url"]').val();
        var ticket_block = document.getElementById("ticket_messages_list");
        var ticket_id = $('input[name="ticket_id"]').val();

        var sender_message_body = GetNewMessageBody('sender');
        var recipient_message_body = GetNewMessageBody('recipient');

        var has_connect = false;
        var connect_attempt = 0;

        function ErrorMessenger() {
            $('#new_message_field').prop('disabled', true);
            $('#send_message_but').prop('disabled', true);
            $('#message_spinner').removeClass('hidden');
        };

        function ApproveMessenger() {
            $('#new_message_field').prop('disabled', false);
            $('#send_message_but').prop('disabled', false);
            $('#message_spinner').addClass('hidden');
        };

        function GetMessage(author_type, data) {
            if (author_type == 'sender') {
                ShowNewMessage(sender_message_body, data.message);
            } else if (author_type == 'recipient') {
                ShowNewMessage(recipient_message_body, data.message);
            }

            if (data.last_message_id) {
                $('input[name="last_message_id"]').val(data.last_message_id);
            }

            CheckScroll($('#ticket_messages_list'), author_type);
        };

        function GetNewMessageBody(author) {
            var message_data = {};
            var image_data = {};

            message_data.message_class = author == 'sender' ? 'author-message has-avatar' : 'recipient-message';

            if (author == 'sender') {
                var sender_data = $('#sd-messenger-sender-data').data();

                message_data.author_name = sender_data.name;
                image_data.has_image = sender_data.hasImage;
                image_data.image_link = sender_data.type == 'V' ? fn_url('companies.update?company_id=' + sender_data.id) : fn_url('profiles.update');

                if (image_data.has_image == 'Y') {
                    image_data.image_path = sender_data.imagePath;
                    image_data.image_alt = sender_data.imageAlt;
                }
            } else if (author == 'recipient') {
                var recipient_data = $('#sd-messenger-recipient-data').data();

                message_data.author_name = recipient_data.name;
                image_data.has_image = recipient_data.hasImage;
                image_data.image_link = recipient_data.type == 'C' ? fn_url('profiles.update?user_id=' + recipient_data.id) : fn_url('companies.view?company_id=' + recipient_data.id);

                if (image_data.has_image == 'Y') {
                    image_data.image_path = recipient_data.imagePath;
                    image_data.image_alt = recipient_data.imageAlt;
                }
            }

            if (image_data.has_image) {
                image_data.image_class = 'ty-pict ty-sd_messaging_system-message__user-image cm-image';
                image_data.image_id = 'det_img_' + Math.floor(Math.random() * 1000000000); // Random 9-digit number
            }

            if (image_data.has_image == 'Y') {
                var author_image =
                    '<a href="' + image_data.image_link + '">'+
                        '<img class="' + image_data.image_class + '" id="' + image_data.image_id + '"  src="' + image_data.image_path + '" alt="' + image_data.image_alt + '" title="' + image_data.image_alt + '"  />'+
                    '</a>';
            } else {
                var author_image =
                    '<a href="' + image_data.image_link + '">'+
                        '<svg class="no-image-icon" width="42" height="42" viewBox="0 0 88 88"><circle class="ty-sd_messaging-no-image__circle" cx="44" cy="44" r="44"></circle>'+
                        '<path class="ty-sd_messaging-no-image__fill" d="M62.122,57.174l-9.345-4.7a2.588,2.588,0,0,1-1.429-2.326V46.828c0.224-.275.46-0.589,0.7-0.935a22.547,22.547,0,0,0,2.89-5.7,3.9,3.9,0,0,0,2.276-3.557V32.7a3.933,'+
                        '3.933,0,0,0-.978-2.581v-5.23a8.848,8.848,0,0,0-2.047-6.416C52.183,16.168,48.923,15,44.5,15s-7.683,1.168-9.693,3.471a8.85,8.85,0,0,0-2.047,6.417v5.23a3.933,3.933,0,0,0-.978,2.581v3.933a3.918,3.918,0,0,0,1.465,3'+
                        '.057,20.68,20.68,0,0,0,3.427,7.116v3.254a2.6,2.6,0,0,1-1.347,2.281L26.6,57.127A8.874,8.874,0,0,0,22,64.916V68.1C22,72.767,36.718,74,44.5,74S67,72.767,67,68.1V65.108A8.828,8.828,0,0,0,62.122,57.174Z"></path></svg>'+
                    '</a>';
            }

            var new_message_body =
                '<div class="ty-sd_messaging_system-message__content '+ message_data.message_class +' ty-mb-l">'+
                    '<div class="ty-sd_messaging_system-message__author">' + author_image + '</div>'+
                    '<div class="ty-sd_messaging_system-all">'+
                        '<div class="ty-sd_messaging_system-name">' + message_data.author_name + '</div>'+
                        '<div class="ty-sd_messaging_system-message" id="message_text">'+
                            '<div class="ty-sd_messaging_system-message__message">'

            return new_message_body;
        }

        function ShowNewMessage(message_body, message_text) {
            if (message_body && message_text) {
                message_text = message_text.replace(/(?:\r\n|\r|\n)/g, "<br />") + '</div></div></div></div>'; // Close message body DOM elements
                message_body = message_body + message_text
                $('#ticket_messages_list').append(message_body);

                $(".ty-no-messages").remove();
                $(".ty-no-items").remove();
                $("#new_message_field").val('');
            }
        }

        function CheckScroll(elm, author_type) {
            if (author_type == 'recipient') {
                if (elm.scrollTop() + elm.innerHeight() >= elm[0].scrollHeight - 200) {
                    ticket_block.scrollTop = ticket_block.scrollHeight;
                }
            } else if (author_type == 'sender') {
                ticket_block.scrollTop = ticket_block.scrollHeight;
            }
        }

        this.CkeckKeyDown = function(elm) {
            elm = elm ? elm : window.event;

            if (elm.keyCode == 13 && !elm.shiftKey) {
                $('#send_message_but').click();
                elm.preventDefault();
            }
        };

        this.ResizeWindow = function() {
            $(window).resize(function() {
                var bodyheight = $(this).height();

                if (_.area == 'A') {
                    height = $(window).width() <= 780 ? 335 : 271;

                    $("#ticket_messages_list").height(bodyheight - height - 15); // FIXME resize window
                    ticket_block.scrollTop = ticket_block.scrollHeight;
                } else {
                    var top_panel = $('.tygh-top-panel').outerHeight();
                    var top_header = $('.tygh-header').outerHeight();
                    var discussion_buttons = $('.ty-discussion-post__buttons').outerHeight();

                    $("#ticket_messages_list").height($(window).height() - top_panel - top_header - discussion_buttons - 60); // FIXME resize window
                    ticket_block.scrollTop = ticket_block.scrollHeight
                }
            }).resize();
        };

        this.SendMessage = function() {
            var data = $('#new_message_form').serializeObject();

            if (data.message_body.trim()) {
                $.ceAjax('request', fn_url('messenger.send_message'), {
                    hidden: true,
                    method: 'post',
                    data: data,
                });
            }
        };

        function GetReconnectMessages() {
            var last_message_id = $('input[name="last_message_id"]').val();

            $.ceAjax('request', fn_url('messenger.get_new_messages'), {
                method: 'post',
                data: {
                    ticket_id: ticket_id,
                    last_message_id: last_message_id,
                },
                hidden: true,
                callback: function (new_messages) {
                    if (new_messages.new_messages) {
                        for (var i = 0; i < new_messages.new_messages.length; i++) {
                            GetMessage(new_messages.new_messages[i].author_type, new_messages.new_messages[i]);
                        }

                        ticket_block.scrollTop = ticket_block.scrollHeight;
                    }
                },
            });
        };

        function UpdateTicketStatus(data) {
            if (data.from_area != _.area) {
                $.ceAjax('request', fn_url('messenger.change_status'), {
                    hidden: true,
                    method: 'post',
                    data: data,
                });
            }
        };

        this.Connect = function() {
            var conn = new ab.Session('wss://' + websocket_url,
                function() {

                    // Reconnect after close
                    if (connect_attempt > 0) {
                        GetReconnectMessages();
                    }

                    has_connect = true;
                    connect_attempt = 0;
                    ApproveMessenger();

                    conn.subscribe(ticket_id, function(ticket, data) {
                        var author_type = data.from_area == _.area ? 'sender' : 'recipient';

                        GetMessage(author_type, data);
                        UpdateTicketStatus(data);
                    });
                },
                function() {
                    has_connect = false;
                    Reconnect();
                    ErrorMessenger();
                }
            );
        };

        function Reconnect() {
            var has_reconnect = false;

            setTimeout(function() {
                if (!has_reconnect && !has_connect && RECONENCT_MAX_ATTEMPT >= ++connect_attempt) {
                    Messenger.Connect();
                    has_reconnect = true;
                } else if (RECONENCT_MAX_ATTEMPT < connect_attempt) {
                    $('#message_spinner').text(_.tr('addons.sd_messaging_system.reload_page'));
                }
            }, RECONNECT_DELAY_TIME);
        }
    }

    var Messenger = new Messenger();

    $(document).ready(function() {
        Messenger.ResizeWindow();
    });

    Messenger.Connect();

    $('#send_message_but').on('click', function() {
        Messenger.SendMessage();
    });

    $('#new_message_field').on('keydown', function (elm) {
        Messenger.CkeckKeyDown(elm);
    });

}(Tygh, Tygh.$));