(function($, _) {
    $(_.doc).on('click', '.cm-emltpl-insert-variable', function() {
        var self = $(this);
        var active_input = self.data('caTargetTemplate') ? $('#'+self.data('caTargetTemplate')) : $('.cm-emltpl-set-active.cm-active');
        
        if (active_input.length && active_input.val()) {
            if (active_input.hasClass('cm-wysiwyg')) {
                active_input.ceEditor('insert', " ");

                return;
            } else {
                active_input.ceInsertAtCaret(" ");
            }
        }
    });
}(Tygh.$, Tygh));