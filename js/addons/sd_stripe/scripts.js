(function(_, $) {
    $(document).ready(function() {
        object();
    });
    object = function() {
        if($(window).width() > 767) {
            $('.ty-cvv2-about').mouseover(function() {
                $('#content_add_new_cart .object-container').height(665);
                $('html.dialog-is-open').css('overflow', 'auto');
            });
            $('.ty-cvv2-about').mouseout(function() {
                $('#content_add_new_cart .object-container').height(448);
                $('.stripe-cc').height(448);
                $('html.dialog-is-open').css('overflow', 'hidden');
            });
        }
    };
}(Tygh, Tygh.$));

