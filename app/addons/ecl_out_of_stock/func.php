<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\ProductTracking;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_ecl_out_of_stock_get_product_exceptions_post($product_id, &$exceptions, $short_list)
{  
    if (AREA == 'C') {
        Registry::set('settings.General.exception_style', 'hide');

        $default_exceptions = $exceptions;
        $exceptions = db_get_array("SELECT product_id, combination_hash as exception_id, combination FROM ?:product_options_inventory WHERE product_id = ?i AND amount <= ?i" , $product_id, 0);

        if (!empty($exceptions)) {
            foreach ($exceptions as $k => $v) {
                $_variants = explode('_', $v['combination']);
                $exceptions[$k]['combination'] = array();

                foreach ($_variants as $kk => $vv) {
                    if (($kk % 2) == 0 && !empty($_variants[$kk + 1])) {
                        $exceptions[$k]['combination'][$vv] = $_variants[$kk + 1];
                    }
                }

                if ($short_list) {
                    $exceptions[$k] = $exceptions[$k]['combination'];
                }
            }
        }
        if (!empty($default_exceptions)) {
            $exceptions = fn_array_merge($exceptions, $default_exceptions);
        }
    }
}

function fn_ecl_out_of_stock_apply_exceptions_post(&$product, $exceptions)
{
    if (!empty($exceptions) && isset($product['selected_options'])) {
        $combination_hash = fn_generate_cart_id($product['product_id'], array('product_options' => $product['selected_options']), true);

        // Change product code and amount
        if (!empty($product['tracking']) && $product['tracking'] == ProductTracking::TRACK_WITH_OPTIONS) {
            $combination = db_get_row("SELECT product_code, amount FROM ?:product_options_inventory WHERE combination_hash = ?s", $combination_hash);
            if (!empty($combination['product_code'])) {
                $product['product_code'] = $combination['product_code'];
            }

            if (Registry::get('settings.General.inventory_tracking') == 'Y') {
                if (isset($combination['amount'])) {
                        $product['inventory_amount'] = $combination['amount'];
                } else {
                        $product['inventory_amount'] = $product['amount'] = 0;
                }
            }
        }
    }
}