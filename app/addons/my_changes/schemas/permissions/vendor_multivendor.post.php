<?php
$schema['controllers']['exim'] = array(       
    'permissions' => false,
);
$schema['controllers']['pages'] = array(       
    'permissions' => false,
);
$schema['controllers']['wk_vendor_kyc']['modes']['manage']['permissions'] = false;
$schema['controllers']['seo_pack']['modes']['manage']['permissions'] = false;
$schema['controllers']['discussions_enabler']['modes']['manage']['permissions'] = false;
$schema['controllers']['profiles']['modes']['manage']['param_permissions']['user_type']['C'] = false;
$schema['controllers']['profiles']['modes']['manage']['param_permissions']['user_type']['V'] = false;
return $schema;
