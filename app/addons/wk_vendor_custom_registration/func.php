<?php
/******************************************************************
# Vendor Custom Registration - Vendor Custom Registration                                               *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/ 
function fn_wk_vendor_custom_registration_update_vendor_custom_fields($field_data, $field_id, $lang_code = DESCR_SL)
{
    if (empty($field_id)) {

        $add_match = false;

        $field_name = $field_data['field_name'];
        if ($field_data['section'] == 'BS') {
            $field_data['section'] = 'B';
            $field_data['field_name'] = !empty($field_name) ? ('b_' . $field_name) : '';
            $add_match = true;
        }
        // Insert main data
        $field_id = db_query("INSERT INTO ?:wk_vendor_custom_fields ?e", $field_data);

        // Insert descriptions
        $_data = array (
            'object_id' => $field_id,
            'object_type' => 'F',
            'description' => $field_data['description'],
        );

        foreach (fn_get_translation_languages() as $_data['lang_code'] => $_v) {
            db_query("INSERT INTO ?:wk_vendor_custom_fields_description
             ?e", $_data);
        }

        if (substr_count('SR', $field_data['field_type']) && is_array($field_data['add_values']) && $add_match == false) {
            fn_wk_vendor_custom_registration_add_field_values($field_data['add_values'], $field_id);
        }

        if ($add_match == true) {
            $field_data['section'] = 'S';
            $field_data['field_name'] = !empty($field_name) ? ('s_' . $field_name) : '';
            $field_data['matching_id'] = $field_id;

            // Update match for the billing field
            $s_field_id = fn_wk_vendor_custom_registration_update_vendor_custom_fields($field_data, 0, $lang_code);
            if (!empty($s_field_id)) {
                db_query('UPDATE ?:wk_vendor_custom_fields SET matching_id = ?i WHERE field_id = ?i', $s_field_id, $field_id);
            }
        }

    } else {
        db_query("UPDATE ?:wk_vendor_custom_fields SET ?u WHERE field_id = ?i", $field_data, $field_id);

        if (!empty($field_data['matching_id']) && $field_data['section'] == 'S') {
            db_query('UPDATE ?:wk_vendor_custom_fields SET field_type = ?s WHERE field_id = ?i', $field_data['field_type'], $field_data['matching_id']);
        }

        $check_for_lang=db_get_field("SELECT object_id FROM ?:wk_vendor_custom_fields_description WHERE object_id=?i AND lang_code = ?s",$field_id, $lang_code);
        if (!empty( $check_for_lang)) {
           db_query("UPDATE ?:wk_vendor_custom_fields_description SET ?u WHERE object_id = ?i AND object_type = 'F' AND lang_code = ?s", $field_data, $field_id, $lang_code);
        } else {
            $_data = array (
            'object_id' => $field_id,
            'object_type' => 'F',
            'description' => $field_data['description'],
            'lang_code' =>$lang_code
            );
            db_query("INSERT INTO ?:wk_vendor_custom_fields_description
                 ?e", $_data);
        }
        if (!empty($field_data['field_type'])) {
            if (strpos('SR', $field_data['field_type']) !== false) {
                if (!empty($field_data['values'])) {
                    foreach ($field_data['values'] as $value_id => $vdata) {
                        db_query("UPDATE ?:wk_vendor_custom_field_values SET ?u WHERE value_id = ?i", $vdata, $value_id);
                        db_query("UPDATE ?:wk_vendor_custom_fields_description SET ?u WHERE object_id = ?i AND object_type = 'V' AND lang_code = ?s", $vdata, $value_id, $lang_code);
                    }

                    // Completely delete removed values
                    $existing_ids = db_get_fields("SELECT value_id FROM ?:wk_vendor_custom_field_values WHERE field_id = ?i", $field_id);
                    $val_ids = array_diff($existing_ids, array_keys($field_data['values']));

                    if (!empty($val_ids)) {
                        fn_wk_vendor_custom_registration_delete_field_values($field_id, $val_ids);
                    }
                } else {
                   if (isset($field_data['add_values'])) {
                        fn_wk_vendor_custom_registration_delete_field_values($field_id);
                    }
                }

                if (!empty($field_data['add_values']) && is_array($field_data['add_values'])) {
                    fn_wk_vendor_custom_registration_add_field_values($field_data['add_values'], $field_id);
                }
            } else {
                fn_wk_vendor_custom_registration_delete_field_values($field_id);
            }
        }
    }

    return $field_id;
}

function fn_wk_vendor_custom_registration_get_vendor_custom_fields($location = 'C', $_auth = array(), $lang_code = CART_LANGUAGE, $params = array())
{
    $auth = & $_SESSION['auth'];
    $select = '';

    if (empty($_auth)) {
        $_auth = $auth;
    }

    if (!empty($params['get_custom'])) {
        $condition = "WHERE ?:wk_vendor_custom_fields
        .is_default = 'N' ";
    } else {
        $condition = "WHERE 1 ";
    }

    if (!empty($params['get_profile_required'])) {
        $condition .= "AND ?:wk_vendor_custom_fields
        .profile_required = 'Y' ";
    }

    if (!empty($params['get_checkout_required'])) {
        $condition .= "AND ?:wk_vendor_custom_fields
            .checkout_required = 'Y' ";
    }

    fn_set_hook('change_location', $location, $select, $condition, $params);

    if ($location == 'A' || $location == 'V' || $location == 'C') {
        $select .= ", ?:wk_vendor_custom_fields
            .profile_required as required";
        $condition .= " AND ?:wk_vendor_custom_fields
            .profile_show = 'Y'";
    } elseif ($location == 'O' || $location == 'I') {
        $select .= ", ?:wk_vendor_custom_fields
                    .checkout_required as required";
        $condition .= " AND ?:wk_vendor_custom_fields
                        .checkout_show = 'Y'";
    }

    if (!empty($params['field_id'])) {
        $condition .= db_quote(' AND ?:wk_vendor_custom_fields
                .field_id = ?i', $params['field_id']);
    }

   
    // Determine whether to retrieve or not email field
    $skip_email_field = false;
    $wk_vendor_custom_fields = db_get_hash_multi_array("SELECT ?:wk_vendor_custom_fields.*, ?:wk_vendor_custom_fields_description.description $select FROM ?:wk_vendor_custom_fields
         LEFT JOIN ?:wk_vendor_custom_fields_description ON ?:wk_vendor_custom_fields_description.object_id = ?:wk_vendor_custom_fields.field_id AND ?:wk_vendor_custom_fields_description.object_type = 'F' AND lang_code = ?s $condition ORDER BY ?:wk_vendor_custom_fields.position", array('section', 'field_id'), $lang_code);
    $matches = array();

    // Collect matching IDs
    if (!empty($wk_vendor_custom_fields['S'])) {
        foreach ($wk_vendor_custom_fields['S'] as $v) {
            $matches[$v['matching_id']] = $v['field_id'];
        }
    }

    foreach ($wk_vendor_custom_fields as $section => $fields) {
        foreach ($fields as $k => $v) {
            if ($v['field_type'] == 'S' || $v['field_type'] == 'R') {
                $_id = $v['field_id'];
                if ($section == 'B' && empty($v['field_name'])) {
                    // If this field is enabled in billing section
                    if (!empty($matches[$v['field_id']])) {
                        $_id = $matches[$v['field_id']];
                    // Otherwise, get it from database
                    } else {
                        $_id = db_get_field("SELECT field_id FROM ?:wk_vendor_custom_fields WHERE matching_id = ?i", $v['field_id']);
                    }
                }
                $wk_vendor_custom_fields[$section][$k]['values'] = db_get_hash_single_array("SELECT ?:wk_vendor_custom_field_values.value_id, ?:wk_vendor_custom_fields_description.description FROM ?:wk_vendor_custom_field_values LEFT JOIN ?:wk_vendor_custom_fields_description ON ?:wk_vendor_custom_fields_description.object_id = ?:wk_vendor_custom_field_values.value_id AND ?:wk_vendor_custom_fields_description.object_type = 'V' AND ?:wk_vendor_custom_fields_description.lang_code = ?s WHERE ?:wk_vendor_custom_field_values.field_id = ?i ORDER BY ?:wk_vendor_custom_field_values.position", array('value_id', 'description'), $lang_code, $_id);
            }
        }
    }

    if (!empty($params['field_id'])) {
        $result = reset($wk_vendor_custom_fields);
        if (!empty($result[$params['field_id']])) {
            return $result[$params['field_id']];
        } else {
            return array();
        }
    }
 
    $sections=array();
    foreach ($wk_vendor_custom_fields as $key => $value) {
       $sections[$key]=true;
    }
    return $wk_vendor_custom_fields;
}


//Get all custom sections
function fn_wk_vendor_custom_registration_get_all_custom_sections($lang_code=CART_LANGUAGE)
{
    $_gat_all_section_ids=db_get_array("SELECT section_id FROM ?:wk_vendor_custom_sections");
    $all_sections=array();
    foreach ($_gat_all_section_ids as $key => $value) {
        $description=db_get_field("SELECT description FROM ?:wk_vendor_custom_sections_description WHERE object_id=?i AND lang_code=?s",$value['section_id'],$lang_code);
        $all_sections[$key]['section_id']=$value['section_id'];
        $all_sections[$key]['description']=$description;
    }
    return $all_sections;
}

function fn_wk_vendor_custom_registration_add_field_values($values = array(), $field_id = 0)
{
    if (empty($values) || empty($field_id)) {
        return false;
    }

    foreach ($values as $_v) {

        if (empty($_v['description'])) {
            continue;
        }
        // Insert main data
        $_v['field_id'] = $field_id;
        $value_id = db_query("INSERT INTO ?:wk_vendor_custom_field_values ?e", $_v);

        // Insert descriptions
        $_data = array (
            'object_id' => $value_id,
            'object_type' => 'V',
            'description' => $_v['description'],
        );
        foreach (fn_get_translation_languages() as $_data['lang_code'] => $_v) {
            db_query("INSERT INTO ?:wk_vendor_custom_fields_description ?e", $_data);
        }
    }

    return true;
}

function fn_wk_vendor_custom_registration_delete_field_values($field_id, $value_ids = array())
{
    if (empty($value_ids)) {
        $value_ids = db_get_fields("SELECT value_id FROM ?:wk_vendor_custom_field_values WHERE field_id = ?i", $field_id);
    }

    if (!empty($value_ids)) {
        db_query("DELETE FROM ?:wk_vendor_custom_fields_description WHERE object_id IN (?n) AND object_type = 'V'", $value_ids);
        db_query("DELETE FROM ?:wk_vendor_custom_field_values WHERE value_id IN (?n)", $value_ids);
    }
}

function fn_delete_wk_vendor_custom_field($field_id, $no_match = false)
{
    $matching_id = db_get_field("SELECT matching_id FROM ?:wk_vendor_custom_fields WHERE field_id = ?i", $field_id);
    if (!$no_match && !empty($matching_id)) {
        fn_delete_wk_vendor_custom_field($matching_id, true);
    }

    fn_wk_vendor_custom_registration_delete_field_values($field_id);
    db_query("DELETE FROM ?:wk_vendor_custom_fields WHERE field_id = ?i", $field_id);
    db_query("DELETE FROM ?:wk_vendor_custom_fields_description WHERE object_id = ?i AND object_type = 'F'", $field_id);
}

function fn_wk_vendor_custom_fields_areas()
{
    $areas = array (
        'profile' => 'registration'
    );
    return $areas;
}

function fn_delete_wk_vendor_custom_section($section_id)
{
    $get_section_fields=db_get_array("SELECT field_id FROM ?:wk_vendor_custom_fields WHERE section=?s",$section_id);
    
    if(!empty($get_section_fields))
    {
        foreach ($get_section_fields as $key => $field_id){
           fn_delete_wk_vendor_custom_field($field_id['field_id']);
        }
    }
    db_query("DELETE FROM ?:wk_vendor_custom_sections WHERE section_id=?i",$section_id);
    db_query("DELETE FROM ?:wk_vendor_custom_sections_description
    WHERE object_id=?i",$section_id);
}

function fn_wk_vendor_custom_fields_get_default_lang_description($object_id) {
    return db_get_field("SELECT description FROM ?:wk_vendor_custom_fields_description WHERE object_id=?i AND lang_code=?s",$object_id,'en');
} 