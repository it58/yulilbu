<?php
/******************************************************************
# Vendor Custom Registration - Vendor Custom Registration                                               *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/  
use Tygh\Registry;
use Tygh\Mailer;
if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'apply_for_vendor') 
    {
        if (Registry::get('settings.Vendors.apply_for_vendor') != 'Y') {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }
        if (fn_image_verification('apply_for_vendor_account', $_REQUEST) == false) {
            fn_save_post_data('user_data', 'user_data');

            return array(CONTROLLER_STATUS_REDIRECT, 'companies.apply_for_vendor');
        }

        $data = $_REQUEST['user_data'];

        $data['timestamp'] = TIME;
        $data['status'] = 'N';
        $data['request_user_id'] = !empty($auth['user_id']) ? $auth['user_id'] : 0;

        $account_data = array();
        $account_data['fields'] = isset($_REQUEST['user_data']['fields']) ? $_REQUEST['user_data']['fields'] : '';
        $account_data['admin_firstname'] = isset($_REQUEST['user_data']['admin_firstname']) ? $_REQUEST['user_data']['admin_firstname'] : '';
        $account_data['admin_lastname'] = isset($_REQUEST['user_data']['admin_lastname']) ? $_REQUEST['user_data']['admin_lastname'] : '';
        $data['request_account_data'] = serialize($account_data);

        if (empty($data['request_user_id'])) {
            $login_condition = empty($data['request_account_name']) ? '' : db_quote(" OR user_login = ?s", $data['request_account_name']);
            $user_account_exists = db_get_field("SELECT user_id FROM ?:users WHERE email = ?s ?p", $data['email'], $login_condition);

            if ($user_account_exists) {
                fn_save_post_data('user_data', 'user_data');
                fn_set_notification('E', __('error'), __('error_user_exists'));

                return array(CONTROLLER_STATUS_REDIRECT, 'companies.apply_for_vendor');
            }
        }

        $company_id = fn_update_company($data);
        $data = array_merge($data, fn_get_company_data($company_id));

        if (!$company_id) {
            fn_save_post_data('user_data', 'user_data');
            fn_set_notification('E', __('error'), __('text_error_adding_request'));

            return array(CONTROLLER_STATUS_REDIRECT, 'companies.apply_for_vendor');
        }
        if(isset($_REQUEST['user_data']['fields'])&& !empty($_REQUEST['user_data']['fields']))
        {
            $extra_data=serialize($_REQUEST['user_data']['fields']);
            $_data=array(
                'wk_vendor_custom_fields'=>$extra_data
                );
            db_query("UPDATE ?:companies SET ?u WHERE company_id = ?i", $_data, $company_id);
        }
        $msg = Registry::get('view')->fetch('views/companies/components/apply_for_vendor.tpl');
        fn_set_notification('I', __('information'), $msg);

        // Notify user department on the new vendor application
        Mailer::sendMail(array(
            'to' => 'default_company_users_department',
            'from' => 'default_company_users_department',
            'data' => array(
                'company_id' => $company_id,
                'company' => $data
            ),
            'tpl' => 'companies/apply_for_vendor_notification.tpl',
        ), 'A', Registry::get('settings.Appearance.backend_default_language'));

        $return_url = !empty($_SESSION['apply_for_vendor']['return_url']) ? $_SESSION['apply_for_vendor']['return_url'] : fn_url('');
        unset($_SESSION['apply_for_vendor']['return_url']);

        return array(CONTROLLER_STATUS_REDIRECT, $return_url);
    }
}