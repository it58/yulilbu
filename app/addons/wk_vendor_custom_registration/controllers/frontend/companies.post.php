<?php
/******************************************************************
# Vendor Custom Registration - Vendor Custom Registration                                               *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/  
use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'apply_for_vendor') 
{       
   $wk_vendor_custom_fields=fn_wk_vendor_custom_registration_get_vendor_custom_fields(AREA, $auth ,CART_LANGUAGE, '');
   Registry::get('view')->assign('wk_vendor_custom_fields',$wk_vendor_custom_fields);
   Registry::get('view')->assign('all_sections', fn_wk_vendor_custom_registration_get_all_custom_sections()); 
}



