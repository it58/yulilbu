<?php
/******************************************************************
# Vendor Custom Registration - Vendor Custom Registration                                               *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/  
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $_suffix = '.manage';

    if($mode =='custom_sections')
    {
        $_data=array(
            'section_key'=>'NS'
        );
        $section_id=db_query("INSERT INTO ?:wk_vendor_custom_sections ?e",$_data);
        $data=array(
        'object_id'=>$section_id,
        'description'=>$_REQUEST['section_info']['section_description'],
        'lang_code'=>DESCR_SL
        );
        db_query("INSERT INTO ?:wk_vendor_custom_sections_description ?e",$data);
        return array(CONTROLLER_STATUS_OK, 'wk_vendor_custom_fields.custom_sections'); 
    }
    if($mode =='custom_section_delete')
    {
        if (!empty($_REQUEST['section_ids'])) {
            foreach ($_REQUEST['section_ids'] as $section_id) {
                fn_delete_wk_vendor_custom_section($section_id);
            }
        }
        return array(CONTROLLER_STATUS_OK, 'wk_vendor_custom_fields.custom_sections');
    }
    if($mode =='delete_custom_section')
    {
        if (isset($_REQUEST['section_id'])&&!empty($_REQUEST['section_id'])) {
            
            fn_delete_wk_vendor_custom_section($_REQUEST['section_id']);
           
        }
        return array(CONTROLLER_STATUS_OK, 'wk_vendor_custom_fields.custom_sections');
    }
    if($mode =='edit_custom_sections')
    {
        if(isset($_REQUEST['section_info']))
        {
            $check_custom_section=db_get_field("SELECT object_id FROM ?:wk_vendor_custom_sections_description WHERE object_id=?i AND lang_code=?s",$_REQUEST['section_info']['section_id'],DESCR_SL);
            $data=array(
                'object_id'=>$_REQUEST['section_info']['section_id'],
                'description'=>$_REQUEST['section_info']['section_description'],
                'lang_code'=>DESCR_SL
            );
            if (!empty($check_custom_section)) {
                db_query("UPDATE ?:wk_vendor_custom_sections_description SET ?u WHERE object_id=?i AND lang_code=?s",$data,$_REQUEST['section_info']['section_id'],DESCR_SL);
            } else {
                db_query("INSERT INTO ?:wk_vendor_custom_sections_description ?e",$data);
            }
        }
        return array(CONTROLLER_STATUS_OK, 'wk_vendor_custom_fields.custom_sections');
    }
    if ($mode == 'update') {

        $field_data = $_REQUEST['field_data'];

        $field_id = fn_wk_vendor_custom_registration_update_vendor_custom_fields($field_data, $_REQUEST['field_id'], DESCR_SL);

        $_suffix = '.update?field_id=' . $field_id;
    }
    if ($mode == 'm_update') {
        if (!empty($_REQUEST['fields_data'])) {
            $fields_data = $_REQUEST['fields_data'];
            if (isset($fields_data['email'])) {
                foreach ($fields_data['email'] as $enable_for => $field_id) {
                    $fields_data[$field_id][$enable_for] = 'Y';
                }

                unset($fields_data['email']);
            }

            foreach ($fields_data as $field_id => $data) {
                fn_wk_vendor_custom_registration_update_vendor_custom_fields($data, $field_id, DESCR_SL);
            }
        }
    }
    if ($mode == 'm_delete') {
        if (!empty($_REQUEST['field_ids'])) {
            foreach ($_REQUEST['field_ids'] as $field_id) {
                fn_delete_wk_vendor_custom_field($field_id);
            }
        }

        if (!empty($_REQUEST['value_ids'])) {
            foreach ($_REQUEST['value_ids'] as $value_id) {
                db_query("DELETE FROM ?:wk_vendor_custom_fields_description WHERE object_id = ?i AND object_type = 'V'", $value_id);
                db_query("DELETE FROM ?:wk_vendor_custom_field_values WHERE value_id = ?i", $value_id);
            }
        }
    }
    if ($mode == 'delete') {
        if (!empty($_REQUEST['field_id'])) {
            fn_delete_wk_vendor_custom_field($_REQUEST['field_id']);
        }
    }    

    return array(CONTROLLER_STATUS_OK, 'wk_vendor_custom_fields' . $_suffix);
}

if ($mode == 'manage') {

    $wk_vendor_custom_fields = fn_wk_vendor_custom_registration_get_vendor_custom_fields('ALL', array(), DESCR_SL);

    Registry::get('view')->assign('wk_vendor_custom_fields_areas', fn_wk_vendor_custom_fields_areas());
   Registry::get('view')->assign('wk_vendor_custom_fields', $wk_vendor_custom_fields);
   Registry::get('view')->assign('all_sections', fn_wk_vendor_custom_registration_get_all_custom_sections());

} elseif ($mode == 'update' || $mode == 'add') {

    if ($mode == 'update') {
        $params['field_id'] = $_REQUEST['field_id'];
        $field = fn_wk_vendor_custom_registration_get_vendor_custom_fields('ALL', array(), DESCR_SL, $params);

        Registry::get('view')->assign('field', $field);
    }

    Registry::get('view')->assign('wk_vendor_custom_fields_areas', fn_wk_vendor_custom_fields_areas());

    //GET all custom sections and assign to tpl
    Registry::get('view')->assign('all_sections', fn_wk_vendor_custom_registration_get_all_custom_sections());
}
if($mode =='custom_sections')
{
   Registry::get('view')->assign('all_sections', fn_wk_vendor_custom_registration_get_all_custom_sections());
}
if($mode =='edit_custom_sections')
{
    if(isset($_REQUEST['section_id']))
    {
        $description=db_get_field("SELECT description FROM ?:wk_vendor_custom_sections_description WHERE object_id=?i AND lang_code=?s",$_REQUEST['section_id'],DESCR_SL);
        Registry::get('view')->assign('description',$description);
        Registry::get('view')->assign('section_id',$_REQUEST['section_id']);
    }
}
?>