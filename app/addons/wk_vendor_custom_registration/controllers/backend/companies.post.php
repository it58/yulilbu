<?php
/******************************************************************
# Vendor Custom Registration - Vendor Custom Registration                                               *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/  
use Tygh\Registry;
use Tygh\Settings;
use Tygh\BlockManager\Layout;
use Tygh\Themes\Styles;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
    $suffix = '';
	if ($mode == 'update') { 
        if(isset($_REQUEST['company_data']['fields'])&& !empty($_REQUEST['company_data']['fields']))
        {
            $extra_data=serialize($_REQUEST['company_data']['fields']);
            $_data=array(
                'wk_vendor_custom_fields'=>$extra_data
                );
            db_query("UPDATE ?:companies SET ?u WHERE company_id = ?i", $_data, $_REQUEST['company_id']);
        }
        
 	}
}
if($mode == 'update' || $mode == 'add')
{
	$params=array();
	$result=array();
	$result1=array();
	if(isset($_REQUEST['company_id']))
	{
		$extra_company_data=fn_get_company_data($_REQUEST['company_id']);
		$result=fn_wk_vendor_custom_registration_get_vendor_custom_fields(AREA,'',CART_LANGUAGE, $params);
		if(!empty($extra_company_data))
		{
			if(isset($extra_company_data['wk_vendor_custom_fields']))
			{
				$total_extra_field_data=array();
				$extra_fields=$extra_company_data['wk_vendor_custom_fields'];
				$extra_fields=unserialize($extra_fields);
				foreach ($extra_fields as $key => $value){
					foreach ($result as $section => $value2) {
						foreach ($value2 as $field_id => $value3)
						{
							if($key==$field_id)
							{
								
								$result1[$section][$field_id]=$value3;
							}
						}
						
					}
				}
				Registry::get('view')->assign('extra_fields',$extra_fields);
			}
		}
		Registry::get('view')->assign('wk_vendor_custom_fields',$result);
	}
	else 
	{
		$result=fn_wk_vendor_custom_registration_get_vendor_custom_fields(AREA,'',CART_LANGUAGE, $params);
		Registry::get('view')->assign('wk_vendor_custom_fields',$result);
	}	
	Registry::get('view')->assign('all_sections', fn_wk_vendor_custom_registration_get_all_custom_sections()); 
}
