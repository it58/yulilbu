
INSERT INTO ?:wk_vendor_custom_fields VALUES (1,'company','Y','Y','I',10,'Y','O',0,'company','organization');
INSERT INTO ?:wk_vendor_custom_fields VALUES (2,'company_description','Y','N','T',20,'Y','O',0,'company','organization');
INSERT INTO ?:wk_vendor_custom_fields VALUES (3,'lang_code','Y','N','S',30,'Y','O',0,'language','language');
INSERT INTO ?:wk_vendor_custom_fields VALUES (4,'firstname','Y','Y','I',40,'Y','C',0,'first-name','given-name');
INSERT INTO ?:wk_vendor_custom_fields VALUES (5,'lastname','Y','Y','I',50,'Y','C',0,'last-name','surname');
INSERT INTO ?:wk_vendor_custom_fields VALUES (6,'email','Y','Y','E',60,'Y','C',0,'billing-email','email');
INSERT INTO ?:wk_vendor_custom_fields VALUES (7,'phone','Y','Y','I',70,'Y','C',0,'phone','phone-full');

INSERT INTO ?:wk_vendor_custom_fields VALUES (8,'url','Y','N','I',80,'Y','C',0,'url','url');
INSERT INTO ?:wk_vendor_custom_fields VALUES (9,'fax','Y','N','I',90,'Y','C',0,'fax','fax-full');
INSERT INTO ?:wk_vendor_custom_fields VALUES (10,'address','Y','Y','I',100,'Y','S',0,'billing-address','street-address');
INSERT INTO ?:wk_vendor_custom_fields VALUES (11,'city','Y','Y','I',110,'Y','S',0,'billing-city','city');
INSERT INTO ?:wk_vendor_custom_fields VALUES (12,'country','Y','Y','O',130,'Y','S',0,'billing-country','country');
INSERT INTO ?:wk_vendor_custom_fields VALUES (13,'state','Y','Y','A',120,'Y','S',0,'billing-state','state');

INSERT INTO ?:wk_vendor_custom_fields VALUES (14,'zipcode','Y','Y','Z',140,'Y','S',0,'billing-zip-code','postal-code');

INSERT INTO ?:wk_vendor_custom_fields_description VALUES (1,'Company','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (2,'Description','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (3,'Language','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (4,'First name','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (5,'Last name','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (6,'Email','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (7,'Phone','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (8,'URL','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (9,'Fax','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (10,'Address','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (11,'City','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (12,'Country','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (13,'State/Province','F','en');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (14,'Zip/postal code','F','en');

INSERT INTO ?:wk_vendor_custom_fields_description VALUES (1,'Компания','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (2,'Описание','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (3,'язык','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (4,'Имя','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (5,'Фамилия','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (6,'Эл. адрес','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (7,'Телефон','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (8,'URL','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (9,'факс','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (10,'Адрес','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (11,'город','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (12,'Страна','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (13,'Штат / провинция','F','ru');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (14,'Почтовый индекс','F','ru');


INSERT INTO ?:wk_vendor_custom_fields_description VALUES (1,'Empresa','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (2,'Descripción','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (3,'Idioma','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (4,'Nombre de pila','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (5,'Apellido','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (6,'Email','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (7,'Teléfono','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (8,'URL','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (9,'Fax','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (10,'Dirección','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (11,'Ciudad','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (12,'País','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (13,'Estado / Provincia','F','es');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (14,'código postal','F','es');


INSERT INTO ?:wk_vendor_custom_fields_description VALUES (1,'Perusahaan','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (2,'Deskripsi','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (3,'Bahasa','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (4,'Nama depan','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (5,'Nama keluarga','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (6,'E-mail','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (7,'Telepon','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (8,'URL','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (9,'Fax','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (10,'Alamat','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (11,'Kota','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (12,'Negara','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (13,'Negara Bagian / Provinsi','F','in');
INSERT INTO ?:wk_vendor_custom_fields_description VALUES (14,'Kode Pos','F','in');


