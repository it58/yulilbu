<?php
$schema['top']['administration']['items']['wk_vendor_custom_fields'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'wk_vendor_custom_fields.manage',
    'position' => 505,
);
return $schema;
