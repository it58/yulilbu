<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

$auth = Tygh::$app['session']['auth'];

$addon_dir = (Registry::get('config.dir.addons'));
require_once($addon_dir . 'sd_messaging_system/func.php');

$schema['messenger'] = array (
    'permissions' => fn_sd_messaging_system_check_messenger_permission($auth) ? 'Y' : 'N',
);

return $schema;