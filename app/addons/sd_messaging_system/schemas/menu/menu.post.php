<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['central']['website']['items']['messages_data'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'messenger.tickets_list',
    'position' => 1900
);

$schema['central']['website']['items']['messenger_forbidden_words'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'messenger.forbidden_words',
    'position' => 2000
);

return $schema;