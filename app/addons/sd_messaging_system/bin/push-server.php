<?php

define('AREA', 'A');

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__FILE__) . '/../../../../init.php';

use Tygh\Registry;

$addon_data = Registry::get('addons.sd_messaging_system');

if (!empty($addon_data['websocket_port']) && !empty($addon_data['pusher_port'])) {
    $loop   = React\EventLoop\Factory::create();
    $pusher = new MyApp\Pusher;

    // Listen for the web server to make a ZeroMQ push after an ajax request
    $context = new React\ZMQ\Context($loop);
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);

    try {
        $pull->bind('tcp://127.0.0.1:' . $addon_data['pusher_port']); // Binding to 127.0.0.1 means the only client that can connect is itself
        $pusher->onLog('New Pusher connect', 'INFO');
    } catch (Exception $e) {
        $pusher->onLog('Error Pusher connect: ' . $e->getMessage(), 'ERROR');
    }

    $pull->on('message', array($pusher, 'onTicketEntry'));

    // Set up our WebSocket server for clients wanting real-time updates
    try {
        $webSock = new React\Socket\Server('0.0.0.0:' . $addon_data['websocket_port'], $loop); // Binding to 0.0.0.0 means remotes can connect
        $pusher->onLog('New WebSocket connect', 'INFO');
    } catch (Exception $e) {
        $pusher->onLog('Error WebSocket connect: ' . $e->getMessage(), 'ERROR');
    }

    $webServer = new Ratchet\Server\IoServer(
        new Ratchet\Http\HttpServer(
            new Ratchet\WebSocket\WsServer(
                new Ratchet\Wamp\WampServer(
                    $pusher
                )
            )
        ),
        $webSock
    );

    $loop->run();
}
