<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Navigation\LastView;
use Tygh\Mailer\Message;

function fn_sd_messaging_system_create_new_ticket($author_id = 0, $recipient_id = 0)
{
    $result = false;

    $company_id = $recipient_id;
    $author_type = AREA == 'A' ? AUTHOR_TYPE_VENDOR : AUTHOR_TYPE_CUSTOMER;

    if (AREA == 'A') {
        $recipient_id = $author_id;
        $author_id = $company_id;
    }

    if ($author_id && $recipient_id) {

        $result = db_query(
            'INSERT INTO ?:messenger_tickets
            (author_id, recipient_id, company_id, timestamp, author_type)
            VALUES (?i, ?i, ?i, ?i, ?s)',
            $author_id,
            $recipient_id,
            $company_id,
            TIME,
            $author_type
        );
    }

    return $result;
}

function fn_sd_messaging_system_get_ticket_id($author_id = 0, $recipient_id = 0)
{
    $ticket_id = 0;

    if ($author_id && $recipient_id) {
        $ticket_id = db_get_field(
            'SELECT ticket_id
            FROM ?:messenger_tickets
            WHERE ((author_id = ?i AND recipient_id = ?i)
                OR (author_id = ?i AND recipient_id = ?i))',
            $author_id,
            $recipient_id,
            $recipient_id,
            $author_id
        );
    }

    return $ticket_id;
}


function fn_sd_messaging_system_get_company_id($recipient_id = 0)
{
    $company_id = 0;
    if ($recipient_id) {
        $company_id = db_get_field(
            'SELECT company_id
            FROM ?:users
            WHERE user_id = ?i',
            $recipient_id
        );
    }

    return $company_id;
}

function fn_sd_messaging_system_is_ticket_available($ticket_id)
{
    $auth = Tygh::$app['session']['auth'];
    $result = false;

    if (!empty($auth['user_id'])) {
        $result =  db_get_field(
            'SELECT ticket_id
            FROM ?:messenger_tickets
            WHERE author_id = ?i OR recipient_id = ?i',
            $auth['user_id'],
            $auth['user_id']
        );
    }

    return $result;
}

function fn_sd_messaging_system_get_user_tickets($user_id = 0)
{

    $tickets = array();

    if (!empty($user_id)) {
        $fields = array(
            'ticket_id',
            'author_id',
            'recipient_id',
            'timestamp',
            'last_message_author_id',
            'last_message_timestamp',
            'last_message',
            'author_type',
            'company_id',
            'last_message_author_type',
            'status',
            'need_highlight'
        );

        $condition = db_quote('AND IF (author_type = ?s, recipient_id = ?i, author_id = ?i)', AUTHOR_TYPE_VENDOR, $user_id, $user_id);

        $tickets = db_get_array(
            'SELECT '
            . implode(', ', $fields)
            . " FROM ?:messenger_tickets
            WHERE 1 $condition
            ORDER BY last_message_timestamp DESC"
        );

        fn_sd_messaging_system_check_tickets_status($tickets, AUTHOR_TYPE_VENDOR);
    }

    return $tickets;
}

function fn_sd_messaging_system_check_tickets_status(&$tickets, $author_type)
{
    if (!empty($tickets)) {
        foreach ($tickets as $key => $ticket) {
            if ($ticket['status'] == 'N') {
                $tickets[$key]['status'] = $ticket['last_message_author_type'] == $author_type ? TICKET_STATUS_NEW : TICKET_STATUS_VIEWED;
            }
        }
    }
}

function fn_sd_messaging_system_fetch_tickets_data($tickets = array(), $fetch_images = false)
{
    $tickets = is_array($tickets) ? $tickets : array();
    $auth = Tygh::$app['session']['auth'];
    $user_ids = array();
    $users_data = array();
    $user_images = array();

    foreach ($tickets as $key => $ticket_data) {
        $tickets[$key]['last_message_date'] = '';
        $tickets[$key]['last_message'] = !empty($ticket_data['last_message']) ? $ticket_data['last_message'] : __('no_messages_yet');

        if (!empty($ticket_data['last_message_timestamp'])) {
            $tickets[$key]['last_message_date'] = fn_sd_messaging_system_convert_ticket_timestamp_to_date($ticket_data['last_message_timestamp']);
        }

        if (!empty($ticket_data['author_id'])) {
            $user_ids[] = $ticket_data['author_id'];
        }

        if (!empty($ticket_data['recipient_id'])) {
            $user_ids[] = $ticket_data['recipient_id'];
        }
    }

    if ($user_ids) {
        $user_ids = array_unique($user_ids);

        $users_data = db_get_hash_array(
            'SELECT user_id, firstname, lastname
            FROM ?:users
            WHERE user_id IN (?n)',
            'user_id',
            $user_ids
        );

        if ($fetch_images && Registry::get('addons.sd_user_profile.status') == 'A') {
            $user_images = fn_get_image_pairs($user_ids, 'userpic', 'M', true, true);
        }
    }

    foreach ($tickets as $key => $ticket_data) {
        if (!empty($ticket_data['company_id']) && $fetch_images) {
            $logos = fn_get_logos($ticket_data['company_id']);
            if (isset($logos['theme']['image'])) {
                $tickets[$key]['vendor_image'] = $logos['theme']['image'];
            }
            $tickets[$key]['vendor_name'] = AREA == 'C' ? fn_get_company_name($ticket_data['company_id']) : __('you');
        }

        $customer_id = $ticket_data['author_type'] == AUTHOR_TYPE_CUSTOMER ? $ticket_data['author_id'] : $ticket_data['recipient_id'];
        $tickets[$key]['customer_id'] = $customer_id;
        $customer_name = '';
        if (!empty($user_images[$customer_id])) {
            $user_images[$customer_id] = array_values($user_images[$customer_id]);
            $tickets[$key]['customer_image'] = reset($user_images[$customer_id]);
        }

        if (!empty($users_data[$customer_id]['firstname'])) {
            $customer_name .= $users_data[$customer_id]['firstname'];
        }
        if (!empty($users_data[$customer_id]['lastname'])) {
            $customer_name .= !empty($users_data[$customer_id]['lastname']) ? ' ' . $users_data[$customer_id]['lastname'] : $users_data[$customer_id]['lastname'];
        }
        $tickets[$key]['customer_name'] = AREA == 'A' ? $customer_name : __('you');

        if (!empty($ticket_data['ticket_id']) && $ticket_data['status'] == TICKET_STATUS_NEW) {
            $tickets[$key]['new_messages_amount'] = fn_sd_messaging_system_get_messages_amount($auth['user_id'], $ticket_data['ticket_id']);
        }
    }

    return $tickets;
}

function fn_sd_messaging_system_convert_ticket_timestamp_to_date($timestamp)
{
    static $today = null;
    static $yesterday = null;

    $proceed = false;
    $time_string = '';
    $current_date = new DateTime();
    $current_date->setTimestamp($timestamp);

    if ($current_date === false) {
        return '';
    }

    if ($today === null) {
        $today = new DateTime();
        $today->setTime(0, 0, 0);
    }

    if ($yesterday === null) {
        $yesterday = new DateTime('-1day');
        $yesterday->setTime(0, 0, 0);
    }

    if ($today <= $current_date) {
        $time_string = $current_date->format(MESSAGE_TODAY_DATE_FORMAT);
    } elseif ($current_date < $today && $yesterday <= $current_date) {
        $time_string = __('yesterday');
    } elseif ($today->format('Y') == $current_date->format('Y')) {
        $time_string = $current_date->format(MESSAGE_SAME_YEAR_DATE_FORMAT);
    } elseif ($today->format('Y') > $current_date->format('Y')) {
        $time_string = $current_date->format(MESSAGE_LAST_YEAR_DATE_FORMAT);
    }

    return $time_string;
}

function fn_sd_messaging_system_get_ticket_by_id($ticket_id = 0)
{
    $auth = Tygh::$app['session']['auth'];
    $ticket = array();

    if ($ticket_id) {
        $condition = db_quote(' AND ticket_id = ?i ', $ticket_id);
        $company_id = Registry::get('runtime.company_id');

        if (AREA == 'A' && !empty($company_id)) {
            $condition .= db_quote(' AND company_id = ?i ', $company_id);
        } elseif (AREA == 'C' && !empty($auth['user_id'])) {
            $condition .= db_quote('AND IF (author_type = ?s, recipient_id = ?i, author_id = ?i)', AUTHOR_TYPE_VENDOR, $auth['user_id'], $auth['user_id']);
        }

        $ticket = db_get_row(
            'SELECT ticket_id, author_id, recipient_id, company_id, author_type, status, last_message_author_id, last_message_author_type, need_highlight 
            FROM ?:messenger_tickets
            WHERE 1'
            . $condition
        );
    }

    return $ticket;
}

function fn_sd_messaging_system_get_ticket_messages($ticket_id)
{
    $messages = array();

    if ($ticket_id) {
        $messages = db_get_array(
            'SELECT message_id, ticket_id, author_id, message, author_type, timestamp, need_highlight 
            FROM ?:messenger_messages
            WHERE ticket_id = ?i
            ORDER BY timestamp ASC',
            $ticket_id
        );

        foreach ($messages as $key => $message) {
            $messages[$key]['date'] = fn_sd_messaging_system_convert_message_timestamp_to_date($message['timestamp']);
            $messages[$key]['message'] = fn_sd_messaging_system_replace_url_message($message['message']);
        }
    }

    return $messages;
}


function fn_sd_messaging_system_get_ticket_messages_with_sorting($ticket_id, $params, $items_per_page = 10, $lang_code = CART_LANGUAGE, $custom_view = 'ticket_messages')
{
    // Init filter
    $params = LastView::instance()->update($custom_view, $params);

    $condition = $join = $group = '';

    // Define fields that should be retrieved
    $fields = array (
        'message_id',
        'ticket_id',
        'author_id',
        'message',
        'timestamp',
        'author_type',
        'need_highlight'
    );

    // Define sort fields
    $sortings = array (
        'date' => 'timestamp',
        'author' => 'author_id',
    );

    $condition .= db_quote(' AND ticket_id = ?i ', $ticket_id);

    if (isset($params['message']) && fn_string_not_empty($params['message'])) {
        $condition .= db_quote(' AND message LIKE ?l ', '%' . trim($params['message']) . '%');
    }

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(' AND (timestamp >= ?i AND timestamp <= ?i)', $params['time_from'], $params['time_to']);
    }

    $sorting = db_sort($params, $sortings, 'date', 'asc');

    $messages = db_get_array(
        'SELECT '
        . implode($fields, ', ') .
        ' FROM ?:messenger_messages
        WHERE 1 '
        . $condition
        . $sorting
    );

    foreach ($messages as $key => $message) {
        $messages[$key]['date'] = fn_sd_messaging_system_convert_message_timestamp_to_date($message['timestamp']);
        $messages[$key]['message'] = fn_sd_messaging_system_replace_url_message($message['message']);
    }

    return array($messages, $params);
}

function fn_sd_messaging_system_save_new_message($ticket_id = 0, $author_id = 0, $message)
{
    if (AREA == 'A' && !empty($ticket_id)) {
        $ticket_data = db_get_row('SELECT author_id, recipient_id, author_type, amount FROM ?:messenger_tickets WHERE ticket_id = ?i', $ticket_id);
        $author_id = $ticket_data['author_type'] == AUTHOR_TYPE_CUSTOMER ? $ticket_data['recipient_id'] : $ticket_data['author_id'];
    }

    if (!isset($ticket_data['amount']) && !empty($ticket_id)) {
        $ticket_data['amount'] = db_get_field('SELECT amount FROM ?:messenger_tickets WHERE ticket_id = ?i', $ticket_id);
    }

    if (!empty($ticket_id) && !empty($author_id) && (!empty($message) || is_numeric($message))) {
        $need_highlight = fn_sd_messaging_system_replace_forbidden_words($message);
        $author_type = AREA == 'C' ? AUTHOR_TYPE_CUSTOMER : AUTHOR_TYPE_VENDOR;
        $message_data = array(
            'ticket_id' => $ticket_id,
            'author_id' => $author_id,
            'message' => $message,
            'timestamp' => TIME,
            'author_type' => $author_type,
            'need_highlight' => $need_highlight ? 'Y' : 'N'
        );

        $last_message_id = db_query('INSERT INTO ?:messenger_messages ?e', $message_data);
    }

    if ($last_message_id) {
        $old_need_highlight = db_get_field('SELECT need_highlight FROM ?:messenger_tickets WHERE ticket_id = ?i', $ticket_id);
        if (!$need_highlight && $old_need_highlight == 'Y') {
            $need_highlight = true;
        }
        $last_message_author_type = AREA == 'A' ? AUTHOR_TYPE_VENDOR : AUTHOR_TYPE_CUSTOMER;

        $last_message_data = array(
            'last_message_author_id' => $author_id,
            'last_message_timestamp' => TIME,
            'last_message' => $message,
            'last_message_author_type' => $last_message_author_type,
            'status' => TICKET_STATUS_NEW,
            'amount' => $ticket_data['amount'] + 1,
            'need_highlight' => $need_highlight ? 'Y' : 'N'
        );

        $result = db_query(
            'UPDATE ?:messenger_tickets
            SET ?u
            WHERE ticket_id = ?i',
            $last_message_data,
            $ticket_id
        );

        db_query('UPDATE ?:messenger_tickets SET ?u WHERE ticket_id = ?i', $last_message_data, $ticket_id);
    }

    return $last_message_id;
}

function fn_sd_messaging_system_get_all_tickets($params, $items_per_page = 10, $custom_view = 'tickets')
{
    $auth = Tygh::$app['session']['auth'];
    $company_id = Registry::get('runtime.company_id');
    // Init filter
    $params = LastView::instance()->update($custom_view, $params);

    // Set default values to input params
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page,
    );

    $params = array_merge($default_params, $params);
    $condition = $join = $group = array();

    // Define fields that should be retrieved
    $fields = array (
        'tickets.*',
        db_quote('IF (tickets.author_type = ?s, CONCAT(users1.firstname, " ", users1.lastname), comp.company) as author_name', AUTHOR_TYPE_CUSTOMER),
        db_quote('IF (tickets.author_type = ?s, CONCAT(users2.firstname, " ", users2.lastname), comp.company) as recipient_name', AUTHOR_TYPE_VENDOR),
        'comp.company',
    );

    if (isset($params['ticket_id']) && fn_string_not_empty($params['ticket_id'])) {
        $condition['ticket_id'] = db_quote(' AND tickets.ticket_id = ?i', $params['ticket_id']);
    }

    if (isset($params['author']) && fn_string_not_empty($params['author'])) {
        $condition['author'] = db_quote(' AND IF (tickets.author_type = ?s, CONCAT(users1.firstname, " ", users1.lastname), comp.company) LIKE ?l', AUTHOR_TYPE_CUSTOMER, '%' . trim($params['author']) . '%');
    }

    if (isset($params['recipient']) && fn_string_not_empty($params['recipient'])) {
        $condition['recipient'] = db_quote(' AND IF (tickets.author_type = ?s, CONCAT(users2.firstname, " ", users2.lastname), comp.company) LIKE ?l', AUTHOR_TYPE_VENDOR, '%' . trim($params['recipient']) . '%');
    }

    if (AREA == 'A' && !empty($company_id)) {
        $condition['company_id'] = db_quote(' AND tickets.company_id = ?i ', $company_id);
    } elseif (isset($params['company']) && fn_string_not_empty($params['company'])) {
        $condition['company'] = db_quote(' AND comp.company LIKE ?l', '%' . trim($params['company']) . '%');
    }

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);

        $condition['period'] = db_quote(' AND (messages.timestamp >= ?i AND messages.timestamp <= ?i)', $params['time_from'], $params['time_to']);
        $join[] = db_quote(' LEFT JOIN ?:messenger_messages as messages ON tickets.ticket_id = messages.ticket_id');
    }

    $join[] = db_quote(' LEFT JOIN ?:users as users1 ON tickets.author_id = users1.user_id');
    $join[] = db_quote(' LEFT JOIN ?:users as users2 ON tickets.recipient_id = users2.user_id');
    $join[] = db_quote(' LEFT JOIN ?:companies as comp ON tickets.company_id = comp.company_id');

    // Define sort fields
    $sortings = array (
        'ticket_id' => 'tickets.ticket_id',
        'author' => 'author_name',
        'recipient' => 'recipient_name',
        'last_message' => 'tickets.last_message',
        'company' => 'comp.company',
        'last_message_date' => 'tickets.last_message_timestamp',
        'amount' => 'tickets.amount',
    );

    $sorting = db_sort($params, $sortings, 'last_message_date', 'desc');

    $join = implode(' ', $join);
    $condition = implode(' ', $condition);
    $group = implode(' ', $group);

    $limit = '';

    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field(
            'SELECT COUNT(DISTINCT(tickets.ticket_id))
            FROM ?:messenger_tickets as tickets '
            . $join
            . ' WHERE 1 '
            . $condition
            . $group
        );

        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $tickets = db_get_array(
        'SELECT DISTINCT '
        . implode($fields, ', ')
        . ' FROM ?:messenger_tickets as tickets '
        . $join
        . ' WHERE 1 '
        . $condition
        . $sorting
        . $limit
    );

    fn_sd_messaging_system_check_tickets_status($tickets, AUTHOR_TYPE_CUSTOMER);
    fn_sd_messaging_system_check_tickets_users_name($tickets);

    return array($tickets, $params);
}

function fn_sd_messaging_system_convert_message_timestamp_to_date($timestamp)
{
    static $now = null;
    static $today = null;
    static $yesterday = null;

    $proceed = false;
    $time_string = '';
    $current_date = new DateTime();
    $current_date->setTimestamp($timestamp);

    if ($current_date === false) {
        return '';
    }

    if ($now === null) {
        $now = new DateTime('now');
    }

    if ($today === null) {
        $today = new DateTime();
        $today->setTime(0, 0, 0);
    }

    if ($yesterday === null) {
        $yesterday = new DateTime('-1day');
        $yesterday->setTime(0, 0, 0);
    }


    if ($today <= $current_date) {
        $dates_diff = $current_date->diff($now);
        $default_lang_diff = true;

        if ($dates_diff !== false) {
            if ($dates_diff->h > 0) {
                $time_string = __('hours_ago', array(abs($dates_diff->h)));
            } elseif ($dates_diff->i > 0) {
                $time_string = __('minutes_ago', array(abs($dates_diff->i)));
            } elseif ($dates_diff->s > 0) {
                $time_string = __('seconds_ago', array(abs($dates_diff->s)));
            }
        }
    } elseif ($current_date < $today && $yesterday <= $current_date) {
        $time_string = __('yesterday');
    } elseif ($today->format('Y') == $current_date->format('Y')) {
        $dates_diff = $current_date->diff($now);

        if ($dates_diff !== false) {
            $time_string = __('days_ago', array(abs($dates_diff->days)));
        }
    } elseif ($today->format('Y') > $current_date->format('Y')) {
        $years_diff = $today->format('Y') - $current_date->format('Y');
        $time_string = __('years_ago', array(abs($years_diff)));
    }

    return $time_string;
}

function fn_sd_messaging_system_is_user_exists($user_id = 0)
{
    $result = false;
    $company_id = fn_get_company_id('users', 'user_id', $user_id);

    if ($user_id) {
        $result = db_get_field(
            'SELECT user_id
            FROM ?:users
            WHERE user_id = ?i
            AND company_id = ?i',
            $user_id,
            $company_id
        );
    }

    return $result;
}

function fn_sd_messaging_system_is_company_exists($company_id)
{
    $company_name = db_get_field ('SELECT company FROM ?:companies WHERE company_id = ?i AND status = ?s', $company_id, 'A');

    return !empty($company_name);
}

function fn_sd_messaging_system_send_email_notification($template_code, $ticket_id)
{
    $result = false;
    if (!empty($ticket_id)) {
        $ticket = fn_sd_messaging_system_get_ticket_by_id($ticket_id);
        $area = AREA == 'A' ? 'C' : 'A';
        if ($area == 'C') {
            $user_id = $ticket['author_type'] == AUTHOR_TYPE_VENDOR ? $ticket['recipient_id'] : $ticket['author_id'];
            $company_id = $ticket['author_type'] == AUTHOR_TYPE_CUSTOMER ? $ticket['recipient_id'] : $ticket['author_id'];
            $user_data = fn_get_user_info($user_id);
            $company_data = fn_get_company_data($company_id);

            if (!empty($user_data)) {
                $target_user_email = $user_data['email'];
                $lang_code = !empty($user_data['lang_code']) ? $user_data['lang_code'] : CART_LANGUAGE;
                $attachment_url = '?dispatch=messenger.view&ticket_id=' . $ticket_id;
                $ticket_url = fn_sd_messaging_system_generate_url(Tygh::$app['session'], false, $attachment_url, true);
            }
        } else {
            $user_id = $ticket['author_type'] == AUTHOR_TYPE_VENDOR ? $ticket['recipient_id'] : $ticket['author_id'];
            $company_id = $ticket['author_type'] == AUTHOR_TYPE_CUSTOMER ? $ticket['recipient_id'] : $ticket['author_id'];
            $user_data = fn_get_user_info($user_id);
            $company_data = fn_get_company_data($company_id);

            if (!empty($company_data)) {
                $target_user_email = $company_data['email'];
                $lang_code = !empty($company_data['lang_code']) ? $company_data['lang_code'] : CART_LANGUAGE;
                $attachment_url = '?dispatch=messenger.view&ticket_id=' . $ticket_id;
                $ticket_url = fn_sd_messaging_system_generate_url(Tygh::$app['session'], true, $attachment_url);
            }
        }
    }

    $message_company_name = isset($company_data['company']) ? $company_data['company'] : '';
    $message_user_name = '';

    if (!empty($user_data['firstname'])) {
        $message_user_name .= $user_data['firstname'];
    }

    if (!empty($user_data['lastname'])) {
        $message_user_name .= !empty($user_data['firstname']) ? ' ' . $user_data['lastname'] : $user_data['lastname'];
    }

    if (!empty($template_code) && !empty($target_user_email)) {
        /** @var \Tygh\Mailer\Mailer $mailer */
        $mailer = Tygh::$app['mailer'];

        $mailer->send(array(
            'to' => $target_user_email,
            'from' => 'company_users_department',
            'data' => array (
                'ticket_url' => $ticket_url,
                'user_data' => $user_data,
                'message_company_name' => $message_company_name,
                'message_user_name' => $message_user_name
            ),
            'template_code' => $template_code,
            'tpl' => 'addons/sd_messaging_system/' . $template_code . '.tpl', // this parameter is obsolete and is used for back compatibility
        ), $area, $lang_code);
    }

    return $result;
}

function fn_sd_messaging_system_delete_tickets($ticket_ids = array())
{
    $result = false;

    if (is_array($ticket_ids) && !empty($ticket_ids)) {
        $company_id = Registry::get('runtime.company_id');

        if (AREA == 'A' && !empty($company_id)) {
            $ticket_ids = db_get_fields('SELECT ticket_id FROM ?:messenger_tickets WHERE ticket_id IN (?n) AND company_id = ?i', $ticket_ids, $company_id);
        }

        $result = db_query('DELETE FROM ?:messenger_tickets WHERE ticket_id IN (?n)', $ticket_ids);

        if ($result) {
            db_query('DELETE FROM ?:messenger_messages WHERE ticket_id IN (?n)', $ticket_ids);
        }
    }

    return $result;
}

function fn_sd_messaging_system_delete_messages($message_ids = array())
{
    $result = false;

    if (is_array($message_ids) && !empty($message_ids)) {
        $result = db_query('DELETE FROM ?:messenger_messages WHERE message_id IN (?n)', $message_ids);
    }

    return $result;
}

function fn_sd_messaging_system_get_new_messages($ticket_id = 0, $last_message_id)
{
    $messages = array();

    if ($ticket_id && is_numeric($last_message_id) && $last_message_id >= 0) {
        $messages = db_get_array(
            'SELECT message_id, ticket_id, author_id, message, author_type, timestamp, need_highlight
            FROM ?:messenger_messages
            WHERE ticket_id = ?i
            AND message_id > ?i
            ORDER BY message_id ASC',
            $ticket_id,
            $last_message_id
        );

        foreach ($messages as $key => $message) {
            $messages[$key]['last_message_id'] = $message['message_id'];
            if (AREA == 'A') {
                $messages[$key]['author_type'] = $message['author_type'] == AUTHOR_TYPE_VENDOR ? 'sender' : 'recipient';
            } else {
                $messages[$key]['author_type'] = $message['author_type'] == AUTHOR_TYPE_VENDOR ? 'recipient' : 'sender';
            }
        }
    }

    return $messages;
}

function fn_sd_messaging_system_change_ticket_status($ticket_id = 0, $status = '')
{
    if ($ticket_id && $status) {
        db_query('UPDATE ?:messenger_tickets SET ?u WHERE ticket_id = ?i', array('status' => $status), $ticket_id);

        $author_type = AREA == 'A' ? AUTHOR_TYPE_CUSTOMER : AUTHOR_TYPE_VENDOR;
        db_query('UPDATE ?:messenger_messages SET ?u WHERE ticket_id = ?i AND author_type = ?s', array('status' => $status), $ticket_id, $author_type);
    }
}

function fn_sd_messaging_system_get_messages_timestamps_and_dates($ticket_id = 0)
{
    $timestamps = array();

    if (!empty($ticket_id)) {
        $timestamps = db_get_fields(
            'SELECT timestamp
            FROM ?:messenger_messages
            WHERE ticket_id = ?i',
            $ticket_id
        );

        $timestamps = array_combine($timestamps, $timestamps);

        foreach ($timestamps as $timestamp => $value) {
            $timestamps[$timestamp] = fn_sd_messaging_system_convert_message_timestamp_to_date($timestamp);
        }
    }

    return $timestamps;
}

function fn_sd_messaging_system_delete_all_messages_in_ticket($ticket_id = 0)
{
    $result = false;
    $check_company_ticket = true;
    $company_id = Registry::get('runtime.company_id');

    if (AREA == 'A' && !empty($company_id)) {
        $check_company_ticket = fn_sd_messaging_system_check_company_ticket($ticket_id, $company_id);
    }

    if ($check_company_ticket && !empty($ticket_id)) {
        $result = db_query(
            'DELETE FROM ?:messenger_messages
            WHERE ticket_id = ?i',
            $ticket_id
        );
        if ($result) {
            $last_message_data = array(
                'last_message_author_id' => 0,
                'last_message_timestamp' => 0,
                'last_message' => '',
                'amount' => 0,
                'need_highlight' => 'N'
            );

            $result = db_query(
                'UPDATE ?:messenger_tickets
                SET ?u
                WHERE ticket_id = ?i',
                $last_message_data,
                $ticket_id
            );
        }
    }

    return $result;
}

function fn_sd_messaging_system_get_check_recipient_exists($auth, $company_id, $user_id)
{
    $error_msg = '';
    $recipient_company_exists = fn_sd_messaging_system_is_company_exists($company_id);

    if (!$recipient_company_exists) {
        $error_msg = __('company_does_not_exist');
    }

    if (AREA == 'C' && $auth['user_type'] == 'V' && $auth['company_id'] == $company_id) {
        $error_msg = __('cant_send_message_yourself_company');
    } elseif (AREA == 'A') {
        $recipient_use_exists = fn_sd_messaging_system_is_user_exists($user_id);
        $check_user_orders = fn_sd_messaging_system_get_user_orders($user_id, $company_id);

        if (!$recipient_use_exists) {
            $error_msg = __('user_does_not_exist');
        } elseif (!$check_user_orders) {
            $error_msg = __('customer_doesnt_by_anything_company');
        }
    }

    return empty($error_msg) ? false :  $error_msg;
}

function fn_sd_messaging_system_get_user_orders($user_id, $company_id)
{
    $result = false;
    if (!empty($user_id) && !empty($company_id)) {
        $result = db_get_array('SELECT order_id FROM ?:orders WHERE user_id = ?i AND company_id = ?i', $user_id, $company_id);
    }

    return $result;
}

function fn_sd_messaging_system_check_company_ticket($ticket_id, $company_id)
{
    $result = false;
    if (!empty($ticket_id) && !empty($company_id)) {
        $result = db_get_array('SELECT ticket_id FROM ?:messenger_tickets WHERE ticket_id = ?i AND company_id = ?i', $ticket_id, $company_id);
    }

    return $result;
}

function fn_sd_messaging_system_replace_ticket_id(&$request)
{
    if (!empty($request['redirect_url']) && !isset($request['ticket_id'])) {
        $array_redirect_url = explode('&', $request['redirect_url']);
        foreach ($array_redirect_url as $redirect_url) {
            $check_string = strpos($redirect_url, 'ticket_id=');
            if ($check_string === 0) {
                $result = str_replace('ticket_id=', '', $redirect_url);
                break;
            }
        }
        $request['ticket_id'] = isset($result) ? $result : 0;
    }
}

function fn_sd_messaging_system_generate_url(&$session, $vendor = false, $attachment_url = false, $ticket_storefront = false)
{
    $config = Registry::get('config');
    $security = Registry::get('settings.Security');
    $vendor_url = $customer_url = '';

    if (!empty($session['new_ticket_order_id'])) {
        $attachment_url = '?dispatch=orders.details&order_id=' . $session['new_ticket_order_id'];
        unset($session['new_ticket_order_id']);
    }

    if (!empty($attachment_url)) {
        if ($vendor) {
            $vendor_url = $security['secure_admin'] == 'Y' ? $config['https_location'] . '/' . $config['vendor_index'] . $attachment_url : $config['http_location'] . '/' . $config['vendor_index'] . $attachment_url;
        } elseif ($ticket_storefront) {
            $customer_url = $security['secure_storefront'] == 'full' ? $config['https_location'] . '/' . $config['customer_index'] . $attachment_url : $config['http_location'] . '/' . $config['customer_index'] . $attachment_url;
        } else {
            $customer_url = $security['secure_storefront'] != 'none' ? $config['https_location'] . '/' . $config['customer_index'] . $attachment_url : $config['http_location'] . '/' . $config['customer_index'] . $attachment_url;
        }
    }

    return ($vendor) ? $vendor_url : $customer_url;
}

function fn_sd_messaging_system_generate_customet_attachment_message(&$session)
{
    $attachement_message = '';
    if (!empty($session['new_ticket_product_id'])) {
        $attachement_message = fn_url('products.view&product_id=' . $session['new_ticket_product_id']);
        unset($session['new_ticket_product_id']);
    } elseif (!empty($session['new_ticket_order_id'])) {
        $attachement_message = fn_url('orders.details&order_id=' . $session['new_ticket_order_id']);
        unset($session['new_ticket_order_id']);
    }

    return $attachement_message;
}

function fn_sd_messaging_system_check_messenger_permission($auth)
{
    if (empty($auth['user_id'])) {
        return false;
    }

    if (AREA == 'C' && $auth['user_type'] == 'C') {
        return true;
    }

    $permission = false;
    
    if ($auth['is_root'] == 'Y') {
        $permission = true;

    } else {
        $messenger_permission = db_get_field('SELECT messenger_permission FROM ?:users WHERE user_id = ?i', $auth['user_id']);
        $permission = $messenger_permission == 'Y';
    }

    if ($permission && !empty($auth['company_id'])) {
        $permission = fn_check_company_messenger_access($auth['company_id']);
    }

    return $permission;
}

function fn_sd_messaging_system_check_tickets_users_name(&$tickets)
{
    if (!empty($tickets)) {
        foreach ($tickets as &$ticket) {
            $replaced_author_name = str_replace(' ', '', $ticket['author_name']);
            $replaced_recipient_name = str_replace(' ', '', $ticket['recipient_name']);
            $ticket['author_name'] = empty($replaced_author_name) ? '-' : $ticket['author_name'];
            $ticket['recipient_name'] = empty($replaced_recipient_name) ? '-' : $ticket['recipient_name'];
        }
    }
}

function fn_sd_messaging_system_get_autocheck_messages_time()
{
    $time = Registry::get('addons.sd_messaging_system.autocheck_messages_time') * 1000;

    return $time;
}

function fn_messenger_status_to_permission_map($status, $permission_to_status = false)
{
    $status_permission = array(
        'A' => 'Y',
        'D' => 'N',
    );

    if ($permission_to_status) {
        $status_permission = array_flip($status_permission);
    }

    return (!empty($status_permission[$status])) ? $status_permission[$status] : '';
}

function fn_update_vendors_messenger_permission_by_plan($plan_id, $messenger, $company_id = 0, $messenger_flag = true)
{
    if (!empty($plan_id) && !empty($messenger)) {

        $condition = '';

        if ($messenger_flag) {
            $condition .= db_quote('AND messenger_flag != ?s ', MESSENGER_DISABLED_FOR_VENDOR_LETTER);
        }

        if (!empty($company_id)) {
            $condition .= db_quote('AND company_id = ?i ', $company_id);
        }

        $company_vendor_email_rel = db_get_hash_single_array('SELECT company_id, email '
                                                           . 'FROM ?:companies '
                                                           . 'WHERE plan_id = ?i '
                                                           . $condition
                                                           , array('company_id', 'email')
                                                           , $plan_id
                                                            );

        $plan_company_ids = array_keys($company_vendor_email_rel);

        $messenger_status = fn_messenger_status_to_permission_map($messenger, true);

        if (!empty($plan_company_ids)) {

            // Update vendor/company messenger state
            db_query('UPDATE ?:companies SET messenger = ?s '
                   . 'WHERE company_id IN (?n)'
                   , $messenger_status, $plan_company_ids);

            // Update messenger_permission for vendor users
            db_query('UPDATE ?:users SET messenger_permission = ?s '
                   . 'WHERE company_id IN (?n) '
                   . "AND user_type = 'V' "
                   , $messenger, $plan_company_ids);
        }
    }
}

function fn_sd_messaging_system_install()
{
    if (db_get_fields("SHOW TABLES LIKE '?:vendor_plans'")) {
        db_query("ALTER TABLE ?:vendor_plans ADD messenger CHAR(1) NOT NULL DEFAULT 'Y'");
    }
}

function fn_sd_messaging_system_uninstall()
{
    if (db_get_fields("SHOW TABLES LIKE '?:vendor_plans'")) {
        db_query('ALTER TABLE ?:vendor_plans DROP messenger');
    }
}

function fn_check_company_messenger_access($company_id)
{
    $result = '';

    if (AREA == 'A' || Registry::get('addons.vendor_plans.status') != 'A') {
        $result = 'A';

    } elseif (!empty($company_id)) {
        $result = db_get_field('SELECT messenger FROM ?:companies WHERE company_id = ?i', $company_id);
    }

    return $result == 'A';
}

function fn_sd_messaging_system_check_send_message_premission($ticket_id)
{
    $result = false;

    if (!empty($ticket_id)) {
        $company_id = db_get_field('SELECT a.company_id FROM ?:messenger_tickets AS a LEFT JOIN ?:companies AS b ON b.company_id = a.company_id WHERE a.ticket_id = ?i AND b.status = ?s', $ticket_id, 'A');
        $result = empty($company_id) ? false : fn_check_company_messenger_access($company_id);
    }

    return $result;
}

/**************
*****HOOKS*****
**************/

function fn_sd_messaging_system_update_user_pre($user_id, &$user_data, $auth, $ship_to_another, $notify_user)
{
    if ($auth['is_root'] != 'Y') {
        unset($user_data['messenger_permission']);
    }
}

function fn_sd_messaging_system_update_company($company_data, $company_id, $lang_code, $action)
{
    if (!empty($company_id) && Registry::get('addons.vendor_plans.status') == 'A') {

        if (!empty($company_data['plan_id'])) {
            $plan_id = $company_data['plan_id'];
            $messenger = db_get_field('SELECT messenger FROM ?:vendor_plans WHERE plan_id = ?i', $plan_id);
        }

        $vendor_messenger_data = db_get_row('SELECT email, messenger FROM ?:companies WHERE company_id = ?i', $company_id);
        $messenger_permission = fn_messenger_status_to_permission_map($vendor_messenger_data['messenger']);

        if (Registry::get('runtime.updated_section') != 'plan') {

            if (!empty($messenger_permission) 
                && $messenger == MESSENGER_DISABLED_FOR_VENDOR_LETTER 
                && $messenger_permission != MESSENGER_DISABLED_FOR_VENDOR_LETTER
               ) {

                $disabled_messenger_status = fn_messenger_status_to_permission_map(MESSENGER_DISABLED_FOR_VENDOR_LETTER, true);
                db_query('UPDATE ?:companies SET messenger = ?s WHERE company_id = ?i', $disabled_messenger_status, $company_id);

                fn_set_notification('E', __('error'), __('messenger_system.messenger_disabled_by_company_plan'));

            } elseif (Registry::get('runtime.updated_section') == 'addons' && !empty($messenger_permission)) {

                // Update messenger_permission for vendor users
                db_query('UPDATE ?:users SET messenger_permission = ?s '
                       . 'WHERE company_id = ?i '
                       . "AND user_type = 'V' "
                       , $messenger_permission, $company_id);

                db_query('UPDATE ?:companies SET messenger_flag = ?s WHERE company_id = ?s', $messenger_permission, $company_id);
            }
        } elseif (!empty($company_data['plan_id'])) {
            fn_update_vendors_messenger_permission_by_plan($plan_id, $messenger, $company_id, false);
        }

        if ($action == 'add' && !empty($company_data['plan_id'])) {
            fn_update_vendors_messenger_permission_by_plan($plan_id, $messenger, $company_id);
        }
    }
}

function fn_sd_messaging_system_vendor_plan_after_save($obj_state)
{
    $plan_id = $obj_state->__get('plan_id');
    $messenger = $obj_state->__get('messenger');

    fn_update_vendors_messenger_permission_by_plan($plan_id, $messenger, 0, false);
}

/**
 * Updates forbidden words
 *
 * @param array $data Array with forbidden words
 *
 */
function fn_sd_messaging_system_update_forbidden_words($data)
{
    if (!empty($data)) {
        db_query('DELETE FROM ?:messenger_forbidden_words');
        $forbidden_words = array_filter($data);
        if (!empty($forbidden_words)) {
            $update_data = array();
            foreach ($forbidden_words as $word) {
                if (!empty($word)) {
                    $update_data[] = array(
                        'word' => $word
                    );
                }
            }
            if (!empty($update_data)) {
                db_query('REPLACE INTO ?:messenger_forbidden_words ?m', $update_data);
            }
        } else {
            fn_set_notification('W', __('warning'), __('addons.fn_sd_messaging_system.no_words_specified'));
        }
    }
}

/**
 * Gets forbidden words
 *
 * @return array $data Array with forbidden words
 *
 */
function fn_sd_messaging_system_get_forbidden_words()
{
    return db_get_hash_single_array('SELECT * FROM ?:messenger_forbidden_words', array('word_id', 'word'));
}

/**
 * Replaces forbidden words with stars
 *
 * @param string $message Message text
 *
 * @return bool $need_highlight Determines if the message should be highlighted
 */
function fn_sd_messaging_system_replace_forbidden_words(&$message)
{
    $need_highlight = false;
    $total_replacement = 0;
    if (!empty($message)) {
        $forbidden_words = fn_sd_messaging_system_get_forbidden_words();
        if (!empty($forbidden_words)) {
            foreach ($forbidden_words as $word) {
                $replacement_number = 0;
                $message = str_replace($word, FORBIDDEN_WORD_REPLACE_STRING, $message, $replacement_number);
                $total_replacement += $replacement_number;
            }
        }
    }
    if ($total_replacement > 0) {
        $need_highlight = true;
    }
    return $need_highlight;
}

/**
 * Get not viewed messages in ticket
 *
 * @param int    $ticket_id    Ticket id
 *
 * @return int
 */
function fn_sd_messaging_system_get_messages_amount($user_id, $ticket_id = 0)
{
    $result = 0;
    $author_type = AREA == 'C' ? AUTHOR_TYPE_VENDOR : AUTHOR_TYPE_CUSTOMER;

    if (!empty($ticket_id)) {
        $result = db_get_field('SELECT COUNT(*) FROM ?:messenger_messages WHERE ticket_id = ?i AND author_type = ?s AND status = ?s', $ticket_id, $author_type, TICKET_STATUS_NEW);
    } elseif (!empty($user_id)) {
        $ticket_list = fn_sd_messaging_system_get_user_tickets($user_id);
        $ticket_ids = array_column($ticket_list, 'ticket_id');

        if (!empty($ticket_ids)) {
            $result = db_get_field('SELECT COUNT(*) FROM ?:messenger_messages WHERE author_type = ?s AND status = ?s AND ticket_id IN (?n)', $author_type, TICKET_STATUS_NEW, $ticket_ids);
        }
    }

    return $result;
}

/**
 * Get not viewed messages in tickets from cache
 *
 * @param int    $ticket_id    Ticket id
 *
 * @return int
 */
function fn_sd_messaging_system_get_cache_messages_amount()
{
    $user_id = Tygh::$app['session']['auth']['user_id'];
    $new_messages_amount = 0;

    if (!empty($user_id)) {
        Registry::registerCache(
            'new_messages_amount_cache_static' . $user_id,
            array('messenger_messages'),
            Registry::cacheLevel('static'),
            true
        );

        if ($cache = Registry::get('new_messages_amount_cache_static' . $user_id)) {
            $new_messages_amount = $cache;
        } else {
            $new_messages_amount = fn_sd_messaging_system_get_messages_amount($user_id);
            Registry::set('new_messages_amount_cache_static' . $user_id, $new_messages_amount);
        }
    }

    return $new_messages_amount;
}

/**
 * Match and replace url text to url link in message.
 *
 * @param string    $message    Message text
 *
 * @return string
 */
function fn_sd_messaging_system_replace_url_message($message)
{
    if (!empty($message)) {
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $message, $match);
        $url_list = reset($match);

        foreach ($url_list as $url) {
            $message = str_replace($url, "<a href='{$url}' target=_blank>{$url}</a>", $message);
        }
    }

    return $message;
}
