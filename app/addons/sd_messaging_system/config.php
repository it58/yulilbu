<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_define('TICKET_STATUS_VIEWED', 'V');
fn_define('TICKET_STATUS_NEW', 'N');

fn_define('MESSAGE_STATUS_NEW', 'N');
fn_define('MESSAGE_STATUS_HIDDEN', 'H');

fn_define('MESSAGE_TODAY_DATE_FORMAT', 'H:i');
fn_define('MESSAGE_SAME_YEAR_DATE_FORMAT', 'm/d');
fn_define('MESSAGE_LAST_YEAR_DATE_FORMAT', 'Y/m/d');

fn_define('USER_IMAGE_WIDTH', '42');
fn_define('USER_IMAGE_HEIGHT', '42');

fn_define('AUTHOR_TYPE_VENDOR', 'V');
fn_define('AUTHOR_TYPE_CUSTOMER', 'C');

fn_define('TRIPLE_DOT_LENGHT', '3');

fn_define('MESSENGER_DISABLED_FOR_VENDOR_LETTER', 'N');

fn_define('FORBIDDEN_WORD_REPLACE_STRING', '***');
