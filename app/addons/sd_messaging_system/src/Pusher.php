<?php

namespace MyApp;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

class Pusher implements WampServerInterface {
    /**
     * A lookup of all the topics clients have subscribed to
     */
    protected $subscribedTickets = array();

    public function onSubscribe(ConnectionInterface $conn, $topic) {
        $this->subscribedTickets[$topic->getId()] = $topic;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onTicketEntry($entry) {
        $entryData = json_decode($entry, true);

        // If the lookup topic object isn't set there is no one to publish to
        if (!array_key_exists($entryData['ticket_id'], $this->subscribedTickets)) {
            return;
        }

        $topic = $this->subscribedTickets[$entryData['ticket_id']];

        // re-send the data to all the clients subscribed to that ticket
        $topic->broadcast($entryData);
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
        $this->onLog("New subscribe ({$conn->resourceId})", 'INFO');
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->onLog("New connection ({$conn->resourceId})", 'INFO');
    }

    public function onClose(ConnectionInterface $conn) {
        $this->onLog("Close connection ({$conn->resourceId})", 'INFO');
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $this->onLog("Call hacked around in console ({$id}, {$topic})", 'WARN');
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $this->onLog("Publish hacked around in console ({$event}, {$topic})\n", 'WARN');
        $conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $this->onLog('Error ticket connection: ' . $e->getMessage(), 'WARN');
    }

    public function onLog($message, $type = 'WARN') {
        if ($message) {
            $time = date('D M d H:i:s Y');
            $logText = "[$time]" . " [$type] " . "$message" . "\n";

            switch ($type) {
            case 'ERROR':
                fwrite(STDERR, $logText);
                break;
            case 'WARN':
            case 'INFO':
                fwrite(STDOUT, $logText);
            }

        }
    }
}
