<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

$session = & Tygh::$app['session'];

if (!fn_sd_messaging_system_check_messenger_permission($session['auth'])) {
    return array(CONTROLLER_STATUS_DENIED);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'delete_ticket') {
        if (!empty($_REQUEST['ticket_id'])) {
            if (fn_sd_messaging_system_delete_tickets(array($_REQUEST['ticket_id']))) {
                fn_set_notification('N', __('notice'), __('ticket_successfully_removed'));
            } else {
                fn_set_notification('E', __('error'), __('error_occurred'));
            }
        }

        return array(CONTROLLER_STATUS_REDIRECT, 'messenger.tickets_list');
    }

    if ($mode == 'm_delete_tickets') {
        if (!empty($_REQUEST['ticket_ids'])) {
            if(fn_sd_messaging_system_delete_tickets($_REQUEST['ticket_ids'])) {
                fn_set_notification('N', __('notice'), __('tickets_successfully_removed'));
            } else {
                fn_set_notification('E', __('error'), __('error_occurred'));
            }
        }

        return array(CONTROLLER_STATUS_REDIRECT, 'messenger.tickets_list');
    }

    if ($mode == 'update_forbidden_words') {
        if (!empty($_REQUEST['forbidden_words'])) {
            fn_sd_messaging_system_update_forbidden_words($_REQUEST['forbidden_words']);
        }
        return array(CONTROLLER_STATUS_REDIRECT, 'messenger.forbidden_words');
    }

    return array(CONTROLLER_STATUS_OK, 'messenger.tickets_list');
}

if ($mode == 'tickets_list') {
    list($tickets, $search) = fn_sd_messaging_system_get_all_tickets($_REQUEST);
    foreach ($tickets as $key => $ticket_data) {
        $tickets[$key]['last_message_date'] = '';

        if (!empty($ticket_data['last_message_timestamp'])) {
            $tickets[$key]['last_message_date'] = fn_sd_messaging_system_convert_ticket_timestamp_to_date($ticket_data['last_message_timestamp']);
        }
    }

    Tygh::$app['view']->assign(array(
        'tickets' => $tickets,
        'search' => $search,
    ));

} elseif ($mode == 'view') {
    if (!empty($_REQUEST['ticket_id']) || !empty($action)) {
        fn_sd_messaging_system_replace_ticket_id($_REQUEST);
        $ticket = fn_sd_messaging_system_get_ticket_by_id($_REQUEST['ticket_id']);

        if ($ticket) {
            $ticket = fn_sd_messaging_system_fetch_tickets_data(array($ticket), true);
            $ticket = reset($ticket);

            list($messages, $search) = fn_sd_messaging_system_get_ticket_messages_with_sorting($ticket['ticket_id'], $_REQUEST);

            if (!empty($ticket['status'])
                && $ticket['status'] == TICKET_STATUS_NEW
                && !empty($ticket['last_message_author_type'])
                && $ticket['last_message_author_type'] != AUTHOR_TYPE_VENDOR
            ) {
                fn_sd_messaging_system_change_ticket_status($ticket['ticket_id'], TICKET_STATUS_VIEWED);
            }

            Tygh::$app['view']->assign(array(
                'ticket' => $ticket,
                'messages' => $messages,
                'search' => $search,
                'attachment_message' => fn_sd_messaging_system_generate_url($session),
            ));

        } elseif (!$ticket && empty($action)) {
            fn_set_notification('E', __('error'), __('ticket_does_not_exist'));
            return array(CONTROLLER_STATUS_REDIRECT, 'messenger.tickets_list');
        }
    }

} elseif ($mode == 'delete_all_messages') {
    if (!empty($_REQUEST['ticket_id'])) {
        $result = fn_sd_messaging_system_delete_all_messages_in_ticket($_REQUEST['ticket_id']);

        if ($result) {
            fn_set_notification('N', __('notice'), __('messages_successfully_removed'));
        } else {
            fn_set_notification('E', __('error'), __('error_occurred'));
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'messenger.tickets_list');

} elseif ($mode == 'forbidden_words') {
    Tygh::$app['view']->assign('forbidden_words', fn_sd_messaging_system_get_forbidden_words());
}