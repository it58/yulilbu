<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

if (empty($auth['user_id'])) {
    fn_set_notification('E', __('error'), __('guest_cannot_start_conversation'));
    return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
}

if (!fn_sd_messaging_system_check_messenger_permission($auth)) {
    return array(CONTROLLER_STATUS_DENIED);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return array(CONTROLLER_STATUS_OK);
}

if ($mode == 'tickets_list') {
    if (!empty($auth['user_id'])) {
        $tickets = fn_sd_messaging_system_get_user_tickets($auth['user_id']);
        $tickets = fn_sd_messaging_system_fetch_tickets_data($tickets, true);
        fn_add_breadcrumb(__('messages'));

        Tygh::$app['view']->assign('tickets', $tickets);
    } else {
        return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
    }

} elseif ($mode == 'view') {
    $error = '';

    if (!empty($_REQUEST['ticket_id'])) {
        $ticket = fn_sd_messaging_system_get_ticket_by_id($_REQUEST['ticket_id']);

        if ($ticket) {
            if ($auth['area'] == 'C'
                && $auth['user_id'] != $ticket['author_id']
                && $auth['user_id'] != $ticket['recipient_id']
            ) {
                $error = __('you_have_no_permission_to_view_this_ticket');
            } else {
                $ticket = fn_sd_messaging_system_fetch_tickets_data(array($ticket), true);
                $ticket = reset($ticket);

                fn_add_breadcrumb(__('messages'), 'messenger.tickets_list');

                $messages = fn_sd_messaging_system_get_ticket_messages($ticket['ticket_id']);

                if (!empty($ticket['status'])
                    && $ticket['status'] == TICKET_STATUS_NEW
                    && !empty($ticket['last_message_author_type'])
                    && $ticket['last_message_author_type'] != AUTHOR_TYPE_CUSTOMER
                ) {
                    fn_sd_messaging_system_change_ticket_status($ticket['ticket_id'], TICKET_STATUS_VIEWED);
                }

                Tygh::$app['view']->assign(array(
                    'ticket' => $ticket,
                    'messages' => $messages,
                    'attachment_message' => fn_sd_messaging_system_generate_customet_attachment_message($_SESSION),
                    'new_messages_amount' => fn_sd_messaging_system_get_messages_amount($auth['user_id'])
                ));
            }
        } else {
            $error = __('ticket_does_not_exist');
        }

    } else {
        $error = __('no_ticket_id_specified');
    }

    if ($error) {
        fn_set_notification('E', __('error'), $error);
        return array(CONTROLLER_STATUS_REDIRECT, 'messenger.tickets_list');
    }
}
