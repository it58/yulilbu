<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

if (empty($auth['user_id'])) {
    if (AREA == 'C') {
        fn_set_notification('E', __('error'), __('guest_cannot_start_conversation'));
    }
    return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
}

if (AREA == 'C' && !empty($_REQUEST['ticket_id']) && !fn_sd_messaging_system_check_send_message_premission($_REQUEST['ticket_id'])) {
    $hide_send_message = true;
    Tygh::$app['view']->assign('hide_send_message', $hide_send_message);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $ticket_id = (empty($_REQUEST['ticket_id'])) ? 0 : $_REQUEST['ticket_id'];

    $check_ticket = fn_sd_messaging_system_get_ticket_by_id($ticket_id);
    if (empty($check_ticket)) {
        exit;
    }

    if ($mode == 'send_message') {
        if (defined('AJAX_REQUEST')) {

            if (!empty($hide_send_message)) {
                fn_set_notification('E', __('error'), __('vendor_cannot_receive_messages'));
                exit;
            }

            $message_body = trim($_REQUEST['message_body']);

            if (!empty($message_body) || is_numeric($message_body)) {
                if (!empty($ticket_id)) {
                    $last_message_id = fn_sd_messaging_system_save_new_message($ticket_id, $_REQUEST['author_id'], $message_body);

                    if ($last_message_id) {
                        $pusher_port = Registry::get('addons.sd_messaging_system.pusher_port');
                        $need_highlight = fn_sd_messaging_system_replace_forbidden_words($message_body);

                        if (!empty($pusher_port)) {
                            $context = new ZMQContext();
                            $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'messenger');
                            $socket->connect('tcp://localhost:' . $pusher_port);

                            $entry_data = array(
                                'message' => fn_sd_messaging_system_replace_url_message($message_body),
                                'ticket_id' => $ticket_id,
                                'last_message_id' => $last_message_id,
                                'from_area' => AREA,
                                'need_highlight' => $need_highlight
                            );

                            $socket->send(json_encode($entry_data));
                        }

                        fn_sd_messaging_system_send_email_notification('sd_messaging_system_new_message_notification', $ticket_id);
                    }
                }
            } else {
                fn_set_notification('E', __('error'), __('message_cannot_be_empty'));
            }

            exit;
        }
    }

    if ($mode == 'get_new_messages') {
        if (defined('AJAX_REQUEST')) {
            if (!empty($ticket_id) && isset($_REQUEST['last_message_id'])) {
                $new_messages = fn_sd_messaging_system_get_new_messages($ticket_id, $_REQUEST['last_message_id']);

                if (!empty($new_messages)) {
                    Registry::get('ajax')->assign('new_messages', $new_messages);
                }
            }

            exit;
        }
    }

    if ($mode == 'change_status') {
        if (defined('AJAX_REQUEST')) {
            if (!empty($_REQUEST['ticket_id'])) {
                fn_sd_messaging_system_change_ticket_status($_REQUEST['ticket_id'], TICKET_STATUS_VIEWED);
            }

            exit;
        }
    }

    return array(CONTROLLER_STATUS_OK);
}

if ($mode == 'create_ticket') {

    if (!empty($hide_send_message)) {
        fn_set_notification('E', __('error'), __('vendor_cannot_receive_messages'));
        exit;
    }

    if (empty($auth['user_id']) && empty($_REQUEST['user_id']) && $auth['user_id'] != $_REQUEST['user_id']) {
        return array(CONTROLLER_STATUS_DENIED);
    }

    if (Registry::get('addons.vendor_plans.status') == 'A' && !empty($_REQUEST['company_id']) && !fn_check_company_messenger_access($_REQUEST['company_id'])) {
        return array(CONTROLLER_STATUS_DENIED);
    }

    if (!empty($_REQUEST['company_id'])) {
        $user_id = (AREA == 'A' & !empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : $auth['user_id'];

        if (!empty($user_id)) {
            $error_msg = fn_sd_messaging_system_get_check_recipient_exists($auth, $_REQUEST['company_id'], $user_id);

            if (!$error_msg) {
                $ticket_id = fn_sd_messaging_system_get_ticket_id($user_id, $_REQUEST['company_id']);

                if (!$ticket_id) {
                    $ticket_id = fn_sd_messaging_system_create_new_ticket($user_id, $_REQUEST['company_id']);

                    if ($ticket_id) {
                        fn_sd_messaging_system_send_email_notification('sd_messaging_system_new_ticket_notification', $ticket_id);
                    }
                }

                if ($ticket_id) {
                    if (!empty($_REQUEST['product_id'])) {
                        Tygh::$app['session']['new_ticket_product_id'] = $_REQUEST['product_id'];
                    } elseif (!empty($_REQUEST['order_id'])) {
                        Tygh::$app['session']['new_ticket_order_id'] = $_REQUEST['order_id'];
                    }

                    return array(CONTROLLER_STATUS_REDIRECT, 'messenger.view&ticket_id=' . $ticket_id);
                }
            } else {
                fn_set_notification('E', __('error'), $error_msg);
            }
        } else {
            fn_set_notification('E', __('error'), __('guest_cannot_start_conversation'));
            return array(CONTROLLER_STATUS_REDIRECT, 'auth.login_form?return_url=' . urlencode(Registry::get('config.current_url')));
        }
    } else {
         fn_set_notification('E', __('error'), __('recipient_company_id_not_specified'));
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'messenger.tickets_list');
}