
DROP TABLE IF EXISTS cscart_messenger_forbidden_words;
CREATE TABLE `cscart_messenger_forbidden_words` (
  `word_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`word_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS cscart_messenger_messages;
CREATE TABLE `cscart_messenger_messages` (
  `message_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author_id` mediumint(8) NOT NULL DEFAULT '0',
  `message` text,
  `timestamp` int(11) DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'N',
  `author_type` char(1) NOT NULL DEFAULT '',
  `need_highlight` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`message_id`,`author_id`,`status`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('1', '1', '3', 'Hello', '1514205653', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('2', '1', '10', 'Hello. How can I help you?', '1514205669', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('3', '1', '3', 'I\'m looking at this item https://real-time-messenger.demo.simtechdev.com/baby-and-children/feeding-diapering-and-nursing/baby-pacifiers/philips-avent-classic-soothers-scf170-22/', '1514205685', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('4', '1', '3', 'Will it be back in stock?', '1514205690', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('5', '1', '10', 'Unfortunately, we are not planning to restock this item.', '1514205698', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('6', '1', '3', 'That\'s bad. I\'m looking for a present for my nephew\'s baby shower. Could you offer me something suitable for a baby present?', '1514205705', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('7', '1', '10', 'Sure. How about this set of pacifiers https://real-time-messenger.demo.simtechdev.com/baby-and-children/feeding-diapering-and-nursing/baby-pacifiers/philips-avent-classic-pacifiers-scf182-24/', '1514205715', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('8', '1', '3', 'Just a sec, I\'ll check it out.', '1514205721', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('9', '1', '10', 'Sure. Take your time.', '1514205732', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('10', '1', '3', 'I like it. Is there anything else?', '1514205738', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('11', '1', '10', 'We also have baby bottles. Please check this one out: https://real-time-messenger.demo.simtechdev.com/baby-and-children/feeding-diapering-and-nursing/feeding-bottles/philips-avent-baby-bottle-scf680-17/', '1514205760', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('12', '1', '3', 'It looks nice. Just what I wanted.', '1514205767', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('13', '1', '10', 'Great.', '1514205772', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('14', '1', '3', 'I\'ll buy it. Thanks a lot!', '1514205779', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('15', '1', '10', 'You are welcome. Get in touch with us if you need something else.', '1514205788', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('16', '1', '3', 'I will. Bye.', '1514205795', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('17', '1', '10', 'Bye.', '1514205803', 'N', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('18', '2', '3', 'Hello. I\'m interested in this vacuum cleaner
\nhttps://real-time-messenger.demo.simtechdev.com/home/domestic-appliances/floor-care/vacuum-cleaners/electrolux-zg8800-vacuum-cleaner/', '1514205844', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('19', '2', '9', 'Hello. I\'m Nick. I will help you with your shopping. Do you have any questions about this item?', '1514205867', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('20', '2', '3', 'Well I\'m wondering if you offer any discounts now.', '1514205897', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('21', '2', '9', 'Unfortunately, we don\'t have any promotions currently.', '1514205903', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('22', '2', '3', 'I see.', '1514205909', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('23', '2', '9', 'Can I interest you in something similar, but with a lower price?', '1514205915', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('24', '2', '3', 'I\'m open to suggestions.', '1514205920', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('25', '2', '9', 'We have a similar Electrolux Z8860C vacuum cleaner. It costs $31.16. Please check it here: https://real-time-messenger.demo.simtechdev.com/home/domestic-appliances/floor-care/vacuum-cleaners/electrolux-z8860c-vacuum-cleaner/', '1514205928', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('26', '2', '3', 'OK. Let me check it.', '1514205933', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('27', '2', '9', 'Take your time.', '1514205944', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('28', '2', '3', 'It looks good. Does it have the same features?', '1514205951', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('29', '2', '9', 'It\'s almost the same. It\'s input power is a bit lower, but it will be fine for home cleaning.', '1514205964', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('30', '2', '3', 'Alright. I\'ll consider this one. I\'ll get back to you soon.', '1514205971', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('31', '2', '9', 'Sure. Message me if you need any help.', '1514205976', 'V', 'V', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('32', '2', '3', 'OK. Thanks for your help.', '1514205982', 'V', 'C', 'N');
INSERT INTO cscart_messenger_messages (`message_id`, `ticket_id`, `author_id`, `message`, `timestamp`, `status`, `author_type`, `need_highlight`) VALUES ('33', '2', '9', 'My pleasure.', '1514205989', 'V', 'V', 'N');

DROP TABLE IF EXISTS cscart_messenger_tickets;
CREATE TABLE `cscart_messenger_tickets` (
  `ticket_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` mediumint(8) NOT NULL DEFAULT '0',
  `recipient_id` mediumint(8) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'V',
  `author_type` char(1) NOT NULL DEFAULT '',
  `last_message_author_id` mediumint(8) NOT NULL DEFAULT '0',
  `last_message_timestamp` int(11) DEFAULT '0',
  `last_message` varchar(255) NOT NULL DEFAULT '',
  `last_message_author_type` char(1) NOT NULL DEFAULT '',
  `amount` int(11) NOT NULL DEFAULT '0',
  `need_highlight` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ticket_id`),
  UNIQUE KEY `author_id` (`author_id`,`recipient_id`,`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO cscart_messenger_tickets (`ticket_id`, `author_id`, `recipient_id`, `company_id`, `timestamp`, `status`, `author_type`, `last_message_author_id`, `last_message_timestamp`, `last_message`, `last_message_author_type`, `amount`, `need_highlight`) VALUES ('1', '3', '10', '10', '1514205627', 'N', 'C', '10', '1514205803', 'Bye.', 'V', '17', 'N');
INSERT INTO cscart_messenger_tickets (`ticket_id`, `author_id`, `recipient_id`, `company_id`, `timestamp`, `status`, `author_type`, `last_message_author_id`, `last_message_timestamp`, `last_message`, `last_message_author_type`, `amount`, `need_highlight`) VALUES ('2', '3', '9', '9', '1514205838', 'V', 'C', '9', '1514205989', 'My pleasure.', 'V', '16', 'N');
