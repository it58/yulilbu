<?php
/******************************************************************
# Packing Slip Changes - Packing Slip Changes                     *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/ 

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
      'update_currency_pre'
);