<?php
/************************************************************************
# wk_advanced_currency_exchange  ---  wk_advanced_currency_exchange     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/
use Tygh\Registry;
use Tygh\Http;

if ( !defined('AREA') ) { die('Access denied'); }

function fn_wk_advanced_currency_exchange_update_currency_pre(&$currency_data, $currency_id ,$lang_code) {
	if(!empty($currency_id)) { 
	    if(isset($currency_data['update_current_exchange_rate']) && $currency_data['update_current_exchange_rate'] == 'Y') {
	   		$currency_data['coefficient'] = $currency_data['current_exchange_rate'];
	   		if($currency_data['modifier_type'] == 'A')
		      {
		      	    if($currency_data['modifier_sign'] == 'A') {
						$currency_data['coefficient']+=$currency_data['modifier_rate'];
					} else {
						$currency_data['coefficient']-=$currency_data['modifier_rate'];
					}
		      }
		      elseif ($currency_data['modifier_type'] == 'P') {
		      	   $get_percentage_amount = ($currency_data['modifier_rate']*$currency_data['coefficient'])/100;
		      
		      	    if($currency_data['modifier_sign'] == 'A') {
						$currency_data['coefficient']+=$get_percentage_amount;
					} else {
						$currency_data['coefficient']-=$get_percentage_amount;
					}
		      }
	    }
		if(isset($currency_data['auto_update'])) {
			$currency_data['auto_update'] = $currency_data['auto_update'];
		} else {
			$currency_data['auto_update'] = 'N';
		}
	    $currency_data['next_update']= TIME + (Registry::get('addons.wk_advanced_currency_exchange.update_time')*60);
	}    
}

// fn_adv_curr_get_live_rate("GBP","USD");
function fn_adv_curr_get_live_rate($from_Currency,$to_Currency) {
	if($from_Currency == $to_Currency) {
        return 1.00;
	}
	$exchange_rate = Registry::get('addons.wk_advanced_currency_exchange.exchange_rate');
	if($exchange_rate == 'free_converter') {
		$data = Http::get("https://free.currencyconverterapi.com/api/v5/convert?compact=y&q=".$from_Currency.'_'.$to_Currency);
		$data = json_decode($data);
		$formate = $from_Currency.'_'.$to_Currency;
		if(!isset($data->error) && isset($data->$formate) && $data->$formate){
			return $data->$formate->val;
		}
	} elseif($exchange_rate == 'fixer_converter') {
		$access_key = $exchange_rate = Registry::get('addons.wk_advanced_currency_exchange.fixer_access_token');
		$url = "https://data.fixer.io/api/latest?access_key=$access_key&base=$from_Currency&symbols=$to_Currency";
		$data = Http::get($url);
		$data = json_decode($data);
		if($data->success){
			return $data->rates->$to_Currency;
		}
	}elseif($exchange_rate == 'paypal_converter'){
		return fn_convert_currency_by_paypal_convertor($from_Currency,$to_Currency);
	}elseif($exchange_rate == 'oanda_converter') {
		$oanda_api_key = $exchange_rate = Registry::get('addons.wk_advanced_currency_exchange.oanda_api_key');
		$headers = array('X-Accept-Datetime-Format','UNIX');
		$url = "https://www.oanda.com/rates/api/v2/rates/spot.json?base=$from_Currency&quote=$to_Currency&api_key=$oanda_api_key";
		$data = Http::get($url);
		$data = json_decode($data,true);
		if(isset($data['quotes'])){
			foreach($data['quotes'] as $_data){
				if($_data['base_currency'] == $from_Currency && $_data['quote_currency'] == $to_Currency){
					return $_data['ask'];
				}
			}
		}
	}elseif($exchange_rate == 'ecb_converter') {
		$XML=simplexml_load_file("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"); 
		if($to_Currency == 'EUR'){
			foreach($XML->Cube->Cube->Cube as $rate){
				if($rate['currency'] == $from_Currency){
					try{
						return floatval(1.0)/floatval($rate['rate']);
					}catch(Exception $e){

					}
				}
			} 
		}elseif($from_Currency == 'EUR'){
			foreach($XML->Cube->Cube->Cube as $rate){
				if($rate['currency'] == $from_Currency){
					return $rate['rate'];
				}
			} 
		}
	}
	return false;    
}

function fn_convert_currency_by_paypal_convertor($from_Currency,$to_Currency){
	$headers = array();
    $headers[] = 'X-PAYPAL-SECURITY-USERID: '.Registry::get('addons.wk_advanced_currency_exchange.paypal_username');
    $headers[] = 'X-PAYPAL-SECURITY-PASSWORD: '.Registry::get('addons.wk_advanced_currency_exchange.paypal_password');
	$headers[] = 'X-PAYPAL-APPLICATION-ID: '.Registry::get('addons.wk_advanced_currency_exchange.paypal_appid');
	$headers[] = 'X-PAYPAL-SECURITY-SIGNATURE: '.Registry::get('addons.wk_advanced_currency_exchange.paypal_signature');
    $headers[] = 'X-PAYPAL-REQUEST-DATA-FORMAT: NV';
    $headers[] = 'X-PAYPAL-RESPONSE-DATA-FORMAT: NV';
    $headers[] = 'Content-type: application/x-www-form-urlencoded';
	$headers[] = 'Connection: close';
	
	if(Registry::get('addons.wk_advanced_currency_exchange.paypal_mode') == 'production'){
		$url = "https://svcs.paypal.com/AdaptivePayments/ConvertCurrency";
	}else{
		$url = "https://svcs.sandbox.paypal.com/AdaptivePayments/ConvertCurrency";
		$headers[] = 'X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T';
	}

	$post_data = array(
        "actionType" => "ConvertCurrency",
		"baseAmountList.currency.code" => $from_Currency,
		"baseAmountList.currency.amount" => 1,
        "convertToCurrencyList.currencyCode" => $to_Currency,
		"requestEnvelope.errorLanguage" => "en_US",
		"countryCode" 					=> Registry::get('settings.general.default_country'),
		"conversionType"				=> "BALANCE_TRANSFER"
    );
	$response = Http::post($url, $post_data, array('headers' => $headers, 'ssl_cert' => ''));
	parse_str($response, $pp_response);

	if(!empty($pp_response) && $pp_response['responseEnvelope_ack'] == 'Success'){
		$response_key = "estimatedAmountTable_currencyConversionList_(0)_currencyList_currency(0)";
		$response_amount = $response_key.'_amount';
		$response_code = $response_key.'_code';
		$pp_response = array_reverse($pp_response);
		foreach($pp_response as $key=>$value){
			if(strpos($key,'_currencyList_currency') !== false && strpos($key,'_amount') !== false){
				return $value;
			}
		}
	}
	return false;
}

function fn_get_live_exchange_specification($lang_code = CART_LANGUAGE){
    $explanation = __('live_exhange_api_specification_info', '', $lang_code);
    return "<div>$explanation</div>";
}