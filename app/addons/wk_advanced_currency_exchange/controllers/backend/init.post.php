<?php
/************************************************************************
# wk_advanced_currency_exchange  ---  wk_advanced_currency_exchange     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;
use Tygh\Http;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$currencies=db_get_array("SELECT * FROM ?:currencies WHERE auto_update = ?s AND next_update < ?i",'Y',TIME);

if(!empty($currencies))
{
	foreach ($currencies as $key => $currency) {

      	$currency['coefficient'] = fn_adv_curr_get_live_rate($currency['currency_code'],CART_PRIMARY_CURRENCY);
		if ($currency['coefficient']) {
	   		if($currency['modifier_type'] == 'A')
		    {
		      if($currency['modifier_sign'] == 'A') {
						$currency['coefficient']+=$currency['modifier_rate'];
					} else {
						$currency['coefficient']-=$currency['modifier_rate'];
					}
		    } elseif ($currency['modifier_type'] == 'P') {
		      $get_percentage_amount = ($currency['modifier_rate']*$currency['coefficient'])/100;
		      if($currency['modifier_sign'] == 'A') {
						$currency['coefficient']+=$get_percentage_amount;
					} else {
						$currency['coefficient']-=$get_percentage_amount;
					}
		    }
			if($currency['coefficient'] > 0) {
				$currency['next_update']= TIME + (Registry::get('addons.wk_advanced_currency_exchange.update_time')*60);
				db_query('UPDATE ?:currencies SET ?u WHERE currency_id = ?i', $currency, $currency['currency_id']);
			}		
		}	
	}
}