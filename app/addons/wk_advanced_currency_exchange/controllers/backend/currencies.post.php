<?php
/************************************************************************
# wk_advanced_currency_exchange  ---  wk_advanced_currency_exchange     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/
use Tygh\Registry;
use Tygh\Http;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'update_all_exchange') {
    db_query('UPDATE ?:currencies SET auto_update = ?s WHERE auto_update = ?s', 'Y', 'N');
    fn_set_notification('N',__('notice'),__('changes_saved_successfully'));
    return array(CONTROLLER_STATUS_OK, 'currencies.manage');
}

if ($mode == 'update') {
    if (!empty($_REQUEST['currency_id'])) {
        $currency = db_get_row("SELECT a.*, b.description FROM ?:currencies as a LEFT JOIN ?:currency_descriptions as b ON a.currency_code = b.currency_code AND lang_code = ?s WHERE a.currency_id = ?s", DESCR_SL, $_REQUEST['currency_id']);
        $currency['current_exchange_rate'] = fn_adv_curr_get_live_rate($currency['currency_code'],CART_PRIMARY_CURRENCY);
        Tygh::$app['view']->assign('currency', $currency);
        // Tygh::$app['view']->assign('wk_primary_currency', $primary_currency);
    }
}