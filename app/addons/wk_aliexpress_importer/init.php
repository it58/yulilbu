<?php

if(!defined('BOOTSTRAP')){die('Access Denied');}

fn_register_hooks(
    'create_order_details',
    'get_order_items_info_post',
    'create_order',
    'get_orders'
    );