<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Settings;

function fn_settings_variants_addons_wk_aliexpress_importer_alipdt_companyid()
{
    $data = array();
    $data = fn_get_short_companies();
    foreach ($data as $key => $value) {
        if(!$key)
            unset($data[$key]);
    }
    return $data;
}

function fn_update_wk_aliexpress_importer_settings($settings){
     
    if (isset($settings)) {
        $settings['wkimporter_cat_addon'] = serialize($settings);
    }
    foreach ($settings as $setting_name => $setting_value) {
        Settings::instance()->updateValue($setting_name, $setting_value);
    }
    $pp_settings = Settings::instance()->getValues('wk_aliexpress_importer', 'ADDON');
  
}
function fn_get_wk_aliexpress_importer_settings(){
    $pp_settings = Settings::instance()->getValues('wk_aliexpress_importer', 'ADDON');
    if (!empty($pp_settings['general']['wkimporter_cat_addon'])) {
        $pp_settings['general']['wkimporter_cat_addon'] = unserialize($pp_settings['general']['wkimporter_cat_addon']);
    }
    return $pp_settings;
}
function fn_wk_aliexpressimport($request){
    $result = array('auth' => false);
    if(isset($request['email']) && !empty($request['email']) && isset($request['key']) && !empty($request['key']))
    {
        $verify_key = db_get_field("SELECT api_key FROM ?:users WHERE email = ?s",$request['email']);
        if(isset($verify_key) && !empty($verify_key) && $verify_key == $request['key'])
            $result = array('auth' => true);
    }
    return $result;
}
function fn_save_aliexpress_product_data($product_id,$request) {
    $data = array(
        'aliexpress_pid' => $request['productid'],
        'product_id' => $product_id,
        'product_url' => $request['p_url']
    );
    db_query("INSERT INTO ?:aliexpress_products ?e",$data);
}

function fn_is_aliexpress_product($s_product_id){
    $ali_pid = db_get_field("SELECT aliexpress_pid FROM ?:aliexpress_products WHERE product_id = ?i",$s_product_id);
    if(isset($ali_pid) && !empty($ali_pid))
        return $ali_pid;
    return false; 
}

function fn_wk_aliexpress_importer_create_order_details($order_id, $cart, &$order_details, $extra){
    if(fn_is_aliexpress_product($order_details['product_id']))
        $order_details['is_aliexpress_pdt'] = 1;
}

/*function fn_wk_aliexpress_importer_get_products_pre($params, $items_per_page, $lang_code){
    if(isset($params['aliexpress_pdt']) && !empty($params['aliexpress_pdt'])){
        $p_ids = db_get_fields("SELECT product_id FROM ?:aliexpress_products WHERE ")
    }
}*/


// function fn_wk_aliexpress_importer_get_products_before_select(&$params)
// {
//     if (isset($params['aliexpress_pdt']) && $params['aliexpress_pdt'] == 'Y') {
//         $p_ids =  db_get_fields("SELECT product_id FROM ?:aliexpress_products WHERE 1");
//         fn_print_r($p_ids);
//         $params['item_ids'] = implode(',', array_keys($p_ids));
//     }
// }
function  fn_save_aliexpress_product_options_data($product_id,$request)
{
    $_REQUEST = array();
    if(isset($request['option_data']) && !empty($request['option_data'])){
        foreach ($request['option_data'] as $key => $op_data) {
      
         $variant_arr = $image_data = $file_image = $type_image = array ();
         foreach ($op_data['value'] as $key => $variant_data) {
             $variant_arr[$key] = array (
                'position' => 0,
                'variant_name' => $variant_data['name'],
                'modifier' => '',
                'modifier_type' => 'A',
                'weight_modifier' => '',
                'weight_modifier_type' => 'A',
                'status' => 'A',
                'point_modifier' => 0.000,
                'point_modifier_type' => 'A',
                'ali_variant_id' => $variant_data['optionid'],

             );
             $image_data[$key] = array(
                 'pair_id' => '',
                    'type' => 'V',
                    'object_id' => 0,
                    'image_alt' => '',
             );
            $file_image[$key] = isset($variant_data['imgUrl']) && !empty($variant_data['imgUrl'])?$variant_data['imgUrl']:'';
            $type_image[$key] = 'url';
         }
         $options = array(
       
            'product_id' => $product_id,
            'option_name' => $op_data['title'],
            'position' => 0,
            'inventory' => 'N',
            'company_id' => 1,
            'option_type' => 'S',
            'description' => '',
            'comment' => '',
            'required' => 'N',
            'regexp' => '',
            'inner_hint' => '',
           'incorrect_message' => '',
           'allowed_extensions' => '',
           'max_file_size' => '',
            'multiupload' => 'N',
            'variants' => $variant_arr,
            'ali_option_id' => $op_data['propid'],
            'lang_code' => DESCR_SL
        );
        $_REQUEST = array(
            'option_id' => 0,
            'option_data' => $options,
            'file_variant_image_image_icon' => $file_image,
            'type_variant_image_image_icon' => $type_image,
            'object' => 'product',
            'variant_image_image_data' => $image_data,
        );
        $option_id = fn_update_product_option($_REQUEST['option_data']);
    }
    }
   
    
}
function fn_wk_aliexpress_importer_get_order_items_info_post(&$order, $v, $k){
    if((!isset($order['ali_url']) && empty($order['ali_url'])) && isset($v['is_aliexpress_pdt']) && $v['is_aliexpress_pdt'] == 1){
        $ali_url  =  db_get_field("SELECT product_url FROM ?:aliexpress_products WHERE product_id = ?i",$v['product_id']);
        // $order['ali_url'] = "https:".$ali_url;
        $order['ali_url'] = $ali_url;
    }
}
function fn_wk_aliexpress_importer_create_order(&$order){
    foreach ($order['products'] as $item_id => $product_data) {
        if(fn_is_aliexpress_product($product_data['product_id']))
            $order['ali_order'] = 1;
    }
   
}
function fn_wk_aliexpress_importer_get_orders($params, $fields, $sortings, &$condition, $join, $group){
     if(isset($_REQUEST['aliexpress_order']))
        $condition .= db_quote(" AND ?:orders.ali_order = ?i", 1);
    
}
function fn_wkaliexpress_fulldesc_update($product_id,$desc) {
    $auth = & Tygh::$app['session']['auth'];
    // fn_print_r($auth);
	// $auth = fn_fill_auth($auth,array(),false,'A');

    $product_data = fn_get_product_data($product_id,$auth); 
    $product_data['full_description'] .= $desc;
    $_REQUEST = array(
        'product_data' => $product_data,
        'product_id' => $product_id
    );
    $product_id = fn_update_product($_REQUEST['product_data'], $_REQUEST['product_id'], DESCR_SL);

    
    // $curr_desc = db_get_field("SELECT full_description FROM ?:product_descriptions WHERE product_id = ?i AND lang_code = ?s",$product_id,DESCR_SL);
    // $new_desc = $curr_desc.$desc;
    // db_query('UPDATE ?:product_descriptions SET full_description = ?s WHERE product_id = ?i AND lang_code = ?s',
    //                 $new_desc, $product_id, DESCR_SL);
}