<?php

use Tygh\Registry;

if(!defined('BOOTSTRAP')){die("Access Denied");}

if($mode == 'authenticate'){
    $result = fn_wk_aliexpressimport($_REQUEST);
    echo $_REQUEST['callback']."(".json_encode($result).");";    
}
if($mode == 'product_descupdate'){

    $callback = $_REQUEST['callback'];
    fn_trusted_vars('desc');
   
    fn_wkaliexpress_fulldesc_update($_REQUEST['product_id'],$_REQUEST['desc']);
    echo $callback."(".json_encode(array('update' => true)).");";
}
if($mode == 'product_optionupdate'){
    $req_data = $_REQUEST;
   if(isset($req_data['product_id']) && !empty($req_data['product_id']))
   {
        fn_save_aliexpress_product_options_data($req_data['product_id'],$req_data);
        echo $req_data['callback']."(".json_encode(array('update' => true)).");";
   }else{
        echo $req_data['callback']."(".json_encode(array('update' => false)).");";

   }
}
if($mode == 'product_import'){
    $req_data = $_REQUEST; 
    $descdata = '';
    $output = file_get_contents($req_data['p_url']);
    $dom = new DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTML($output);
    libxml_clear_errors();
    $id = $dom->getElementById("j-product-desc");
    $descdata = DOMinnerHTML($id);
    $categ_data = unserialize(Registry::get('addons.wk_aliexpress_importer.wkimporter_cat_addon'));
    if(Registry::get('addons.wk_aliexpress_importer.alipdt_price') == 'I'){
        $price = $req_data['price'] + ($req_data['price'] * Registry::get('addons.wk_aliexpress_importer.alipdt_priceval')/100);
    }elseif(Registry::get('addons.wk_aliexpress_importer.alipdt_price') == 'D'){
        $price = $req_data['price'] - ($req_data['price'] * Registry::get('addons.wk_aliexpress_importer.alipdt_priceval')/100);
    }elseif(Registry::get('addons.wk_aliexpress_importer.alipdt_price') == 'S'){
        $price = $req_data['price'];
    }elseif (Registry::get('addons.wk_aliexpress_importer.alipdt_price') == 'C') {
        $price = Registry::get('addons.wk_aliexpress_importer.alipdt_priceval');        
    }
    $product_data = array(
        'product' =>$_REQUEST['product'],
        'price' => $price,
        'category_ids' => $categ_data['category_ids'],
        'company_id' => Registry::get('addons.wk_aliexpress_importer.alipdt_companyid'),
        'amount' => Registry::get('addons.wk_aliexpress_importer.alipdt_qty'),
        'status' => 'A',
        'page_title' => isset($_REQUEST['page_title']) && !empty($_REQUEST['page_title'])?$_REQUEST['page_title']:'',
        'meta_description' => isset($_REQUEST['meta_description']) && !empty($_REQUEST['meta_description']) ? $_REQUEST['meta_description']: '',
        'meta_keywords' => isset($_REQUEST['meta_keyword']) && !empty($_REQUEST['meta_keyword']) ? $_REQUEST['meta_keyword'] : '',
        'full_description' => isset($descdata) && !empty($descdata) ? $descdata : '',
    );
    $_REQUEST = array ();
    if(isset($req_data['imageUrl']) && !empty($req_data['imageUrl'])){
        $_REQUEST['type_product_main_image_detailed'] = array('url');
        $_REQUEST['file_product_main_image_icon'] = array('product_main');
        $_REQUEST['type_product_main_image_icon'] = array('local');
        $_REQUEST['file_product_main_image_detailed'] = array($req_data['imageUrl']);
        $_REQUEST['product_main_image_data'] = array(
            array(
                'pair_id' => '',
                'type' => 'M',
                'object_id' => 0,
                'image_alt' => '',
                'detailed_alt' => '', 
            )
        );
    $_REQUEST['file_product_add_additional_image_icon'] = array (
        'product_add_additional'
    );

    $_REQUEST['type_product_add_additional_image_icon'] = array
        (
            'local'
        );

    }
    if(isset($req_data['add_image']) && !empty($req_data['add_image'])){
        $_REQUEST['product_add_additional_image_data'] = array();
        foreach ($req_data['add_image'] as $key => $image_data) {
            $_REQUEST['product_add_additional_image_data'][$key] = array(
                'pair_id' => '',
                'type' => 'A',
                'object_id' => 0,
                'image_alt' => '',
                'detailed_alt' => $image_data['alt'], 
            );
            $_REQUEST['type_product_add_additional_image_detailed'][$key] = 'url';
            $_REQUEST['file_product_add_additional_image_detailed'][$key] = 'product_add_additional';
            $_REQUEST['file_product_add_additional_image_detailed'][$key] = $image_data['src'];
        }
    }
    $_REQUEST['product_id'] = 0;
    $_REQUEST['product_data'] = $product_data;
    
    if (!empty($_REQUEST['product_data']['product'])) {
        if (isset($_REQUEST['product_data']['category_ids'])) {
            $_REQUEST['product_data']['category_ids'] = explode(',', $_REQUEST['product_data']['category_ids']);
        }
        $product_id = fn_update_product($_REQUEST['product_data'], $_REQUEST['product_id'], DESCR_SL);
        if ($product_id === false) {
            echo $req_data['callback']."(".json_encode(array('update' => false)).");";  
        }else{
             
            fn_save_aliexpress_product_options_data($product_id,$req_data);
            fn_save_aliexpress_product_data($product_id,$req_data);
            echo $req_data['callback']."(".json_encode(array('update' => true,'product_id' => $product_id)).");";            
        }
    }
}
if($mode == 'getorder_data'){
    $result = array ();
    if(isset($_REQUEST['order_id']) && !empty($_REQUEST['order_id'])){
        $order_info = fn_get_order_info($_REQUEST['order_id']);
        
        $result['customer_info'] = array (
            'firstname' => $order_info['firstname'],
            'lastname' => $order_info['lastname'],
            'company' => $order_info['company'],
            'b_firstname' => $order_info['b_firstname'],
            'b_lastname' => $order_info['b_lastname'],
            'b_address' => $order_info['b_address'],
            'b_address_2' => $order_info['b_address_2'],
            'b_city' => $order_info['b_city'],
            'b_county' => $order_info['b_county'],
            'b_state' => $order_info['b_state'],
            'b_country' => $order_info['b_country'],
            'b_zipcode' => $order_info['b_zipcode'],
            'b_phone' => $order_info['b_phone'],
            's_firstname' => $order_info['s_firstname'],
            's_lastname' => $order_info['s_lastname'],
            's_address' => $order_info['s_address'],
            's_address_2' => $order_info['s_address_2'],
            's_city' => $order_info['s_city'],
            's_county' => $order_info['s_county'],
            's_state' => $order_info['s_state'],
            's_country' => $order_info['s_country'],
            's_zipcode' => $order_info['s_zipcode'],
            's_phone' => $order_info['s_phone'],
            's_address_type' => $order_info['s_address_type'],
            'phone' => $order_info['phone'],
            'fax' => $order_info['fax'],
            'url' => $order_info['url'],
            'email' => $order_info['email']
        );
        $url = $aliproduct_ids = $aliqty = $alioptions = '';
        foreach ($order_info['products'] as $key => $p_data) {
            if(isset($p_data['extra']['product_options']) && !empty($p_data['extra']['product_options'])){
                $aliexpress_options = '';
               
                foreach ($p_data['extra']['product_options'] as $option_id => $variant_id) {
                    $ali_option_id = db_get_field("SELECT ali_option_id FROM ?:product_options WHERE product_id = ?i AND option_id = ?i",$p_data['product_id'],$option_id);
                    $ali_variant_id = db_get_field("SELECT ali_variant_id FROM ?:product_option_variants WHERE option_id = ?i AND variant_id = ?i",$option_id,$variant_id);
                   
                    if(empty($aliexpress_options))
                        $aliexpress_options = $ali_variant_id;
                    else {
                       $aliexpress_options .= "+".$ali_variant_id;
                    }
                   
                }
               
            }
           
            if(isset($p_data['is_aliexpress_pdt']) && $p_data['is_aliexpress_pdt']){
                $aliexpress_data = db_get_row("SELECT * FROM ?:aliexpress_products WHERE product_id = ?i",$p_data['product_id']);
               
                $result['pro_data'][] = array(
                    'store_pid' => $p_data['product_id'], 
                    'qty' => $p_data['amount'],
                    'url' => $aliexpress_data['product_url'],
                    'options' => isset($aliexpress_options) && !empty($aliexpress_options) ? $aliexpress_options:'',
                    'ali_pid' =>  $aliexpress_data['aliexpress_pid']
                );
            }
        }
    echo $_REQUEST['callback']."(".json_encode($result).");";         
    }

}
if($mode == 'getcustomer_data'){
    $result = array ();
    if(isset($_REQUEST['order_id']) && !empty($_REQUEST['order_id'])){
        $order_info = fn_get_order_info($_REQUEST['order_id']);
        $result = array (
            'contact_name' => $order_info['firstname']." ".$order_info['lastname'],
            'shipping_address_1' => $order_info['s_address'],
            'shipping_address_2' => $order_info['s_address_2'],
            'shipping_city' => $order_info['s_city'],
            's_county' => $order_info['s_county'],
            's_state' => $order_info['s_state'],
            's_country' => $order_info['s_country'],
            'zipcode' => $order_info['s_zipcode'],
            'telephone' => isset($order_info['s_phone']) && !empty($order_info['s_phone']) ? $order_info['s_phone']:$order_info['b_phone'],
            's_address_type' => $order_info['s_address_type'],
            'phone' => $order_info['phone'],
            'fax' => $order_info['fax'],
            'url' => $order_info['url'],
            'email' => $order_info['email']
        );
       
        }
    echo $_REQUEST['callback']."(".json_encode($result).");";         
}
function DOMinnerHTML(DOMNode $element) 
{ 
    $innerHTML = ""; 
    $children  = $element->childNodes;

    foreach ($children as $child) 
    { 
        // fn_print_r($child);
        if($child->nodeValue == 'Product Description')
            continue;
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        // if($child->hasChildNodes)
        // {
            
        //     DOMinnerHTML($child);
        // }
    }

    return $innerHTML; 
} 
function get_inner_html( $node ) {
    $innerHTML= '';
    $children = $node->childNodes;
    foreach ($children as $child) {
        $innerHTML .= $child->ownerDocument->saveXML( $child );
    }
    return $innerHTML;
} 
exit;
    