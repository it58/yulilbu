<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

$dirs = Registry::get('config.dir');

$files = array(
    $dirs['addons'] . 'sd_seo_pack/schemas/sd_seo_pack/literal_converter.php',
    $dirs['schemas'] . 'literal_converter/ru.php',
    $dirs['schemas'] . 'literal_converter/umlauts.php',
);

$schema = array();

foreach ($files as $file) {
    $schema = include($file);
}

return $schema;
