<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema = !empty($schema) ? $schema : array();

$general = array(
    '\'' => '',
    '"' => '',
    '&' => SD_SEO_PACK_SEO_DELIMITER . 'and' . SD_SEO_PACK_SEO_DELIMITER,
    '?' => '_',
    ' ' => '_',
    '/' => '_',
    '(' => '_',
    ')' => '_',
    '[' => '_',
    ']' => '_',
    '%' => '_',
    '#' => '_',
    ',' => '_',
    ':' => '_',
    '.' => '_'
);

return array_merge($schema, $general);
