<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema = array(
    SD_SEO_PACK_TYPE_PRODUCT => array('product', 'company_name', 'product_code', 'price', 'list_price', 'amount', 'weight', 'short_description', 'full_description', 'lang_code'),
    SD_SEO_PACK_TYPE_CATEGORY => array('category', 'company_name', 'description', 'lang_code'),
    SD_SEO_PACK_TYPE_PAGE => array('page', 'company_name', 'avail_till_timestamp', 'avail_from_timestamp', 'description', 'lang_code'),
);

return $schema;
