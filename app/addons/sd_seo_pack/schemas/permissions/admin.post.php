<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['seo_pack'] = array(
    'permissions' => 'sd_seo_pack',
    'modes' => array(
        'manage' => array(
            'permissions' => 'sd_seo_pack'
        ),
        'update' => array(
            'permissions' => 'sd_seo_pack'
        ),
        'add' => array(
            'permissions' => 'sd_seo_pack'
        ),
        'delete' => array(
            'permissions' => 'sd_seo_pack'
        ),
        'm_delete' => array(
            'permissions' => 'sd_seo_pack'
        ),
        'clone' => array(
            'permissions' => 'sd_seo_pack'
        ),
        'm_clone' => array(
            'permissions' => 'sd_seo_pack'
        ),
    ),
);

return $schema;