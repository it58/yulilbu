<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Debugger;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$params = $_REQUEST;
$params['pattern_id'] = empty($params['pattern_id']) ? 0 : $params['pattern_id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $suffix = '.manage';
    $params['pattern_id'] = empty($params['pattern_data']['pattern_id']) ? 0 : $params['pattern_data']['pattern_id'];

    if ($mode == 'update' || $mode == 'add') {
        if (!empty($params['pattern_data'])) {
            fn_sd_seo_pack_prepare_text_fields($params['pattern_data']);
            $pattern_id = fn_sd_seo_pack_update_pattern($params['pattern_data'], $params['pattern_id'], DESCR_SL);
            if (!empty($pattern_id)) {
                $suffix = '.update?pattern_id=' . $pattern_id;
            }
        }

    } elseif ($mode == 'delete' || $mode == 'm_delete') {
        if (!empty($params['pattern_ids'])) {
            fn_sd_seo_pack_delete_pattern($params['pattern_ids']);
        }
        if (!empty($params['return_url'])) {
            return array(CONTROLLER_STATUS_OK, $params['return_url']);
        }

    } elseif ($mode == 'clone' || $mode == 'm_clone') {
        if (!empty($params['return_url'])) {
            $return_url = $params['return_url'];
        }
        if (!empty($params['pattern_ids'])) {
            if (is_array($params['pattern_ids'])) {
                $pids = array();
                foreach ($params['pattern_ids'] as $pattern_id) {
                    $pids[] = fn_sd_seo_pack_clone_pattern($pattern_id);
                }
                $return_url = 'seo_pack.manage?pattern_ids[]=' . implode('&pattern_ids[]=', $pids);
            } else {
                $pid = fn_sd_seo_pack_clone_pattern($params['pattern_ids']);
                $return_url = 'seo_pack.update?pattern_id=' . $pid;
            }
        }
        if (!empty($return_url)) {
            if (!empty($_REQUEST['redirect_url'])) {
                unset($_REQUEST['redirect_url']);
            }
            return array(CONTROLLER_STATUS_OK, $return_url);
        }
    }

    return array(CONTROLLER_STATUS_OK, 'seo_pack' . $suffix);
}

if ($mode == 'manage') {
    $params['location'] = true;
    list($patterns, $search) = fn_sd_seo_pack_get_patterns($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);

    Tygh::$app['view']->assign(array(
        'search' => $search,
        'patterns' => $patterns,
        'is_vendor' => (fn_allowed_for('MULTIVENDOR') && fn_get_runtime_company_id() != 0)
    ));

} elseif ($mode == 'update' || $mode == 'add') {
    $type = !empty($params['type']) ? $params['type'] : SD_SEO_PACK_TYPE_PRODUCT;

    if (fn_allowed_for('MULTIVENDOR') && fn_get_runtime_company_id() != 0) {
        $is_vendor = true;
        $type = ($type != SD_SEO_PACK_TYPE_CATEGORY) ? $type : SD_SEO_PACK_TYPE_PRODUCT;
    }

    $tabs['general'] = array(
        'title' => __('general'),
        'js' => true
    );

    if ($mode == 'update') {
        $tabs['update'] = array (
            'title' => __('update'),
            'js' => true
        );
        $get_pattern = array(
            'pattern_ids' => array($params['pattern_id'])
        );
        list($patterns, ) = fn_sd_seo_pack_get_patterns($get_pattern, 1, DESCR_SL);

        if (empty($patterns)) {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }

        $pattern = reset($patterns);
        $type = $pattern['type'];

        Tygh::$app['view']->assign('pattern_data', $pattern);
    }

    Tygh::$app['view']->assign(array(
        'type' => $type,
        'variables' => fn_sd_seo_pack_get_variables($type),
        'is_vendor' => !empty($is_vendor)
    ));
    Registry::set('navigation.tabs', $tabs);

} elseif ($mode == 'generate') {

    $lang_code = !empty($params['lang_code']) ? $params['lang_code'] : DESCR_SL;
    $page = !empty($params['page']) ? $params['page'] : 1;
    $items_per_page = !empty($params['items_per_page']) ? $params['items_per_page'] : Registry::get('settings.Appearance.admin_elements_per_page');
    $return_url = !empty($params['return_url']) ? $params['return_url'] : 'seo_pack.manage';
    
    if (Debugger::isActive() || fn_is_development()) {
        Registry::set('runtime.comet', true);
    }

    $get_pattern = array(
        'pattern_ids' => array($params['pattern_id'])
    );
    list($patterns, ) = fn_sd_seo_pack_get_patterns($get_pattern, 1, $lang_code);

    if (!empty($patterns)) {
        $pattern_data = reset($patterns);
        $upgrade_fields = fn_sd_seo_pack_get_upgrade_fields($pattern_data);
        if (!empty($upgrade_fields)) {
            $put_data = array(
                'pattern_data' => $pattern_data,
                'upgrade_fields' => $upgrade_fields
            );

            $generate_params = array(
                'page' => $page,
                'items_per_page' => SD_SEO_PACK_ITEMS_IN_GENERATION,
                'subcats' => Registry::get('settings.General.show_products_from_subcategories'),
                'page_ids' => !empty($pattern_data['page_ids']) ? $pattern_data['page_ids'] : array(),
                'category_ids' => !empty($pattern_data['category_ids']) ? $pattern_data['category_ids'] : array(),
                'product_ids' => !empty($pattern_data['product_ids']) ? $pattern_data['product_ids'] : array()
            );
            if (!empty($pattern_data['company_id'])) {
                $generate_params['company_id'] = $pattern_data['company_id'];
            }

            if ($pattern_data['type'] == SD_SEO_PACK_TYPE_PRODUCT) {
                list($products, $search) = fn_sd_seo_pack_get_products_for_cron($generate_params, $lang_code);
                if (!empty($products)) {
                    $put_data['data'] = $products;
                    $put_data['type'] = SD_SEO_PACK_TYPE_PRODUCT;

                    $data = fn_sd_seo_pack_update_meta($put_data, $lang_code);
                    $data['pattern_data'] = $pattern_data;
                    fn_sd_seo_pack_update_product_meta($data, $lang_code);
                }

            } elseif ($pattern_data['type'] == SD_SEO_PACK_TYPE_CATEGORY) {
                list($categories, $search) = fn_sd_seo_pack_get_categories_for_cron($generate_params, $lang_code);
                if (!empty($categories)) {
                    $put_data['data'] = $categories;
                    $put_data['type'] = SD_SEO_PACK_TYPE_CATEGORY;

                    $data = fn_sd_seo_pack_update_meta($put_data, $lang_code);
                    $data['pattern_data'] = $pattern_data;
                    fn_sd_seo_pack_update_category_meta($data, $lang_code);
                }

            } elseif ($pattern_data['type'] == SD_SEO_PACK_TYPE_PAGE) {
                $generate_params['full_search'] = true;
                list($pages, $search) = fn_sd_seo_pack_get_pages($generate_params, $lang_code);
                if (!empty($pages)) {
                    $put_data['data'] = $pages;
                    $put_data['type'] = SD_SEO_PACK_TYPE_PAGE;

                    $data = fn_sd_seo_pack_update_meta($put_data, $lang_code);
                    $data['pattern_data'] = $pattern_data;
                    fn_sd_seo_pack_update_page_meta($data, $lang_code);
                }
            }

            $counter = (($page - 1) * $generate_params['items_per_page'] + $search['sd_count_objects']) . '/' . $search['total_items'] . PHP_EOL;
            fn_echo(__('addons.sd_seo_pack.updating_'.$pattern_data['type'], array('[counter]' => $counter)));
            
            $page++;

            if ($search['total_items'] > $search['page'] * $search['items_per_page']) {
                $url = array(
                    'pattern_id' => $params['pattern_id'],
                    'lang_code' => $lang_code,
                    'page' => $page,
                    'items_per_page' => $items_per_page,
                    'return_url' => $return_url,
                );

                fn_redirect(fn_url('seo_pack.generate' . fn_sd_seo_pack_get_line_get_params($url)));
            } else {
                fn_set_notification('N', '', __('addons.sd_seo_pack.generating_done'));
                return array(CONTROLLER_STATUS_OK, urldecode($return_url));
            }
        }
    }

    fn_set_notification('E', __('error'), __('addons.sd_seo_pack.generating_error'));
    return array(CONTROLLER_STATUS_OK, urldecode($return_url));
}
