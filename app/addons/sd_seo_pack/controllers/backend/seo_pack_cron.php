<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'apply') {
    if (!empty($_REQUEST['cron_password']) && $_REQUEST['cron_password'] == Registry::get('settings.Security.cron_password')) {

        $languages = fn_get_translation_languages();
        $patterns_count = fn_sd_seo_pack_get_active_patterns_count();
        $subcats = Registry::get('settings.General.show_products_from_subcategories');

        foreach ($languages as $language) {
            $lang_code = $language['lang_code'];
            list($patterns, ) = fn_sd_seo_pack_get_patterns(array('status' => 'A'), $patterns_count, $lang_code);

            if (empty($patterns)) {
                continue;
            }

            foreach ($patterns as $pattern_data) {
                $upgrade_fields = fn_sd_seo_pack_get_upgrade_fields($pattern_data);
                if (!empty($upgrade_fields)) {
                    $page = 0;
                    $put_data = array(
                        'pattern_data' => $pattern_data,
                        'upgrade_fields' => $upgrade_fields
                    );
                    do {
                        $page++;
                        $params = array(
                            'page' => $page,
                            'subcats' => $subcats,
                            'page_ids' => !empty($pattern_data['page_ids']) ? $pattern_data['page_ids'] : array(),
                            'category_ids' => !empty($pattern_data['category_ids']) ? $pattern_data['category_ids'] : array(),
                            'product_ids' => !empty($pattern_data['product_ids']) ? $pattern_data['product_ids'] : array()
                        );
                        if (!empty($pattern_data['company_id'])) {
                            $params['company_id'] = $pattern_data['company_id'];
                        }

                        if ($pattern_data['type'] == SD_SEO_PACK_TYPE_PRODUCT) {
                            list($products, $search) = fn_sd_seo_pack_get_products_for_cron($params, $lang_code);
                            if (!empty($products)) {
                                $put_data['data'] = $products;
                                $put_data['type'] = SD_SEO_PACK_TYPE_PRODUCT;

                                $data = fn_sd_seo_pack_update_meta($put_data, $lang_code);
                                $data['pattern_data'] = $pattern_data;
                                fn_sd_seo_pack_update_product_meta($data, $lang_code);
                            }

                        } elseif ($pattern_data['type'] == SD_SEO_PACK_TYPE_CATEGORY) {
                            list($categories, $search) = fn_sd_seo_pack_get_categories_for_cron($params, $lang_code);
                            if (!empty($categories)) {
                                $put_data['data'] = $categories;
                                $put_data['type'] = SD_SEO_PACK_TYPE_CATEGORY;

                                $data = fn_sd_seo_pack_update_meta($put_data, $lang_code);
                                $data['pattern_data'] = $pattern_data;
                                fn_sd_seo_pack_update_category_meta($data, $lang_code);
                            }

                        } elseif ($pattern_data['type'] == SD_SEO_PACK_TYPE_PAGE) {
                            $params['full_search'] = true;
                            list($pages, $search) = fn_sd_seo_pack_get_pages($params, $lang_code);
                            if (!empty($pages)) {
                                $put_data['data'] = $pages;
                                $put_data['type'] = SD_SEO_PACK_TYPE_PAGE;

                                $data = fn_sd_seo_pack_update_meta($put_data, $lang_code);
                                $data['pattern_data'] = $pattern_data;
                                fn_sd_seo_pack_update_page_meta($data, $lang_code);
                            }
                        }

                    } while ($search['total_items'] > $search['page'] * $search['items_per_page']);
                }
            }
        }
        fn_print_r(__('done'));
        exit;
    }
}