<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Http;
use Tygh\Addons\SchemesManager;

if (!defined('AREA')) { die('Access denied'); }

function fn_sd_seo_pack_update_pattern($data, $pattern_id, $lang_code = CART_LANGUAGE)
{
    if (empty($data)) {
        return 0;
    }
    if (!empty($pattern_id)) {
        db_query('UPDATE ?:sd_seo_pack_patterns SET ?u WHERE pattern_id = ?i', $data, $pattern_id);
        db_query('UPDATE ?:sd_seo_pack_patterns_descriptions SET ?u WHERE pattern_id = ?i AND lang_code = ?s', $data, $pattern_id, $lang_code);
    } else {
        $pattern_id = $data['pattern_id'] = db_query('REPLACE INTO ?:sd_seo_pack_patterns ?e', $data);

        foreach (fn_get_translation_languages() as $data['lang_code'] => $language) {
            db_query('REPLACE INTO ?:sd_seo_pack_patterns_descriptions ?e', $data);
        }
    }
    return $pattern_id;
}

function fn_sd_seo_pack_clone_pattern($pattern_id)
{
    if (!empty($pattern_id)) {
        $data = db_get_row('SELECT * FROM ?:sd_seo_pack_patterns WHERE pattern_id = ?i', $pattern_id);
        unset($data['pattern_id']);
        $data['status'] = 'D';
        $clone_pattern_id = db_query('INSERT INTO ?:sd_seo_pack_patterns ?e', $data);

        $data = db_get_array('SELECT * FROM ?:sd_seo_pack_patterns_descriptions WHERE pattern_id = ?i', $pattern_id);
        foreach ($data as $row) {
            $row['pattern_id'] = $clone_pattern_id;
            $row['name'] .= ' [CLONE]';
            db_query('INSERT INTO ?:sd_seo_pack_patterns_descriptions ?e', $row);
        }
    }
    return $clone_pattern_id;
}

function fn_sd_seo_pack_delete_pattern($pattern_ids)
{
    if (!empty($pattern_ids)) {
        if (!is_array($pattern_ids)) {
            $pattern_ids = array($pattern_ids);
        }
        db_query('DELETE FROM ?:sd_seo_pack_patterns WHERE pattern_id IN (?n)', $pattern_ids);
        db_query('DELETE FROM ?:sd_seo_pack_patterns_descriptions WHERE pattern_id IN (?n)', $pattern_ids);
    }
}

function fn_sd_seo_pack_get_patterns($params, $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
    // Set default values to input params
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page,
        'get_hidden' => true
    );

    $params = array_merge($default_params, $params);

    // Define fields that should be retrieved
    $fields = array (
        '?:sd_seo_pack_patterns.*',
        '?:sd_seo_pack_patterns_descriptions.name, meta_keywords, meta_description, page_title, alt_img, seo_name'
    );

    // Define sort fields
    $sortings = array (
        'name' => '?:sd_seo_pack_patterns_descriptions.name',
        'type' => '?:sd_seo_pack_patterns.type',
        'status' => '?:sd_seo_pack_patterns.status'
    );

    $conditions = $joins = $groups = array();

    $conditions['company'] = fn_get_company_condition('?:sd_seo_pack_patterns.company_id');

    if (!empty($params['pattern_ids'])) {
        $conditions['id'] = db_quote('?:sd_seo_pack_patterns.pattern_id IN (?n)', $params['pattern_ids']);
    }

    if (!empty($params['type'])) {
        $conditions['type'] = db_quote('?:sd_seo_pack_patterns.type = ?s', $params['type']);
    }

    if (!empty($params['status'])) {
        $conditions['status'] = db_quote('?:sd_seo_pack_patterns.status = ?s', $params['status']);
    }

    $joins['description'] = db_quote('LEFT JOIN ?:sd_seo_pack_patterns_descriptions ON ?:sd_seo_pack_patterns_descriptions.pattern_id = ?:sd_seo_pack_patterns.pattern_id AND ?:sd_seo_pack_patterns_descriptions.lang_code = ?s', $lang_code);

    $sorting = db_sort($params, $sortings, 'name', 'desc');

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field('SELECT COUNT(*) FROM ?:sd_seo_pack_patterns ?p WHERE 1 ?p ?p', implode(' ', $joins), implode(' AND ', $conditions), implode(', ', $groups));
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $patterns = db_get_hash_array(
        'SELECT ?p'
        .' FROM ?:sd_seo_pack_patterns ?p'
        .' WHERE 1 ?p ?p ?p ?p'
        , 'pattern_id', implode(', ', $fields), implode(' ', $joins), implode(' AND ', $conditions), implode(', ', $groups), $sorting, $limit
    );

    if (!empty($params['location'])) {
        foreach ($patterns as $pattern_id => $pattern) {
            if (!empty($pattern['product_ids'])) {
                $pids = (strpos($pattern['product_ids'], ',') !== false) ? $pattern['product_ids'] : array($pattern['product_ids']);
                $patterns[$pattern_id]['product_names'] = fn_get_product_name($pids, $lang_code);
            }
            if (!empty($pattern['category_ids'])) {
                $cids = (strpos($pattern['category_ids'], ',') !== false) ? $pattern['category_ids'] : array($pattern['category_ids']);
                $patterns[$pattern_id]['category_names'] = fn_get_category_name($cids, $lang_code);
            }
            if (!empty($pattern['page_ids'])) {
                $paids = (strpos($pattern['page_ids'], ',') !== false) ? explode(',', $pattern['page_ids']) : array($pattern['page_ids']);
                $patterns[$pattern_id]['page_names'] = fn_get_page_name($paids, $lang_code);
            }
        }
    }

    return array($patterns, $params);
}

function fn_sd_seo_pack_get_active_patterns_count()
{
    return db_get_field('SELECT COUNT(*) FROM ?:sd_seo_pack_patterns WHERE status = ?s', 'A');
}

function fn_sd_seo_pack_get_products_for_cron($params, $lang_code = CART_LANGUAGE)
{
    if (!empty($params['product_ids']) || !empty($params['category_ids'])) {
        $count = 0;
        $default_params = array (
            'page' => 1,
            'items_per_page' => !empty($params['items_per_page']) ? $params['items_per_page'] : SD_SEO_PACK_ITEMS_IN_ITERATION
        );

        if (!empty($params['product_ids']) && !empty($params['category_ids'])) {
            $params['sd_pid'] = $params['product_ids'];
            $params['sd_cid'] = $params['category_ids'];

        } else {
            if (!empty($params['product_ids'])) {
                $params['pid'] = $params['product_ids'];
                unset($params['product_ids']);
            }
            if (!empty($params['category_ids'])) {
                $params['cid'] = $params['category_ids'];
                unset($params['category_ids']);
            }
        }

        if (!empty($params['company_id'])) {
            if (!fn_allowed_for('MULTIVENDOR')) {
                unset($params['company_id']);//for sharing products
            }
        }

        $params['sd_seo_pack'] = true;

        $product_params = array_merge($default_params, $params);

        list($products, $params) = fn_get_products($product_params, 0, $lang_code);

        if (!empty($products)) {
            foreach ($products as $product_id => $product) {
                $products[$product_id]['product_features'] = fn_get_product_features_list($product, 'A');
            }
            fn_gather_additional_products_data($products, array(
                'get_icon' => true,
                'get_detailed' => true,
                'get_additional' => true,
                'get_options' => true,
                'get_discounts' => true,
                'get_features' => true
            ));
            $count = count($products);
        }
        $params['sd_count_objects'] = $count;
        $res = array($products, $params);
    }
    return !empty($res) ? $res : array();
}

function fn_sd_seo_pack_get_products(&$params, &$fields, &$sortings, &$condition, &$join, $sorting, $group_by, $lang_code, $having)
{
    if (!empty($params['sd_seo_pack'])) {
        $sortings['product_id'] = 'products.product_id';
        $params['sort_by'] = 'product_id';
        $params['sort_order'] = 'asc';

        $fields[] = ' descr1.meta_keywords, descr1.meta_description, descr1.page_title, descr1.full_description, descr1.short_description';
    }
    if (!empty($params['sd_pid']) && !empty($params['sd_cid'])) {
        $sd_conditions = array();
        $pids = is_array($params['sd_pid']) ? $params['sd_pid'] : explode(',', $params['sd_pid']);
        $cids = is_array($params['sd_cid']) ? $params['sd_cid'] : explode(',', $params['sd_cid']);

        if (isset($params['subcats']) && $params['subcats'] == 'Y') {
            $ids = db_get_fields(
                'SELECT a.category_id'
                . ' FROM ?:categories as a'
                . ' LEFT JOIN ?:categories as b ON b.category_id IN (?n)'
                . " WHERE a.id_path LIKE CONCAT(b.id_path, '/%')"
                , $cids
            );
            $cids = fn_array_merge($cids, $ids, false);
        }

        $sd_conditions[] = db_quote('?:categories.category_id IN (?n)', $cids);
        $sd_conditions[] = db_quote('products.product_id IN (?n)', $pids);

        $condition .= db_quote(' AND ( ?p )', implode(' OR ', $sd_conditions));
    }
}

function fn_sd_seo_pack_get_categories_for_cron($data, $lang_code = CART_LANGUAGE)
{
    if (!empty($data['category_ids'])) {
        $default_params = array (
            'page' => !empty($data['page']) ? $data['page'] : 1,
            'get_images' => true
        );

        $category_params = array_merge($default_params, $data);
        $items_per_page = !empty($data['items_per_page']) ? $data['items_per_page'] : SD_SEO_PACK_ITEMS_IN_ITERATION;

        list($categories, $search) = fn_sd_seo_pack_get_paginated_categories($category_params, $items_per_page, $lang_code);

        foreach ($categories as $key => $category) {
            if (!empty($category['main_pair']['detailed']['alt'])) {
                $categories[$key]['alt_img'] = $category['main_pair']['detailed']['alt'];
            }
        }
        
        $search['sd_count_objects'] = count($categories);

        $res = array($categories, $search);
    }

    return !empty($res) ? $res : array();
}

function fn_sd_seo_pack_get_paginated_categories($params, $items_per_page = 0, $lang_code = DESCR_SL)
{
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $fields = array(
        '?:categories.*',
        '?:category_descriptions.*',
    );

    $conditions = $joins = array();

    if (!empty($params['status'])) {
        $conditions[] = db_quote('?:categories.status IN (?a)', $params['status']);
    }
    if (!empty($params['parent_id'])) {
        $conditions[] = db_quote('?:categories.parent_id = ?i', $params['parent_id']);
    }
    if (!empty($params['node_id'])) {
        $from_id_path = db_get_field('SELECT id_path FROM ?:categories WHERE category_id = ?i', $params['node_id']);
        $conditions[] = db_quote('?:categories.id_path LIKE ?l', "$from_id_path/%");
    }
    if (!empty($params['category_id'])) {
        $conditions[] = db_quote('?:categories.category_id = ?i', $params['category_id']);
    } elseif (!empty($params['category_ids'])) {
        $cids = is_array($params['category_ids']) ? $params['category_ids'] : explode(',', $params['category_ids']);

        if (isset($params['subcats']) && $params['subcats'] == 'Y') {
            $_ids = db_get_fields(
                'SELECT a.category_id'
                .' FROM ?:categories as a'
                .' LEFT JOIN ?:categories as b  ON b.category_id IN (?n)'
                .' WHERE a.id_path LIKE CONCAT(b.id_path, ?s)'
                , $cids, '/%'
            );

            $cids = fn_array_merge($cids, $_ids, false);
        }

        $conditions['category_ids'] = db_quote('?:categories.category_id IN (?n)', $cids);
    } elseif (!empty($params['item_ids'])) {
        $conditions[] = db_quote('?:categories.category_id IN (?n)', explode(',', $params['item_ids']));
    }
    if (!empty($params['except_id']) 
        && (empty($params['item_ids'])
            || !empty($params['item_ids']) 
            && !in_array($params['except_id'], explode(',', $params['item_ids']))
        )
    ) {
        $conditions[] = db_quote('?:categories.category_id != ?i AND ?:categories.parent_id != ?i', $params['except_id'], $params['except_id']);
    }
    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $conditions[] = db_quote('(?:categories.timestamp >= ?i AND ?:categories.timestamp <= ?i)', $params['time_from'], $params['time_to']);
    }
    if (!empty($params['level'])) {
        $conditions[] = db_quote('?:categories.level = ?i', $params['level']);

    } elseif (!empty($params['levels'])) {
        $conditions[] = db_quote('?:categories.level IN (?n)', $params['levels']);
    }
    if (!empty($params['company_id'])) {
        $conditions[] = db_quote('?:categories.company_id = ?i', $params['company_id']);
    } elseif (!empty($params['sd_company_ids'])) {
        $conditions[] = db_quote('?:categories.company_id IN (?n)', $params['sd_company_ids']);
    }

    if (!empty($params['empty'])) {
        $conditions[] = db_quote('0');
    }

    $joins[] = db_quote('LEFT JOIN ?:category_descriptions ON ?:categories.category_id = ?:category_descriptions.category_id AND ?:category_descriptions.lang_code = ?s', $lang_code);

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field('SELECT COUNT(*) FROM ?:categories ?p WHERE ?p ', implode(' ', $joins), implode(' AND ', $conditions));
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $categories = db_get_hash_array(
        'SELECT ?p'
        .' FROM ?:categories ?p'
        .' WHERE ?p ?p ?p'
        , 'category_id', implode(',', $fields), implode(' ', $joins), implode(' AND ', $conditions), 'ORDER BY ?:categories.category_id ASC', $limit
    );

    if (!empty($categories) && !empty($params['get_images'])) {
        $category_ids = array_keys($categories); 
        $image_pairs_for_categories = fn_get_image_pairs($category_ids, 'category', 'M', true, true, $lang_code);
        if (!empty($image_pairs_for_categories)) {
            foreach ($image_pairs_for_categories as $category_id => $image_data) {
                $categories[$category_id]['main_pair'] = reset($image_data);
            }
        }
    }

    return array($categories, $params);
}


function fn_sd_seo_pack_get_pages($params, $lang_code = CART_LANGUAGE)
{
    if (!empty($params['page_ids'])) {
        $params['item_ids'] = $params['page_ids'];
        $items_per_page = !empty($params['items_per_page']) ? $params['items_per_page'] : SD_SEO_PACK_ITEMS_IN_ITERATION;
        list($pages, $search) = fn_get_pages($params, $items_per_page, $lang_code);
        $search['sd_count_objects'] = count($pages);
        $res = array($pages, $search);
    }
    return !empty($res) ? $res : array();
}

function fn_sd_seo_pack_get_upgrade_fields($data)
{
    $fields = array('meta_keywords', 'meta_description', 'page_title', 'alt_img', 'seo_name');
    foreach ($fields as $field) {
        if (!empty($data[$field]) && $data['upd_' . $field] == 'Y') {
            $res[] = $field;
        }
    }
    return !empty($res) ? $res : array();
}

function fn_sd_seo_pack_get_variables($type = SD_SEO_PACK_TYPE_PRODUCT)
{
    $schema = fn_get_schema('sd_seo_pack', 'variables', 'php');

    if ($type == SD_SEO_PACK_TYPE_PRODUCT) {
        list($features, ) = fn_get_product_features(array('exclude_group' => true));

        $schema_features = array();
        if (!empty($features)) {
            foreach ($features as $feature) {
                $schema_features['feature_' . $feature['feature_id']] = 'feature_' . fn_sd_seo_pack_generate_name($feature['description']);
            }

            $schema[SD_SEO_PACK_TYPE_PRODUCT] = fn_array_merge($schema[SD_SEO_PACK_TYPE_PRODUCT], $schema_features);
        }
    }

    return !empty($schema[$type]) ? $schema[$type] : array();
}

function fn_sd_seo_pack_generate_name($str, $object_type = '', $object_id = 0, $is_multi_lang = false)
{
    $delimiter = SD_SEO_PACK_SEO_DELIMITER;
    $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8'); // convert html special chars back to original chars

    $result = '';

    if (!empty($str)) {
        if ($is_multi_lang) {
            $literals = "/[^a-z\p{Ll}\p{Lu}\p{Lt}\p{Lm}\p{Lo}\p{Nd}\p{Pc}\p{Mn}0-9-\.]/u";
            $convert_letters = fn_get_schema('sd_seo_pack', 'literal_converter');
        } else {
            $literals = "/[^a-z0-9_]/";
            $convert_letters = fn_get_schema('sd_seo_pack', 'schema');
        }
        $str = strtr($str, $convert_letters);

        if (!empty($object_type)) {
            $str .= $delimiter . $object_type . $object_id;
        }

        $str = fn_strtolower($str); // only lower letters
        $str = preg_replace($literals, '', $str); // URL can contain latin letters, numbers, dashes and points only
        $str = preg_replace("/($delimiter){2,}/", $delimiter, $str); // replace double (and more) dashes with one dash

        $result = trim($str, '_'); // remove trailing dash if exist
    }

    return $result;
}

function fn_sd_seo_pack_get_value_variables($type, $data, $lang_code = CART_LANGUAGE)
{
    if (!empty($type) && !empty($data)) {
        $res = array();
        $variables = fn_sd_seo_pack_get_variables($type);

        foreach ($variables as $key => $variable) {
            if (!empty($data[$variable])) {
                if ($variable == 'price' || $variable == 'lise_price') {
                    $res[$variable] = fn_format_price($data[$variable]);
                } elseif ($variable == 'lang_code') {
                    $res[$variable] = $lang_code;
                } else {
                    $res[$variable] = strip_tags($data[$variable]);
                }

            } elseif (preg_match('/^(feature_)/', $variable) 
                && preg_match('/^(feature_)/', $key) 
            ) {
                $feature_id = preg_replace('/^(feature_)/', '', $key);
                if (!empty($data['product_features'][$feature_id])) {
                    if ($data['product_features'][$feature_id]['feature_type'] == 'C' 
                        || $data['product_features'][$feature_id]['feature_type'] == 'T'
                    ) {
                        $res[$variable] = $data['product_features'][$feature_id]['value'];

                    } elseif ($data['product_features'][$feature_id]['feature_type'] == 'S'
                        || $data['product_features'][$feature_id]['feature_type'] == 'N'
                        || $data['product_features'][$feature_id]['feature_type'] == 'E'
                    ) {
                        $res[$variable] = $data['product_features'][$feature_id]['variant'];

                    } elseif ($data['product_features'][$feature_id]['feature_type'] == 'M') {
                        $_res = array();
                        foreach ($data['product_features'][$feature_id]['variants'] as $variant) {
                            $_res[] = $variant['variant'];
                        }
                        $res[$variable] = implode(', ', $_res);

                    } elseif ($data['product_features'][$feature_id]['feature_type'] == 'O') {
                        $res[$variable] = $data['product_features'][$feature_id]['value_int'];

                    } elseif ($data['product_features'][$feature_id]['feature_type'] == 'D') {
                        $res[$variable] = fn_date_format($data['product_features'][$feature_id]['value_int'], Registry::get('settings.Appearance.date_format'));
                    }
                }
            }
        }
    }

    return !empty($res) ? $res : array();
}

function fn_sd_seo_pack_render_pattern($data, $lang_code = CART_LANGUAGE, $area = 'C')
{
    if (!empty($data['variables']) && !empty($data['template'])) {
        $renderer = Tygh::$app['template.renderer'];

        $template = new \Tygh\Template\Mail\Template;
        $template->setTemplate($data['template']);

        $context = new \Tygh\Template\Mail\Context($data['variables'], $area, $lang_code);
        $collection = new \Tygh\Template\Collection($context->data);

        try {
            $res = $renderer->renderTemplate($template, $context, $collection);
        } catch (Exception $e) {
            $res = false;
        }
    }
    return !empty($res) ? $res : '';
}

function fn_sd_seo_pack_update_product_meta($products, $lang_code = CART_LANGUAGE)
{
    if (!empty($products)) {
        $seo_addon_status = Registry::get('addons.seo.status');
        foreach ($products as $product) {
            if (!empty($product['upd_data'])) {
                if (!fn_allowed_for('MULTIVENDOR')
                    && (((!empty($products['pattern_data']['company_id']) && !empty($product['company_id']) && $products['pattern_data']['company_id'] == $product['company_id']) 
                        || (empty($products['pattern_data']['company_id']) || empty($product['company_id']))))
                    || fn_allowed_for('MULTIVENDOR')
                ) {
                    db_query(
                        'UPDATE ?:product_descriptions'
                        .' SET ?u'
                        .' WHERE product_id = ?i AND lang_code = ?s'
                        , $product['upd_data'], $product['product_id'], $lang_code
                    );
                    if (!empty($product['upd_data']['alt_img']) && !empty($product['main_pair']['detailed_id'])) {
                        db_query(
                            'UPDATE ?:common_descriptions'
                            .' SET description = ?s'
                            .' WHERE object_id = ?i AND lang_code = ?s AND object_holder = ?i'
                            , $product['upd_data']['alt_img'], $product['main_pair']['detailed_id'], $lang_code, 'images'
                        );
                    }
                }
                if (!fn_allowed_for('MULTIVENDOR')) {
                    if (!empty($products['pattern_data']['company_id'])) {
                        $company_id = $products['pattern_data']['company_id'];
                    } elseif (!empty($product['company_id'])) {
                        $company_id = $product['company_id'];
                    } else {
                        $company_id = 0;
                    }
                    if (!empty($company_id)) {
                        db_query(
                            'UPDATE ?:ult_product_descriptions'
                            .' SET ?u'
                            .' WHERE product_id = ?i AND lang_code = ?s AND company_id = ?i'
                            , $product['upd_data'], $product['product_id'], $lang_code, $company_id
                        );
                    }
                }
                if ($seo_addon_status == 'A' && !empty($product['upd_data']['seo_name'])) {
                    $product['seo_name'] = $product['upd_data']['seo_name'];
                    if (isset($products['pattern_data']['seo_create_redirect'])) {
                        $product['seo_create_redirect'] = $products['pattern_data']['seo_create_redirect'];
                    }
                    fn_seo_update_object($product, $product['product_id'], 'p', $lang_code);
                }
            }
        }
    }
}

function fn_sd_seo_pack_update_category_meta($categories, $lang_code = CART_LANGUAGE)
{
    if (!empty($categories)) {
        $seo_addon_status = Registry::get('addons.seo.status');
        foreach ($categories as $category) {
            if (!empty($category['upd_data'])) {
                db_query(
                    'UPDATE ?:category_descriptions'
                    .' SET ?u'
                    .' WHERE category_id = ?i AND lang_code = ?s'
                    , $category['upd_data'], $category['category_id'], $lang_code
                );
            }
            if (!empty($category['upd_data']['alt_img']) && !empty($category['main_pair']['detailed_id'])) {
                db_query(
                    'UPDATE ?:common_descriptions'
                    .' SET description = ?s'
                    .' WHERE object_id = ?i AND lang_code = ?s AND object_holder = ?i'
                    , $category['upd_data']['alt_img'], $category['main_pair']['detailed_id'], $lang_code, 'images'
                );
            }
            if ($seo_addon_status == 'A' && !empty($category['upd_data']['seo_name'])) {
                $category['seo_name'] = $category['upd_data']['seo_name'];
                if (isset($categories['pattern_data']['seo_create_redirect'])) {
                    $category['seo_create_redirect'] = $categories['pattern_data']['seo_create_redirect'];
                }
                fn_seo_update_object($category, $category['category_id'], 'c', $lang_code);
            }
        }
    }
}

function fn_sd_seo_pack_update_page_meta($pages, $lang_code = CART_LANGUAGE)
{
    if (!empty($pages)) {
        $seo_addon_status = Registry::get('addons.seo.status');
        foreach ($pages as $page) {
            if (!empty($page['upd_data'])) {
                db_query(
                    'UPDATE ?:page_descriptions'
                    .' SET ?u'
                    .' WHERE page_id = ?i AND lang_code = ?s'
                    , $page['upd_data'], $page['page_id'], $lang_code
                );
                if ($seo_addon_status == 'A' && !empty($page['upd_data']['seo_name'])) {
                    $page['seo_name'] = $page['upd_data']['seo_name'];
                    if (isset($pages['pattern_data']['seo_create_redirect'])) {
                        $page['seo_create_redirect'] = $pages['pattern_data']['seo_create_redirect'];
                    }
                    fn_seo_update_object($page, $page['page_id'], 'a', $lang_code);
                }
            }
        }
    }
}

function fn_sd_seo_pack_update_meta($data, $lang_code = CART_LANGUAGE)
{
    $res = array();
    if (empty($data['data'])
        || empty($data['pattern_data'])
        || empty($data['upgrade_fields'])
        || empty($data['type'])
    ) {
        return $res;
    }
    $seo_addon_status = Registry::get('addons.seo.status');
    foreach ($data['data'] as $index => $object) {
        $values_for_upgrade_fields = fn_sd_seo_pack_get_value_variables($data['type'], $object, $lang_code);
        if (!empty($data['pattern_data']['company_id'])) {
            $company_id = $data['pattern_data']['company_id'];
        } elseif (!empty($object['company_id'])) {
            $company_id = $object['company_id'];
        } else {
            $company_id = 0;
        }
        $values_for_upgrade_fields['company_data'] = array(
            'company_id' => $company_id,
            'company_name' => fn_get_company_name($company_id),
        );
        $values_for_upgrade_fields['lang_code'] = $lang_code;

        foreach ($data['upgrade_fields'] as $field) {
            if (empty($object[$field]) || $data['pattern_data']['rewriting'] == 'Y') {
                $render_params = array(
                    'variables' => $values_for_upgrade_fields,
                    'template' => $data['pattern_data'][$field]
                );
                $data['data'][$index]['upd_data'][$field] = fn_sd_seo_pack_render_pattern($render_params, $lang_code, 'C');
                if ($seo_addon_status == 'A' && $field == 'seo_name' && !empty($data['data'][$index]['upd_data'][$field])) {
                    $seo_settings = fn_get_seo_settings($company_id);
                    $non_latin_symbols = $seo_settings['non_latin_symbols'];
                    $data['data'][$index]['upd_data'][$field] = fn_generate_name($data['data'][$index]['upd_data'][$field], '', 0, ($non_latin_symbols == 'Y'));
                    if (!empty($data['pattern_data']['seo_name_symbols'])) {
                        $data['data'][$index]['upd_data'][$field] = mb_strimwidth($data['data'][$index]['upd_data'][$field], 0, $data['pattern_data']['seo_name_symbols']);
                    }
                }
            }
        }
    }
    return $data['data'];
}

function fn_sd_seo_pack_get_categories($params, $join, $condition, &$fields, $group_by, $sortings, $lang_code)
{
    $fields[] = '?:category_descriptions.description';
}

function fn_sd_seo_pack_get_line_get_params($params)
{
    $res = array();
    if (!empty($params) && is_array($params)) {
        foreach ($params as $name => $value) {
            if ($name != 'dispatch') {
                $res[] = $name . '=' . urlencode($value);
            }
        }
    }
    return !empty($res) ? '?' . implode('&', $res) : '';
}

function fn_sd_seo_pack_check_license_status($license = '')
{
    if (!fn_allowed_for('MULTIVENDOR')) {
        $companies = db_get_array('SELECT storefront, secure_storefront FROM ?:companies');
    } else {
        $companies = array(array('storefront' => fn_url('', 'C', 'http')));
    }

    $addon = 'sd_seo_pack';
    $request = array(
        'companies' => $companies,
        'host' => Registry::get('config.current_host'),
        'lang_code' => CART_LANGUAGE,
        'addon' => $addon,
        'addon_version' => fn_get_addon_version($addon),
        'license' => !empty($license) ? trim($license) : Registry::get("addons.{$addon}.lkey")
    );

    Registry::set('log_cut', true);
    $response = Http::get(
        base64_decode('aHR0cHM6Ly93d3cuc2ltdGVjaGRldi5jb20vaW5kZXgucGhwP2Rpc3BhdGNoPWxpY2Vuc2VzLmNoZWNr'),
        array('request' => urlencode(json_encode($request))),
        array('timeout' => 3)
    );

    if (Http::getStatus() == Http::STATUS_OK) {
        $response_data = json_decode($response, true);
        if ($response_data !== null) {
            $status = isset($response_data['status']) ? $response_data['status'] : 'F';

            if (isset($response_data['notice'])) {
                fn_set_notification(
                    isset($response_data['type']) ? $response_data['type'] : 'W',
                    SchemesManager::getName($addon, CART_LANGUAGE),
                    $response_data['notice'],
                    isset($response_data['state']) ? $response_data['state'] : ''
                );
            }
        } else {
            $status = $response;
        }

        if ($status != 'A') {
            fn_update_addon_status($addon, 'D', false);
        }
    } else {
        $status = 'A';
    }

    return $status == 'A';
}

function fn_settings_actions_addons_sd_seo_pack(&$new_status, $old_status)
{
    if ($new_status == 'A' && !fn_sd_seo_pack_check_license_status()) {
        $new_status = 'D';
    }
}

function fn_settings_actions_addons_sd_seo_pack_lkey(&$new_value, $old_value)
{
    if (fn_sd_seo_pack_check_license_status($new_value)) {
        $new_value = trim($new_value);
    }
}

function fn_sd_seo_pack_set_admin_notification($user_data)
{
    if (AREA == 'A' && $user_data['is_root'] == 'Y') {
        fn_sd_seo_pack_check_license_status();
    }
}

/**
 * Prepares fields data to save: removes extra spaces and etc.
 * @param array &$field_data 
 * @return void
 */
function fn_sd_seo_pack_prepare_text_fields(&$field_data)
{
    $field_names = array('meta_keywords', 'meta_description', 'page_title', 'alt_img', 'seo_name');
    foreach ($field_names as $name) {
        if (!empty($field_data[$name])) {
            // separates stacked variable blocks at textareas
            $field_data[$name] = str_replace('}}{{', '}} {{', $field_data[$name]);

            // removes leading, trailing whitespaces and replaces double whitespaces and line breaks by one whitespace
            $field_data[$name] = trim(preg_replace('/\s+|[\r\n]/', ' ', $field_data[$name]));
        }
    }
}
