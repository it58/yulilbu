<?php
/**
 *
 * Thank you for your purchase! You are the best!
 *
 * @copyright    (C) 2017 Hungryweb
 * @website      https://www.hungryweb.net/
 * @support      support@hungryweb.net
 * @license      https://www.hungryweb.net/license-agreement.html
 *
 * ---------------------------------------------------------------------------------
 * This is a commercial software, only users who have purchased a valid license
 * and accepts the terms of the License Agreement can install and use this program.
 * ---------------------------------------------------------------------------------
 *
 */

$schema['top']['addons']['items']['hw_custom_code'] = array(
    'href' => 'hw_custom_code.manage',
    'position' => 600
);

return $schema;
