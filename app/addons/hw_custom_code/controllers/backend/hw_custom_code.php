<?php
/**
 *
 * Thank you for your purchase! You are the best!
 *
 * @copyright    (C) 2017 Hungryweb
 * @website      https://www.hungryweb.net/
 * @support      support@hungryweb.net
 * @license      https://www.hungryweb.net/license-agreement.html
 *
 * ---------------------------------------------------------------------------------
 * This is a commercial software, only users who have purchased a valid license
 * and accepts the terms of the License Agreement can install and use this program.
 * ---------------------------------------------------------------------------------
 *
 */

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Languages\Languages;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    fn_trusted_vars('custom_code');

    if ($mode == 'update') {
    	$exists = (int)db_get_field('SELECT COUNT(*) FROM ?:common_descriptions WHERE object_holder=?s', 'custom_code');
    	if($exists==0){
    	             //add to all languages
	            $_data = array();
	            $_data['object_holder'] =	'custom_code';
	           $i=0;
	            foreach ($_REQUEST['custom_code'] as $object => $description) {
	            		$i++;
			$_data['object_id'] = $i;
			$_data['object'] = $object;
			$_data['description'] = $description;

			foreach (fn_get_translation_languages() as $_data['lang_code'] => $_v) {
				db_query("INSERT INTO ?:common_descriptions ?e", $_data);
			}
	            }	            		
    	}else{
    		//update
	               foreach ($_REQUEST['custom_code'] as $object => $description) { 					
			$where = db_quote(' AND object_holder = ?s', 'custom_code');
			$where .= db_quote(' AND object = ?s', $object);
			if($_REQUEST['custom_code']['all_languages'] == 'N' && $object != 'all_languages'){ 
				$where .= db_quote(' AND lang_code = ?s', DESCR_SL); 
			}

    			db_query("UPDATE ?:common_descriptions SET description=?s WHERE 1 ?p",  $description, $where); 
    		}

    	}

    }

    return array(CONTROLLER_STATUS_OK, "hw_custom_code.manage");
}

if ($mode == 'manage') {
	$custom_code = fn_hw_custom_code_get_data();
	Tygh::$app['view']->assign('custom_code', $custom_code);
	/*
	$tabs = array();
	$tabs['hw_cc_head'] = array(
		'title' => __('hw_cc_head'),
		'js' => true
	);
	$tabs['hw_cc_styles'] = array(
		'title' => __('hw_cc_styles'),
		'js' => true
	);
	$tabs['hw_cc_scripts'] = array(
		'title' => __('hw_cc_scripts'),
		'js' => true
	);
	$tabs['hw_cc_footer'] = array(
		'title' => __('hw_cc_footer'),
		'js' => true
	);

	Registry::set('navigation.tabs',$tabs);*/
}