<?php
/**
 *
 * Thank you for your purchase! You are the best!
 *
 * @copyright    (C) 2017 Hungryweb
 * @website      https://www.hungryweb.net/
 * @support      support@hungryweb.net
 * @license      https://www.hungryweb.net/license-agreement.html
 *
 * ---------------------------------------------------------------------------------
 * This is a commercial software, only users who have purchased a valid license
 * and accepts the terms of the License Agreement can install and use this program.
 * ---------------------------------------------------------------------------------
 *
 */

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

use Tygh\Registry;

$custom_code = fn_hw_custom_code_get_data();
Tygh::$app['view']->assign('custom_code', $custom_code);