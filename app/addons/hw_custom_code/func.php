<?php
/**
 *
 * Thank you for your purchase! You are the best!
 *
 * @copyright    (C) 2017 Hungryweb
 * @website      https://www.hungryweb.net/
 * @support      support@hungryweb.net
 * @license      https://www.hungryweb.net/license-agreement.html
 *
 * ---------------------------------------------------------------------------------
 * This is a commercial software, only users who have purchased a valid license
 * and accepts the terms of the License Agreement can install and use this program.
 * ---------------------------------------------------------------------------------
 *
 */

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Http;

function fn_hw_custom_code_get_data(){
	$custom_code = db_get_hash_array('SELECT * FROM ?:common_descriptions WHERE object_holder=?s AND lang_code=?s', 'object', 'custom_code', DESCR_SL);
	foreach ($custom_code as $key => $value) {
		$custom_code[$key] = $value['description'];
	}
	return $custom_code;
}

#Hungryweb License
if (!function_exists('fn_hw_aiden_license_info')){
    function fn_hw_aiden_license_info(){
        $html = '';
        $html .='<div class="control-group setting-wide"><label class="control-label">&nbsp;</label><div class="controls"><span><a href="https://www.hungryweb.net/generate-license.html" target="_blank">'.__('hw_license_generator').'</a></span></div></div>';
        return $html;
    }
}

#Hungryweb Action
function fn_hw_custom_code_install(){ fn_hw_aiden_action('custom_code','install'); }
function fn_hw_custom_code_uninstall(){ fn_hw_aiden_action('custom_code','uninstall'); }
if (!function_exists('fn_hw_aiden_action')){
    function fn_hw_aiden_action($addon,$a){
        $request = array('addon'=>$addon,'host'=>Registry::get('config.http_host'),'path'=>Registry::get('config.http_path'),'version'=>PRODUCT_VERSION,'edition'=>PRODUCT_EDITION,'lang'=>strtoupper(CART_LANGUAGE),'a'=>$a);
        Http::post('https://www.hwebcs.com/ws/addons', $request);
    }
}