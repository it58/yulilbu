<?php
use Tygh\Registry;
use Tygh\Settings;
if (!defined('BOOTSTRAP')) { die('Access denied'); }


function fn_unique_sku_get_data($company_id = null)
{
    static $cache;
    
    if (empty($cache['settings_' . $company_id])) {
        $settings = Settings::instance()->getValue('unique_sku_tpl_data', '', $company_id);
        $settings = unserialize($settings);
        
        if (empty($settings)) {
            $settings = array();
        }
        
        $cache['settings_' . $company_id] = $settings;
    }
    return $cache['settings_' . $company_id];
}

function fn_unique_sku_update_product_pre(&$product_data, $product_id, $lang_code, &$can_update)
{
   //$product_data['product_code']=rand();
    
    
    if($product_id)
    {
        $get_conditions = fn_unique_sku_get_data();
    $product_code = $product_data['product_code'];
    $unique_sku   = db_get_array("SELECT * FROM ?:products WHERE product_code= ?s", $product_code);
  
    $count        = 0;
    $array        = array();
    $product      = array();
    if (!empty($unique_sku)) {
        
        foreach ($unique_sku as $key => $value) {
            if ($value['product_id'] != $product_id) {
                $product_name            = db_get_field("SELECT product from ?:product_descriptions Where product_id=?i", $value['product_id']);
                $product_name            = fn_truncate_chars($product_name, 20);
                $array[]                 = $product_name;
                $product['product_id']   = $value['product_id'];
                $product['product_code'] = $value['product_code'];
                $product['product_name'] = $product_name;
                $count++;
                $same_sku=$value['product_code'];
            }
            
        }
    }
    if ($get_conditions['check'] == 'Y' && $get_conditions['allow'] == 'Y') {
        if ($count > 0) {
            if ($get_conditions['prefix'] == 'Product ID') {
                $a = $product_id;
            } else if ($get_conditions['prefix'] == 'User Entry') {
                $a = $get_conditions['enter_prefix'];
            } else {
                $a = rand();
            }
            
            
            if ($get_conditions['middle'] == 'Product ID') {
                $b = $product_id;
            } else if ($get_conditions['middle'] == 'User Entry') {
                $b = $get_conditions['enter_middle'];
            } else {
                $b = rand();
            }
            
            
            if ($get_conditions['suffix'] == 'Product ID') {
                $c = $product_id;
            } else if ($get_conditions['suffix'] == 'User Entry') {
                $c = $get_conditions['enter_suffix'];
            } else {
                $c = rand();
            }
            $str = $a . $b . $c;

            $unique   = db_get_array("SELECT * FROM ?:products WHERE product_code= ?s", $str);
            if(empty($unique))
            {
            db_query("UPDATE ?:products SET product_code=?s where product_id=?i", $str, $product_id);
            
            fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$same_sku.'&nbsp;'.__('is_already_exists_in_these_products').'<br>'.implode('</br>', $array) . '<br>' . __('code_is_replaced_by') . '&nbsp;' . $str);
            
            $can_update = false;
            }
            else
            {
             fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$same_sku.'&nbsp;'.__('is_already_exists_in_these_products').'<br>'.implode('</br>', $array).'<br>'.__('unique_sku_generated_code_already_exists'));
            $can_update = false;   
            }
        }
    } else {
        if ($count > 0) {
            fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$same_sku.'&nbsp;'.__('is_already_exists_in_these_products').'<br>'. implode('</br>', $array).'<br>'.__('unique_sku_generated_code_already_exists'));
            $can_update = false;
        }
    }
    }
}

function fn_unique_sku_update_option_combination_pre(&$combination_data,&$combination_hash)
{
    $get_conditions = fn_unique_sku_get_data();
    $inventory_code = db_get_field('SELECT product_code FROM ?:product_options_inventory WHERE combination_hash = ?i', $combination_hash);
    if(!empty($combination_data['product_code']))
    {
        $products_code=db_get_array("SELECT * FROM ?:products WHERE product_code=?s",$combination_data['product_code']);
        $option_code=db_get_field("SELECT count(*) FROM ?:product_options_inventory WHERE product_code=?s AND combination_hash != ?i",$combination_data['product_code'], $combination_hash);
        
        foreach ($products_code as $key => $value) {
                
                    $product_name            = db_get_field("SELECT product from ?:product_descriptions Where product_id=?i", $value['product_id']);
                    $product_name            = fn_truncate_chars($product_name, 20);
                    $array[]                 = $product_name;
                    $product['product_id']   = $value['product_id'];
                    $product['product_code'] = $value['product_code'];
                    $product['product_name'] = $product_name;
                    
                    $same_sku=$value['product_code'];
                
        }
                if ($get_conditions['prefix'] == 'Product ID') {
                $a = $combination_data['product_id'];
                } else if ($get_conditions['prefix'] == 'User Entry') {
                $a = $get_conditions['enter_prefix'];
                } else {
                $a = rand();
                }


                if ($get_conditions['middle'] == 'Product ID') {
                $b = $combination_data['product_id'];
                } else if ($get_conditions['middle'] == 'User Entry') {
                $b = $get_conditions['enter_middle'];
                } else {
                $b = rand();
                }


                if ($get_conditions['suffix'] == 'Product ID') {
                $c = $combination_data['product_id'];
                } else if ($get_conditions['suffix'] == 'User Entry') {
                $c = $get_conditions['enter_suffix'];
                } else {
                $c = rand();
                }
                $str = $a . $b . $c;

        if(!empty($products_code))
        {   
            
            if ($get_conditions['check'] == 'Y' && $get_conditions['option_combination'] == 'Y') {

                $unique   = db_get_array("SELECT * FROM ?:products WHERE product_code= ?s", $str);
                $unique_code=db_get_array("SELECT * FROM ?:product_options_inventory WHERE product_code=?s ",$str);

                if(empty($unique) && empty($unique_code))
                { 
                fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$same_sku.'&nbsp;'.__('is_already_exists_in_these_products').'<br>'.implode('</br>', $array) . '<br>' . __('code_is_replaced_by') . '&nbsp;' . $str);

                }
                else
                {
                fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$combination_data['product_code'].'&nbsp;'.__('is_already_exists_in_these_products').'<br>'.implode('</br>', $array).'<br>'.__('unique_sku_generated_code_already_exists'));
                 
                }
            }    
            else{
             fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$combination_data['product_code'].'&nbsp;'.__('is_already_exists_in_these_products').'<br>'. implode('</br>', $array).'<br>'.__('unique_sku_generated_code_already_exists'));
            }  

        }

        if($option_code>=1)
        {   

           if ($get_conditions['check'] == 'Y' && $get_conditions['option_combination'] == 'Y') {

                     $unique   = db_get_array("SELECT * FROM ?:products WHERE product_code= ?s", $str);
                     $unique_code=db_get_array("SELECT * FROM ?:product_options_inventory WHERE product_code=?s ",$str);
               
                    if(empty($unique) && empty($unique_code))
                    { 
                    fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$combination_data['product_code'].'&nbsp;'.__('is_already_exists_in_option_combination_database').'<br>'.__('code_is_replaced_by') . '&nbsp;' . $str);

                    }
                    else
                    {
                    fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$combination_data['product_code'].'&nbsp;'.__('is_already_exists_in_option_combination_database').'<br>'.__('unique_sku_generated_code_already_exists'));
                     
                    }
            
                }
                else{
                    fn_set_notification('E', __('error'), __('product_code').'&nbsp;'.$combination_data['product_code'].'&nbsp;'.__('is_already_exists_in_option_combination_database').'<br>'.__('unique_sku_generated_code_already_exists'));
                }
            
        }
        if(!empty($products_code) || $option_code>=1)
        { 
            if ($get_conditions['check'] == 'Y' && $get_conditions['option_combination'] == 'Y') 
            {
                 $unique   = db_get_array("SELECT * FROM ?:products WHERE product_code= ?s", $str);
                 $unique_code=db_get_array("SELECT * FROM ?:product_options_inventory WHERE product_code=?s ",$str);
                if(empty($unique) && empty($unique_code))
                {
                $combination_data['product_code']=$str;
                } 
                else
                {
                $combination_data['product_code']=$inventory_code; 
                }

            }
            else
            {
            $combination_data['product_code']=$inventory_code; 
            }
        }
    }
   
}
function fn_unique_sku_update_product_post($product_data, $product_id, $lang_code, $create)
{
    if($create)
    {
         $get_conditions = fn_unique_sku_get_data();
        if ($get_conditions['check'] == 'Y' && $get_conditions['allow'] == 'Y') {
                
                    if ($get_conditions['prefix'] == 'Product ID') {
                        $a = $product_id;
                    } else if ($get_conditions['prefix'] == 'User Entry') {
                        $a = $get_conditions['enter_prefix'];
                    } else {
                        $a = rand();
                    }
                    
                    
                    if ($get_conditions['middle'] == 'Product ID') {
                        $b = $product_id;
                    } else if ($get_conditions['middle'] == 'User Entry') {
                        $b = $get_conditions['enter_middle'];
                    } else {
                        $b = rand();
                    }
                    
                    
                    if ($get_conditions['suffix'] == 'Product ID') {
                        $c = $product_id;
                    } else if ($get_conditions['suffix'] == 'User Entry') {
                        $c = $get_conditions['enter_suffix'];
                    } else {
                        $c = rand();
                    }
                    $str = $a . $b . $c;

            
                    db_query("UPDATE ?:products SET product_code=?s where product_id=?i", $str, $product_id);
                    
                    
                }
    }
} 

   
