<?php

use Tygh\Embedded;
use Tygh\Http;
use Tygh\Mailer;
use Tygh\Pdf;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Session;
use Tygh\Settings;
use Tygh\Shippings\Shippings;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'update' || $mode == 'show_existing' && $_REQUEST['addon'] == 'unique_sku' && (!empty($_REQUEST['order_cancelation_data']))) {
       
        fn_unique_sku_update_data($_REQUEST['order_cancelation_data']);
        
    }
}
if ($mode == 'show_existing') {
    if ($_REQUEST['addon'] == 'unique_sku') {
        Registry::get('view')->assign('order_cancelation_data', fn_unique_sku_get_data());
    }
    
    if (isset($_REQUEST['order_cancelation_data']) && isset($_REQUEST['order_cancelation_data']['prefix']) && isset($_REQUEST['order_cancelation_data']['middle']) && isset($_REQUEST['order_cancelation_data']['suffix'])) {
        if (isset($_REQUEST['order_cancelation_data']['enter_prefix']) && $_REQUEST['order_cancelation_data']['prefix'] == 'User Entry' || isset($_REQUEST['order_cancelation_data']['enter_middle']) && $_REQUEST['order_cancelation_data']['middle'] == 'User Entry' || isset($_REQUEST['order_cancelation_data']['enter_suffix']) && $_REQUEST['order_cancelation_data']['suffix'] == 'User Entry') {
            return array(
                CONTROLLER_STATUS_REDIRECT,
                'addons.update?addon=unique_sku&show_existing=Y&check='.$_REQUEST['order_cancelation_data']['check'].'&prefix=' . $_REQUEST['order_cancelation_data']['prefix'] . '&middle=' . $_REQUEST['order_cancelation_data']['middle'] . '&suffix=' . $_REQUEST['order_cancelation_data']['suffix'] . '&enter_prefix=' . $_REQUEST['order_cancelation_data']['enter_prefix'] . '&enter_middle=' . $_REQUEST['order_cancelation_data']['enter_middle'] . '&enter_suffix=' . $_REQUEST['order_cancelation_data']['enter_suffix']
            );
        } else {
            return array(
                CONTROLLER_STATUS_REDIRECT,
                'addons.update?addon=unique_sku&show_existing=Y&check='.$_REQUEST['order_cancelation_data']['check'].'&prefix=' . $_REQUEST['order_cancelation_data']['prefix'] . '&middle=' . $_REQUEST['order_cancelation_data']['middle'] . '&suffix=' . $_REQUEST['order_cancelation_data']['suffix']
            );
        }
        
    } else if (isset($_REQUEST['addon']) && $_REQUEST['addon'] == 'unique_sku') {
        
        return array(
            CONTROLLER_STATUS_REDIRECT,
            'addons.update?addon=unique_sku&show_existing=Y'
        );
        
    }
    
    
    
}
if ($mode == 'update') {

    if (isset($_REQUEST['addon']) && $_REQUEST['addon'] == 'unique_sku') {
         Registry::get('view')->assign('order_cancelation_data', fn_unique_sku_get_data());
        
        if (isset($_REQUEST['product_data'])) {
           
            $params = $_REQUEST['product_data'];
            $abc=array();
            $count=0;
            $ch=0;
            $exist=array();
            foreach ($params as $key => $value) {

                $unique_sku = db_get_array("SELECT * FROM ?:products WHERE product_code= ?s",$value['new_sku']);
              

                if(!empty($unique_sku))
                {
                    $exist[$count]=$value['new_sku'];
                    $count++;
                    
                    
                }
               
                $exist_code=$value['new_sku'];
                if(!empty($value['new_sku']))
                {
                    $abc[]=$value['new_sku'];
                }
            }
           
           
            

            if(count(array_unique($abc))<count($abc))
            {              
                $allow_update='N';
            }
            else
            {
                
              $allow_update='Y';
                
            }
            if($allow_update=='Y')
            {           

                foreach ($params as $key => $value) {
                    
                    $code = $value['new_sku'];
                    if (!empty($code)) {
                        db_query("UPDATE ?:products SET product_code=?s where product_id=?i", $code, $key);
                        $save = 'Y';
                    } else {
                        $save = 'N';
                    }
                }
            }
            else{

                if($count>0)
                {           

                    fn_set_notification('W', __('warning'), __('product_code').'&nbsp;'.$exist_code.'&nbsp;'.__('already_exists_in_database_try_different_code'));

                    
                }
                else
                {   
                    fn_set_notification('W', __('warning'), __('same_code_for_different_products_not_allowed_try_different_code'));
                        
                    
                }
            }
           
        }
        
        
        if (isset($_REQUEST['show_existing']) && $_REQUEST['show_existing'] == 'Y') {
            
            
            
            $product  = db_get_array('SELECT product_id, product_code FROM ?:products WHERE product_code IN (SELECT product_code FROM ?:products GROUP BY product_code HAVING COUNT( * ) >1 )');
            $products = array();
            
            foreach ($product as $key => $value) {
                $p_name                         = db_get_field("SELECT product FROM ?:product_descriptions where product_id=?i", $value['product_id']);
                $products[$key]['product_id']   = $value['product_id'];
                $products[$key]['product_code'] = $value['product_code'];
                $products[$key]['product_name'] = $p_name;
                
                if (isset($_REQUEST['check']) && isset($_REQUEST['prefix']) && isset($_REQUEST['middle']) && isset($_REQUEST['suffix'])) {
                    $products[$key]['prefix'] = (($_REQUEST['prefix'] == 'Random') ? rand() : (($_REQUEST['prefix'] == 'User Entry') ? $_REQUEST['enter_prefix'] : (($_REQUEST['prefix'] == 'Product ID') ? $value['product_id'] : "")));

                    $products[$key]['middle'] = (($_REQUEST['middle'] == 'Random') ? rand() : (($_REQUEST['middle'] == 'User Entry') ? $_REQUEST['enter_middle'] : (($_REQUEST['middle'] == 'Product ID') ? $value['product_id'] : "")));

                    $products[$key]['suffix'] = (($_REQUEST['suffix'] == 'Random') ? rand() : (($_REQUEST['suffix'] == 'User Entry') ? $_REQUEST['enter_suffix'] : (($_REQUEST['suffix'] == 'Product ID') ? $value['product_id'] : "")));
                }
                
                
            }
            
            Registry::get('view')->assign('products', $products);
        }
    }
}

function fn_unique_sku_update_data($order_cancelation_data, $company_id = null)
{
    if (!$setting_id = Settings::instance()->getId('unique_sku_tpl_data', '')) {
        $setting_id = Settings::instance()->update(array(
            'name' => 'unique_sku_tpl_data',
            'section_id' => 0,
            'section_tab_id' => 0,
            'type' => 'A', // any not existing type
            'position' => 0,
            'is_global' => 'N',
            'handler' => ''
        ));
    }
    
    Settings::instance()->updateValueById($setting_id, serialize($order_cancelation_data), $company_id);
}
