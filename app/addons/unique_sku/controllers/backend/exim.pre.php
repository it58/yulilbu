<?php

use Tygh\Bootstrap;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Tools\Url;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
     if ($mode == 'import') {
        $file = fn_filter_uploaded_data('csv_file');
        if (!empty($file)) {
            if (empty($_REQUEST['pattern_id'])) {
                fn_set_notification('E', __('error'), __('error_exim_pattern_not_found'));
            } else {
                $pattern = fn_unique_sku_get_pattern_definition($_REQUEST['pattern_id'], 'import');
                $existing_product;
                $exist=array();
                $count=0;
                $p_name=array();
                if (($data = fn_unique_sku_get_csv($pattern, $file[0]['path'], $_REQUEST['import_options'])) != false) {
                    foreach ($data as $key => $value) {
                        $existing_product=db_get_field('SELECT count(*) from ?:products where product_code=?s',$value['Product code']);
                        if($existing_product>1)
                        {
                            
                            $count++;
                            $exist[]=$value['Product code'];
                        }
                    }

                    if($count>=1)
                    {
                        fn_set_notification('E', __('error'), __('these_product_codes_already_exist_in_database').'<br>'.implode('<br>',$exist));
                        exit;
                    }
                    
                }
            }
        }
    }
}

function fn_unique_sku_import($pattern, $import_data, $options)
{
   
   if (empty($pattern) || empty($import_data)) {
        return false;
    }

    $processed_data = array (
        'E' => 0, // existent
        'N' => 0, // new
        'S' => 0, // skipped
    );

    $alt_keys = array();
    $primary_fields = array();
    $table_groups = array();
    $default_groups = array();
    $add_fields = array();
    $primary_object_ids = array();
    $required_fields = array();
    $alt_fields = array();

    if (!empty($pattern['pre_processing'])) {
        $data_pre_processing = array(
            'import_data' => &$import_data,
            'pattern' => &$pattern,
        );
        fn_unique_sku_exim_processing('import', $pattern['pre_processing'], $options, $data_pre_processing);
    }

    if (!empty($pattern['references'])) {
        $table_groups =  $pattern['references'];
    }

    if (!fn_unique_sku_import_parse_languages($pattern, $import_data, $options)) {
        fn_set_notification('E', __('error'), __('error_exim_invalid_count_langs'));

        return false;
    }

    foreach ($pattern['export_fields'] as $field => $data) {

        $_db_field = (empty($data['db_field']) ? $field : $data['db_field']);

        // Collect fields with default values
        if (isset($data['default'])) {
            if (is_array($data['default'])) {
                $default_groups[$_db_field] = call_user_func_array(array_shift($data['default']), $data['default']);
            } else {
                $default_groups[$_db_field] = $data['default'];
            }
        }

        // Get alt keys for primary table
        if (!empty($data['alt_key'])) {
            $alt_keys[$field] = $_db_field;
        }

        if (!empty($data['alt_field'])) {
            $alt_fields[$_db_field] = $data['alt_field'];
        }

        if (!empty($data['required']) && $data['required'] = true) {
            $required_fields[] = $_db_field;
        }

        if (!isset($data['linked']) || $data['linked'] == true) {
            // Get fields for primary table
            if (empty($data['table']) || $data['table'] == $pattern['table']) {
                $primary_fields[$field] = $_db_field;
            }

            // Group fields by tables
            if (!empty($data['table'])) {
                $table_groups[$data['table']]['fields'][$_db_field] = true;
            }
        }

        // Create set with fields that must be added to data import if they are not exist
        // %'s are for compatibility with %% field type in "process_put" key
        if (!empty($data['use_put_from'])) {
            $_f = str_replace('%', '', $data['use_put_from']);
            $_f = !empty($pattern['export_fields'][$_f]['db_field']) ? $pattern['export_fields'][$_f]['db_field'] : $_f;
            $add_fields[$_f][] = $_db_field;
        }
    }


 $processing_groups = fn_unique_sku_import_build_groups('process_put', $pattern['export_fields']);

    // Generate converting groups
    $converting_groups = fn_unique_sku_import_build_groups('convert_put', $pattern['export_fields']);

    //Generate pre inserting groups
    $pre_inserting_groups = fn_unique_sku_import_build_groups('pre_insert', $pattern['export_fields']);

    //Generate post inserting groups
    $post_inserting_groups = fn_unique_sku_import_build_groups('post_insert', $pattern['export_fields']);

    fn_set_progress('parts', sizeof($import_data));

     $data = reset($import_data);
    $multi_lang = array_keys($data);
    $main_lang = reset($multi_lang);

    foreach ($import_data as $k => $v) {

        //If the required field is empty skip this record
        $_alt_keys = array();
        $object_exists = true;

        if (!(isset($pattern['import_skip_db_processing']) && $pattern['import_skip_db_processing'])) {

            fn_set_progress('echo', __('importing_data'));
            if (empty($primary_object_id)) {

                // If scheme is used for update objects only, skip this record
                if (!empty($pattern['update_only'])) {
                    $_a = array();
                    foreach ($alt_keys as $_d => $_v) {
                        if (!isset($v[$main_lang][$_v])) {
                            continue;
                        }
                        $_a[] = $_d . ' = ' . $v[$main_lang][$_v];
                    }
                    fn_set_progress('echo', __('object_does_not_exist') . ' (' . implode(', ', $_a) . ')...', false);

                    $processed_data['S']++;
                    continue;
                }

                $object_exists = false;
                $processed_data['N']++;

                // For new objects - fill the default values
                if (!empty($default_groups)) {
                    foreach ($default_groups as $field => $value) {
                        if (empty($v[$main_lang][$field])) {
                            $v[$main_lang][$field] = $value;
                        }
                    }
                }
            } else {
                $processed_data['E']++;
            }

            /*foreach ($pattern['key'] as $field) {
                unset($v[$main_lang][$field]);
            }*/
            if ($object_exists == true) {
                fn_set_progress('echo', __('updating') . ' ' . $pattern['name'] . ' <b>' . implode(',', $primary_object_id) . '</b>. ', false);
                db_query('UPDATE ?:' . $pattern['table'] . ' SET ?u WHERE ?w', $v[$main_lang], $primary_object_id);
            } else {
                
                 $o_id = db_query('INSERT INTO ?:' . $pattern['table'] . ' ?e', $v[$main_lang]);
                if ($o_id !== true) {
                    $primary_object_id = array(reset($pattern['key']) => $o_id);
                } else {
                    foreach ($pattern['key'] as $_v) {
                        $primary_object_id[$_v] = $v[$main_lang][$_v];
                    }
                }

                fn_set_progress('echo', __('creating') . ' ' . $pattern['name'] . ' <b>' . implode(',', $primary_object_id) . '</b>. ', false);

            }
        }
    }

}

function fn_unique_sku_get_pattern_definition($pattern_id, $get_for = '')
{
    // First, check basic patterns
    $schema = fn_get_schema('exim', $pattern_id);

    if (empty($schema)) {
        fn_set_notification('E', __('error'), __('error_exim_pattern_not_found'));

        return false;
    }

    if ((!empty($schema['export_only']) && $get_for == 'import') || (!empty($schema['import_only']) && $get_for == 'export')) {
        return array();
    }

    $has_alt_keys = false;

    foreach ($schema['export_fields'] as $field_id => $field_data) {
        if (!empty($field_data['table'])) {
            // Table exists in export fields, but doesn't exist in references definition
            if (empty($schema['references'][$field_data['table']])) {
                fn_set_notification('E', __('error'), __('error_exim_pattern_definition_references'));

                return false;
            }
        }

        // Check if schema has alternative keys to import basic data
        if (!empty($field_data['alt_key'])) {
            $has_alt_keys = true;
        }

        if ((!empty($field_data['export_only']) && $get_for == 'import') || (!empty($field_data['import_only']) && $get_for == 'export')) {
            unset($schema['export_fields'][$field_id]);
        }
    }

    if ($has_alt_keys == false) {
        fn_set_notification('E', __('error'), __('error_exim_pattern_definition_alt_keys'));

        return false;
    }

    return $schema;
}

function fn_unique_sku_get_csv($pattern, $file, $options)
{
    $max_line_size = 65536; // 64 Кб
    $result = array();

    if ($options['delimiter'] == 'C') {
        $delimiter = ',';
    } elseif ($options['delimiter'] == 'T') {
        $delimiter = "\t";
    } else {
        $delimiter = ';';
    }

    if (!empty($file) && file_exists($file)) {

        $encoding = fn_detect_encoding($file, 'F', !empty($options['lang_code']) ? $options['lang_code'] : CART_LANGUAGE);

        if (!empty($encoding)) {
             $file = fn_convert_encoding($encoding, 'UTF-8', $file, 'F');
        } else {
            fn_set_notification('W', __('warning'), __('text_exim_utf8_file_format'));
        }

        $f = false;
        if ($file !== false) {
            $f = fopen($file, 'rb');
        }

        if ($f) {
            // Get import schema
            $import_schema = fgetcsv($f, $max_line_size, $delimiter);
            if (empty($import_schema)) {
                fn_set_notification('E', __('error'), __('error_exim_cant_read_file'));

                return false;
            }

            // Check if we selected correct delimiter
            // If line was read without delimition, array size will be == 1.
            if (sizeof($import_schema) == 1) {

                // we could export one column if it is correct, otherwise show error
                if (!in_array($import_schema[0], array_keys($pattern['export_fields']))) {

                    fn_set_notification('E', __('error'), __('error_exim_incorrent_delimiter'));

                    return false;
                }
            }

            // Analyze schema - check for required fields
            

            // Collect data
            $schema_size = sizeof($import_schema);
            $skipped_lines = array();
            $line_it = 1;
            while (($data = fn_fgetcsv($f, $max_line_size, $delimiter)) !== false) {

                $line_it ++;
                if (fn_is_empty($data)) {
                    continue;
                }

                if (sizeof($data) != $schema_size) {
                    $skipped_lines[] = $line_it;
                    continue;
                }

                $result[] = array_combine($import_schema, Bootstrap::stripSlashes($data));
            }

            if (!empty($skipped_lines)) {
                fn_set_notification('W', __('warning'), __('error_exim_incorrect_lines', array(
                    '[lines]' => implode(', ', $skipped_lines)
                )));
            }

            return $result;
        } else {
            fn_set_notification('E', __('error'), __('error_exim_cant_open_file'));

            return false;
        }
    } else {
        fn_set_notification('E', __('error'), __('error_exim_file_doesnt_exist'));

        return false;
    }
}

function fn_unique_sku_import_prepare_groups(&$data, $groups, $options, $skip_record = false)
{
    if (!empty($groups)) {
        foreach ($groups as $group) {
            if (!isset($data[$group['this_field']])) {
                continue;
            }
            $vars = array(
                'lang_code' => !empty($data['lang_code']) ? $data['lang_code'] : '',
                'field' => $group['this_field']
            );

            $params = fn_exim_get_values($group['args'], array(), $options, $vars, $data, '');
            $params[] = & $skip_record;

            $data[$group['this_field']] = call_user_func_array($group['function'], $params);
        }
    }

    return true;
}

function fn_unique_sku_exim_processing($type_processing, $processing, $options, $vars = array())
{
    $result = true;

    foreach ($processing as $data) {
        if ((!empty($data['import_only']) && $type_processing == 'export') || (!empty($data['export_only']) && $type_processing == 'import')) {
            continue;
        }

        $args = fn_unique_sku_exim_get_values($data['args'], array(), $options, array(), $vars, '');
        $result = call_user_func_array($data['function'], $args) && $result;
    }

    return $result;
}

function fn_unique_sku_exim_get_values($values, $pattern, $options, $vars = array(), $data = array(), $quote = "'")
{
    $val = array();

    foreach ($values as $field => $value) {

        if (is_array($value)) {
            $val[$field] = fn_unique_sku_exim_set_quotes($value);

        } else {
            $operator = substr($value, 0, 1);

            if ($operator === '@') {
                $opt = str_replace('@', '', $value);
                $val[$field] = (isset($options[$opt])) ? fn_unique_sku_exim_set_quotes($options[$opt], $quote) : '';

            } elseif ($value === '#this') {
                if (!empty($vars['field'])) {
                    $val[$field] = fn_unique_sku_exim_set_quotes($data[$vars['field']], $quote);
                } else {
                    $val[$field] = fn_unique_sku_exim_set_quotes($data[$field], $quote);
                }

            } elseif ($value === '#key') {
                $val[$field] = (sizeof($vars['key']) == 1) ? reset($vars['key']) : (isset($vars['key'][$field]) ? $vars['key'][$field] : $vars['key']);

            } elseif ($operator === '&') {
                $val[$field] = $pattern['table'] . '.' . substr($value, 1);

            } elseif ($value === '#field') {
                if (!empty($vars['field'])) {
                    $val[$field] = $vars['field'];
                } else {
                    $val[$field] = $field;
                }

            } elseif ($value === '#lang_code') {
                $val[$field] = !empty($vars['lang_code']) ? fn_unique_sku_exim_set_quotes($vars['lang_code'], $quote) : '';

            } elseif ($value === '#row') {
                $val[$field] = $data;

            } elseif ($operator === '#') {
                $val[$field] = substr($value, 1);

            } elseif ($operator === '$') {
                $opt = str_replace('$', '', $value);
                if (isset($data[$opt])) {
                    $data[$opt] = fn_unique_sku_exim_set_quotes($data[$opt], $quote);
                    $val[$field] = &$data[$opt];
                } else {
                    $val[$field] = '';
                }

            } else {
                $val[$field] = fn_unique_sku_exim_set_quotes($value, $quote);
            }
        }
    }

    return $val;
}

function fn_unique_sku_exim_set_quotes($value, $quote = "'")
{
    if (is_array($value) && !empty($value)) {
        foreach ($value as $k => $v) {
            $values[$k] = fn_unique_sku_exim_set_quotes($v, $quote);
        }
        $result = $values;
    } elseif (gettype($value) == 'string') {
        $result = $quote . $value . $quote;

    } else {
        $result = $value;
    }

    return $result;
}

function fn_unique_sku_import_build_groups($type_group, $export_fields)
{
    $groups = array();
    if (!empty($type_group)) {
        foreach ($export_fields as $field => $data) {
            $db_field = (empty($data['db_field']) ? $field : $data['db_field']);
            if (!empty($data[$type_group])) {
                $args = $data[$type_group];
                $function = array_shift($args);
                $groups[] = array (
                    'function' => $function,
                    'this_field' => $db_field,
                    'args' => $args,
                    'table' => !empty($data['table']) ? $data['table'] : '',
                    'multilang' => !empty($data['multilang']) ? true : false,
                    'return_result' => !empty($data['return_result']) ? $data['return_result'] : false,
                );
            }
        }
    }

    return $groups;
}

function fn_unique_sku_import_parse_languages($pattern, &$import_data, $options)
{

    foreach ($pattern['export_fields'] as $field_name => $field) {
        if (!empty($field['type']) && $field['type'] == 'languages') {
            if (empty($field['db_field'])) {
                $field_lang = $field_name;
            } else {
                $field_lang = $field['db_field'];
            }
        }
    }

    // Languages
    $langs = array();

    // Get all lang from data
    foreach ($import_data as $k => $v) {
        if (!isset($v['lang_code']) || in_array($v['lang_code'], $langs)) {
            break;
        }
        $langs[] = $v['lang_code'];
    }

    if (empty($langs)) {
        foreach ($import_data as $key => $data) {
            $import_data[$key]['lang_code'] = DEFAULT_LANGUAGE;
        }

        $langs[] = DEFAULT_LANGUAGE;
    }

    $langs = array_intersect($langs, array_keys(fn_get_translation_languages()));
    $count_langs = count($langs);

    $count_lang_data = array();
    foreach ($langs as $lang) {
        $count_lang_data[$lang] = 0;
    }

    $data = array();
    $result = true;
    if (isset($field_lang)) {
        foreach ($import_data as $v) {
            if (!empty($v[$field_lang]) && in_array($v[$field_lang], $langs)) {
                $data[] = $v;
                $count_lang_data[$v[$field_lang]]++;
            }
        }

        // Check
        $count_data = reset($count_lang_data);
        foreach ($langs as $lang) {
            if ($count_lang_data[$lang] != $count_data) {
                $result = false;
                break;
            }
        }

        if ($result) {
            // Chunk on languages
            $data_lang = array_chunk($data, $count_langs);
            $data = array();

            foreach ($data_lang as $data_key => $data_value) {
                foreach ($data_value as $v) {
                    $data[$data_key][$v[$field_lang]] = $v;
                }
            }

            if (fn_allowed_for('ULTIMATE')) {
                foreach ($data as $data_key => $data_value) {
                    $data_main = array_shift($data_value);
                    if (empty($data_main['company'])) {
                        $data_main['company'] = Registry::get('runtime.company_data.company');
                    }
                    foreach ($data_value as $v) {
                        $data[$data_key][$v[$field_lang]]['company'] = $data_main['company'];
                    }
                }
            }

            $import_data = $data;
        }
    } else {
        $main_lang = reset($langs);
        foreach ($import_data as $data_key => $data_value) {
            $data[$data_key][$main_lang] = $data_value;
        }
        $import_data = $data;
    }

    return $result;
}