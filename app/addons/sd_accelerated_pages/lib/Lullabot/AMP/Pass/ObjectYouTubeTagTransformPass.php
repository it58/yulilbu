<?php
 namespace Lullabot\AMP\Pass; use QueryPath\DOMQuery; use Lullabot\AMP\Utility\ActionTakenLine; use Lullabot\AMP\Utility\ActionTakenType; class ObjectYouTubeTagTransformPass extends BasePass { const DEFAULT_ASPECT_RATIO = 1.7778; const DEFAULT_VIDEO_WIDTH = 560; const DEFAULT_VIDEO_HEIGHT = 315; function pass() { $all_objects = $this->q->find('object:not(noscript object)'); foreach ($all_objects as $el) { $dom_el = $el->get(0); $lineno = $this->getLineNo($dom_el); $context_string = $this->getContextString($dom_el); $actionTakenType = ''; if ($this->isYouTubeObject($el)) { $youtube_code = $this->getYouTubeCode($el); if (empty($youtube_code)) { continue; } $el->after("<amp-youtube data-videoid=\"$youtube_code\" layout=\"responsive\"></amp-youtube>"); $new_el = $el->next(); $new_dom_el = $new_el->get(0); $actionTakenType = ActionTakenType::YOUTUBE_OBJECT_CONVERTED; $this->setStandardAttributesFrom($el, $new_el, self::DEFAULT_VIDEO_WIDTH, self::DEFAULT_VIDEO_HEIGHT, self::DEFAULT_ASPECT_RATIO); } else { continue; } $el->removeChildren()->remove(); $this->addActionTaken(new ActionTakenLine('object', $actionTakenType, $lineno, $context_string)); $this->context->addLineAssociation($new_dom_el, $lineno); } return $this->transformations; } protected function isYouTubeObject(DOMQuery $el) { $params = $el->find('param'); foreach ($params as $param) { if ($param->attr('name') == 'movie') { $param_value = $param->attr('value'); if (preg_match('&(*UTF8)(youtube\.com|youtu\.be)&i', $param_value)) { return true; } } } return false; } protected function getYouTubeCode(DOMQuery $el) { $matches = []; $youtube_code = ''; $params = $el->find('param'); foreach ($params as $param) { if ($param->attr('name') == 'movie') { $param_value = $param->attr('value'); $pattern = '~(?#!js YouTubeId Rev:20160125_1800)
                    # Match non-linked youtube URL in the wild. (Rev:20130823)
                    https?://          # Required scheme. Either http or https.
                    (?:[0-9A-Z-]+\.)?  # Optional subdomain.
                    (?:                # Group host alternatives.
                      youtu\.be/       # Either youtu.be,
                    | youtube          # or youtube.com or
                      (?:-nocookie)?   # youtube-nocookie.com
                      \.com            # followed by
                      \S*?             # Allow anything up to VIDEO_ID,
                      [^\w\s-]         # but char before ID is non-ID char.
                    )                  # End host alternatives.
                    ([\w-]{11})        # $1: VIDEO_ID is exactly 11 chars.
                    (?=[^\w-]|$)       # Assert next char is non-ID or EOS.
                    (?!                # Assert URL is not pre-linked.
                      [?=&+%\w.-]*     # Allow URL (query) remainder.
                      (?:              # Group pre-linked alternatives.
                        [\'"][^<>]*>   # Either inside a start tag,
                      | </a>           # or inside <a> element text contents.
                      )                # End recognized pre-linked alts.
                    )                  # End negative lookahead assertion.
                    [?=&+%\w.-]*       # Consume any URL (query) remainder.
                    ~ix'; if (preg_match($pattern, $param_value, $matches)) { if (!empty($matches[1])) { $youtube_code = $matches[1]; return $youtube_code; } } } } return $youtube_code; } } 