<?php
 namespace FastImageSize\Type; interface TypeInterface { const LONG_SIZE = 4; const SHORT_SIZE = 2; public function getSize($filename); } 