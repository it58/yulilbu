<?php
 namespace QueryPath\CSS; interface Traverser { public function find($selector); public function matches(); } 