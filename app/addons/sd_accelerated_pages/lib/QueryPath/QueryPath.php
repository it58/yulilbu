<?php
 use \Masterminds\HTML5; class QueryPath { const VERSION = '3.0.x'; const VERSION_MAJOR = 3; const HTML_STUB = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Untitled</title>
  </head>
  <body></body>
  </html>'; const HTML5_STUB = '<!DOCTYPE html>
    <html>
    <head>
    <title>Untitled</title>
    </head>
    <body></body>
    </html>'; const XHTML_STUB = '<?xml version="1.0"?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Untitled</title>
  </head>
  <body></body>
  </html>'; public static function with($document = NULL, $selector = NULL, $options = array()) { $qpClass = isset($options['QueryPath_class']) ? $options['QueryPath_class'] : '\QueryPath\DOMQuery'; $qp = new $qpClass($document, $selector, $options); return $qp; } public static function withXML($source = NULL, $selector = NULL, $options = array()) { $options += array( 'use_parser' => 'xml', ); return self::with($source, $selector, $options); } public static function withHTML($source = NULL, $selector = NULL, $options = array()) { $options += array( 'ignore_parser_warnings' => TRUE, 'convert_to_encoding' => 'ISO-8859-1', 'convert_from_encoding' => 'auto', 'use_parser' => 'html', ); return @self::with($source, $selector, $options); } public static function withHTML5($source = NULL, $selector = NULL, $options = array()) { $qpClass = isset($options['QueryPath_class']) ? $options['QueryPath_class'] : '\QueryPath\DOMQuery'; if(is_string($source)) { $html5 = new HTML5(); if (strpos($source, '<') !== FALSE && strpos($source, '>') !== FALSE) { $source = $html5->loadHTML($source); } else { $source = $html5->load($source); } } $qp = new $qpClass($source, $selector, $options); return $qp; } public static function enable($extensionNames) { if (is_array($extensionNames)) { foreach ($extensionNames as $extension) { \QueryPath\ExtensionRegistry::extend($extension); } } else { \QueryPath\ExtensionRegistry::extend($extensionNames); } } public static function enabledExtensions() { return \QueryPath\ExtensionRegistry::extensionNames(); } public static function encodeDataURL($data, $mime = 'application/octet-stream', $context = NULL) { if (is_resource($data)) { $data = stream_get_contents($data); } elseif (filter_var($data, FILTER_VALIDATE_URL)) { $data = file_get_contents($data, FALSE, $context); } $encoded = base64_encode($data); return 'data:' . $mime . ';base64,' . $encoded; } } 