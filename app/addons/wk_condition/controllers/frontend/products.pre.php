<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Enum\ProductFeatures;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'view') {
   
   $info=db_get_array("SELECT * FROM ?:product_features WHERE show_on_product =?s",'Y');
   foreach($info as $key => $item){
    $feature_id=db_get_array("SELECT feature_id FROM ?:product_features WHERE parent_id =?i",$item['feature_id']);  
   }
   
   $features_info=db_get_array("SELECT * FROM ?:product_features_values WHERE product_id =?i",$_REQUEST['product_id']);
  
   $product_features=array();
   foreach($features_info as $key=>$item){
    foreach($feature_id as $id=>$value){
        if($item['feature_id'] == $value['feature_id']){
        $variant_description=db_get_field("SELECT variant FROM ?:product_feature_variant_descriptions
        WHERE variant_id =?i",$item['variant_id']);
        $feature_description=db_get_field("SELECT description FROM ?:product_features_descriptions WHERE feature_id =?i",$item['feature_id']);  
       
      
         $product_features[$item['feature_id']]['variant']=$variant_description;
         $product_features[$item['feature_id']]['feature']=$feature_description;
        }

    }
       
   }
   Tygh::$app['view']->assign('features', $product_features);
}
