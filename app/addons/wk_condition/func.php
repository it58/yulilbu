<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;


if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_wk_condition_get_product_feature_data_post(&$feature_data){
    
    
        $show_on_product=db_get_field("SELECT show_on_product FROM ?:product_features where feature_id =?i",$feature_data['feature_id']);
        
        $feature_data['show_on_product']=$show_on_product;
   
   
} 
function fn_wk_condition_update_product_feature_pre(&$feature_data, &$feature_id, &$lang_code){
    
    

    if(isset($feature_data['show_on_product']) && $feature_data['show_on_product'] == 'Y'){
        $show_on_product=db_get_row("SELECT show_on_product,feature_id FROM ?:product_features where show_on_product =?s",'Y');
        
        if(!empty($show_on_product)){
            if($feature_id != $show_on_product['feature_id']){
            $feature_data['show_on_product']='N';
            fn_set_notification('E', __('error'), __('you_can_not_more_than'));

            }
        }
    }
      
}