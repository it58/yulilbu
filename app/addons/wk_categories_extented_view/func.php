<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_wk_categories_extented_view_install(){
    $addon_name = fn_get_lang_var('wk_categories_extented_view');
    Tygh::$app['view']->assign('mode','notification');
    fn_set_notification('S', __('well_done'), __('wk_webkul_user_guide_content', array('[support_link]' => 'https://webkul.uvdesk.com/en/customer/create-ticket/', '[user_guide]' => 'https://webkul.com/blog/cs-cart-categories_extented_view/', '[addon_name]' => $addon_name)));
}

function fn_wk_categories_extented_view_get_categories(&$params, &$join, &$condition, &$fields, &$group_by, &$sortings, &$lang_code){
    if(isset($_REQUEST['dispatch']) && $_REQUEST['dispatch'] == 'products.wk_search_category'){
        $condition .= db_quote(" AND ?:category_descriptions.category LIKE ?l", "%" . $_REQUEST['search_string'] ."%");
    }
}