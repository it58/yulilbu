<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if($mode == 'wk_get_child_categories'){
        $categories_tree = fn_get_categories(array('category_id'=>$_REQUEST['parent_category_id']), DESCR_SL);
        $id = $_REQUEST['result_ids'];
        Tygh::$app['view']->assign('wk_categories_tree',$categories_tree);
        Tygh::$app['view']->assign('wk_parent_category_id',$_REQUEST['parent_category_id']);
        Tygh::$app['view']->assign('wk_category_path','');
        $res = Tygh::$app['view']->fetch('addons/wk_categories_extented_view/common/category_tree.tpl');
        Registry::get('ajax')->assignHtml("$id",$res);
        exit;
    }
    if($mode == 'wk_search_category'){
        $categories_tree = fn_get_categories(array('category_id'=>$_REQUEST['parent_category_id']), DESCR_SL);
        $id = $_REQUEST['result_ids'];
        Tygh::$app['view']->assign('wk_categories_tree',$categories_tree);
        Tygh::$app['view']->assign('wk_parent_category_id',$_REQUEST['parent_category_id']);
        $res = Tygh::$app['view']->fetch('addons/wk_categories_extented_view/common/category_search.tpl');
        Registry::get('ajax')->assignHtml("$id",$res);
        exit;
    }
    return;
}

if($mode == 'add' || $mode == 'update'){
    $category_ids = '';
    if($mode == 'add'){
        Registry::set('navigation.tabs', array (
            'detailed' => array (
                'title' => __('general'),
                'js' => true
            ),
        ));
        $product_data = fn_restore_post_data('product_data');
        if($product_data && isset($product_data['category_ids']) && !empty($product_data['category_ids'])){
            $category_ids = $product_data['category_ids'];
        }       
    }else{
        Registry::del('navigation.tabs.images');
        Registry::del('navigation.tabs.seo');
        Registry::del('navigation.tabs.addons');
        Registry::del('navigation.tabs.qty_discounts');
        //Registry::del('navigation.tabs.tags');

        // Get current product data
        $skip_company_condition = false;
        if (fn_allowed_for('ULTIMATE') && fn_ult_is_shared_product($_REQUEST['product_id']) == 'Y') {
            $skip_company_condition = true;
        }
        $product_data = fn_get_product_data($_REQUEST['product_id'], $auth, DESCR_SL, '', true, true, true, true, false, false, $skip_company_condition);
        $category_ids = $product_data['category_ids'];
    }
    if($category_ids){
        $category_path = db_get_field("SELECT id_path FROM ?:categories WHERE category_id IN (?a)",$category_ids);
        Tygh::$app['view']->assign('wk_category_path',$category_path);
    }
    Tygh::$app['view']->assign('wk_parent_category_id',0);
}