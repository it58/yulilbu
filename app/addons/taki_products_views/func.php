<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

//version 4
function fn_taki_update_product_views($product_id){
	$taki_views_old = db_get_field("SELECT taki_views from ?:products WHERE product_id = ?i",$product_id);
	$data = array(
		'taki_views'	=> $taki_views_old+1,
	);
	db_query("UPDATE ?:products SET ?u WHERE product_id = ?i", $data, $product_id);
}
function fn_taki_products_views_get_product_data(&$product_id, &$field_list, &$join){
	
}

function fn_taki_products_views_get_products(&$params, &$fields, &$sortings, &$condition, &$join, &$sorting, &$group_by, &$lang_code, &$having){
	$fields[] = 'products.taki_views';
	if (!empty($params['bestsellers'])) {
		
	}else{
		#$fields[] = 'SUM(?:product_sales.amount) as sales_amount';
		$fields[] = '?:product_sales.amount as sales_amount';
		$join .= ' LEFT JOIN ?:product_sales ON ?:product_sales.product_id = products.product_id AND ?:product_sales.category_id = products_categories.category_id ';
	}
	 
	return true;
}