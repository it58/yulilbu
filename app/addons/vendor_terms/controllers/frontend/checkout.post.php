<?php
/***************************************************************************
<<<<<<< HEAD
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Tygh;

defined('BOOTSTRAP') or die('Access denied');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    return [CONTROLLER_STATUS_OK];
}

if ($mode === 'checkout' && fn_allowed_for('MULTIVENDOR')) {
    /** @var \Tygh\SmartyEngine\Core $view */
    $view = Tygh::$app['view'];
    /** @var array $cart */
    $cart = &Tygh::$app['session']['cart'];

    foreach ($cart['product_groups'] as $group) {
        $company_ids[] = $group['company_id'];
    }

    if (!empty($company_ids)) {
        $view->assign('vendor_terms', fn_get_vendor_terms($company_ids));
=======
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'checkout' && fn_allowed_for('MULTIVENDOR')) {
    $view = Tygh::$app['view'];
    $cart = &Tygh::$app['session']['cart'];

    if ($view->getTemplateVars('final_step') == $cart['edit_step']) {
        $company_ids = array();
        foreach ($cart['product_groups'] as $group) {
            $company_ids[] = $group['company_id'];
        }

        if (!empty($company_ids)) {
            $view->assign('vendor_terms', fn_get_vendor_terms($company_ids));
        }
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    }
}
