<?php
function fn_settings_actions_addons_energothemes_vivashop_addon_et_skin($new_value, $old_value){
	
	if (!empty($old_value) && $new_value != $old_value) {
		fn_check_cache(array('ctpl'=>true,'cc'=>true));
		fn_set_notification('N',__('notice'),'<strong>VIVAshop - '.$new_value.'</strong> functionality has been activated and the cache has been cleared.');
	}
	return true;
}
