<?php
/* SKIN 1 */
$schema['VIVAshop SKIN 1 - Main slider banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-banner-1.jpg',
	'block_name' => 'SKIN 1 - Main slider banners',
);
$schema['VIVAshop SKIN 1 - Main slider banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-banner-2.jpg',
	'block_name' => 'SKIN 1 - Main slider banners'
);
$schema['VIVAshop SKIN 1 - Main slider banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-banner-3.jpg',
	'block_name' => 'SKIN 1 - Main slider banners',
);
$schema['VIVAshop SKIN 1 - Main slider banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-banner-4.jpg',
	'block_name' => 'SKIN 1 - Main slider banners',
);
$schema['VIVAshop SKIN 1 - Main slider banner 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-banner-5.jpg',
	'block_name' => 'SKIN 1 - Main slider banners',
);

$schema['VIVAshop SKIN 1 - Main slider side banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-side-banner-1.jpg',
	'block_name' => 'SKIN 1 - Main slider side banners',
);
$schema['VIVAshop SKIN 1 - Main slider side banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_main-slider-side-banner-2.jpg',
	'block_name' => 'SKIN 1 - Main slider side banners',
);

$schema['VIVAshop SKIN 1 - Home side banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-side-banner-1.jpg',
	'block_name' => 'SKIN 1 - Home side banner 1',
);
$schema['VIVAshop SKIN 1 - Home side banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-side-banner-2.jpg',
	'block_name' => 'SKIN 1 - Home side banner 2',
);
$schema['VIVAshop SKIN 1 - Home side banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-side-banner-3.jpg',
	'block_name' => 'SKIN 1 - Home side banner 3',
);
$schema['VIVAshop SKIN 1 - Home side banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-side-banner-4.jpg',
	'block_name' => 'SKIN 1 - Home side banner 4',
);

$schema['VIVAshop SKIN 1 - Home middle banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-middle-banner-1.jpg',
	'block_name' => 'SKIN 1 - Home middle banner 1',
);
$schema['VIVAshop SKIN 1 - Home middle banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-middle-banner-2.jpg',
	'block_name' => 'SKIN 1 - Home middle banner 2',
);
$schema['VIVAshop SKIN 1 - Home middle banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-middle-banner-3.jpg',
	'block_name' => 'SKIN 1 - Home middle banner 3',
);

$schema['VIVAshop SKIN 1 - Home footer banner'] = array(
	'position' => '1',
	'image' => 'viva_skin_1_home-footer-banner.jpg',
	'block_name' => 'SKIN 1 - Home footer banner',
);

// /* SKIN 2*/
$schema['VIVAshop SKIN 2 - Main slider banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_main-slider-banner-1.jpg',
	'block_name' => 'SKIN 2 - Main slider banners',
);
$schema['VIVAshop SKIN 2 - Main slider banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_main-slider-banner-2.jpg',
	'block_name' => 'SKIN 2 - Main slider banners',
);
$schema['VIVAshop SKIN 2 - Main slider banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_main-slider-banner-3.jpg',
	'block_name' => 'SKIN 2 - Main slider banners',
);
$schema['VIVAshop SKIN 2 - Main slider banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_main-slider-banner-4.jpg',
	'block_name' => 'SKIN 2 - Main slider banners',
);
$schema['VIVAshop SKIN 2 - Main slider banner 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_main-slider-banner-5.jpg',
	'block_name' => 'SKIN 2 - Main slider banners',
);

$schema['VIVAshop SKIN 2 - Home small banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-small-banner-1.jpg',
	'block_name' => 'SKIN 2 - Home small banner 1',
);
$schema['VIVAshop SKIN 2 - Home small banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-small-banner-2.jpg',
	'block_name' => 'SKIN 2 - Home small banner 2',
);
$schema['VIVAshop SKIN 2 - Home small banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-small-banner-3.jpg',
	'block_name' => 'SKIN 2 - Home small banner 3',
);
$schema['VIVAshop SKIN 2 - Home small banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-small-banner-4.jpg',
	'block_name' => 'SKIN 2 - Home small banner 4',
);

$schema['VIVAshop SKIN 2 - Home middle banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-middle-banner-1.jpg',
	'block_name' => 'SKIN 2 - Home middle banner 1',
);
$schema['VIVAshop SKIN 2 - Home middle banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-middle-banner-2.jpg',
	'block_name' => 'SKIN 2 - Home middle banner 2',
);
$schema['VIVAshop SKIN 2 - Home middle banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-middle-banner-3.jpg',
	'block_name' => 'SKIN 2 - Home middle banner 3',
);
$schema['VIVAshop SKIN 2 - Home middle banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-middle-banner-4.jpg',
	'block_name' => 'SKIN 2 - Home middle banner 4',
);
$schema['VIVAshop SKIN 2 - Home middle banner 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-middle-banner-5.jpg',
	'block_name' => 'SKIN 2 - Home middle banner 5',
);

$schema['VIVAshop SKIN 2 - Browse our Featured Categories 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-1.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-2.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-3.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-4.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-5.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 6'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-6.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 7'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-7.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 8'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-8.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 9'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-9.jpg',
	'block_name' => 'Browse our Featured Categories',
);
$schema['VIVAshop SKIN 2 - Browse our Featured Categories 10'] = array(
	'position' => '1',
	'image' => 'viva_skin_2_home-featured-category-banner-10.jpg',
	'block_name' => 'Browse our Featured Categories',
);

// /* SKIN 3 */
$schema['VIVAshop SKIN 3 - Main slider banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_main-slider-banner-1.jpg',
	'block_name' => 'SKIN 3 - Main slider banners',
);
$schema['VIVAshop SKIN 3 - Main slider banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_main-slider-banner-2.jpg',
	'block_name' => 'SKIN 3 - Main slider banners',
);
$schema['VIVAshop SKIN 3 - Main slider banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_main-slider-banner-3.jpg',
	'block_name' => 'SKIN 3 - Main slider banners',
);

$schema['VIVAshop SKIN 3 - Main slider side banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_main-slider-side-banner-1.jpg',
	'block_name' => 'SKIN 3 - Main slider side banner 1',
);
$schema['VIVAshop SKIN 3 - Main slider side banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_main-slider-side-banner-2.jpg',
	'block_name' => 'SKIN 3 - Main slider side banner 2',
);
$schema['VIVAshop SKIN 3 - Main slider side banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_main-slider-side-banner-3.jpg',
	'block_name' => 'SKIN 3 - Main slider side banner 3',
);

$schema['VIVAshop SKIN 3 - Home middle banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_home-middle-banner-1.jpg',
	'block_name' => 'SKIN 3 - Home middle banner 1',
);
$schema['VIVAshop SKIN 3 - Home middle banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_home-middle-banner-2.jpg',
	'block_name' => 'SKIN 3 - Home middle banner 2',
);
$schema['VIVAshop SKIN 3 - Home middle banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_home-middle-banner-3.jpg',
	'block_name' => 'SKIN 3 - Home middle banner 3',
);
$schema['VIVAshop SKIN 3 - Home middle banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_home-middle-banner-4.jpg',
	'block_name' => 'SKIN 3 - Home middle banner 4',
);
$schema['VIVAshop SKIN 3 - Home middle banner 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_3_home-middle-banner-5.jpg',
	'block_name' => 'SKIN 3 - Home middle banner 5',
);

// /* COMMON */
$schema['VIVAshop - Bestseller banner'] = array(
	'position' => '1',
	'image' => 'viva_skin_bestseller-banner.jpg',
	'block_name' => 'Bestsellers banner',
);
$schema['VIVAshop - New items banner'] = array(
	'position' => '1',
	'image' => 'viva_skin_new-items-banner.jpg',
	'block_name' => 'New Items banner',
);
$schema['VIVAshop - On sale banner'] = array(
	'position' => '1',
	'image' => 'viva_skin_on-sale-banner.jpg',
	'block_name' => 'On Sale banner',
);

$schema['VIVAshop - Sidebox banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_sidebox-banner-1.jpg',
	'block_name' => 'Our Best Picks',
);
$schema['VIVAshop - Sidebox banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_sidebox-banner-2.jpg',
	'block_name' => 'Our Best Picks',
);
$schema['VIVAshop - Sidebox banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_sidebox-banner-3.jpg',
	'block_name' => 'Our Best Picks',
);
$schema['VIVAshop - Sidebox banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_sidebox-banner-4.jpg',
	'block_name' => 'Our Best Picks',
);
$schema['VIVAshop - Sidebox banner 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_sidebox-banner-5.jpg',
	'block_name' => 'Our Best Picks',
);

$schema['VIVAshop - Category side banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_category-side-banner-1.jpg',
	'block_name' => 'Category side banner 1',
);
$schema['VIVAshop - Category side banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_category-side-banner-2.jpg',
	'block_name' => 'Category side banner 2',
);

$schema['VIVAshop - Category banner 1'] = array(
	'position' => '1',
	'image' => 'viva_skin_cell-phones-banner.jpg',
	'block_name' => 'Category banner',
);
$schema['VIVAshop - Category banner 2'] = array(
	'position' => '1',
	'image' => 'viva_skin_computers-banner.jpg',
);
$schema['VIVAshop - Category banner 3'] = array(
	'position' => '1',
	'image' => 'viva_skin_electronics-banner.jpg',
);
$schema['VIVAshop - Category banner 4'] = array(
	'position' => '1',
	'image' => 'viva_skin_food-beverage-banner.jpg',
);
$schema['VIVAshop - Category banner 5'] = array(
	'position' => '1',
	'image' => 'viva_skin_home-deco-banner.jpg',
);
$schema['VIVAshop - Category banner 6'] = array(
	'position' => '1',
	'image' => 'viva_skin_kids-banner.jpg',
);
$schema['VIVAshop - Category banner 7'] = array(
	'position' => '1',
	'image' => 'viva_skin_men-banner.jpg',
);
$schema['VIVAshop - Category banner 8'] = array(
	'position' => '1',
	'image' => 'viva_skin_wines-banner.jpg',
);
$schema['VIVAshop - Category banner 9'] = array(
	'position' => '1',
	'image' => 'viva_skin_women-banner.jpg',
);

return $schema;