<?php
use Tygh\Settings;
use Tygh\Registry;
use Tygh\Themes\Themes;
function fn_energothemes_get_parent_theme($theme_name){
	$manifest=Themes::factory($theme_name)->getManifest();
	return $manifest['parent_theme'];
}


function fn_energothemes_vivashop_addon_demo_data(){

  return __('energothemes_vivashop_addon.demo_banners', array(
      '[demo_banners_url]' =>  fn_url('vivashop_demo_banners.install')
  ));

}
function fn_energothemes_vivashop_addon_demo_data_install(){
	$banners = fn_get_schema('vivashop_demo_data', 'banners');
	$skip=false;

	if ($banners and is_array($banners)){
		foreach ($banners as $name => $banner) {
			$result=db_get_array("SELECT banner_id FROM ?:banner_descriptions WHERE banner LIKE ?l","%$name%");

			if ($result){
				fn_set_notification('W',__('notice'),__('energothemes_vivashop_addon.demo_already_installed'));
				$skip=true;
			}
		}
		if (!$skip){
			foreach ($banners as $name => $banner) {
				$banner_data = array(
					'banner' => $name,
					'status' => 'A',
					'type' => 'G',
					'localization' => '',
					'timestamp' => TIME,
					'target' => 'T',
					'url' => '#',
					'description' => '',
					'company_id' => fn_get_default_company_id(),
				);
				$banner_data = array_merge($banner_data, $banner);

				$demo_directory = Registry::get('config.dir.files')."/1/vivashop_demo_banners/";

				$images = array(
					'banner_id' => 0,
					'banners_main_image_data' => array(
						array(
							'pair_id' => '',
							'type' => 'M',
							'object_id' => 0,
							'image_alt' => '',
						)
					),
					'file_banners_main_image_icon' => array(
						(!empty($banner['image']) ? $demo_directory.$banner['image'] : "")
					),
					'type_banners_main_image_icon' => array(
						'server'
					),
				);

				$_REQUEST = array_merge($_REQUEST, $images);
				$banner_id = fn_banners_update_banner($banner_data, 0, CART_LANGUAGE);

				if (fn_allowed_for('ULTIMATE') && $banner_id) {
					fn_share_object_to_all('banners', $banner_id);
				}

				if (!empty($banner['block_name'])){
					$block_name=$banner['block_name'];
					$result=db_get_array("SELECT name,block_id, lang_code FROM ?:bm_blocks_descriptions WHERE name LIKE ?l","%$block_name%");
					
					foreach ($result as $key => $value) {
						$name=$value['name'];
						$id=$value['block_id'];
						$lang_code=$value['lang_code'];
						$multiple=false;

						if (strrpos($name, 's',-1)){
							$multiple=true;
						}

						$content=db_get_field("SELECT content FROM ?:bm_blocks_content WHERE block_id = ?i AND lang_code = ?s", $id, $lang_code);
						$content=unserialize($content);
						if ($content['items']['filling']=='manually'){

							if ($multiple){
								$content['items']['item_ids'] = empty($content['items']['item_ids']) ? $banner_id : $content['items']['item_ids'].",".$banner_id;
							}else{
								$content['items']['item_ids'] = $banner_id;	
							}
						}
						$content=serialize($content);
						db_query("UPDATE ?:bm_blocks_content SET content = ?s WHERE block_id = ?i AND lang_code = ?s",$content,$id,$lang_code);
					}
				}
			}
			fn_set_notification('N', __('successful'),__('energothemes_vivashop_addon.demo_success'));
		}
	}
}