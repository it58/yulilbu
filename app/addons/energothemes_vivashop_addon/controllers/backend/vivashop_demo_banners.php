<?php
if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'install') {
    fn_energothemes_vivashop_addon_demo_data_install();
}
