<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_search_sault(&$sesssion, $params)
{
    ksort($params);
    
    $exclude_params = array(
        'result_ids',
        'save_view_results',
        'is_ajax',
        'layout',
        'sort_by',
        'sort_order',
        'items_per_page'
    );
    foreach ($exclude_params as $p) {
        if (isset($params[$p])) {
            unset($params[$p]);
        }
    }

    $random_md5 = md5(json_encode($params));

    if (empty($_SESSION['random_md5']) || $_SESSION['random_md5'] != $random_md5) {
        $_SESSION['random_md5'] = $random_md5;
        $_SESSION['random_salt'] = fn_generate_salt();
        $_SESSION['disable_cache_once'] = true;
    }
    
    return true;
}

function fn_ecl_random_products_products_sorting(&$sorting, $simple_mode)
{
    $sorting['random'] = array('description' => __('random'), 'default_order' => 'asc', 'desc' => false);
}

function fn_ecl_random_products_get_products(&$params, &$fields, &$sortings, $condition, $join, $sorting, $group_by, $lang_code, $having)
{
    // $sortings['random'] = 'RAND()'; slow method

    if (AREA == 'C') {
        $sortings['random'] = 'random';

        $default_sorting = fn_get_default_products_sorting();
        if ((!empty($params['sort_by']) && $params['sort_by'] == 'random') || empty($params['sort_by']) && $default_sorting['sort_by'] == 'random') {
            if (!empty($params['block_data']) || empty($_SESSION['random_salt'])) {
                $salt = fn_generate_salt();
            } else {
                $salt = $_SESSION['random_salt'];
                if (!empty($_SESSION['disable_cache_once'])) {
                    $params['use_caching'] = false;
                    unset($_SESSION['disable_cache_once']);
                }
            }
            $fields['random'] = db_quote("MD5(CONCAT(?s,products.product_id)) as random", $salt);
        }
    }
}

function fn_settings_variants_addons_ecl_random_products_default_sorting()
{
    $sortings = fn_get_products_sorting();
    unset($sortings['random']);

    $orders = fn_get_products_sorting_orders();

    $return = array();

    foreach ($sortings as $option => $info) {
        foreach ($orders as $order) {
            if (!isset($info[$order]) || $info[$order] !== false) {
                $label = 'sort_by_' . $option . '_' . $order;
                $return[$option . '-' . $order] = __($label);
            }
        }
    }

    return $return;
}

function fn_ecl_random_products_render_block_content_pre($template_variable, $field, &$block_scheme, $block)
{
    if (!empty($block['content']) && !empty($block['content']['items']) && !empty($block['content']['items']['filling']) && $block['content']['items']['filling'] == 'random') {
        $block_scheme['cache'] = NULL;
    }
}