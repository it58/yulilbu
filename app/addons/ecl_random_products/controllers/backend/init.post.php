<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$params  = explode('-', Registry::get('settings.Appearance.default_products_sorting'));
$sort_by = array_shift($params);
if ($sort_by == 'random') {
    Registry::set('settings.Appearance.default_products_sorting', Registry::get('addons.ecl_random_products.default_sorting'));
}
 
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}