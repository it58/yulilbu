<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

$schema['products']['content']['items']['fillings']['random'] = array(
    'params' => array(
        'sort_by' => 'random',
        'sort_order' => 'asc',
        'request' => array(
            'cid' => '%CATEGORY_ID%'
        ),
    ),
    'disable_cache' => true
);

$schema['main']['cache_overrides_by_dispatch']['categories.view']['session_handlers'][] = 'random_salt';
return $schema;