<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use \Tygh\Registry;

$schema['top']['administration']['items']['import_data']['subitems']['categories'] = array(
    'href' => 'exim.import?section=categories',
    'position' => 600,
);
$schema['top']['administration']['items']['export_data']['subitems']['categories'] = array(
    'href' => 'exim.export?section=categories',
    'position' => 600,
);

return $schema;