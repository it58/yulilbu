<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;

if (Registry::get('runtime.company_id')) {
    unset($schema['top']['administration']['items']['export_data']['subitems']['categories']);
    unset($schema['top']['administration']['items']['import_data']['subitems']['categories']);
}

return $schema;