<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

$schema['export']['sections']['categories'] = array(
    'permission' => false,
);
$schema['import']['sections']['categories'] = array(
    'permission' => false,
);

return $schema;