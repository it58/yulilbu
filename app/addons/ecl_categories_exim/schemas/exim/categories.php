<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;

include_once(Registry::get('config.dir.addons') . 'ecl_categories_exim/schemas/exim/categories.functions.php');

$schema = array(
    'section' => 'categories',
    'pattern_id' => 'categories',
    'name' => __('categories'),
    'key' => array('category_id'),
    'order' => 0,
    'table' => 'categories',
    'order_by' => 'level',
    'permissions' => array(
        'import' => 'manage_catalog',
        'export' => 'view_catalog',
    ),
    'references' => array(
        'category_descriptions' => array(
            'reference_fields' => array('category_id' => '#key', 'lang_code' => '#lang_code'),
            'join_type' => 'LEFT'
        ),
        'images_links' => array(
            'reference_fields' => array('object_id' => '#key', 'object_type' => 'category', 'type' => 'M'),
            'join_type' => 'LEFT',
            'import_skip_db_processing' => true
        ),
        'companies' => array(
            'reference_fields' => array('company_id' => '&company_id'),
            'join_type' => 'LEFT',
            'import_skip_db_processing' => true
        )
    ),
    'condition' => array(
        'use_company_condition' => true,
    ),
    'options' => array(
        'lang_code' => array(
            'title' => 'language',
            'type' => 'languages',
            'default_value' => array(DEFAULT_LANGUAGE),
        ),
        'category_delimiter' => array(
            'title' => 'category_delimiter',
            'description' => 'text_category_delimiter',
            'type' => 'input',
            'default_value' => '///'
        ),
        'images_path' => array(
            'title' => 'images_directory',
            'description' => 'text_images_directory',
            'type' => 'input',
            'default_value' => 'exim/backup/images/',
            'notes' => __('text_file_editor_notice', array('[href]' => fn_url('file_editor.manage?path=/'))),
        ),
    ),
    'pre_export_process' => array(
        'add_code_for_all_categories' => array(
            'function' => 'fn_ecl_categories_exim_set_category_codes',
            'args' => array(),
            'export_only' => true,
        ),
    ),
    'pre_processing' => array(
        'check_category_codes' => array(
            'function' => 'fn_check_category_codes',
            'args' => array('$import_data'),
            'import_only' => true,
        )
    ),
    'import_get_primary_object_id' => array(
        'fill_categories_alt_keys' => array(
            'function' => 'fn_import_fill_categories_alt_keys',
            'args' => array('$pattern', '$alt_keys', '$object', '$skip_get_primary_object_id'),
            'import_only' => true,
        ),
    ),
    'range_options' => array(
        'selector_url' => 'categories.manage',
        'object_name' => __('categories'),
    ),
    'export_fields' => array(
        'Category path' => array(
            'process_get' => array('fn_get_category_path', '#key', '#lang_code', '@category_delimiter'),
            'process_put' => array('fn_exim_set_categories', '#key', '#this', '@category_delimiter'),
            'multilang' => true,
            'linked' => false, // this field is not linked during import-export
        ),
        'Category code' => array(
            'db_field' => 'category_code',
            'alt_key' => true,
            'required' => true,
            'alt_field' => 'category_id'
        ),
        'Language' => array(
            'table' => 'category_descriptions',
            'db_field' => 'lang_code',
            'type' => 'languages',
            'required' => true,
            'multilang' => true
        ),
        'Status' => array(
            'db_field' => 'status'
        ),
        'Date added' => array(
            'db_field' => 'timestamp',
            'process_get' => array('fn_timestamp_to_date', '#this'),
            'convert_put' => array('fn_date_to_timestamp', '#this'),
            'return_result' => true,
            'default' => array('time')
        ),
        'Detailed image' => array(
            'db_field' => 'detailed_id',
            'table' => 'images_links',
            'process_get' => array('fn_export_image', '#this', 'detailed', '@images_path'),
            'process_put' => array('fn_import_images', '@images_path', '%Thumbnail%', '#this', '0', 'M', '#key', 'category')
        ),
        'Thumbnail' => array(
            'table' => 'images_links',
            'db_field' => 'image_id',
            'use_put_from' => '%Detailed image%',
            'process_get' => array('fn_export_image', '#this', 'category', '@images_path')
        ),
        'Description' => array(
            'table' => 'category_descriptions',
            'db_field' => 'description',
            'multilang' => true,
        ),
        'Meta keywords' => array(
            'table' => 'category_descriptions',
            'db_field' => 'meta_keywords',
            'multilang' => true,
        ),
        'Meta description' => array(
            'table' => 'category_descriptions',
            'db_field' => 'meta_description',
            'multilang' => true,
        ),
        'Page title' => array(
            'table' => 'category_descriptions',
            'db_field' => 'page_title',
            'multilang' => true,
        ),
        'Position' => array(
            'db_field' => 'position'
        ),
        'Product details view' => array(
            'db_field' => 'product_details_view'
        ),
        'Category id' => array(
            'db_field' => 'category_id'
        ),
    ),
);

// user groups are not available for free version
if (!fn_allowed_for('ULTIMATE:FREE')) {
    $schema['export_fields']['User group IDs'] = array(
        'db_field' => 'usergroup_ids'
    );
}

// extend for seo
if (Registry::get('addons.seo.status') == 'A') {
    $schema['references']['seo_names'] = array (
        'reference_fields' => array ('object_id' => '#key', 'type' => 'c', 'dispatch' => '', 'lang_code' => '#category_descriptions.lang_code'),
        'join_type' => 'LEFT',
        'import_skip_db_processing' => true
    );

    if (fn_allowed_for('ULTIMATE')) {
        $schema['references']['seo_names']['reference_fields']['company_id'] = '&company_id';
    }

    $schema['export_fields']['SEO name'] = array (
        'table' => 'seo_names',
        'db_field' => 'name',
        'process_put' => array ('fn_create_import_seo_name', '#key', 'c', '#this', '%Category path%', 0, '', '', '#lang_code'),
    );

    if (Registry::get('addons.seo.single_url') == 'N') {
        $schema['export_fields']['SEO name']['multilang'] = true;
    }
}

// check cs-cart feature
if (fn_allowed_for('ULTIMATE')) {
    $company_schema = array(
        'table' => 'companies',
        'db_field' => 'company',
        'process_put' => array('fn_exim_set_category_company', '#key', '#this')
    );

    if (fn_allowed_for('ULTIMATE')) {
        $schema['export_fields']['Store'] = $company_schema;
        
        if (!Registry::get('runtime.company_id')) {
            $schema['export_fields']['Store']['required'] = true;
            $schema['export_fields']['Category path']['process_put'] = array('fn_exim_set_categories', '#key', '#this', '@category_delimiter', '%Store%');
        }
        $schema['import_process_data']['check_category_company_id'] = array(
            'function' => 'fn_import_check_category_company_id',
            'args' => array('$primary_object_id', '$object', '$pattern', '$options', '$processed_data', '$processing_groups', '$skip_record'),
            'import_only' => true,
        );
    }
}

return $schema;
