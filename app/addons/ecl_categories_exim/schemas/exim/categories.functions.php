<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;

function fn_exim_set_categories($_category_id, $categories_data, $category_delimiter, $store_name = '')
{
    if (fn_is_empty($categories_data)) {
        return false;
    }

    $paths = array();

    foreach ($categories_data as $lang => $data) {
        $_paths = array($data);

        foreach ($_paths as $k => $cat_path) {
            $category = (strpos($cat_path, $category_delimiter) !== false) ? explode($category_delimiter, $cat_path) : array($cat_path);
            foreach ($category as $key_cat => $cat) {
                $paths[$k][$key_cat][$lang] = $cat;
            }
        }
    }

    $company_id = 0;
    if (fn_allowed_for('ULTIMATE')) {
        if (Registry::get('runtime.company_id')) {
            $company_id = Registry::get('runtime.company_id');
        } else {
            $company_id = fn_get_company_id_by_name($store_name);

            if (!$company_id) {
                $company_data = array('company' => $store_name, 'email' => '');
                $company_id = fn_update_company($company_data, 0);
            }
        }
    }

    foreach ($paths as $key_path => $categories) {

        if (!empty($categories)) {
            $parent_id = '0';

            $updated_category = array_pop($categories);

            if (!empty($categories)) {
                foreach ($categories as $cat) {
                    $category_condition = '';
                    if (fn_allowed_for('ULTIMATE')) {
                        if (!empty($paths_store[$key_path]) && !Registry::get('runtime.company_id')) {
                            $path_company_id = fn_get_company_id_by_name($paths_store[$key_path]);
                            $category_condition = fn_get_company_condition('?:categories.company_id', true, $path_company_id);
                        } else {
                            $category_condition = fn_get_company_condition('?:categories.company_id', true, $company_id);
                        }
                    }

                    reset($cat);
                    $main_lang = key($cat);
                    $main_cat = array_shift($cat);

                    $category_id = db_get_field("SELECT ?:categories.category_id FROM ?:category_descriptions INNER JOIN ?:categories ON ?:categories.category_id = ?:category_descriptions.category_id $category_condition WHERE ?:category_descriptions.category = ?s AND lang_code = ?s AND parent_id = ?i", $main_cat, $main_lang, $parent_id);

                    if (!empty($category_id)) {
                        $parent_id = $category_id;
                    } else {

                        $category_data = array(
                            'parent_id' => $parent_id,
                            'category' =>  $main_cat,
                            'timestamp' => TIME,
                        );

                        if (fn_allowed_for('ULTIMATE')) {
                            $category_data['company_id'] = !empty($path_company_id) ? $path_company_id : $company_id;
                        }

                        $category_id = fn_update_category($category_data);

                        foreach ($cat as $lang => $cat_data) {
                            $category_data = array(
                                'parent_id' => $parent_id,
                                'category' => $cat_data,
                                'timestamp' => TIME,
                            );

                            if (fn_allowed_for('ULTIMATE')) {
                                $category_data['company_id'] = $company_id;
                            }

                            fn_update_category($category_data, $category_id, $lang);
                        }

                        $parent_id = $category_id;
                    }

                }
            }

            foreach ($updated_category as $lang_code => $cat_name) {
                $category_data = array(
                    'parent_id' => $parent_id,
                    'category' => $cat_name,
                    'timestamp' => TIME,
                );

                if (fn_allowed_for('ULTIMATE')) {
                    $category_data['company_id'] = $company_id;
                }

                fn_update_category($category_data, $_category_id, $lang_code);
            }
        }
    }

    return true;
}

function fn_import_check_category_company_id(&$primary_object_id, &$object, &$pattern, &$options, &$processed_data, &$processing_groups, &$skip_record)
{
    if (Registry::get('runtime.company_id')) {
        if ($pattern['pattern_id'] == 'categories') {
            $object['company_id'] = Registry::get('runtime.company_id');
        }

        if (!empty($primary_object_id)) {
            reset($primary_object_id);
            list($field, $value) = each($primary_object_id);
            $company_id = db_get_field('SELECT company_id FROM ?:categories WHERE ' . $field . ' = ?s', $value);

            if ($company_id != Registry::get('runtime.company_id')) {
                $processed_data['S']++;
                $skip_record = true;
            }
        }
    }
}

function fn_exim_set_category_company($category_id, $company_name)
{
    $company_id = fn_exim_set_company('categories', 'category_id', $category_id, $company_name);

    return $company_id;
}

function fn_check_category_codes(&$data)
{
    if (!empty($data)) {
        foreach ($data as $key => $category_data) {
            if (isset($category_data['category_code']) && empty($category_data['category_code']) && !empty($category_data['Category path'])) {
                $data[$key]['category_code'] = fn_create_category_code();
            }
        }
    }

    return true;
}

function fn_import_fill_categories_alt_keys($pattern, &$alt_keys, &$object, &$skip_get_primary_object_id)
{
    if (Registry::get('runtime.company_id')) {
        $alt_keys['company_id'] = Registry::get('runtime.company_id');

    } elseif (!empty($object['company'])) {
        // field store is set
        $company_id = fn_get_company_id_by_name($object['company']);
        if ($company_id !== null) {
            $alt_keys['company_id'] = $company_id;
        } else {
            $skip_get_primary_object_id = true;
        }
    } else {
        // field store is not set
        $skip_get_primary_object_id = true;
    }
}