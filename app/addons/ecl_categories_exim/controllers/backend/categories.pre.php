<?php
/*****************************************************************************
*                                                                            *
*                   All rights reserved! eCom Labs LLC                       *
* http://www.ecom-labs.com/about-us/ecom-labs-modules-license-agreement.html *
*                                                                            *
*****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'export_range') {
        if (!empty($_REQUEST['category_ids'])) {
            if (empty($_SESSION['export_ranges'])) {
                $_SESSION['export_ranges'] = array();
            }

            if (empty($_SESSION['export_ranges']['categories'])) {
                $_SESSION['export_ranges']['categories'] = array('pattern_id' => 'categories');
            }

            $_ids = $_REQUEST['category_ids'];
            if (!empty($action) && $action == 'subcats') {
                foreach ($_ids as $cid) {
                    $from_id_path = db_get_field("SELECT id_path FROM ?:categories WHERE category_id = ?i", $cid);
                    $subcats = db_get_fields("SELECT category_id FROM ?:categories WHERE id_path LIKE ?l", "$from_id_path/%");
                    if (!empty($subcats)) {
                        $_ids = array_merge($_ids, $subcats);
                    }
                }
            }
            $_ids = array_unique($_ids);

            $_SESSION['export_ranges']['categories']['data'] = array('category_id' => $_ids);

            unset($_REQUEST['redirect_url']);

            return array(CONTROLLER_STATUS_REDIRECT, 'exim.export?section=categories&pattern_id=' . $_SESSION['export_ranges']['categories']['pattern_id']);
        }
    }

    if ($mode == 'export_category_products') {
        if (!empty($_REQUEST['category_ids'])) {
            $params = array(
                'only_short_fields' => true,
                'extend' => array(
                    'companies'
                ),
                'cid' => $_REQUEST['category_ids']
            );

            if (fn_allowed_for('ULTIMATE')) {
                $params['extend'][] = 'sharing';
            }
            $action = Registry::get('runtime.action');
            if (!empty($action) && $action == 'subcategories') {
                $params['subcats'] = 'Y';
            }

            list($products, $search) = fn_get_products($params);

            if (empty($products)) {
                fn_set_notification('E', __('error'), __('text_no_products_found'));
                return array(CONTROLLER_STATUS_REDIRECT, 'categories.manage');
            }

            if (empty($_SESSION['export_ranges'])) {
                $_SESSION['export_ranges'] = array();
            }

            if (empty($_SESSION['export_ranges']['products'])) {
                $_SESSION['export_ranges']['products'] = array('pattern_id' => 'products');
            }

            $_SESSION['export_ranges']['products']['data'] = array('product_id' => array_keys($products));

            unset($_REQUEST['redirect_url']);

            return array(CONTROLLER_STATUS_REDIRECT, 'exim.export?section=products&pattern_id=' . $_SESSION['export_ranges']['products']['pattern_id']);
        }
    }
    return;
}