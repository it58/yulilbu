<?php
/************************************************************************
# Wk Registration Confirmation  ---  Wk Registration Confirmation       *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$data=db_get_array("SELECT * FROM ?:wk_user_registration WHERE expire_timestamp < ?i",TIME);
$vendor_confirm_data=db_get_array("SELECT * FROM ?:wk_vendor_registration WHERE expire_timestamp < ?i",TIME);

foreach ($data as $key => $user_data) {

	if(Registry::get('addons.wk_registration_confirmation.cust_delete_user_setting') == 'Y')
		fn_delete_user($user_data['user_id']); 

	db_query('DELETE FROM ?:wk_user_registration WHERE user_id = ?i',$user_data['user_id']);
}

foreach ($vendor_confirm_data as $key => $vendor_data) {
	db_query('DELETE FROM ?:wk_vendor_registration WHERE company_id = ?i',$vendor_data['company_id']);
}

/*$registration_confirmation = fn_get_registration_sttings_data();
Registry::get('view')->assign('registration_confirmation', $registration_confirmation);
*/