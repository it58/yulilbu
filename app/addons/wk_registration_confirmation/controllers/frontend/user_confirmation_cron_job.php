<?php
/************************************************************************
# Wk Registration Confirmation    ---  Wk Registration Confirmation     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode == 'cron') {
	$count = 0;
	$users = db_get_array('SELECT user_id,confirmation_code,expire_timestamp,email FROM ?:wk_user_registration WHERE resend_time <= ?i AND can_resend = ?s AND expire_timestamp > ?i',TIME,'Y',TIME);
	if(!empty($users)) {
		foreach($users as $user) {	
			$user_data = fn_get_user_info($user['user_id']);
			if(!empty($user_data)){
				$seting_exp_time=Registry::get('addons.wk_registration_confirmation.cust_expire_time');
				$seting_resend_time = !empty(Registry::get('addons.wk_registration_confirmation.cust_resend_time')) || !is_numeric(Registry::get('addons.wk_registration_confirmation.cust_resend_time')) || !(Registry::get('addons.wk_registration_confirmation.cust_resend_time') < 0) ?(3600 * Registry::get('addons.wk_registration_confirmation.cust_resend_time')):3600;
				if(empty($seting_exp_time) || !is_numeric($seting_exp_time) || ($seting_exp_time < 0))
					$exp_tym=6*3600;
				else
					$exp_tym=3600*$seting_exp_time;
				$email_data['email'] = $user_data['email'];
		        $email_data['dor'] = date('d/m/Y',$user_data['timestamp']);
		        $email_data['name'] = fn_get_user_name($user_data['user_id']);
		       	$email_data['days_countdown'] = round(($user_data['timestamp']+$exp_tym - time())/3600);
		        $email_data['confirm_link'] = fn_url('user_registration_varify.varify&confirmation='.$user['confirmation_code'].'&email='.$user_data['email']);
		        $from = 'default_company_users_department';
		      
		        $link_to_click = "<a href='$email_data[confirm_link]'>Click Here</a>";
		        
		        $wk_data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
				$mailer = Tygh::$app['mailer'];
	            $mailer->send(array(
		                    'to' => $user_data['email'],
		                    'from' => $from,
		                    'data' => array(
		                        'wk_data' => $wk_data,
		                    ),
							'template_code' => 'confirmation_link_mail', 
		                    'tpl' => 'addons/wk_registration_confirmation/customer_confirmation.tpl',
		                    'company_id' => 0
		                ), fn_check_user_type_admin_area('C', Registry::get('settings.Appearance.backend_default_language')));

		        $resend_data = 'C';
			    fn_update_resend($resend_data, $user_data['user_id']);
			}
		}
	}
}