<?php

use Tygh\Http;
use Tygh\Registry;
use Tygh\Settings;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

require_once(Registry::get('config.dir.functions') . 'fn.sales_reports.php'); // FIXME: move amchars to separate file

$chart_type = (!empty($_REQUEST['chart_type']) ? $_REQUEST['chart_type'] : 'table');
$reports_group = empty($_REQUEST['reports_group']) ? 'product_views' : $_REQUEST['reports_group'];
$company_id = Registry::get('runtime.company_id');

if ($mode == 'report') {
    if ($reports_group == 'product_views') {
        $params = $_REQUEST;
        
        $params['get_field'] = 'viewed';
        list($data, $search, $period) = fn_cp_extended_statistics_get_product_views($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('viewed'), array(__('views')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('statistics_period', $period);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'product_favorites') {
        $params = $_REQUEST;
        $params['get_field'] = 'favorites';
        list($data, $search, $period) = fn_cp_extended_statistics_get_product_views($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('favorites'), array(__('favorites')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('statistics_period', $period);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'revenue') {
        $params = $_REQUEST;
        list($data, $search, $period) = fn_cp_extended_statistics_get_revenue($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('subtotal'), array(__('revenue')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('statistics_period', $period);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'traffic_source') {
        if (!empty($company_id)) {
            return array(CONTROLLER_STATUS_DENIED);
        }
        list($where, $join, $params) = fn_cp_extended_statistics_stat_filter_data($_REQUEST);
        $where .= " AND client_type = 'U' AND referrer != ''";

        $filter_condition = fn_cp_extended_statistics_bild_sql_join($join);

        $data = array();

        $data['data'] = db_get_array("SELECT CONCAT(referrer_scheme, '://', referrer_host, '/') AS label, COUNT(*) AS count FROM ?:stat_sessions $filter_condition WHERE $where GROUP BY referrer_scheme, referrer_host ORDER BY count DESC LIMIT ?i", $params['limit']);
        
        if ($chart_type != 'table') {
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('label' => 'title', 'count' => 'value')));
        }
        Registry::get('view')->assign('statistics_data', fn_cp_extended_statistics_stat_format_data($data));
        Registry::get('view')->assign('chart_type', 'table');
        Registry::get('view')->assign('search', $_REQUEST);
    } elseif ($reports_group == 'top_keywords') {
        if (!empty($company_id)) {
            return array(CONTROLLER_STATUS_DENIED);
        }
        $params = $_REQUEST;
        list($data, $product_features, $search) = fn_cp_extended_statistics_get_top_keywords($params);
        
        Registry::get('view')->assign('product_features', $product_features);
        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('search', $search);
        
        
        
//         list($data, $search, $engines) = fn_cp_extended_statistics_get_top_keywords($params, Registry::get('settings.Appearance.elements_per_page'));
// 
//         if ($chart_type != 'table') {        
//             Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('viewed'), array(__('views')), true));
//         }       
// 
//         Registry::get('view')->assign('statistics_data', $data);
//         Registry::get('view')->assign('engines', $engines);
//         if (!isset($_REQUEST['selected_engines'])) {
//             $selected_engines = array_keys($engines);
//         } else {
//             $selected_engines = explode(',', $_REQUEST['selected_engines']);
//         }
//         Registry::get('view')->assign('search', $search);
//         Registry::get('view')->assign('selected_engines', $selected_engines);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'most_active') {
        $params = $_REQUEST;
        
        $params['get_field'] = 'viewed';
        $params['order_by'] = 'viewed';
        list($data, $search) = fn_cp_extended_statistics_most_active($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('viewed'), array(__('views')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'most_favorites') {
        $params = $_REQUEST;
        
        $params['get_field'] = 'favorites';
        $params['order_by'] = 'favorites';
        list($data, $search) = fn_cp_extended_statistics_most_active($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('favorites'), array(__('favorites')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'views_by_countries') {
        if (!empty($company_id)) {
            return array(CONTROLLER_STATUS_DENIED);
        }
        $params = $_REQUEST;
        
        list($data, $search) = fn_cp_extended_statistics_views_by_countries($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('views_by_countries'), array(__('views_by_countries')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);
        
    } elseif ($reports_group == 'orders_by_countries') {
        $params = $_REQUEST;
        
        list($data, $search) = fn_cp_extended_statistics_orders_by_countries($params, Registry::get('settings.Appearance.elements_per_page'));
        if ($chart_type != 'table') {        
            Registry::get('view')->assign('chart_data', fn_cp_extended_statistics_map_data($chart_type, $data['data'], array('orders_by_countries'), array(__('orders_by_countries')), true));
        }       

        Registry::get('view')->assign('statistics_data', $data);
        Registry::get('view')->assign('search', $search);
        if (empty($_REQUEST['chart_type'])) {
            $chart_type = 'table';
        } else {
            $chart_type = $_REQUEST['chart_type'];
        }
        Registry::get('view')->assign('chart_type', $chart_type);        
    }
}

// [Page sections]
$sections = array();

if (empty($company_id)) {
    foreach (array('product_views', 'product_favorites', 'revenue', 'traffic_source', 'top_keywords', 'most_active', 'most_favorites', 'views_by_countries', 'orders_by_countries') as $s) {
        $sections[$s] = array (
            'title' => __($s),
            'href' => "cp_extended_statistics.report?reports_group=$s",
            'ajax' => true
        );
    }
} else {
    foreach (array('product_views', 'product_favorites', 'revenue', 'most_active', 'most_favorites', 'orders_by_countries') as $s) {
        $sections[$s] = array (
            'title' => __($s),
            'href' => "cp_extended_statistics.report?reports_group=$s",
            'ajax' => true
        );
    }
}
Registry::set('navigation.dynamic.sections', $sections);
Registry::set('navigation.dynamic.active_section', empty($active_section) ? $reports_group : $active_section);

Registry::get('view')->assign('reports_group', $reports_group);
