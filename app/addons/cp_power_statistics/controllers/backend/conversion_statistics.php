<?php
use Tygh\Registry;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'manage')
{
    $params = $_REQUEST;
	
    list($transitions, $search,$totals) = fn_get_transitions($params, Registry::get('settings.Appearance.admin_elements_per_page'), true);
    $storefront_url = 'http://' . Registry::get('config.current_host');
    Registry::get('view')->assign('search', $search);
    Registry::get('view')->assign('storefront_url', $storefront_url);
    Registry::get('view')->assign('transitions', $transitions);
    Registry::get('view')->assign('totals', $totals);
    Registry::get('view')->assign('display_totals', fn_display_conversion_totals($transitions));
    Registry::get('view')->assign('countries', fn_get_simple_countries(false, DESCR_SL));

}

if ($mode == 'm_delete') {
	foreach ($_REQUEST['url_ids'] as $v) {
		fn_delete_transition($v);
	}
}

if ($mode == 'delete') {
	fn_delete_transition($_REQUEST['url_id']);
	return array(CONTROLLER_STATUS_REDIRECT);
}

if ($mode == 'delete_all_urls_without_order') {
	$result = db_query("DELETE FROM ?:conversion_statistics WHERE order_id = ?i", 0);
	fn_set_notification('N', __("notice"), __("transition_without_order_has_been_deleted"));
	fn_redirect(fn_url('addons.manage'));
}
