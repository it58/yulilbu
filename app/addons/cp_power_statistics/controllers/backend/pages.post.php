<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

if ($mode == 'update') {
            Registry::set('navigation.tabs.referers', array (
                'title' => __('referers'),
                'js' => true
            ));
            
	    list($referers, $search) = fn_ps_get_referers($_REQUEST, Registry::get('settings.Appearance.admin_orders_per_page'),true);
	    Registry::get('view')->assign('ref_search', $search);
	    Registry::get('view')->assign('referers', $referers);  
	    
	    Registry::get('view')->assign('countries', fn_get_simple_countries(false, DESCR_SL));	    
}
            
            
?>