<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_REQUEST['popularity']['product_id'])) {
      db_query('REPLACE INTO ?:product_popularity ?e', $_REQUEST['popularity']);
    }
}

if ($mode == 'update') {
            Registry::set('navigation.tabs.power_statistic', array (
                'title' => __('power_statistic'),
                'js' => true
            ));
            
            Registry::set('navigation.tabs.referers', array (
                'title' => __('referers'),
                'js' => true
            ));            

            $orders = fn_ps_get_orders($_REQUEST['product_id']);

            Registry::get('view')->assign('orders', $orders);             

            Registry::get('view')->assign('ps_popularity', db_get_row('SELECT * FROM ?:product_popularity WHERE product_id = ?i', $_REQUEST['product_id']));            
            
	    list($referers, $search) = fn_ps_get_referers($_REQUEST, Registry::get('settings.Appearance.admin_orders_per_page'),true);
	    Registry::get('view')->assign('ref_search', $search);
	    Registry::get('view')->assign('referers', $referers);     
	    
	    Registry::get('view')->assign('countries', fn_get_simple_countries(false, DESCR_SL));	    
}
            
            
?>