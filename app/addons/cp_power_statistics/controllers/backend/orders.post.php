<?php

use Tygh\Http;
use Tygh\Mailer;
use Tygh\Pdf;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Settings;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'details') {
	$transitions = db_get_array('SELECT * FROM ?:conversion_statistics WHERE order_id = ?i ORDER by timestamp ASC', $_REQUEST['order_id']);
	if (!empty($transitions)) {
		Registry::set('navigation.tabs.conversion_statistics', array (
			'title' => __('conversion_statistics'),
			'js' => true
		));
		Registry::get('view')->assign('transitions', $transitions);    
		Registry::get('view')->assign('countries', fn_get_simple_countries(false, DESCR_SL));		
	}
}    
?>