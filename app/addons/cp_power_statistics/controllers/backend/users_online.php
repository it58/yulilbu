<?php
use Tygh\Registry;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'manage')
{
    $params = $_REQUEST;
	
    list($users_online, $search) = fn_ps_get_users_online($params, Registry::get('settings.Appearance.admin_orders_per_page'),true);

    Registry::get('view')->assign('search', $search);
    
    Registry::get('view')->assign('users_online', $users_online);
    Registry::get('view')->assign('countries', fn_get_simple_countries(false, DESCR_SL));
    
    $country_users = fn_ps_get_country_users();

    Registry::get('view')->assign('country_users' , $country_users);

    $browser_users = fn_ps_get_browser_users();
    
    Registry::get('view')->assign('browser_users' , $browser_users);

}

if ($mode == 'm_delete') {
	foreach ($_REQUEST['url_ids'] as $v) {
		fn_delete_transition($v);
	}
}

if ($mode == 'delete') {
	fn_delete_transition($_REQUEST['url_id']);
	return array(CONTROLLER_STATUS_REDIRECT);
}

if ($mode == 'delete_all_urls_without_order') {
	$result = db_query("DELETE FROM ?:conversion_statistics WHERE order_id = ?i", 0);
	fn_set_notification('N', __("notice"), __("transition_without_order_has_been_deleted"));
	fn_redirect(fn_url('addons.manage'));
}
