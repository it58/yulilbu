<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'get_info') {
	if ($_REQUEST['extended_information'] == 'only_countries' || $_REQUEST['extended_information'] == 'all_information') {
	  $country_users = fn_ps_get_country_users();

	  Registry::get('view')->assign('country_users' , $country_users);
	  Registry::get('view')->assign('countries', fn_get_simple_countries(false, DESCR_SL));	
	}

	if ($_REQUEST['extended_information'] == 'only_browsers' || $_REQUEST['extended_information'] == 'all_information') {	
	  $browser_users = fn_ps_get_browser_users();
	  Registry::get('view')->assign('browser_users' , $browser_users);
	}

	Registry::get('view')->assign('view_extinfo' , $_REQUEST['view_extinfo']);
	
        if (defined('AJAX_REQUEST')) {
            Registry::set('runtime.root_template', 'addons/cp_power_statistics/views/users_online/get_info.tpl');
        }
}

?>