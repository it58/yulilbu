<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$_cur_transition_data['client_ip'] = fn_get_ip(true);
$_cur_transition_data['client_ip'] = $_cur_transition_data['client_ip']['host'];
$_cur_transition_data['country_code'] = fn_get_country_by_ip($_cur_transition_data['client_ip']);
$_cur_transition_data['browser'] = getBrowser();
$_cur_transition_data['browser'] = !empty($_cur_transition_data['browser']['short_name']) ? $_cur_transition_data['browser']['short_name'] : 'unknown';

// Get clear transition storefront url
$_cur_transition_data['from_domain'] = !empty($_SERVER['HTTP_REFERER']) ? parse_url($_SERVER['HTTP_REFERER']) : '';
$_cur_transition_data['from_domain'] = !empty($_cur_transition_data['from_domain']['host']) ? $_cur_transition_data['from_domain']['host'] : '';
$_cur_transition_data['from_domain'] = str_replace('www.', '', $_cur_transition_data['from_domain']);

// Get clear current storefront url
$_cur_storefront_domain = explode('/', Registry::get('runtime.company_data.storefront'));
$_cur_storefront_domain = $_cur_storefront_domain[0];
$_cur_storefront_domain = str_replace('www.', '', $_cur_storefront_domain);

if (!empty($_cur_transition_data['client_ip']) && $_cur_transition_data['client_ip'] != ip2long($_SERVER['SERVER_ADDR']) && defined('CRAWLER') === false) {
	// Write information about current user (for user online)
	if ($controller != 'image' && $mode != 'captcha') {
	// Captacha makes connection. We need to exclude it. It is needed only ourside condition with checking of current domain

	  $user_online['hash_id'] = fn_get_cookie('cps_user_online');
	  
	  if (empty($user_online['hash_id'])) {
	    $user_online['hash_id'] = mt_rand(100, 999) . mt_rand(100, 999);
	  }

	  $user_online['from_url'] = db_get_field('SELECT from_url FROM ?:users_online WHERE hash_id = ?i', $user_online['hash_id']);
	  
	  if (is_null($user_online['from_url']) || empty($user_online['from_url'])) {
	    $user_online['from_url'] = !empty($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != 'null' ? $_SERVER['HTTP_REFERER'] : 'direct_transition';
	  }
	  
	  $user_online['client_ip'] = $_cur_transition_data['client_ip'];
	  $user_online['timestamp'] = TIME;
	  $user_online['country_code'] = !empty($_cur_transition_data['country_code']) ? $_cur_transition_data['country_code'] : '01';
	  $user_online['browser'] = strtolower($_cur_transition_data['browser']);
	  $user_online['company_id'] = Registry::get('runtime.company_id');
	  $user_online['current_url'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	  $check = db_get_field('SELECT COUNT(*) FROM ?:users_online WHERE client_ip = ?i AND browser = ?s AND ?i - timestamp < ?i', $user_online['client_ip'], 'unknown', TIME, 60*Registry::get('addons.cp_power_statistics.user_offline_time'));
	  
	  if (empty($check)) {  
	    db_query('REPLACE INTO ?:users_online ?e', $user_online);
	    fn_set_cookie('cps_user_online', $user_online['hash_id'], Registry::get('addons.cp_power_statistics.cookie_lifetime')*24*3600);
	 }
	    
	  fn_ps_delete_offline_users();
	}
	// Finish write information about current user
}

if ($_cur_transition_data['from_domain'] != $_cur_storefront_domain && !empty($_cur_transition_data['client_ip']) && $_cur_transition_data['client_ip'] != ip2long($_SERVER['SERVER_ADDR']) && defined('CRAWLER') === false) {

	// Write product referer
	if (!empty($_REQUEST['product_id']) || !empty($_REQUEST['category_id']) || !empty($_REQUEST['page_id'])) {
	  if (!empty($_REQUEST['product_id'])) {
	    $product_referer['product_id'] = $_REQUEST['product_id'];
	  } elseif (!empty($_REQUEST['category_id'])) {
	    $product_referer['category_id'] = $_REQUEST['category_id'];	  
	  } elseif (!empty($_REQUEST['page_id'])) {
	    $product_referer['page_id'] = $_REQUEST['page_id'];	  
	  }
	    
	  $product_referer['referer'] = !empty($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != 'null' ? $_SERVER['HTTP_REFERER'] : '';
	  $product_referer['timestamp'] = TIME;  
	  $product_referer['client_ip'] = $_cur_transition_data['client_ip'];
	  $product_referer['country_code'] = $_cur_transition_data['country_code'];  
	  $product_referer['browser'] = strtolower($_cur_transition_data['browser']); 
	  
	  fn_ps_add_referer($product_referer);
	  
	}
	// Finish write product referer
    fn_cp_power_statistics_add_views($_cur_transition_data);

	// Writing for conversion
	$_saved_transition_data = array();
	$unique_code = fn_get_cookie('transition_data');
	
	if (!empty($unique_code) && $unique_code != 'null') {
		$_saved_transition_data = db_get_array('SELECT * FROM ?:conversion_statistics WHERE unique_id = ?i', $unique_code);
	} else {
		$unique_code = mt_rand(100, 999) . mt_rand(100, 999);
		fn_set_cookie('transition_data', $unique_code, Registry::get('addons.cp_power_statistics.cookie_lifetime')*24*3600);
	}	

	if (empty($_saved_transition_data)) {
		$_cur_transition_data['first_transition'] = 'Y';
	}
	
	$last_key = count($_saved_transition_data) - 1;
	
	if(!isset($_saved_transition_data[$last_key]['timestamp'])){
		$_saved_transition_data[$last_key]['timestamp'] =0;
	}
	
	// Cookie will be saved only if request was made in save_transitions_timeout seconds
	if (($last_key + 1 < Registry::get('addons.cp_power_statistics.save_transitions_amout') 
	|| Registry::get('addons.cp_power_statistics.save_transitions_amout') == 0) 
	&& (TIME - $_saved_transition_data[$last_key]['timestamp'] >
	 Registry::get('addons.cp_power_statistics.save_transitions_timeout') 
	|| empty($_saved_transition_data[$last_key]['timestamp']))) {

		if (!empty($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != 'null') {
			$_cur_transition_data['from_url'] = $_SERVER['HTTP_REFERER'];
		} else {
			$_cur_transition_data['from_url'] = '';
		}
		
		$_cur_transition_data['to_url'] = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$_cur_transition_data['timestamp'] = TIME;
		$company_id = Registry::get('runtime.company_id');
		$_cur_transition_data['company_id'] = !empty($company_id) ? $company_id : 0;
		$_cur_transition_data = array($_cur_transition_data);
		$_merged_cookie_and_curr = array_merge($_saved_transition_data, $_cur_transition_data);
		foreach ($_merged_cookie_and_curr as $data) {
			$data['unique_id'] = $unique_code;	    
			$data['from_url'] = !empty($data['from_url']) ? $data['from_url'] : 'direct_transition';
			$data['from_domain'] = !empty($data['from_domain']) ? $data['from_domain'] : 'direct_transition';
			
			db_query('REPLACE INTO ?:conversion_statistics ?e', $data);
		}	
	}
}