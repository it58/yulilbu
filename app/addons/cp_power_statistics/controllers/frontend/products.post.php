<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;
use Tygh\Session;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

// Products search
if ($mode == 'search') {
    
    // Get all search params
    $search_params = $_REQUEST;
    unset($search_params['dispatch']);
    unset($search_params['page']);
    unset($search_params['result_ids']);
    unset($search_params['x']);
    unset($search_params['y']);
    $search_params['match'] = empty($search_params['match']) ? 'any' : $search_params['match']; // any, all, exact
    
    foreach ($search_params as $k => $v) {
        if (empty($v)) {
            unset($search_params[$k]);
            continue;
        }
        $search_params[$k] = $v;
    }
    
    ksort($search_params);
    $search_params = serialize($search_params);
    $md5_search_params = md5($search_params);
    $search = Registry::get('view')->getTemplateVars('search');
    $product_count = !empty($search['total_items']) ? $search['total_items'] : 0;
    
    // Save search params
    $sess_id = db_get_field("SELECT sess_id FROM ?:stat_sessions WHERE session = ?s AND expiry > ?i ORDER BY timestamp DESC LIMIT 1", Session::getId(), TIME);
    if (!empty($sess_id)) {
        $record_exist = db_get_field("SELECT sess_id FROM ?:stat_product_search WHERE sess_id = ?i AND md5 = ?s", $sess_id, $md5_search_params);
        if (!$record_exist) {
            $_data = array(
                'sess_id' => $sess_id,
                'search_string' => $search_params,
                'md5' => $md5_search_params,
                'quantity' => $product_count
            );
            fn_set_data_company_id($_data);
            
            db_query('INSERT INTO ?:stat_product_search ?e', $_data);
        }
    }
}

if ($mode == 'view' || $mode == 'quick_view') {
    $product = Registry::get('view')->tpl_vars['product']->value;
    
    $global_popularity = db_get_row('SELECT show_popularity, show_viewed, show_added, show_deleted, show_bought, show_total, popularity_view, show_popularity_header, show_orders, orders_view, show_orders_header, show_orders_items, show_only_total_popularity, show_only_total_orders, global_total_plus FROM ?:products WHERE global_popularity = ?s', 'Y');
    
    if (!empty($global_popularity)) {
        $product = array_merge($product, $global_popularity);
    }
    
    $ps_count_rows_popularity = 0;
    
    if ($product['show_viewed'] == 'Y') {
        $ps_count_rows_popularity++;
    }
    if ($product['show_added'] == 'Y') {
        $ps_count_rows_popularity++;
    }
    if ($product['show_deleted'] == 'Y') {
        $ps_count_rows_popularity++;
    }
    if ($product['show_bought'] == 'Y') {
        $ps_count_rows_popularity++;
    }
    if ($product['show_total'] == 'Y') {
        $ps_count_rows_popularity++;
    }
    
    Registry::get('view')->assign('ps_count_rows_popularity', $ps_count_rows_popularity);
    Registry::get('view')->assign('ps_popularity', db_get_row('SELECT * FROM ?:product_popularity WHERE product_id = ?i', $_REQUEST['product_id']));
    
    $ps_orders = fn_ps_get_orders($_REQUEST['product_id']);
    
    Registry::get('view')->assign('ps_orders', $ps_orders);
    
    if (!empty($product['show_orders_items']) && !is_array($product['show_orders_items'])) {
        $product['show_orders_items'] = explode(',', $product['show_orders_items']);
    }
    
    $total_orders = !empty($product['total_plus']) ? $product['total_plus'] : 0;
    foreach ($ps_orders as $o) {
        if(!empty($product['show_orders_items'])) {
            if (in_array($o['status'], $product['show_orders_items'])) {
                $total_orders += $o['count'];
            }
        }
    }
    
    if (!empty($global_popularity['global_total_plus'])) {
        $total_orders += $global_popularity['global_total_plus'];
    }
    
    Registry::get('view')->assign('total_orders', $total_orders);
    
    Registry::get('view')->assign('product', $product);
    
    
    fn_cp_power_statistics_set_product_popularity($_REQUEST['product_id']);
}
