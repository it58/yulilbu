<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'get_info') {

	list($products, $search) = fn_ps_get_latest_products($_REQUEST);
	
        Registry::get('view')->assign('ps_search', $search);
        Registry::get('view')->assign('products' , $products);

        $selected_layout = 'products_without_options';
        Registry::get('view')->assign('show_qty', true);
        Registry::get('view')->assign('products', $products);
        Registry::get('view')->assign('search', $search);
        Registry::get('view')->assign('selected_layout', $selected_layout);	
        
        Registry::get('view')->assign('block_id', $_REQUEST['block_id']);        
        
        if (defined('AJAX_REQUEST')) {
            Registry::set('runtime.root_template', 'addons/cp_power_statistics/views/latest_purchases/get_info.tpl');
        }
}

?>