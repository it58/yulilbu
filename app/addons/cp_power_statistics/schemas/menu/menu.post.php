<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

$schema['central']['website']['items']['power_statistics'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'subitems' => array(
        'alt_users_online' => array(
            'href' => 'users_online.manage',
            'position' => 100
        ), 
        'def_users_online' => array(
            'href' => 'statistics.visitors&section=general&report=online',
            'position' => 200
        ),          
        'conversion_statistics' => array(
            'href' => 'conversion_statistics.manage&only_with_order=Y',
            'position' => 300
        ),
        'statistics_history' => array(
            'href' => 'statistics.reports?reports_group=general',
            'position' => 400,
            'subitems' => array (
                'general' => array(
                    'href' => 'statistics.reports?reports_group=general',
                    'position' => 600
                ),            
                'system' => array(
                    'href' => 'statistics.reports?reports_group=system',
                    'position' => 600
                ),
                'geography' => array(
                    'href' => 'statistics.reports?reports_group=geography',
                    'position' => 700
                ),
                'referrers' => array(
                    'href' => 'statistics.reports?reports_group=referrers',
                    'position' => 800
                ),
                'pages' => array(
                    'href' => 'statistics.reports?reports_group=pages',
                    'position' => 900
                ),
                'audience' => array(
                    'href' => 'statistics.reports?reports_group=audience',
                    'position' => 1000
                ),
                'products' => array(
                    'href' => 'statistics.reports?reports_group=products',
                    'position' => 1100
                ) 
            )
        ),         
        'most_popular_products' => array(
            'href' => 'products.manage&sort_by=popularity&sort_order=desc',
            'position' => 600
        ),
        'most_viewed_products' => array(
            'href' => 'products.manage&sort_by=viewed&sort_order=desc',
            'position' => 500
        ),               
        'cp_extended_statistics' => array(
            'href' => 'cp_extended_statistics.report',
            'position' => 800
        ),               
    ),
    'href' => 'conversion_statistics.manage',
    'position' => 4000,
);

if (Registry::get('addons.banners.status') == 'A') {
    $schema['central']['website']['items']['power_statistics']['subitems']['statistics_history']['subitems']['banners'] =  array(
            'href' => 'statistics.banners',
            'position' => 1200
        );
}
return $schema;
