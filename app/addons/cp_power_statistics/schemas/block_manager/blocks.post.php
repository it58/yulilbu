<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['ps_uonline'] = array (
        'templates' => array(
            'addons/cp_power_statistics/blocks/users_online.tpl' => array(),           
        ),
        'wrappers' => 'blocks/wrappers',
        'content' => array (
            'users_online' => array (
                'type' => 'function',
                'function' => array('fn_ps_get_count_users_online'),
            )
        ),
    'settings' => array (
        'users_online_view' => array (
        'type' => 'selectbox',
        'values' => array (
            'icon' => 'icon',
            'text' => 'text',
            'icon_text' => 'icon_text'    
        ),
        'default_value' => 'icon'
        ),
        'show_countries' => array (
        'type' => 'checkbox',
        'default_value' => 'Y'
        ),
        'show_browsers' => array (
        'type' => 'checkbox',
        'default_value' => 'Y'
        ),
        'extended_information' => array (
        'type' => 'selectbox',
        'values' => array (
            'none' => 'none',
            'only_countries' => 'only_countries',
            'only_browsers' => 'only_browsers',
            'all_information' => 'all_information',
        ),      
        'default_value' => 'all_information'
        ),
        'view_extended_information' => array (
        'type' => 'selectbox',
        'values' => array (
            'vertically' => 'vertically',
            'horizontally' => 'horizontally'
        ),      
        'default_value' => 'vertically'
        ),      
    ),    
);

$schema['latest_purchases'] = array (
        'templates' => array(
            'addons/cp_power_statistics/blocks/latest_purchases.tpl' => array(),           
        ),
        'wrappers' => 'blocks/wrappers',
	'settings' => array (
	    'products_limit' => array (
		'type' => 'input',
		'default_value' => '3'
	    )    
	)
);

return $schema;