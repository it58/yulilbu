<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/
use Tygh\Http;
use Tygh\Registry;
use Tygh\Settings;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_cp_power_statistics_init_secure_controllers(&$controllers)
{
    $controllers['statistics'] = 'passive';
}

function fn_cp_power_statistics_get_banners_post(&$banners, &$params)
{
    if (AREA == 'C' && !fn_is_empty($banners) && !defined('AJAX_REQUEST')) {
        foreach ($banners as $k => $v) {
            if ($v['type'] == 'T' && !empty($v['description'])) {
                $i = $pos = 0;
                $matches = array();
                while (preg_match('/href=([\'|"])(.*?)([\'|"])/i', $banners[$k]['description'], $matches, PREG_OFFSET_CAPTURE, $pos)) {
                    $banners[$k]['description'] = substr_replace($banners[$k]['description'], fn_url("statistics.banners?banner_id=$v[banner_id]&amp;link=" . $i++, 'C'), $matches[2][1], strlen($matches[2][0]));
                    $pos = $matches[2][1];
                }
            } elseif (!empty($v['url'])) {
                //$banners[$k]['url'] = "statistics.banners?banner_id=$v[banner_id]";
            }
            
            $banner_stat = array(
                'banner_id' => $v['banner_id'],
                'type' => 'V',
                'timestamp' => TIME
            );
            fn_set_data_company_id($banner_stat);
            
            db_query('INSERT INTO ?:stat_banners_log ?e', $banner_stat);
        }
    } else {
        return false;
    }
}

function fn_cp_power_statistics_delete_banners(&$banner_id)
{
    db_query("DELETE FROM ?:stat_banners_log WHERE banner_id = ?i", $banner_id);
}

function fn_cp_power_statistics_search_by_objects(&$conditions, &$params)
{
    if (!empty($conditions['products'])) {
        $obj = $conditions['products'];
        $params['products_found'] = db_get_field("SELECT COUNT(DISTINCT($obj[table].$obj[key])) FROM ?:products as $obj[table] $obj[join] WHERE $obj[condition]");
    }
}

function fn_cp_power_statistics_init_templater(&$view)
{
    if (AREA == 'C' && USER_AGENT == 'crawler' && !empty($_SERVER['HTTP_USER_AGENT']) && !defined('AJAX_REQUEST')) {
        $view->registerFilter('output', 'fn_statistics_track_robots');
    }
}

function fn_statistics_track_robots($tpl_output, &$view)
{
    if (strpos($tpl_output, '<title>') === false) {
        return $tpl_output;
    }
    
    $sess_id = db_get_field('SELECT sess_id FROM ?:stat_sessions WHERE uniq_code = ?i AND timestamp > ?i' . fn_get_ult_company_condition('?:stat_sessions.company_id'), fn_crc32($_SERVER['HTTP_USER_AGENT']), TIME - (24 * 60 * 60));
    
    if (empty($sess_id)) {
        $ip = fn_get_ip(true);
        $referer = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        $parse_url = parse_url($referer);
        
        $stat_data = array(
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'host_ip' => $ip['host'],
            'proxy_ip' => $ip['proxy'],
            'client_language' => !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '',
            'ip_id' => fn_stat_ip_exist($ip),
            'client_type' => 'B',
            'robot' => CRAWLER,
            'referrer' => $referer,
            'timestamp' => TIME,
            'referrer_scheme' => empty($parse_url['scheme']) ? '' : $parse_url['scheme'],
            'referrer_host' => empty($parse_url['host']) ? '' : $parse_url['host'],
            'expiry' => 0,
            'uniq_code' => fn_crc32($_SERVER['HTTP_USER_AGENT'])
        );
        fn_set_data_company_id($stat_data);
        
        $request_type = STAT_LAST_REQUEST;
        $sess_id = db_query('INSERT INTO ?:stat_sessions ?e', $stat_data);
        $last_url = '';
    } else {
        $last_url = db_get_field("SELECT url FROM ?:stat_requests WHERE sess_id = ?i AND (request_type & ?i) = ?i", $sess_id, STAT_LAST_REQUEST, STAT_LAST_REQUEST);
        db_query("UPDATE ?:stat_requests SET request_type = request_type & " . STAT_ORDINARY_REQUEST . " WHERE sess_id = ?s", $sess_id);
        $request_type = STAT_END_REQUEST;
    }
    
    // Add to stat requests
    $this_url = fn_stat_prepare_url(REAL_URL);
    if ($last_url != $this_url) {
        $title = '';
        if (preg_match_all('/\<title\>(.*?)\<\/title\>/', $tpl_output, $m)) {
            $title = fn_html_escape($m[1][0], true);
        }
        
        $ve = array(
            'sess_id' => $sess_id,
            'timestamp' => TIME,
            'url' => $this_url,
            'title' => $title,
            'https' => defined('HTTPS') ? 'Y' : 'N',
            'loadtime' => microtime(true) - MICROTIME,
            'request_type' => $request_type
        );
        fn_set_data_company_id($ve);
        
        db_query("INSERT INTO ?:stat_requests ?e", $ve);
    }
    
    return $tpl_output;
    
}

//
// CHECK: Do IP exist?
//
function fn_stat_ip_exist($ip)
{
    if (!empty($ip['host']) && fn_is_inet_ip($ip['host'], true)) {
        $ip_num = $ip['host'];
    } elseif (!empty($ip['proxy']) && fn_is_inet_ip($ip['proxy'], true)) {
        $ip_num = $ip['proxy'];
    }
    $ip_id = isset($ip_num) ? db_get_field("SELECT ip_id FROM ?:stat_ips WHERE ip = ?i" . fn_get_ult_company_condition('?:stat_ips.company_id'), $ip_num) : false;
    if (empty($ip_id) && !empty($ip_num)) {
        $ip_id = fn_stat_save_ip(array(
            'ip' => $ip_num
        ));
    }
    
    return empty($ip_id) ? false : $ip_id;
}

//
// Save IP data.
//
function fn_stat_save_ip($ip_data)
{
    if (!empty($ip_data['ip'])) {
        $ip_data['country_code'] = fn_get_country_by_ip($ip_data['ip']);
        fn_set_data_company_id($ip_data);
        
        return db_query('INSERT INTO ?:stat_ips ?e', $ip_data);
    }
    
    return false;
}

function fn_stat_prepare_url($url)
{
    $url = fn_stat_cut_www($url);
    $location = fn_stat_cut_www(Registry::get('config.http_location'));
    $s_location = fn_stat_cut_www(Registry::get('config.https_location'));
    
    // Remove url prefix
    if (strpos($url, $location) !== false) {
        $url = str_replace($location, '', $url);
        
    } elseif (strpos($url, $s_location) !== false) {
        $url = str_replace($s_location, '', $url);
    }
    
    return $url;
}

function fn_stat_cut_www($url)
{
    return str_replace('://www.', '://', $url);
}

function fn_stat_get_visitors_count($time_from, $time_to)
{
    if (fn_allowed_for('ULTIMATE')) {
        $company_condition = fn_get_company_condition('?:stat_sessions.company_id');
    } else {
        $company_condition = '';
    }
    $visitors = db_get_field('SELECT COUNT(*) FROM ?:stat_sessions WHERE `timestamp` BETWEEN ?i AND ?i AND client_type = ?s ?p', $time_from, $time_to, 'U', $company_condition);
    
    return $visitors;
}

/**
 * Get list search terms
 *
 * @param integer $limit List limit
 * @return array search terms list
 */
function fn_get_statistic_search_terms($limit = 0)
{
    $limit_query = "";
    if (!empty($limit)) {
        $limit_query = db_quote("LIMIT ?i", $limit);
    }
    
    $field_company = "";
    $company_condition = "";
    
    if (fn_allowed_for('ULTIMATE')) {
        $field_company = "?:stat_sessions.company_id, ";
        $company_condition = fn_get_company_condition('?:stat_sessions.company_id');
    }
    
    $search_terms = db_get_array("SELECT " . "?:stat_product_search.search_string, " . "ROUND(AVG(?:stat_product_search.quantity), 0) as quantity, " . "?p " . "COUNT(*) as count " . "FROM ?:stat_product_search " . "INNER JOIN ?:stat_sessions " . "ON ?:stat_sessions.sess_id = ?:stat_product_search.sess_id " . "WHERE 1 " . "?p " . "GROUP BY ?p ?:stat_product_search.md5 " . "ORDER BY count DESC " . "?p ", $field_company, $company_condition, $field_company, $limit_query);
    
    foreach ($search_terms as $key => $term) {
        $search_terms[$key]['search_string'] = !empty($term['search_string']) ? unserialize($term['search_string']) : array();
        if (fn_allowed_for('ULTIMATE')) {
            $search_terms[$key]['search_string']['company_id'] = $term['company_id'];
        }
    }
    
    return $search_terms;
}

/**
 * Hook for add statistics charts to dashboard
 *
 * @param integer $time_from Start time range
 * @param integer $time_to End time range
 * @param integer $graphs Charts data
 * @param integer $graph_tabs Tabs
 */
function fn_cp_power_statistics_dashboard_get_graphs_data(&$time_from, &$time_to, &$graphs, &$graph_tabs, &$is_day)
{
    if (!fn_check_view_permissions('statistics.reports', 'GET')) {
        return false;
    }
    
    $company_condition = '';
    if (fn_allowed_for('ULTIMATE')) {
        $company_condition = fn_get_company_condition('?:stat_requests.company_id');
    }
    
    for ($i = $time_from; $i <= $time_to; $i = $i + ($is_day ? 60 * 60 : SECONDS_IN_DAY)) {
        $date = !$is_day ? date("Y, (n-1), j", $i) : date("H", $i);
        if (empty($graphs['dashboard_statistics_visits_chart'][$date])) {
            $graphs['dashboard_statistics_visits_chart'][$date] = array(
                'cur' => 0,
                'prev' => 0
            );
        }
    }
    
    $visits = db_get_fields("SELECT timestamp FROM ?:stat_requests WHERE timestamp BETWEEN ?i AND ?i ?p GROUP BY sess_id ", $time_from, $time_to, $company_condition);
    foreach ($visits as $visit) {
        $date = !$is_day ? date("Y, (n-1), j", $visit) : date("H", $visit);
        $graphs['dashboard_statistics_visits_chart'][$date]['cur']++;
    }
    
    $visits_prev = db_get_fields("SELECT timestamp FROM ?:stat_requests WHERE timestamp BETWEEN ?i AND ?i ?p GROUP BY sess_id ", $time_from - ($time_to - $time_from), $time_from, $company_condition);
    foreach ($visits_prev as $visit) {
        $date = !$is_day ? date("Y, (n-1), j", $visit + ($time_to - $time_from)) : date("H", $visit + ($time_to - $time_from));
        $graphs['dashboard_statistics_visits_chart'][$date]['prev']++;
    }
    
    $graph_tabs['visits_chart'] = array(
        'title' => __('visits'),
        'js' => true
    );
}

function fn_cp_power_statistics_get_products($params, &$fields, &$sortings, $condition, &$join, &$sorting, $group_by, $lang_code, $having)
{
    
    if (AREA != 'C') {
        if (!isset($params['sort_by'])) {
            $params['sort_by'] = '';
        }
        
        $fields['viewed'] = 'popularity.viewed';
        $fields['popularity'] = 'popularity.total';
        
        $sortings['viewed'] = 'popularity.viewed';
        $sortings['popularity'] = 'popularity.total';
        
        if ($params['sort_by'] != 'popularity') {
            $join .= db_quote(" LEFT JOIN ?:product_popularity as popularity ON popularity.product_id = products.product_id");
        }
    }
    
    return true;
}

function fn_cp_power_statistics_get_products_pre(&$params, $items_per_page, $lang_code)
{
    if (!empty($params['get_latest_pruchases']) && $params['get_latest_pruchases'] == true) {
        
        if (!empty($params['limit'])) {
            $limit = db_quote('LIMIT 0, ?i', $params['limit']);
        }
        
        if (!empty($params['cid'])) {
            $condition = db_quote(' AND category_id IN (?n)', $params['cid']);
        }
        
        $params['pid'] = db_get_fields("SELECT DISTINCT ?:order_details.product_id FROM ?:order_details LEFT JOIN ?:products_categories ON ?:products_categories.product_id = ?:order_details.product_id WHERE 1 $condition ORDER by ?:order_details.order_id DESC $limit");
    }
    
    
    
    return true;
}

function fn_ps_get_latest_products($params = array())
{
    
    if (!empty($params['products_limit'])) {
        $limit = db_quote('LIMIT 0, ?i', $params['products_limit']);
    }
    
    $params['pid'] = db_get_fields('SELECT DISTINCT ?:order_details.product_id FROM ?:order_details ORDER by ?:order_details.order_id DESC ' . $limit);
    
    $pids = array_flip($params['pid']);
    
    if (empty($params['pid'])) {
        $params['pid'] = 'undefined';
    }
    
    list($products, $search) = fn_get_products($params, Registry::get('settings.Appearance.products_per_page'));
    
    unset($pids);
    
    // Order in the same way above. Not good. We need add condition to get_products
    foreach ($products as $pr) {
        $pids[$pr['product_id']] = $pr;
    }
    $products = $pids;
    // Order in the same way above. Not good. We need add condition to get_products 
    
    fn_gather_additional_products_data($products, array(
        'get_icon' => true,
        'get_detailed' => true,
        'get_additional' => true,
        'get_options' => true
    ));
    
    return array(
        $products,
        $search
    );
}

function fn_ps_get_latest_purchases()
{
    return true;
}

function fn_ps_add_referer($referer = array())
{
    if (!empty($referer['product_id'])) {
        $table = 'products_referers';
        $referer['item_id'] = $referer['product_id'];
    } elseif (!empty($referer['category_id'])) {
        $table = 'categories_referers';
        $referer['item_id'] = $referer['category_id'];
    } elseif (!empty($referer['page_id'])) {
        $table = 'pages_referers';
        $referer['item_id'] = $referer['page_id'];
    }
    
    // Not very good to collect full statistic. But good to predict ddos
    if (empty($referer['country_code']) || empty($referer['browser']) || empty($referer['client_ip'])) {
        return true;
    }
    
    if (!empty($referer['item_id'])) {
        $check = db_get_field("SELECT COUNT(*) FROM ?:$table WHERE item_id = ?i AND ?i - timestamp <= ?i", $referer['item_id'], TIME, Registry::get('addons.cp_power_statistics.save_transitions_timeout'));
        
        if (!empty($check)) {
            return true;
        }
        
        if (!empty($referer['referer'])) {
            db_query("REPLACE INTO ?:$table ?e", $referer);
        }
    }
    
    return true;
}

function fn_cp_power_statistics_add_orders($order_id)
{
    $ip = fn_get_ip(true);
    $country_code = fn_get_country_by_ip($ip['host']);
    $company_id = db_get_field("SELECT company_id FROM ?:orders WHERE order_id = ?i", $order_id);
    
    $_data = array (
        'country_code' => $country_code,
        'order_count' => 1,
        'timestamp' => mktime(0, 0, 0, date('n'), date('j'), date('Y')),
        'company_id' => $company_id
    );

    db_query("INSERT INTO ?:cp_statistics_country_orders ?e ON DUPLICATE KEY UPDATE order_count = order_count + 1", $_data);

    return false;
    
}


function fn_cp_power_statistics_add_views($referer = array())
{
    if (empty($_SESSION['cp_statistics_country_views'])) {
        $_SESSION['cp_statistics_country_views'] = array();
    }
    
    $country_code = isset($referer['country_code']) ? $referer['country_code'] : '0';
    if (empty($_SESSION['cp_statistics_country_views'][$country_code])) {
        $_data = array (
            'country_code' => $country_code,
            'views' => 1,
            'timestamp' => mktime(0, 0, 0, date('n'), date('j'), date('Y')),
        );

        db_query("INSERT INTO ?:cp_statistics_country_views ?e ON DUPLICATE KEY UPDATE views = views + 1", $_data);

        $_SESSION['cp_statistics_country_views'][$country_code] = true;

        return true;
    }

    return false;
    
}

function fn_ps_get_orders($product_id = 0)
{
    $orders = db_get_hash_array('SELECT COUNT(DISTINCT(?:order_details.order_id)) as count, ?:orders.status as status  FROM ?:orders LEFT JOIN ?:order_details ON ?:order_details.order_id = ?:orders.order_id WHERE ?:order_details.product_id = ?i GROUP by status', 'status', $product_id);
    return $orders;
}

function fn_cp_power_statistics_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
{
    if (!empty($product_data['show_orders_items'])) {
        $product_data['show_orders_items'] = explode(',', $product_data['show_orders_items']);
    }
    
    return true;
}

function fn_cp_power_statistics_update_product_pre(&$product_data, $product_id, $lang_code, $can_update = '')
{
    if (!empty($product_data['global_popularity']) && $product_data['global_popularity'] == 'Y') {
        db_query('UPDATE ?:products SET global_popularity = ?s', 'N');
    }
    
    if (!empty($product_data['show_orders_items'])) {
        $product_data['show_orders_items'] = implode(',', $product_data['show_orders_items']);
    } else {
        $product_data['show_orders_items'] = '';
    }
    return true;
}

function fn_ps_get_country_users()
{
    $condition = '';
    if (Registry::get('runtime.company_id')) {
        $condition = db_quote('AND  company_id = ?i', Registry::get('runtime.company_id'));
    }
    
    return db_get_hash_array("SELECT count(*) as users_count, country_code FROM ?:users_online WHERE 1 $condition GROUP by country_code", 'country_code', Registry::get('runtime.company_id'));
}

function fn_ps_get_browser_users()
{
    $condition = '';
    if (Registry::get('runtime.company_id')) {
        $condition = db_quote('AND  company_id = ?i', Registry::get('runtime.company_id'));
    }
    
    return db_get_hash_array("SELECT count(*) as users_count, browser FROM ?:users_online WHERE 1 $condition GROUP by browser", 'browser', Registry::get('runtime.company_id'));
}

function fn_get_browser_name($short_name)
{
    $full_names = array(
        'msie' => 'Internet Explorer',
        'firefox' => 'Mozilla Firefox',
        'chrome' => 'Google Chrome',
        'safari' => 'Apple Safari',
        'opera' => 'Opera',
        'netscape' => 'Netscape'
    );
    
    $full_name = $full_names[$short_name];
    
    return $full_name;
}

function getBrowser()
{
    $u_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";
    
    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    $ub = "Chrome";
    
    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }
    
    // finally get the correct version number
    $known = array(
        'Version',
        $ub,
        'other'
    );
    $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            if (!empty($matches['version'][0])) {
                $version = $matches['version'][0];
            }
        } else {
            $version = isset($matches['version'][1]) ? $matches['version'][1] : '';
        }
    } else {
        $version = $matches['version'][0];
    }
    
    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }
    
    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'short_name' => $ub,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}

function fn_ps_get_users_online($params, $items_per_page, $get_totals = false)
{
    $params = LastView::instance()->update('users_online', $params);
    
    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );
    
    $params = array_merge($default_params, $params);
    
    $fields = array(
        '?:users_online.hash_id',
        '?:users_online.client_ip',
        '?:users_online.timestamp',
        '?:users_online.country_code',
        '?:users_online.browser',
        '?:users_online.company_id',
        '?:users_online.current_url',
        '?:users_online.from_url'
    );
    
    // Define sort fields
    $sortings = array(
        'hash_id' => '?:users_online.hash_id',
        'client_ip' => '?:users_online.client_ip',
        'timestamp' => '?:users_online.timestamp',
        'country_code' => '?:users_online.country_code',
        'browser' => '?:users_online.browser',
        'current_url' => '?:users_online.current_url',
        'from_url' => '?:users_online.from_url'
    );
    
    $sorting = db_sort($params, $sortings, 'hash_id', 'desc');
    
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(?:users_online.hash_id) FROM ?:users_online WHERE 1");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    
    $users_online = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:users_online WHERE 1 $sorting $limit");
    
    
    LastView::instance()->processResults('users_online', $users_online, $params);
    
    return array(
        $users_online,
        $params
    );
}

function fn_ps_get_referers($params, $items_per_page, $get_totals = false)
{
    if (!empty($params['product_id'])) {
        $table = 'products_referers';
        $condition = db_quote(' AND item_id = ?i', $params['product_id']);
    } elseif (!empty($params['category_id'])) {
        $table = 'categories_referers';
        $condition = db_quote(' AND item_id = ?i', $params['category_id']);
    } elseif (!empty($params['page_id'])) {
        $table = 'pages_referers';
        $condition = db_quote(' AND item_id = ?i', $params['page_id']);
    } else {
        return true;
    }
    
    $params = LastView::instance()->update('referers', $params);
    
    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );
    
    $params = array_merge($default_params, $params);
    
    $fields = array(
        "?:$table.item_id",
        "?:$table.referer",
        "?:$table.timestamp",
        "?:$table.client_ip",
        "?:$table.country_code",
        "?:$table.browser",
        "?:$table.timestamp"
    );
    
    // Define sort fields
    $sortings = array(
        'item_id' => "?:$table.item_id",
        'referer' => "?:$table.referer",
        'client_ip' => "?:$table.client_ip",
        'country_code' => "?:$table.country_code",
        'browser' => "?:$table.browser",
        'timestamp' => "?:$table.timestamp"
    );
    
    $sorting = db_sort($params, $sortings, 'item_id', 'desc');
    
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(?:$table.item_id) FROM ?:$table WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    
    $referers = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:$table WHERE 1 $condition $sorting $limit");
    
    
    LastView::instance()->processResults('products_referers', $referers, $params);
    
    return array(
        $referers,
        $params
    );
}

function fn_ps_get_count_users_online()
{
    fn_ps_delete_offline_users();
    
    $condition = '';
    if (Registry::get('runtime.company_id')) {
        $condition = db_quote('AND  company_id = ?i', Registry::get('runtime.company_id'));
    }
    
    return db_get_field("SELECT count(*) FROM ?:users_online WHERE 1 $condition");
}

function fn_ps_delete_offline_users()
{
    
    $condition = '';
    if (Registry::get('runtime.company_id')) {
        $condition = db_quote('AND  company_id = ?i', Registry::get('runtime.company_id'));
    }
    
    $users_online = db_get_array('SELECT * FROM ?:users_online');
    
    db_query('TRUNCATE ?:users_online');
    
    foreach ($users_online as $kk => $user_online) {
        if ((TIME - $user_online['timestamp']) < 60 * Registry::get('addons.cp_power_statistics.user_offline_time')) {
            db_query('REPLACE INTO ?:users_online ?e', $user_online);
        }
    }
    
    //    @db_query("DELETE FROM ?:users_online WHERE ?i - timestamp > ?i AND company_id = ?i $condition", TIME, 60*Registry::get('addons.cp_power_statistics.user_offline_time'), Registry::get('runtime.company_id'));
    
    return true;
}

function fn_cp_power_statistics_place_order($order_id, $action, $order_status, $cart, $auth)
{
    $unique_code = fn_get_cookie('transition_data');
    
    if (!empty($unique_code) && $unique_code != 'null') {
        db_query('UPDATE ?:conversion_statistics SET order_id = ?i WHERE unique_id = ?i', $order_id, $unique_code);
    }
    
    fn_set_cookie('transition_data', '', '');
    
    fn_cp_power_statistics_add_orders($order_id);
    return true;
}
function fn_get_transitions($params, $items_per_page, $get_totals = false)
{
    $params = LastView::instance()->update('transitions', $params);
    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );
    $params = array_merge($default_params, $params);
    $params['include_incompleted'] = empty($params['include_incompleted']) ? false : $params['include_incompleted']; // default incomplited orders should not be displayed
    if (!empty($params['status']) && (is_array($params['status']) && in_array(STATUS_INCOMPLETED_ORDER, $params['status']) || !is_array($params['status']) && $params['status'] == STATUS_INCOMPLETED_ORDER)) {
        $params['include_incompleted'] = true;
    }
    
    $fields = array(
        '?:conversion_statistics.order_id',
        '?:conversion_statistics.url_id',
        '?:conversion_statistics.unique_id',
        '?:conversion_statistics.from_url',
        '?:conversion_statistics.to_url',
        '?:conversion_statistics.from_domain',
        '?:conversion_statistics.timestamp',
        '?:conversion_statistics.client_ip',
        '?:conversion_statistics.first_transition',
        '?:conversion_statistics.country_code',
        '?:orders.status',
        '?:orders.timestamp AS order_timestamp',
        '?:orders.total'
    );
    
    // Define sort fields
    $sortings = array(
        'order_id' => '?:conversion_statistics.order_id',
        'unique_id' => '?:conversion_statistics.unique_id',
        'from_url' => '?:conversion_statistics.from_url',
        'to_url' => '?:conversion_statistics.to_url',
        'from_domain' => '?:conversion_statistics.from_domain',
        'timestamp' => array(
            "?:conversion_statistics.timestamp",
            "?:conversion_statistics.order_id"
        )
    );
    
    /*conditions*/
    
    $condition = $_condition = $join = $group = '';
    $join .= " LEFT JOIN ?:orders ON ?:orders.order_id = ?:conversion_statistics.order_id";
    //fn_print_r($params);
    if (isset($params['q']) && fn_string_not_empty($params['q'])) {
        $params['q'] = trim($params['q']);
        if ($params['match'] == 'any') {
            $pieces = fn_explode(' ', $params['q']);
            $search_type = ' OR ';
        } elseif ($params['match'] == 'all') {
            $pieces = fn_explode(' ', $params['q']);
            $search_type = ' AND ';
        } else {
            $pieces = array(
                $params['q']
            );
            $search_type = '';
        }
        
        $_condition = array();
        foreach ($pieces as $piece) {
            if (strlen($piece) == 0) {
                continue;
            }
            $tmp = db_quote("(?:conversion_statistics.unique_id LIKE ?l)", '%' . $piece . '%');
            if ($params['qfrom_url'] == 'Y') {
                $tmp .= db_quote(" OR ?:conversion_statistics.from_url LIKE ?l", '%' . $piece . '%');
                $tmp .= db_quote(" OR ?:conversion_statistics.from_url LIKE ?l", '%' . htmlentities($piece, ENT_QUOTES, 'UTF-8') . '%');
            }
            if ($params['qfrom_domain'] == 'Y') {
                $tmp .= db_quote(" OR ?:conversion_statistics.from_domain LIKE ?l", '%' . $piece . '%');
                $tmp .= db_quote(" OR ?:conversion_statistics.from_domain LIKE ?l", '%' . htmlentities($piece, ENT_QUOTES, 'UTF-8') . '%');
            }
            if ($params['qto_url'] == 'Y') {
                $tmp .= db_quote(" OR ?:conversion_statistics.to_url LIKE ?l", '%' . $piece . '%');
                $tmp .= db_quote(" OR ?:conversion_statistics.to_url LIKE ?l", '%' . htmlentities($piece, ENT_QUOTES, 'UTF-8') . '%');
            }
            ;
            $_condition[] = '(' . $tmp . ')';
        }
        $_cond = implode($search_type, $_condition);
        
        if (!empty($_condition)) {
            $condition .= ' AND ' . $_cond;
        }
        unset($_condition);
    }
    if (isset($params['only_with_order']) && $params['only_with_order'] == 'Y') {
        $condition .= db_quote(' AND ?:orders.order_id = ?:conversion_statistics.order_id ');
    }
    if (isset($params['only_first_transition']) && $params['only_first_transition'] == 'Y') {
        $condition .= db_quote(" AND  ?:conversion_statistics.first_transition = 'Y' ");
    }
    if (!empty($params['user_id'])) {
        $condition .= db_quote(' AND ?:orders.user_id IN (?n)', $params['user_id']);
    }
    if (!empty($params['admin_user_id'])) {
        $condition .= db_quote(" AND ?:new_orders.user_id = ?i", $params['admin_user_id']);
        $join .= " LEFT JOIN ?:new_orders ON ?:new_orders.order_id = ?:conversion_statistics.order_id";
    }
    if (!empty($params['status'])) {
        $condition .= db_quote(' AND ?:orders.status IN (?a)', $params['status']);
    }
    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        
        $condition .= db_quote(" AND (?:orders.timestamp >= ?i AND ?:orders.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }
    if (!empty($params['tr_period']) && $params['tr_period'] != 'A') {
        
        list($params['tr_time_from'], $params['tr_time_to']) = fn_create_transition_periods($params);
        $condition .= db_quote(" AND (?:conversion_statistics.timestamp >= ?i AND ?:conversion_statistics.timestamp <= ?i)", $params['tr_time_from'], $params['tr_time_to']);
    }
    if (!empty($params['order_id'])) {
        $condition .= db_quote(' AND' . ' ?:orders.order_id IN (?n)', (!is_array($params['order_id']) && (strpos($params['order_id'], ',') !== false) ? explode(',', $params['order_id']) : $params['order_id']));
    }
    if (!empty($params['unique_id'])) {
        $condition .= db_quote('AND' . ' ?:conversion_statistics.unique_id IN (?n)', (!is_array($params['unique_id']) && (strpos($params['unique_id'], ',') !== false) ? explode(',', $params['unique_id']) : $params['unique_id']));
    }
    if (!empty($params['country_code'])) {
        $condition .= db_quote('AND' . ' ?:conversion_statistics.country_code IN (?a)', (!is_array($params['country_code']) && (strpos($params['country_code'], ',') !== false) ? explode(',', $params['country_code']) : $params['country_code']));
    }
    if (!empty($params['client_ip'])) {
        $condition .= db_quote('AND' . ' ?:conversion_statistics.client_ip = ?i', (!empty($params['client_ip']) ? $params['client_ip'] : ''));
    }
    
    /*for get_all search*/
    if (isset($params['all_no_order']) && $params['all_no_order'] == 'no_order') {
        $condition .= db_quote(" AND ?:conversion_statistics.order_id =?i", 0);
    }
    if (isset($params['all_order_id']) && fn_string_not_empty($params['all_order_id'])) {
        $condition .= db_quote(" AND ?:conversion_statistics.order_id LIKE ?l", trim($params['all_order_id']));
    }
    if (isset($params['all_from_url']) && fn_string_not_empty($params['all_from_url'])) {
        $condition .= db_quote(" AND ?:conversion_statistics.from_url LIKE ?l", trim($params['all_from_url']));
    }
    if (isset($params['all_from_domain']) && fn_string_not_empty($params['all_from_domain'])) {
        $condition .= db_quote(" AND ?:conversion_statistics.from_domain LIKE ?l", trim($params['all_from_domain']));
    }
    if (isset($params['all_to_url']) && fn_string_not_empty($params['all_to_url'])) {
        $condition .= db_quote(" AND ?:conversion_statistics.to_url LIKE ?l", trim($params['all_to_url']));
    }
    /*total*/
    if (isset($params['total_from']) && fn_is_numeric($params['total_from'])) {
        $condition .= db_quote(" AND ?:orders.total >= ?d", fn_convert_price($params['total_from']));
    }
    
    if (!empty($params['total_to']) && fn_is_numeric($params['total_to'])) {
        $condition .= db_quote(" AND ?:orders.total <= ?d", fn_convert_price($params['total_to']));
    }
    /*for many companies*/
    $join .= ' LEFT JOIN ?:companies ON ?:companies.company_id = ?:conversion_statistics.company_id';
    $condition .= db_quote(' AND ?:companies.company_id = ?i', Registry::get('runtime.company_data.company_id'));
    
    /*conditions*/
    if ($params['only_with_order'] == 'Y') {
        $sorting = db_sort($params, $sortings, 'order_id', 'desc');
    } else {
        $sorting = db_sort($params, $sortings, 'timestamp', 'desc');
    }
    
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(?:conversion_statistics.order_id) FROM ?:conversion_statistics $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    //fn_print_r($join, $condition, Registry::get('runtime.company_data.company_id'));
    $transitions = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:conversion_statistics $join WHERE 1 $condition $group $sorting $limit");
    
    if ($get_totals == true) {
        $group .= " GROUP BY ?:orders.order_id ";
        $paid_statuses = array(
            'P',
            'C'
        );
        $totals = array(
            'gross_total' => db_get_field("SELECT sum(t.total) FROM ( SELECT total FROM ?:conversion_statistics $join WHERE 1 $condition $group) as t"),
            'totally_paid' => db_get_field("SELECT sum(t.total) FROM ( SELECT total FROM ?:conversion_statistics $join WHERE ?:orders.status IN (?a) $condition $group) as t", $paid_statuses)
        );
        
        $params['paid_statuses'] = $paid_statuses;
    }
    
    LastView::instance()->processResults('transitions', $transitions, $params);
    
    return array(
        $transitions,
        $params,
        ($get_totals == true ? $totals : array())
    );
}

/*function for second period selector in advanced serch*/
function fn_create_transition_periods($params)
{
    $today = getdate(TIME);
    $period = !empty($params['tr_period']) ? $params['tr_period'] : null;
    
    $time_from = !empty($params['tr_time_from']) ? fn_parse_date($params['tr_time_from']) : 0;
    $time_to = !empty($params['tr_time_to']) ? fn_parse_date($params['tr_time_to'], true) : TIME;
    
    // Current dates
    if ($period == 'D') {
        $time_from = mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']);
        $time_to = TIME;
        
    } elseif ($period == 'W') {
        $wday = empty($today['wday']) ? "6" : (($today['wday'] == 1) ? "0" : $today['wday'] - 1);
        $wstart = getdate(strtotime("-$wday day"));
        $time_from = mktime(0, 0, 0, $wstart['mon'], $wstart['mday'], $wstart['year']);
        $time_to = TIME;
        
    } elseif ($period == 'M') {
        $time_from = mktime(0, 0, 0, $today['mon'], 1, $today['year']);
        $time_to = TIME;
        
    } elseif ($period == 'Y') {
        $time_from = mktime(0, 0, 0, 1, 1, $today['year']);
        $time_to = TIME;
        
        // Last dates
    } elseif ($period == 'LD') {
        $today = getdate(strtotime("-1 day"));
        $time_from = mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']);
        $time_to = mktime(23, 59, 59, $today['mon'], $today['mday'], $today['year']);
        
    } elseif ($period == 'LW') {
        $today = getdate(strtotime("-1 week"));
        $wday = empty($today['wday']) ? 6 : (($today['wday'] == 1) ? 0 : $today['wday'] - 1);
        $wstart = getdate(strtotime("-$wday day", mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year'])));
        $time_from = mktime(0, 0, 0, $wstart['mon'], $wstart['mday'], $wstart['year']);
        
        $wend = getdate(strtotime("+6 day", $time_from));
        $time_to = mktime(23, 59, 59, $wend['mon'], $wend['mday'], $wend['year']);
        
    } elseif ($period == 'LM') {
        $today = getdate(strtotime("-1 month"));
        $time_from = mktime(0, 0, 0, $today['mon'], 1, $today['year']);
        $time_to = mktime(23, 59, 59, $today['mon'], date('t', strtotime("-1 month")), $today['year']);
        
    } elseif ($period == 'LY') {
        $today = getdate(strtotime("-1 year"));
        $time_from = mktime(0, 0, 0, 1, 1, $today['year']);
        $time_to = mktime(23, 59, 59, 12, 31, $today['year']);
        
        // Last dates
    } elseif ($period == 'HH') {
        $today = getdate(strtotime("-23 hours"));
        $time_from = mktime($today['hours'], $today['minutes'], $today['seconds'], $today['mon'], $today['mday'], $today['year']);
        $time_to = TIME;
        
    } elseif ($period == 'HW') {
        $today = getdate(strtotime("-6 day"));
        $time_from = mktime($today['hours'], $today['minutes'], $today['seconds'], $today['mon'], $today['mday'], $today['year']);
        $time_to = TIME;
        
    } elseif ($period == 'HM') {
        $today = getdate(strtotime("-29 day"));
        $time_from = mktime($today['hours'], $today['minutes'], $today['seconds'], $today['mon'], $today['mday'], $today['year']);
        $time_to = TIME;
        
    } elseif ($period == 'HC') {
        $today = getdate(strtotime('-' . $params['last_days'] . ' day'));
        $time_from = mktime($today['hours'], $today['minutes'], $today['seconds'], $today['mon'], $today['mday'], $today['year']);
        $time_to = TIME;
    }
    
    Registry::get('view')->assign('time_from', $time_from);
    Registry::get('view')->assign('time_to', $time_to);
    
    return array(
        $time_from,
        $time_to
    );
}

function fn_delete_transition($url_id)
{
    $result = db_query("DELETE FROM ?:conversion_statistics WHERE url_id = ?i", $url_id);
    fn_set_notification('N', __("notice"), __("transition_has_been_deleted"));
    
    return $result;
}
function fn_conversion_statistics_delete_urls()
{
    return __('transition_delete_urls', array(
        '[delete_url]' => fn_url('conversion_statistics.delete_all_urls_without_order')
    ));
}
function fn_display_conversion_totals($transitions)
{
    $result = array();
    $result['gross_total'] = 0;
    $result['totally_paid'] = 0;
    
    if (is_array($transitions)) {
        foreach ($transitions as $k => $v) {
            $result['gross_total'] += $v['total'];
            if ($v['status'] == 'C' || $v['status'] == 'P') {
                $result['totally_paid'] += $v['total'];
            }
        }
    }
    
    return $result;
}

function fn_cp_power_statistics_pre_add_to_wishlist($product_data, $wishlist, $auth)
{

    foreach ($product_data as $product) {
        $_data = array (
            'product_id' => $product['product_id'],
            'favorites' => 1,
            'timestamp' => mktime(date('H'), 0, 0, date('n'), date('j'), date('Y'))
        );
    
        db_query("INSERT INTO ?:cp_statistics_product_popularity ?e ON DUPLICATE KEY UPDATE favorites = favorites + 1", $_data, POPULARITY_VIEW);
    }
}

function fn_cp_power_statistics_set_product_popularity($product_id, $popularity_view = POPULARITY_VIEW)
{
    $date = date('Y.m.d');
    
    if (empty($_SESSION['cp_products_popularity'])) {
        $_SESSION['cp_products_popularity'] = array();
    }
    // clean up old records
    $dates = array_keys($_SESSION['cp_products_popularity']);
    foreach ($dates as $_date) {
        if ($_date < $date) {
            unset($_SESSION['cp_products_popularity'][$_date]);
        }
    }
    
    if (empty($_SESSION['cp_products_popularity'][$date]['viewed'][$product_id])) {
        $_data = array (
            'product_id' => $product_id,
            'viewed' => 1,
            'total' => $popularity_view,
            'timestamp' => mktime(date('H'), 0, 0, date('n'), date('j'), date('Y'))
        );

        db_query("INSERT INTO ?:cp_statistics_product_popularity ?e ON DUPLICATE KEY UPDATE viewed = viewed + 1, total = total + ?i", $_data, $popularity_view);

        $_SESSION['cp_products_popularity'][$date]['viewed'][$product_id] = true;

        return true;
    }

    return false;
}

function fn_cp_extended_statistics_get_product_views($params, $items_per_page = 0)
{
    // Set default values to input params
    if (empty($params['items_per_page'])) {
        $params['items_per_page'] = $items_per_page;
    }
    $default_params = array (
        'page' => 1,
        'period' => 'A'
    );

    $params = array_merge($default_params, $params);

    // Get filter information
    $join = $condition = '';
    
    if (!empty($params['get_field'])) {
        $fields = array(
            "SUM(" . $params['get_field'] . ") as " . $params['get_field'],
        );
        $condition .= " AND " . $params['get_field'] . " <> 0";
    } else {
        $fields = array(
            "SUM(viewed) as viewed",
            "SUM(favorites) as favorites",
        );
    }
    
    if (Registry::get('runtime.company_id')) {
        $fields[] = '?:products.company_id';
        $join .= "LEFT JOIN ?:products ON ?:products.product_id = ?:cp_statistics_product_popularity.product_id";
        $condition .= db_quote(" AND ?:products.company_id = ?i", Registry::get('runtime.company_id'));
    }

    $group_date = 'timestamp';
    $_period = (in_array($params['period'], array('D', 'LD', 'HH'))) ? STAT_PERIOD_HOUR : STAT_PERIOD_DAY;
    if ($_period == STAT_PERIOD_HOUR) {
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:cp_statistics_product_popularity.timestamp), '%Y-%m-%d %H:00')";
    } elseif ($_period == STAT_PERIOD_MONTH) {
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:cp_statistics_product_popularity.timestamp), '%Y-%m-01')";
    } else {
        $_period = STAT_PERIOD_DAY;
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:cp_statistics_product_popularity.timestamp), '%Y-%m-%d')"; // Day
    }

    
    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(" AND (?:cp_statistics_product_popularity.timestamp >= ?i AND ?:cp_statistics_product_popularity.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT $group_date) FROM ?:cp_statistics_product_popularity $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    
    if (!empty($params['order_by'])) {
        $order_by = $params['order_by'];
    } else {
        $order_by = 'date';
    }

    $data = array();
    $_data = db_get_hash_array("SELECT $group_date as date, " . implode(', ', $fields) . ", ?:cp_statistics_product_popularity.timestamp FROM ?:cp_statistics_product_popularity $join WHERE 1 $condition GROUP BY date ORDER BY $order_by DESC $limit", 'date');
    
    $data['data'] = $_data;

    return array($data, $params, $_period);
}

function fn_cp_extended_statistics_most_active($params, $items_per_page = 0)
{
    // Set default values to input params
    if (empty($params['items_per_page'])) {
        $params['items_per_page'] = $items_per_page;
    }
    $default_params = array (
        'page' => 1,
        'period' => 'A'
    );

    $params = array_merge($default_params, $params);

    // Get filter information
    $join = $condition = '';
    
    if (!empty($params['get_field'])) {
        $fields = array(
            "SUM(?:cp_statistics_product_popularity." . $params['get_field'] . ") as " . $params['get_field'],
        );
        $condition .= " AND " . $params['get_field'] . " <> 0";
    } else {
        $fields = array(
            "SUM(?:cp_statistics_product_popularity.viewed) as viewed",
            "SUM(?:cp_statistics_product_popularity.favorites) as favorites",
        );
    }
    $fields[] = "?:product_descriptions.product";
    $join .= db_quote(" LEFT JOIN ?:product_descriptions ON ?:cp_statistics_product_popularity.product_id = ?:product_descriptions.product_id AND ?:product_descriptions.lang_code = ?s", DESCR_SL);

    if (Registry::get('runtime.company_id')) {
        $company_id = Registry::get('runtime.company_id');
        $join .= db_quote(" LEFT JOIN ?:products ON ?:cp_statistics_product_popularity.product_id = ?:products.product_id ");
        $fields[] = "?:products.company_id";
        $condition .= db_quote(" AND ?:products.company_id = ?i", $company_id);
    }
    
    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(" AND (?:cp_statistics_product_popularity.timestamp >= ?i AND ?:cp_statistics_product_popularity.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT ?:cp_statistics_product_popularity.product_id) FROM ?:cp_statistics_product_popularity $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    
    if (!empty($params['order_by'])) {
        $order_by = $params['order_by'];
    } else {
        $order_by = 'date';
    }

    $data = array();
    $_data = db_get_hash_array("SELECT ?:cp_statistics_product_popularity.product_id, " . implode(', ', $fields) 
        . " FROM ?:cp_statistics_product_popularity $join "
        . " WHERE 1 $condition GROUP BY ?:cp_statistics_product_popularity.product_id ORDER BY $order_by DESC $limit", 'product_id');
    
    $data['data'] = $_data;

    return array($data, $params);
}

function fn_cp_extended_statistics_views_by_countries($params, $items_per_page = 0)
{
    // Set default values to input params
    if (empty($params['items_per_page'])) {
        $params['items_per_page'] = $items_per_page;
    }
    $default_params = array (
        'page' => 1,
        'period' => 'A'
    );

    $params = array_merge($default_params, $params);

    // Get filter information
    $join = $condition = '';
    
    $fields = array(
        "SUM(?:cp_statistics_country_views.views) as viewed",
        "?:cp_statistics_country_views.*",
        "?:country_descriptions.country",
    );
    $join .= db_quote(" LEFT JOIN ?:country_descriptions ON ?:cp_statistics_country_views.country_code = ?:country_descriptions.code AND ?:country_descriptions.lang_code = ?s", DESCR_SL);

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(" AND (?:cp_statistics_country_views.timestamp >= ?i AND ?:cp_statistics_country_views.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT ?:cp_statistics_country_views.country_code) FROM ?:cp_statistics_country_views $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    
    $data = array();
    $_data = db_get_hash_array("SELECT " . implode(', ', $fields) 
        . " FROM ?:cp_statistics_country_views $join "
        . " WHERE 1 $condition GROUP BY ?:cp_statistics_country_views.country_code ORDER BY ?:cp_statistics_country_views.views DESC $limit", 'country_code');
    
    $data['data'] = $_data;

    return array($data, $params);
}

function fn_cp_extended_statistics_orders_by_countries($params, $items_per_page = 0)
{
    // Set default values to input params
    if (empty($params['items_per_page'])) {
        $params['items_per_page'] = $items_per_page;
    }
    $default_params = array (
        'page' => 1,
        'period' => 'A'
    );

    $params = array_merge($default_params, $params);

    // Get filter information
    $join = $condition = '';
    
    if (Registry::get('runtime.company_id')) {
        $condition .= db_quote(" AND ?:cp_statistics_country_orders.company_id = ?i", Registry::get('runtime.company_id'));
    }
    
    $fields = array(
        "SUM(?:cp_statistics_country_orders.order_count) as count",
        "?:cp_statistics_country_orders.*",
        "?:country_descriptions.country",
    );
    $join .= db_quote(" LEFT JOIN ?:country_descriptions ON ?:cp_statistics_country_orders.country_code = ?:country_descriptions.code AND ?:country_descriptions.lang_code = ?s", DESCR_SL);

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(" AND (?:cp_statistics_country_orders.timestamp >= ?i AND ?:cp_statistics_country_orders.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT ?:cp_statistics_country_orders.country_code) FROM ?:cp_statistics_country_orders $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }
    $data = array();
    $_data = db_get_hash_array("SELECT " . implode(', ', $fields) 
        . " FROM ?:cp_statistics_country_orders $join "
        . " WHERE 1 $condition GROUP BY ?:cp_statistics_country_orders.country_code ORDER BY count DESC $limit", 'country_code');
    
    $data['data'] = $_data;

    return array($data, $params);
}

function fn_cp_extended_statistics_get_revenue($params, $items_per_page = 0)
{
    // Set default values to input params
    if (empty($params['items_per_page'])) {
        $params['items_per_page'] = $items_per_page;
    }
    $default_params = array (
        'page' => 1,
        'period' => 'A'
    );

    $params = array_merge($default_params, $params);

    // Get filter information
    $join = $condition = '';

    $group_date = 'timestamp';
    $_period = (in_array($params['period'], array('D', 'LD', 'HH'))) ? STAT_PERIOD_HOUR : STAT_PERIOD_DAY;
    if (isset($params['period'])) {
        if ($params['period'] == 'A') {
            $_period = STAT_PERIOD_YEAR;
        } elseif (in_array($params['period'], array('Y', 'LY'))) {
            $_period = STAT_PERIOD_MONTH;
        }
    }
    if ($_period == STAT_PERIOD_HOUR) {
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:orders.timestamp), '%Y-%m-%d %H:00')";
    } elseif ($_period == STAT_PERIOD_MONTH) {
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:orders.timestamp), '%Y-%m-01')";
    } elseif ($_period == STAT_PERIOD_DAY) {
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:orders.timestamp), '%Y-%m-%d')"; // Day
    } else {
        $_period == STAT_PERIOD_YEAR;
        $group_date = "DATE_FORMAT(FROM_UNIXTIME(?:orders.timestamp), '%Y')"; // Year
    }

    if (Registry::get('runtime.company_id')) {
        
        $condition .= db_quote(" AND ?:orders.company_id = ?i", Registry::get('runtime.company_id'));
    }
    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(" AND (?:orders.timestamp >= ?i AND ?:orders.timestamp <= ?i)", $params['time_from'], $params['time_to']);
    }
    
    $condition .= " AND ?:orders.status <> 'N'";

    $fields = array(
        "SUM(subtotal) as subtotal",
    );
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT $group_date) FROM ?:orders $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }

    $data = array();
    $_data = db_get_hash_array("SELECT $group_date as date, " . implode(', ', $fields) . ", timestamp FROM ?:orders $join WHERE 1 $condition GROUP BY date ORDER BY date ASC $limit", 'date');
    
    $data['data'] = $_data;

    return array($data, $params, $_period);
}

function fn_cp_extended_statistics_map_data($chart, $data, $map, $titles = array(), $aggregate = false)
{
    $result = array();

    if ($aggregate == false) {
        foreach ($data as $k => $v) {
            foreach ($map as $field => $alias) {
                $result[$k][$alias] = ($v[$field] == '') ? __('undefined') : $v[$field];
            }
        }
    } else {
        foreach ($data as $k => $v) {
            foreach ($map as $_k => $field) {
                $result[$field]['values'][$k]['title'] = $k;
                $result[$field]['values'][$k]['value'] = $v[$field];
                $result[$field]['title'] = $titles[$_k];
            }
        }
    }

    /*if (count($result) > $max_items) {
        $other = array_splice($result, $max_items);

        $total = 0;
        foreach ($other as $k => $v) {
            $total += $v['value'];
        }

        $result[] = arra
    }*/

    return fn_amcharts_data($chart, $result);
}

function fn_cp_extended_statistics_stat_filter_data($params)
{
    $params = LastView::instance()->update('statistics', $params);
    $join = array();
    $where = '';

    if (!empty($params)) {
        $sql_where = array();
        $pattern = "/AND|OR/";

        foreach ($params as $param => $value) {
            if (empty($value)) {
                continue;
            }

            $conditions = preg_split($pattern, $value);
            if (empty($conditions)) {
                $conditions = array($value);
            }
            preg_match_all($pattern, $value, $conjunctions);
            $_condition = array();
            foreach ($conditions as $k => $v) {
                $_condition[] = array(
                    'condition' => trim($v),
                    'conjunction' => ((empty($k) || empty($conjunctions[0][$k - 1]) ? '' : trim($conjunctions[0][$k - 1]))),
                );
            }
            $__where = '';

            foreach ($_condition as $c) {
                if ($param == 'ip_address') {
                    $_long_ip = sprintf('%u', ip2long($c['condition']));
                    $_where = db_quote("?:stat_sessions.proxy_ip = ?i OR ?:stat_sessions.host_ip = ?i", $_long_ip, $_long_ip);

                } elseif ($param == 'language') {
                        $_where = db_quote("(?:stat_sessions.client_language LIKE ?l OR ?:stat_languages.language LIKE ?l)", "$c[condition]%", "$c[condition]%");
                        $join['?:stat_languages'] = "?:stat_sessions.client_language = ?:stat_languages.lang_code";

                } elseif ($param == 'search_phrase') {
                        $_where = db_quote("?:stat_search_phrases.phrase = ?s", $c['condition']);
                        $join['?:stat_search_phrases'] = "?:stat_sessions.phrase_id = ?:stat_search_phrases.phrase_id";

                } elseif ($param == 'url') {
                        $_where = db_quote("?:stat_requests.url LIKE ?l", "%$c[condition]%");
                        $join['?:stat_requests'] = "?:stat_sessions.sess_id = ?:stat_requests.sess_id";

                } elseif ($param == 'page_title') {
                        $_where = db_quote("?:stat_requests.title LIKE ?l", "%$c[condition]%");
                        $join['?:stat_requests'] = "?:stat_sessions.sess_id = ?:stat_requests.sess_id";

                } elseif ($param == 'user_agent') {
                        $_where = db_quote("?:stat_sessions.user_agent LIKE ?l", "%$c[condition]%");

                } elseif ($param == 'browser_name') {
                        $_where = db_quote("?:stat_browsers.browser LIKE ?l", "%$c[condition]%");
                        $join['?:stat_browsers'] = "?:stat_sessions.browser_id = ?:stat_browsers.browser_id";

                } elseif ($param == 'browser_version') {
                        $_where = db_quote("?:stat_browsers.version = ?s", $c['condition']);
                        $join['?:stat_browsers'] = "?:stat_sessions.browser_id = ?:stat_browsers.browser_id";

                } elseif ($param == 'operating_system') {
                        $_where = db_quote("UPPER(?:stat_sessions.os) LIKE UPPER(?s)", "%$c[condition]%");

                } elseif ($param == 'country') {
                        $_where = db_quote("(?:stat_ips.country_code = ?s OR ?:country_descriptions.country LIKE ?l)", $c['condition'], $c['condition']);
                        $join['?:stat_ips'] = "?:stat_sessions.ip_id = ?:stat_ips.ip_id";
                        $join['?:country_descriptions'] = db_quote("?:stat_ips.country_code = ?:country_descriptions.code AND ?:country_descriptions.lang_code = ?s", CART_LANGUAGE);

                } elseif ($param == 'referrer_url') {
                        $_where = db_quote("?:stat_sessions.referrer LIKE ?l", "%$c[condition]%");

                } else {
                        $_where = '';
                }

                if (!empty($_where)) {
                    $__where .= "$c[conjunction] $_where ";
                }
            }

            if (!empty($__where)) {
                $sql_where[] = $__where;
            }
        }

        if (!empty($sql_where)) {
            $where .= (empty($where) ? '' : ' AND ') . (!empty($params['exclude_condition']) ? 'NOT' : '') . '('. implode(') AND ' . (!empty($params['exclude_condition']) ? 'NOT ' : ' ') . '(', $sql_where) . ')';
        } else {
            $where .= "1";
        }
    }

    if (empty($params['period'])) {
        $params['period'] = 'A';
    }

    list($params['time_from'], $params['time_to']) = fn_create_periods($params);

    if (!empty($params['time_from']) || !empty($params['time_to'])) {
        $where = (empty($where) ? '' : $where . ' AND ') . fn_cp_extended_statistics_get_sql_where_time('?:stat_sessions', $params['time_from'], $params['time_to']);
        if (!empty($join['?:stat_requests'])) {
            $where .= ' AND ' . fn_cp_extended_statistics_get_sql_where_time('?:stat_requests', $params['time_from'], $params['time_to']);
        }
    }

    if ($company_condition = fn_get_ult_company_condition('?:stat_sessions.company_id')) {
        $where = empty($where) ? '1' : $where;
        $where .= $company_condition;
    }

    if (empty($params['limit'])) {
        $params['limit'] = 20;
    }

    return array($where, $join, $params);
    
}

function fn_cp_extended_statistics_get_sql_where_time($table_name, $time_from, $time_to, $field = 'timestamp')
{
    return (db_quote("$table_name.$field >= ?i AND $table_name.$field < ?i", $time_from, empty($time_to) ? TIME : $time_to));
}

function fn_cp_extended_statistics_bild_sql_join($joins)
{
    if (empty($joins)) {
        return '';
    }

    // Sort joins to avoid cross-joining errors in mysql 5.1+
    $sorted_joins = array();
    $joined_tables = array();

    $joined_tables = array_keys($joins);
    $required = array();

    foreach ($joins as $table => $condition) {
        $required[$table] = array();
        foreach ($joined_tables as $check_table) {
            if ($check_table == $table) {
                continue;
            }

            if (strpos($condition, $check_table) !== false) {
                $required[$table][] = $check_table;
            }
        }
    }
    foreach ($joins as $table => $condition) {
        if (empty($required[$table])) {
            $sorted_joins[$table] = $condition;
            unset($joins[$table]);
        }
    }

    $included_tables = array_keys($sorted_joins);
    while (count($joins) > 0) {
        foreach ($joins as $table => $condition) {
            $not_inluded_tables = array_diff($required[$table], $included_tables);
            if (empty($not_included_tables)) {
                $included_tables[] = $table;
                $sorted_joins[$table] = $condition;
                unset($joins[$table]);
            }
        }
    }

    $r_join = '';
    foreach ($sorted_joins as $table => $condition) {
        $r_join .= "LEFT JOIN $table ON ($condition) ";
    }

    return $r_join;
}

function fn_cp_extended_statistics_stat_format_data($data)
{

    if (!empty($data['data'])) {
        // Calculate percentage
        $total = 0;

        foreach ($data['data'] as $v) {
            $total += $v['count'];
        }

        // Calculate percent value for every item
        foreach ($data['data'] as $k => $v) {
            $data['data'][$k]['percent'] = !empty($total) ? round($v['count'] * 100 / $total, 2) : 0;
        }
    }

    return $data;
}

function fn_cp_extended_statistics_get_top_keywords($params, $items_per_page = 0)
{
    // Get filter information
    list($where, $join, $params) = fn_cp_extended_statistics_stat_filter_data($params);
    $fields = "?:stat_product_search.search_string AS label, ?:stat_product_search.md5,"
        . " DATE_FORMAT(FROM_UNIXTIME(?:stat_sessions.timestamp), '%Y-%m-%d') AS date,"
        . " AVG(?:stat_product_search.quantity) AS quantity, COUNT(*) as count";

    if (fn_allowed_for('ULTIMATE')) {
        $join['?:companies'] = "?:stat_sessions.company_id = ?:companies.company_id";
        $fields .= ', ?:companies.storefront';
    }

    $where .= " AND client_type = 'U'";
    $join['?:stat_sessions'] = "?:stat_product_search.sess_id=?:stat_sessions.sess_id";

    $filter_condition = fn_cp_extended_statistics_bild_sql_join($join);

    $data = array();
    if (empty($params['report'])) {
        $report = 'search';
    } else {
        $report = $params['report'];
    }
    $product_features = array();
    
    $sortings = array (
        'keywords' => 'label',
        'date' => 'date',
        'count' => 'count',
        'quantity' => 'quantity',
    );
    $sorting = db_sort($params, $sortings, 'date', 'desc');
    
    if ($report == 'search') {
        $data['data'] = db_get_array(
                "SELECT $fields"
                . " FROM ?:stat_product_search $filter_condition"
                . " WHERE $where"
                . " GROUP BY ?:stat_product_search.md5,"
                . " DATE_FORMAT(FROM_UNIXTIME(?:stat_sessions.timestamp), '%Y-%m-%d')"
                . " $sorting LIMIT ?i", $params['limit']
            );

        if (!empty($data['data'])) {
            foreach ($data['data'] as $k => $v) {
                $data['data'][$k]['label'] = unserialize($v['label']);
                $data['data'][$k]['url'] = empty($data['data'][$k]['label']) ? '' : http_build_query($data['data'][$k]['label']);

                $url = fn_url("products.search?" . $data['data'][$k]['url'], 'C', 'rel');
                if (!empty($v['storefront'])) {
                    $data['data'][$k]['storefront_url'] = (defined('HTTPS') ? 'https://' : 'http://') . $v['storefront'] . '/' . $url;
                } else {
                    $data['data'][$k]['storefront_url'] = $url;
                }
            }
        }

        list($product_features) = fn_get_product_features();
    }

    $data['report'] = $report;
    
    return array($data, $product_features, $params);
}
