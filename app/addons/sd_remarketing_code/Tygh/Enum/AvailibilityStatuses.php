<?php
 namespace Tygh\Enum; class AvailibilityStatuses { const INSTOCK = 'in stock'; const OUTSTOCK = 'out of stock'; const PREORDER = 'preorder'; } 