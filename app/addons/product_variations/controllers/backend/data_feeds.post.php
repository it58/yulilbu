<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

<<<<<<< HEAD
use Tygh\Addons\ProductVariations\ServiceProvider;
=======

use Tygh\Addons\ProductVariations\Product\Manager as ProductManager;

>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * @var string $mode
 * @var string $action
 * @var array $auth
 */

<<<<<<< HEAD
if ($mode === 'update' || $mode === 'add') {
    /** @var \Tygh\SmartyEngine\Core $view */
    $view = Tygh::$app['view'];
    $view->assign('product_types', ServiceProvider::getTypeCollection()->getTypeNames());
=======

if ($mode === 'update' || $mode === 'add') {
    /** @var \Tygh\SmartyEngine\Core $view */
    $view = Tygh::$app['view'];
    /** @var ProductManager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];
    $product_types = $product_manager->getProductTypeNames();

    unset($product_types[ProductManager::PRODUCT_TYPE_CONFIGURABLE]);

    $view->assign('product_types', $product_types);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
}