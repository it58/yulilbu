<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/
use Tygh\Registry;


//add addition tab  products-->Related Groups


    
$schema['central']['products']['items']['import_from_ebay'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'cp_ebay_import.manage',
    'position' => 600,
    'subitems' => array(
        'ebay_category_manage' => array(
            'href' => 'cp_ebay_import.category_manage',
            'position' => 1000
        ),
     )
);
return $schema;



