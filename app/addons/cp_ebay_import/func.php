<?php

use Tygh\Bootstrap;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Tools\Url;
use Tygh\Http;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_cp_ebay_import_delete_company($company_id, $result) {
	db_query("DELETE FROM ?:ebay_import_transfer WHERE  company_id = ?i",$company_id);
}

function fn_cp_ebay_import_delete_ebay_categories($country) {
	if (isset($country)) {
		$country_name = db_get_field("SELECT country_name FROM ?:ebay_country_code WHERE country_code = ?i", $country);
		db_query("DELETE FROM ?:ebay_cp_categories WHERE  country_name = ?s", $country_name);
	}
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}

function fn_cp_ebay_import_save_ebay_global_cat($ebay_category_ids, $ebay_country, $global_mapping) {
	foreach ($ebay_category_ids as $ebay_categ) {
		if (!empty($ebay_categ)) {
			db_query("UPDATE ?:ebay_cp_categories SET category_id = ?i WHERE ebay_category_id = ?i AND country_name = ?s", $global_mapping, $ebay_categ, $ebay_country[$ebay_categ]);
		}
	}
	fn_set_notification('N', __('notice'), __('categories_updated'));
	return true;
}

function fn_cp_ebay_import_delete_ebay_seller_listings($seller, $company_id, $country_code) {
	$country_name = db_get_field("SELECT country_name FROM ?:ebay_country_code WHERE country_code = ?i", $country_code);
	db_query("DELETE FROM ?:ebay_import_transfer WHERE ebay_seller = ?s AND company_id = ?i AND country_name = ?s", $seller, $company_id, $country_name);
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}

function fn_cp_ebay_import_add_all_to_import_queue ($company_id, $status) {
	db_query("UPDATE ?:ebay_import_transfer SET select_list = ?s WHERE company_id = ?i", $status, $company_id);
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}

function fn_cp_ebay_import_unselect_selected_listings($ebay_listing_ids, $company_id, $ebay_country) {
	foreach ($ebay_listing_ids as $select_list) {
		db_query("UPDATE ?:ebay_import_transfer SET select_list = ?s WHERE ebay_id = ?i AND company_id = ?i AND country_name = ?s", 'N', $select_list, $company_id, $ebay_country[$select_list]);
	}
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}
function fn_cp_ebay_import_save_selected_listings_checkbox($ebay_listing_ids, $ebay_listing_not_sel, $company_id, $ebay_country) {
	foreach ($ebay_listing_not_sel as $unselect_list) {
		db_query("UPDATE ?:ebay_import_transfer SET select_list = ?s WHERE ebay_id = ?i AND company_id = ?i AND country_name = ?s", 'N', $unselect_list, $company_id, $ebay_country[$unselect_list]);
	}
	foreach ($ebay_listing_ids as $select_list) {
		db_query("UPDATE ?:ebay_import_transfer SET select_list = ?s WHERE ebay_id = ?i AND company_id = ?i AND country_name = ?s", 'Y', $select_list, $company_id, $ebay_country[$select_list]);
	}
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}
function fn_cp_ebay_import_update_category_post($category_data, $category_id, $lang_code) {
	
	if (!empty($category_data['cp_ebay_category']) && !empty($category_data['old_ebay_category'])) {
		if ($category_data['cp_ebay_category'] != $category_data['old_ebay_category']) {
			db_query("UPDATE ?:categories SET cp_ebay_category = ?s", 'N');
			db_query("UPDATE ?:categories SET cp_ebay_category = ?s WHERE category_id = ?i", $category_data['cp_ebay_category'], $category_id);
		}
	}
}
function fn_cp_ebay_import_change_accsess_to_ebay($company_ids, $allow_accsess) {
	if (!empty($company_ids)) {
		if (!empty($allow_accsess) && $allow_accsess == 'Y') {
			db_query("UPDATE ?:companies SET allow_ebay = ?s WHERE company_id IN (?n)", 'Y', $company_ids);
		} else {
			db_query("UPDATE ?:companies SET allow_ebay = ?s WHERE company_id IN (?n)", 'N', $company_ids);
		}
	}
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}

// get Ebay categories
function fn_cp_ebay_import_get_ebay_categories($country_cod, $lvl_limit, $ebay_keys) {
	
	if (!empty($ebay_keys['ebay_dev_key']) && !empty($ebay_keys['ebay_api_key']) && !empty($ebay_keys['ebay_cert_key']) && !empty($ebay_keys['ebay_user_token'])) {
		$headers = array(
			'X-EBAY-API-CALL-NAME:GetCategories',                              
			'X-EBAY-API-COMPATIBILITY-LEVEL:967',
			'X-EBAY-API-DEV-NAME:'.$ebay_keys['ebay_dev_key'],
			'X-EBAY-API-APP-NAME:'.$ebay_keys['ebay_api_key'],
			'X-EBAY-API-CERT-NAME:'.$ebay_keys['ebay_cert_key'],
			'X-EBAY-API-SITEID:'.$country_cod,
			"X-EBAY-API-REQUEST-ENCODING: XML",    
			'Content-Type: text/xml;charset=utf-8',
	    );
		if (empty($lvl_limit)) {
			$lvl_limit = 0;
		}
	    $endpoint = 'https://api.ebay.com/ws/api.dll';
	    $xmlRequest  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		$xmlRequest .= "<GetCategoriesRequest xmlns='urn:ebay:apis:eBLBaseComponents'>";
		$xmlRequest .= "<RequesterCredentials>";
			$xmlRequest .= "<eBayAuthToken>".$ebay_keys['ebay_user_token']."</eBayAuthToken>";
		$xmlRequest .= "</RequesterCredentials>";
		$xmlRequest .= "<LevelLimit>".$lvl_limit."</LevelLimit>";
		$xmlRequest .= "<ViewAllNodes>true</ViewAllNodes>";
		$xmlRequest .= "<DetailLevel>ReturnAll</DetailLevel>";
		$xmlRequest .= "<ErrorLanguage>en_US</ErrorLanguage>";
		$xmlRequest .= "<WarningLevel>Low</WarningLevel>";
		$xmlRequest .= "</GetCategoriesRequest>";
	    
		$session  = curl_init($endpoint);                       
	    curl_setopt($session, CURLOPT_POST, true);              
	    curl_setopt($session, CURLOPT_POSTFIELDS, $xmlRequest); 
	    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);    
	    curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
	    $responseXML = curl_exec($session);
	    curl_close($session);
		$responseXML = simplexml_load_string($responseXML); 
		$array_items = json_decode(json_encode($responseXML), TRUE);
		$country_name = db_get_field("SELECT country_name FROM ?:ebay_country_code WHERE country_code = ?i", $country_cod);
		if (!empty($array_items['Ack']) && $array_items['Ack'] == 'Success') {
			if (!empty($array_items['CategoryArray'])) {
				if (!empty($array_items['CategoryArray']['Category'])) {
					
					foreach ($array_items['CategoryArray']['Category'] as $eb_cat) {
						$cat_data = array(
							'ebay_category_id' => $eb_cat['CategoryID'],
							'ebay_cat_lvl' => $eb_cat['CategoryLevel'],
							'ebay_cat_name' => $eb_cat['CategoryName'],
							'ebay_cat_parent' => $eb_cat['CategoryParentID']
						);
						if (!empty($country_name)) {
							$cat_data['country_name'] = $country_name;
						}
						if (!empty($cat_data)) {
							/*$chek_ebay_cat = db_get_field("SELECT ebay_category_id FROM ?:ebay_first_categories WHERE ebay_category_id = ?i AND country_name = ?s", $eb_cat['CategoryID'], $country_name);
							if (!empty($chek_ebay_cat)) {
								db_query("UPDATE ?:ebay_first_categories SET ?u WHERE ebay_category_id = ?i AND country_name = ?s", $cat_data, $eb_cat['CategoryID'], $country_name);
							} else {*/
								db_query("REPLACE INTO ?:ebay_first_categories ?e", $cat_data);
								db_query("REPLACE INTO ?:cp_ebay_all_categories ?e", $cat_data);
							//}
							$ebay_cat_ids[] = $eb_cat['CategoryID'];
						}
					}
					if (!empty($ebay_cat_ids)) {
						db_query("DELETE FROM ?:ebay_first_categories WHERE ebay_category_id NOT IN (?n)", $ebay_cat_ids);
					}
				}
			}
			$new_cat = db_get_hash_array("SELECT * FROM ?:ebay_first_categories WHERE country_name = ?s", 'ebay_category_id', $country_name);
			if (!empty($new_cat)) {
				foreach($new_cat as $categ) {
					if ($categ['ebay_cat_lvl'] != 1) {
						if ($categ['ebay_cat_parent'] != $categ['ebay_category_id']) {
							$new_cat_data = array();
							$new_cat_data['ebay_category_id'] = $categ['ebay_category_id'];
							$new_cat_data['ebay_cat_name'] = $categ['ebay_cat_name'];
							$new_cat_data['country_name'] = $country_name;
							$cat_path = fn_cp_ebay_import_get_ebay_cat_path('', $categ, $new_cat);
							$new_cat_data['ebay_cat_path'] = $cat_path;
							
						}
					} else {
						$new_cat_data = array();
						$new_cat_data['ebay_category_id'] = $categ['ebay_category_id'];
						$new_cat_data['ebay_cat_name'] = $categ['ebay_cat_name'];
						$new_cat_data['ebay_cat_path'] = $categ['ebay_cat_name'];
						$new_cat_data['country_name'] = $country_name;
					}
					$chek_new_ebay_cat = db_get_field("SELECT ebay_category_id FROM ?:ebay_cp_categories WHERE ebay_category_id = ?i AND country_name = ?s", $categ['ebay_category_id'], $country_name);
					if (!empty($chek_new_ebay_cat)) {
						db_query("UPDATE ?:ebay_cp_categories SET ?u WHERE ebay_category_id = ?i AND country_name = ?s", $new_cat_data, $categ['ebay_category_id'], $country_name);
					} else {
						db_query("REPLACE INTO ?:ebay_cp_categories ?e", $new_cat_data);
					}
					$ebay_new_cat_ids[] = $categ['ebay_category_id'];
				}
				if (!empty($ebay_new_cat_ids)) {
					db_query("DELETE FROM ?:ebay_cp_categories WHERE ebay_category_id NOT IN (?n) AND country_name = ?s", $ebay_new_cat_ids, $country_name);
				}
			}
		}
		if (!empty($array_items['Ack']) && $array_items['Ack'] == 'Failure' && !empty($array_items['Errors'])) {
			if (!is_array($array_items['Errors'])) {
				$array_items['Errors'] = array($array_items['Errors']);
			}
			if (empty($array_items['Errors'][0])) {
				fn_set_notification('E', __('error'), __('ebay_request_error').$array_items['Errors']['ShortMessage']);
				
			} else {
				foreach ($array_items['Errors'] as $error) {
					fn_set_notification('E', __('error'), __('ebay_request_error').$error['ShortMessage']);
				}
			}
		}
	} else {
		fn_set_notification('W', __('warning'), __('please_enter_api_key_at_addons_settings'));
	}
	fn_set_notification('N', __('notice'), __('completed'));
	return true;
}
// get full category path
function fn_cp_ebay_import_get_ebay_cat_path ($path, $categ, $new_cat) {
	if (!empty($path)) {
		$path = $categ['ebay_cat_name'].' / '.$path;
	} else {
		$path = $categ['ebay_cat_name'];
	}
	if ($new_cat[$categ['ebay_cat_parent']]['ebay_cat_lvl'] != 1) {
		$path = fn_cp_ebay_import_get_ebay_cat_path($path, $new_cat[$categ['ebay_cat_parent']], $new_cat);
	} else {
		$path = $new_cat[$categ['ebay_cat_parent']]['ebay_cat_name'].' / '.$path;
	}
	return $path;
}
// save listings images
function fn_cp_ebay_import_save_listings_images($listings_images, $product_id, $company_id, $new_porduct) {
	
	if (!empty($new_porduct) && $new_porduct == 'N') {
		fn_delete_image_pairs($product_id, 'product', 'A');
	}
	if (!is_array($listings_images)) {
		$listings_images = array($listings_images);
	}
	foreach ($listings_images as $key => $lis_image) {
		$new_path = array();
		$new_path = explode('?', $lis_image);
		$second_parce = explode('/z/',$new_path[0]);
		$third_parce = explode('/$',$second_parce[1]);
		/*'http://i.ebayimg.com/images/g/U~8AAOSwLN5Wl3t3/s-l1600.jpg';
		'http://i.ebayimg.com/00/s/OTYwWDk2MA==/z/U~8AAOSwLN5Wl3t3/$_12.JPG';*/
		if ($key == 0) {
			$prefix = '';
			$image_file = '';
			//$detailed_file = $new_path[0];
			$detailed_file = 'http://i.ebayimg.com/images/g/'.$third_parce[0].'/s-l1600.jpg';
			$position = 0;
			$type = 'M';
			$object_id = $product_id;
			$object = 'product';
			fn_cp_ebay_import_import_images($prefix, $image_file, $detailed_file, $position, $type, $object_id, $object);
		} else {
			$prefix = '';
			$image_file = '';
			//$detailed_file = $new_path[0];
			$detailed_file = 'http://i.ebayimg.com/images/g/'.$third_parce[0].'/s-l1600.jpg';
			$position = 0;
			$type = 'A';
			$object_id = $product_id;
			$object = 'product';
			fn_cp_ebay_import_import_images($prefix, $image_file, $detailed_file, $position, $type, $object_id, $object);
		}
	}
	return true;
}
// get parent categry id of listing
function fn_cp_ebay_import_try_to_get_listing_parent_category($listing_cat_id, $country_code) {
	$list_cat_id = '';
	if (!empty($listing_cat_id)) {
		$parent_ct_id = db_get_row("SELECT ebay_cat_parent, ebay_cat_lvl FROM ?:cp_ebay_all_categories WHERE ebay_category_id = ?i AND country_name = ?s", $listing_cat_id, $country_code);
		if (!empty($parent_ct_id) && !empty($parent_ct_id['ebay_cat_parent'])) {
			$check_maped_cat = db_get_field("SELECT category_id FROM ?:ebay_cp_categories WHERE ebay_category_id = ?i", $parent_ct_id['ebay_cat_parent']);
			if (!empty($check_maped_cat)) {
				return $check_maped_cat;
			} elseif($parent_ct_id['ebay_cat_lvl'] != 1) {
				$list_cat_id = fn_cp_ebay_import_try_to_get_listing_parent_category($parent_ct_id['ebay_cat_parent'], $country_code);
			}
		}
	}
	return $list_cat_id;
}
// import listings from Ebay
function fn_cp_ebay_import_import_listings_from_ebay($ebay_data) {
	if (!empty($ebay_data['company_id'])) {
		$ebay_listing_ids = db_get_fields("SELECT ebay_id FROM ?:ebay_import_transfer WHERE select_list = ?s AND company_id = ?i", 'Y', $ebay_data['company_id']);
	}
	if (!empty($ebay_listing_ids)) {
		$ebay_category = db_get_field("SELECT category_id FROM ?:categories WHERE cp_ebay_category = ?s", 'Y');
		if (!empty($ebay_category)) {
			$user_ebay_data = db_get_row("SELECT allow_ebay, company_id FROM ?:companies WHERE company_id = ?i", $ebay_data['company_id']);
			if (!empty($ebay_data['ebay_dev_key'])) {
				$user_ebay_data['ebay_dev_key'] = $ebay_data['ebay_dev_key'];
			}
			if (!empty($ebay_data['ebay_api_key'])) {
				$user_ebay_data['ebay_api_key'] = $ebay_data['ebay_api_key'];
			}
			if (!empty($ebay_data['ebay_cert_key'])) {
				$user_ebay_data['ebay_cert_key'] = $ebay_data['ebay_cert_key'];
			}
			if (!empty($ebay_data['ebay_user_token'])) {
				$user_ebay_data['ebay_user_token'] = $ebay_data['ebay_user_token'];
			}
			if (!empty($user_ebay_data['ebay_dev_key']) && !empty($user_ebay_data['ebay_api_key']) && !empty($user_ebay_data['ebay_cert_key']) && !empty($user_ebay_data['ebay_user_token']) && !empty($user_ebay_data['allow_ebay']) && $user_ebay_data['allow_ebay'] == 'Y') {
				$import_limit = Registry::get('addons.cp_ebay_import.step_of_importing_listings');
				if (empty($import_limit)) {
					$import_limit = 40;
				}
				if (empty($_SESSION['ebay_import'])) {
					$_SESSION['ebay_import'] = array();
					$_SESSION['ebay_import']['updated'] = 0;
					$_SESSION['ebay_import']['new_products'] = 0;
					$_SESSION['ebay_import']['imported'] = 0;
				}
				foreach ($ebay_listing_ids as $key => $ebay_id) {
					if ($key == $import_limit) {
						return false;
					}
					$listing_data = db_get_row("SELECT * FROM ?:ebay_import_transfer WHERE ebay_id = ?i", $ebay_id);
					
					$check_product = db_get_field("SELECT product_id FROM ?:products WHERE ebay_id = ?i AND company_id = ?i", $ebay_id, $user_ebay_data['company_id']);
					//create new product
					if (empty($check_product)) {
						$put_category = db_get_field("SELECT category_id FROM ?:ebay_cp_categories WHERE ebay_category_id = ?i", $listing_data['ebay_category_id']);
						if (!empty($put_category)) {
							$listing_data['category_ids'][0] = $put_category;
						} else {
							$parent_id = fn_cp_ebay_import_try_to_get_listing_parent_category($listing_data['ebay_category_id'], $listing_data['country_name']);
							if (!empty($parent_id)) {
								$listing_data['category_ids'][0] = $parent_id;
							} else {
								$listing_data['category_ids'][0] = $ebay_category;
							}
						}
						$listing_data['company_id'] = $user_ebay_data['company_id'];
						$listing_data['status'] = 'D';
						$listing_data['timestamp'] = date("m/d/y", $listing_data['timestamp']);
						$product_id = 0;
						$product_id = fn_update_product($listing_data, $product_id, DESCR_SL);
						$new_porduct = 'Y';
					} else {
						// update product
						$product_id = fn_update_product($listing_data, $check_product, DESCR_SL);
						$new_porduct = 'N';
					}
					// upload listing images
					if (!empty($product_id)) {
						if (!empty($listing_data['images'])) {
							$listing_data['images'] = explode(';', $listing_data['images']);
							fn_cp_ebay_import_save_listings_images($listing_data['images'], $product_id, $ebay_data['company_id'], $new_porduct);
						}
					}
					if (!empty($new_porduct) && $new_porduct == 'N') {
						$_SESSION['ebay_import']['updated'] = $_SESSION['ebay_import']['updated'] + 1;
					}
					if (!empty($new_porduct) && $new_porduct == 'Y') {
						$_SESSION['ebay_import']['new_products'] = $_SESSION['ebay_import']['new_products'] + 1;
					}
					$_SESSION['ebay_import']['imported'] = $_SESSION['ebay_import']['imported'] + 1;
					db_query("UPDATE ?:ebay_import_transfer SET import_status = ?s, select_list = ?s WHERE ebay_id = ?i", 'Y', 'N', $ebay_id);
				}
				fn_set_notification('W', __('important'), __('ebay_import_end', array(
					'[update]' => $_SESSION['ebay_import']['updated'],
					'[new_products]' => $_SESSION['ebay_import']['new_products'],
					'[imported]' => $_SESSION['ebay_import']['imported']
				)));
				$_SESSION['ebay_import'] = array();
			}
		} else {
			fn_set_notification('W', __('warning'), __('select_category_for_import_ebay'));
		}
	} else {
			fn_set_notification('W', __('warning'), __('please_select_listings_to_import'));
	}
	return true;
}
// creating array of listings for transfer table
function fn_cp_ebay_import_create_array_of_listings($results, $company_id, $ebay_seller, $country_code) {
	if (!empty($results)) {
		$country_name = db_get_field("SELECT country_name FROM ?:ebay_country_code WHERE country_code = ?i", $country_code);
		foreach ($results as $result) {
			if (!empty($result['ListingType']) && $result['ListingType'] == 'FixedPriceItem') {
				if (empty($result['Description'])) {
					$result['Description'] = '';
				}
				$new_price = $result['SellingStatus']['CurrentPrice'];
				$listing_cur = db_get_field("SELECT coefficient FROM ?:currencies WHERE currency_code = ?s", $result['Currency']);
				if (!empty($listing_cur)) {
					$new_price = $result['SellingStatus']['CurrentPrice']*$listing_cur;
				} else {
					fn_set_notification('W', __('warning'), __('no_currency_from_listing'));
				}
				if (!empty($result['PictureDetails']['PictureURL'])) {
					if (!is_array($result['PictureDetails']['PictureURL'])) {
						$result['PictureDetails']['PictureURL'] = array($result['PictureDetails']['PictureURL']);
					}
					$images = implode(';', $result['PictureDetails']['PictureURL']);
				}
				if (!empty($result['ShippingDetails']['CalculatedShippingRate']['WeightMajor']) || !empty($result['ShippingDetails']['CalculatedShippingRate']['WeightMinor'])) {
					$list_weight = $result['ShippingDetails']['CalculatedShippingRate']['WeightMajor'] + $result['ShippingDetails']['CalculatedShippingRate']['WeightMinor']/16;
				} elseif (!empty($result['ShippingPackageDetails']['WeightMajor']) || !empty($result['ShippingPackageDetails']['WeightMinor'])) {
					$list_weight = $result['ShippingPackageDetails']['WeightMajor'] + $result['ShippingPackageDetails']['WeightMinor']/16;
				} else {
					$list_weight = 0;
				}
				$listing = array(
					'company_id' => $company_id,
					'ebay_id' => $result['ItemID'],
					'ebay_category_id' => $result['PrimaryCategory']['CategoryID'],
					'ebay_seller' => $ebay_seller,
					'ebay_url' => $result['ListingDetails']['ViewItemURL'],
					'product' => $result['Title'],
					'images' => $images,
					'country_name' => $country_name,
					'full_description' => $result['Description'],
					'timestamp' => strtotime($result['ListingDetails']['StartTime']),
					'price' => $new_price,
					'currency_code' => $result['Currency'],
					'amount' => $result['Quantity'],
					'select_list' => 'N',
					'import_status' => 'N',
					'weight' => $list_weight,
				);
				
				if (!empty($listing)) {
					$chek_ebay_list = db_get_field("SELECT ebay_id FROM ?:ebay_import_transfer WHERE ebay_id = ?i AND company_id = ?i", $result['ItemID'], $company_id);
					if (!empty($chek_ebay_list)) {
						db_query("UPDATE ?:ebay_import_transfer SET ?u WHERE ebay_id = ?i AND company_id = ?i", $listing, $result['ItemID'], $company_id);
					} else {
						db_query("REPLACE INTO ?:ebay_import_transfer ?e", $listing);
					}
				}
				$_SESSION['ebay_new_list_ids'][] = $result['ItemID'];
			}
		}
	}
	return true;
}

function fn_cp_ebay_import_get_all_sellers_listings ($params, $user_ebay_data) {
	
	if (empty($params['ebay_country'])) {
		$params['ebay_country'] = 0;
	}
	if (empty($params['page'])) {
		$_SESSION['ebay_seller_page'] = $params['page'] = 1;
	}
	$headers = array(
		'X-EBAY-API-CALL-NAME:GetSellerList',                              
		'X-EBAY-API-COMPATIBILITY-LEVEL:967',
		'X-EBAY-API-DEV-NAME:'.$user_ebay_data['ebay_dev_key'],
		'X-EBAY-API-APP-NAME:'.$user_ebay_data['ebay_api_key'],
		'X-EBAY-API-CERT-NAME:'.$user_ebay_data['ebay_cert_key'],
		'X-EBAY-API-SITEID:'.$params['ebay_country'],
		"X-EBAY-API-REQUEST-ENCODING: XML",    
		'Content-Type: text/xml;charset=utf-8',
	);
	$endpoint = 'https://api.ebay.com/ws/api.dll';
	
	$xmlRequest  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	$xmlRequest .= "<GetSellerListRequest xmlns='urn:ebay:apis:eBLBaseComponents'>";
	$xmlRequest .= "<RequesterCredentials>";
		$xmlRequest .= "<eBayAuthToken>".$user_ebay_data['ebay_user_token']."</eBayAuthToken>";
	$xmlRequest .= "</RequesterCredentials>";
	$xmlRequest .= "<AdminEndedItemsOnly>false</AdminEndedItemsOnly>";
	//$xmlRequest .= "<CategoryID> int </CategoryID>";
	//$xmlRequest .= "<EndTimeFrom>2016-01-17T21:59:59.005Z</EndTimeFrom>";
	//$xmlRequest .= "<EndTimeTo>2016-01-18T21:59:59.005Z</EndTimeTo>";
	//$xmlRequest .= "<GranularityLevel>Medium</GranularityLevel>";
	$xmlRequest .= "<IncludeVariations>true</IncludeVariations>";
	$xmlRequest .= "<IncludeWatchCount>true</IncludeWatchCount>";
	//$xmlRequest .= "<MotorsDealerUsers> UserIDArrayType";
		//$xmlRequest .= "<UserID> UserIDType (string) </UserID>";
	//$xmlRequest .= "</MotorsDealerUsers>";
	$xmlRequest .= "<Pagination> PaginationType";
		$xmlRequest .= "<EntriesPerPage>".$params['listings_limit']."</EntriesPerPage>";
		$xmlRequest .= "<PageNumber>".$params['page']."</PageNumber>";
	$xmlRequest .= "</Pagination>";
	//$xmlRequest .= "<SKUArray> SKUArrayType";
		//$xmlRequest .= "<SKU> SKUType (string) </SKU>";
	//$xmlRequest .= "</SKUArray>";
	//$xmlRequest .= "<Sort> int </Sort>";
	$xmlRequest .= "<StartTimeFrom>".$params['from_date']."T21:59:59.005Z</StartTimeFrom>";
	$xmlRequest .= "<StartTimeTo>".$params['to_date']."T21:59:59.005Z</StartTimeTo>";
	$xmlRequest .= "<UserID>".$params['ebay_seller']."</UserID>";
	$xmlRequest .= "<DetailLevel>ReturnAll</DetailLevel>";
	$xmlRequest .= "<ErrorLanguage>en_US</ErrorLanguage>";
	//$xmlRequest .= "<MessageID> string </MessageID>";
	//$xmlRequest .= "<OutputSelector> string </OutputSelector>";
	//$xmlRequest .= "<Version> string </Version>";
	$xmlRequest .= "<WarningLevel>Low</WarningLevel>";
	$xmlRequest .= "</GetSellerListRequest>";
	
	$session  = curl_init($endpoint);                       
	curl_setopt($session, CURLOPT_POST, true);              
	curl_setopt($session, CURLOPT_POSTFIELDS, $xmlRequest); 
	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);    
	curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
	$responseXML = curl_exec($session);
	curl_close($session);
	$responseXML = simplexml_load_string($responseXML); 
	$array_items = json_decode(json_encode($responseXML), TRUE);
	
	if (!empty($array_items['Ack']) && $array_items['Ack'] == 'Success') {
		if (!empty($array_items['ItemArray']['Item'])) {
			fn_cp_ebay_import_create_array_of_listings($array_items['ItemArray']['Item'], $params['company_id'], $params['ebay_seller'], $params['ebay_country']);
			if (!empty($array_items['PaginationResult']['TotalNumberOfPages']) && $array_items['PaginationResult']['TotalNumberOfPages'] != 1) {
				$params['page'] = $params['page'] + 1;
				$_SESSION['ebay_seller_page'] = $params['page'];
				if (($params['page']%3) == 0) {
					if ($params['page'] <= $array_items['PaginationResult']['TotalNumberOfPages']) {
						return false;
					}
				}
				if ($params['page'] <= $array_items['PaginationResult']['TotalNumberOfPages']) {
					$res = fn_cp_ebay_import_get_all_sellers_listings($params, $user_ebay_data);
					if (!$res) {
						return false;
					}
				}
			}
		} 
	}
	if (!empty($array_items['Ack']) && $array_items['Ack'] == 'Failure' && !empty($array_items['Errors'])) {
		if (!is_array($array_items['Errors'])) {
			$array_items['Errors'] = array($array_items['Errors']);
		}
		if (empty($array_items['Errors'][0])) {
			fn_set_notification('E', __('error'), __('ebay_request_error').$array_items['Errors']['ShortMessage']);
			
		} else {
			foreach ($array_items['Errors'] as $error) {
				fn_set_notification('E', __('error'), __('ebay_request_error').$error['ShortMessage']);
			}
		}
	}
	return true;
}

function fn_cp_ebay_import_get_ebay_listings ($params) {
	if (!empty($params['ebay_dev_key'])) {
		$user_ebay_data['ebay_dev_key'] = $params['ebay_dev_key'];
	}
	if (!empty($params['ebay_api_key'])) {
		$user_ebay_data['ebay_api_key'] = $params['ebay_api_key'];
	}
	if (!empty($params['ebay_cert_key'])) {
		$user_ebay_data['ebay_cert_key'] = $params['ebay_cert_key'];
	}
	if (!empty($params['ebay_user_token'])) {
		$user_ebay_data['ebay_user_token'] = $params['ebay_user_token'];
	}
	$user_ebay_data['allow_ebay'] = db_get_field("SELECT allow_ebay FROM ?:companies WHERE company_id = ?i", $params['company_id']);
	if (!empty($user_ebay_data['ebay_dev_key']) && !empty($user_ebay_data['ebay_api_key']) && !empty($user_ebay_data['ebay_cert_key']) && !empty($user_ebay_data['ebay_user_token']) && !empty($user_ebay_data['allow_ebay']) && $user_ebay_data['allow_ebay'] == 'Y') {
		
		$params['from_date'] = strtotime($params['from_date']);
		$params['from_date'] = date("Y-m-d", $params['from_date']);
		$params['to_date'] = strtotime($params['to_date']);
		$params['to_date'] = date("Y-m-d", $params['to_date']);
		
		$params['listings_limit'] = Registry::get('addons.cp_ebay_import.step_of_importing_listings');
		if (empty($params['listings_limit'])) {
			$params['listings_limit'] = 50;
		}
		
		if (empty($_SESSION['ebay_new_list_ids'])) {
			$_SESSION['ebay_new_list_ids'] = array();
		}
		if (empty($_SESSION['ebay_seller_page'])) {
			$_SESSION['ebay_seller_page'] = '';
		} else {
			$params['page'] = $_SESSION['ebay_seller_page'];
		}
		$back_result = fn_cp_ebay_import_get_all_sellers_listings($params, $user_ebay_data);
		if (!$back_result) {
			return false;
		}
		if (!empty($_SESSION['ebay_new_list_ids'])) {
			$time_from = strtotime($params['from_date']);
			$time_to = strtotime($params['to_date']);
			$country_name = db_get_field("SELECT country_name FROM ?:ebay_country_code WHERE country_code = ?i", $params['ebay_country']);
			db_query("DELETE FROM ?:ebay_import_transfer WHERE ebay_id NOT IN (?n) AND country_name = ?s AND timestamp >= ?i AND timestamp <= ?i AND ebay_seller = ?s", $_SESSION['ebay_new_list_ids'], $country_name, $time_from, $time_to, $params['ebay_seller']);
		}
		unset($_SESSION['ebay_new_list_ids']);
		unset($_SESSION['ebay_seller_page']);
	} else {
		fn_set_notification('W', __('warning'), __('please_enter_api_keys'));
	}
	fn_set_notification('N', __('notice'), __('ebay_listings_added'));
	return true;
}

function fn_cp_ebay_import_get_vendor_info($params, $items_per_page = 0, $lang_code = CART_LANGUAGE) {
	
	$vendor_data = array();
	$all_listings = array();
	if (!empty($params['company_id'])) {
		$vendor_data = db_get_row("SELECT allow_ebay FROM ?:companies WHERE company_id = ?i", $params['company_id']);
		$vendor_data['company_id'] = $params['company_id'];
	 
		// get all user listings
		$default_params = array (
			'page' => 1,
			'items_per_page' => $items_per_page,
			'get_hidden' => true
		);
		$params = array_merge($default_params, $params);
		$fields = array (
			"?:ebay_import_transfer.*",
		);
		$sortings = array (
			'ebay_seller' => "?:ebay_import_transfer.ebay_seller",
			'product' => "?:ebay_import_transfer.product",
			'price' => "?:ebay_import_transfer.price",
			'amount' => "?:ebay_import_transfer.amount",
			'import_status' => "?:ebay_import_transfer.import_status",
			'select_list' => "?:ebay_import_transfer.select_list",
			'country_name' => "?:ebay_import_transfer.country_name"
		);
		$condition = $join = $group = '';
		if (!empty($params['type']) && $params['type'] == 'simple') {
			if (!empty($params['q'])) {
				$condition .= db_quote(" AND ?:ebay_import_transfer.product LIKE ?l", '%'.$params['q'].'%');
			}
			if (!empty($params['country_code'])) {
				$condition .= db_quote(" AND ?:ebay_import_transfer.country_name LIKE ?l", '%'.$params['country_code'].'%');
			}
		}
		$condition .= db_quote(' AND ?:ebay_import_transfer.company_id = ?i', $params['company_id']);
		$sorting = db_sort($params, $sortings, 'ebay_seller', 'asc');
		$limit = '';
		if (!empty($params['items_per_page'])) {
			$params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:ebay_import_transfer WHERE 1 $condition $group");
			$limit = db_paginate($params['page'], $params['items_per_page']);
		}
		$all_listings = db_get_hash_array('SELECT ' . implode(', ', $fields) . " FROM ?:ebay_import_transfer $join WHERE 1 $condition $group $sorting $limit", 'ebay_id');
		$vendor_data['count_queue'] = db_get_field("SELECT COUNT(*) FROM ?:ebay_import_transfer WHERE select_list = ?s AND company_id = ?i", 'Y', $params['company_id']);
		$vendor_data['ebay_sellers'] = db_get_fields("SELECT ebay_seller FROM ?:ebay_import_transfer WHERE company_id = ?i", $params['company_id']);
		$vendor_data['ebay_sellers'] = array_unique($vendor_data['ebay_sellers']);
	}
	return array($vendor_data, $all_listings, $params);
}
function fn_cp_ebay_import_get_ebay_cat_for_manage ($params, $items_per_page = 0, $lang_code = CART_LANGUAGE) {

	$category_data = array();
	$default_params = array (
		'page' => 1,
		'items_per_page' => $items_per_page,
		'get_hidden' => true
	);
	$params = array_merge($default_params, $params);
	$fields = array (
		"?:ebay_cp_categories.*",
	);
	$sortings = array (
		'ebay_cat_name' => "?:ebay_cp_categories.ebay_cat_name",
		'ebay_cat_path' => "?:ebay_cp_categories.ebay_cat_path",
		'category_id' => "?:ebay_cp_categories.category_id",
		'country_name' => "?:ebay_cp_categories.country_name",
	);
	$condition = $join = $group = '';
	if (!empty($params['type']) && $params['type'] == 'simple') {
		if (!empty($params['q'])) {
			$condition .= db_quote(" AND ?:ebay_cp_categories.ebay_cat_name LIKE ?l", '%'.$params['q'].'%');
		}
		if (!empty($params['country_code'])) {
			$condition .= db_quote(" AND ?:ebay_cp_categories.country_name LIKE ?l", '%'.$params['country_code'].'%');
		}
	}
	
	$sorting = db_sort($params, $sortings, 'ebay_cat_name', 'asc');
	$limit = '';
	if (!empty($params['items_per_page'])) {
		$params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:ebay_cp_categories WHERE 1 $condition $group");
		$limit = db_paginate($params['page'], $params['items_per_page']);
	}
	$category_data = db_get_hash_array('SELECT ' . implode(', ', $fields) . " FROM ?:ebay_cp_categories $join WHERE 1 $condition $group $sorting $limit", 'ebay_category_id');
	
	return array($category_data, $params);
}
function fn_cp_ebay_import_save_ebay_cart_cetegories ($link_with_cat, $ebay_country) {
	
	if (!empty($link_with_cat)) {
		if (!is_array($link_with_cat)) {
			$link_with_cat = array($link_with_cat);
		}
		foreach ($link_with_cat as $ebay_cat => $cart_cat) {
			if (!empty($cart_cat) || $cart_cat == 0) {
				db_query("UPDATE ?:ebay_cp_categories SET category_id = ?i WHERE ebay_category_id = ?i AND country_name = ?s", $cart_cat, $ebay_cat, $ebay_country[$ebay_cat]);
			}
		}
	}
	fn_set_notification('N', __('notice'), __('categories_updated'));
	return true;
}
/*
function fn_cp_ebay_import_update_vendor_info($data) {
	
	if (!empty($data)) {
		$check_new_user = db_get_field("SELECT company_id FROM ?:ebay_import_acc WHERE company_id = ?i", $data['company_id']);
		if (!empty($check_new_user)) {
			db_query("UPDATE ?:ebay_import_acc SET ?u WHERE company_id = ?i", $data, $data['company_id']);
		} else {
			db_query("INSERT INTO ?:ebay_import_acc ?e", $data);
		}
	}
	fn_set_notification('N', __('notice'), __('vendor_acc_updated'));
	return true;
}*/

function fn_cp_ebay_import_import_images($prefix, $image_file, $detailed_file, $position, $type, $object_id, $object)
{
    static $updated_products = array();

    if (!empty($object_id)) {
        // Process multilang requests
        if (!is_array($object_id)) {
            $object_id = array($object_id);
        }

        foreach ($object_id as $_id) {
            if (empty($updated_products[$_id]) && !empty($_REQUEST['import_options']['remove_images']) && $_REQUEST['import_options']['remove_images'] == 'Y') {
                $updated_products[$_id] = true;

                fn_delete_image_pairs($_id, $object, 'A');
            }
            $_REQUEST["server_import_image_icon"] = '';
            $_REQUEST["type_import_image_icon"] = '';

            // Get image alternative text if exists
            if (!empty($image_file) && strpos($image_file, '#') !== false) {
                list ($image_file, $image_alt) = explode('#', $image_file);
            }

            if (!empty($detailed_file) && strpos($detailed_file, '#') !== false) {
                list ($detailed_file, $detailed_alt) = explode('#', $detailed_file);
            }

            if (!empty($image_alt)) {
                preg_match_all('/\[([A-Za-z]+?)\]:(.*?);/', $image_alt, $matches);
                if (!empty($matches[1]) && !empty($matches[2])) {
                    $image_alt = array_combine(array_values($matches[1]), array_values($matches[2]));
                }
            }

            if (!empty($detailed_alt)) {
                preg_match_all('/\[([A-Za-z]+?)\]:(.*?);/', $detailed_alt, $matches);
                if (!empty($matches[1]) && !empty($matches[2])) {
                    $detailed_alt = array_combine(array_values($matches[1]), array_values($matches[2]));
                }
            }
            $type_image_detailed = (strpos($detailed_file, '://') === false) ? 'server' : 'url';
            $type_image_icon = (strpos($image_file, '://') === false) ? 'server' : 'url';

            $_REQUEST["type_import_image_icon"] = array($type_image_icon);
            $_REQUEST["type_import_image_detailed"] = array($type_image_detailed);
            
            $image_file = fn_cp_ebay_import_find_file($prefix, $image_file);

            if ($image_file !== false) {
                if ($type_image_icon == 'url') {
                    $_REQUEST["file_import_image_icon"] = array($image_file);

                } elseif (strpos($image_file, Registry::get('config.dir.root')) === 0) {
                    $_REQUEST["file_import_image_icon"] = array(str_ireplace(fn_get_files_dir_path(), '', $image_file));

                } else {
                    fn_set_notification('E', __('error'), __('error_images_need_located_root_dir'));
                    $_REQUEST["file_import_image_detailed"] = array();
                }
            } else {
                $_REQUEST["file_import_image_icon"] = array();
            }
            $detailed_file = fn_cp_ebay_import_find_file($prefix, $detailed_file);
            if ($detailed_file !== false) {
                if ($type_image_detailed == 'url') {
                    $_REQUEST["file_import_image_detailed"] = array($detailed_file);

                } elseif (strpos($detailed_file, Registry::get('config.dir.root')) === 0) {
                    $_REQUEST["file_import_image_detailed"] = array(str_ireplace(fn_get_files_dir_path(), '', $detailed_file));

                } else {
                    fn_set_notification('E',  __('error'), __('error_images_need_located_root_dir'));
                    $_REQUEST["file_import_image_detailed"] = array();
                }

            } else {
                $_REQUEST["file_import_image_detailed"] = array();
            }
            $_REQUEST['import_image_data'] = array(
                array(
                    'type' => $type,
                    'image_alt' => empty($image_alt) ? '' : $image_alt,
                    'detailed_alt' => empty($detailed_alt) ? '' : $detailed_alt,
                    'position' => empty($position) ? 0 : $position,
                )
            );

            $result = fn_attach_image_pairs('import', $object, $_id);
        }
        return $result;
    }
    return false;
}
function fn_cp_ebay_import_find_file($prefix, $file)
{
    $file = Bootstrap::stripSlashes($file);

    // Url
    if (strpos($file, '://') !== false) {
        return $file;
    }
    $prefix = fn_normalize_path(rtrim($prefix, '/'));
    $file = fn_normalize_path($file);
    $files_path = fn_get_files_dir_path();

    // Absolute path
    if (is_file($file) && strpos($file, $files_path) === 0) {
        return $file;
    }
    // Path is relative to files directory
    if (is_file($files_path . $file)) {
        return $files_path . $file;
    }
    // Path is relative to prefix inside files directory
    if (is_file($files_path . $prefix . '/' . $file)) {
        return $files_path . $prefix . '/' . $file;
    }
    // Prefix is absolute path
    if (strpos($prefix, $files_path) === 0 && is_file($prefix . '/' . $file)) {
        return $prefix . '/' . $file;
    }
    return false;
}