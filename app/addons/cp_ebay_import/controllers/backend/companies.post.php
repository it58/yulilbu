<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/* POST data processing */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
   if ($mode == 'allow_accsess_ebay') {
		if (!empty($_REQUEST['company_ids'])) {
			$params = array();
			$params['allow_accsess'] = 'Y';
			fn_cp_ebay_import_change_accsess_to_ebay ($_REQUEST['company_ids'], $params['allow_accsess']);
		}
		$suffix = ".manage";
    }
    if ($mode == 'forbid_accsess_ebay') {
		if (!empty($_REQUEST['company_ids'])) {
			$params = array();
			$params['allow_accsess'] = 'N';
			fn_cp_ebay_import_change_accsess_to_ebay ($_REQUEST['company_ids'], $params['allow_accsess']);
		}
		$suffix = ".manage";
    }
}
