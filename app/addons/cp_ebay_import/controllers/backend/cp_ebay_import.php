<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/* POST data processing */
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $suffix = '';
    if (!empty($_REQUEST['ebay_data'])) {
		if (empty($_SESSION['cp_ebay_data_key'])) {
			$_SESSION['cp_ebay_data_key'] = array();
		}
		if (!empty($_REQUEST['ebay_data']['ebay_dev_key'])) {
			$_SESSION['cp_ebay_data_key']['ebay_dev_key'] = $_REQUEST['ebay_data']['ebay_dev_key'];
		}
		if (!empty($_REQUEST['ebay_data']['ebay_api_key'])) {
			$_SESSION['cp_ebay_data_key']['ebay_api_key'] = $_REQUEST['ebay_data']['ebay_api_key'];
		}
		if (!empty($_REQUEST['ebay_data']['ebay_cert_key'])) {
			$_SESSION['cp_ebay_data_key']['ebay_cert_key'] = $_REQUEST['ebay_data']['ebay_cert_key'];
		}
		if (!empty($_REQUEST['ebay_data']['ebay_user_token'])) {
			$_SESSION['cp_ebay_data_key']['ebay_user_token'] = $_REQUEST['ebay_data']['ebay_user_token'];
		}
	}
    /*
    if ($mode == 'manage') {
		if (!empty($_REQUEST['ebay_data'])) {
			fn_cp_ebay_import_update_vendor_info ($_REQUEST['ebay_data']);
		}
		$suffix = ".manage";
    }
    */
    if ($mode == 'get_listings') {
		if (!empty($_REQUEST['ebay_data']['company_id']) && !empty($_REQUEST['ebay_data']['ebay_seller'])) {
			$result = fn_cp_ebay_import_get_ebay_listings ($_REQUEST['ebay_data']);
			if (!$result) {
				$_SESSION['ebay_data_continue'] = $_REQUEST['ebay_data'];
				$url = fn_url('cp_ebay_import.continue_listings');
				@print '<meta http-equiv="Refresh" content="0;url='.$url.'">';
				exit;
			}
		}
		$suffix = ".manage";
    }
    if ($mode == 'first_import_from_ebay') {
		if (!empty($_REQUEST['ebay_data']['company_id'])) {
			$result = fn_cp_ebay_import_import_listings_from_ebay ($_REQUEST['ebay_data']);
			if (!$result) {
				$_SESSION['ebay_data_continue'] = $_REQUEST['ebay_data'];
				$url = fn_url('cp_ebay_import.import_from_ebay');
				@print '<meta http-equiv="Refresh" content="0;url='.$url.'">';
				exit;
			} 
		}
		$suffix = ".manage";
    }
    if ($mode == 'save_selected') {
    
		if (!empty($_REQUEST['ebay_listing_ids'])) {
			fn_cp_ebay_import_save_selected_listings_checkbox ($_REQUEST['ebay_listing_ids'], $_REQUEST['ebay_listing_not_sel'], $_REQUEST['company_id'], $_REQUEST['ebay_country']);
		}
		$suffix = ".manage";
    }
    if ($mode == 'unselec_selected') {
		
		if (!empty($_REQUEST['ebay_listing_ids'])) {
			fn_cp_ebay_import_unselect_selected_listings ($_REQUEST['ebay_listing_ids'], $_REQUEST['company_id'], $_REQUEST['ebay_country']);
		}
		$suffix = ".manage";
	}
	if ($mode == 'delete_seller_listings') {
		if (!empty($_REQUEST['ebay_seller_for_delete'])) {
			fn_cp_ebay_import_delete_ebay_seller_listings ($_REQUEST['ebay_seller_for_delete'], $_REQUEST['ebay_data']['company_id'], $_REQUEST['ebay_data']['ebay_country']);
		}
		$suffix = ".manage";
	}
	
	if ($mode == 'get_categories') {
		if (isset($_REQUEST['ebay_country_get_cat']) && !empty($_REQUEST['ebay_data'])) {
			fn_cp_ebay_import_get_ebay_categories($_REQUEST['ebay_country_get_cat'], $_REQUEST['lvl_limit'], $_REQUEST['ebay_data']);
		}
		$suffix = ".category_manage";
	}
	
	if ($mode == 'delete_ebay_categories') {
		if (isset($_REQUEST['ebay_country_get_cat'])) {
			fn_cp_ebay_import_delete_ebay_categories($_REQUEST['ebay_country_get_cat']);
		}
		$suffix = ".category_manage";
	}
	if ($mode == 'category_manage') {
		if (!empty($_REQUEST['link_with_cat']) && !empty($_REQUEST['ebay_country'])) {
			fn_cp_ebay_import_save_ebay_cart_cetegories($_REQUEST['link_with_cat'], $_REQUEST['ebay_country']);
		}
		$suffix = ".category_manage";
	}
	if ($mode == 'global_mapping') {
		if (!empty($_REQUEST['ebay_category_ids']) && !empty($_REQUEST['ebay_country'])) {
			fn_cp_ebay_import_save_ebay_global_cat($_REQUEST['ebay_category_ids'], $_REQUEST['ebay_country'], $_REQUEST['global_mapping']);
		} else {
			fn_set_notification('N', __('notice'), __('select_ebay_categories'));
		}
		$suffix = ".category_manage";
	}
    return array(CONTROLLER_STATUS_OK, "cp_ebay_import$suffix");
}
if ($mode == 'manage') {

	$params = $_REQUEST;
	$params['company_id'] = Registry::get('runtime.company_id');
	
	list($ebay_data, $all_listings, $search) = fn_cp_ebay_import_get_vendor_info($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
	$cp_ebay_data_key = array();
	if (!empty($_SESSION['cp_ebay_data_key'])) {
		$cp_ebay_data_key = $_SESSION['cp_ebay_data_key'];
	}
	Registry::get('view')->assign('cp_ebay_data_key', $cp_ebay_data_key);
	Registry::get('view')->assign('search', $search);
	Registry::get('view')->assign('all_listings', $all_listings);
    Registry::get('view')->assign('ebay_data', $ebay_data);

} 
if ($mode == 'category_manage') {
	$params = $_REQUEST;
	$params['company_id'] = Registry::get('runtime.company_id');
	list($ebay_data, $search) = fn_cp_ebay_import_get_ebay_cat_for_manage($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
	$cp_ebay_data_key = array();
	if (!empty($_SESSION['cp_ebay_data_key'])) {
		$cp_ebay_data_key = $_SESSION['cp_ebay_data_key'];
	}
	Registry::get('view')->assign('cp_ebay_data_key', $cp_ebay_data_key);
	Registry::get('view')->assign('search', $search);
    Registry::get('view')->assign('ebay_data', $ebay_data);

}
/*
if ($mode == 'import_from_ebay') {

	if (!empty($_REQUEST['company_id'])) {
		$result = fn_cp_ebay_import_import_listings_from_ebay ($_REQUEST['company_id']);
		if (!$result) {
			$url = fn_url('cp_ebay_import.import_from_ebay&company_id='.$_REQUEST['company_id']);
			Registry::get('view')->assign('process', $_SESSION['ebay_import']);
			fn_echo('<meta http-equiv="Refresh" content="0;url='.$url.'">');
		} else {
			return array(CONTROLLER_STATUS_OK, "cp_ebay_import.manage");
		}
	}
}
*/
if ($mode == 'add_all_to_queue') {

	if (!empty($_REQUEST['company_id'])) {
		fn_cp_ebay_import_add_all_to_import_queue($_REQUEST['company_id'], $_REQUEST['status']);
	}
	return array(CONTROLLER_STATUS_REDIRECT, "cp_ebay_import.manage");
}
if ($mode == 'continue_listings') {
	if (!empty($_SESSION['ebay_data_continue'])) {
		$_REQUEST['ebay_data'] = array();
		$_REQUEST['ebay_data'] = $_SESSION['ebay_data_continue'];
	}
	if (!empty($_REQUEST['ebay_data'])) {
		
		$result = fn_cp_ebay_import_get_ebay_listings ($_REQUEST['ebay_data']);
		if (!$result) {
			$url = fn_url('cp_ebay_import.continue_listings');
			$count_items = count($_SESSION['ebay_new_list_ids']);
			Registry::get('view')->assign('count_items', $count_items);
			fn_echo('<meta http-equiv="Refresh" content="0;url='.$url.'">');
		} else {
			return array(CONTROLLER_STATUS_OK, "cp_ebay_import.manage");
		}
	}
} elseif ($mode == 'import_from_ebay') {
	if (!empty($_SESSION['ebay_data_continue'])) {
		$_REQUEST['ebay_data'] = array();
		$_REQUEST['ebay_data'] = $_SESSION['ebay_data_continue'];
	}
	if (!empty($_REQUEST['ebay_data'])) {
		
		$result = fn_cp_ebay_import_import_listings_from_ebay ($_REQUEST['ebay_data']);
		if (!$result) {
			$url = fn_url('cp_ebay_import.import_from_ebay');
			Registry::get('view')->assign('process', $_SESSION['ebay_import']);
			fn_echo('<meta http-equiv="Refresh" content="0;url='.$url.'">');
		} else {
			return array(CONTROLLER_STATUS_OK, "cp_ebay_import.manage");
		}
	}
}








