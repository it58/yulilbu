<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Mailer;
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode == 'update' || $mode == 'add'){
  if(fn_allowed_for('MULTIVENDOR')){
    $c=1;
    if(Registry::get('runtime.company_id')){
       $company_id=Registry::get('runtime.company_id');
       $user_type=db_get_field("SELECT user_type FROM ?:users where company_id = ?i",$company_id);
       if($user_type == 'V'){
           
           $shipping_id=db_get_array("SELECT shipping_id FROM ?:shippings WHERE company_id=?i AND status=?s",$company_id,'A');
           if(empty($shipping_id)){
            fn_set_notification('W', __('warning'), __('wk_select_shipping_first', array(
                '[url]' => fn_url('shippings.manage'),
            )));
            $c=0;
           }
           else
              $c=1;

               
       }
    }
    Registry::get('view')->assign('shipping_enable', $c);
}
}

if($mode == 'manage'){
    if(fn_allowed_for('MULTIVENDOR')){
        $c=1;
        if(Registry::get('runtime.company_id')){
           $company_id=Registry::get('runtime.company_id');
           $user_type=db_get_field("SELECT user_type FROM ?:users where company_id = ?i",$company_id);
           if($user_type == 'V'){
               $c=0;
               $shipping_id=db_get_array("SELECT shipping_id FROM ?:shippings WHERE company_id=?i AND status=?s",$company_id,'A');
               if(empty($shipping_id)){
                fn_set_notification('W', __('warning'), __('wk_select_shipping_first', array(
                    '[url]' => fn_url('shippings.manage'),
                )));
                $c=0;
               }
               else
                  $c=1;
            }
        }
        Registry::get('view')->assign('shipping_', $c);       
    }
}
  