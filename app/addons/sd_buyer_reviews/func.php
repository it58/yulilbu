<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Gets discussion data of ordered products
 *
 */
function fn_sd_buyer_reviews_get_order_items_info_post(&$order, $v, $k)
{
    if (!empty($v['product_id']) && AREA == 'C' && !empty(Tygh::$app['session']['auth']['user_id'])) {
        $order['products'][$k]['extra']['discussion'] = db_get_row(
            'SELECT ?:discussion.type, ?:discussion.thread_id, COUNT(?:discussion_posts.post_id) AS reviews_amount FROM ?:discussion '
            . 'LEFT JOIN ?:discussion_posts ON ?:discussion_posts.thread_id = ?:discussion.thread_id '
            . 'WHERE ?:discussion.object_id = ?i AND ?:discussion.object_type = ?s AND ?:discussion_posts.user_id = ?i', $v['product_id'], 'P', Tygh::$app['session']['auth']['user_id']
        );
    }
}