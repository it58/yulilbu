<?php

$schema['seller_badges'] = array(
   'permissions' => array('GET' => 'view_seller_badges', 'POST' => 'manage_seller_badges'),
   'modes' => array(
        'delete' => array(
           'permissions' => 'manage_banners_badges'
 	    )
    ),
);
$schema['assign_badge'] = array(
   'permissions' => array('GET' => 'view_assign_badge', 'POST' => 'manage_assign_badge'),
   'modes' => array(
        'delete' => array(
           'permissions' => 'manage_assign_badge'
 	    )
    ),
);
$schema['tools']['modes']['update_status']['param_permissions']['table']['seller_badges'] = 'manage_seller_badges';
$schema['tools']['modes']['update_status']['param_permissions']['table']['assign_badge'] = 'manage_assign_badge';


return $schema;

?>