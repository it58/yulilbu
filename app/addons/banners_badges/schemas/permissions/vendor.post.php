<?php


$schema['controllers']['seller_badges'] = array (
    'permissions' => true,
    );
$schema['controllers']['assign_badge'] = array (
    'permissions' => true,
    );
$schema['controllers']['tools']['modes']['update_status']['param_permissions']['table']['seller_badges'] = true;
$schema['controllers']['tools']['modes']['update_status']['param_permissions']['table']['assign_badge'] = true;

return $schema;
?>