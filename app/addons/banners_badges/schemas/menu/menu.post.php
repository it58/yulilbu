<?php
/******************************************************************
# Seller Badges -  Seller Badges                                 *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/   
  $schema['central']['vendors']['items']['Seller_Badges']=array(
  	'attrs'=>array(
  		'class'=>'is-addon'
  		),
  	'href'=>'seller_badges.manage',
  	'position'=>30,
  	'subitems' => array(
	  		'create_badges' => array(
	            'href' => 'seller_badges.manage',
	            'position' => 203
	        ),
        'assignment' => array(
          'href' => 'assign_badge.add',
          'position' => 204
          ),
  		)
  	);
  return $schema;
?>  