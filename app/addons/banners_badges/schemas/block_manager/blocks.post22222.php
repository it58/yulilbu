<?php

use Tygh\Registry;
use Tygh\Session;

$schema['badges'] = array (
    'content' => array (
        'items' => array (
            'remove_indent' => true,
            'hide_label' => true,
            'type' => 'enum',
            'object' => 'badges',
            'items_function' => 'fn_get_badges',
            'fillings' => array (
                'manually' => array (
                    'picker' => 'addons/banners_badges/pickers/assign_badge/picker.tpl',
                    'picker_params' => array (
                        'view_mode' => 'list',
                        'data_id' => 'badge_share',
                        'input_name' => 'seller_badges_data[badge_id]',
                        'multiple' => 'true',
                        'show_add_button' => 'true',
                        'hidden_field' => 'true', 

                    ),
                    'params' => array (
                        'sort_by' => 'position',
                        'sort_order' => 'asc'
                    )
                ),
                'newest' => array (
                    'params' => array (
                        'sort_by' => 'timestamp',
                        'sort_order' => 'desc',
                    )
                ),
            ),
        ),
    ),
    'templates' => array (
        'addons/banners_badges/blocks/carousel.tpl' => array(
            'settings' => array (
                'navigation' => array (
                    'type' => 'selectbox',
                    'values' => array (
                        'N' => 'none',
                        'D' => 'dots',
                        'P' => 'pages',
                        'A' => 'arrows'
                    ),
                    'default_value' => 'D'
                ),
                'delay' => array (
                    'type' => 'input',
                    'default_value' => '3'
                ),
            ),
        )
    ),
    'wrappers' => 'blocks/wrappers',
);

return $schema;
