<?php
use Tygh\Registry;
use Tygh\Session;
$schema['badges'] = array (
    'templates' =>'addons/banners_badges/blocks/carousel.tpl',
    'settings' => array (
                'navigation' => array (
                    'type' => 'selectbox',
                    'values' => array (
                        'N' => 'none',
                        'D' => 'dots',
                        'P' => 'pages',
                        'A' => 'arrows'
                    ),
                    'default_value' => 'D'
                ),
                'delay' => array (
                    'type' => 'input',
                    'default_value' => '3'
                ),
            ),
  
    'wrappers' => 'blocks/wrappers',
);

return $schema;
