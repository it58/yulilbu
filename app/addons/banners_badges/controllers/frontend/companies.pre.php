<?php
/******************************************************************
# Vendor Badges                        		  					  *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/

use Tygh\Registry;
use Tygh\Settings;
use Tygh\BlockManager\Layout;
use Tygh\Themes\Styles;

if($mode == 'products'){
	$company_data = !empty($_REQUEST['company_id']) ? fn_get_company_data($_REQUEST['company_id']) : array();

    if (empty($company_data)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    $company_id = $company_data['company_id'];
    $vendor_banner_data = fn_get_image_pairs($company_id, 'vendor_banner', 'M', true, false);
    $vendor_banner_data['url'] = fn_get_banner_url($company_id);
    Tygh::$app['view']->assign('vendor_banner_data', $vendor_banner_data);
	
}