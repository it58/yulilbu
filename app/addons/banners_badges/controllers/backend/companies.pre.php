<?php
/******************************************************************
# Vendor Badges                        		                      *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/

use Tygh\Registry;
use Tygh\Settings;
use Tygh\BlockManager\Layout;
use Tygh\Themes\Styles;

if ($mode == 'update' || $mode == 'add') {
        $tabs['badges'] = array(
            'title' => __('badges'),
            'js' => true
        );
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
if ($mode == 'update') {
	$company_id = $_REQUEST['company_id'];
	if(isset($_REQUEST['company_data']) && !empty($_REQUEST['company_data'])){
		fn_attach_image_pairs('banners_badges_main', 'vendor_banner', $company_id, $lang_code = DESCR_SL);
		$data = array(
		'company_id' => $company_id,
		'url' => $_REQUEST['company_data']['banners_badges_url'],
		);
		db_query("DELETE FROM ?:seller_banners WHERE company_id = ?i", $company_id);
		db_query('INSERT INTO ?:seller_banners ?e', $data);
	}
 	}
}
if($mode == 'update')
{
	$company_id = $_REQUEST['company_id'];
	$banner_data = fn_get_image_pairs($company_id, 'vendor_banner', 'M', true, false);
	$banner_data['url'] = db_get_field("SELECT url FROM ?:seller_banners WHERE company_id = ?i",$company_id);
	Registry::get('view')->assign('banners_badges_data' , $banner_data);
	$badges_assign_list = db_get_array("SELECT * FROM ?:seller_badges_assignment WHERE company_id = ?i",$company_id);
	foreach ($badges_assign_list as $key => $badge_assign_data) {
		$res = fn_get_image_pairs($badge_assign_data['badge_id'], 'seller_badges', 'M', true, false);
		$badges_assign_list[$key]['main_pair'] = $res;
		$company_name = db_get_field("SELECT company FROM ?:companies WHERE company_id=?i",$badge_assign_data['company_id']);
		$badges_assign_list[$key]['company_name'] = $company_name;	
		$badge_name = db_get_field("SELECT badge_name FROM ?:seller_badges WHERE id=?i",$badge_assign_data['badge_id']);
		$badges_assign_list[$key]['badge_name'] = $badge_name;	
	}
	Registry::get('view')->assign('badges_assign_list', $badges_assign_list);
}
if ($mode == 'b_delete') {
	$company_id = $_REQUEST['company_id'];
	$id = $_REQUEST['id'];
	db_query("DELETE FROM ?:seller_badges_assignment WHERE id = ?i", $id );
	fn_set_notification('N', __('notice'), __('assign_badge_removed_from_particular_seller'));
	return array(CONTROLLER_STATUS_REDIRECT, "companies.update?company_id=$company_id&selected_section=badges");
}
