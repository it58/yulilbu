<?php
/******************************************************************
# Vendor Badges                                                   *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/

use Tygh\Registry;
use Tygh\Settings;
use Tygh\Mailer;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }     
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if ($mode == 'update' && $_REQUEST['addon'] == 'banners_badges' && (!empty($_REQUEST['badges_sales_period'])))
        {
            fn_trusted_vars('badges_sales_period');
            fn_banners_badges_setting_data($_REQUEST['badges_sales_period']);  
        }
        if ($mode == 'update' && $_REQUEST['addon'] == 'banners_badges' && (!empty($_REQUEST['wk_banners_badges'])))
        {
            fn_trusted_vars('wk_banners_badges');
            fn_banners_badges_mail_setting_data($_REQUEST['wk_banners_badges']);  
        }
        if(($mode == 'update') && ($_REQUEST['addon'] == 'banners_badges') && !empty($_REQUEST['file_default_vendor_logo_image_icon']))
        {
            fn_attach_image_pairs('default_vendor_logo', 'default_vendor_logo', 0);
        }
    }
    if ($mode == 'update') {
        if ($_REQUEST['addon'] == 'banners_badges') {
            $badges_sales_data = fn_banners_badges_get_setting_data();
            $wk_banners_badges_mail_settings_data = fn_banners_badges_get_mail_setting_data();           
            Registry::get('view')->assign('badges_sales_data', $badges_sales_data);
            Registry::get('view')->assign('wk_banners_badges_mail_settings_data', $wk_banners_badges_mail_settings_data);
            $default_vendor_logo_data = fn_get_image_pairs(0, 'default_vendor_logo', 'M', false, true);
            Registry::get('view')->assign('default_vendor_logo_data', $default_vendor_logo_data);
        }
    }
       
function fn_banners_badges_setting_data($badges_sales_selected_period,$company_id = null)
{
    if (!$setting_id = Settings::instance()->getId('badges_sales_data', '')) {
        $setting_id = Settings::instance()->update(array(
            'name' =>           'badges_sales_data',
            'section_id' =>     0,
            'section_tab_id' => 0,
            'type' =>           'A', 
            'position' =>       0,
            'is_global' =>      'N',
            'handler' =>        ''
        ));
    }   
    Settings::instance()->updateValueById($setting_id, serialize($badges_sales_selected_period), $company_id);
}
function fn_banners_badges_mail_setting_data($badges_mail_template_data,$company_id = null)
{
    if (!$setting_id = Settings::instance()->getId('badges_mail_data', '')) {
        $setting_id = Settings::instance()->update(array(
            'name' =>           'badges_mail_data',
            'section_id' =>     0,
            'section_tab_id' => 0,
            'type' =>           'A', 
            'position' =>       0,
            'is_global' =>      'N',
            'handler' =>        ''
        ));
    }   
    Settings::instance()->updateValueById($setting_id, serialize($badges_mail_template_data), $company_id);
}
?>