<?php
/******************************************************************
# Seller Badges--- Seller Badges                                  *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/ 

use Tygh\Embedded;
use Tygh\Http;
use Tygh\Mailer;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Session;
use Tygh\Settings;
use Tygh\Shippings\Shippings;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'manage') {

	$params = $_REQUEST;
	$condition = '';
	$limit = '';
	$order = '';
	if (isset($_REQUEST['sort_order'])) {
		if($_REQUEST['sort_order'] == 'asc'){
			$params['sort_order_rev'] = 'desc';
			$params['sort_order'] = $_REQUEST['sort_order'];
		}else{
			$params['sort_order_rev'] = 'asc';
			$params['sort_order'] = $_REQUEST['sort_order'];
		}
	}else{
		$params['sort_order_rev'] = 'asc';
		$params['sort_order'] = 'desc';
	}
	if (isset($params['is_search']) && $params['is_search'] == "Y") {
		if (isset($params['badge_name']) && !empty($params['badge_name'])) {
			$condition = db_quote(" AND badge_name LIKE ?l", "%{$params['badge_name']}%");
		}
		
	}
	if (empty($params['items_per_page'])) {
		$params['items_per_page'] = Registry::get('settings.Appearance.admin_elements_per_page');
	}
	if(empty($params['page'])) {
		$params['page'] = 1;
	}

	$params['total_items'] = db_get_field("SELECT count(*) FROM ?:seller_badges WHERE 1 $condition $order $limit");
	if (!empty($params['limit'])) {
		$limit = db_quote(" LIMIT 0, ?i", $params['limit']);
	} elseif (!empty($params['items_per_page'])) {
		$limit = db_paginate($params['page'], $params['items_per_page'] , $params['total_items']);
	}
	$badges_list = db_get_array("SELECT * FROM ?:seller_badges WHERE 1 $condition $order $limit");
	foreach ($badges_list as $key => $badge_data) {
		$res = fn_get_image_pairs($badge_data['id'], 'seller_badges', 'M', true, false);
		$badges_list[$key]['main_pair'] = $res;	
	}
	Registry::get('view')->assign('badges_list', $badges_list);

	Registry::get('view')->assign('search', $params);

}
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	if($mode == 'update_status')
	{
		fn_tools_update_status($_REQUEST);
		fn_banners_badges_clear_cache();
	}
	
	if ($mode == 'add') {
		if(isset($_REQUEST['seller_badges_data']) && !empty($_REQUEST['seller_badges_data']))
		{
			$seller_badges_data = $_REQUEST['seller_badges_data'];
			$badge_seller_can_have = fn_banners_badges_get_setting_seller_badge();
			if(isset($seller_badges_data['badge_autoenable']) && $seller_badges_data['badge_autoenable'] == 'Y')
			{	
				$auto_enable = $seller_badges_data['badge_autoenable'];
				if($seller_badges_data['criteria'][1] == NULL)
				{
					fn_save_post_data('seller_badges_data');
					fn_set_notification('E', __('error'), __('select_atleast_one_condition_for_autoenable'));
	      			return array(CONTROLLER_STATUS_REDIRECT, "seller_badges.add");
	      		}
				
			}else
			{
			 $auto_enable = 'N';
			}

			if (isset($seller_badges_data['company_id']) && !empty($seller_badges_data['company_id'])) {
				$total_vendor_selected = count($seller_badges_data['company_id']);
				if($total_vendor_selected > $seller_badges_data['no_of_seller'])
				{
					fn_save_post_data('seller_badges_data');
					fn_set_notification('E', __('error'), __('no_of_seller_selected_are_more_than_total_seller_to_be_assigned'));
	          		return array(CONTROLLER_STATUS_OK, "seller_badges.add"); 
	          	}

			}
			$current_priority = $seller_badges_data['priority'];
			$check_priority_exist = db_get_field("SELECT id FROM ?:seller_badges WHERE priority=?i",$seller_badges_data['priority']); 
			if($check_priority_exist)
			{
				fn_save_post_data('seller_badges_data');
				fn_set_notification('E', __('error'), __('same_priority_badge_exist_already'));
	          	return array(CONTROLLER_STATUS_OK, "seller_badges.add"); 
			}
			else{
				$badge_id = db_get_next_auto_increment_id('seller_badges');
				fn_attach_image_pairs('banners_badges_main', 'seller_badges', $badge_id, $lang_code = DESCR_SL);
				$description = $seller_badges_data['badge_description'];
				$url = $seller_badges_data['url'];
				$data = array(
					'priority' => $seller_badges_data['priority'],
					'badge_name' => $seller_badges_data['badge_name'],
					'desc' => $description,
					'status' => $seller_badges_data['status'],
					'url' => $url,
					'total_seller' => $seller_badges_data['no_of_seller'],
					'created_on' => time(),
					'auto_enable' => $auto_enable,
					);
				db_query('INSERT INTO ?:seller_badges ?e', $data);
				if (isset($seller_badges_data['company_id']) && !empty($seller_badges_data['company_id']))
				{
				foreach ($seller_badges_data['company_id'] as $key => $value) 
				{
					$is_already_have_badge = db_get_field("SELECT count(id) FROM ?:seller_badges_assignment WHERE company_id = ?i",$value);
					if (($badge_seller_can_have == 'single') && $is_already_have_badge) 
					{
					fn_save_post_data('seller_badges_data');
					fn_set_notification('E', __('error'), __('onle_one_badge_can_be_assigned_to_a_seller'));
					return array(CONTROLLER_STATUS_OK, "assign_badge.add");
					}else{
						$data = array(
						'badge_id' => $badge_id,
						'company_id' => $value,
						'assign_time' => time(),
						'manual_assign' => 1
						);
						db_query('INSERT INTO ?:seller_badges_assignment ?e', $data);
						fn_badges_notify_to_vendor($value,$badge_id,time());
					}	
				}
				}				
				$count = sizeof($seller_badges_data['criteria']);
				if($seller_badges_data['criteria'][1] != NULL){	
					for ($i=1; $i <= $count ; $i++) { 
						$data = array(
							'badge_id' =>$badge_id,
							'condition' => $seller_badges_data['condition'][$i],
							'criteria' => $seller_badges_data['criteria'][$i],
							'amount' => $seller_badges_data['amount'][$i]
							);
						db_query('INSERT INTO ?:seller_badges_conditions ?e', $data);
					}
				}
				fn_set_notification('N', __('notice'), __('badge_created_successfully'));
				return array(CONTROLLER_STATUS_OK, "seller_badges.update?id=$badge_id");
			}
		}	
	}
	if($mode == 'update')
	{

		// exit;
		fn_banners_badges_clear_cache();
		$err = 0;
		if(isset($_REQUEST['seller_badges_data']) && !empty($_REQUEST['seller_badges_data']))
		{
			$seller_badges_data = $_REQUEST['seller_badges_data'];
			$id = $_REQUEST['id'];
			$current_priority = $_REQUEST['seller_badges_data']['priority'];
			$check_priority_exist = db_get_field("SELECT id FROM ?:seller_badges WHERE priority=?i",$_REQUEST['seller_badges_data']['priority']); 
			if (isset($seller_badges_data['company_id']) && !empty($seller_badges_data['company_id']))
			{
				$total_vendor_selected = count($seller_badges_data['company_id']);
				if($total_vendor_selected > $seller_badges_data['no_of_seller'])
				{
					fn_set_notification('E', __('error'), __('no_of_seller_selected_are_more_than_total_seller_to_be_assigned'));
	          		$err = 1;
	          	}

			}
			if(isset($_REQUEST['seller_badges_data']['pre_criteria']) && !empty($_REQUEST['seller_badges_data']['pre_criteria']))
			{
				if(isset($seller_badges_data['badge_autoenable']))
					$auto_enable = 'Y';
				else
					$auto_enable = 'N';
			}else{
				if(isset($seller_badges_data['badge_autoenable']) && $seller_badges_data['badge_autoenable'] == 'Y')
				{	
					$auto_enable = $seller_badges_data['badge_autoenable'];
					if($seller_badges_data['criteria'][1] == NULL)
					{
						fn_set_notification('E', __('error'), __('select_atleast_one_condition_for_autoenable'));
		      			return array(CONTROLLER_STATUS_OK, "seller_badges.update?id=$id");
		      		}
					
				}else
					$auto_enable = 'N';
			}
			if(!empty($check_priority_exist) && ($check_priority_exist != $_REQUEST['id']))
			{
				$id = $_REQUEST['id'];
				fn_set_notification('E', __('error'), __('same_priority_badge_exist_already'));
	          	$err = 1;
	          	
			}
			if ($err) {
			 return array(CONTROLLER_STATUS_OK, "seller_badges.update?id=$id"); 
			}else{
				$badge_id = $_REQUEST['id'];
				fn_attach_image_pairs('banners_badges_main', 'seller_badges', $badge_id, $lang_code = DESCR_SL);
				if($auto_enable == 'N')
					fn_check_badge_auto_enable($auto_enable,$badge_id);
				fn_check_changes_in_badge_data($seller_badges_data,$badge_id);
				$data = array(
					'priority' => $_REQUEST['seller_badges_data']['priority'],
					'badge_name' => $_REQUEST['seller_badges_data']['badge_name'],
					'desc' => $_REQUEST['seller_badges_data']['badge_description'],
					'status' => $_REQUEST['seller_badges_data']['status'],
					'url' => $_REQUEST['seller_badges_data']['url'],
					'total_seller' => $_REQUEST['seller_badges_data']['no_of_seller'],
					'auto_enable' => $auto_enable
					);
				db_query('UPDATE ?:seller_badges SET ?u WHERE id = ?i ' , $data , $badge_id);
				$company_ids = $_REQUEST['seller_badges_data']['company_id'];
				
				/*if(!empty($already_assigned) && !empty($company_ids))
				{
					foreach ($already_assigned as $company_id => $manual_assign) {
						if(!(in_array($company_id, $company_ids)))
						db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i AND company_id = ?i", $badge_id,$company_id );	
					}
				}*/
				fn_badges_check_already_assigned($company_ids,$badge_id);
				
				if (isset($company_ids) && !empty($company_ids)) {
					foreach ($company_ids as $key => $company_id) 
					{
						$data = array(
						'badge_id' => $badge_id,
						'company_id' => $company_id,
						'assign_time' => time(),
						'manual_assign' => 1
						);
					db_query('INSERT INTO ?:seller_badges_assignment ?e', $data);
					fn_badges_notify_to_vendor($company_id,$badge_id,time());
					}
				}
				if (isset($_REQUEST['seller_badges_data']['criteria'][1]) && !empty($_REQUEST['seller_badges_data']['criteria'][1])) {
					$count = sizeof($_REQUEST['seller_badges_data']['criteria']);
				for ($i=1; $i <= $count ; $i++) { 
					$data = array(
						'badge_id' =>$badge_id,
						'condition' => $_REQUEST['seller_badges_data']['condition'][$i],
						'criteria' => $_REQUEST['seller_badges_data']['criteria'][$i],
						'amount' => $_REQUEST['seller_badges_data']['amount'][$i]
						);
					db_query('INSERT INTO ?:seller_badges_conditions ?e', $data);
				}
			}
			if(isset($_REQUEST['seller_badges_data']['pre_criteria']) && !empty($_REQUEST['seller_badges_data']['pre_criteria']))
			{
				foreach ($_REQUEST['seller_badges_data']['pre_criteria'] as $key => $pre_criteria) {
					$data = array(
					'condition' => $_REQUEST['seller_badges_data']['pre_condition'][$key],
					'criteria' => $_REQUEST['seller_badges_data']['pre_criteria'][$key],
					'amount' => $_REQUEST['seller_badges_data']['pre_amount'][$key]
						);
				db_query('UPDATE ?:seller_badges_conditions SET ?u WHERE id = ?i ' , $data , $key);
				}
			}				
			return array(CONTROLLER_STATUS_OK, "seller_badges.update?id=$badge_id");
			}

		}
	}
}
if ($mode == 'update') {
	if(empty($_REQUEST['id']))
		return array(CONTROLLER_STATUS_REDIRECT, 'seller_badges.manage');
	$seller_badges_data = fn_get_badge_data($_REQUEST['id']);
	//to assure there is atleast one condition
	/*if(!isset($seller_badges_data['conditions']) && ($seller_badges_data['conditions'][0]['criteria'] == NULL))
		$conditions = 0;
	else
		$conditions = 1;*/
	Registry::get('view')->assign('seller_badges_data' , $seller_badges_data);
	// Registry::get('view')->assign('conditions' , $conditions);
}
if ($mode == 'add' || $mode == 'update') {
	$tabs['detailed'] = array (
		'title' => __('general'),
		'href' => "seller_badges.add?selected_section=details",
		'js' => true
	);
	$tabs['select_vendor'] = array (
		'title' => __('select_vendor'),
		'href' => "seller_badges.add?selected_section=select_vendor",
		'js' => true
	);
	$tabs['badges_condition'] = array (
		'title' => __('badges_condition'),
		'href' => "seller_badges.add?selected_section=badges_condition",
		'js' => true
	);
	if($mode == 'update')
	{
		$tabs['auto_assign'] = array (
		'title' => __('auto_assign'),
		'href' => "seller_badges.add?selected_section=auto_assign",
		'js' => true
		);
	}
	Registry::set('navigation.tabs', $tabs);
}

if ($mode == 'delete') {
	$badge_id = $_REQUEST['id'];
	db_query("DELETE FROM ?:seller_badges WHERE id = ?i", $badge_id );
	db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i", $badge_id );
	db_query("DELETE FROM ?:seller_badges_conditions WHERE badge_id = ?i", $badge_id );
	fn_set_notification('N', __('notice'), __('badge_have_been_deleted'));
	return array(CONTROLLER_STATUS_REDIRECT, 'seller_badges.manage');
}
if($mode == 'c_delete')
{
	if(!empty($_REQUEST['id']))
	{
		$badge_id = $_REQUEST['badge_id'];
		$id =$_REQUEST['id'];
		db_query("DELETE FROM ?:seller_badges_conditions WHERE id = ?i", $id );
		fn_set_notification('N', __('notice'), __('condition_has_been_deleted'));
		return array(CONTROLLER_STATUS_OK, "seller_badges.update?id=$badge_id");
	}
}
if ($mode == 'm_delete') {

	if (isset($_REQUEST['badges_ids']) && !empty($_REQUEST['badges_ids'])) {
		$badges_id = $_REQUEST['badges_ids'];
		foreach ($badges_id as $index => $badge_id) {
			db_query("DELETE FROM ?:seller_badges WHERE id = ?i", $badge_id );
			db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i", $badge_id );
			db_query("DELETE FROM ?:seller_badges_conditions WHERE badge_id = ?i", $badge_id );	
		}
		fn_set_notification('N', __('notice'), __('badge_have_been_deleted'));
		return array(CONTROLLER_STATUS_REDIRECT, $_REQUEST['redirect_url']);
	}
}
if($mode == 'preview_html_upload_request')
{
	fn_trusted_vars('wk_banners_badges');
    $wk_banners_badges_settings_data=$_REQUEST['wk_banners_badges'];
    $body = $wk_banners_badges_settings_data['upload_mail_text'];
    Registry::get('view')->assign('body', $body);
    Registry::get('view')->display('addons/banners_badges/views/components/preview_popup.tpl');
    exit();
}
if($mode == 'add')
{
	if(isset($_SESSION['saved_post_data']['seller_badges_data']))
		$seller_badges_post_data = fn_restore_post_data('seller_badges_data');
	if(!empty($seller_badges_post_data))
	{
		$seller_badges_post_data['total_seller'] = $seller_badges_post_data['no_of_seller'];
		$seller_badges_post_data['desc'] = $seller_badges_post_data['badge_description'];
		Registry::get('view')->assign('seller_badges_data' , $seller_badges_post_data);

	}
	
}
