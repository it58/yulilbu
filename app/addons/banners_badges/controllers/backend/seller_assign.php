<?php
/******************************************************************
# Seller Badges--- Seller Badges                                  *
# ----------------------------------------------------------------*
# author Webkul                                                   *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/ 

use Tygh\Embedded;
use Tygh\Http;
use Tygh\Mailer;
use Tygh\Pdf;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Session;
use Tygh\Settings;
use Tygh\Shippings\Shippings;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'manage') {

	$params = $_REQUEST;
	$condition = '';
	$limit = '';
	$order = '';

	if (isset($_REQUEST['sort_order'])) {
		if($_REQUEST['sort_order'] == 'asc'){
			$params['sort_order_rev'] = 'desc';
			$params['sort_order'] = $_REQUEST['sort_order'];
		}else{
			$params['sort_order_rev'] = 'asc';
			$params['sort_order'] = $_REQUEST['sort_order'];
		}
	}else{
		$params['sort_order_rev'] = 'asc';
		$params['sort_order'] = 'desc';
	}
	if (isset($params['is_search']) && $params['is_search'] == "Y") {
		if (isset($params['badge_name']) && !empty($params['badge_name'])) {
			$condition_badge_id = db_quote(" AND badge_name LIKE ?l", "%{$params['badge_name']}%");
			$badges_assign_id = db_get_fields("SELECT id FROM ?:seller_badges WHERE 1 $condition_badge_id");
			$condition .= db_quote(" AND badge_id IN (?n)",$badges_assign_id);
		}
		if (isset($params['company_name']) && !empty($params['company_name'])) {
			$condition_company_id = db_quote(" AND company LIKE ?l", "%{$params['company_name']}%");
			$company_id = db_get_fields("SELECT company_id FROM ?:companies WHERE 1 $condition_company_id");
			$condition .= db_quote(" AND company_id IN (?n)",$company_id);
		}
		
	}
	if (empty($params['items_per_page'])) {
		$params['items_per_page'] = Registry::get('settings.Appearance.admin_elements_per_page');
	}
	if(empty($params['page'])) {
		$params['page'] = 1;
	}

	$params['total_items'] = db_get_field("SELECT count(*) FROM ?:seller_badges_assignment WHERE 1 $condition $order $limit");
	if (!empty($params['limit'])) {
		$limit = db_quote(" LIMIT 0, ?i", $params['limit']);
	} elseif (!empty($params['items_per_page'])) {
		$limit = db_paginate($params['page'], $params['items_per_page'] , $params['total_items']);
	}
	$badges_assign_list = db_get_array("SELECT * FROM ?:seller_badges_assignment WHERE 1 $condition $order $limit");	
	foreach ($badges_assign_list as $key => $badge_assign_data) {
		$res = fn_get_image_pairs($badge_assign_data['badge_id'], 'seller_badges', 'M', true, false);
		$badges_assign_list[$key]['main_pair'] = $res;
		$company_name = db_get_field("SELECT company FROM ?:companies WHERE company_id=?i",$badge_assign_data['company_id']);
		$badges_assign_list[$key]['company_name'] = $company_name;	
		$badge_name = db_get_field("SELECT badge_name FROM ?:seller_badges WHERE id=?i",$badge_assign_data['badge_id']);
		$badges_assign_list[$key]['badge_name'] = $badge_name;	
	}
	Registry::get('view')->assign('badges_assign_list', $badges_assign_list);

	Registry::get('view')->assign('search', $params);

}

if ($mode == 'delete') {
	$id = $_REQUEST['id'];
	db_query("DELETE FROM ?:seller_badges_assignment WHERE id = ?i", $id );
	fn_set_notification('N', __('notice'), __('assign_badge_removed_from_particular_seller'));
	return array(CONTROLLER_STATUS_REDIRECT, 'seller_assign.manage');
}
if ($mode == 'm_delete') {
		$assignment_ids = $_REQUEST['assignment_ids'];
	if (isset($assignment_ids) && !empty($assignment_ids)) {
		foreach ($assignment_ids as $index => $assignment_id) {
			db_query("DELETE FROM ?:seller_badges_assignment WHERE id = ?i", $assignment_id);		
		fn_set_notification('N', __('notice'), __('assign_badge_removed_from_particular_seller'));
		}
		return array(CONTROLLER_STATUS_REDIRECT, 'seller_assign.manage');

	}
}