<?php

/******************************************************************
# Vendor Badges                        		  					  *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/
use Tygh\Registry;
use Tygh\Settings;
use Tygh\BlockManager\Layout;
use Tygh\Themes\Styles;

if ($mode == 'update' || $mode == 'add') {
   		$pre_tabs =  Registry::get('navigation.tabs');
        $pre_tabs['badges'] = array(
            'title' => __('badges'),
            'js' => true
        );
        Registry::set('navigation.tabs', $pre_tabs);
}