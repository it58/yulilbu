<?php

/******************************************************************
# Vendor Badges                        		  					  *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'picker') {
	$badges = db_get_array("SELECT id,badge_name FROM ?:seller_badges WHERE 1");
	Registry::get('view')->assign('badges', $badges);
	Registry::get('view')->display('addons/banners_badges/pickers/assign_badge/picker_contents.tpl');
	exit;
}


if ($mode == 'add' || $mode == 'update') {
	$tabs['detailed'] = array (
		'title' => __('general'),
		'js' => true
	);
	Registry::set('navigation.tabs', $tabs);
}

if ($mode == 'add') {
   $existing_badge_ids = "";
   if(isset($_REQUEST['seller_badges_data']) && !empty($_REQUEST['seller_badges_data']))
  {
	$company_id = $_REQUEST['product_data']['company_id'];
		$err = "";
	if(isset($_REQUEST['seller_badges_data']['badge_id']) && !empty($_REQUEST['seller_badges_data']['badge_id']))
		$badge_ids = $_REQUEST['seller_badges_data']['badge_id'];
	else
		{
			fn_set_notification('E', __('error'), __('please_select_some_badges_in_order_to_assign_to_seller'));
			return array(CONTROLLER_STATUS_OK, "assign_badge.add"); 
		}	
		foreach ($badge_ids as $key => $badge_id) {
			$total_possible_seller = db_get_field("SELECT total_seller FROM ?:seller_badges WHERE id = ?i",$badge_id);
		$total_assigned_seller = db_get_field("SELECT count(id) FROM ?:seller_badges_assignment WHERE badge_id = ?i",$badge_id);
		$assigned_sellers_list = db_get_fields("SELECT company_id FROM ?:seller_badges_assignment WHERE badge_id = ?i",$badge_id);
		if(in_array($company_id,  $assigned_sellers_list))
		{
		  $badge_name = db_get_field("SELECT badge_name FROM ?:seller_badges WHERE id = ?i",$badge_id);
		  $existing_badge_ids .= " ".$badge_name;
		  if(($key = array_search($badge_id, $badge_ids)) !== false) 
			unset($badge_ids[$key]);
		}
		if(($total_assigned_seller == $total_possible_seller) || $total_assigned_seller >= $total_possible_seller) {
				$badge_name = db_get_field("SELECT badge_name FROM ?:seller_badges WHERE id = ?i",$badge_id);
				$err .= $badge_name .",";	
			}
		}
	if ($existing_badge_ids != NULL) {
		fn_set_notification('W', __('warning'), __('wk_following_badges_are_already_assigned').' '.$existing_badge_ids);
	}
	if(!empty($err))
	{
		fn_set_notification('E', __('error'), __('wk_badges_can_not_be_assigned_as_they_reached_their_maximum_limit').' '.$err);
		return array(CONTROLLER_STATUS_OK, "assign_badge.add"); 

	}else
	{
		$badge_seller_can_have = fn_banners_badges_get_setting_seller_badge();
		$is_already_have_badge = db_get_field("SELECT count(id) FROM ?:seller_badges_assignment WHERE company_id = ?i",$company_id);
		if (($badge_seller_can_have == 'single') && $is_already_have_badge) 
		{
			fn_set_notification('E', __('error'), __('onle_one_badge_can_be_assigned_to_a_seller'));
			return array(CONTROLLER_STATUS_OK, "assign_badge.add");
		}else
		{
		if(!empty($badge_ids))
		  {
			foreach ($badge_ids as $key => $badge_id) {
			$data = array(
			  'badge_id' => $badge_id,
			  'company_id' => $company_id,
			  'assign_time' => time(),
			  'manual_assign' => 1
			  );
			db_query('INSERT INTO ?:seller_badges_assignment ?e', $data);
			fn_badges_notify_to_vendor($company_id,$badge_id,time());
			  }
			fn_set_notification('N',__('notice'),__('badges_have_been_assigned_successfully'));
			  return array(CONTROLLER_STATUS_OK, "companies.update?company_id=$company_id");
		  }else
			return array(CONTROLLER_STATUS_OK, "assign_badge.add");
		}
 

	}
  }

}
