<?php
/******************************************************************
# Vendor Badges                        		  *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/
use Tygh\Registry;
use Tygh\Settings;
use Tygh\BlockManager\Block;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
/**
 * function to get url related to vendor banner
 *@param int $company_id  company_id
 *@return string $url    url
 */
function fn_get_banner_url($company_id)
{
	   if (empty($company_id)) {
        return false;
    }
    $url = db_get_field('SELECT url FROM ?:seller_banners WHERE company_id = ?i' , $company_id);
    return $url; 
}
/**
 * function to get badge data
 *@param int $id  badge_id
 *@return array $seller_badges_data   
 */
function fn_get_badge_data($id)
{
	if(!empty($id) && isset($id))
	{
		$seller_badges_data = db_get_array('SELECT * FROM ?:seller_badges WHERE id = ?i' , $id);
		$seller_badges_data[0]['auto']['company_id'] = db_get_fields('SELECT company_id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i', $id,0);
		$seller_badges_data[0]['manual']['company_id'] = db_get_fields('SELECT company_id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i' , $id,1);
		$seller_badges_data[0]['conditions'] = db_get_array('SELECT * FROM ?:seller_badges_conditions WHERE badge_id = ?i' , $id);
		$seller_badges_data[0]['condition_count'] = sizeof($seller_badges_data[0]['conditions']);
		$seller_badges_data[0]['main_pair'] = fn_get_image_pairs($id, 'seller_badges', 'M', true, false);
		return $seller_badges_data[0];
	}
}
/**
 * function to get badge data
 *@param int $badge_id  badge_id
 *@return string $badge_name name_of_badge   
 */
function fn_get_badge_name($badge_id){
	$badge_name = db_get_array('SELECT badge_name FROM ?:seller_badges WHERE id = ?i' , $badge_id);
	return $badge_name;
}
/**
 * function to get badge data
 *@return array $cache add-on setting data   
 */
function fn_banners_badges_get_setting_data($company_id = NULL)
{

	static $cache;
  if (empty($cache['settings_' . $company_id])) {
    $settings = Settings::instance()->getValue('badges_sales_data', '', $company_id);
    $settings = unserialize($settings);
    if (empty($settings)) {
      $settings = array();
    }
    $cache['settings_' . $company_id] = $settings;
  }
  return $cache['settings_' . $company_id];
}
/**
 * function to get badge data
 *@return array $cache add-on setting data for mail templating 
 */
function fn_banners_badges_get_mail_setting_data($company_id = NULL)
{

	static $cache;
  if (empty($cache['settings_' . $company_id])) {
    $settings = Settings::instance()->getValue('badges_mail_data', '', $company_id);
    $settings = unserialize($settings);
    if (empty($settings)) {
      $settings = array();
    }
    $cache['settings_' . $company_id] = $settings;
  }
  return $cache['settings_' . $company_id];
}
/**
 * function to get badge data
 *@return array $badges list of all badges which are currently active are auto enable
 */
function fn_banners_badges_get_badges()
{
	$badges = db_get_array("SELECT * FROM ?:seller_badges WHERE auto_enable = 'Y' AND status = 'A'");
	return $badges;
}
/**
 * function to get badge data
 *@return int $company_id company_id
 *@return array $items list of all badges assigned to paarticular vendor
 */
function fn_banners_badges_get_seller_badges($company_id)
{
	$items = array();
	$no_of_badges_to_vendor = Registry::get('addons.banners_badges.seller_badge');
	if($no_of_badges_to_vendor == 'multiple')
	{
		$assigned_badges = db_get_fields("SELECT badge_id FROM ?:seller_badges_assignment WHERE company_id = ?i",$company_id);
		foreach ($assigned_badges as $index => $badge_id) {
			$badge_data = db_get_array("SELECT * FROM ?:seller_badges WHERE id = ?i",$badge_id);
			if($badge_data[0]['status'] == 'A')
			{
				$main_pair_data = fn_get_image_pairs($badge_id, 'seller_badges', 'M', true, false);
				if(empty($main_pair_data))
					continue;
				$items[$index]['badge_id'] = $badge_id;
				$items[$index]['url'] = $badge_data[0]['url'];
				$items[$index]['name'] = $badge_data[0]['badge_name'];
				$items[$index]['main_pair'] = $main_pair_data;
			}
		}
		return $items;
	}else{
		$badge_id = db_get_field("SELECT badge_id FROM ?:seller_badges_assignment WHERE company_id = ?i ORDER BY assign_time DESC",$company_id);
		$badge_data = db_get_array("SELECT * FROM ?:seller_badges WHERE id = ?i",$badge_id);
		if($badge_data[0]['status'] == 'A')
		{
			$main_pair_data = fn_get_image_pairs($badge_id, 'seller_badges', 'M', true, false);
			if(!empty($main_pair_data)){
				$items[$index]['main_pair'] = $main_pair_data;
				$items[$index]['url'] = $badge_data[0]['url'];
				$items[$index]['name'] = $badge_data[0]['badge_name'];
			}
		}
		return $items;
	}
	return $items;
}
/**
 * hook to update banner detail of particular vendor 
 */
//commented code is for vendor default logo functionality 
function fn_banners_badges_update_company(&$company_data, &$company_id, $lang_code, $can_update)
{
	/*$use_default_logo_setting = Registry::get("addons.banners_badges.use_default_logo");
	if(isset($_REQUEST['file_logotypes_image_icon']['theme']) && !empty($_REQUEST['file_logotypes_image_icon']['theme']))
	{
		$_SESSION['wk_logo_available'] = 'Y';
	}else
		{
			if($use_default_logo_setting == 'Y')
			{
				$_REQUEST['file_logotypes_image_icon']['theme'] = "C:\fakepath\a.jpg";
				$_REQUEST['type_logotypes_image_icon']['theme'] ="local";
				$_SESSION['wk_logo_available'] = 'N';

			}else
				$_SESSION['wk_logo_available'] = 'Y';
		}*/
			
	if(isset($_REQUEST['file_banners_badges_main_image_icon']) && !empty($_REQUEST['file_banners_badges_main_image_icon']))
	{
		if($company_id)
		{
			fn_attach_image_pairs('banners_badges_main', 'vendor_banner', $company_id, $lang_code = DESCR_SL);
			$data = array(
			'company_id' => $company_id,
			'url' => $_REQUEST['company_data']['banners_badges_url'],
			);
			db_query("DELETE FROM ?:seller_banners WHERE company_id = ?i", $company_id);
			db_query('INSERT INTO ?:seller_banners ?e', $data);
		}
	}
}

function fn_banners_badges_get_setting_seller_badge()
{
	$badge_seller_can_have = Registry::get('addons.banners_badges.seller_badge');
	return $badge_seller_can_have;
}
/**
 * function to assign badges automatically on the bases of their condition
 */
function fn_banners_badges_auto_assign()
{	
if($_REQUEST['dispatch'] == 'seller_badges.update'){
	$badge_seller_can_have = Registry::get('addons.banners_badges.seller_badge');
	$sales_period = fn_banners_badges_get_setting_data();
	if(isset($sales_period) && !empty($sales_period))
	{
	$start_date = fn_parse_date($sales_period['start_date']);
	$end_date = fn_parse_date($sales_period['end_date']);
	}else
		return false;
	$vendor_pdt_review_post_ids = array();
	$vendor_pdt_review = array();
	$vendor_review_post_ids = array();
	$vendor_review = array();
	$vendor = array();
	$orders_count = array();
	$products_count =array();
	$sales_amount = array();
	$possible_assignment_of_seller = array();
	$total_badge_of_seller = array();

	list($vendor, $search) = fn_get_companies($_REQUEST, $auth );

	foreach ($vendor as $index => $vendor_data) {
		$vendor_id = $vendor_data['company_id'];
		// count order of each vendor
		$orders_count[$vendor_id] = count(db_get_fields("SELECT order_id FROM ?:orders WHERE company_id = ?i",$vendor_id));
		//count product of each vendor
		$vendor_product_ids = db_get_fields("SELECT product_id FROM ?:products WHERE company_id = ?i",$vendor_id);
		$products_count[$vendor_id] = count($vendor_product_ids);
		//calculate vendor review
		$vendor_review_thread = db_get_field("SELECT thread_id FROM ?:discussion WHERE object_id = ?i AND object_type = 'M' ",$vendor_id);
		$vendor_review_post_ids[$vendor_id] = db_get_fields("SELECT post_id FROM ?:discussion_posts WHERE thread_id = ?i ",$vendor_review_thread);
		$count = 0;
		foreach ($vendor_review_post_ids[$vendor_id] as $key => $value) {
			$msg = db_get_array("SELECT message FROM ?:discussion_messages WHERE post_id = ?i",$value);
			if (!empty($msg) && ($msg != NULL) ) 
				$count++;
		}
		$vendor_review[$vendor_id] = $count;
	//calculate vendor product review ($vendor_pdt_review)
		if(isset($vendor_product_ids) && !empty($vendor_product_ids))
		{
			$vendor_pdt_review_thread = db_get_fields("SELECT thread_id FROM ?:discussion WHERE object_type = 'P' AND object_id IN (?a) ",$vendor_product_ids);
			foreach ($vendor_pdt_review_thread as $key => $thread_id) {
				$vendor_pdt_review_post_ids[$vendor_id][] = db_get_fields("SELECT post_id FROM ?:discussion_posts WHERE thread_id = ?i ",$thread_id);
			}
			$count = 0;
			if (isset($vendor_pdt_review_post_ids[$vendor_id]) && !empty($vendor_pdt_review_post_ids[$vendor_id])) {
				foreach ($vendor_pdt_review_post_ids[$vendor_id] as $index => $post_ids) {
				foreach ($post_ids as $key => $value) {
					$msg = db_get_array("SELECT message FROM ?:discussion_messages WHERE post_id = ?i",$value);
					if (!empty($msg) && ($msg != NULL) ) 
						$count++;
				}
				}
			}
			$vendor_pdt_review[$vendor_id] = $count;
		}else
			$vendor_pdt_review[$vendor_id] = 0;

	//calculate sales amount each vendor
			$order_amount = 0.00;
 		$sales_amount[$vendor_id] = abs(db_get_field("SELECT SUM(payout_amount) FROM ?:vendor_payouts WHERE start_date >= ?i AND end_date <= ?i AND company_id = ?i AND order_amount = ?d",$start_date,$end_date,$vendor_id,$order_amount));
	//no of badge a seller have
		$total_badge_of_seller[$vendor_id] =  db_get_field("SELECT count(badge_id) FROM ?:seller_badges_assignment WHERE company_id = ?i ",$vendor_id);
	}
// find badges data
	$conditions = array();
	$badges = fn_banners_badges_get_badges();
	$no_of_already_assigned_seller = array();

	foreach ($badges as $key => $badge_data) {
		$badge_id = $badge_data['id'];
	// a badge already assigned to how much seller
		$no_of_already_assigned_seller[$badge_id] = db_get_field("SELECT count(company_id) FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = 1",$badge_id);
	//no of seller upto we can assign that badge 		
		$possible_assignment_of_seller[$badge_id] = $badge_data['total_seller'] - $no_of_already_assigned_seller[$badge_id];
				// conditions of each badge
		$conditions[$badge_id] = db_get_array("SELECT * FROM ?:seller_badges_conditions WHERE badge_id = ?i",$badge_id);
	}
	$elligible_sellers = array();
	foreach ($conditions as $badge_id => $condition_array) 
	{	
		$elligible_sellers = array();
		$lists = array();
		$final_elligible_sellers = array();
		$cond_count = count($condition_array);
		for($i= 0; $i < $cond_count;$i++) {
			$condition = $condition_array[$i];
		// To fetch in which table we have to lukng for sellers
			switch ($condition['criteria']) {
				case 'O':
					$table = $orders_count;
					break;
				case 'S':
					$table = $sales_amount;
					break;
				case 'P':
					$table = $products_count;
						break;
				case 'VR':
					$table = $vendor_review;
					break;
				case 'VPR':
					$table = $vendor_pdt_review;
					break;
				default:
					$table = NULL;
					break;
			}
//to find elligible seller accr to 1st condition and so on
			if ($condition['condition'] == 'EQ') {
				foreach ($table as $company_id => $value) {
					if($value == $condition['amount'])
						$elligible_sellers[$i][] = $company_id;
				}
			}else if($condition['condition'] == 'LTE')
			{
				foreach ($table as $company_id => $value) {
					if($value <= $condition['amount'])
						$elligible_sellers[$i][] = $company_id;
				}
			}else if($condition['condition'] == 'GTE')	
			{
				foreach ($table as $company_id => $value) {
					if($value >= $condition['amount'])
						$elligible_sellers[$i][] = $company_id;
				}
			}else if($condition['condition'] == 'LT')
			{
				foreach ($table as $company_id => $value) {
					if($value < $condition['amount'])
						$elligible_sellers[$i][] = $company_id;
				}
			}else if($condition['condition'] == 'GT')
			{
				foreach ($table as $company_id => $value) {
					if($value > $condition['amount'])
						$elligible_sellers[$i][] = $company_id;
				}
			}
		}
		$elligible_sellers_array_count = count($elligible_sellers);
// if below condition true means there is one condition which not follows by any of vendor so not assign any one
		if($elligible_sellers_array_count != $cond_count)
			continue;
// if there is only one condition no need to intersect
		if (($elligible_sellers_array_count == $cond_count) && ($elligible_sellers_array_count == 1)) {
			$final_elligible_sellers = $elligible_sellers[0];
		}
		else
		{
			for($i= 0; $i < $cond_count;$i++) {
			$lists[] = $elligible_sellers[$i];
			}	
		// take intersection of eligible's seller array to find who fullfill all condition 
			if(!empty($lists)){
				$intersection_of_sellers = call_user_func_array('array_intersect',$lists);
				foreach ($intersection_of_sellers as $key => $value) {
					$final_elligible_sellers[] = $value;
				}
			}
		}
		//check global setting before assignment		
		if($badge_seller_can_have == 'single')
		{
			foreach ($final_elligible_sellers as $index => $company_id) 
			{
				$is_already_have_badge = db_get_field("SELECT count(id) FROM ?:seller_badges_assignment WHERE company_id = ?i",$company_id);
				if($is_already_have_badge)
				{

				}else
					{
					$data = array(
						'badge_id' => $badge_id,
						'company_id' => $company_id,
						'assign_time' => time(),
						'manual_assign' => 0
							);
					db_query('INSERT INTO ?:seller_badges_assignment ?e', $data);
					fn_badges_notify_to_vendor($company_id,$badge_id,time());
					}
			}
		}else{
			if($possible_assignment_of_seller[$badge_id] < count($final_elligible_sellers))
			{
				foreach ($final_elligible_sellers as $index => $company_id) 
					{
					$new_table[$company_id] = $table[$company_id];
					}
				arsort($new_table);
				$new_table = fn_check_is_already_exist($new_table,$badge_id);
				if(!empty($new_table))
				{
					$elligible_sellers_with_values = array_slice($new_table,0,$possible_assignment_of_seller[$badge_id],true);
					if(!empty($elligible_sellers_with_values))
					foreach ($elligible_sellers_with_values as $company_id => $value) 
					{
						$is_already_assigned = db_get_field("SELECT id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND company_id = ?i",$badge_id, $company_id);
						if(!$is_already_assigned)
						{
						$data = array(
							'badge_id' => $badge_id,
							'company_id' => $company_id,
							'assign_time' => time(),
							'manual_assign' => 0
								);
						db_query('INSERT INTO ?:seller_badges_assignment ?e', $data);
						fn_badges_notify_to_vendor($company_id,$badge_id,time());
						}
					}
				}
			}else
			{
				// fn_print_r($final_elligible_sellers);
				foreach ($final_elligible_sellers as $index => $company_id) 
				{
					$is_already_assigned = db_get_field("SELECT id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND company_id = ?i",$badge_id, $company_id);
					if(!$is_already_assigned)
					{
						$data = array(
						'badge_id' => $badge_id,
						'company_id' => $company_id,
						'assign_time' => time(),
						'manual_assign' => 0
							);
					db_query('INSERT INTO ?:seller_badges_assignment ?e', $data);
					fn_badges_notify_to_vendor($company_id,$badge_id,time());
					}
				}
			}
		}
	}
}
fn_banners_badges_clear_cache();
}
/**
 * function to get default logo data required only when default vendor logo *functionality is enable 
 */
function fn_get_wk_vendor_default_logo_data()
{
	$set_as_default = Registry::get('addons.banners_badges.use_default_logo');
	$default_vendor_logo_data = fn_get_image_pairs(0, 'default_vendor_logo', 'M', false, true);
	$data = array(
		'set_as_default' => $set_as_default,
		'default_vendor_logo_data' => $default_vendor_logo_data
		);
	return $data;
}

/**
 * hook used to upload default vendor logo from addon setting data .
 */

/*function fn_banners_badges_update_image(&$image_data,$image_id, $image_type, $images_path, $_data, $mime_type, $is_clone)
{
	
	if(isset($_REQUEST['company_data']) && !empty($_REQUEST['company_data']) && (isset($_SESSION['wk_logo_available']) && $_SESSION['wk_logo_available'] == 'N'))
	{
		
		$default_vendor_logo_data = fn_get_image_pairs(0, 'default_vendor_logo', 'M', false, true);
		$wk_default_image_name = explode('/', $default_vendor_logo_data['detailed']['absolute_path']);
		$default_image_name = end($wk_default_image_name);
		array_pop($wk_default_image_name);
		$name_array = explode('.', $default_image_name);
		$default_image_ext = end($name_array);
		array_pop($name_array);
		$name_array = implode('.', $name_array);
		$new_default_image_name = $name_array.'_'.rand().$default_image_ext;
		$default_image_path = implode('/', $wk_default_image_name);
		copy($default_image_path.'/'.$default_image_name,$default_image_path.'/'.$new_default_image_name);
		$image_data['name'] = $new_default_image_name;
		$image_data['path'] = $default_image_path.'/'.$new_default_image_name;
		unset($_SESSION['wk_logo_available']);
	}
}*/

/**
 * function to notify vendor after badge assignment 
 *@param int $company_id company_id
 *@param int $badge_id badge_id
 *@param int $assign_time assign_time
 */
function fn_badges_notify_to_vendor($company_id,$badge_id,$assign_time)
{
	$send_to = db_get_row("SELECT company,email FROM ?:companies WHERE company_id = ?i",$company_id);
	if(isset($send_to['company']) && !empty($send_to['company']))
		$vendor_name = $send_to['company'];
	else
		$vendor_name = "__('vendor')";
	if (isset($send_to['email']) && !empty($send_to['email']))
		$to = $send_to['email'];
	else
	{
		fn_set_notification('E',__('error'),__('email_of_vendor_not_found_so_cant_send_mail'));
		return false;
	}
	$wk_banners_badges_mail_settings_data = fn_banners_badges_get_mail_setting_data();
	if (empty($wk_banners_badges_mail_settings_data)) {
		fn_set_notification('E',__('error'),__('email_template_setting_not_available_cant_notify_vendor'));
		return false;
	}
	$tpl_file = 'addons/banners_badges/wk_congrates_vendor.tpl';
	$mail_content = $wk_banners_badges_mail_settings_data['upload_mail_text'];
	$mail_content = fn_banners_badges_replace_placeholders($mail_content,$badge_id,$vendor_name,$assign_time);
	fn_banners_badges_send_mail($mail_content,$to,$wk_banners_badges_mail_settings_data['upload_mail_sub'],$tpl_file);
}
/**
 * function to replace placeholder of mail text 
 *@param string $mail_text text set in addon setting sub
 *@param int $badge_id badge_id
 *@param string $vendor_name vendor name
 *@param string $assign_time Time when badge assigned
 *@return string $mail_content content of mail subject
 */
function fn_banners_badges_replace_placeholders($mail_text,$badge_id,$vendor_name,$assign_time)
{
	$badge_name = db_get_field("SELECT badge_name FROM ?:seller_badges WHERE id = ?i",$badge_id);
	$time = date('m/d/Y', $assign_time);
	$mail_text = str_replace('[#vendor_name#]', $vendor_name, $mail_text);
	$mail_text = str_replace('[#badge_name#]', $badge_name, $mail_text);
	$mail_content = str_replace('[#assign_time#]', $time, $mail_text); 
	return $mail_content;
}
/**
 * function to send mail
 *@param string $mail_content text send in mail
 *@param string $to vendor email to whom send
 *@param string $subject subject of mail 
 *@param string $tpl_file TPl file associated
 */
function fn_banners_badges_send_mail($mail_content,$to,$subject,$tpl_file)
{
   Mailer::sendMail(array(
      'to' => $to,
      'from' =>'default_company_support_department',
      'data' => array(
          'mail_content' => $mail_content,
          'subject'=>$subject
          ),
      'tpl' =>$tpl_file
      ));
}
/**
 * hook to retrieve all badges of particular company to show on product detail page 
 */
function fn_banners_badges_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
{
	$product_data['badges'] = fn_banners_badges_get_seller_badges($product_data['company_id']);
}

function fn_badges_check_already_assigned(&$company_ids,$badge_id)
{
	if(empty($company_ids))
		db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i",$badge_id,1);
	else 
	{
		$already_auto_assigned = db_get_fields("SELECT company_id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i",$badge_id,0);
		if(!empty($already_auto_assigned))
		{
			foreach ($already_auto_assigned as $key => $company_id) {
				if(in_array($company_id, $company_ids))
					{
						$set_warning = 1;
						unset($company_ids[$key]);
					}

			}
			if(isset($set_warning) && $set_warning)
				fn_set_notification('W', __('warning'), __('can_not_reassign_badge'));
		}
		$already_assigned = db_get_fields("SELECT company_id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i",$badge_id,1);
		if(!empty($already_assigned))
		{
			foreach ($already_assigned as $key => $company_id) {
				if(!in_array($company_id, $company_ids))
				{
					$data = array(
						'badge_id' => $badge_id,
						'company_id' => $company_id,
						'manual_assign' => 1	
						);
					db_query("DELETE FROM ?:seller_badges_assignment WHERE ?w",$data);
				}
			}
			foreach ($company_ids as $key => $company_id) {
				if(in_array($company_id, $already_assigned))
					unset($company_ids[$key]);
			}
		}
	}
}
function fn_banners_badges_clear_cache()
{
	$wk_dir_path=Registry::get('config.dir.cache_registry');
	$cached_files=scandir ($wk_dir_path);
	foreach ($cached_files as $key => $file_name) {
		if (strpos($file_name,'block_content_11') !== false) {
			$files_path = $wk_dir_path.$file_name.'/';
			$files = glob($files_path . '*', GLOB_MARK); 
			    foreach ($files as $file) {
			        unlink($file);
			    }
			rmdir($wk_dir_path.$file_name);
		}
	}
	$wk_dir_path=Registry::get('config.dir.cache_templates');// It return directory path of template cache
	$current_theme = fn_get_theme_path('[theme]', 'C');
	$wk_dir_path=$wk_dir_path.$current_theme; // Suppose we use responsive theme
	clearstatcache();
	if(is_dir($wk_dir_path))
	$cached_files=scandir ($wk_dir_path); // retrieve all cached file 
	clearstatcache();
	if(!empty($cached_files))
	foreach ($cached_files as $key => $file_name) {
		if (strpos($file_name,'product') !== false) {
		unlink($wk_dir_path."/".$file_name);
		}
		if (strpos($file_name,'promo_text.post.tpl.php') !== false) {
		unlink($wk_dir_path."/".$file_name);
		}
		
	}
	
}
function fn_check_changes_in_badge_data($request_data,$badge_id)
{
	$change = 0;
	if($request_data['criteria'][1])
	{
		db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i",$badge_id,0);
	}

	if(isset($badge_id) && !empty($badge_id))
	{
		$badge_data_pre = db_get_row("SELECT * FROM ?:seller_badges WHERE id = ?i",$badge_id);

		if(!empty($badge_data_pre))
		{
			if(($badge_data_pre['priority'] != $request_data['priority']) || ($badge_data_pre['total_seller'] != $request_data['no_of_seller']))
				$change = 1;
			if(!$change)
			{
				$pre_conditions = array();
				if(isset($request_data['pre_condition']))
				{
					foreach ($request_data['pre_condition'] as $key => $condition) {
						$data = array (
							'badge_id' => $badge_id,
						    'condition' => $condition,
						    'criteria' => $request_data['pre_criteria'][$key],
						    'amount' => $request_data['pre_amount'][$key]
						);

						$condition_id = db_get_field("SELECT id FROM ?:seller_badges_conditions WHERE ?w",$data);
						if(empty($condition_id))
							$change = 1;

					}
				}
			}
		}
	}
	if($change)
	{
		db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i",$badge_id,0);
	}
}
function fn_check_badge_auto_enable($auto_enable,$badge_id)
{
	$pre_auto_enable = db_get_field("SELECT auto_enable FROM ?:seller_badges WHERE id = ?i",$badge_id);
	if($pre_auto_enable == 'Y' && $auto_enable == 'N')
		db_query("DELETE FROM ?:seller_badges_assignment WHERE badge_id = ?i AND manual_assign = ?i",$badge_id,0);
}
function fn_check_is_already_exist($new_table,$badge_id)
{
	if(!empty($new_table))
	{
		foreach ($new_table as $company_id => $value) {
			$is_already_assigned = db_get_field("SELECT id FROM ?:seller_badges_assignment WHERE badge_id = ?i AND company_id = ?i",$badge_id, $company_id);
			if(isset($is_already_assigned) && !empty($is_already_assigned))
				unset($new_table[$company_id]);	
		}
		return $new_table;
	}
}
