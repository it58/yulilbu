<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_define('SD_PUSHER_CHANNELS_PUBLIC', 'public');
fn_define('SD_PUSHER_CHANNELS_PRIVATE', 'private-notify');
fn_define('SD_PUSHER_CHANNELS_PRODUCT_PRESENCE', 'presence-product');
fn_define('SD_PUSHER_CHANNELS_ORDER_PRESENCE', 'presence-order');
fn_define('SD_PUSHER_CHANNELS_ORDER_EDIT_PRESENCE', 'presence-order-edit');
fn_define('SD_PUSHER_CHANNELS_CUSTOMER_PRESENCE', 'presence-customer');
fn_define('SD_PUSHER_CHANNELS_CATEGORY_PRESENCE', 'presence-category');

fn_define('SD_PUSHER_DEFAULT_NOTIFICATION_TYPE', "I");
fn_define('SD_PUSHER_ENCRYPTED', true);

fn_define('SD_PUSHER_EVENT_NOTIFICATION', 'notification');

fn_define('SD_PUSHER_TYPES_NEW_ORDER', 'sd_pusher.types.new_order');
fn_define('SD_PUSHER_TYPES_NEW_REVIEW', 'sd_pusher.types.new_review');
fn_define('SD_PUSHER_TYPES_NEW_MESSAGE', 'sd_pusher.types.new_message');
