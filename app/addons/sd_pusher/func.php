<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

require_once('lib/Pusher.php');

use Tygh\Registry;
use Tygh\Pusher\Pusher;

function fn_sd_pusher_check_channel($token, $channel_name)
{
    $result = false;
    $channel_data = explode('_', $channel_name);
    $channel_type = array_shift($channel_data);

    if ($channel_type == SD_PUSHER_CHANNELS_PUBLIC) {
        $result = $channel_name;
    } else {
        $user_id = fn_sd_pusher_decrypt($token);

        if (is_numeric(trim($user_id))) {
            $channel_id = array_pop($channel_data);

            if (is_numeric(trim($channel_id))) {
                if (intval($channel_id) == intval($user_id)) {
                    $result = $channel_name;
                }
            }
        }
    }

    return $result;
}

function fn_sd_pusher_encrypt($string)
{
    return fn_encrypt_text((string) $string);
}

function fn_sd_pusher_decrypt($string)
{
    return fn_decrypt_text((string) $string);
}

function fn_sd_pusher_forbidden(){
    header('', true, 403);
    fn_echo("Forbidden");
}

function fn_sd_pusher_get_presence_data($user_id = array())
{
    $presence_data = array();

    if (!empty($user_id)) {
        $presence_data = db_get_row('SELECT CONCAT(firstname, " ", lastname) as name, company_id FROM ?:users WHERE user_id = ?i', $user_id);
    }

    return $presence_data;
}

function fn_sd_pusher_get_messanger_channel_names()
{
    $channels = Pusher::getChannels();
    $channels = is_array($channels) ? $channels : array();

    return array_keys($channels);
}

function fn_sd_pusher_get_channels_restricted_by_company_id($channels = array(), $company_id = 0)
{
    if (is_array($channels)) {
        $user_ids = array();
        $company_ids = array(0);

        foreach ($channels as $channel) {
            if (strpos($channel, SD_PUSHER_CHANNELS_PRIVATE) !== false) {
                $parts = explode('_', $channel);
                $user_id = (int) end($parts);

                if (!empty($user_id)) {
                    $user_ids[] = $user_id;
                }
            }
        }

        if (is_numeric($company_id) && $company_id != 0) {
            $company_ids[] = $company_id;
        }

        $company_condition = db_quote(' AND company_id IN (?n)', $company_ids);

        $valid_user_ids = db_get_fields(
            'SELECT user_id
            FROM ?:users
            WHERE user_id IN (?n)'
            . $company_condition,
            $user_ids
        );

        $channels = array_reduce($valid_user_ids, function($channels, $user_id) {
            $channels[] = SD_PUSHER_CHANNELS_PRIVATE . '_' . $user_id;
            return $channels;
        });
    }

    return $channels;
}

function fn_sd_pusher_get_order_company_id($order_id)
{
    $company_id = 0;

    if (!empty($order_id)) {
        $company_id = db_get_field('SELECT company_id FROM ?:orders WHERE order_id = ?i', $order_id);
    }

    return $company_id;
}

// HOOKS

function fn_sd_pusher_dispatch_assign_template($controller, $mode, $area, $controllers_cascade)
{
    if (AREA == 'A') {
        $cart = Tygh::$app['session']['cart'];

        $product_presence_event = $controller == "products" && $mode == "update" && !empty($_REQUEST['product_id']);
        $order_presence_event = $controller == "orders" && $mode == "details" && !empty($_REQUEST['order_id']);
        $order_edit_presence_event = $controller == "order_management" && $mode == "update" && !empty($cart['order_id']);
        $customer_presence_event = $controller == "profiles" && $mode == "update" && !empty($_REQUEST['user_id']);
        $cetegory_presence_event = $controller == "categories" && $mode == "update" && !empty($_REQUEST['category_id']);

        $show_admins_online_content = $product_presence_event
            || $order_presence_event
            || $order_edit_presence_event
            || $customer_presence_event
            || $cetegory_presence_event;

        Tygh::$app['view']->assign(array(
            'product_presence_event' => $product_presence_event,
            'order_presence_event' => $order_presence_event,
            'order_edit_presence_event' => $order_edit_presence_event,
            'customer_presence_event' => $customer_presence_event,
            'cetegory_presence_event' => $cetegory_presence_event,
            'show_admins_online_content' => $show_admins_online_content,
        ));
    }
}

function fn_sd_pusher_place_order($order_id, $action, $order_status, $cart, $auth)
{
    if (Registry::get('addons.sd_pusher.enable_new_order_notification') == 'Y') {
        // skip parent order
        if (isset($cart['parent_order_id']) && !$cart['parent_order_id'] && count($cart['product_groups']) > 1) {
            return;
        }

        $channels = fn_sd_pusher_get_messanger_channel_names();
        $oder_company_id = fn_sd_pusher_get_order_company_id($order_id);
        $channels = fn_sd_pusher_get_channels_restricted_by_company_id($channels, $oder_company_id);

        if (!empty($channels) && is_array($channels)) {
            $new_order_notification = array(
                'notification' => array(
                    'type' => 'N',
                    'message' => 'sd_pusher.notify.new_order',
                    'message_state' => 'K',
                    'variables' => array(
                        'url' => "orders.details&order_id=$order_id",
                    ),
                ),
            );

            Pusher::send(SD_PUSHER_TYPES_NEW_ORDER, SD_PUSHER_EVENT_NOTIFICATION, $order_id, $new_order_notification, $channels);
        }
    }
}

function fn_sd_pusher_get_discussion_object_data($data, $object_id, $object_type)
{
    if (AREA == 'C'
        && Registry::get('addons.sd_pusher.enable_new_review_notification') == 'Y'
        && Registry::get('runtime.controller') == 'discussion'
        && Registry::get('runtime.mode') == 'add'
    ) {
        $channels = fn_sd_pusher_get_messanger_channel_names();
        $company_id = 0;
        $switch_company = '';

        if (fn_allowed_for('ULTIMATE')) {
            $company_id = Registry::get('runtime.company_id');
            $switch_company = "&switch_company_id=$company_id";
        }

        $channels = fn_sd_pusher_get_channels_restricted_by_company_id($channels, $company_id);

        if (!empty($channels) && is_array($channels)) {
            $new_order_notification = array(
                'notification' => array(
                    'type' => 'N',
                    'message' => 'sd_pusher.notify.new_review',
                    'message_state' => 'K',
                    'variables' => array(
                        'url' => "discussion_manager.manage&object_type={$object_type}{$switch_company}",
                    ),
                ),
            );

            Pusher::send(SD_PUSHER_TYPES_NEW_REVIEW, SD_PUSHER_EVENT_NOTIFICATION, 0, $new_order_notification, $channels);
        }
    }
}

function fn_sd_pusher_sd_messaging_system_save_new_message($ticket_id, $author_id, $message)
{
    if (AREA == 'C'
        && Registry::get('addons.sd_pusher.enable_new_message_notification') == 'Y'
        && Registry::get('addons.sd_messaging_system.status') == 'A'
        && !empty($ticket_id)
        && !empty($author_id)
    ) {
        $channels = fn_sd_pusher_get_messanger_channel_names();
        $company_id = db_get_field('SELECT company_id FROM ?:messenger_tickets WHERE ticket_id = ?i', $ticket_id);
        $channels = fn_sd_pusher_get_channels_restricted_by_company_id($channels, $company_id);

        if (!empty($channels) && is_array($channels)) {
            $new_message_notification = array(
                'notification' => array(
                    'type' => 'N',
                    'message' => 'sd_pusher.notify.new_message',
                    'message_state' => 'K',
                    'variables' => array(
                        'customer' => fn_get_user_name($author_id),
                        'url' => "messenger.view&ticket_id={$ticket_id}",
                    ),
                ),
            );

            Pusher::send(SD_PUSHER_TYPES_NEW_MESSAGE, SD_PUSHER_EVENT_NOTIFICATION, 0, $new_message_notification, $channels);
        }
    }
}