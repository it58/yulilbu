<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_redirect_to_sitemap_dispatch_before_display()
{
	$status = Registry::get('view')->getTemplateVars('exception_status');
	if ($status == CONTROLLER_STATUS_NO_PAGE) {
		fn_redirect('sitemap.view');
		fn_set_notification('W', fn_get_lang_var('warning'), fn_get_lang_var('no_pages'));
	}
}

?>