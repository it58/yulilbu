<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'add_to_check' && !empty($_REQUEST['order_id'])) {
    if (fn_sd_stripe_add_order_to_check($_REQUEST['order_id'])) {
        fn_set_notification('N', __('notice'), __('sd_stripe_add_order_to_check'));
        if (defined('AJAX_REQUEST')) {
            $info_on_check_bitcoin = fn_sd_stripe_get_info_on_check_order($_REQUEST['order_id']);
            Tygh::$app['view']->assign('info_on_check_bitcoin', $info_on_check_bitcoin);
            Tygh::$app['view']->display('addons/sd_stripe/views/orders/components/info_on_check_bitcoin.tpl');
            exit();
        }
    }
}
