<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'place_order') {
        $params = $_POST;
        $save_stripe = !empty($_REQUEST['save_new_stripe_card']) && $_REQUEST['save_new_stripe_card'] == 'Y';
        if (!empty($auth['user_id']) && $save_stripe) {
            fn_stripe_add_credit_card($_SESSION['auth']['user_id'], $_REQUEST['payment_info']);
        }
        $redirect_params = fn_sd_stripe_get_stripe_redirect_params($params);

        if (!empty($params['stripeToken']) && $params['stripeTokenType'] == 'alipay_account') {
            return array(CONTROLLER_STATUS_REDIRECT, 'checkout.checkout?' . http_build_query($redirect_params));
        }
    }
    return;
}
