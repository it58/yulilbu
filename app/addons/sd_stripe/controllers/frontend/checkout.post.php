<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$cart = &$_SESSION['cart'];

if ($mode == 'checkout') {
    $edit_step = !empty($cart['edit_step']) ? $cart['edit_step'] : '';

    if ($edit_step == 'step_four' && !empty($auth['user_id'])) {
        fn_init_stripe();

        $user_stripe_data = fn_get_user_stripe_data($auth['user_id']);

        if (!empty($user_stripe_data)) {
            Registry::get('view')->assign('user_stripe_credit_cards', $user_stripe_data['credit_cards']);
        }
    }
}