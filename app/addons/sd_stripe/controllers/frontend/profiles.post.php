<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'update') {
    Registry::set('navigation.tabs.stripe', array(
        'title' => __('stripe'),
        'js' => true
    ));

    $user_data = Registry::get('view')->getTemplateVars('user_data');

    fn_init_stripe();
    $user_data['user_stripe_data'] = fn_get_user_stripe_data($user_data['user_id']);
    Registry::get('view')->assign('user_data', $user_data);

} elseif ($mode == 'get_stripe_account') {
    if (!empty($auth['user_id'])) {
        fn_init_stripe();
        $user_data = fn_get_user_info($auth['user_id']);

        try {
            $stripe_response = Stripe\Customer::create(array(
                'email' => $user_data['email'],
                'description' => $user_data['firstname'] . ' ' . $user_data['lastname'],
                'metadata' => array(
                    __('phone') => $user_data['phone'],
                    __('fax') => $user_data['fax'],
                    __('url') => $user_data['url'],
                    __('billing_address') => $user_data['b_address'],
                    __('billing_address') . '#2' => $user_data['b_address_2'],
                    __('city') => $user_data['b_city'],
                    __('state') => fn_get_state_name($user_data['b_state'], $user_data['b_country']),
                    __('country') => fn_get_country_name($user_data['b_country']),
                    __('b_zipcode') => $user_data['b_zipcode'],
                )
            ));

            if (!empty($stripe_response)) {
                fn_set_user_strype_id($auth['user_id'], $stripe_response['id']);
                fn_set_notification('N', __('notice'), __('stripe_account_created'));
            }
        } catch (Stripe\Error\Base $e) {
            fn_set_notification('E', __('error'), __('get_stripe_account_error_text'));
        }

        return array(CONTROLLER_STATUS_REDIRECT, 'profiles.update?selected_section=stripe');
    }

} elseif ($mode == 'delete_stripe_account') {
    if (!empty($auth['user_id'])) {
        fn_init_stripe();
        $user_data = fn_get_user_info($auth['user_id'], false);

        if (!empty($user_data['stripe_id'])) {
            try {
                $user_stripe_data = Stripe\Customer::retrieve($user_data['stripe_id']);
                $user_stripe_data->delete();

                fn_delete_user_stripe_id($user_data['user_id']);
                fn_set_notification('N', __('notice'), __('stripe_account_deleted'));
            } catch (Stripe\Error\Base $e) {
                $stripe_response = $e->getJsonBody();
                if (!empty($stripe_response['error']['message'])) {
                    fn_set_notification('E', __('error'), $stripe_response['error']['message']);
                }
            }
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'profiles.update?selected_section=stripe');

} elseif ($mode == 'stripe_delete_card') {
    if (!empty($auth['user_id']) && !empty($_REQUEST['card_id'])) {
        fn_init_stripe();
        $user_data = fn_get_user_info($auth['user_id'], false);

        if (!empty($user_data['stripe_id'])) {
            try {
                $stripe_customer = Stripe\Customer::retrieve($user_data['stripe_id']);
                $stripe_customer->sources->retrieve($_REQUEST['card_id'])->delete();

                fn_set_notification('N', __('notice'), __('stripe_credit_card_was_deleted'));
            } catch (Stripe\Error\Base $e) {
                $stripe_response = $e->getJsonBody();
                if (!empty($stripe_response['error']['message'])) {
                    fn_set_notification('E', __('error'), $stripe_response['error']['message']);
                }
            }
        }
    }

    return array(CONTROLLER_STATUS_REDIRECT, 'profiles.update?selected_section=stripe');
}