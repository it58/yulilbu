<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $cart = & Tygh::$app['session']['cart'];

    if ($mode == 'update_payment') {
        unset($cart['active_stripe_tab']);
    }

    if ($mode == 'active_stripe_tab') {
        if (!empty($_REQUEST['active_stripe_tab'])) {
            $cart['active_stripe_tab'][$cart['payment_id']] = $_REQUEST['active_stripe_tab'];
        }
        $suffix = !empty($cart['order_id']) ? '.update' : '.add';
        return array(CONTROLLER_STATUS_OK, 'order_management' . $suffix);
    }
    return;
}