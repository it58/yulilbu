<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'check_orders') {
    $addon_setting = Registry::get('addons.sd_stripe');
    if (!empty($_REQUEST['cron_key']) && $_REQUEST['cron_key'] == $addon_setting['cron_key']) {
        set_time_limit(0);
        fn_sd_stripe_check_orders($addon_setting);
    } else {
        fn_echo(__('sd_stripe_cron_key_error') . '<br/>');
    }
    die(__('done'));
}