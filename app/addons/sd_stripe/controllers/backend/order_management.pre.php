<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$params = $_POST;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'place_order') {
        $redirect_params = fn_sd_stripe_get_stripe_redirect_params($params);
        if (!empty($redirect_params)) {
            return array(CONTROLLER_STATUS_REDIRECT, 'order_management.update?' . http_build_query($redirect_params));
        }
    } elseif ($mode == 'add') {
        $redirect_params = fn_sd_stripe_get_stripe_redirect_params($params);
        if (!empty($redirect_params)) {
            return array(CONTROLLER_STATUS_REDIRECT, 'order_management.add?' . http_build_query($redirect_params));
        }
    }
    return;
}