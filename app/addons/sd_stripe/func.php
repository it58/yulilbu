<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_stripe_install()
{
    db_query("DELETE FROM ?:payment_processors WHERE processor_script = ?s", 'stripe.php');
    db_query("INSERT INTO ?:payment_processors ?e", array(
        'processor' => 'Stripe',
        'processor_script' => 'stripe.php',
        'processor_template' => 'addons/sd_stripe/views/orders/components/payments/stripe_cc.tpl',
        'admin_template' => 'stripe.tpl',
        'callback' => 'Y',
        'type' => 'P',
        'addon' => 'sd_stripe'
    ));

    $service = Tygh::$app['template.mail.service'];
    $service->createTemplate(array(
        'code' => 'stripe_payment',
        'area' => 'C',
        'status' => 'A',
        'subject' => '{{ company_name }}:{{__("order")}} #{{order_info.order_id}} {{order_status.email_subj}}',
        'template' => '{{ snippet("header", {"title": __("change_order_status_f_subj", {"[order]": order_info.order_id}) } ) }}
                        <td>
                            {{ __("dear") }} {% if order_info.firstname %}{{ order_info.firstname }} {% else %} {{ __("customer") }} {% endif %},<br />
                            {{fail_check_message_to_email}}<br /><br />
                            {{ include_invoice(order_info.order_id) }}
                        </td>
                        {{ snippet("footer") }}',
    ));

}

function fn_stripe_uninstall()
{
    $template_code = 'stripe_payment';
    $area = 'C';
    $service = Tygh::$app['template.mail.service'];
    $service->removeTemplateByCodeAndArea($template_code, $area);

    db_query('DELETE FROM ?:template_emails WHERE code = ?s AND area = ?s', $template_code, $area);

    $processor_ids = db_get_fields('SELECT processor_id FROM ?:payment_processors WHERE processor_script = ?s', 'stripe.php');
    if ($processor_ids) {
        $payment_ids = db_get_fields('SELECT payment_id FROM ?:payments WHERE processor_id IN (?n)', $processor_ids);
        if ($payment_ids) {
            foreach($payment_ids as $payment_id) {
                fn_delete_payment($payment_id);
            }
        }
        db_query('DELETE FROM ?:payment_processors WHERE processor_id IN (?n)', $processor_ids);
    }
}

function fn_get_object_stripe_id($obj_id, $obj_type)
{
    return db_get_field("SELECT stripe_id FROM ?:objects_stripe_ids WHERE obj_id = ?i AND obj_type = ?s", $obj_id, $obj_type);
}

function fn_get_user_stripe_id($user_id)
{
    return fn_get_object_stripe_id($user_id, STRIPE_USER);
}

function fn_set_object_stripe_id($obj_id, $obj_type, $stripe_id)
{
    if (!empty($stripe_id)) {
        db_query("REPLACE INTO ?:objects_stripe_ids ?e", array(
            'obj_id' => $obj_id,
            'obj_type' => $obj_type,
            'stripe_id' => $stripe_id
        ));
    }
}

function fn_set_user_strype_id($user_id, $user_stripe_id)
{
    if (!empty($user_stripe_id)) {
        fn_set_object_stripe_id($user_id, STRIPE_USER, $user_stripe_id);
    }
}

function fn_delete_object_stripe_id($obj_id, $obj_type)
{
    db_query("DELETE FROM ?:objects_stripe_ids WHERE obj_id = ?i AND obj_type = ?s", $obj_id, $obj_type);
}

function fn_delete_user_stripe_id($user_id)
{
    fn_delete_object_stripe_id($user_id, STRIPE_USER);
}

function fn_init_stripe()
{
    static $only_one = false;
    if ($only_one) {
        return;
    }
    $only_one = true;

    require_once(Registry::get('config.dir.lib') . 'other/stripe/Stripe.php');

    Stripe\Stripe::setApiKey(Registry::get('addons.sd_stripe.secret_key'));
    Stripe\Stripe::setApiVersion(STRIPE_VERSION);
}

function fn_get_user_stripe_data($user_id)
{
    $user_data = fn_get_user_info($user_id);
    $user_stripe_data = array();

    if (!empty($user_data['stripe_id'])) {
        try {
            $stripe_data = Stripe\Customer::retrieve($user_data['stripe_id']);
            if (!empty($stripe_data['deleted'])) {
                fn_delete_user_stripe_id($user_data['user_id']);
            } else {
                $user_stripe_data = array(
                    'timestamp' => $stripe_data['created'],
                    'account_balance' => $stripe_data['account_balance'] / 100,
                    'currency' => strtoupper($stripe_data['currency'])
                );
                $_cards = array();
                $user_cards = $stripe_data['sources'];
                foreach ($user_cards['data'] as $_card) {
                    $_cards[] = $_card;
                }
                $user_stripe_data['credit_cards'] = $_cards;
            }
        } catch (Stripe\Error\Base $e) {
            $stripe_response = $e->getJsonBody();
            fn_set_notification('E', __('error'), $stripe_response['error']['message']);
        }
    }

    return $user_stripe_data;
}

/*
 * Hooks
 */

function fn_sd_stripe_get_user_info($user_id, $get_profile, $profile_id, &$user_data)
{
    $user_data['stripe_id'] = fn_get_user_stripe_id($user_id);
}

function fn_sd_stripe_pay_by_strip_card($order_info)
{
    return !empty($order_info['payment_info']['stripe_type_flag']) && $order_info['payment_info']['stripe_type_flag'] == 'stripe';
}

function fn_sd_stripe_pay_by_strip_bitcoin($order_info)
{
    return !empty($order_info['payment_info']['stripe_type_flag']) && $order_info['payment_info']['stripe_type_flag'] == 'bitcoin';
}

function fn_sd_stripe_pay_by_strip_alipay($order_info)
{
    return !empty($order_info['payment_info']['stripe_type_flag']) && $order_info['payment_info']['stripe_type_flag'] == 'alipay';
}

function fn_sd_stripe_get_bitcoin_receiver_by_order_id($order_id, $force_notification = false)
{
    $result = false;
    $order_info = fn_get_order_info($order_id);

    if (fn_sd_stripe_pay_by_strip_bitcoin($order_info)) {
        $id = $order_info['payment_info']['transaction_id'];

        try {
            fn_init_stripe();
            $payment_response = Stripe\Source::retrieve($id);
            $payment_response = $payment_response;
            if (!empty($payment_response['status'])) {
                $charge = Stripe\Charge::create(array(
                    'amount' => $payment_response['amount'],
                    'currency' => $payment_response['currency'],
                    'source' => $id,
                ));
                if ($charge['status'] == 'succeeded') {
                    $pp_response['retrieve_created'] = $payment_response['created'];
                    $payment_response['bitcoin'] = $payment_response['bitcoin'];
                    $pp_response['bitcoin_amount_received'] = $payment_response['bitcoin']['amount_received'];
                    if ($payment_response['bitcoin']['amount_received'] >= $payment_response['bitcoin']['amount']) {
                        $pp_response['order_status'] = 'O';
                        $result = true;
                    }
                }
            }
        } catch(Stripe\Error\Base $e) {
            $payment_response = $e->getJsonBody();
            $pp_response['error_created'] = TIME;
            $pp_response['reason_text'] = $payment_response['error']['message'];
        }

        fn_update_order_payment_info($order_id, $pp_response);

        if ($result) {
            fn_change_order_status($order_id, 'O', '', $force_notification);
        }
    }
    return $result;
}


function fn_sd_stripe_add_order_to_check($order_id)
{
    $result = false;
    if (!empty($order_id)) {
        $maximum_check = Registry::get('addons.sd_stripe.maximum_check');
        if (is_numeric($maximum_check) && $maximum_check > 0) {
            $param = array(
                'order_id' => $order_id,
                'have_to_check' => $maximum_check
            );
            $result = db_query('REPLACE INTO ?:stripe_order_ids_to_check ?e', $param);
        }
    }
    return $result;
}

function fn_sd_stripe_remove_order_from_check($order_id)
{
    if (!empty($order_id)) {
        db_query('DELETE FROM ?:stripe_order_ids_to_check WHERE order_id = ?i', $order_id);
    }
}

function fn_sd_stripe_get_info_on_check_order($order_id, $parent_order_id = null)
{
    $result = array();
    if (!empty($order_id)) {
        if ($parent_order_id === null) {
            $parent_order_id = db_get_field('SELECT parent_order_id FROM ?:orders WHERE order_id = ?i', $order_id);
        }
        $result = db_get_row('SELECT order_id, have_to_check FROM ?:stripe_order_ids_to_check WHERE order_id = ?i OR order_id = ?i', $order_id, $parent_order_id);
    }
    return $result;
}


function fn_sd_stripe_get_order_info(&$order, $additional_data)
{
    if (fn_sd_stripe_pay_by_strip_bitcoin($order)) {
        $info_on_check_bitcoin = fn_sd_stripe_get_info_on_check_order($order['order_id'], $order['parent_order_id']);
        if (!empty($info_on_check_bitcoin)) {
            $order['info_on_check_bitcoin'] = $info_on_check_bitcoin;
        }
    }
}

function fn_sd_stripe_cron_url()
{
    $key = Registry::get('addons.sd_stripe.cron_key');
    $url = fn_url('stripe_check_paid.check_orders?cron_key=' . urlencode($key), 'A');
    $text = __('sd_stripe_cron_url_info') . '<br/ >' . ' <a href="' . $url . '">' . $url . '</a><br />';

    return $text;
}

function fn_sd_stripe_send_notify_fail_check($order_id)
{
    $order_info = fn_get_order_info($order_id);
    if (!empty($order_info)) {
        $order_statuses = fn_get_statuses(STATUSES_ORDER, array(), true, false, ($order_info['lang_code'] ? $order_info['lang_code'] : CART_LANGUAGE), $order_info['company_id']);
        $order_status = $order_statuses[$order_info['status']];
        $payment_id = !empty($order_info['payment_method']['payment_id']) ? $order_info['payment_method']['payment_id'] : 0;
        $payment_method = fn_get_payment_data($payment_id, $order_info['order_id'], $order_info['lang_code']);
        $status_settings = $order_statuses[$order_info['status']]['params'];

        $mailer = Tygh::$app['mailer'];

        $mailer->send(array(
            'to' => $order_info['email'],
            'from' => 'company_orders_department',
            'data' => array(
                'order_info' => $order_info,
                'order_status' => $order_status,
                'fail_check_message_to_email' => __('fail_check_message_to_email'),
            ),
            'template_code' => 'stripe_payment',
            'tpl' => 'addons/sd_stripe/fail_check_payment_confirmation_notification.tpl',
            'company_id' => $order_info['company_id'],
        ), 'C', $order_info['lang_code']);
    }
}

function fn_sd_stripe_check_orders($addon_setting)
{
    $maximum_check = $addon_setting['maximum_check'];
    if (!empty($maximum_check) && $maximum_check > 0) {
        $orders_for_check = db_get_array('SELECT order_id, have_to_check FROM ?:stripe_order_ids_to_check');
        if (!empty($orders_for_check)) {
            $remove_order_ids = array();
            $check_false_order_ids = array();

            if ($addon_setting['notify_customer'] == 'Y') {
                $force_notification['C'] = true;
            }
            if ($addon_setting['notify_orders_department'] == 'Y') {
                $force_notification['A'] = true;
            }
            if (fn_allowed_for('MULTIVENDOR') && $addon_setting['notify_vendor'] == 'Y') {
                $force_notification['V'] = true;
            }
            if (!isset($force_notification)) {
                $force_notification = false;
            }

            $notify_customer_fail_check = ($addon_setting['notify_customer_fail_check'] == 'Y');

            foreach ($orders_for_check as $order_check) {
                if ($order_check['have_to_check'] <= 0) {
                    $remove_order_ids[] = $order_check['order_id'];
                } else {
                    $result_check = fn_sd_stripe_get_bitcoin_receiver_by_order_id($order_check['order_id'], $force_notification);
                    if ($result_check) {
                        $remove_order_ids[] = $order_check['order_id'];
                    } else {
                        if ($order_check['have_to_check'] > 1) {
                            $check_false_order_ids[] = $order_check['order_id'];
                        } else {
                             if ($notify_customer_fail_check) {
                                fn_sd_stripe_send_notify_fail_check($order_check['order_id']);
                            }
                            $remove_order_ids[] = $order_check['order_id'];
                        }
                    }
                }
            }

            if (!empty($remove_order_ids)) {
                db_query('DELETE FROM ?:stripe_order_ids_to_check WHERE order_id in (?n)', $remove_order_ids);
            }

            if (!empty($check_false_order_ids)) {
                db_query('UPDATE ?:stripe_order_ids_to_check SET have_to_check = have_to_check-1 WHERE order_id in (?n)', $check_false_order_ids);
            }
        }
    }
}

function fn_sd_stripe_get_stripe_redirect_params($params) {

    $redirect_params = array();

    if (!empty($params['stripeToken']) && ($params['stripeTokenType'] == 'alipay_account' || $params['stripeTokenType'] == 'card')) {
        $redirect_params['stripeToken'] = $params['stripeToken'];
        $redirect_params['stripeEmail'] = $params['stripeEmail'];
        $redirect_params['active_stripe_tab'] = 'alipay';

        if (!empty($params['tab_id'])) {
            $redirect_params['active_tab'] = $params['tab_id'];
        }
    }

    return $redirect_params;
}

function fn_sd_stripe_get_currencies()
{
    $currencies = array('AED', 'AFN', 'ALL', 'AMD', 'ANG', 'AOA', 'ARS', 'AUD', 'AWG', 'AZN', 'BAM', 'BBD', 'BDT', 'BGN', 'BIF', 'BMD', 'BND', 'BOB', 'BRL', 'BSD',
        'BWP', 'BZD', 'CAD', 'CDF', 'CHF', 'CLP', 'CNY', 'COP', 'CRC', 'CVE', 'CZK', 'DJF', 'DKK', 'DOP', 'DZD', 'EEK', 'EGP', 'ETB', 'EUR', 'FJD', 'FKP', 'GBP',
        'GEL', 'GIP', 'GMD', 'GNF', 'GTQ', 'GYD', 'HKD', 'HNL', 'HRK', 'HTG', 'HUF', 'IDR', 'ILS', 'INR', 'ISK', 'JMD', 'JPY', 'KES', 'KGS', 'KHR', 'KMF', 'KRW',
        'KYD', 'KZT', 'LAK', 'LBP', 'LKR', 'LRD', 'LSL', 'LTL', 'LVL', 'MAD', 'MDL', 'MGA', 'MKD', 'MNT', 'MOP', 'MRO', 'MUR', 'MVR', 'MWK', 'MXN', 'MYR', 'MZN',
        'NAD', 'NGN', 'NIO', 'NOK', 'NPR', 'NZD', 'PAB', 'PEN', 'PGK', 'PHP', 'PKR', 'PLN', 'PYG', 'QAR', 'RON', 'RSD', 'RUB', 'RWF', 'SAR', 'SBD', 'SCR', 'SEK',
        'SGD', 'SHP', 'SLL', 'SOS', 'SRD', 'STD', 'SVC', 'SZL', 'THB', 'TJS', 'TOP', 'TRY', 'TTD', 'TWD', 'TZS', 'UAH', 'UGX', 'USD', 'UYU', 'UZS', 'VEF', 'VND',
        'VUV', 'WST', 'XAF', 'XCD', 'XOF', 'XPF', 'YER', 'ZAR', 'ZMW');

    return $currencies;
}

function fn_stripe_add_credit_card($user_id, $payment_info)
{
    $current_controller = Registry::get('runtime.controller');
    $user_info = fn_get_user_info($user_id);
    if (!empty($user_info['stripe_id']) && !empty($payment_info['stripeToken'])) {

        try {
            fn_init_stripe();
            $customer = Stripe\Customer::retrieve($user_info['stripe_id']);
            $card = $customer->sources->create(array('source' => $payment_info['stripeToken']));
            $customer->default_source = $card->id;
            $customer->save();
            fn_set_notification('N', __('notice'), __('stripe_credit_card_was_added'));

        } catch (Stripe\Error\Base $e) {
            $stripe_response = $e->getJsonBody();
            if ($current_controller != 'checkout') {
                fn_set_notification('E', __('error'), $stripe_response['error']['message']);
            }
        }
    }
}