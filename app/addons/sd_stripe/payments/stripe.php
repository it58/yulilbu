<?php
/***************************************************************************
*                                                                          *
*    Copyright (c) 2009 Simbirsk Technologies Ltd. All rights reserved.    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$user_info = fn_get_user_info($order_info['user_id']);

if (CART_PRIMARY_CURRENCY != $processor_data['processor_params']['currency']) {
    $cur = Registry::get('currencies.' . $processor_data['processor_params']['currency']);
    if (isset($cur)) {
        $order_total = $order_info['total'] / $cur['coefficient'];
        $order_total = round($order_total, $cur['decimals']);
        $order_info['total'] = $order_total;
    } else {
        $message = __('sd_stripe_undefine_currency');
        $message = str_replace('[currency]', $processor_data['processor_params']['currency'], $message);

        fn_set_notification('E', __('error'), $message);
        if (AREA == 'C') {
            fn_redirect('checkout.checkout');
        } else {
            fn_redirect('order_management.add');
        }
    }
}

$payment_request_params = array(
    'amount' => ($processor_data['processor_params']['currency'] == 'JPY') ? (int)$order_info['total'] : $order_info['total'] * CENT, //for yen the 100Y format is used, not 100.00Y
    'currency' => $processor_data['processor_params']['currency'],
    'metadata' => array(
        __('customer_email') => $order_info['email'],
        __('order_id') => $order_info['order_id']
    ),
);

if (fn_sd_stripe_pay_by_strip_alipay($order_info)) {

    if (empty($order_info['payment_info']['stripeEmail']) || !fn_validate_email($order_info['payment_info']['stripeEmail'])) {
        fn_set_notification('E', __('error'), __('not_valid_alipay_email'));
        fn_order_placement_routines('checkout_redirect');
    }

    $payment_request_params['source'] = $order_info['payment_info']['stripeToken'];

    try {
        fn_init_stripe();
        $payment_response = Stripe\Charge::create($payment_request_params);
        $pp_response['order_status'] = 'P';
        $pp_response['transaction_id'] = $payment_response['id'];

    } catch(\Stripe\Error\Base $e) {
        $payment_response = $e->getJsonBody();
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = $payment_response['error']['message'];
    }

} elseif (fn_sd_stripe_pay_by_strip_bitcoin($order_info)) {
    $payment_request_params['statement_descriptor'] = Registry::get('settings.Company.company_name') . ': ' . __('payment_for_order') . $order_info['order_id'];

    if (empty($order_info['payment_info']['bitcoin_email']) || !fn_validate_email($order_info['payment_info']['bitcoin_email'])) {
        fn_set_notification('E', __('error'), __('not_valid_bitcoin_email'));
        fn_order_placement_routines('checkout_redirect');
    }

    try {
        fn_init_stripe();
        $payment_request_params['type'] = 'bitcoin';
        $payment_request_params['owner'] = array('email' => $order_info['payment_info']['bitcoin_email']);
        $payment_response = Stripe\Source::create($payment_request_params);
        $payment_response = $payment_response;
        $payment_response['bitcoin'] = $payment_response['bitcoin'];
        $payment_response['owner'] = $payment_response['owner'];
        $pp_response['order_status'] = 'P';
        $pp_response['transaction_id'] = $payment_response['id'];
        $pp_response['created'] = $payment_response['created'];
        $pp_response['bitcoin_uri'] = $payment_response['bitcoin']['uri'];
        $pp_response['inbound_address'] = $payment_response['bitcoin']['address'];
        $pp_response['bitcoin_amount'] = $payment_response['bitcoin']['amount'];
        $pp_response['bitcoin_amount_received'] = $payment_response['bitcoin']['amount_received'];
        $pp_response['email'] = $payment_response['owner']['email'];

    } catch(Stripe\Error\Base $e) {
        $payment_response = $e->getJsonBody();
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = $payment_response['error']['message'];
    }

} elseif (fn_sd_stripe_pay_by_strip_card($order_info)) {

    $payment_request_params['receipt_email'] = $order_info['email'];
    $payment_request_params['description'] = Registry::get('settings.Company.company_name') . ': ' . __('payment_for_order') . $order_info['order_id'];

    if (!empty($order_info['payment_info']['card_id']) && !empty($user_info['stripe_id'])) {

        $payment_request_params['customer'] = $user_info['stripe_id'];
        $payment_request_params['card'] = $order_info['payment_info']['card_id'];
    } elseif (!empty($_REQUEST['save_new_stripe_card']) && $_REQUEST['save_new_stripe_card'] == 'Y' && !empty($user_info['stripe_id'])) {

        try {
            fn_init_stripe();
            $customer = Stripe\Customer::retrieve($user_info['stripe_id']);
            $customer = $customer;
            $payment_request_params['customer'] = $customer['id'];
            $payment_request_params['card'] = $customer['default_source'];

        } catch(Exception $e) {
            $payment_response = $e->getJsonBody();
            $pp_response['reason_text'] = $payment_response['error']['message'];
        }

    } elseif (!empty($order_info['payment_info']['stripeToken'])) {

        $payment_request_params['source'] = $order_info['payment_info']['stripeToken'];
        $payment_request_params['amount'] = $order_info['total'] * 100;
        $payment_request_params['currency'] = $order_info['secondary_currency'];
    }

    try {
        fn_init_stripe();
        $payment_response = Stripe\Charge::create($payment_request_params);
        $payment_response = $payment_response;
        $pp_response['order_status'] = 'P';
        $pp_response['transaction_id'] = $payment_response['id'];

    } catch(Exception $e) {
        $payment_response = $e->getJsonBody();
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = $payment_response['error']['message'];
    }

} else {
    fn_set_notification('E', __('error'), __('sd_strip_not_valid_payment_settings'));
    fn_order_placement_routines('checkout_redirect');
}

fn_finish_payment($order_info['order_id'], $pp_response, false);

if (!empty($need_add_to_check) && $need_add_to_check) {
    fn_sd_stripe_add_order_to_check($order_info['order_id']);
}
fn_order_placement_routines('route', $order_info['order_id']);