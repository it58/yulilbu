<?php

use Tygh\Registry;
use Tygh\Storage;
use Tygh\Session;
use Tygh\Settings;
use Tygh\Languages\Languages;

if ( !defined('AREA') ) { die('Access denied'); }

function fn_wk_quantity_sold_update_product_post(&$product_data, &$product_id)
{
    if(isset($product_data['wk_quantity_sold_val'])){
        if (!($product_data['wk_quantity_sold_val'])) {
            $id = db_get_field("SELECT id FROM ?:wk_quantity_sold WHERE product_id  = ?i", $product_id);
            if(!$id)
            return false;
        }
        $_data = array (
                'product_id' => $product_id,
                'quantity_sold'=> $product_data['wk_quantity_sold_val'],
            );
        db_query("REPLACE INTO ?:wk_quantity_sold ?e", $_data);
        return true;
    }
}

function fn_wk_quantity_sold_get_product_data(&$product_id, &$field_list, &$join, &$auth)
{
    $field_list .= ", ?:wk_quantity_sold.quantity_sold as wk_quantity_sold_val";
    $join .= db_quote(" LEFT JOIN ?:wk_quantity_sold ON ?:wk_quantity_sold.product_id = ?:products.product_id AND ?:wk_quantity_sold.product_id = ?i", $product_id);
}

function fn_wk_quantity_sold_get_product_data_post(&$product_data, $auth, $preview, $lang_code){
	$quantity_sold=fn_get_quantity_sold_info($product_data['product_id']);
	if($quantity_sold){
		$product_data['quantity_sold'] = $quantity_sold;
	}
}

function fn_wk_quantity_sold_get_products_post(&$products, $params, $lang_code){
	foreach($products as $product_data){
		$product_id=$product_data['product_id'];
		$quantity_sold=fn_get_quantity_sold_info($product_id);
		if($quantity_sold){
			$products[$product_id]['quantity_sold']=$quantity_sold;
		}
	}
}

function fn_get_quantity_sold_info($product_id){
    $quantity_sold = db_get_field("SELECT quantity_sold FROM ?:wk_quantity_sold WHERE product_id = ?i",$product_id);
    $product_sold = db_get_field("SELECT amount FROM ?:product_sales WHERE product_id = ?i",$product_id);
    $total_item_sold = (int)$quantity_sold + (int)$product_sold;
    return $total_item_sold;
}
