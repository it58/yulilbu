<?php    

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
	'update_product_post',
	'get_product_data',
    'get_product_data_post',
    'get_products_post'
);
