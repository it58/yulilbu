<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh\Addons\SdShippingByProduct\Documents\Order;

use Tygh\Template\Document\Order\Context;
use Tygh\Template\IActiveVariable;
use Tygh\Template\IVariable;
use Tygh\Tools\Formatter;

/**
 * Class DetailedShippingInfoVariable
 * @package Tygh\Addons\SdShippingByProduct\Documents\Order
 */
class DetailedShippingInfoVariable implements IVariable, IActiveVariable
{
    public $items = array();

    public function __construct(Context $context, Formatter $formatter)
    {
        $order = $context->getOrder();
        $products = $order->data['products'];

        foreach ($order->data['products'] as $item_id => $product) {
            if (!empty($product['extra']['shipping_info']['shipping_id'])) {
                $this->items[$item_id] = array(
                    'product' => $product['product'],
                    'shipping_method' => fn_get_shipping_name($product['extra']['shipping_info']['shipping_id']),
                    'shipping_cost' => !empty($product['extra']['shipping_info']['shipping_cost']) ? $formatter->asPrice($product['extra']['shipping_info']['shipping_cost']) : 0,
                    'delivery_time' => !empty($product['extra']['shipping_info']['delivery_time']) ? $product['extra']['shipping_info']['delivery_time'] : ''
                );
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function attributes()
    {
        return array(
            'items' => array(
                '[0..N]' => array(
                    'product', 'shipping_method', 'shipping_cost', 'delivery_time'
                )
            )
        );
    }
}