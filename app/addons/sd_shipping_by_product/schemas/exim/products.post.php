<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['export_fields']['Shipping ID'] = array (
    'db_field' => 'shipping_id',
);

$schema['export_fields']['Shipping method'] = array (
    'db_field' => 'shipping_id',
    'process_get' => array('fn_get_shipping_name', '#this'),
    'convert_put' => array('fn_sd_shipping_by_product_get_shipping_id_by_name', '#this'),
);

$schema['export_fields']['Shipping cost'] = array (
    'db_field' => 'shipping_cost',
);

$schema['export_fields']['Delivery time'] = array (
    'db_field' => 'delivery_time',
    'process_get' => array('fn_sd_shipping_by_product_get_delivery_time_value', '#this'),
    'convert_put' => array('fn_sd_shipping_by_product_get_delivery_time_id_by_name', '#this'),
);

return $schema;