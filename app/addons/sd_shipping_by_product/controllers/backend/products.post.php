<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return array(CONTROLLER_STATUS_OK);
}

if ($mode == 'update' || $mode == 'add') {

    $product_data = Tygh::$app['view']->getTemplateVars('product_data');
    $all_countries = fn_get_simple_countries(true, CART_LANGUAGE);
    if (!empty($product_data['available_countries'])) {
        $all_countries = array_diff_assoc($all_countries, $product_data['available_countries']);
    } else {
        $product_data['available_countries'] = $all_countries;
        $all_countries = array();
    }

    Tygh::$app['view']->assign(array(
        'shipping_methods' => fn_sd_shipping_by_product_get_simple_shipping_list(),
        'delivery_time_list' => fn_sd_shipping_by_product_get_delivery_time_list(),
        'all_available_countries' => $all_countries,
        'product_data' => $product_data
    ));

}