<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Gets shipping ID by shipping name
 *
 * @param string $shipping_method Shipping name
 * @return int Shipping ID
 */
function fn_sd_shipping_by_product_get_shipping_id_by_name($shipping_method)
{
    return !empty($shipping_method) ? db_get_field('SELECT shipping_id FROM ?:shipping_descriptions WHERE shipping = ?s', $shipping_method) : 0;
}

/**
 * Gets product shipping data
 *
 * @param int $product_id Product identifier
 * @return array Shipping data
 */
function fn_sd_shipping_by_product_get_product_shipping_data($product_id)
{
    $shipping_data = array();
    $addon_settings = Registry::get('addons.sd_shipping_by_product');
    $default_shipping_data = array(
        'shipping_id' => !empty($addon_settings['default_shipping_method']) ? $addon_settings['default_shipping_method'] : 0,
        'shipping_cost' => !empty($addon_settings['default_shipping_cost']) ? $addon_settings['default_shipping_cost'] : 0,
        'delivery_time' => !empty($addon_settings['default_delivery_time']) ? $addon_settings['default_delivery_time'] : 0,
        'available_countries' => array()
    );
    if (!empty($product_id)) {
        $shipping_data = db_get_row(
            'SELECT shipping_id, shipping_cost, delivery_time, available_countries FROM ?:products '
            . 'WHERE product_id = ?i',
            $product_id
        );
        if (empty($shipping_data['shipping_id'])) {
            $shipping_data['shipping_id'] = $default_shipping_data['shipping_id'];
        }
        if (empty(floatval($shipping_data['shipping_cost']))) {
            $shipping_data['shipping_cost'] = $default_shipping_data['shipping_cost'];
        }
        if (is_null($shipping_data['delivery_time'])) {
            $shipping_data['delivery_time'] = $default_shipping_data['delivery_time'];
        }
        if (!empty($shipping_data['available_countries'])) {
            $shipping_data['available_countries'] = explode(',', $shipping_data['available_countries']);
        } else {
            $shipping_data['available_countries'] = array();
        }
    } else {
        $shipping_data = $default_shipping_data;
    }

    return $shipping_data;
}

/**
 * Hook calculates shipping for each product
 *
 */
function fn_sd_shipping_by_product_calculate_cart_taxes_pre(&$cart, $cart_products, &$product_groups, $calculate_taxes, $auth)
{
    $product_shippings = array();
    $cart['shipping_cost'] = $cart['display_shipping_cost'] = 0;
    $cart['chosen_shipping_by_product'] = array();
    $count_shipping_failed = 0;
    foreach ($product_groups as $group_key => $group) {
        if (!empty($group['products'])) {
            foreach ($group['products'] as $item_id => $product) {
                if (!empty($product['product_id'])) {
                    $product_shippings[$item_id] = fn_sd_shipping_by_product_get_product_shipping_data($product['product_id']);
                    $product_groups[$group_key]['products'][$item_id]['chosen_shippings'] = $product_groups[$group_key]['products'][$item_id]['shippings'] = array();
                    if (!empty($cart['user_data']['s_country']) && !empty($product_shippings[$item_id]['available_countries']) && !in_array($cart['user_data']['s_country'], $product_shippings[$item_id]['available_countries'])) {
                        $count_shipping_failed++;
                        $cart['company_shipping_failed'] = true;
                        continue;
                    }
                    if (!empty($product_shippings[$item_id]['shipping_id'])) {
                        $shipping_id = $product_shippings[$item_id]['shipping_id'];
                        $product_shippings[$item_id]['rate'] = !empty($product_shippings[$item_id]['shipping_cost']) ? $product_shippings[$item_id]['shipping_cost'] : 0;
                        $product_shippings[$item_id]['product_delivery_time'] = isset($product_shippings[$item_id]['delivery_time']) ? $product_shippings[$item_id]['delivery_time'] : 0;
                        $product_shippings[$item_id]['product_delivery_time_text'] = fn_sd_shipping_by_product_get_delivery_time_value($product_shippings[$item_id]['product_delivery_time']);
                        $shipping_data = $product_groups[$group_key]['products'][$item_id]['chosen_shippings'][$shipping_id] = $product_groups[$group_key]['products'][$item_id]['shippings'][$shipping_id] = array_merge($product_shippings[$item_id], fn_get_shipping_info($shipping_id));
                        $shipping_data['group_key'] = $group_key;
                        $cart['products'][$item_id]['extra']['shipping_info'] = $product_shippings[$item_id];
                        $cart['chosen_shipping_by_product'][$group_key][$item_id] = $cart['chosen_shipping'][$group_key] = $shipping_id;
                        $product_groups[$group_key]['shippings'][$shipping_id] = $shipping_data;
                    }
                    $cart['shipping_cost'] += !empty($product_shippings[$item_id]['shipping_cost']) ? $product_shippings[$item_id]['shipping_cost'] : 0;
                }
                if (!isset($cart['chosen_shipping'][$group_key]) && !$group['shipping_no_required']) {
                    $count_shipping_failed++;
                    $cart['company_shipping_failed'] = true;
                }
            }
        }
    }
    $cart['display_shipping_cost'] = $cart['shipping_cost'];
    if (!empty($product_groups) && count($product_groups) == $count_shipping_failed) {
        $cart['shipping_failed'] = true;
    } else {
        $cart['shipping_failed'] = $cart['company_shipping_failed'] = false;
    }
}

/**
 * Gets delivery time list
 *
 * @return array Delivery time list
 */
function fn_sd_shipping_by_product_get_delivery_time_list()
{
    return array(
        '1 ' . __('day'),
        '2 ' . __('days'),
        '3-5 ' . __('days'),
        '5-7 ' . __('days'),
        '7-15 ' . __('days'),
        '15-21 ' . __('days'),
        '21-30 ' . __('days'),
        '30-60 ' . __('days'),
        '60-90 ' . __('days')
    );
}

/**
 * Gets delivery time value
 *
 * @param int $delivery_time_id Delivery time ID
 * @return array Delivery time list
 */
function fn_sd_shipping_by_product_get_delivery_time_value($delivery_time_id)
{
    $list = fn_sd_shipping_by_product_get_delivery_time_list();

    return !empty($list[$delivery_time_id]) ? $list[$delivery_time_id] : '';
}

/**
 * Hook gets delivery time text value for order products
 *
 */
function fn_sd_shipping_by_product_get_order_info(&$order, $additional_data)
{
    if (!empty($order['products'])) {
        foreach ($order['products'] as $item_id => $product) {
            if (isset($product['extra']['shipping_info']['delivery_time'])) {
                $order['products'][$item_id]['extra']['shipping_info']['delivery_time'] = fn_sd_shipping_by_product_get_delivery_time_value($product['extra']['shipping_info']['delivery_time']);
            }
        }
    }
}

/**
 * Gets delivery time ID by its value
 *
 * @param string $delivery_time Delivery time value
 * @return int Delivery time ID
 */
function fn_sd_shipping_by_product_get_delivery_time_id_by_name($delivery_time)
{
    $list = fn_sd_shipping_by_product_get_delivery_time_list();

    return in_array($delivery_time, $list) ? array_search($delivery_time, $list) : 0;
}

/**
 * Gets simple shipping list with the company condition
 *
 * @param string $lang_code 2-letter language code (e.g. 'en', 'ru', etc.)
 * @return array Shipping list
 */
function fn_sd_shipping_by_product_get_simple_shipping_list($lang_code = CART_LANGUAGE)
{
    $condition = fn_get_company_condition('company_id', true, '', true);
    return db_get_hash_single_array('SELECT a.shipping_id, b.shipping FROM ?:shippings as a LEFT JOIN ?:shipping_descriptions as b ON a.shipping_id = b.shipping_id AND b.lang_code = ?s WHERE status = ?s ?p ORDER BY a.position', array('shipping_id', 'shipping'), $lang_code, 'A', $condition);
}

/**
 * Hook updates product available countries
 *
 */
function fn_sd_shipping_by_product_update_product_pre(&$product_data, $product_id, $lang_code, $can_update)
{
    $product_data['available_countries'] = isset($product_data['available_countries']) ? implode(',', $product_data['available_countries']) : '';
}

/**
 * Hook gets product available countries
 *
 */
function fn_sd_shipping_by_product_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
{
    if (!empty($product_data['available_countries'])) {
        $available_countries = explode(',', $product_data['available_countries']);
        $product_data['available_countries'] = db_get_hash_single_array(
            'SELECT a.code, b.country FROM ?:countries as a '
            . 'LEFT JOIN ?:country_descriptions as b ON b.code = a.code AND b.lang_code = ?s '
            . 'WHERE a.code IN (?a)',
            array('code', 'country'), DESCR_SL, $available_countries
        );
    } else {
        $product_data['available_countries'] = array();
    }
}
