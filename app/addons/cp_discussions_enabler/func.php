<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }


function fn_cp_discussions_enabler_get_discussion($object_id, $object_type, &$discussion) {
    $preset = fn_get_discussions_preset();
    if (!empty($preset) && empty($object_id)) {
        if (!empty($preset['type_' . $object_type])) {
            $discussion['type'] = $preset['type_' . $object_type];
        }
    }  
}

function fn_cp_discussions_enabler_place_order($order_id, $action, $order_status, $cart, $auth) {
   
    $company_id = db_get_field('SELECT company_id FROM ?:orders WHERE order_id=?i', $order_id);
    $preset = fn_get_discussions_preset($company_id);
   
    if (!empty($preset) && $preset['type_O'] != 'D') {
         $discussion = array(
            'object_type' => 'O',
            'object_id' => $order_id,
            'type' => $preset['type_O']
        );

        fn_update_discussion($discussion);
    }  
}

function fn_get_discussions_items() {

    if (fn_allowed_for('MULTIVENDOR') && Registry::get('runtime.company_id')) {
        $items = array('P' => __('products'), 'A' => __('pages'), 'B' => __('blog'), 'O' => __('orders'));
    } else {
        $items = array('P' => __('products'), 'A' => __('pages'), 'B' => __('blog'), 'C' => __('categories'), 'O' => __('orders'));
    }
    
    return $items;
}

function fn_update_discussions_preset($data) {
    
    if ((fn_allowed_for('ULTIMATE') || fn_allowed_for('MULTIVENDOR')) && Registry::get('runtime.company_id')) {
       $company_id = Registry::get('runtime.company_id');
    } else {
       $company_id = 0;
    }

    foreach ($data as $k => $v) {
        $_data['type_'.$k] = $v;
    }
    $_data['company_id'] = $company_id;

    db_query('REPLACE INTO ?:discussions_settings ?e', $_data);    
    
    return true;
    
}

function fn_get_discussions_preset($company_id = 0) {

    if ((fn_allowed_for('ULTIMATE') || fn_allowed_for('MULTIVENDOR')) && Registry::get('runtime.company_id')) {
       $company_id = Registry::get('runtime.company_id');
    } 

    $preset = db_get_row('SELECT * FROM ?:discussions_settings WHERE company_id=?i', $company_id);
    
    return $preset;
    
}

function fn_update_discussions_settings($params) {

    $company_condition = '';
    $company_id = 0;
    
    $_company_id = Registry::get('runtime.company_id');
    
    if (fn_allowed_for('ULTIMATE')) {
        $company_id = !empty($company_id) ?  $company_id : 0;
//         $company_condition  .= db_quote(' AND company_id=?i', $company_id);
    }
    
    if (fn_allowed_for('MULTIVENDOR') && !empty($_company_id)) {
         $company_id = $_company_id;
         $company_condition .= db_quote(' AND company_id=?i', $company_id);
    }
    
    if ($params['item_type'] == 'P') {
    
        $params['only_short_fields'] = true;
        $params['extend'][] = 'companies';

        if (fn_allowed_for('ULTIMATE')) {
            $params['extend'][] = 'sharing';
        }
        
        if ($params['cp_only_empty'] == 'Y') {
            $set_products = db_get_fields('SELECT object_id FROM ?:discussion WHERE object_type=?s AND type=?s ?p', 'P', 'D', $company_condition);
            $products = db_get_fields('SELECT product_id FROM ?:products WHERE product_id IN (?a) ?p', $set_products, $company_condition);
        } else {
            $products = db_get_fields('SELECT product_id FROM ?:products WHERE 1 ?p', $company_condition);
        }    
  
        foreach ($products as $product_id) {
            $discussion = array(
                'object_type' => 'P',
                'object_id' => $product_id,
                'type' => $params['discussion'],
                'company_id' => empty($company_id) ? db_get_field('SELECT company_id FROM ?:products WHERE product_id=?i', $product_id) : $company_id,
            );
            fn_update_discussion($discussion);
        }    
  
    } elseif ($params['item_type'] == 'A') {
    
        if ($params['cp_only_empty'] == 'Y') {
            $set_pages = db_get_fields('SELECT object_id FROM ?:discussion WHERE object_type=?s AND type=?s ?p', 'A', 'D', $company_condition);
            $pages = db_get_fields('SELECT page_id FROM ?:pages WHERE page_id IN (?a) AND page_type <> ?s ?p', $set_pages, 'B', $company_condition);
        } else {
            $pages = db_get_fields('SELECT page_id FROM ?:pages WHERE page_type <> ?s ?p', 'B', $company_condition);
        }
        
        foreach ($pages as $page_id) {
            $discussion = array(
                'object_type' => 'A',
                'object_id' => $page_id,
                'type' => $params['discussion'],
                'for_all_companies' => 1
            );

            fn_update_discussion($discussion);
        }
       
    }  elseif ($params['item_type'] == 'B') {
    
        if ($params['cp_only_empty'] == 'Y') {
            $set_pages = db_get_fields('SELECT object_id FROM ?:discussion WHERE object_type=?s AND type=?s ?p', 'A', 'D', $company_condition);
            $pages = db_get_fields('SELECT page_id FROM ?:pages WHERE page_id IN (?a) AND page_type = ?s ?p', $set_pages, 'B', $company_condition);
        } else {
            $pages = db_get_fields('SELECT page_id FROM ?:pages WHERE page_type = ?s ?p', 'B', $company_condition);
        }
        
        foreach ($pages as $page_id) {
            $discussion = array(
                'object_type' => 'A',
                'object_id' => $page_id,
                'type' => $params['discussion'],
                'for_all_companies' => 1
            );

            fn_update_discussion($discussion);
        }
       
    } elseif ($params['item_type'] == 'C') {
        
        if ($params['cp_only_empty'] == 'Y') {
            $set_categories = db_get_fields('SELECT object_id FROM ?:discussion WHERE object_type=?s AND type=?s ?p', 'C', 'D', $company_condition);
            $category_ids = db_get_fields('SELECT category_id FROM ?:categories WHERE category_id IN (?a) ?p', $set_categories, $company_condition);
        } else {
            $category_ids = db_get_fields('SELECT category_id FROM ?:categories WHERE 1 ?p', $company_condition);
        }
        
        foreach ($category_ids as $category_id) {
            $discussion = array(
                'object_type' => 'C',
                'object_id' => $category_id,
                'type' => $params['discussion']
            );

            fn_update_discussion($discussion);
        }
    } elseif ($params['item_type'] == 'O') {
        if ($params['cp_only_empty'] == 'Y') {
            $set_orders = db_get_fields('SELECT object_id FROM ?:discussion WHERE object_type=?s AND type=?s ?p', 'O', 'D', $company_condition);
            $orders = db_get_fields('SELECT order_id FROM ?:orders WHERE order_id IN (?a) ?p', $set_orders, $company_condition);
        } else {
            $orders = db_get_fields('SELECT order_id FROM ?:orders WHERE 1 ?p', $company_condition);
        }
        
        foreach ($orders as $order_id) {
            $discussion = array(
                'object_type' => 'O',
                'object_id' => $order_id,
                'type' => $params['discussion']
            );

            fn_update_discussion($discussion);
        }
    }   
}