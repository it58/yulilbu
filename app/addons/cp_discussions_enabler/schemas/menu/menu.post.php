<?php

$schema['central']['website']['items']['cp_discussions_enabler'] = array(
    'attrs' => array(
        'class' => 'is-addon'
    ),
    'href' => 'discussions_enabler.manage',
    'position' => 1000
);

return $schema;
