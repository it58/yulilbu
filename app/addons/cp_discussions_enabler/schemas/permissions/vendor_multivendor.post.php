<?php

$schema['controllers']['discussions_enabler']['permissions'] = true;
$schema['controllers']['discussions_enabler']['modes'] = array (
    'update' => array(
        'permissions' => array ('GET' => true, 'POST' => true),
    ),
    'manage' => array(
        'permissions' => true,
    ),
);

return $schema;

