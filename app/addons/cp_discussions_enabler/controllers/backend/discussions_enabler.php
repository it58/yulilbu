<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'save_preset') {
        if (!empty($_REQUEST['discussion_preset'])) {
            fn_update_discussions_preset($_REQUEST['discussion_preset']);
        }
    }
    if ($mode == 'mass_update') {
        if (!empty($_REQUEST['discussions_settings'])) {
            fn_update_discussions_settings($_REQUEST['discussions_settings']);
        }
    }
    
    return array(CONTROLLER_STATUS_OK, "discussions_enabler.manage");
}

if ($mode == 'manage') {
    $discussions_preset = fn_get_discussions_preset();
    if (!empty($discussions_preset)) {
        foreach ($discussions_preset as $k => $v) {
            $key = str_replace('type_', '', $k);
            $_preset[$key] = $v;
        }

        Registry::get('view')->assign('discussions_preset', $_preset);
    }
}
