<?php

if(!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Storage;

if (empty($_SESSION['auth']['user_id'])) {
    $url = !empty($_REQUEST['return_url']) ? $_REQUEST['return_url'] : Registry::get('config.current_url');
    if(defined('AJAX_REQUEST')) {
        Tygh::$app['ajax']->assign('force_redirection', fn_url("auth.login_form?return_url=" . urlencode($url)));
    }
    return array(CONTROLLER_STATUS_REDIRECT, fn_url("auth.login_form?return_url=" . urlencode($url)));
}

$author_id = $_SESSION['auth']['user_id'];
$params = $_REQUEST;

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if($mode == 'send_new_message') {
        if(empty($params['conversation_id'])) {
            //create new conversation
            $data = array(
                'timestamp' => time(),
                'author_id' => $author_id,
                'subject' => $params['conversation_data']['subject']
            );
            $params['conversation_id'] = db_query("INSERT INTO ?:cp_conversations ?e", $data);

            //assign selected users to created conversation
            foreach(array($author_id, $params['conversation_data']['recipient_id']) as $_id) {
                $data = array(
                    'conversation_id' => $params['conversation_id'],
                    'recipient_id' => $_id
                );
                db_query("REPLACE INTO ?:cp_conversation_users ?e", $data);
            }
        }

        //create new message and assign it to selected conversation
        $data = array(
            'conversation_id' => $params['conversation_id'],
            'user_id' => $author_id,
            'message' => $params['conversation_data']['message'],
            'timestamp' => time()
        );
        $message_id = db_query("INSERT INTO ?:cp_messages ?e", $data);

        //attach images or files
        if(!empty($message_id)) {
            if(AREA == 'C') {
                if(!empty($_FILES['message_files'])) {
                    $files = array();
                    foreach($_FILES['message_files']['tmp_name'] as $k => $file) {
                        $files[] = array(
                            'path' => $file,
                            'name' => $_FILES['message_files']['name'][$k]
                        );
                    }
                }

            } else {
                $files = fn_filter_uploaded_data('message_files');
            }
            if(!empty($files)) {
                foreach($files as $file) {
                    list($filesize, $filename) = Storage::instance('messages_files')->put($file['name'], array(
                        'file' => $file['path']
                    ));
                    $udata = array(
                        'message_id' => $message_id,
                        'filename' => $filename
                    );
                    db_query("INSERT INTO ?:cp_conversation_message_files ?e", $udata);
                }
            }

            //send email fo all users assigned t this conversation
            $user_ids = db_get_fields("SELECT recipient_id FROM ?:cp_conversation_users WHERE conversation_id = ?i AND `read` = ?s AND recipient_id != ?i", $params['conversation_id'], 'Y', $author_id);
            if(!empty($user_ids)) {
                foreach($user_ids as $user_id) {
                    $user_data = db_get_row("SELECT email, company_id FROM ?:users WHERE user_id = ?i AND user_type = ?s", $user_id, 'C');
                    if(empty($user_data)) {
                        continue;
                    }
                    $ekey = fn_cp_conversations_generate_ekey();
                    $udata = array(
                        'ekey' => $ekey
                    );
                    db_query("UPDATE ?:cp_conversation_users SET ?u WHERE conversation_id = ?i AND recipient_id = ?i", $udata, $params['conversation_id'], $user_id);
                    $mailer = Tygh::$app['mailer'];
                    $link = fn_url("conversations.answer&ekey=$ekey&user_id=$user_id&conversation_id=$params[conversation_id]", 'C');
                    $mailer->send(array(
                        'to' => $user_data['email'],
                        'from' => 'default_company_orders_department',
                        'data' => array(
                            'mess' => __('use_following_link_to_check_your_inbox'),
                            'link' => $link
                        ),
                        'tpl' => 'addons/cp_conversations/new_message.tpl',
                        'company_id' => $user_data['company_id'],
                    ), 'C', CART_LANGUAGE);
                }
            }

            //mark this conversation as unread for all users but author
            $udata = array('read' => 'N');
            db_query("UPDATE ?:cp_conversation_users SET ?u WHERE conversation_id = ?i AND recipient_id != ?i", $udata, $params['conversation_id'], $author_id);
            $udata = array('read' => 'Y');
            db_query("UPDATE ?:cp_conversation_users SET ?u WHERE conversation_id = ?i AND recipient_id = ?i", $udata, $params['conversation_id'], $author_id);

            fn_set_notification('N', __('notice'), __('message_has_been_sent'));
        }
        $_mode = AREA == 'C' ? 'list' : 'update';
        return array(CONTROLLER_STATUS_OK, fn_url("conversations.$_mode&conversation_id=$params[conversation_id]"));

    } elseif($mode == 'mass_update') {
        if(!empty($params['conversation_ids'])) {
            if($action == 'move_to_spam') {
                //unmark all selected conversations before moving
                $udata = array('spam' => 'N', 'archive' => 'N', 'trash'=> 'N');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                //delete selected conversations from all folders
                db_query("DELETE FROM ?:cp_conversation_folder_links WHERE user_id = ?i AND conversation_id IN (?n)", $author_id, $params['conversation_id']);
                //move to spam
                $udata = array('spam' => 'Y');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                fn_set_notification('N', __('notice'), __('marked_as_spam'));

            } elseif($action == 'move_to_archive') {
                //unmark all selected conversations before moving
                $udata = array('spam' => 'N', 'archive' => 'N', 'trash'=> 'N');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                //move to spam
                $udata = array('archive' => 'Y');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                fn_set_notification('N', __('notice'), __('successfully_archived'));

            } elseif($action == 'move_to_trash') {
                //unmark all selected conversations before moving
                $udata = array('spam' => 'N', 'archive' => 'N', 'trash'=> 'N');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                //remove selected conversations from all folders
                db_query("DELETE FROM ?:cp_conversation_folder_links WHERE user_id = ?i AND conversation_id IN (?n)", $author_id, $params['conversation_ids']);
                //move to spam
                $udata = array('trash' => 'Y');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                fn_set_notification('N', __('notice'), __('successfully_moved_to_trash'));

            } elseif($action == 'remove_from_folder') {
                //remove selected conversations from folder
                if(!empty($dispatch_extra)) {
                    $folder_id = $dispatch_extra;
                    db_query("DELETE FROM ?:cp_conversation_folder_links WHERE folder_id = ?i AND user_id = ?i AND conversation_id IN (?n)", $folder_id, $author_id, $params['conversation_ids']);
                    fn_set_notification('N', __('notice'), __('deleted_from_folder'));
                }

            } elseif($action == 'move_to_inbox') {
                //unmark all selected conversations before moving
                $udata = array('spam' => 'N', 'archive' => 'N', 'trash'=> 'N');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                fn_set_notification('N', __('notice'), __('successfully_moved_to_inbox'));

            } elseif($action == 'mark_as_unread') {
                //unmark all selected conversations before moving
                $udata = array('read' => 'N');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                fn_set_notification('N', __('notice'), __('marked_as_unread'));

            } elseif($action == 'move_to_folder' || $action == 'add_to_folder') {
                if(!empty($params['folder_ids']) || !empty($params['new_folder'])) {
                    $folder_ids = !empty($params['folder_ids']) ? $params['folder_ids'] : array();
                    if(!empty($params['new_folder'])) {
                        //create new folder and move selected conersations
                        $udata = array(
                            'folder' => $params['new_folder'],
                            'user_id' => $author_id
                        );
                        $folder_ids[] = db_query("INSERT INTO ?:cp_conversation_folders ?e", $udata);
                    }
                    if($action == 'move_to_folder') {
                        //if we moving conversations (not adding) we should add it to archive, so it will disapear from inbox
                        $udata = array('archive' => 'Y');
                        db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN (?n)", $udata, $author_id, $params['conversation_ids']);
                    }
                    foreach($params['conversation_ids'] as $conversation_id) {
                        //move converations into folders
                        foreach($folder_ids as $folder_id) {
                            $udata = array(
                                'folder_id' => $folder_id,
                                'conversation_id' => $conversation_id,
                                'user_id' => $author_id
                            );
                            db_query("INSERT INTO ?:cp_conversation_folder_links ?e", $udata);
                        }
                    }
                    fn_set_notification('N', __('notice'), __('conversations_successfully_moved'));
                }

            } elseif($action == 'mark_as_read') {
                $udata = array('read' => 'Y');
                db_query("UPDATE ?:cp_conversation_users SET ?u WHERE recipient_id = ?i AND conversation_id IN(?n)", $udata, $author_id, $params['conversation_ids']);
                fn_set_notification('N', __('notice'), __('marked_as_read'));

            } elseif($action == 'delete') {
                foreach($params['conversation_ids'] as $id) {
                    fn_cp_conversations_delete_conversation($id);
                    fn_set_notification('N', __('notice'), __('conversation_has_been_deleted'));
                }
            }

        } elseif($action == 'delete_folder') {
            if(!empty($params['folder_id'])) {
                db_query("DELETE FROM ?:cp_conversation_folders WHERE folder_id = ?i", $params['folder_id']);
                db_query("DELETE FROM ?:cp_conversation_folder_links WHERE folder_id = ?i", $params['folder_id']);
                fn_set_notification('N', __('notice'), __('folder_has_been_deleted'));
            }
            $_mode = AREA == 'C' ? 'list' : 'manage';
            return array(CONTROLLER_STATUS_OK, fn_url($url));
        }
        if(AREA == 'C') {
            if(!empty($params['redirect_url'])) {
                $url = $params['redirect_url'];

            } elseif(!empty($params['folder']) || !empty($params['folder_id'])) {
                $url = 'conversations.list';
                !empty($params['folder']) && $url .= "&folder=$params[folder]";
                !empty($params['folder_id']) && $url .= "&folder_id=$params[folder_id]";

            } elseif(!empty($params['conversation_id'])) {
                $url = "conversations.view&conversation_id=$params[conversation_id]";

            } else {
                $url = "conversations.list";
            }
        } else {
            $url = "conversations.manage";
        }
        return array(CONTROLLER_STATUS_OK, fn_url($url));
    }
} 

if($mode == 'new') {
    $result = array();
    if(!empty($params['q'])) {
        $q = $params['q'];
        if(AREA == 'A') {
            $condition = db_quote("(firstname LIKE ?l OR lastname LIKE ?l)", "%$q%", "%$q%");
            $setting = Registry::get('addons.cp_conversations.allow_conversations_with_admin');
            if($setting == 'N') {
                $condition .= db_quote(" AND user_type != ?s", 'A');
            }
            $result['recipients'] = db_get_array("SELECT CONCAT_WS(' ', firstname, lastname) as name, user_id as object_id, 'U' as object_type FROM ?:users
                WHERE $condition LIMIT 0, 5");

        } else {
            $result['recipients'] = db_get_array("SELECT company as name, company_id as object_id, 'C' as object_type FROM ?:companies 
                WHERE company LIKE ?l LIMIT 0, 5", "%$q%");
        }

    } elseif(!empty($params['recipient_id']) || !empty($params['admin_message'])) {
        $result['recipient_id'] = $params['recipient_id'];
        if(AREA == 'A') {
            $result['recipient_name'] = db_get_field("SELECT CONCAT_WS(' ', firstname, lastname) FROM ?:users WHERE user_id = ?i", $params['recipient_id']);
            $condition = '';
            $company_id = Registry::get('runtime.company_id');

            if(!empty($company_id)) {
                //vendor gets only his own conversations
                $my_conversations = db_get_fields("SELECT conversation_id FROM ?:cp_conversation_users WHERE recipient_id = ?i", $author_id);
                $condition .= db_quote(" AND ?:cp_conversations.conversation_id IN (?n)", $my_conversations);
            }
            $result['conversations'] = db_get_hash_array("SELECT ?:cp_conversations.conversation_id, subject FROM ?:cp_conversations 
                LEFT JOIN ?:cp_conversation_users ON ?:cp_conversations.conversation_id = ?:cp_conversation_users.conversation_id
                WHERE recipient_id = ?i $condition", 'conversation_id', $params['recipient_id']);

        } else {
            $my_conversations = db_get_fields("SELECT conversation_id FROM ?:cp_conversation_users WHERE recipient_id = ?i", $author_id);
            if(!empty($params['admin_message'])) {
                $result['recipient_id'] = db_get_field("SELECT user_id FROM ?:users WHERE company_id = ?i AND is_root = ?s AND user_type = ?s", 0, 'Y', 'A');
                $result['recipient_name'] = fn_get_user_name($result['recipient_id']);
                $result['conversations'] = db_get_hash_array("SELECT ?:cp_conversations.conversation_id, subject FROM ?:cp_conversations 
                    LEFT JOIN ?:cp_conversation_users ON ?:cp_conversations.conversation_id = ?:cp_conversation_users.conversation_id
                    WHERE recipient_id = ?i AND ?:cp_conversations.conversation_id IN (?n)", 'conversation_id', $result['recipient_id'], $my_conversations);

            } else {
                if(empty($params['conversation_id'])) {
                    $result['recipient_id'] = db_get_field("SELECT user_id FROM ?:users WHERE company_id = ?i AND is_root = ?s", $params['recipient_id'], 'Y');
                }
                $result['recipient_name'] = db_get_field("SELECT company FROM ?:companies WHERE company_id = ?i", $params['recipient_id']);
                $recipients = db_get_fields("SELECT user_id FROM ?:users WHERE company_id = ?i AND user_type = ?s", $params['recipient_id'], 'V');
                
                $result['conversations'] = db_get_hash_array("SELECT ?:cp_conversations.conversation_id, subject FROM ?:cp_conversations 
                    LEFT JOIN ?:cp_conversation_users ON ?:cp_conversations.conversation_id = ?:cp_conversation_users.conversation_id
                    WHERE recipient_id IN (?n) AND ?:cp_conversations.conversation_id IN (?n)", 'conversation_id', $recipients, $my_conversations);
            }
        }
    }
    if(!empty($params['conversation_id'])) {
        $result['conversation_data'] = fn_cp_conversations_get_conversation_data($params['conversation_id']);
    }

    Registry::get('view')->assign('search_result', $result);
    Registry::get('view')->display('addons/cp_conversations/components/new_conversation.tpl');
    exit;

}