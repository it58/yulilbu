<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

defined('BOOTSTRAP') or die('Access denied');

<<<<<<< HEAD
use Tygh\BlockManager\Block;
use Tygh\BlockManager\ProductTabs;
use Tygh\Registry;
use Tygh\Addons\MasterProducts\ServiceProvider;
use Tygh\Addons\ProductVariations\ServiceProvider as VariationsServiceProvider;
use Tygh\Addons\ProductVariations\Product\Group\Events\ProductAddedEvent;
use Tygh\Addons\ProductVariations\Product\Group\GroupFeatureCollection;
=======
use Tygh\Addons\MasterProducts\Product\AdditionalDataLoader;
use Tygh\BlockManager\Block;
use Tygh\BlockManager\ProductTabs;
use Tygh\Enum\ProductTracking;
use Tygh\Registry;
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

/**
 * Installs the add-on products block and the product tab.
 */
function fn_master_products_install()
{
    $company_ids = [0];

    /** @var \Tygh\BlockManager\Block $block */
    $block = Block::instance();
    /** @var ProductTabs $product_tabs */
    $product_tabs = ProductTabs::instance();

    foreach ($company_ids as $company_id) {
        $block_data = [
            'type'         => 'products',
            'properties'   => [
                'template' => 'addons/master_products/blocks/products/vendor_products.tpl',
            ],
            'content_data' => [
                'content' => [
                    'items' => [
                        'filling' => 'master_products.vendor_products_filling',
                        'limit'   => '0',
                    ],
                ],
            ],
            'company_id'   => $company_id,
        ];

        $block_description = [
            'lang_code' => DEFAULT_LANGUAGE,
            'name'      => __('master_products.vendor_products_block_name', [], DEFAULT_LANGUAGE),
        ];

        $block_id = $block->update($block_data, $block_description);

        $tab_data = [
            'tab_type'      => 'B',
            'block_id'      => $block_id,
            'template'      => '',
            'addon'         => 'master_products',
            'status'        => 'A',
            'is_primary'    => 'N',
            'position'      => 0,
            'product_ids'   => null,
            'company_id'    => $company_id,
            'show_in_popup' => 'N',
            'lang_code'     => DEFAULT_LANGUAGE,
            'name'          => __('master_products.vendor_products_tab_name', [], DEFAULT_LANGUAGE),
        ];

        $product_tabs->update($tab_data);
    }
}

/**
 * Hook handler: adds extra products search parameters.
 *
 * @param array  $params         Products search params
 * @param int    $items_per_page Amount of products shown per page
 * @param string $lang_code      Two-letter language code for product descriptions
 */
function fn_master_products_get_products_pre(&$params, $items_per_page, $lang_code)
{
    $params = array_merge([
<<<<<<< HEAD
=======
        'merge_with_master_products' => null,
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
        'show_master_products_only'  => false,
        'area'                       => AREA,
    ], $params);

<<<<<<< HEAD
    $params['runtime_company_id'] = (int) Registry::ifGet('runtime.vendor_id', Registry::get('runtime.company_id'));
=======
    $params['runtime_company_id'] = Registry::get('runtime.company_id');

    if ($params['merge_with_master_products'] === null) {
        $params['merge_with_master_products'] = !$params['show_master_products_only'] && (
            $params['area'] === 'C' ||
            $params['runtime_company_id']
        );
    }

    // replace product ID of the parent configurable product with the variation product ID
    if (!empty($params['is_vendor_products_list'])) {
        $product_id = reset($params['parent_product_id']);

        /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
        $product_manager = Tygh::$app['addons.product_variations.product.manager'];

        $product_type = $product_manager->getProductFieldValue($product_id, 'product_type');
        if ($product_type === $product_manager::PRODUCT_TYPE_CONFIGURABLE) {
            if (!empty($params['product_data'][$product_id]['product_options'])) {
                $selected_options = $params['product_data'][$product_id]['product_options'];
                $variation_id = $product_manager->getVariationId($product_id, $selected_options);
                $params['parent_product_id'] = $variation_id;
            } else {
                $params['parent_product_id'] = [$product_manager->getDefaultVariationId($product_id)];
            }
        }
    }
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

    // vendors must see only active master products
    if ($params['show_master_products_only'] && $params['runtime_company_id']) {
        $params['status'] = 'A';
    }
}

/**
 * Hook handler: modifies products obtaining process to include vendor products into the list.
 */
function fn_master_products_get_products(
    &$params,
    &$fields,
    $sortings,
    &$condition,
    &$join,
    $sorting,
    &$group_by,
    $lang_code,
    $having
) {
<<<<<<< HEAD
    $condition_replacements = [];

    if ($params['area'] === 'C') {
        $condition .= db_quote(' AND products.master_product_status IN (?a)', ['A']);

        $search = db_quote('AND companies.status = ?s', 'A');
        $replace = db_quote('AND (companies.status = ?s OR products.company_id = ?i)', 'A', 0);
        $condition_replacements[$search] = $replace;

        if (!empty($params['vendor_products_by_product_id'])) {
            $repository = ServiceProvider::getProductRepository();

            $master_product_id = $repository->findMasterProductId($params['vendor_products_by_product_id']);

            if (!$master_product_id) {
                $master_product_id = (int) $params['vendor_products_by_product_id'];
            }

            $condition .= db_quote(' AND products.master_product_id = ?i', $master_product_id);
        } elseif (empty($params['runtime_company_id'])) {
            $condition .= db_quote(' AND products.master_product_id = ?i AND (products.company_id > 0 OR products.master_product_offers_count > 0)', 0);
        }
=======
    $parent_product_id = null;
    if (isset($params['parent_product_id'])) {
        $parent_product_id = array_filter((array) $params['parent_product_id']);
    }

    // FIXME: Dirty hack
    if ($params['merge_with_master_products']) {
        $fields['product_id'] = '(CASE'
            . ' WHEN master_products.product_id <> 0 THEN master_products.product_id'
            . ' ELSE products.product_id'
            . ' END) AS product_id';
    }

    $fields['is_mergeable_with_master_product'] = db_quote(
        '?s AS is_mergeable_with_master_product',
        $params['merge_with_master_products'] ? 'Y' : 'N'
    );

    $fields['vendor_product_id'] = 'products.product_id AS vendor_product_id';
    $fields['master_product_id'] = 'master_products.product_id AS master_product_id';

    $join = db_quote(' LEFT JOIN ?:products AS master_products ON master_products.product_id = products.parent_product_id'
            . ' AND master_products.product_type = products.product_type'
        ) . $join;

    if ($params['merge_with_master_products']) {
        $group_by = 'product_id';
    }

    $condition_replacements = [];

    if ($parent_product_id && $params['merge_with_master_products']) {
        if (is_array($params['parent_product_id'])) {
            $parent_product_id_condition = db_quote('IN (?n)', $params['parent_product_id']);
        } else {
            $parent_product_id_condition = db_quote('= ?i', $params['parent_product_id']);
        }

        $search = db_quote('products.parent_product_id ?p', $parent_product_id_condition);
        $replace = db_quote(
            '(products.parent_product_id ?p OR master_products.parent_product_id ?p)',
            $parent_product_id_condition,
            $parent_product_id_condition
        );

        $condition_replacements[$search] = $replace;
    }

    if ($params['product_type'] && $params['merge_with_master_products']) {
        $search = db_quote('AND products.product_type IN (?a)', $params['product_type']);
        $replace = db_quote(
            'AND (products.product_type IN (?a) OR master_products.product_type IN (?a))',
            $params['product_type'],
            $params['product_type']
        );
        $condition_replacements[$search] = $replace;
    }

    if (!empty($params['pid'])) {
        $search = db_quote('AND companies.status = ?s', 'A');
        $replace = db_quote('AND (companies.status = ?s OR products.company_id = ?i)', 'A', 0);
        $condition_replacements[$search] = $replace;
    }

    if ($params['area'] === 'C') {
        $search = db_quote('AND products.status IN (?a)', ['A']);
        $replace = db_quote(
            'AND products.status IN (?a) AND (master_products.status IN (?a) OR master_products.status IS NULL)',
            ['A'],
            ['A']
        );
        $condition_replacements[$search] = $replace;
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    }

    if ($params['show_master_products_only']) {
        if ($params['runtime_company_id']) {
            $search = db_quote(' AND products.company_id = ?i', $params['runtime_company_id']);
            $replace = db_quote(' AND products.company_id = ?i', 0);
            $condition_replacements[$search] = $replace;
        } else {
            $condition .= db_quote(' AND products.company_id = ?i', 0);
        }
<<<<<<< HEAD
    } elseif ($params['area'] === 'A'
        && empty($params['pid'])
        && empty($params['show_all_products'])
        && empty($params['has_not_variation_group'])
        && !isset($params['master_product_id'])
        && !isset($params['parent_product_id'])
    ) {
        $condition .= db_quote(' AND products.company_id <> ?i ', 0);
    }

    if (!empty($params['remove_company_condition'])) {
        $search = db_quote(' AND products.company_id = ?i', $params['runtime_company_id']);
        $condition_replacements[$search] = '';
    }

    if (!empty($params['master_product_id'])) {
        if (is_array($params['master_product_id'])) {
            $condition .= db_quote(' AND products.master_product_id IN (?n)', $params['master_product_id']);
        } else {
            $condition .= db_quote(' AND products.master_product_id = ?i', $params['master_product_id']);
        }
    }

    // FIXME: Dirty hack
    if ($condition_replacements) {
        $condition = strtr($condition, $condition_replacements);
    }

    $fields['master_product_id'] = 'products.master_product_id';
    $fields['company_id'] = 'products.company_id';
=======
    } elseif ($params['area'] === 'A' && !$parent_product_id) {
        $condition .= db_quote(' AND products.company_id <> ?i ', 0);
    }

    // FIXME: Dirty hack
    $condition = strtr($condition, $condition_replacements);

    return;
}

/**
 * Hook handler: adds master product ID into a list of fetched product fields.
 */
function fn_master_products_get_product_data($product_id, &$field_list, &$join, $auth, $lang_code, $condition)
{
    $join = db_quote(' LEFT JOIN ?:products AS master_products ON master_products.product_id = ?:products.parent_product_id'
            . ' AND master_products.product_type = ?:products.product_type'
        ) . $join;

    $field_list .= ', master_products.product_id AS master_product_id';
}

/**
 * Hook handler: modifies seo name and stock information.
 */
function fn_master_products_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
{
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    if (!$product_manager->getVendorProduct($product_data['product_id'])) {
        return;
    }

    /** @var \Tygh\Addons\MasterProducts\Product\Type $product_type */
    $product_type = $product_manager->getProductTypeInstance($product_data['product_type']);

    $product_data['is_vendor_product'] = true;
    // Skip creating seo name
    $product_data['seo_name'] = $product_data['product_id'];

    if (isset($product_data['image_pairs']) && $product_type->isFieldMergeableForVendorProduct('additional_images')) {
        $product_data['image_pairs'] = fn_get_image_pairs($product_data['master_product_id'], 'product', 'A', true, true, $lang_code);
    }

    if (isset($product_data['main_pair']) && $product_type->isFieldMergeableForVendorProduct('detailed_image')) {
        $product_data['main_pair'] = fn_get_image_pairs($product_data['master_product_id'], 'product', 'M', true, true, $lang_code);
    }
}

/**
 * Hook handler: prepares environment to inject vendor data into a product data.
 */
function fn_master_products_gather_additional_products_data_params(
    $product_ids,
    $params,
    &$products,
    $auth,
    &$products_images,
    &$additional_images,
    $product_options,
    $has_product_options,
    $has_product_options_links
) {
    $loader = new AdditionalDataLoader(
        $products,
        $params,
        $auth,
        CART_LANGUAGE,
        Tygh::$app['addons.product_variations.product.manager'],
        Tygh::$app['db']
    );

    $runtime_company_id = fn_master_products_get_runtime_company_id();

    $loader->setCompanyId($runtime_company_id);

    Registry::set('master_products_loader', $loader);
}

/**
 * Hook handler: injects vendor product data into a product data.
 */
function fn_master_products_gather_additional_product_data_before_options(&$product, $auth, &$params)
{
    /** @var AdditionalDataLoader $loader */
    $loader = Registry::get('master_products_loader');

    $product = $loader->loadBaseData($product);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

    return;
}

<<<<<<< HEAD
function fn_master_products_gather_additional_products_data_params($product_ids, &$params, &$products, $auth)
{
    if (!isset($params['get_vendor_products'])) {
        $params['get_vendor_products'] = false;
    }

    if (!isset($params['get_vendor_product_ids'])) {
        $params['get_vendor_product_ids'] = false;
    }

    if (!isset($params['runtime_company_id'])) {
        $params['runtime_company_id'] = (int) Registry::ifGet('runtime.vendor_id', Registry::get('runtime.company_id'));
    }

    if ($params['get_vendor_products'] && !empty($params['runtime_company_id'])) {
        list($vendor_products) = fn_get_products([
            'master_product_id' => $product_ids,
            'company_id'        => $params['runtime_company_id']
        ]);

        $product_id_map = array_column($vendor_products, 'product_id', 'master_product_id');

        foreach ($products as &$product) {
            $product_id = $product['product_id'];
            $vendor_product_id = isset($product_id_map[$product_id]) ? (int) $product_id_map[$product_id] : null;

            $product['vendor_product'] = isset($vendor_products[$vendor_product_id]) ? $vendor_products[$vendor_product_id] : null;
            $product['vendor_product_id'] = $vendor_product_id;
        }
        unset($product);
    }

    if ($params['get_vendor_product_ids'] && !empty($params['runtime_company_id'])) {
        $repository = ServiceProvider::getProductRepository();

        $product_id_map = $repository->findVendorProductIdsByMasterProductIds($product_ids, $params['runtime_company_id']);

        foreach ($products as &$product) {
            $product_id = $product['product_id'];
            $product['vendor_product_id'] = isset($product_id_map[$product_id]) ? (int) $product_id_map[$product_id] : null;
        }
        unset($product);
    }
}

function fn_master_products_gather_additional_products_data_post($product_ids, $params, $products, $auth, $lang_code)
{
    if (AREA !== 'C') {
        return;
    }

    $vendor_id = fn_master_products_get_runtime_company_id();

    if (empty($vendor_id)) {
        return;
    }

    $repository = VariationsServiceProvider::getProductRepository();
    $product_id_map = VariationsServiceProvider::getProductIdMap();

    foreach ($products as &$product) {
        if (empty($product['company_id'])
            || empty($product['master_product_id'])
            || empty($product['detailed_params']['info_type'])
            || $product['detailed_params']['info_type'] !== 'D'
        ) {
            continue;
        }

        $master_product_id = $product['master_product_id'];

        if (!$product_id_map->isChildProduct($master_product_id) && !$product_id_map->isParentProduct($master_product_id)) {
            continue;
        }

        $master_product = $repository->findProduct($master_product_id);

        if (!$master_product) {
            continue;
        }

        $master_product = $repository->loadProductGroupInfo($master_product);
        $master_product = $repository->loadProductFeaturesVariants($master_product);

        if (empty($master_product['variation_features_variants'])) {
            continue;
        }

        $product['variation_features_variants'] = $master_product['variation_features_variants'];
        $master_product_ids = [];

        foreach ($product['variation_features_variants'] as $features) {
            if (empty($features['variants'])) {
                continue;
            }

            foreach ($features['variants'] as $variant) {
                if (empty($variant['product'])) {
                    continue;
                }

                $master_product_ids[$variant['product']['product_id']] = $variant['product']['product_id'];
            }
        }

        if (!$master_product_ids) {
            continue;
        }

        $vendor_products_map = ServiceProvider::getProductRepository()->findVendorProductIdsByMasterProductIds($master_product_ids, $vendor_id, ['A']);

        if (empty($vendor_products_map)) {
            continue;
        }

        foreach ($product['variation_features_variants'] as &$features) {
            if (empty($features['variants'])) {
                continue;
            }

            foreach ($features['variants'] as &$variant) {
                if (empty($variant['product'])) {
                    continue;
                }

                $variant_product_id = $variant['product']['product_id'];

                if (empty($vendor_products_map[$variant_product_id])) {
                    continue;
                }

                $variant['product']['product_id'] = $vendor_products_map[$variant_product_id];
            }
            unset($variant);
        }
        unset($features);
    }
    unset($product);
}

/**
 * Hook handler: adds master product ID into a list of fetched product fields.
 */
function fn_master_products_get_product_data($product_id, &$field_list, &$join, $auth, $lang_code, $condition)
{
    $field_list .= ', ?:products.master_product_id, ?:products.master_product_status';
=======
/**
 * Hook handler: adds icon of the master product when adding a product to the cart.
 */
function fn_master_products_get_cart_product_icon(&$product_id, $product_data, $selected_options, &$image)
{
    if ($image === null) {
        /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
        $product_manager = Tygh::$app['addons.product_variations.product.manager'];

        $vendor_product = $product_manager->getVendorProduct($product_id);
        if ($vendor_product) {
            $product_id = $vendor_product['master_product_id'];
        }
    }
}

/**
 * Hook handler: adds master product categories.
 */
function fn_master_products_pre_get_cart_product_data(
    $hash,
    $product,
    $skip_promotion,
    $cart,
    $auth,
    $promotion_amount,
    $fields,
    &$join
) {
    Registry::set('master_products.active_product', $product, true);
}

/**
 * Hook handler: adds company ID and vendor product ID into cart ID calculation.
 */
function fn_master_products_generate_cart_id(&$_cid, $extra, $only_selectable)
{
    if (!empty($extra['vendor_product_id'])) {
        $_cid[] = $extra['vendor_product_id'];
    }
}

/**
 * Hook handler: sets company ID for a vendor product.
 */
function fn_master_products_add_to_cart(&$cart, $product_id, $_id)
{
    if (isset($cart['products'][$_id]['extra']['company_id'])) {
        $cart['products'][$_id]['company_id'] = $cart['products'][$_id]['extra']['company_id'];
    }
}

/**
 * Hook handler: overrides variation price.
 */
function fn_master_products_add_product_to_cart_get_price(
    $product_data,
    $cart,
    $auth,
    $update,
    $_id,
    &$data,
    $product_id,
    $amount,
    &$price,
    $zero_price_action,
    $allow_add
) {
    if (isset($data['extra']['vendor_product_id'])) {
        $vendor_product_id = $data['extra']['vendor_product_id'];

        if (isset($data['extra']['variation_product_id'])) {
            $data['extra']['variation_product_id'] = $vendor_product_id;
        }

        $price = fn_get_product_price($vendor_product_id, $amount, $auth);
    }
}

/**
 * Hook handler: overrides variation stock information.
 * FIXME: Remove code duplication
 *
 * @see fn_product_variations_check_amount_in_stock_before_check
 */
function fn_master_products_check_amount_in_stock_before_check(
    $product_id,
    $amount,
    $product_options,
    $cart_id,
    $is_edp,
    $original_amount,
    $cart,
    $update_id,
    &$product,
    &$current_amount
) {
    if (
        (isset($product['tracking'])
            && $product['tracking'] === ProductTracking::DO_NOT_TRACK)
        || Registry::get('settings.General.inventory_tracking') == 'N'
    ) {
        return;
    }

    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    $product_type = $product_manager->getProductFieldValue($product_id, 'product_type');

    $product_type_instance = $product_manager->getProductTypeInstance($product_type);

    $active_product = Registry::get('master_products.active_product');

    $company_id = fn_master_products_get_runtime_company_id($active_product);
    if (!$company_id) {
        return;
    }

    $target_product_id = $product_id;
    if ($product_type === $product_manager::PRODUCT_TYPE_CONFIGURABLE) {
        $target_product_id = $product_manager->getVariationId($product_id, $product_options);
    }

    $vendor_product_id = $product_manager->getVendorProductId($target_product_id, $company_id);
    if ($vendor_product_id) {
        $current_amount = $product_manager->getProductFieldValue($vendor_product_id, 'amount');
        $avail_since = $product_manager->getProductFieldValue($vendor_product_id, 'avail_since');

        if (!empty($avail_since) && TIME < $avail_since) {
            $current_amount = 0;
        }

        foreach ($product as $key => $value) {
            if ($product_type_instance->isFieldMergeable($key)) {
                $product[$key] = $product_manager->getProductFieldValue($vendor_product_id, $key);
            }
        }
    }

    if ($product_type === $product_manager::PRODUCT_TYPE_CONFIGURABLE) {
        if (!empty($cart['products']) && is_array($cart['products'])) {
            foreach ($cart['products'] as $key => $item) {
                if ($key != $cart_id && $item['product_id'] == $product_id) {
                    if (isset($item['extra']['variation_product_id'])) {
                        $item_variation_id = $item['extra']['variation_product_id'];
                    } else {
                        $item_variation_id = $product_manager->getVariationId($product_id, $item['product_options']);
                    }

                    if ($item_variation_id == $target_product_id) {
                        $current_amount -= $item['amount'];
                    }
                }
            }
        }
    }
}

/**
 * Hook handler: overrides master product code.
 *
 * @see fn_product_variations_get_product_code
 */
function fn_master_products_get_product_code($product_id, $selected_options, &$product_code)
{
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    $active_product = Registry::get('master_products.active_product');

    $company_id = fn_master_products_get_runtime_company_id($active_product);
    if (!$company_id) {
        return;
    }

    $product_type = $product_manager->getProductFieldValue($product_id, 'product_type');

    /** @var \Tygh\Addons\MasterProducts\Product\Type $product_type_instance */
    $product_type_instance = $product_manager->getProductTypeInstance($product_type);

    if (!$product_type_instance->isFieldAvailableForVendorProduct('product_code')) {
        return;
    }

    $master_product_id = $product_id;
    if ($product_type === $product_manager::PRODUCT_TYPE_CONFIGURABLE) {
        $master_product_id = $product_manager->getVariationId($product_id, $selected_options);
    }

    if ($master_product_id) {
        $vendor_product_id = $product_manager->getVendorProductId($master_product_id, $company_id);
        if ($vendor_product_id) {
            $product_code = $product_manager->getProductFieldValue($vendor_product_id, 'product_code');
        }
    }
}

/**
 * Hook handler: overrides master product data.
 *
 * @see fn_product_variations_get_cart_product_data
 */
function fn_master_products_get_cart_product_data($product_id, &$_pdata, $product, $auth, &$cart, $hash)
{
    $cart['products'][$hash]['product_type'] = $_pdata['product_type'];

    $_pdata['company_id'] = $product['company_id'];

    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    if (empty($product['extra']['vendor_product_id'])) {
        return;
    }

    $amount = !empty($product['amount_total']) ? $product['amount_total'] : $product['amount'];

    $vendor_product_id = $product['extra']['vendor_product_id'];

    $product_type_instance = $product_manager->getProductTypeInstance($_pdata['product_type']);

    if ($_pdata['product_type'] === $product_manager::PRODUCT_TYPE_CONFIGURABLE) {
        $_pdata['extra']['variation_product_id'] = $vendor_product_id;
    }

    if (!isset($_pdata['extra'])) {
        $_pdata['extra'] = [];
    }

    $_pdata['extra'] = array_merge($_pdata['extra'], $product['extra']);

    $_pdata['price'] = fn_get_product_price($vendor_product_id, $amount, $auth);
    $_pdata['in_stock'] = $product_manager->getProductFieldValue($vendor_product_id, 'amount');

    if (!isset($product['stored_price']) || $product['stored_price'] !== 'Y') {
        $_pdata['base_price'] = $_pdata['price'];
    }

    foreach ($_pdata as $key => $value) {
        if ($product_type_instance->isFieldMergeable($key) && $key !== 'amount') {
            $_pdata[$key] = $product_manager->getProductFieldValue($vendor_product_id, $key);
        }
    }
}

/**
 * Hook handler: stores currently processed product in the runtime to determine the proper vendor product ID when
 * chaning a product stock.
 */
function fn_master_products_change_order_status_before_update_product_amount(
    $order_id,
    $status_to,
    $status_from,
    $force_notification,
    $place_order,
    $order_info,
    $k,
    $v
) {
    Registry::set('master_products.active_product', $v, true);
}

/**
 * Hook handler: stores currently processed product in the runtime to determe product stock on when placing an order.
 */
function fn_master_products_checkout_place_order_before_check_amount_in_stock(
    $cart,
    $auth,
    $params,
    $cart_id,
    $product,
    $_is_edp
) {
    Registry::set('master_products.active_product', $product, true);
}

/**
 * Hook handler: changes vendor product stock.
 */
function fn_master_products_update_product_amount_pre(
    &$product_id,
    $amount,
    $product_options,
    $sign,
    &$tracking,
    &$current_amount,
    &$product_code
) {
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    $active_product = Registry::get('master_products.active_product');

    if (!isset($active_product['extra']['vendor_product_id'])) {
        return;
    }

    $vendor_product_id = $active_product['extra']['vendor_product_id'];

    /** @var \Tygh\Addons\MasterProducts\Product\Type $product_type */
    $product_type = $product_manager->getProductTypeInstanceByProductId($product_id);

    $product_id = $vendor_product_id;

    $current_amount = $product_manager->getProductFieldValue($vendor_product_id, 'amount', null, true);

    if ($product_type->isFieldAvailableForVendorProduct('product_code')) {
        $product_code = $product_manager->getProductFieldValue($vendor_product_id, 'product_code');
    }
}

/**
 * Hook handler: sets vendor product code.
 */
function fn_master_products_create_order_details($order_id, $cart, &$order_details, $extra)
{
    if (!empty($extra['vendor_product_id'])) {
        /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
        $product_manager = Tygh::$app['addons.product_variations.product.manager'];

        $vendor_product_id = $extra['vendor_product_id'];

        /** @var \Tygh\Addons\MasterProducts\Product\Type $product_type */
        $product_type = $product_manager->getProductTypeInstanceByProductId($vendor_product_id);

        if ($product_type->isFieldAvailableForVendorProduct('product_code')) {
            $order_details['product_code'] = $product_manager->getProductFieldValue($vendor_product_id, 'product_code');
        }
    }
}

/**
 * Hook handler: excludes options of variations that are not sold by vendor.
 */
function fn_master_products_additional_data_loader_get_variation_codes_by_product_ids(
    $instance,
    $product_ids,
    $statuses,
    $fields,
    $join,
    &$condition
) {
    $company_id = fn_master_products_get_runtime_company_id();
    if (!$company_id) {
        return;
    }

    $condition .= db_quote(
        ' AND EXISTS('
        . ' SELECT 1 FROM ?:products AS vendor_products WHERE'
        . ' vendor_products.parent_product_id = variations.product_id AND vendor_products.company_id = ?i'
        . ' OR vendor_products.product_id = variations.product_id AND variations.company_id = ?i'
        . ' )',
        $company_id, $company_id
    );
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
}

/**
 * Fetches company ID from any passed object or runtime.
 * FIXME: Obtaining company_id from the $_REQUEST is ugly. Must be redone.
 *
 * @param array|null $object Object to extract company_id from
 * @param string     $area   Site area
 *
 * @return int Company ID
 */
function fn_master_products_get_runtime_company_id($object = null, $area = AREA)
{
    if ($object === null && $area === 'C') {
        // FIXME
        $object = $_REQUEST;
    }

    static $runtime_company_id;

<<<<<<< HEAD
    if (isset($object['vendor_id'])) {
        return (int) $object['vendor_id'];
=======
    if (isset($object['company_id'])) {
        return (int) $object['company_id'];
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    }

    if ($runtime_company_id === null) {
        $runtime_company_id = (int) Registry::ifGet('runtime.vendor_id', Registry::get('runtime.company_id'));
    }

    return $runtime_company_id;
}

/**
 * Helper function that generates sidebar menu with master and vendor products on the products management pages.
 *
 * @param string $controller Currently dispatched controller
 * @param string $mode       Currently dispatched controller mode
 */
function fn_master_products_generate_navigation_sections($controller, $mode)
{
    $section = $controller . '.' . $mode;

    Registry::set('navigation.dynamic.sections', [
        'products.manage'          => [
            'title' => __('master_products.products_being_sold'),
            'href'  => 'products.manage',
        ],
        'products.master_products' => [
            'title' => __('master_products.products_that_vendors_can_sell'),
            'href'  => 'products.master_products',
        ],
    ]);

    Registry::set('navigation.dynamic.active_section', $section);
}

/**
 * Hook handler: allows viewing master products.
 */
function fn_master_products_company_products_check(&$product_ids, $notify, &$company_condition)
{
    $controller = Registry::ifGet('runtime.controller', 'products');
    $mode = Registry::ifGet('runtime.mode', 'update');
    $request_method = isset($_SERVER['REQUEST_METHOD']) // FIXME
        ? $_SERVER['REQUEST_METHOD']
        : 'GET';

    if ($controller !== 'products' || $mode !== 'update' || $request_method !== 'GET') {
        return;
    }

    $company_condition = fn_get_company_condition(
        '?:products.company_id',
        true,
        Registry::get('runtime.company_id'),
        true
    );
}

/**
 * Hook handler: allows viewing master products.
 */
function fn_master_products_is_product_company_condition_required_post($product_id, &$is_required)
{
    $product_company_id = (int) db_get_field('SELECT company_id FROM ?:products WHERE product_id = ?i', $product_id);

    if ($product_company_id === 0) {
        $is_required = false;
    }
}

/**
<<<<<<< HEAD
 * Hook handler: updates vendor products descriptions when editing a master product.
 */
function fn_master_products_update_product_post($product_data, $product_id, $lang_code, $create)
{
    if ($create) {
        return;
    }

    $product_id_map = ServiceProvider::getProductIdMap();

    if (!$product_id_map->isMasterProduct($product_id) && !$product_id_map->isVendorProduct($product_id)) {
        return;
    }

    $service = ServiceProvider::getService();

    $service->onTableChanged('products', $product_id);
    $service->onTableChanged('product_descriptions', $product_id);
    $service->onTableChanged('product_status', $product_id);

    $service->actualizeMasterProductPrice($product_id);
    $service->actualizeMasterProductOffersCount($product_id);
    $service->actualizeMasterProductQuantity($product_id);
=======
 * Hook handler: removes unnecessary tabs from vendor product editing page.
 */
function fn_master_products_dispatch_before_display()
{
    $controller = Registry::get('runtime.controller');
    $mode = Registry::get('runtime.mode');

    if (AREA !== 'A' || $controller !== 'products' || $mode !== 'update') {
        return;
    }

    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];
    /** @var \Tygh\SmartyEngine\Core $view */
    $view = Tygh::$app['view'];

    /** @var array $product_data */
    $product_data = $view->getTemplateVars('product_data');
    if (empty($product_data['is_vendor_product'])) {
        return;
    }

    /** @var \Tygh\Addons\MasterProducts\Product\Type $product_type */
    $product_type = $product_manager->getProductTypeInstance($product_data['product_type']);
    $tabs = Registry::get('navigation.tabs');

    if (is_array($tabs)) {

        foreach ($tabs as $key => $tab) {
            if (!$product_type->isTabAvailableForVendorProduct($key)) {
                unset($tabs[$key]);
            }
        }

        Registry::set('navigation.tabs', $tabs);
    }
}

/**
 * Hook handler: updates vendor products descriptions when editing a master product.
 */
function fn_master_products_update_product_post($product_data, $product_id, $lang_code, $create)
{
    if ($create) {
        return;
    }

    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    $vendor_product_ids_list = $product_manager->getVendorProductIds($product_id);

    foreach ($vendor_product_ids_list as $vendor_product_id) {
        $product_manager->cloneProductDescriptions($product_id, $vendor_product_id, $lang_code);
        $product_manager->cloneProductFeaturesValues($product_id, $vendor_product_id);
    }

    $vendor_product = $product_manager->getVendorProduct($product_id);
    if ($vendor_product) {
        $product_manager->actualizeMasterProductPrice($vendor_product['master_product_id']);
    }
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
}

/**
 * Hook handler: prevents vendor product categories update.
 */
function fn_master_products_update_product_categories_pre($product_id, &$product_data, $rebuild, $company_id)
{
    if (empty($product_data['category_ids'])) {
        return;
    }

<<<<<<< HEAD
    $repository = ServiceProvider::getProductRepository();

    if ($repository->findMasterProductId($product_id)) {
=======
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    if ($product_manager->getVendorProduct($product_id)) {
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
        $product_data['category_ids'] = [];
    }
}

/**
 * Hook handler: updates vendor products categories when editing a master product.
 */
function fn_master_products_update_product_categories_post(
    $product_id,
    $product_data,
    $existing_categories,
    $rebuild,
    $company_id
) {
<<<<<<< HEAD
    $service = ServiceProvider::getService();
    $service->onTableChanged('products_categories', $product_id);
=======
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    $vendor_product_ids_list = $product_manager->getVendorProductIds($product_id);

    foreach ($vendor_product_ids_list as $vendor_product_id) {
        $product_manager->cloneProductCategories($product_id, $vendor_product_id);
    }
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
}

/**
 * Hook handler: actualizers master product price on vendor product removal.
 */
function fn_master_products_delete_product_pre($product_id, $status)
{
    Registry::del('master_products.removed_product');

    if (!$status) {
        return;
    }

<<<<<<< HEAD
    $repository = ServiceProvider::getProductRepository();

    $master_product_id = $repository->findMasterProductId($product_id);

    if ($master_product_id) {
        Registry::set('master_products.removed_product.master_product_id', $master_product_id, true);
=======
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];
    $vendor_product = $product_manager->getVendorProduct($product_id);
    if ($vendor_product) {
        Registry::set('master_products.removed_product', $vendor_product, true);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    }
}

/**
 * Hook handler: removes vendor products after master product removal.
 */
function fn_master_products_delete_product_post($product_id, $is_deleted)
{
    if (!$is_deleted) {
        return;
    }

<<<<<<< HEAD
    $repository = ServiceProvider::getProductRepository();
    $service = ServiceProvider::getService();

    $vendor_product_ids = $repository->findVendorProductIds($product_id);
=======
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];
    $vendor_product_ids = $product_manager->getVendorProductIds($product_id);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

    foreach ($vendor_product_ids as $vendor_product_id) {
        fn_delete_product($vendor_product_id);
    }

<<<<<<< HEAD
    $master_product_id = Registry::get('master_products.removed_product.master_product_id');

    if ($master_product_id) {
        $service->actualizeMasterProductPrice($master_product_id);
        $service->actualizeMasterProductOffersCount($master_product_id);
        $service->actualizeMasterProductQuantity($master_product_id);
=======
    $removed_vendor_product = Registry::get('master_products.removed_product');
    if ($removed_vendor_product) {
        $product_manager->actualizeMasterProductPrice($removed_vendor_product['master_product_id']);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    }
    Registry::del('master_products.removed_product');
}

<<<<<<< HEAD

/**
 * Hook handler: actualizes master product price when the disabling/enabling vendor products.
 */
function fn_master_products_tools_change_status($params, $result)
{
    if (!$result || $params['table'] !== 'products') {
        return;
    }

    $product_id = $params['id'];

    $product_id_map = ServiceProvider::getProductIdMap();

    if (!$product_id_map->isMasterProduct($product_id) && !$product_id_map->isVendorProduct($product_id)) {
        return;
    }

    $service = ServiceProvider::getService();

    $service->onTableChanged('product_status', $product_id);
    $service->actualizeMasterProductPrice($product_id);
    $service->actualizeMasterProductOffersCount($product_id);
    $service->actualizeMasterProductQuantity($product_id);
}

function fn_master_products_product_type_create_by_product($product, $product_id, &$type)
{
    if (!empty($product['master_product_id'])) {
        $type = PRODUCT_TYPE_VENDOR_PRODUCT_OFFER;
    }
}

/**
 * Hook handler: normalize request for children products
 */
function fn_master_products_get_route(&$req, &$result, $area, &$is_allowed_url)
{
    if ($area !== 'C'
        || empty($req['dispatch'])
        || $req['dispatch'] !== 'products.view'
        || empty($req['product_id'])
        || empty($req['vendor_id'])
    ) {
        return;
    }

    $repository = ServiceProvider::getProductRepository();

    if (isset($req['variation_id'])) {
        $vendor_product_id = $repository->findVendorProductId($req['variation_id'], $req['vendor_id']);

        if (!$vendor_product_id) {
            return;
        }

        $product_id_map = VariationsServiceProvider::getProductIdMap();

        $parent_product_id = $product_id_map->getParentProductId($vendor_product_id);

        if ($parent_product_id) {
            $req['variation_id'] = $vendor_product_id;
            $req['product_id'] = $parent_product_id;
        } else {
            $req['product_id'] = $vendor_product_id;
            unset($req['variation_id']);
        }
    } else {
        $vendor_product_id = $repository->findVendorProductId($req['product_id'], $req['vendor_id']);

        if (!$vendor_product_id) {
            return;
        }

        $req['product_id'] = $vendor_product_id;
=======
/*
 * Hook handler: modifies parameters passed to the fn_get_product_data when viewing master product's variation
 * in the administration panel.
 */
function fn_master_products_product_variations_get_parent_product_data(
    array $product,
    array $auth,
    $lang_code,
    &$parent_product_id,
    $field_list,
    $get_add_pairs,
    $get_main_pair,
    $get_taxes,
    $get_qty_discounts,
    $preview,
    $features,
    &$skip_company_condition,
    $feature_variants_selected_only
) {
    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    if ($product['company_id'] == 0) {
        $skip_company_condition = true;
    }

    if (!empty($product['master_product_id'])) {
        $parent_product_id = $product_manager->getProductFieldValue($product['master_product_id'], 'parent_product_id');
        $skip_company_condition = true;
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    }
}

/**
<<<<<<< HEAD
 * Hook handler: sync global options
 */
function fn_master_products_add_global_option_link_post($product_id, $option_id)
{
    $sync_service = ServiceProvider::getService();
    $sync_service->onTableChanged('product_global_option_links', $product_id, ['option_id' => $option_id]);
}

/**
 * Hook handler: sync global options
 */
function fn_master_products_delete_global_option_link_post($product_id, $option_id)
{
    $sync_service = ServiceProvider::getService();
    $sync_service->onTableChanged('product_global_option_links', $product_id, ['option_id' => $option_id]);
}

/**
 * Hook handler: sync feature values
 */
function fn_master_products_update_product_features_value_post($product_id)
{
    $sync_service = ServiceProvider::getService();
    $sync_service->onTableChanged('product_features_values', $product_id);
}

/**
 * @param \Tygh\Addons\ProductVariations\Service                       $group_service
 * @param \Tygh\Addons\ProductVariations\Product\Group\Group           $group
 * @param \Tygh\Addons\ProductVariations\Product\Group\Events\AEvent[] $events
 */
function fn_master_products_variation_group_save_group($group_service, $group, $events)
{
    if (Registry::get('runtime.product_variations_converter_mode') !== 'combinations') {
        return;
    }

    static $service;
    static $product_repository;
    static $query_factory;
    static $group_repository;

    $product_ids = [];
    $product_map = [];

    foreach ($events as $event) {
        if ($event instanceof ProductAddedEvent) {
            $product_id = $event->getProduct()->getProductId();
            $parent_product_id = $event->getProduct()->getParentProductId();

            $product_map[$parent_product_id][$product_id] = $product_id;

            if ($parent_product_id) {
                continue;
            }

            $product_ids[] = $product_id;
        }
    }

    if (!$product_ids) {
        return;
    }

    if ($service === null) {
        $service = ServiceProvider::getService();
    }

    if ($product_repository === null) {
        $product_repository = ServiceProvider::getProductRepository();
    }

    if ($query_factory === null) {
        $query_factory = VariationsServiceProvider::getQueryFactory();
    }

    if ($group_repository === null) {
        $group_repository = VariationsServiceProvider::getGroupRepository();
    }

    foreach ($product_ids as $product_id) {
        $vendor_product_ids = $product_repository->findVendorProductIds($product_id);

        if (empty($vendor_product_ids)) {
            continue;
        }

        $service->syncAllData($product_id, $vendor_product_ids);

        $query = $query_factory->createQuery(
            $product_repository::TABLE_PRODUCTS,
            ['product_id' => $vendor_product_ids],
            ['product_id', 'company_id']
        );

        foreach ($query->select() as $item) {
            $group_product_ids = [$item['product_id']];

            foreach ($product_map[$product_id] as $variation_product_id) {
                $result = $service->createVendorProduct($variation_product_id, $item['company_id']);

                if ($result->isSuccess()) {
                    $group_product_ids[] = $result->getData('vendor_product_id');
                }
            }

            if (count($group_product_ids) <= 1) {
                continue;
            }

            $group_id = $group_repository->findGroupIdByProductId($item['product_id']);

            if ($group_id) {
                $result = $group_service->attachProductsToGroup($group_id, $group_product_ids);
            } else {
                $result = $group_service->createGroup($group_product_ids, null, $group->getFeatures());
            }

            if (!$result->isSuccess()) {
                foreach ($group_product_ids as $item_product_id) {
                    if ($item_product_id != $item['product_id']) {
                        fn_delete_product($item_product_id);
                    }
                }
            }
        }
    }
}

function fn_master_products_clone_product_data($product_id, &$data, $is_cloning_allowed)
{
    if (empty($data)) {
        return;
    }

    unset(
        $data['master_product_id'],
        $data['master_product_status'],
        $data['master_product_offers_count']
    );
}

function fn_master_products_variation_group_create_products_by_combinations_item($service, $parent_product_id, $combination_id, $combination, &$product_data)
{
    if (empty($product_data)) {
        return;
    }

    unset(
        $product_data['master_product_id'],
        $product_data['master_product_status'],
        $product_data['master_product_offers_count']
    );
}

/**
 * @param \Tygh\Addons\ProductVariations\SyncService $sync_service
 * @param array $events
 */
function fn_master_products_variation_sync_flush_sync_events($sync_service, $events)
{
    if (Registry::get('runtime.product_variations_converter_mode')) {
        return;
    }

    $product_ids = [];
    $table_product_ids = [];

    foreach ($events as $event) {
        if (empty($event['destination_product_ids'])
            || empty($event['table_id'])
        ) {
            continue;
        }

        foreach ($event['destination_product_ids'] as $product_id) {
            $product_ids[$product_id] = $product_id;
            $table_product_ids[$product_id][$event['table_id']] = $event['table_id'];
        }
    }

    if (empty($product_ids)) {
        return;
    }

    $product_repository = ServiceProvider::getProductRepository();
    $service = ServiceProvider::getService();

    $vendor_product_ids_map = $product_repository->findVendorProductIdsByMasterProductIds($product_ids);

    foreach ($vendor_product_ids_map as $master_product_id => $vendor_product_ids) {
        foreach ($table_product_ids[$master_product_id] as $table_id) {
            $service->syncData($table_id, $master_product_id, (array) $vendor_product_ids);
        }
    }
}

function fn_master_products_get_attachments_pre($object_type, &$object_id, $type, $lang_code)
{
    if ($object_type !== 'product' || !empty($params['skip_check_vendor_product'])) {
        return;
    }

    $product_repository = ServiceProvider::getProductRepository();

    $master_product_id = $product_repository->findMasterProductId($object_id);

    if ($master_product_id) {
        $object_id = $master_product_id;
    }
}

function fn_master_products_get_discussion_pre(&$object_id, $object_type, $get_posts, $params)
{
    if ($object_type !== DISCUSSION_OBJECT_TYPE_PRODUCT || !empty($params['skip_check_vendor_product'])) {
        return;
    }

    $product_repository = ServiceProvider::getProductRepository();

    $master_product_id = $product_repository->findMasterProductId($object_id);

    if ($master_product_id) {
        $object_id = $master_product_id;
    }
}

function fn_master_products_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
{
    ServiceProvider::getProductIdMap()->setMastertProductIdMapByProducts([$product_data]);
}

function fn_master_products_load_products_extra_data_pre(&$products, $params, $lang_code)
{
    ServiceProvider::getProductIdMap()->setMastertProductIdMapByProducts($products);
}

function fn_master_products_url_pre(&$url, $area, $protocol, $lang_code)
{
    if ($area !== 'C'
        || strpos($url, 'products.view') === false
        || Registry::get('addons.seo.status') !== 'A'
    ) {
        return;
    }

    $parsed_url = parse_url($url);
    $dispatch = null;

    if (empty($parsed_url['query'])) {
        return;
    }

    parse_str($parsed_url['query'], $parsed_query);

    if (isset($parsed_query['dispatch'])) {
        $dispatch = $parsed_query['dispatch'];
    } elseif (isset($parsed_url['path'])) {
        $dispatch = $parsed_url['path'];
    }

    if (empty($parsed_query['product_id']) || $dispatch !== 'products.view') {
        return;
    }

    $product_id = $parsed_query['product_id'];
    $master_product_id = ServiceProvider::getProductIdMap()->getMasterProductId($product_id);

    if (!$master_product_id) {
        return;
    }

    $company_id = ServiceProvider::getProductIdMap()->getVendorProductCompanyId($product_id);

    if (Registry::get('runtime.seo.is_creating_canonical_url')) {
        $url = strtr($url, ["product_id={$product_id}" => "product_id={$master_product_id}"]);
    } else {
        $url = strtr($url, ["product_id={$product_id}" => "product_id={$master_product_id}&vendor_id={$company_id}"]);
    }
}

function fn_master_products_update_image_pairs($pair_ids, $icons, $detailed, $pairs_data, $object_id, $object_type)
{
    if (empty($pair_ids) || empty($object_id) || $object_type !== 'product') {
        return;
    }

    $sync_service = ServiceProvider::getService();
    $sync_service->onTableChanged('images_links', $object_id);
}

function fn_master_products_delete_image_pair($pair_id, $object_type, $image)
{
    if (empty($image) || empty($image['object_id']) || $image['object_type'] !== 'product') {
        return;
    }

    $sync_service = ServiceProvider::getService();
    $sync_service->onTableChanged('images_links', $image['object_id']);
}

function fn_product_variations_master_products_create_vendor_product($master_product_id, $company_id, $product, $vendor_product_id)
{
    $group_repository = VariationsServiceProvider::getGroupRepository();
    $variation_service = VariationsServiceProvider::getService();
    $product_repository = ServiceProvider::getProductRepository();

    $master_product_group = $group_repository->findGroupInfoByProductId($master_product_id);

    if (empty($master_product_group)) {
        return;
    }

    $product_ids = $group_repository->findGroupProductIdsByGroupIds([$master_product_group['id']]);
    $vendor_product_ids = $product_repository->findVendorProductIdsByMasterProductIds($product_ids, $company_id);

    if (empty($vendor_product_ids)) {
        return;
    }

    $group_ids = $group_repository->findGroupIdsByProductIds($vendor_product_ids);

    if (empty($group_ids)) {
        $group_id = null;
    } else {
        $group_id = reset($group_ids);
    }

    if ($group_id !== null) {
        $variation_service->attachProductsToGroup($group_id, [$vendor_product_id]);
    } else {
        $variation_service->createGroup([$vendor_product_id], null, GroupFeatureCollection::createFromFeatureList($master_product_group['feature_collection']));
    }
}
=======
 * Hook handler: sets company ID for products to load proper additional data when viewing vendor product variations.
 */
function fn_master_products_load_products_extra_data_post(&$products, $product_ids, $params, $lang_code)
{
    if (AREA !== 'A') {
        return;
    }

    if (Registry::get('runtime.company_id')) {
        return;
    }

    if (isset($params['company_id']) && $params['company_id'] !== '') {
        foreach ($products as &$product) {
            $product['company_id'] = $params['company_id'];
        }
    }

    return;
}

/**
 * Hook handler: modifies detected company ID when spliting products into sub-carts.
 */
function fn_master_products_direct_payments_cart_service_get_group_products_get_company_id(
    $products_data,
    $key,
    $item,
    &$vendor_id
) {
    if (isset($item['extra']['company_id'])) {
        $vendor_id = $item['extra']['company_id'];
    }
}

/**
 * Hook handler: actualizes master product price when the disabling/enabling vendor products.
 */
function fn_master_products_tools_change_status($params, $result)
{
    if (!$result || $params['table'] !== 'products') {
        return;
    }

    $vendor_product_id = $params['id'];

    /** @var \Tygh\Addons\MasterProducts\Product\Manager $product_manager */
    $product_manager = Tygh::$app['addons.product_variations.product.manager'];

    $vendor_product = $product_manager->getVendorProduct($vendor_product_id);
    if ($vendor_product) {
        $product_manager->actualizeMasterProductPrice($vendor_product['master_product_id']);
    }
}
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
