<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

defined('BOOTSTRAP') or die('Access denied');

use Tygh\Addons\MasterProducts\ServiceProvider;

<<<<<<< HEAD
=======
// replace product variations product manager with the master products one
unset(Tygh::$app['addons.product_variations.product.manager']);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
Tygh::$app->register(new ServiceProvider());

fn_register_hooks(
    // general products list management
    'get_products_pre',
    'get_products',
    'get_product_data',
    'get_product_data_post',
    'gather_additional_products_data_params',
<<<<<<< HEAD
    'gather_additional_products_data_post',
    'load_products_extra_data_pre',
=======
    'gather_additional_product_data_before_options',
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

    // administration panel products management
    'company_products_check',
    'is_product_company_condition_required_post',
<<<<<<< HEAD
=======
    'dispatch_before_display',
    'product_variations_get_parent_product_data',
    'load_products_extra_data_post',

    // cart content management
    'get_cart_product_icon',
    'pre_get_cart_product_data',
    'add_product_to_cart_get_price',
    'check_amount_in_stock_before_check',
    'get_product_code',
    'add_to_cart',
    'get_cart_product_data',
    'generate_cart_id',

    // order placement
    'update_product_amount_pre',
    'change_order_status_before_update_product_amount',
    'checkout_place_order_before_check_amount_in_stock',
    'create_order_details',

    // product variations tweaks
    'additional_data_loader_get_variation_codes_by_product_ids',
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

    // product update routine
    'update_product_post',
    'update_product_categories_pre',
    'update_product_categories_post',
<<<<<<< HEAD
    'add_global_option_link_post',
    'delete_global_option_link_post',
    'update_product_features_value_post',
    'variation_group_save_group',
    'clone_product_data',
    'variation_group_create_products_by_combinations_item',
    'variation_sync_flush_sync_events',
    'update_image_pairs',
    'delete_image_pair',
=======
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

    // master and vendor products data actualization on products removal/disable
    'delete_product_pre',
    'delete_product_post',
    'tools_change_status',

<<<<<<< HEAD
    'product_type_create_by_product',
    ['get_route', 1950],
    ['url_pre', 1450],

    ['get_attachments_pre', 500],
    ['get_discussion_pre', 500],

    ['master_products_create_vendor_product', '', 'product_variations']
=======
    // direct payments compatibility
    'direct_payments_cart_service_get_group_products_get_company_id'
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
);
