INSERT INTO `cscart_vendor_following_events` (`event_id`, `is_selected`, `event_name`) VALUES
(1, 1, 'Product is created'),
(2, 1, 'Product back in stock'),
(3, 1, 'New catalogue promotion for Vendor''s products is created'),
(4, 1, 'Review is created');