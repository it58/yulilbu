<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'change_order_status',
    'delete_product_post',
    'delete_category_after',
    'get_products_before_select',
    'get_products',
    'products_sorting',
<<<<<<< HEAD
    'update_product_pre',
=======
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    'update_product_post',
    'get_product_data'
);
