<?php

use Tygh\Bootstrap;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Tools\Url;
use Tygh\Http;
use Tygh\Settings;
use Tygh\Tools\ImageHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
define('WEBSERVICE_RES', 'http://api.resmush.it/ws.php?img=');

function fn_cp_image_optimization_update_image ($image_data, $image_id, $image_type, $images_path, &$_data, $mime_type, $is_clone) {
    $_data['cp_in_opt_list'] = 'N';
    if (!empty($image_id)) {
        db_query("DELETE FROM ?:all_store_images WHERE db_image_id = ?i", $image_id);
    }
}
function fn_cp_image_optimization_del_imgs_by_mask($filename, $prefix = '') {
    $filename = fn_substr($filename, 0, strrpos($filename, '.'));
    if (!empty($filename)) {
        Storage::instance('images')->deleteByPattern($prefix . $filename . '*');
    }
    return true;
}
function fn_cp_image_optimization_add_from_db_images() {
    $all_db_images = db_get_hash_array("SELECT ?:images.image_id, ?:images.image_x, ?:images.image_y, ?:images.image_path, ?:images_links.detailed_id, ?:images_links.object_type, ?:images_links.pair_id, ?:images_links.type, ?:images_links.object_id FROM ?:images 
        LEFT JOIN ?:images_links ON ?:images_links.detailed_id = ?:images.image_id OR ?:images_links.image_id = ?:images.image_id WHERE ?:images.cp_in_opt_list = ?s LIMIT 500", 'image_id', 'N');
    
    if (!empty($all_db_images)) {
        $counter = count($all_db_images);
        if (Registry::get('addons.hidpi.status') == 'A') {
            $hidpi_status = true;
        }
        $all_save_path = array();
        $image_ids = array_keys($all_db_images);
        foreach($all_db_images as $image_id => $db_img_tab) {
            if (!empty($db_img_tab['object_type'])) {
                $db_image_array = array();
                $pre_img_tab = $db_img_tab['object_type'];
                if (!empty($db_img_tab['detailed_id']) && ($db_img_tab['object_type'] == 'product' || $db_img_tab['object_type'] == 'category')) {
                    $pre_dir_img_tab = 'detailed';
                } else {
                    if (!file_exists('images/'.$db_img_tab['object_type'] . '/')) {
                        $pre_dir_img_tab = 'detailed';
                    } else {
                        $pre_dir_img_tab = $db_img_tab['object_type'];
                    }
                }
                $db_pre_path = $pre_dir_img_tab . '/' . floor($image_id / MAX_FILES_IN_DIR);
                if (file_exists('images/' . $db_pre_path . '/' . $db_img_tab['image_path']) && !empty($db_img_tab['image_path'])) {
                    $all_save_path[] = fn_cp_image_optimization_save_db_image_data($db_pre_path, $db_img_tab, $image_id);
        //new
                    if (!empty($hidpi_status)) {
                        $img_hidpi_name = fn_hdpi_form_name($db_img_tab['image_path']);
                        if (!empty($img_hidpi_name) && file_exists('images/' . $db_pre_path . '/' . $img_hidpi_name)) {
                            $hidpi_img_data = $db_img_tab;
                            $hidpi_img_data['image_path'] = $img_hidpi_name;
                            $all_save_path[] = fn_cp_image_optimization_save_db_image_data($db_pre_path, $hidpi_img_data, $image_id);
                        }
                    }
        //
                }
            }
            //db_query("UPDATE ?:images SET cp_in_opt_list = ?s WHERE image_id = ?i", 'Y', $image_id);
        }
        if (!empty($image_ids)) {
            db_query("UPDATE ?:images SET cp_in_opt_list = ?s WHERE image_id IN (?n)", 'Y', $image_ids);
        }
        db_query("UPDATE ?:all_store_images SET exist_trig = ?s WHERE image_path IN (?a)", 'Y', $all_save_path);
        if ($counter >= 500) {
            fn_cp_image_optimization_add_from_db_images();
        }
    }
    return true;
}
function fn_cp_image_optimization_save_db_image_data ($db_pre_path, $db_img_tab, $image_id) {
    $db_full_path = Storage::instance('images')->getUrl($db_pre_path . '/' . $db_img_tab['image_path']);
    if (!empty($db_full_path)) {
        $db_full_path = explode('?t=', $db_full_path);
        $db_path = $db_full_path[0];
        $db_abs_path = Storage::instance('images')->getAbsolutePath($db_pre_path . '/' . $db_img_tab['image_path']);
        $db_f_s = filesize($db_abs_path)/1048576;
        $db_img_file_size = number_format($db_f_s, 3, '.', '');
    }
    $db_image_array = array(
        'image_name' => $db_img_tab['image_path'],
        'image_path' => $db_path,
        'image_size' => $db_img_file_size,
        'image_ext' => fn_get_file_ext($db_img_tab['image_path']),
        'object_type' => $db_img_tab['object_type'],
        'dir' => $db_pre_path,
        'db_image_id' => $image_id
    );
    //db_query("INSERT INTO ?:all_store_images_delete SET image_path = ?s, image_name = ?s", $db_path, $db_img_tab['image_path']);
    $db_check_image = db_get_field("SELECT image_id FROM ?:all_store_images WHERE image_path = ?s", $db_path);
    if (empty($db_check_image)) {
        db_query("INSERT INTO ?:all_store_images ?e", $db_image_array);
    } else {
        db_query("UPDATE ?:all_store_images SET ?u WHERE image_id = ?i", $db_image_array, $db_check_image);
    }
    return $db_path;
}
function fn_cp_image_optimization_add_images($prefix, $image_file, $detailed_file, $position, $type, $object_id, $object) {
	static $updated_products = array();

    if (!empty($object_id)) {
        // Process multilang requests
        if (!is_array($object_id)) {
            $object_id = array($object_id);
        }
        foreach ($object_id as $_id) {
			
            $_REQUEST["server_import_image_icon"] = '';
            $_REQUEST["type_import_image_icon"] = '';

            // Get image alternative text if exists
            if (!empty($image_file) && strpos($image_file, '#') !== false) {
                list ($image_file, $image_alt) = explode('#', $image_file);
            }

            if (!empty($detailed_file) && strpos($detailed_file, '#') !== false) {
                list ($detailed_file, $detailed_alt) = explode('#', $detailed_file);
            }

            if (!empty($image_alt)) {
                preg_match_all('/\[([A-Za-z]+?)\]:(.*?);/', $image_alt, $matches);
                if (!empty($matches[1]) && !empty($matches[2])) {
                    $image_alt = array_combine(array_values($matches[1]), array_values($matches[2]));
                }
            }

            if (!empty($detailed_alt)) {
                preg_match_all('/\[([A-Za-z]+?)\]:(.*?);/', $detailed_alt, $matches);
                if (!empty($matches[1]) && !empty($matches[2])) {
                    $detailed_alt = array_combine(array_values($matches[1]), array_values($matches[2]));
                }
            }
            $type_image_detailed = (strpos($detailed_file, '://') === false) ? 'server' : 'url';
            $type_image_icon = (strpos($image_file, '://') === false) ? 'server' : 'url';

            $_REQUEST["type_import_image_icon"] = array($type_image_icon);
            $_REQUEST["type_import_image_detailed"] = array($type_image_detailed);
            
            $image_file = fn_cp_image_optimization_find_file($prefix, $image_file);

            if ($image_file !== false) {
                if ($type_image_icon == 'url') {
                    $_REQUEST["file_import_image_icon"] = array($image_file);

                } elseif (strpos($image_file, Registry::get('config.dir.root')) === 0) {
                    $_REQUEST["file_import_image_icon"] = array(str_ireplace(fn_get_files_dir_path(), '', $image_file));

                } else {
                    fn_set_notification('E', __('error'), __('error_images_need_located_root_dir'));
                    $_REQUEST["file_import_image_detailed"] = array();
                }
            } else {
                $_REQUEST["file_import_image_icon"] = array();
            }
            $detailed_file = fn_cp_image_optimization_find_file($prefix, $detailed_file);
            if ($detailed_file !== false) {
                if ($type_image_detailed == 'url') {
                    $_REQUEST["file_import_image_detailed"] = array($detailed_file);

                } elseif (strpos($detailed_file, Registry::get('config.dir.root')) === 0) {
                    $_REQUEST["file_import_image_detailed"] = array(str_ireplace(fn_get_files_dir_path(), '', $detailed_file));

                } else {
                    fn_set_notification('E',  __('error'), __('error_images_need_located_root_dir'));
                    $_REQUEST["file_import_image_detailed"] = array();
                }

            } else {
                $_REQUEST["file_import_image_detailed"] = array();
            }
            $_REQUEST['import_image_data'] = array(
                array(
                    'type' => $type,
                    'image_alt' => empty($image_alt) ? '' : $image_alt,
                    'detailed_alt' => empty($detailed_alt) ? '' : $detailed_alt,
                    'position' => empty($position) ? 0 : $position,
                )
            );

            $result = fn_attach_image_pairs('import', $object, $_id);
        }
        return $result;
    }
    return false;
}
function fn_cp_image_optimization_find_file($prefix, $file)
{
    $file = Bootstrap::stripSlashes($file);

    // Url
    if (strpos($file, '://') !== false) {
        return $file;
    }
    $prefix = fn_normalize_path(rtrim($prefix, '/'));
    $file = fn_normalize_path($file);
    $files_path = fn_get_files_dir_path();

    // Absolute path
    if (is_file($file) && strpos($file, $files_path) === 0) {
        return $file;
    }
    // Path is relative to files directory
    if (is_file($files_path . $file)) {
        return $files_path . $file;
    }
    // Path is relative to prefix inside files directory
    if (is_file($files_path . $prefix . '/' . $file)) {
        return $files_path . $prefix . '/' . $file;
    }
    // Prefix is absolute path
    if (strpos($prefix, $files_path) === 0 && is_file($prefix . '/' . $file)) {
        return $prefix . '/' . $file;
    }
    return false;
}
function fn_cp_image_optimization_cp_pagination($params, $area = AREA) {
	if (empty($params['total_items']) || empty($params['items_per_page'])) {
        return array();
    }
    $deviation = ($area == 'A') ? 5 : 7;
    $max_pages = 10;
    $per_page = 10;

    $total_pages = ceil((int) $params['total_items'] / $params['items_per_page']);

    // Pagination in other areas displayed as in any search engine
    $page_from = fn_get_page_from($params['page'], $deviation);
    $page_to = fn_get_page_to($params['page'], $deviation, $total_pages);

    $pagination = array (
        'navi_pages' => range($page_from, $page_to),
        'prev_range' => ($page_from > 1) ? $page_from - 1 : 0,
        'next_range' => ($page_to < $total_pages) ? $page_to + 1: 0,
        'current_page' => $params['page'],
        'prev_page' => ($params['page'] > 1) ? $params['page'] - 1 : 0,
        'next_page' => ($params['page'] < $total_pages) ? $params['page'] + 1 : 0,
        'total_pages' => $total_pages,
        'total_items' => $params['total_items'],
        'items_per_page' => $params['items_per_page'],
        'per_page_range' => range($per_page, $per_page * $max_pages, $per_page)
    );

    if ($pagination['prev_range']) {
        $pagination['prev_range_from'] = fn_get_page_from($pagination['prev_range'], $deviation);
        $pagination['prev_range_to'] = fn_get_page_to($pagination['prev_range'], $deviation, $total_pages);
    }

    if ($pagination['next_range']) {
        $pagination['next_range_from'] = fn_get_page_from($pagination['next_range'], $deviation);
        $pagination['next_range_to'] = fn_get_page_to($pagination['next_range'], $deviation, $total_pages);
    }
	$pagination['per_page_range'][] = 500;
	$pagination['per_page_range'][] = 1000;
	$pagination['per_page_range'][] = 1500;
    if (!in_array($params['items_per_page'], $pagination['per_page_range'])) {
        $pagination['per_page_range'][] = $params['items_per_page'];
        
        sort($pagination['per_page_range']);
    }

    return $pagination;
}
function fn_cp_image_optimization_cp_resize_image_low_vers($src, $new_width = 0, $new_height = 0, $bg_color = '#ffffff')
{
$bg_color = '#ffffff';
   static $notification_set = false;
    static $gd_settings = array();

    if (file_exists($src) && (!empty($new_width) || !empty($new_height)) && extension_loaded('gd')) {
        $img_functions = array(
            'png' => function_exists('imagepng'),
            'jpg' => function_exists('imagejpeg'),
            'gif' => function_exists('imagegif'),
        );

        if (empty($gd_settings)) {
            $gd_settings = Settings::instance()->getValues('Thumbnails');
        }

        list($width, $height, $mime_type) = fn_get_image_size($src);
        if (empty($width) || empty($height)) {
            return false;
        }

        $ext = fn_get_image_extension($mime_type);
        if (empty($img_functions[$ext])) {
            if ($notification_set == false) {
                fn_set_notification('E', __('error'), __('error_image_format_not_supported', array(
                    '[format]' => $ext
                )));
                $notification_set = true;
            }

            return false;
        }

        if (empty($new_width) || empty($new_height)) {
            if ($width < $new_width) {
                $new_width = $width;
            }
            if ($height < $new_height) {
                $new_height = $height;
            }
        }

        $dst_width = $new_width;
        $dst_height = $new_height;

        if (empty($new_height)) { // if we passed width only, calculate height
            $dst_height = $new_height = ($height / $width) * $new_width;

        } elseif (empty($new_width)) { // if we passed height only, calculate width
            $dst_width = $new_width = ($width / $height) * $new_height;

        } else { // we passed width and height, we need to fit image in this sizes
            if ($new_width * $height / $width > $dst_height) {
                $new_width = $width * $dst_height / $height;
            }
            $new_height = ($height / $width) * $new_width;
            if ($new_height * $width / $height > $dst_width) {
                $new_height = $height * $dst_width / $width;
            }
            $new_width = ($width / $height) * $new_height;

            $make_box = true;
        }

        $new_width = intval($new_width);
        $new_height = intval($new_height);

        $dst = imagecreatetruecolor($dst_width, $dst_height);

        if (function_exists('imageantialias')) {
            imageantialias($dst, true);
        }

        if ($ext == 'gif') {
            $new = imagecreatefromgif($src);
        } elseif ($ext == 'jpg') {
            $new = imagecreatefromjpeg($src);
        } elseif ($ext == 'png') {
            $new = imagecreatefrompng($src);
        }

        list($r, $g, $b) = (empty($bg_color)) ? fn_parse_rgb('#ffffff') : fn_parse_rgb($bg_color);
        $c = imagecolorallocate($dst, $r, $g, $b);

        if (empty($bg_color) && ($ext == 'png' || $ext == 'gif')) {
            if (function_exists('imagecolorallocatealpha') && function_exists('imagecolortransparent') && function_exists('imagesavealpha') && function_exists('imagealphablending')) {
                $c = imagecolorallocatealpha($dst, 255, 255, 255, 127);
                imagecolortransparent($dst, $c);
                imagesavealpha($dst, true);
                imagealphablending($dst, false);
            }
        }

        imagefilledrectangle($dst, 0, 0, $dst_width, $dst_height, $c);

        if (!empty($make_box)) {
            $x = intval(($dst_width - $new_width) / 2);
            $y = intval(($dst_height - $new_height) / 2);
        } else {
            $x = 0;
            $y = 0;
        }

        imagecopyresampled($dst, $new, $x, $y, 0, 0, $new_width, $new_height, $width, $height);

        // Free memory from image
        imagedestroy($new);

        /*if ($gd_settings['convert_to'] == 'original') {
            $convert_to = $ext;
        } elseif (!empty($img_functions[$gd_settings['convert_to']])) {
            $convert_to = $gd_settings['convert_to'];
        } else {
            $convert_to = key($img_functions);
        }*/
		$convert_to = 'jpg';
        ob_start();
        if ($convert_to == 'gif') {
            imagegif($dst);
        } elseif ($convert_to == 'jpg') {
            imagejpeg($dst, null, $gd_settings['jpeg_quality']);
        } elseif ($convert_to == 'png') {
            imagepng($dst);
        }
        $content = ob_get_clean();

        return array($content, $convert_to);
    }

    return false;
}
function fn_cp_image_optimization_cp_resize_image($src, $new_width = 0, $new_height = 0, $bg_color = '#ffffff', $custom_settings = array())
{

   static $general_settings = array();
    if (empty($general_settings)) {
        $general_settings = Settings::instance()->getValues('Thumbnails');
    }

    gc_collect_cycles();

    $settings = empty($custom_settings) ? $general_settings : $custom_settings;

    /** @var \Imagine\Image\ImagineInterface $imagine */
    $imagine = Tygh::$app['image'];

   $format = 'jpg';
    /*if ($format === 'original') {
        if ($original_file_type = fn_get_image_extension(fn_get_mime_content_type($src, false))) {
            $format = $original_file_type;
        } else {
            $format = 'png';
        }
    }*/

    $transparency = null;
    if (empty($bg_color)) {
        $bg_color = '#FFF';

        if ($format == 'png' || $format == 'gif') {
            $transparency = 0;
        }
    } elseif (!preg_match('/^#([0-9a-f]{3}){1,2}$/i', $bg_color)) {
        $bg_color = '#FFF';
    }

    try {
        $image = $imagine->open($src);

       $image->usePalette(new \Imagine\Image\Palette\RGB());

        // This is a non-necessary operation
        // which can however trigger exceptions if isn't supported by a driver
       /* fn_catch_exception(function () use ($image) {
            $image->usePalette(new \Imagine\Image\Palette\RGB());
        });*/

        $filter = ($imagine instanceof \Imagine\Gd\Imagine)
            ? \Imagine\Image\ImageInterface::FILTER_UNDEFINED
            : \Imagine\Image\ImageInterface::FILTER_LANCZOS;

        $new_size = new \Imagine\Image\Box($new_width, $new_height);
        $thumbnail = $image->thumbnail(
            $new_size,
            \Imagine\Image\ImageInterface::THUMBNAIL_INSET,
            $filter
        );

        // In case that created thumbnail is smaller than required size, we create
        // an empty canvas of required size and center thumbnail on it
        $thumbnail_coordinates = new \Imagine\Image\Point(
            (int)(($new_size->getWidth() - $thumbnail->getSize()->getWidth()) / 2),
            (int)(($new_size->getHeight() - $thumbnail->getSize()->getHeight()) / 2)
        );

        if (!$image->palette()->supportsAlpha()) {
            $transparency = null;
        }
        $canvas_color = $image->palette()->color($bg_color, $transparency);

        $canvas = $imagine->create($new_size, $canvas_color);

        $canvas->paste($thumbnail, $thumbnail_coordinates);

        unset($thumbnail, $image);

        $thumbnail = $canvas;

        $options = array(
            'jpeg_quality' => $settings['jpeg_quality'],
            'png_compression_level' => 9,
            'filter' => $filter,
            'flatten' => true,
        );

        $return = array($thumbnail->get($format, $options), $format);

        unset($thumbnail);

        gc_collect_cycles();

        return $return;
	} catch (\Exception $e) {
        $error_message = __('error_unable_to_create_thumbnail', array(
            '[error]' => $e->getMessage(),
            '[file]' => $src
        ));

        if (AREA == 'A') {
            fn_set_notification('E', __('error'), $error_message);
        }

        gc_collect_cycles();

        return false;
    }
}
function fn_cp_image_optimization_convert_png_to_jpg($image_path, $filename) {
	$height = 0;
	$filename = preg_replace("/\.[^.]*?$/", ".jpg", $filename);
  
	$th_filename = '';
    if (Storage::instance('images')->isExist($filename)) {
        $th_filename = $filename;
    } else {
        if (Registry::get('config.tweaks.lazy_thumbnails') && !Storage::instance('images')->isExist($image_path)) {
            foreach (array('jpg', 'jpeg') as $ext) {
                $image_path = preg_replace("/\.[^.]*?$/", "." . $ext, $image_path);
                if (Storage::instance('images')->isExist($image_path)) {
                    break;
                }
            }
        }
        list($img_width, $img_height, , $tmp_path) = fn_get_image_size(Storage::instance('images')->getAbsolutePath($image_path));
        if (!empty($tmp_path)) {
			/*$cust_sett = array();
			$cust_sett['convert_to'] = 'jpg';
			$cust_sett['jpeg_quality'] = 80;*/
			if (version_compare(PRODUCT_VERSION, '4.3.1', '>')) {
				list($cont, $format) = fn_cp_image_optimization_cp_resize_image($tmp_path, $img_width, $img_height, Registry::get('settings.Thumbnails.thumbnail_background_color'));
			} else {
				list($cont, $format) = fn_cp_image_optimization_cp_resize_image_low_vers($tmp_path, $img_width, $img_height);
			}
            if (!empty($cont)) {
                list(, $th_filename) = Storage::instance('images')->put($filename, array(
                    'contents' => $cont,
                    'caching' => true
                ));
            }
        }
    }
    return $th_filename;
}
function fn_cp_image_optimization_get_images_for_list($params, $items_per_page) {

	$get_image_list = array();
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page
    );
    $params = array_merge($default_params, $params);
    $fields = array (
        "?:all_store_images.*",
    );
    $sortings = array (
        'image_name' => "?:all_store_images.image_name",
        'image_size' => "?:all_store_images.image_size",
        'image_ext' => "?:all_store_images.image_ext",
        'object_type' => "?:all_store_images.object_type",
        'optimization' => "?:all_store_images.optimization"
    );
    $condition = $join = $group = '';
    
    $params['search_ext'] = db_get_fields("SELECT DISTINCT image_ext FROM ?:all_store_images");
    $params['search_type'] = db_get_fields("SELECT DISTINCT object_type FROM ?:all_store_images");
    if (!empty($params['s_image'])) {
		$params['s_image'] = trim($params['s_image']);
		$condition .= db_quote(" AND ?:all_store_images.image_name LIKE ?l", '%'.$params['s_image'].'%');
	}
	if (!empty($params['s_size_from'])) {
		$params['s_size_from'] = trim($params['s_size_from']);
		$condition .= db_quote(" AND ?:all_store_images.image_size > ?d", $params['s_size_from']);
	}
	if (!empty($params['s_size_to'])) {
		$params['s_size_to'] = trim($params['s_size_to']);
		$condition .= db_quote(" AND ?:all_store_images.image_size < ?d", $params['s_size_to']);
	}
	if (!empty($params['s_ext'])) {
		$params['s_ext'] = trim($params['s_ext']);
		$condition .= db_quote(" AND ?:all_store_images.image_ext = ?s", $params['s_ext']);
	}
	if (!empty($params['s_type'])) {
		$params['s_type'] = trim($params['s_type']);
		$condition .= db_quote(" AND ?:all_store_images.object_type = ?s", $params['s_type']);
	}
	$condition .= db_quote(" AND ?:all_store_images.image_name != ?s", '');
	
	$sorting = db_sort($params, $sortings, 'image_name', 'asc');
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:all_store_images $join WHERE 1 $condition $group");
        $limit = db_paginate($params['page'], $params['items_per_page']);
    }

	$get_image_list = db_get_hash_array('SELECT ' . implode(', ', $fields) . " FROM ?:all_store_images $join WHERE 1 $condition $group $sorting $limit", 'image_id');
	if (!empty($get_image_list)) {
	foreach($get_image_list as $key => $op_img_data){
			$all_same_images = db_get_hash_array("SELECT ?:images.image_id, ?:images.image_x, ?:images.image_y, ?:images.image_path, ?:images_links.detailed_id, ?:images_links.object_type, ?:images_links.pair_id, ?:images_links.type, ?:images_links.object_id FROM ?:images LEFT JOIN ?:images_links ON ?:images_links.detailed_id = ?:images.image_id OR ?:images_links.image_id = ?:images.image_id WHERE ?:images.image_path = ?s", 'image_id', $op_img_data['image_name']);
			if (!empty($all_same_images)) {
				foreach($all_same_images as $image_id => $img_tab) {
					$pre_img_tab = $img_tab['object_type'];
					if (!empty($img_tab['detailed_id']) && ($img_tab['object_type'] == 'product' || $img_tab['object_type'] == 'category')) {
						$pre_dir_img_tab = 'detailed';
					} else {
						$pre_dir_img_tab = $img_tab['object_type'];
					}
					$pre_path = $pre_dir_img_tab . '/' . floor($image_id / MAX_FILES_IN_DIR);
					$full_path = Storage::instance('images')->getUrl($pre_path . '/' . $img_tab['image_path']);
					if (!empty($full_path)) {
						$full_path = explode('?t=', $full_path);
						$path = $full_path[0];
						if ($path == $op_img_data['image_path']) {
							$get_image_list[$key]['type'] = $img_tab['type'];
							if ($img_tab['type'] == 'V') {
								$get_image_list[$key]['feature_id'] = db_get_field("SELECT feature_id FROM ?:product_feature_variants WHERE variant_id = ?i", $img_tab['object_id']);
							}
							$get_image_list[$key]['object_id'] = $img_tab['object_id'];
							$get_image_list[$key]['strore_image_id'] = $img_tab['image_id'];
							$get_image_list[$key]['img_pair_id'] = $img_tab['pair_id'];
							$get_image_list[$key]['cp_image_data'] = array(
								'image_path' => $op_img_data['image_path'],
								'alt' => '',
								'image_x' => $img_tab['image_x'],
								'image_y' => $img_tab['image_y'],
								'absolute_path' => Storage::instance('images')->getAbsolutePath($pre_path . '/' . $img_tab['image_path']),
								'relative_path' => $pre_path . '/' . $img_tab['image_path']
							);
							break;
						}
					}
				}
			}
			if (empty($get_image_list[$key]['cp_image_data'])) {
			//28.05.2016
				/*$fff = getimagesize($op_img_data['image_path']);
				if (!empty($fff)) {*/
					if (!empty($op_img_data['dir'])) {
						$abs_path = Storage::instance('images')->getAbsolutePath($op_img_data['dir'] . '/' . $op_img_data['image_name']);
						$rel_path = $op_img_data['dir'] . '/' . $op_img_data['image_name'];
					} else {
						$abs_path = Storage::instance('images')->getAbsolutePath($op_img_data['image_name']);
						$rel_path = $op_img_data['image_name'];
					}
					$get_image_list[$key]['cp_image_data'] = array(
						'image_path' => $op_img_data['image_path'],
						'alt' => '',
						/*'image_x' => $fff[0],
						'image_y' => $fff[1],*/
						'absolute_path' => $abs_path,
						'relative_path' => $rel_path
					);
				//}
			}
			if (empty($get_image_list[$key]['object_type'])) {
				$get_image_list[$key]['object_type'] = 'other';
			}
		}
		//fn_print_r($get_image_list);
		/*if (!empty($params['sort_by']) && $params['sort_by'] == 'img_type') {
			if (empty($params['cp_sort_type'])) {
				usort($get_image_list, "fn_cp_image_optimization_sort_images_by_type");
				$params['cp_sort_type'] = 'first'; 
			} elseif ($params['cp_sort_type'] == 'first') {
				usort($get_image_list, "fn_cp_image_optimization_sort_images_by_type_more");
				$params['cp_sort_type'] = 'no_first';
			} elseif ($params['cp_sort_type'] == 'no_first') {
				usort($get_image_list, "fn_cp_image_optimization_sort_images_by_type");
				$params['cp_sort_type'] = 'first';
			}
		}*/
	}
    return array($get_image_list, $params);
}
/*
function fn_cp_image_optimization_sort_images_by_type($a, $b) {
     if ($a['object_type'] == $b['object_type']) {
		return 0;
     }
     return ($a['object_type'] < $b['object_type']) ? 1 : -1;
}
function fn_cp_image_optimization_sort_images_by_type_more($a, $b) {
     if ($a['object_type'] == $b['object_type']) {
		return 0;
     }
     return ($a['object_type'] < $b['object_type']) ? -1 : 1;
}*/
function fn_cp_image_optimization_cron_path_info() {
    //$host = defined('HTTPS') ? Registry::get('config.https_host') . Registry::get('config.https_path') . '/' : Registry::get('config.http_host') . Registry::get('config.http_path') . '/';
    $admin_ind = Registry::get('config.admin_index');
    //$hint = __("export_cron_hint") . '<br>1: php '.$host.''.$admin_ind.'?dispatch=cp_image_optimization.cron_file_compressing&cron_pass=' . Registry::get('addons.cp_image_optimization.cron_pass') . '<br>2: curl "'. $host . ''.$admin_ind.'?dispatch=cp_image_optimization.cron_file_compressing&cron_pass=' . Registry::get('addons.cp_image_optimization.cron_pass') . '"' . '<br>3: wget -q "'. $host . ''.$admin_ind.'?dispatch=cp_image_optimization.cron_file_compressing&cron_pass=' . Registry::get('addons.cp_image_optimization.cron_pass') . '"';
    $__params = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
    if (!empty($__params) && !empty($__params['cron_pass'])) {
        $cron_pass = $__params['cron_pass'];
    } else {
        $cron_pass = '';
    }
    $hint = '<b>' . __("export_cron_hint") . ':</b><br>php ' .Registry::get('config.dir.root') .'/' . $admin_ind . ' --dispatch=cp_image_optimization.cron_file_compressing --cron_pass=' . $cron_pass;
    $hint .= '<br />' . __('cp_io_manual_cron_run') . ': <a href="' . fn_url('cp_image_optimization.cron_file_compressing&is_manual=1&cron_pass=' . $cron_pass) .'">' . __('cp_io_run') . '</a>';
    $hint .=  '<br />' . __('cp_io_cron_attention') . '.';
    return $hint;
}

function fn_cp_image_optimization_count_not_opt_images() {
    
    $get_all = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
    $get_all['number_of_all_images'] = db_get_field("SELECT COUNT(image_id) FROM ?:all_store_images");
    $get_all['number_of_not_opt_images'] = db_get_field("SELECT COUNT(image_id) FROM ?:all_store_images WHERE optimization = ?s", 'N');
	$get_all['number_of_opt_images'] = db_get_field("SELECT COUNT(image_id) FROM ?:all_store_images WHERE optimization = ?s", 'O');
	$get_all['number_of_errors_images'] = db_get_field("SELECT COUNT(image_id) FROM ?:all_store_images WHERE optimization = ?s", 'E');
	$get_all['total_save']  = $get_all['total_save']/1024;
	$get_all['total_save'] = number_format($get_all['total_save'], 3, '.', ' ');
	if (!empty($get_all['folders'])) {
        $get_all['folders'] = explode(',', $get_all['folders']);
	} else {
        $get_all['folders'] = array();
	}
    return $get_all;
}

function fn_cp_image_optimization_save_api_key($api_key) {
    
    db_query("UPDATE ?:optimize_image SET api_key_for_opt = ?s WHERE key_id = 1", $api_key);
    return true;
}

function fn_cp_image_optimization_find_images ($folder_name, $dir, $params, $adit_folders = array()) {
	$dir = $dir.'/'.$folder_name;
	$dir_list = scandir(DIR_ROOT . '/images'. $dir);
	foreach ($dir_list as $v) {
		if (is_dir(DIR_ROOT . '/images'.$dir. '/' . $v) && strpos($v, '.') === false) {
			if (strpos($v, 'cache') === false && strpos($v, 'backup') === false) {
				fn_cp_image_optimization_find_images($v, $dir, $params, $adit_folders);
				$count[] = $v;
			}
		} else {
			$other[] = $v;
		}
	}
	if (!empty($other)) {
		//$other = $dir_list;
		$dir = substr($dir, 1);
		$all_save_path = array();
		foreach ($other as $img) {
			$ext = fn_get_file_ext($img);
			if (!empty($ext)) {
				$ext = strtolower($ext);
				if (in_array($ext, array('png', 'jpg', 'gif', 'jpeg'))) {
					
					$img_abs_path = Storage::instance('images')->getAbsolutePath($dir . '/' . $img);
					$img_f_s = filesize($img_abs_path)/1048576;
					$img_file_size = number_format($img_f_s, 3, '.', '');
					
					$old_path = Storage::instance('images')->getUrl($dir . '/' . $img);
					$old_path = explode('?t=', $old_path);
					$path = $old_path[0];
					
					/*$all_same_images = db_get_hash_array("SELECT ?:images.image_id, ?:images.image_path, ?:images_links.detailed_id, ?:images_links.object_type, ?:images_links.type, ?:images_links.object_id FROM ?:images LEFT JOIN ?:images_links ON ?:images_links.detailed_id = ?:images.image_id OR ?:images_links.image_id = ?:images.image_id WHERE ?:images.image_path = ?s", 'image_id', $img);
					if (!empty($all_same_images)) {
						foreach($all_same_images as $image_id => $img_tab) {
							$pre_img_tab = $img_tab['object_type'];
							if (!empty($img_tab['detailed_id']) && ($img_tab['object_type'] == 'product' || $img_tab['object_type'] == 'category')) {
								$pre_img_tab = 'detailed';
							}
							$pre_path = $pre_img_tab . '/' . floor($image_id / MAX_FILES_IN_DIR);
							$full_path = Storage::instance('images')->getUrl($pre_path . '/' . $img_tab['image_path']);
							if (!empty($full_path)) {
								$full_path = explode('?t=', $full_path);
								$table_path = $full_path[0];
								if ($table_path == $path) {
									$object_type = $img_tab['object_type'];
									break;
								}
							}
							
						}
					}*/
					if (empty($object_type)) {
						$object_type = 'other';
					}
					//db_query("INSERT INTO ?:all_store_images_delete SET image_path = ?s, image_name = ?s", $path, $img);
					$all_save_path[] = $path;
					$check_image = db_get_field("SELECT image_id FROM ?:all_store_images WHERE image_path = ?s", $path);
					if (empty($check_image)) {
                        $new_data = array(
                            'image_path' => $path,
                            'image_name' => $img,
                            'optimization' => 'N',
                            'dir' => $dir,
                            'image_ext' => $ext,
                            'image_size' => $img_file_size,
                            'object_type' => $object_type
                        );
                        db_query("INSERT INTO ?:all_store_images ?e", $new_data);
                        //db_query("INSERT INTO ?:all_store_images SET image_path = ?s, image_name = ?s, optimization = ?s, dir = ?s, image_ext = ?s, image_size = ?d, object_type = ?s", $path, $img, 'N', $dir, $ext, $img_file_size, $object_type);
                    } else {
                        $hh = array(
                            'image_id' => $check_image,
                            'image_ext' => $ext,
                            'image_size' => $img_file_size,
                            'object_type' => $object_type
                        );
                        db_query("UPDATE ?:all_store_images SET ?u WHERE image_id = ?i", $hh, $check_image);
                        //db_query("UPDATE ?:all_store_images SET image_ext = ?s, image_size = ?d, object_type = ?s WHERE image_id = ?i", $ext, $img_file_size, $object_type, $check_image);
                    }
				} 
			}
		}
		if (!empty($all_save_path)) {
            db_query("UPDATE ?:all_store_images SET exist_trig = ?s WHERE image_path IN (?a)", 'Y', $all_save_path);
		}
	}
    return true;
}

function fn_cp_image_optimization_check_api_key($api_key) {
	$chek_key = db_get_field("SELECT image_path FROM ?:all_store_images LIMIT 1");
	$chek_params=array();
	$chek_params['key'] = $api_key;
	$chek_params['lossy'] = 1;
	//$wait_delay = Registry::get('addons.cp_image_optimization.shortpixel_wait');
	if (empty($wait_delay)) {
		$wait_delay = 6;
	}
	$chek_params['wait'] = $wait_delay;
	$chek_params['urllist'] = $chek_key;
	$chek_params = json_encode($chek_params);
	$chek_params = str_replace('"urllist":"', '"urllist":["', $chek_params);
	$chek_params = str_replace('"}', '"]}', $chek_params);
	$chek_url = 'https://api.shortpixel.com/v2/reducer.php';
	$chek_ch = curl_init();
	curl_setopt($chek_ch, CURLOPT_URL, $chek_url);
	curl_setopt($chek_ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($chek_ch, CURLOPT_POST, true);
	curl_setopt($chek_ch, CURLOPT_POSTFIELDS, $chek_params);
	$chek_html = curl_exec($chek_ch);
	curl_close($chek_ch);
	$chek_html = json_decode($chek_html, true);
	$chek_url_files = (array) $chek_html;
	if (!empty($chek_url_files['Status']['Code']) && $chek_url_files['Status']['Code'] == -402) {
		fn_set_notification('W', __('warning'), __('invalid_api_key'));
		return false;
	}
	if (!empty($chek_url_files['Status']['Code']) && $chek_url_files['Status']['Code'] == -403) {
		fn_set_notification('W', __('warning'), __('quota_exceeded'));
		return false;
	}
	return true;
}

function fn_cp_image_optimization_optimize_all_images($params) {
	if (empty($params['continue_request'])) {
		if (Registry::get('runtime.controller') == 'cp_image_optimization' && Registry::get('runtime.mode') == 'find_new' || (!empty($params['cron_compressing']) && $params['cron_compressing'] == 'Y') || (!empty($params['image_list_proc']) && $params['image_list_proc'] == 'Y')) {
            
			//db_query("TRUNCATE TABLE ?:all_store_images_delete");
			db_query("UPDATE ?:all_store_images SET exist_trig = ?s", 'N');
            fn_cp_image_optimization_add_from_db_images();
            db_query("UPDATE ?:all_store_images LEFT JOIN ?:images ON ?:images.image_id = ?:all_store_images.db_image_id SET ?:all_store_images.exist_trig = ?s WHERE ?:all_store_images.db_image_id > ?i", 'Y', 0);
			$dir_list = scandir(DIR_ROOT . '/images');
			$dir = '';
			$adit_folders = db_get_field("SELECT folders FROM ?:optimize_image WHERE key_id = ?i", 1);
			if (!empty($adit_folders)) {
                $adit_folders = explode(',', $adit_folders);
			} else {
                $adit_folders = array();
			}
			foreach ($dir_list as $v) {
				if (is_dir(DIR_ROOT . '/images/' . $v) && strpos($v, '.') === false) {
					if ($v == 'companies'/* || $v == 'logos'*/ || in_array($v,$adit_folders)) {
						$images = fn_cp_image_optimization_find_images($v, $dir, $params, $adit_folders);
					}
				} else {
					$other[] = $v;
				}
			}
			if (!empty($other)) {
				//$other = $dir_list;
				$dir = substr($dir, 1);
				$all_save_path = array();
				foreach ($other as $img) {
					$ext = fn_get_file_ext($img);
					if (!empty($ext)) {
						$ext = strtolower($ext);
						if (in_array($ext, array('png', 'jpg', 'gif', 'jpeg'))) {
							if (!empty($dir)) {
                                $old_path = Storage::instance('images')->getUrl($dir . '/' . $img);
                            } else {
                                $old_path = Storage::instance('images')->getUrl($img);
                            }
							$img_abs_path = Storage::instance('images')->getAbsolutePath($dir . '/' . $img);
							$img_f_s = filesize($img_abs_path)/1048576;
							$img_file_size = number_format($img_f_s, 3, '.', '');
							
							$old_path = explode('?t=', $old_path);
							$path = $old_path[0];
// 							$img_fields = '?:images.image_id, ?:images.image_path, ?:images_links.detailed_id, ?:images_links.object_type, ?:images_links.type, ?:images_links.object_id';
// 							$all_same_images = db_get_hash_array("SELECT " . $img_fields . " FROM ?:images LEFT JOIN ?:images_links ON ?:images_links.detailed_id = ?:images.image_id OR ?:images_links.image_id = ?:images.image_id WHERE ?:images.image_path = ?s", 'image_id', $img);
// 							if (!empty($all_same_images)) {
// 								foreach($all_same_images as $image_id => $img_tab) {
// 									if (!empty($img_tab['detailed_id']) && ($img_tab['object_type'] == 'product' || $img_tab['object_type'] == 'category')) {
// 										$pre_img_tab = 'detailed';
// 									} else {
//                                         if (!file_exists('images/'.$img_tab['object_type'] . '/')) {
//                                             $pre_img_tab = 'detailed';
//                                         } else {
//                                             $pre_img_tab = $img_tab['object_type'];
//                                         }
//                                     }
// 									$pre_path = $pre_img_tab . '/' . floor($image_id / MAX_FILES_IN_DIR);
// 									$full_path = Storage::instance('images')->getUrl($pre_path . '/' . $img_tab['image_path']);
// 									if (!empty($full_path)) {
// 										$full_path = explode('?t=', $full_path);
// 										$table_path = $full_path[0];
// 										if ($table_path == $path) {
// 											$object_type = $img_tab['object_type'];
// 											break;
// 										}
// 									}
// 								}
// 							}
							if (empty($object_type)) {
								$object_type = 'other';
							}
							//db_query("INSERT INTO ?:all_store_images_delete SET image_path = ?s, image_name = ?s", $path, $img);
							$all_save_path[] = $path;
							$check_image = db_get_field("SELECT image_id FROM ?:all_store_images WHERE image_path = ?s", $path);
							if (empty($check_image)) {
                                $new_data = array(
                                    'image_path' => $path,
                                    'image_name' => $img,
                                    'optimization' => 'N',
                                    'dir' => $dir,
                                    'image_ext' => $ext,
                                    'image_size' => $img_file_size,
                                    'object_type' => $object_type
                                );
                                db_query("INSERT INTO ?:all_store_images ?e", $new_data);
                                //db_query("INSERT INTO ?:all_store_images SET image_path = ?s, image_name = ?s, optimization = ?s, dir = ?s, image_ext = ?s, image_size = ?d, object_type = ?s", $path, $img, 'N', $dir, $ext, $img_file_size, $object_type);
                            } else {
                                $hh = array(
                                    'image_id' => $check_image,
                                    'image_ext' => $ext,
                                    'image_size' => $img_file_size,
                                    'object_type' => $object_type
                                );
                                db_query("UPDATE ?:all_store_images SET ?u WHERE image_id = ?i", $hh, $check_image);
                                
                                //db_query("UPDATE ?:all_store_images SET image_ext = ?s, image_size = ?d, object_type = ?s WHERE image_id = ?i", $ext, $img_file_size, $object_type, $check_image);
                            }
						} 
					}
				}
				if (!empty($all_save_path)) {
                    db_query("UPDATE ?:all_store_images SET exist_trig = ?s WHERE image_path IN (?a)", 'Y', $all_save_path);
				}
			}
			//if ((!empty($params['image_list_proc']) && $params['image_list_proc'] == 'Y') || (Registry::get('runtime.controller') == 'cp_image_optimization' && Registry::get('runtime.mode') == 'find_new')) {
                db_query("UPDATE ?:all_store_images SET object_type = ?s WHERE image_path LIKE ?l", 'thumbnail', '%/thumbnails/%');
			//}
			db_query("DELETE FROM ?:all_store_images WHERE exist_trig = ?s", 'N');
		}
	}
	
    if (Registry::get('runtime.controller') == 'cp_image_optimization' && (Registry::get('runtime.mode') == 'optimize_all' || Registry::get('runtime.mode') == 'optimize_new' || Registry::get('runtime.mode') == 'optimize_errors') || (!empty($params['cron_compressing']) && $params['cron_compressing'] == 'Y') || !empty($params['cp_image_ids'])) {
		//$image_limit = Registry::get('addons.cp_image_optimization.step_of_compressing_images');
		if (empty($image_limit)) {
			$image_limit = 80;
		}
		if (!empty($params['cron_compressing']) && $params['cron_compressing'] == 'Y') {
            $cp_cron_run = true;
		}
        $img_opt_counter = 0;
		if (empty($params['continue_request'])) {
			if (Registry::get('runtime.mode') == 'optimize_all') {
				db_query("UPDATE ?:all_store_images SET optimization = ?s", 'N');
				$all_store_images = db_get_array("SELECT * FROM ?:all_store_images");
			} elseif (Registry::get('runtime.mode') == 'optimize_new' || !empty($cp_cron_run)) {
				$all_store_images = db_get_array("SELECT * FROM ?:all_store_images WHERE optimization = ?s", 'N');
			} elseif (Registry::get('runtime.mode') == 'optimize_errors') {
				$all_store_images = db_get_array("SELECT * FROM ?:all_store_images WHERE optimization = ?s", 'E');
			} elseif (!empty($params['cp_image_ids'])) {
				$all_store_images = db_get_array("SELECT * FROM ?:all_store_images WHERE image_id IN (?n)", $params['cp_image_ids']);
				$image_limit = 200;
			}
			if (empty($cp_cron_run)) {
                if (empty($_SESSION['compress_img'])) {
                    $_SESSION['compress_img'] = array();
                }
                $_SESSION['compress_img']['save'] = 0;
                $_SESSION['compress_img']['skip'] = 0;
                $_SESSION['compress_img']['errors'] = 0;
                $_SESSION['compress_img']['optimizated'] = 0;
                $_SESSION['compress_img']['big_img'] = 0;
            }
			$forbid_format = 0;
		} else {
			$all_store_images = db_get_array("SELECT * FROM ?:all_store_images WHERE optimization = ?s", 'N');
		}
        $saved_settings = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
		$params['img_quality'] = !empty($saved_settings['img_quality']) ? $saved_settings['img_quality'] : 88;
        $params['compressor'] = !empty($saved_settings['compressor']) ? $saved_settings['compressor'] : 'L';
		$params['choosen_optimizerer'] = !empty($saved_settings['choosen_optimizerer']) ? $saved_settings['choosen_optimizerer'] : 'B';
		if (!empty($params['compressor'])) {
            if ($params['compressor'] == 'L') {
                $s_compression = 1;
            } elseif ($params['compressor'] == 'G') {
                $s_compression = 2;
            } elseif ($params['compressor'] == 'S') {
                $s_compression = 0;
            } else {
                $s_compression = 1;
            }
		}
		if (!empty($all_store_images)) {
			if ($params['choosen_optimizerer'] == 'R' || $params['choosen_optimizerer'] == 'B') {
				//$api_key = db_get_field("SELECT api_key_for_opt FROM ?:optimize_image WHERE key_id = ?i", 1);
				$api_key = !empty($saved_settings['api_key_for_opt']) ? $saved_settings['api_key_for_opt'] : '';
				if ($params['choosen_optimizerer'] == 'B' && !empty($api_key)) {
					/*$back_key = fn_cp_image_optimization_check_api_key ($api_key);
					if (!$back_key) {
						//return true;
					}*/
					$back_key = true;
				}
				$wait_delay = !empty($saved_settings['shortpixel_wait']) ? $saved_settings['shortpixel_wait'] : 6;
				$save = 0;
				foreach ($all_store_images as $key => $image) {
					if (!empty($image['image_path']) && !empty($image['image_id'])) {
                        if (file_exists('images/' . $image['dir'] . '/' . $image['image_name'])) {
                            if (!empty($cp_cron_run) && !empty($params['cron_value']) && !empty($img_opt_counter) && $params['cron_value'] == $img_opt_counter) {
                                break;
                            }
                            $img_opt_counter += 1;
//                             if (Registry::get('runtime.mode') != 'optimize_errors') {
//                                 if ($key == $image_limit) {
//                                     return false;
//                                 }
//                             }
                            $status = '';
                            $s = urlencode($image['image_path']).'&qlty='.$params['img_quality'];
                            //28.052016
                            if (Bootstrap::getIniParam('allow_url_fopen') == true) {
                                $o = json_decode(@file_get_contents(WEBSERVICE_RES . $s));
                            } else {
                                $o = json_decode(Http::get(WEBSERVICE_RES . $s));
                            }
                            //end
                            $url_files = (array) $o;
                            if (!empty($url_files['error'])) {
                                if ($url_files['error'] == 502 || $url_files['error'] == 403) {
                                // try to compress this file by shortpixel.com (because file size is more than 2Mb or forbidden format)
                                    if ($params['choosen_optimizerer'] == 'B' && !empty($api_key) && !empty($back_key)) {
                                        $short_size = $short_down_path = $compr_text = $status = '';
                                        //$img_path = array(urlencode($image['image_path']));
                                        $img_path = array($image['image_path']);
                                        $snd_params=array();
                                        $snd_params['key'] = $api_key;
                                        $snd_params['lossy'] = $s_compression;
                                        $snd_params['wait'] = $wait_delay;
                                        $snd_params['urllist'] = $img_path;
                                        $snd_params = json_encode($snd_params);
                                        $url = 'https://api.shortpixel.com/v2/reducer.php';
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_URL, $url);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($ch, CURLOPT_POST, true);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $snd_params);
                                        $html = curl_exec($ch);
                                        curl_close($ch);
                                        $html = json_decode($html, true);
                                        $url_files_2 = (array) $html;
                                        
                                        // Code > 2 - it is an error
                                        if (!empty($url_files['Status']['Code']) && $url_files['Status']['Code'] == -402) {
                                            fn_set_notification('W', __('warning'), __('invalid_api_key'));
                                            $back_key = false;
                                            continue;
                                        }
                                        if (!empty($url_files['Status']['Code']) && $url_files['Status']['Code'] == -403) {
                                            fn_set_notification('W', __('warning'), __('quota_exceeded'));
                                            $back_key = false;
                                            continue;
                                        }
                                        if (!empty($url_files_2['Status']['Code']) && $url_files_2['Status']['Code'] == -402) {
                                            $invalid_key = 1;
                                        }
                                        if (!empty($url_files_2['Status']['Code']) && $url_files_2['Status']['Code'] == -403) {
                                            $quota_exceeded = 1;
                                        }
                                        if (!empty($url_files_2[0]['Status']['Code']) && $url_files_2[0]['Status']['Code'] == -403) {
                                            $quota_exceeded = 1;
                                        }
                                        if (!empty($url_files_2[0]['Status']['Code']) && $url_files_2[0]['Status']['Code'] == -301 && empty($cp_cron_run)) {
                                            $_SESSION['compress_img']['big_img'] = $_SESSION['compress_img']['big_img'] + 1;
                                            continue;
                                        }
                                        $short_errors = array();
                                        $short_errors[0] = -102;
                                        $short_errors[1] = -105;
                                        $short_errors[2] = -106;
                                        $short_errors[3] = -201;
                                        $short_errors[4] = -202;
                                        $short_errors[5] = -302;
                                        $short_errors[6] = -303;
                                        $short_errors[7] = -401;
                                        if (!empty($url_files_2[0]['Status']['Code']) && $url_files_2[0]['Status']['Code'] > 2) {
                                            if (empty($cp_cron_run)) {
                                                $_SESSION['compress_img']['errors'] = $_SESSION['compress_img']['errors'] + 1;
                                            }
                                            db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                            $status = 'error';
                                        } elseif (!empty($url_files_2[0]['Status']['Code']) && in_array($url_files_2[0]['Status']['Code'], $short_errors)) {
                                            if (empty($cp_cron_run)) {
                                                $_SESSION['compress_img']['errors'] = $_SESSION['compress_img']['errors'] + 1;
                                            }
                                            db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                            $status = 'error';
                                        }
                                    // if no path for downloadin, but image was compressed and request comeback to fast. Sending request 1 more time
                                        if (empty($url_files_2[0]['LossyURL']) && !empty($url_files_2[0]['Status']['Code']) && $url_files_2[0]['Status']['Code'] < 3) {
                                            $second = curl_init();
                                            curl_setopt($second, CURLOPT_URL, $url);
                                            curl_setopt($second, CURLOPT_RETURNTRANSFER, 1);
                                            curl_setopt($second, CURLOPT_POST, true);
                                            curl_setopt($second, CURLOPT_POSTFIELDS, $snd_params);
                                            $snd_html = curl_exec($second);
                                            curl_close($second);
                                            $snd_html = json_decode($snd_html, true);
                                            $url_files_2 = (array) $snd_html;
                                    // download compress image
                                        } 
                                        if (!empty($url_files_2[0]['LossyURL'])) {
                                            if (!empty($s_compression)) {
                                                $short_size = $url_files_2[0]['LossySize'];
                                                $short_down_path = $url_files_2[0]['LossyURL'];
                                                if ($s_compression == 1) {
                                                    $compr_text = __('cp_lossy');
                                                } elseif ($s_compression == 2) {
                                                    $compr_text = __('cp_glossy');
                                                } else {
                                                    $compr_text = __('cp_lossy');
                                                }
                                            } else {
                                                $short_size = $url_files_2[0]['LoselessSize'];
                                                $short_down_path = $url_files_2[0]['LosslessURL'];
                                                $compr_text = __('cp_lossless');
                                            }
                                        }
                                        if (!empty($short_down_path)) {
                                            if ($url_files_2[0]['OriginalSize'] > $short_size) {
                                                $url2 = $short_down_path;
                                            //28.05.2016
                                                if (Bootstrap::getIniParam('allow_url_fopen') == true) {
                                                    $result = @file_get_contents($url2);
                                                } else {
                                                    $result = Http::get($url2);
                                                }
                                            //end
                                                $fsp = fopen('./images/'.$image['dir'].'/'.$image['image_name'], 'wb');
                                                fwrite($fsp, $result);
                                                fclose($fsp);
                                                if (empty($cp_cron_run)) {
                                                    $_SESSION['compress_img']['save'] = $_SESSION['compress_img']['save'] + ($url_files_2[0]['OriginalSize'] - $short_size);
                                                    $_SESSION['compress_img']['optimizated'] = $_SESSION['compress_img']['optimizated'] + 1;
                                                } else {
                                                    $save += $url_files_2[0]['OriginalSize'] - $short_size;
                                                }
                                                $new_file_size = $short_size/1048576;
                                                $text = 'Shortpixel. ' . __('cp_io_before') . ' - ' . number_format($url_files_2[0]['OriginalSize']/1024, 2, '.', '') . ' Kb<br />' . $compr_text;
                                                $for_update = array(
                                                    'optimization' => 'O',
                                                    'image_size' => $new_file_size,
                                                    'image_id' => $image['image_id'],
                                                    'opt_info' => $text,
                                                );
                                                db_query("UPDATE ?:all_store_images SET ?u WHERE image_id = ?i", $for_update, $image['image_id']);
                        //remove thumbs and watermarks
//                                                 if (!empty($image['object_type']) && $image['object_type'] != 'thumbnail') {
//                                                     fn_delete_image_thumbnails($image['dir'].'/'.$image['image_name']);
//                                                     fn_cp_image_optimization_del_imgs_by_mask($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                                     fn_delete_image_thumbnails($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                                 } elseif (!empty($image['object_type']) && $image['object_type'] == 'thumbnail') {
//                                                     fn_cp_image_optimization_del_imgs_by_mask($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                                 }
                        //
                                                //db_query("UPDATE ?:all_store_images SET optimization = ?s, image_size = ?d WHERE image_id = ?i", 'O', $new_file_size, $image['image_id']);
                                                
                                            //31.05.2016
                                                $all_space = db_get_field("SELECT total_save FROM ?:optimize_image WHERE key_id = ?i", 1);
                                                $all_save = $url_files_2[0]['OriginalSize'] - $short_size;
                                                $all_save = $all_save/1024;
                                                $all_save = number_format($all_save, 3, '.', '');
                                                $new_save_space = $all_space + $all_save;
                                                
                                                $for_total = array(
                                                    'total_save' => $new_save_space,
                                                    'key_id' => 1
                                                );
                                                db_query("UPDATE ?:optimize_image SET ?u WHERE key_id = ?i", $for_total, 1);
                                                //db_query("UPDATE ?:optimize_image SET total_save = ?d WHERE key_id = ?i", $new_save_space, 1);
                                            //end
                                            
                                                $status = 'optimized';
                                            } else {
                                                db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'O', $image['image_id']);
                                                if (empty($cp_cron_run)) {
                                                    $_SESSION['compress_img']['skip'] = $_SESSION['compress_img']['skip'] + 1;
                                                }
                                                $status = 'already optimized';
                                            }
                                        }
                                    } else {
                                        if (empty($cp_cron_run)) {
                                            $_SESSION['compress_img']['errors'] = $_SESSION['compress_img']['errors'] + 1;
                                            $_SESSION['compress_img']['big_img'] = $_SESSION['compress_img']['big_img'] + 1;
                                        }
                                        db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                        $status = 'error';
                                    }
                                } else {
                                    if (empty($cp_cron_run)) {
                                        $_SESSION['compress_img']['errors'] = $_SESSION['compress_img']['errors'] + 1;
                                    }
                                    db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                    $status = 'error';
                                }
                                if ($params['choosen_optimizerer'] == 'R') {
                                    db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                    $status = 'error';
                                }
                            } else {
                                if (!empty($url_files['dest_size']) && ($url_files['src_size'] > $url_files['dest_size'])) {
                                    if (empty($cp_cron_run)) {
                                        $_SESSION['compress_img']['save'] = $_SESSION['compress_img']['save'] + ($url_files['src_size'] - $url_files['dest_size']);
                                    } else {
                                        $save += $url_files['src_size'] - $url_files['dest_size'];
                                    }
                                
                                //31.05.2016
                                    $all_space = db_get_field("SELECT total_save FROM ?:optimize_image WHERE key_id = ?i", 1);
                                    $all_save = $url_files['src_size'] - $url_files['dest_size'];
                                    $all_save = $all_save/1024;
                                    $all_save = number_format($all_save, 3, '.', '');
                                    $new_save_space = $all_space + $all_save;
                                    
                                    $for_total = array(
                                        'total_save' => $new_save_space,
                                        'key_id' => 1
                                    );
                                    db_query("UPDATE ?:optimize_image SET ?u WHERE key_id = ?i", $for_total, 1);
                                    //db_query("UPDATE ?:optimize_image SET total_save = ?d WHERE key_id = ?i", $new_save_space, 1);
                                //end
                                    $ch = curl_init($url_files['dest']);
                                    $frp = fopen('./images/'.$image['dir'].'/'.$image['image_name'], 'wb');
                                    curl_setopt($ch, CURLOPT_FILE, $frp);
                                    curl_setopt($ch, CURLOPT_HEADER, 0);
                                    curl_exec($ch);
                                    curl_close($ch);
                                    fclose($frp);
                                    if (empty($cp_cron_run)) {
                                        $_SESSION['compress_img']['optimizated'] = $_SESSION['compress_img']['optimizated'] + 1;
                                    }
                                    $new_file_size = $url_files['dest_size']/1048576;
                                    $text = 'Resmush. ' . __('cp_io_before') . ' - ' . number_format($url_files['src_size']/1024, 2, '.', '') . ' Kb<br />' . __('cp_io_quality') . ' - ' . $params['img_quality'];
                                    $for_update = array(
                                        'optimization' => 'O',
                                        'image_size' => $new_file_size,
                                        'image_id' => $image['image_id'],
                                        'opt_info' => $text,
                                    );
                                    db_query("UPDATE ?:all_store_images SET ?u WHERE image_id = ?i", $for_update, $image['image_id']);
                        //remove thumbs and watermarks
//                                     if (!empty($image['object_type']) && $image['object_type'] != 'thumbnail') {
//                                         fn_delete_image_thumbnails($image['dir'].'/'.$image['image_name']);
//                                         fn_cp_image_optimization_del_imgs_by_mask($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                         fn_delete_image_thumbnails($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                     } elseif (!empty($image['object_type']) && $image['object_type'] == 'thumbnail') {
//                                         fn_cp_image_optimization_del_imgs_by_mask($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                     }
                        //
                                    //db_query("UPDATE ?:all_store_images SET optimization = ?s, image_size = ?d WHERE image_id = ?i", 'O', $new_file_size, $image['image_id']);
                                    $status = 'optimized';
                                } else {
                                    db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'O', $image['image_id']);
                                    if (empty($cp_cron_run)) {
                                        $_SESSION['compress_img']['skip'] = $_SESSION['compress_img']['skip'] + 1;
                                    }
                                    $status = 'already optimized';
                                }
                            }
                            if (!file_exists('var/logs/cp_image_optimization/')) {
                                $structure = 'var/logs/cp_image_optimization/';
                                $oldmask = umask(0);
                                mkdir($structure, 0777, true);
                                umask($oldmask);
                            }
                            $today = date("d_m_y");
                            if (!file_exists('var/logs/cp_image_optimization/'.$today.'.txt')) {
                                $fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'wb');
                                $log_result = 'file: '.$image['image_path'].'  status: '.$status ;
                                fwrite($fp, $log_result."\r\n");
                                fclose($fp);
                            } else {
                                $fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'a+');
                                $log_result = 'file: '.$image['image_path'].'  status: '.$status ;
                                fwrite($fp, $log_result."\r\n");
                                fclose($fp);
                            }
						} else {
                            db_query("DELETE FROM ?:all_store_images WHERE image_id = ?i", $image['image_id']);
						}
					}
				}
				$status_key = 'Ok';
				if (empty($cp_cron_run)) {
                    $save = 0;
                    $save = $_SESSION['compress_img']['save']/1048576;
				}
				$save = number_format($save, 2, ',', ' ');
				if (empty($cp_cron_run)) {
                    fn_set_notification('W', __('important'), __('image_optimization_end', array(
                        '[update]' => $_SESSION['compress_img']['optimizated'],
                        '[skip]' => $_SESSION['compress_img']['skip'],
                        '[save_space]' => $save.'Mb',
                        '[total_errors]' => $_SESSION['compress_img']['errors'],
                        '[big_img_file]' => $_SESSION['compress_img']['big_img']
                    )));
				}
				if (!empty($invalid_key) && $invalid_key == 1) {
                    if (empty($cp_cron_run)) {
                        fn_set_notification('W', __('warning'), __('invalid_api_key'));
					}
					$status_key = 'Invalid api key';
				}
				if (!empty($quota_exceeded) && $quota_exceeded == 1) {
                    if (empty($cp_cron_run)) {
                        fn_set_notification('W', __('warning'), __('quota_exceeded'));
					}
					$status_key = 'Quota exceeded';
				}
				if (!file_exists('var/logs/cp_image_optimization/')) {
					$structure = 'var/logs/cp_image_optimization/';
					$oldmask = umask(0);
					mkdir($structure, 0777, true);
					umask($oldmask);
				}
				$today = date("d_m_y");
				if (!file_exists('var/logs/cp_image_optimization/'.$today.'.txt')) {
					$fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'wb');
					$log_result = 'save: '.$save.'Mb';
					fwrite($fp, $log_result."\r\n");
					fwrite($fp, $status_key."\r\n");
					fclose($fp);
				} else {
					$fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'a+');
					$log_result = 'save: '.$save.'Mb';
					fwrite($fp, $log_result."\r\n");
					fwrite($fp, $status_key."\r\n");
					fclose($fp);
				}
			//31.05.2016
				/*
				$all_space = db_get_field("SELECT total_save FROM ?:optimize_image WHERE key_id = ?i", 1);
				$all_save = $_SESSION['compress_img']['save']/1048576;
				$all_save = number_format($all_save, 3, '.', ' ');
				$new_save_space = $all_space + $all_save;
				db_query("UPDATE ?:optimize_image SET total_save = ?d WHERE key_id = ?i", $new_save_space, 1);
				*/
			//end
                if (empty($cp_cron_run)) {
                    $_SESSION['compress_img'] = array();
				}
			}
		} else {
            if (empty($cp_cron_run)) {
                fn_set_notification('W', __('notice'), __('no_image_for_compress'));
			}
		}
		if ($params['choosen_optimizerer'] == 'S') {
			if (!empty($all_store_images)) {
				$api_key = db_get_field("SELECT api_key_for_opt FROM ?:optimize_image WHERE key_id = ?i", 1);
				if (!empty($api_key)) {
                    $back_key = true;
					/*$back_key = fn_cp_image_optimization_check_api_key ($api_key);
					if (!$back_key) {
						return true;
					}*/
					$wait_delay = !empty($saved_settings['shortpixel_wait']) ? $saved_settings['shortpixel_wait'] : 6;
					$save = 0;
					foreach ($all_store_images as $key => $image) {
						if (!empty($image['image_path']) && !empty($image['image_id']) && !empty($back_key)) {
                            if (file_exists('images/' . $image['dir'] . '/' . $image['image_name'])) {
                                if (!empty($cp_cron_run) && !empty($params['cron_value']) && !empty($img_opt_counter) && $params['cron_value'] == $img_opt_counter) {
                                    break;
                                }
                                $short_size = $short_down_path = $compr_text = $status = '';
                                $img_opt_counter += 1;
//                                 if (Registry::get('runtime.mode') != 'optimize_errors') {
//                                     if ($key == $image_limit) {
//                                         return false;
//                                     }
//                                 }
                                //$img_path = array(urlencode($image['image_path']));
                                $img_path = array($image['image_path']);
                                $snd_params=array();
                                $snd_params['key'] = $api_key;
                                $snd_params['lossy'] = $s_compression;
                                $snd_params['wait'] = $wait_delay;
                                $snd_params['urllist'] = $img_path;
                                $snd_params = json_encode($snd_params);
                                $url = 'https://api.shortpixel.com/v2/reducer.php';
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $snd_params);
                                $html = curl_exec($ch);
                                curl_close($ch);
                                $html = json_decode($html, true);
                                $url_files = (array) $html;
                                // Code > 2 - it is an error
                                
                                if (!empty($url_files['Status']['Code']) && $url_files['Status']['Code'] == -402) {
                                    fn_set_notification('W', __('warning'), __('invalid_api_key'));
                                    $back_key = false;
                                    break;
                                }
                                if (!empty($url_files['Status']['Code']) && $url_files['Status']['Code'] == -403) {
                                    fn_set_notification('W', __('warning'), __('quota_exceeded'));
                                    $back_key = false;
                                    break;
                                }
                                /*if (!empty($url_files['Status']['Code']) && $url_files['Status']['Code'] == -402) {
                                    $invalid_key = 1;
                                }
                                if (!empty($url_files[0]['Status']['Code']) && $url_files[0]['Status']['Code'] == -403) {
                                    $quota_exceeded = 1;
                                }*/
                                if (!empty($url_files[0]['Status']['Code']) && $url_files[0]['Status']['Code'] == -301 && empty($cp_cron_run)) {
                                    $_SESSION['compress_img']['big_img'] = $_SESSION['compress_img']['big_img'] + 1;
                                    continue;
                                }
                                $short_errors = array();
                                $short_errors[] = -102;
                                $short_errors[] = -105;
                                $short_errors[] = -106;
                                $short_errors[] = -201;
                                $short_errors[] = -202;
                                $short_errors[] = -302;
                                $short_errors[] = -303;
                                $short_errors[] = -401;
                                if (!empty($url_files[0]['Status']['Code']) && $url_files[0]['Status']['Code'] > 2) {
                                    if (empty($cp_cron_run)) {
                                        $_SESSION['compress_img']['errors'] = $_SESSION['compress_img']['errors'] + 1;
                                    }
                                    db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                    $status = 'error';
                                } elseif (!empty($url_files[0]['Status']['Code']) && in_array($url_files[0]['Status']['Code'], $short_errors)) {
                                    if (empty($cp_cron_run)) {
                                        $_SESSION['compress_img']['errors'] = $_SESSION['compress_img']['errors'] + 1;
                                    }
                                    db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'E', $image['image_id']);
                                    $status = 'error';
                                }
                                
                                // if no path for downloadin, but image was compressed and request comeback to fast. Sending request 1 more time
                                if (empty($url_files[0]['LossyURL']) && !empty($url_files[0]['Status']['Code']) && $url_files[0]['Status']['Code'] < 3) {
                                    $second = curl_init();
                                    curl_setopt($second, CURLOPT_URL, $url);
                                    curl_setopt($second, CURLOPT_RETURNTRANSFER, 1);
                                    curl_setopt($second, CURLOPT_POST, true);
                                    curl_setopt($second, CURLOPT_POSTFIELDS, $snd_params);
                                    $snd_html = curl_exec($second);
                                    curl_close($second);
                                    $snd_html = json_decode($snd_html, true);
                                    $url_files = (array) $snd_html;
                                // download compress image
                                }
                                if (!empty($url_files[0]['LossyURL'])) {
                                    if (!empty($s_compression)) {
                                        $short_size = $url_files[0]['LossySize'];
                                        $short_down_path = $url_files[0]['LossyURL'];
                                        if ($s_compression == 1) {
                                            $compr_text = __('cp_lossy');
                                        } elseif ($s_compression == 2) {
                                            $compr_text = __('cp_glossy');
                                        } else {
                                            $compr_text = __('cp_lossy');
                                        }
                                    } else {
                                        $short_size = $url_files[0]['LoselessSize'];
                                        $short_down_path = $url_files[0]['LosslessURL'];
                                        $compr_text = __('cp_lossless');
                                    }
                                }
                                if (!empty($short_down_path)) {
                                    if ($url_files[0]['OriginalSize'] > $short_size) {
                                        $url2 = $short_down_path;
                                    //28.05.2016
                                        if (Bootstrap::getIniParam('allow_url_fopen') == true) {
                                            $result = @file_get_contents($url2);
                                        } else {
                                            $result = Http::get($url2);
                                        }
                                    //end
                                        $fp = fopen('./images/'.$image['dir'].'/'.$image['image_name'], 'wb');
                                        fwrite($fp, $result);
                                        fclose($fp);
                                        if (empty($cp_cron_run)) {
                                            $_SESSION['compress_img']['save'] = $_SESSION['compress_img']['save'] + ($url_files[0]['OriginalSize'] - $short_size);
                                            $_SESSION['compress_img']['optimizated'] = $_SESSION['compress_img']['optimizated'] + 1;
                                        } else {
                                            $save += $url_files[0]['OriginalSize'] - $short_size;
                                        }
                                        $new_file_size = $short_size/1048576;
                                        $text = 'Shortpixel. ' . __('cp_io_before') . ' - ' . number_format($url_files[0]['OriginalSize']/1024, 2, '.', '') . ' Kb<br />' . $compr_text;
                                        $for_update = array(
                                            'optimization' => 'O',
                                            'image_size' => $new_file_size,
                                            'image_id' => $image['image_id'],
                                            'opt_info' => $text
                                        );
                                        db_query("UPDATE ?:all_store_images SET ?u WHERE image_id = ?i", $for_update, $image['image_id']);
                        //remove thumbs and watermarks
//                                         if (!empty($image['object_type']) && $image['object_type'] != 'thumbnail') {
//                                             fn_delete_image_thumbnails($image['dir'].'/'.$image['image_name']);
//                                             fn_cp_image_optimization_del_imgs_by_mask($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                             fn_delete_image_thumbnails($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                         } elseif (!empty($image['object_type']) && $image['object_type'] == 'thumbnail') {
//                                             fn_cp_image_optimization_del_imgs_by_mask($image['dir'].'/'.$image['image_name'], 'watermarked/*/');
//                                         }
                        //
                                        //db_query("UPDATE ?:all_store_images SET optimization = ?s, image_size = ?d WHERE image_id = ?i", 'O', $new_file_size, $image['image_id']);
                                                
                                    //31.05.2016
                                        $all_space = db_get_field("SELECT total_save FROM ?:optimize_image WHERE key_id = ?i", 1);
                                        $all_save = $url_files[0]['OriginalSize'] - $short_size;
                                        $all_save = $all_save/1024;
                                        $all_save = number_format($all_save, 3, '.', '');
                                        $new_save_space = $all_space + $all_save;
                                        
                                        $cp_total_ar = array(
                                            'total_save' => $new_save_space,
                                            'key_id' => 1
                                        );
                                        db_query("UPDATE ?:optimize_image SET ?u WHERE key_id = ?i", $cp_total_ar, 1);
                                        //db_query("UPDATE ?:optimize_image SET total_save = ?d WHERE key_id = ?i", $new_save_space, 1);
                                    //end
                                    
                                        $status = 'optimized';
                                    } else {
                                        db_query("UPDATE ?:all_store_images SET optimization = ?s WHERE image_id = ?i", 'O', $image['image_id']);
                                        if (empty($cp_cron_run)) {
                                            $_SESSION['compress_img']['skip'] = $_SESSION['compress_img']['skip'] + 1;
                                        }
                                        $status = 'already optimized';
                                    }
                                }
                                if (!file_exists('var/logs/cp_image_optimization/')) {
                                    $structure = 'var/logs/cp_image_optimization/';
                                    $oldmask = umask(0);
                                    mkdir($structure, 0777, true);
                                    umask($oldmask);
                                }
                                $today = date("d_m_y");
                                if (!file_exists('var/logs/cp_image_optimization/'.$today.'.txt')) {
                                    $fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'wb');
                                    $log_result = 'file: '.$image['image_path'].'  status: '.$status ;
                                    fwrite($fp, $log_result."\r\n");
                                    fclose($fp);
                                } else {
                                    $fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'a+');
                                    $log_result = 'file: '.$image['image_path'].'  status: '.$status ;
                                    fwrite($fp, $log_result."\r\n");
                                    fclose($fp);
                                }
							} else {
                                db_query("DELETE FROM ?:all_store_images WHERE image_id = ?i", $image['image_id']);
                            }
						}
					}
					if (empty($cp_cron_run)) {
                        $save = 0;
                        $save = $_SESSION['compress_img']['save']/1048576;
					}
					$status_key = 'Ok';
					$save = number_format($save, 2, ',', ' ');
					if (empty($cp_cron_run)) {
                        fn_set_notification('W', __('important'), __('image_optimization_end', array(
                            '[update]' => $_SESSION['compress_img']['optimizated'],
                            '[skip]' => $_SESSION['compress_img']['skip'],
                            '[save_space]' => $save.'Mb',
                            '[total_errors]' => $_SESSION['compress_img']['errors'],
                            '[big_img_file]' => $_SESSION['compress_img']['big_img']
                        )));
					}
					if (!empty($invalid_key) && $invalid_key == 1) {
                        if (empty($cp_cron_run)) {
                            fn_set_notification('W', __('warning'), __('invalid_api_key'));
                        }
                        $status_key = 'Invalid api key';
                    }
                    if (!empty($quota_exceeded) && $quota_exceeded == 1) {
                        if (empty($cp_cron_run)) {
                            fn_set_notification('W', __('warning'), __('quota_exceeded'));
                        }
                        $status_key = 'Quota exceeded';
                    }
                    if (!file_exists('var/logs/cp_image_optimization/')) {
                        $structure = 'var/logs/cp_image_optimization/';
                        $oldmask = umask(0);
                        mkdir($structure, 0777, true);
                        umask($oldmask);
                    }
                    $today = date("d_m_y");
                    if (!file_exists('var/logs/cp_image_optimization/'.$today.'.txt')) {
                        $fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'wb');
                        $log_result = 'save: '.$save.'Mb';
                        fwrite($fp, $log_result."\r\n");
                        fwrite($fp, $status_key."\r\n");
                        fclose($fp);
                    } else {
                        $fp = fopen('var/logs/cp_image_optimization/'.$today.'.txt', 'a+');
                        $log_result = 'save: '.$save.'Mb';
                        fwrite($fp, $log_result."\r\n");
                        fwrite($fp, $status_key."\r\n");
                        fclose($fp);
                    }
				//31.05.2016
					/*
					$all_space = db_get_field("SELECT total_save FROM ?:optimize_image WHERE key_id = ?i", 1);
					$all_save = $_SESSION['compress_img']['save']/1048576;
					$$all_save = number_format($all_save, 3, '.', ' ');
					$new_save_space = $all_space + $all_save;
					db_query("UPDATE ?:optimize_image SET total_save = ?d WHERE key_id = ?i", $new_save_space, 1);
					*/
				//end
				} else {
                    if (empty($cp_cron_run)) {
                        fn_set_notification('W', __('warning'), __('invalid_api_key'));
					}
				}
			}
			if (empty($cp_cron_run)) {
                $_SESSION['compress_img'] = array();
			}
		}
		return true;
	}
	// deleting old images from store
    if (Registry::get('runtime.controller') == 'cp_image_optimization' && Registry::get('runtime.mode') == 'find_new' || (!empty($params['cron_compressing']) && $params['cron_compressing'] == 'Y') || (!empty($params['image_list_proc']) && $params['image_list_proc'] == 'Y')) {
		/*$old_store_images = db_get_fields("SELECT image_path FROM ?:all_store_images");
		$new_store_images = db_get_fields("SELECT image_path FROM ?:all_store_images_delete");
		$for_delete = array_diff($old_store_images, $new_store_images);
		if (!empty($for_delete)) {
			foreach ($for_delete as $delete) {
				db_query("DELETE FROM ?:all_store_images WHERE image_path = ?s AND db_image_id = ?i", $delete, 0);
			}
		}*/
		/*$all_db_image_ids = db_get_fields("SELECT image_id FROM ?:images");
		if (!empty($all_db_image_ids)) {
            db_query("DELETE FROM ?:all_store_images WHERE db_image_id > ?i AND db_image_id NOT IN (?n)", 0, $all_db_image_ids);
		}*/
    }
    return true;
}

