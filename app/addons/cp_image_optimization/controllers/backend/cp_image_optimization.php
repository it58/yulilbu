<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Bootstrap;
use Tygh\Http;
use Tygh\Registry;
use Tygh\Storage;

if (!defined('BOOTSTRAP')) { die('Access denied'); }


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$suffix = '';
	
	
	if ($mode == 'optimize_all' || $mode == 'find_new' || $mode == 'optimize_new' || $mode == 'optimize_errors') {
			$__params = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
			$params = array();
			$params = $_REQUEST;
            $params['choosen_optimizerer'] = !empty($__params['choosen_optimizerer']) ? $__params['choosen_optimizerer'] : 'B';
            $params['compressor'] = !empty($__params['compressor']) ? $__params['compressor'] : 'L';
            $params['img_quality'] = !empty($__params['img_quality']) ? $__params['img_quality'] : 90;
            $params['shortpixel_wait'] = !empty($__params['shortpixel_wait']) ? $__params['shortpixel_wait'] : 6;
            
			if (!empty($params['choosen_optimizerer'])) {
				$result = fn_cp_image_optimization_optimize_all_images($params);
				if (!$result) {
					$url = fn_url('cp_image_optimization.optimize_new&choosen_optimizerer='.$params['choosen_optimizerer']);
					@print '<meta http-equiv="Refresh" content="0;url='.$url.'">';
					exit;
				}
			}       
			$suffix = ".manage";
	}
	if ($mode == 'save_api_key') {
        $data = array(
            'key_id' => 1,
            'choosen_optimizerer' => !empty($_REQUEST['choosen_optimizerer']) ? $_REQUEST['choosen_optimizerer'] : 'B',
            'img_quality' => !empty($_REQUEST['img_quality']) ? $_REQUEST['img_quality'] : 90,
            'api_key_for_opt' => !empty($_REQUEST['api_key_for_opt']) ? $_REQUEST['api_key_for_opt'] : '',
            'compressor' => !empty($_REQUEST['compressor']) ? $_REQUEST['compressor'] : 'L',
            'shortpixel_wait' => !empty($_REQUEST['shortpixel_wait']) ? $_REQUEST['shortpixel_wait'] : 6,
            'cron_value' => !empty($_REQUEST['cron_value']) ? $_REQUEST['cron_value'] : 0,
            'cron_pass' => !empty($_REQUEST['cron_pass']) ? $_REQUEST['cron_pass'] : 0,
        );
        if (!empty($_REQUEST['cp_folders'])) {
            $data['folders'] = implode(',', $_REQUEST['cp_folders']);
        } else {
            $data['folders'] = '';
        }
        db_query("UPDATE ?:optimize_image SET ?u WHERE key_id = ?i", $data, $data['key_id']);
        //fn_cp_image_optimization_save_api_key($_REQUEST['api_key_for_opt']);
        $suffix = ".manage";
	}
	if ($mode == 'optim_few') {
		$params = $_REQUEST;
		if (!empty($params['cp_image_ids'])) {
			if (!is_array($params['cp_image_ids'])) {
				$params['cp_image_ids'] = array($params['cp_image_ids']);
			}
			$__params = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
			if (!empty($__params['choosen_optimizerer'])) {
                $params['choosen_optimizerer'] = $__params['choosen_optimizerer'];
            } else {
                $params['choosen_optimizerer'] = 'B';
            }
            if (!empty($dispatch_extra)) {
                $params['compressor'] = $dispatch_extra;
            } else {
                $params['compressor'] = !empty($__params['compressor']) ? $__params['compressor'] : 'L';
            }
            $params['img_quality'] = !empty($__params['img_quality']) ? $__params['img_quality'] : 90;
            $params['shortpixel_wait'] = !empty($__params['shortpixel_wait']) ? $__params['shortpixel_wait'] : 6;
            //fn_print_die($params);
			fn_cp_image_optimization_optimize_all_images($params);
		}
		if (!empty($_SERVER['HTTP_REFERER'])) {
			return array(CONTROLLER_STATUS_OK, $_SERVER['HTTP_REFERER']);
		} else {
			return array(CONTROLLER_STATUS_OK, "cp_image_optimization.image_list");
		}
	}
	if ($mode == 'add_new_img') {
		if (!empty($_FILES) && !empty($_REQUEST['image_data']) && !empty($_REQUEST['type_new_image_image_icon'][0]) && $_REQUEST['type_new_image_image_icon'][0] != 'url') {
			if (!empty($_FILES['file_new_image_image_icon']['name'][0])) {
				if (!empty($_FILES['file_new_image_image_icon']['tmp_name'][0])) {
					move_uploaded_file($_FILES['file_new_image_image_icon']['tmp_name'][0], './images/'.$_REQUEST['image_data']['dir'].'/'.$_REQUEST['image_data']['image_name']);
				} 
				$image_uploaded = true;
			}
		} elseif (!empty($_REQUEST['type_new_image_image_icon'][0]) && $_REQUEST['type_new_image_image_icon'][0] == 'url') {
			if (Bootstrap::getIniParam('allow_url_fopen') == true) {
				$result = @file_get_contents($_REQUEST['file_new_image_image_icon'][0]);
			} else {
				$result = Http::get($_REQUEST['file_new_image_image_icon'][0]);
			}
			//$result = file_get_contents($_REQUEST['file_new_image_image_icon'][0]);
			$fsp = fopen('./images/'.$_REQUEST['image_data']['dir'].'/'.$_REQUEST['image_data']['image_name'], 'wb');
			fwrite($fsp, $result);
			fclose($fsp);
			$image_uploaded = true;
		}
		if (!empty($image_uploaded)) {
			$delete_thumb = $_REQUEST['image_data']['dir'].'/'.$_REQUEST['image_data']['image_name'];
			fn_delete_image_thumbnails($delete_thumb);
		}
		if (!empty($_SERVER['HTTP_REFERER'])) {
			return array(CONTROLLER_STATUS_OK, $_SERVER['HTTP_REFERER']);
		} else {
			return array(CONTROLLER_STATUS_OK, "cp_image_optimization.image_list");
		}
	}
	return array(CONTROLLER_STATUS_OK, "cp_image_optimization$suffix");
	
}

if ($mode == 'manage') {
    $get_all = fn_cp_image_optimization_count_not_opt_images();
    $dir_list = scandir(DIR_ROOT . '/images');
    $folders = array();
    $all_img_types = db_get_fields("SELECT DISTINCT object_type FROM ?:images_links");
    foreach ($dir_list as $v) {
        if (is_dir(DIR_ROOT . '/images/' . $v) && strpos($v, '.') === false) {
            if (!in_array($v,$all_img_types) && $v != 'detailed' && $v != 'companies') {
                $folders[] = $v;
            }
        }
    }
    Registry::get('view')->assign('cron_info', fn_cp_image_optimization_cron_path_info()); 
    Registry::get('view')->assign('scan_folders', $folders);    
    Registry::get('view')->assign('get_all', $get_all);
}
if ($mode == 'optimize_new') {
	$__params = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
    $params = array();
    $params = $_REQUEST;
    $params['choosen_optimizerer'] = !empty($__params['choosen_optimizerer']) ? $__params['choosen_optimizerer'] : 'B';
    $params['compressor'] = !empty($__params['compressor']) ? $__params['compressor'] : 'L';
    $params['img_quality'] = !empty($__params['img_quality']) ? $__params['img_quality'] : 90;
    $params['shortpixel_wait'] = !empty($__params['shortpixel_wait']) ? $__params['shortpixel_wait'] : 6;
	if (!empty($params['choosen_optimizerer'])) {
		$params['continue_request'] = 4;
		$result = fn_cp_image_optimization_optimize_all_images($params);
		if (!$result) {
			$url = fn_url('cp_image_optimization.optimize_new&choosen_optimizerer='.$params['choosen_optimizerer']);
			$saving = $_SESSION['compress_img']['save']/1048576;
			$saving = number_format($saving, 4, ',', ' ');
			Registry::get('view')->assign('process', $_SESSION['compress_img']);
			Registry::get('view')->assign('saving', $saving);
			fn_echo('<meta http-equiv="Refresh" content="0;url='.$url.'">');
		} else {
			return array(CONTROLLER_STATUS_OK, "cp_image_optimization.manage");
		}
	}
}
if ($mode == 'cron_file_compressing') {
    $__params = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
    if (!empty($_GET['cron_pass']) && !empty($__params) && !empty($__params['cron_pass']) && $__params['cron_pass'] == $_GET['cron_pass']) {
        $params = array();
        $params['choosen_optimizerer'] = !empty($__params['choosen_optimizerer']) ? $__params['choosen_optimizerer'] : 'B';
        $params['compressor'] = !empty($__params['compressor']) ? $__params['compressor'] : 'L';
        $params['img_quality'] = !empty($__params['img_quality']) ? $__params['img_quality'] : 90;
        $params['shortpixel_wait'] = !empty($__params['shortpixel_wait']) ? $__params['shortpixel_wait'] : 6;
        $params['cron_value'] = !empty($__params['cron_value']) ? $__params['cron_value'] : 0;
        $params['cron_pass'] = !empty($__params['cron_pass']) ? $__params['cron_pass'] : 0;
        $params['cron_compressing'] = 'Y';
        $result = fn_cp_image_optimization_optimize_all_images($params);
    }
    exit;
}

if ($mode == 'list_find_new') {
	$params = $_REQUEST;
	$params['image_list_proc'] = 'Y';
	fn_cp_image_optimization_optimize_all_images($params);
	return array(CONTROLLER_STATUS_REDIRECT, 'cp_image_optimization.image_list');
	
} elseif ($mode == 'image_list') {
	$params = $_REQUEST;
	$params['image_list_proc'] = 'Y';
	
	//fn_cp_image_optimization_optimize_all_images($params);
	
	if (Registry::get('settings.Appearance.admin_elements_per_page') > 30 && empty($params['items_per_page'])) {
        list($get_image_list, $search) = fn_cp_image_optimization_get_images_for_list($params, 30);
    } else {
        list($get_image_list, $search) = fn_cp_image_optimization_get_images_for_list($params, Registry::get('settings.Appearance.admin_elements_per_page'));
    }
	$opt_choosen = db_get_field("SELECT choosen_optimizerer FROM ?:optimize_image WHERE key_id = ?i", 1);
	Registry::get('view')->assign('opt_choosen', $opt_choosen);
	Registry::get('view')->assign('search', $search);
	Registry::get('view')->assign('image_list', $get_image_list);
	
} elseif ($mode == 'optim_few') {
	$params = $_REQUEST;
	if (!empty($params['cp_image_ids'])) {
		if (!is_array($params['cp_image_ids'])) {
            $params['cp_image_ids'] = array($params['cp_image_ids']);
        }
        $__params = db_get_row("SELECT * FROM ?:optimize_image WHERE key_id = ?i", 1);
        if (!empty($__params['choosen_optimizerer'])) {
            $params['choosen_optimizerer'] = $__params['choosen_optimizerer'];
        } else {
            $params['choosen_optimizerer'] = 'B';
        }
        $params['img_quality'] = $__params['img_quality'];
        fn_cp_image_optimization_optimize_all_images($params);
	}
	if (!empty($_SERVER['HTTP_REFERER'])) {
		return array(CONTROLLER_STATUS_OK, $_SERVER['HTTP_REFERER']);
	} else {
		return array(CONTROLLER_STATUS_OK, "cp_image_optimization.image_list");
	}
} elseif ($mode == 'download_img') {
	
	$params = $_REQUEST;
	if (!empty($params['img_href'])) {
		$file = $params['img_href']; 
		header("Content-Description: File Transfer"); 
		header("Content-Type: application/octet-stream"); 
		header("Content-Disposition: attachment; filename=\"$file\"");
		readfile ($file); 
	}
	if (!empty($_SERVER['HTTP_REFERER'])) {
		return array(CONTROLLER_STATUS_OK, $_SERVER['HTTP_REFERER']);
	} else {
		return array(CONTROLLER_STATUS_OK, "cp_image_optimization.image_list");
	}
} elseif ($mode == 'convert_to_jpg') {
	exit;
	if (!empty($_REQUEST['image_name']) && !empty($_REQUEST['object_type']) && !empty($_REQUEST['object_id'])) {
		
		$ext = fn_get_file_ext($_REQUEST['image_name']);
		$ext = strtolower($ext);
		if (!empty($ext) && $ext == 'png') {
			if (!file_exists('images/cp_opt_temp/')) {
				$structure = 'images/cp_opt_temp/';
				$oldmask = umask(0);
				mkdir($structure, 0777, true);
				umask($oldmask);
			}
			$abs_path = Storage::instance('images')->getAbsolutePath('');
			if (Bootstrap::getIniParam('allow_url_fopen') == true) {
				$result = @file_get_contents($abs_path.''.$_REQUEST['img_dir'].'/'.$_REQUEST['image_name']);
			} else {
				$result = Http::get($abs_path.''.$_REQUEST['img_dir'].'/'.$_REQUEST['image_name']);
			}
			//$result = file_get_contents($abs_path.''.$_REQUEST['img_dir'].'/'.$_REQUEST['image_name']);
			fn_put_contents($abs_path.'cp_opt_temp/'.$_REQUEST['image_name'], $result);
			$th_filename = fn_cp_image_optimization_convert_png_to_jpg('cp_opt_temp/'.$_REQUEST['image_name'], 'cp_opt_temp/'.$_REQUEST['image_name']);
			if (!empty($_REQUEST['strore_image_id'])) {
				$alt_data = db_get_hash_array('SELECT * FROM ?:common_descriptions WHERE object_id = ?i AND object_holder = ?s', 'lang_code', $_REQUEST['strore_image_id'], 'images');
			}
			if (!empty($th_filename)) {
				$prefix = '';
				$image_file = '';
				$detailed_file = Storage::instance('images')->getUrl($th_filename);
				if (!empty($_REQUEST['img_pair_id'])) {
					$img_or_det = db_get_row("SELECT image_id, detailed_id FROM ?:images_links WHERE pair_id = ?i", $_REQUEST['img_pair_id']);
					if (!empty($img_or_det['image_id'])) {
						$image_file = Storage::instance('images')->getUrl($th_filename);
						$detailed_file = '';
					}
				} 
				$position = 0;
				$type = $_REQUEST['type'];
				$object_id = $_REQUEST['object_id'];
				$object = $_REQUEST['object_type'];
				
				//fn_delete_image_pairs($object_id, $object, $type);
				if (!empty($_REQUEST['strore_image_id']) && !empty($_REQUEST['img_pair_id'])) {
					fn_delete_image_pair($_REQUEST['img_pair_id'], $_REQUEST['object_type']);
					db_query("DELETE FROM ?:images WHERE image_id = ?i", $_REQUEST['strore_image_id']);
					db_query("DELETE FROM ?:common_descriptions WHERE object_id = ?i AND object_holder = 'images'", $_REQUEST['strore_image_id']);
					fn_delete_image_thumbnails($_REQUEST['img_dir'].'/'.$_REQUEST['image_name']);
				}
				
				$result = fn_cp_image_optimization_add_images($prefix, $image_file, $detailed_file, $position, $type, $object_id, $object);
				Storage::instance('images')->delete('cp_opt_temp/'.$_REQUEST['image_name']);
				Storage::instance('images')->delete($th_filename);
				if (!empty($result[0]) && !empty($alt_data)) {
					$alt_img_or_det = db_get_row("SELECT image_id, detailed_id FROM ?:images_links WHERE pair_id = ?i", $result[0]);
					if (!empty($alt_img_or_det['image_id'])) {
						$mg_id_for_alt = $alt_img_or_det['image_id'];
					} elseif (!empty($alt_img_or_det['detailed_id'])) {
						$mg_id_for_alt = $alt_img_or_det['detailed_id'];
					}
					if (!empty($mg_id_for_alt)) {
						foreach($alt_data as $alt_d) {
							db_query("UPDATE ?:common_descriptions SET description = ?s WHERE object_id = ?i AND lang_code = ?s AND object_holder = ?s", $alt_d['description'], $mg_id_for_alt, $alt_d['lang_code'], $alt_d['object_holder']);
						}
					}
				}
				fn_set_notification('N', __('notice'), __('cp_convert_to_jpg').' '.__('completed'));
			}
		}
	}
	if (!empty($_SERVER['HTTP_REFERER'])) {
		return array(CONTROLLER_STATUS_OK, $_SERVER['HTTP_REFERER']);
	} else {
		return array(CONTROLLER_STATUS_OK, "cp_image_optimization.image_list");
	}
}





