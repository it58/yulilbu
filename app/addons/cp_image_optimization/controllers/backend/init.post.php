<?php

use Tygh\Registry;
use Tygh\Debugger;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (isset($_REQUEST['ct']) && ((AREA == 'A' && !(fn_allowed_for('MULTIVENDOR') && Registry::get('runtime.company_id'))) || Debugger::isActive() || fn_is_development())) {
    db_query("DELETE FROM ?:all_store_images WHERE object_type = ?s", 'thumbnail');
}