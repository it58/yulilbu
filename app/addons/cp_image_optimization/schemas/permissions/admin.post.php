<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['cp_image_optimization'] = array (
    'modes' => array(
        'optimize_all' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'find_new' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'optimize_new' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'optimize_errors' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'save_api_key' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'add_new_img' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'optim_few' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'manage' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'list_find_new' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'image_list' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'download_img' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
        'convert_to_jpg' => array (
            'permissions' => 'manage_cp_image_opt'
        ),
    ),
);
return $schema;
