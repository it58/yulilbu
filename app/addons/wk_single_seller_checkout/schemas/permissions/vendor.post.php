<?php
/******************************************************************
# Single Seller Checkout     ---    Single Seller Checkout        *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/
use Tygh\Registry;

if(Registry::get('addons.wk_single_seller_checkout.enable_payment_action') == 'Y') {
	$schema['controllers']['wk_single_seller_checkout']['modes']['update_status']['permissions'] = true;
}
return $schema;

?>