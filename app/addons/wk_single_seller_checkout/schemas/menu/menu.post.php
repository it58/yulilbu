<?php
/******************************************************************
# Single Seller Checkout     ---    Single Seller Checkout        *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/

use Tygh\Registry;

if(Registry::get('runtime.company_id')) {
	$schema['central']['orders']['items']['payment'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'wk_single_seller_checkout.payment',
    'position' => 800,
);
}

return $schema;
