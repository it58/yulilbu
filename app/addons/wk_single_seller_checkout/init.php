<?php
/******************************************************************
# Single Seller Checkout     ---    Single Seller Checkout        *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/                                                                                                  


if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
	'check_add_to_cart_pre',
	'pre_place_order',
	'prepare_checkout_payment_methods',
	'checkout_select_default_payment_method'
);