<?php
/******************************************************************
# Single Seller Checkout     ---    Single Seller Checkout        *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if($mode == 'update_status') {
		if(isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {
			list($company_id,$payment_id) = explode("-", $_REQUEST['id']);
			if($_REQUEST['status'] == 'A') {
				db_query('DELETE FROM ?:vendor_restricted_payment WHERE payment_id = ?i AND company_id = ?i',$payment_id,$company_id);
			} else {
				$data = array('payment_id' => $payment_id, 'company_id' => $company_id);
				db_query('INSERT INTO ?:vendor_restricted_payment ?e',$data);
			}
			fn_set_notification('N',__(""),__("status_changed"));
			Tygh::$app['ajax']->assign('return_status', $_REQUEST['status']);

		} else {
			fn_set_notification('E',__(""),__("status_not_changed"));
			Tygh::$app['ajax']->assign('return_status', $_REQUEST['status']);
		}
		exit;
	}
}
?>