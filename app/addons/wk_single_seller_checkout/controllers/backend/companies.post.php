<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }


if($mode == 'update') {
	if(Registry::get('addons.wk_single_seller_checkout.enable_payment_action') == 'Y') {
	$tabs = Registry::get('navigation.tabs');
	$tabs['payment_method'] = array(
            'title' => __('payment_method'),
            'js' => true
        );
	if (ACCOUNT_TYPE == 'vendor') {
        unset($tabs['payment_method']);
    }
    Registry::set('navigation.tabs', $tabs);
	
	$payments = fn_get_payments(array('status' => 'A'));
	$disable_payments = fn_wk_ssc_fn_get_vendor_disable_paymet($_REQUEST['company_id']);

	if(!empty($disable_payments) && is_array($disable_payments)) {
		foreach ($payments as $key => $payment) {
			if(in_array($payment['payment_id'], $disable_payments))
					$payments[$key]['status'] = 'D'; 
		}
	}
	
    Registry::get('view')->assign('payments',$payments);}
}