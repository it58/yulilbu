<?php
/******************************************************************
# Single Seller Checkout   ---    Single Seller Checkout          *
# ----------------------------------------------------------------*
# author    Webkul                                                *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL     *
# Websites: http://webkul.com                                     *
*******************************************************************
*/
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_wk_single_seller_checkout_pre_place_order($cart, &$allow, $product_groups) {
    if(count($product_groups) > 1 && AREA != 'A') {
        fn_set_notification('W',__("warning"), "you can checkout with only one vendor products");

        $allow = false;
    }
}

function fn_wk_single_seller_checkout_check_add_to_cart_pre($cart, $product, $product_id, &$result) {
    if(AREA != 'A') {
    if(!empty($cart['products']) && count($cart['product_groups']) > 1) {
        $result = false;
        fn_set_notification('W',__("warning"), "cart can only contain products from single vendor, please clear your cart first");
    } elseif(!empty($cart['products']) && count($cart['product_groups']) == 1) {
        if($cart['product_groups'][0]['company_id'] != fn_wk_ssc_fn_get_product_company_name($product_id)) {
            $result = false;
            fn_set_notification('W',__("warning"), "cart can only contain products from single vendor, please clear your cart first");
        }
    }}
}

function fn_wk_ssc_fn_get_product_company_name($product_id) {
    return db_get_field("SELECT company_id FROM ?:products WHERE product_id = ?i",$product_id);
}

function fn_wk_ssc_fn_get_vendor_disable_paymet($company_id) {
    return db_get_fields("SELECT payment_id FROM ?:vendor_restricted_payment WHERE company_id = ?i",$company_id);
}

function fn_wk_single_seller_checkout_prepare_checkout_payment_methods(&$cart, &$auth, &$payment_groups)
{
    if(AREA != 'A' && !empty($cart['products']) && isset($cart['product_groups']) && !empty($cart['product_groups']) && Registry::get('addons.wk_single_seller_checkout.enable_payment_action') == 'Y')
    {
        $disable_payment_ids = db_get_fields('SELECT payment_id FROM ?:vendor_restricted_payment WHERE company_id = ?i',$cart['product_groups'][0]['company_id']);
        if ($disable_payment_ids) {
            foreach ($payment_groups as $g_key => $group) {
                foreach ($group as $p_key => $payment) {
                    if (in_array($payment['payment_id'], $disable_payment_ids)) {
                        unset($payment_groups[$g_key][$p_key]);
                    }
                }
                if (empty($payment_groups[$g_key])) {
                    unset($payment_groups[$g_key]);
                }
            }
        }
    }
}
function fn_wk_single_seller_checkout_checkout_select_default_payment_method(&$cart, &$payment_methods, &$completed_steps)
{
    if(Registry::get('addons.wk_single_seller_checkout.enable_payment_action') == 'Y') {
        $available_payment_ids = array();
        foreach ($payment_methods as $group) {
            foreach ($group as $method) {
                $available_payment_ids[] = $method['payment_id'];
            }
        }
        
        // Change default payment if it doesn't exists
        if (!in_array($cart['payment_id'], $available_payment_ids)) {
            $cart['payment_id'] = reset($available_payment_ids);
            $cart['payment_method_data'] = fn_get_payment_method_data($cart['payment_id']);
        }
    }
}


function fn_get_active_payment() {

    $payments = fn_get_payments($params = array('status' => 'A'));
    $return_payment = array();

    foreach ($payments as $key => $payment) {
        $return_payment[$payment['payment_id']] = $payment['payment'];
    }

    return $return_payment;
}

function fn_get_restricted_payment_method_tpl() {
    $restricted_payments = Registry::get('addons.wk_single_seller_checkout.restricted_payment');
    foreach ($restricted_payments as $payment_id => $value) {
        $restricted_payments[$payment_id] = $payment_id;
    }
    return $restricted_payments;
}