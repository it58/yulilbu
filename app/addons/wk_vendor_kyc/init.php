<?php
/******************************************************************
# Vendor Kyc   ---      Vendor KYC                                *
# ----------------------------------------------------------------*
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.   *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL         *
# Websites: http://webkul.com                                     *
*******************************************************************
*/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
fn_register_hooks(
	'change_company_status_before_mail',
	'get_companies'
);

Registry::set('config.storage.wk_vendor_kyc', array(
    'prefix' => 'wk_vendor_kyc',
    'secured' => true,
    'dir' => Registry::get('config.dir.var')
));
