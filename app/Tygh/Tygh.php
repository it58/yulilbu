<?php

namespace Tygh;

<<<<<<< HEAD
use InvalidArgumentException;
use Tygh\Core\ApplicationInterface;

=======
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
/**
 * Class Tygh is an utility class for creation and convenient access for currently running Application class instance.
 *
 * @package Tygh
 */
<<<<<<< HEAD
class Tygh
{
=======
class Tygh {

>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    /**
     * @var Application
     */
    public static $app;

    /**
     * Creates application object and registers it at static variable.
     *
<<<<<<< HEAD
     * @param string $class     Application class name
     * @param string $root_path Application root directory path
     *
     * @return Application
     */
    public static function createApplication($app_fqcn = '\Tygh\Application', $root_path = null)
    {
        if ($root_path === null) {
            if (defined('DIR_ROOT')) {
                $root_path = DIR_ROOT;
            } else {
                $root_path = dirname(dirname(__DIR__));
            }
        }

        if (!is_a($app_fqcn, ApplicationInterface::class, true)) {
            throw new InvalidArgumentException(sprintf(
                'Application class must implement the %s interface.',
                ApplicationInterface::class
            ));
        }

        self::$app = new $app_fqcn($root_path);
=======
     * @param string $class Application class name
     *
     * @return Application
     */
    public static function createApplication($class = '\Tygh\Application')
    {
        self::$app = new $class();
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

        Registry::setAppInstance(self::$app);

        return self::$app;
    }
}