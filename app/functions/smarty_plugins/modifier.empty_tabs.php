<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     modifier<br>
 * Name:     empty_tabs<br>
 * Purpose:  find ids of empty tabs
 * Example:  {$a|empty_tabs}
 * -------------------------------------------------------------
 */

function smarty_modifier_empty_tabs($content)
{
    if (!empty($content)) {
<<<<<<< HEAD
        preg_match_all('/\<div id="([\w]*)"( class="[\w\- ]*")*>[\n\r\t ]*(\<\!--([\w]*)--\>)?[\n\r\t ]*\<\/div>/is', $content, $matches);
=======
        preg_match_all('/\<div id="([\w]*)"( class="[\w- ]*")*>[\n\r\t ]*(\<\!--([\w]*)--\>)?[\n\r\t ]*\<\/div>/is', $content, $matches);
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868

        if (!empty($matches[1])) {
            return array_map('smarty_change_tab_id', $matches[1]);
        }
    }

    return array();
}

function smarty_change_tab_id($str)
{
    return substr($str, strpos($str, '_') + 1);
}

/* vim: set expandtab: */
