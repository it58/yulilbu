<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 10:54:38
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/hooks/companies/detailed_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18645824095d4a4b7eed17f9-55114822%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'da87afa569b31791935a20edf00134d042fb251f' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/hooks/companies/detailed_content.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '18645824095d4a4b7eed17f9-55114822',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'addons' => 0,
    'runtime' => 0,
    'obj_id' => 0,
    'id' => 0,
    'company_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a4b7eef7626_55690819',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a4b7eef7626_55690819')) {function content_5d4a4b7eef7626_55690819($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('messenger_system.messenger_for_vendor','status','active','disabled','status','active','disabled'));
?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['vendor_plans']['status']=='A') {?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("messenger_system.messenger_for_vendor"),'target'=>"#messenger_vendor_setting"), 0);?>

    <div id="messenger_vendor_setting" class="in collapse">
        <fieldset>

        <?php if (!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
            <div class="control-group">
                <label class="control-label cm-required"><?php echo $_smarty_tpl->__("status");?>
:</label>
                <div class="controls">
                    <label class="radio inline" for="company_data_messenger_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_a"><input type="radio" name="company_data[messenger]" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_a" <?php if ($_smarty_tpl->tpl_vars['company_data']->value['messenger']=="A") {?>checked="checked"<?php }?> value="A" /><?php echo $_smarty_tpl->__("active");?>
</label>
                    <label class="radio inline" for="company_data_messenger_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_d"><input type="radio" name="company_data[messenger]" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['obj_id']->value)===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
_d" <?php if ($_smarty_tpl->tpl_vars['company_data']->value['messenger']=="D") {?>checked="checked"<?php }?> value="D" /><?php echo $_smarty_tpl->__("disabled");?>
</label>
                </div>
            </div>
        <?php } else { ?>
            <div class="control-group">
                <label class="control-label"><?php echo $_smarty_tpl->__("status");?>
:</label>
                <div class="controls">
                    <label class="radio"><input type="radio" checked="checked" /><?php if ($_smarty_tpl->tpl_vars['company_data']->value['messenger']=="A") {
echo $_smarty_tpl->__("active");
} elseif ($_smarty_tpl->tpl_vars['company_data']->value['messenger']=="D") {
echo $_smarty_tpl->__("disabled");
}?></label>
                </div>
            </div>
        <?php }?>

        </fieldset>
    </div>
<?php }?><?php }} ?>
