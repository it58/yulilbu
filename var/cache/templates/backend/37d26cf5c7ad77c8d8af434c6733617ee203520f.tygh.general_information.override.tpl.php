<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 10:54:38
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/wk_vendor_custom_registration/hooks/companies/general_information.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6912446485d4a4b7e4867a3-31604794%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '37d26cf5c7ad77c8d8af434c6733617ee203520f' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/wk_vendor_custom_registration/hooks/companies/general_information.override.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '6912446485d4a4b7e4867a3-31604794',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wk_vendor_custom_fields' => 0,
    'extra_fields' => 0,
    'runtime' => 0,
    'company_data' => 0,
    'id' => 0,
    'theme_info' => 0,
    'current_style' => 0,
    'profile_fields' => 0,
    'ship_to_another' => 0,
    'all_sections' => 0,
    'section_data' => 0,
    'section_id' => 0,
    'company_settings' => 0,
    'item' => 0,
    'section' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a4b7e500446_98403466',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a4b7e500446_98403466')) {function content_5d4a4b7e500446_98403466($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_unpuny')) include '/home/yulibu/public_html/app/functions/smarty_plugins/modifier.unpuny.php';
if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('storefront_url','ttc_storefront_url','secure_storefront_url','design','store_theme','goto_theme_configuration','status','active','pending','new','disabled','create_administrator_account','contact_information','contact_information','shipping_address','shipping_address','settings','company'));
?>

<?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"O",'title'=>'','wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value,'extra_fields'=>$_smarty_tpl->tpl_vars['extra_fields']->value), 0);?>


<?php if (fn_allowed_for("ULTIMATE")) {?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:storefronts")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:storefronts"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="control-group">
        <label for="elm_company_storefront" class="control-label cm-required"><?php echo $_smarty_tpl->__("storefront_url");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("ttc_storefront_url")), 0);?>
:</label>
        <div class="controls">
        <?php if ($_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
            http://<?php echo htmlspecialchars(smarty_modifier_unpuny($_smarty_tpl->tpl_vars['company_data']->value['storefront']), ENT_QUOTES, 'UTF-8');?>

        <?php } else { ?>
            <input type="text" name="company_data[storefront]" id="elm_company_storefront" size="32" value="<?php echo htmlspecialchars(smarty_modifier_unpuny($_smarty_tpl->tpl_vars['company_data']->value['storefront']), ENT_QUOTES, 'UTF-8');?>
" class="input-large" placeholder="http://" />
        <?php }?>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_company_secure_storefront"><?php echo $_smarty_tpl->__("secure_storefront_url");?>
:</label>
        <div class="controls">
        <?php if ($_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
            https://<?php echo htmlspecialchars(smarty_modifier_unpuny($_smarty_tpl->tpl_vars['company_data']->value['secure_storefront']), ENT_QUOTES, 'UTF-8');?>

        <?php } else { ?>
            <input type="text" name="company_data[secure_storefront]" id="elm_company_secure_storefront" size="32" value="<?php echo htmlspecialchars(smarty_modifier_unpuny($_smarty_tpl->tpl_vars['company_data']->value['secure_storefront']), ENT_QUOTES, 'UTF-8');?>
" class="input-large" placeholder="https://" />
        <?php }?>
        </div>
    </div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:storefronts"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:storefronts_design")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:storefronts_design"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("design")), 0);?>


    <div class="control-group">
        <label class="control-label"><?php echo $_smarty_tpl->__("store_theme");?>
:</label>
        <div class="controls">
            <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['theme_info']->value['title'], ENT_QUOTES, 'UTF-8');?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['current_style']->value['name'], ENT_QUOTES, 'UTF-8');?>
</p>
            <a href="<?php echo htmlspecialchars(fn_url("themes.manage?switch_company_id=".((string)$_smarty_tpl->tpl_vars['id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("goto_theme_configuration");?>
</a>
        </div>
    </div>
    <?php } else { ?>
        
        <input type="hidden" value="responsive" name="company_data[theme_name]">
    <?php }?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:storefronts_design"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php }?>

<?php if (fn_allowed_for("MULTIVENDOR")) {?>
    <?php if (!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/select_status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"company_data[status]",'id'=>"company_data",'obj'=>$_smarty_tpl->tpl_vars['company_data']->value,'items_status'=>fn_get_predefined_statuses("companies",$_smarty_tpl->tpl_vars['company_data']->value['status'])), 0);?>

    <?php } else { ?>
        <div class="control-group">
            <label class="control-label"><?php echo $_smarty_tpl->__("status");?>
:</label>
            <div class="controls">
                <label class="radio"><input type="radio" checked="checked" /><?php if ($_smarty_tpl->tpl_vars['company_data']->value['status']=="A") {
echo $_smarty_tpl->__("active");
} elseif ($_smarty_tpl->tpl_vars['company_data']->value['status']=="P") {
echo $_smarty_tpl->__("pending");
} elseif ($_smarty_tpl->tpl_vars['company_data']->value['status']=="N") {
echo $_smarty_tpl->__("new");
} elseif ($_smarty_tpl->tpl_vars['company_data']->value['status']=="D") {
echo $_smarty_tpl->__("disabled");
}?></label>
            </div>
        </div>
    <?php }?>

  
<?php }?>


<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
    function fn_toggle_required_fields()
    {
        var $ = Tygh.$;
        var checked = $('#company_description_vendor_admin').prop('checked');

        $('#company_description_username').prop('disabled', !checked);
        $('#company_description_first_name').prop('disabled', !checked);
        $('#company_description_last_name').prop('disabled', !checked);

        $('.cm-profile-field').each(function(index){
            $('#' + Tygh.$(this).prop('for')).prop('disabled', !checked);
        });
    }

    function fn_switch_store_settings(elm)
    {
        jelm = Tygh.$(elm);
        var close = true;
        if (jelm.val() != 'all' && jelm.val() != '' && jelm.val() != 0) {
            close = false;
        }
        
        Tygh.$('#clone_settings_container').toggleBy(close);
    }

    function fn_check_dependence(object, enabled)
    {
        if (enabled) {
            Tygh.$('.cm-dependence-' + object).prop('checked', 'checked').prop('readonly', true).on('click', function(e) {
                return false
            });
        } else {
            Tygh.$('.cm-dependence-' + object).prop('readonly', false).off('click');
        }
    }
    <?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    

    <?php if (!fn_allowed_for("ULTIMATE")) {?>
        <div class="control-group">
            <label class="control-label" for="elm_company_vendor_admin"><?php echo $_smarty_tpl->__("create_administrator_account");?>
:</label>
            <div class="controls">
                <label class="checkbox">
                    <input type="checkbox" name="company_data[is_create_vendor_admin]" id="elm_company_vendor_admin" checked="checked" value="Y" onchange="fn_toggle_required_fields();" />
                </label>
            </div>
        </div>
    <?php }?>
<?php }?>

<?php if (fn_allowed_for("MULTIVENDOR")) {?>
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:contact_information")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:contact_information"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profile_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"C",'title'=>$_smarty_tpl->__("contact_information")), 0);?>

    <?php } else { ?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("contact_information")), 0);?>

    <?php }?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"C",'title'=>''), 0);?>


    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:contact_information"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:shipping_address")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:shipping_address"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profile_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"B",'title'=>$_smarty_tpl->__("shipping_address"),'shipping_flag'=>false), 0);?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("shipping_address")), 0);?>

        <?php }?>
        <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>'S','body_id'=>'','ship_to_another'=>$_smarty_tpl->tpl_vars['ship_to_another']->value,'title'=>'','address_flag'=>fn_compare_shipping_billing($_smarty_tpl->tpl_vars['profile_fields']->value),'wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value,'extra_fields'=>$_smarty_tpl->tpl_vars['extra_fields']->value), 0);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:shipping_address"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php }?>
<?php if (!empty($_smarty_tpl->tpl_vars['all_sections']->value)) {?>
    <?php  $_smarty_tpl->tpl_vars["section_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["section_data"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['all_sections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["section_data"]->key => $_smarty_tpl->tpl_vars["section_data"]->value) {
$_smarty_tpl->tpl_vars["section_data"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["section_data"]->key;
?>
        <?php $_smarty_tpl->tpl_vars["section_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['section_data']->value['section_id'], null, 0);?> 
        <?php if (!empty($_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value[$_smarty_tpl->tpl_vars['section_id']->value])) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>$_smarty_tpl->tpl_vars['section_data']->value['section_id'],'title'=>$_smarty_tpl->tpl_vars['section_data']->value['description'],'wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value,'extra_fields'=>$_smarty_tpl->tpl_vars['extra_fields']->value), 0);?>

       <?php }?>
    <?php } ?>
<?php }?>
<?php if (fn_allowed_for("ULTIMATE")) {?>
    <?php ob_start();
echo $_smarty_tpl->__("settings");
$_tmp5=ob_get_clean();?><?php ob_start();
echo $_smarty_tpl->__("company");
$_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp5.": ".$_tmp6), 0);?>

    
    <?php  $_smarty_tpl->tpl_vars["item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item"]->_loop = false;
 $_smarty_tpl->tpl_vars["field_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['company_settings']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["item"]->key => $_smarty_tpl->tpl_vars["item"]->value) {
$_smarty_tpl->tpl_vars["item"]->_loop = true;
 $_smarty_tpl->tpl_vars["field_id"]->value = $_smarty_tpl->tpl_vars["item"]->key;
?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/settings_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('item'=>$_smarty_tpl->tpl_vars['item']->value,'section'=>"Company",'html_id'=>"field_".((string)$_smarty_tpl->tpl_vars['section']->value)."_".((string)$_smarty_tpl->tpl_vars['item']->value['name'])."_".((string)$_smarty_tpl->tpl_vars['item']->value['object_id']),'html_name'=>"update[".((string)$_smarty_tpl->tpl_vars['item']->value['object_id'])."]"), 0);?>

    <?php } ?>
<?php }?>

          


                    <?php }} ?>
