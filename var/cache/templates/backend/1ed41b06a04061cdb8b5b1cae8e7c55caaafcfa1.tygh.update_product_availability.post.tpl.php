<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:40
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_shipping_by_product/hooks/products/update_product_availability.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13692496515d49804cbea079-96316188%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ed41b06a04061cdb8b5b1cae8e7c55caaafcfa1' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_shipping_by_product/hooks/products/update_product_availability.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '13692496515d49804cbea079-96316188',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shipping_methods' => 0,
    'product_data' => 0,
    'shipping_id' => 0,
    'shipping_name' => 0,
    'delivery_time_list' => 0,
    'delivery_time_id' => 0,
    'delivery_time' => 0,
    'all_available_countries' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d49804cc13f47_62025223',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d49804cc13f47_62025223')) {function content_5d49804cc13f47_62025223($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('addons.sd_shipping_by_product.product_shipping','shipping_method','shipping_cost','ttc_shipping_cost','delivery_time','addons.sd_shipping_by_product.available_countries'));
?>
<hr>
<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("addons.sd_shipping_by_product.product_shipping"),'target'=>"#acc_shipping_details"), 0);?>

<div id="acc_shipping_details" class="collapse in">
    <div class="control-group">
        <label class="control-label cm-required" for="elm_shipping_method"><?php echo $_smarty_tpl->__("shipping_method");?>
:</label>
        <div class="controls">
            <select class="span5" id="elm_shipping_method" name="product_data[shipping_id]">
                <option value=""><?php echo $_smarty_tpl->__('addons.sd_shipping_by_product.please_select_shipping');?>
</option>
                <?php  $_smarty_tpl->tpl_vars["shipping_name"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["shipping_name"]->_loop = false;
 $_smarty_tpl->tpl_vars["shipping_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['shipping_methods']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["shipping_name"]->key => $_smarty_tpl->tpl_vars["shipping_name"]->value) {
$_smarty_tpl->tpl_vars["shipping_name"]->_loop = true;
 $_smarty_tpl->tpl_vars["shipping_id"]->value = $_smarty_tpl->tpl_vars["shipping_name"]->key;
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['product_data']->value['shipping_id']==$_smarty_tpl->tpl_vars['shipping_id']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping_name']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label cm-required" for="elm_shipping_cost"><?php echo $_smarty_tpl->__("shipping_cost");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("ttc_shipping_cost")), 0);?>
:</label>
        <div class="controls">
            <input type="text" name="product_data[shipping_cost]" id="elm_shipping_cost" size="10" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['shipping_cost'], ENT_QUOTES, 'UTF-8');?>
" class="input-small" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label cm-required" for="elm_delivery_time"><?php echo $_smarty_tpl->__("delivery_time");?>
:</label>
        <div class="controls">
            <select class="span5" id="elm_delivery_time" name="product_data[delivery_time]">
                <?php  $_smarty_tpl->tpl_vars["delivery_time"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["delivery_time"]->_loop = false;
 $_smarty_tpl->tpl_vars["delivery_time_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_time_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["delivery_time"]->key => $_smarty_tpl->tpl_vars["delivery_time"]->value) {
$_smarty_tpl->tpl_vars["delivery_time"]->_loop = true;
 $_smarty_tpl->tpl_vars["delivery_time_id"]->value = $_smarty_tpl->tpl_vars["delivery_time"]->key;
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['product_data']->value['delivery_time']==$_smarty_tpl->tpl_vars['delivery_time_id']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_time_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_time']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div id="available_countries">
        <?php echo $_smarty_tpl->getSubTemplate ("common/double_selectboxes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("addons.sd_shipping_by_product.available_countries"),'first_name'=>"product_data[available_countries]",'first_data'=>$_smarty_tpl->tpl_vars['product_data']->value['available_countries'],'second_name'=>"all_countries",'second_data'=>$_smarty_tpl->tpl_vars['all_available_countries']->value), 0);?>

    </div>
</div><?php }} ?>
