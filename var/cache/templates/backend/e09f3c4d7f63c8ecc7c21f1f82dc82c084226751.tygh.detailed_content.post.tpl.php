<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:41
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/rma/hooks/products/detailed_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8256172095d49804d955d46-41512374%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e09f3c4d7f63c8ecc7c21f1f82dc82c084226751' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/rma/hooks/products/detailed_content.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '8256172095d49804d955d46-41512374',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_data' => 0,
    'runtime' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d49804d96b147_27618781',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d49804d96b147_27618781')) {function content_5d49804d96b147_27618781($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('rma','returnable','return_period_days'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("rma"),'target'=>"#acc_addon_rma"), 0);?>

<div id="acc_addon_rma" class="collapse in">
<div class="control-group">
    <label class="control-label" for="is_returnable"><?php echo $_smarty_tpl->__("returnable");?>
:</label>
    <div class="controls">
        <label class="checkbox">
        <input type="hidden" name="product_data[is_returnable]" id="is_returnable" value="N" />
        <input type="checkbox" name="product_data[is_returnable]" value="Y" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['is_returnable']=="Y"||$_smarty_tpl->tpl_vars['runtime']->value['mode']=="add") {?>checked="checked"<?php }?> onclick="Tygh.$.disable_elms(['return_period'], !this.checked);" />
        </label>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="return_period"><?php echo $_smarty_tpl->__("return_period_days");?>
:</label>
    <div class="controls">
        <input type="text" id="return_period" name="product_data[return_period]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['return_period'])===null||$tmp==='' ? "10" : $tmp), ENT_QUOTES, 'UTF-8');?>
" size="10"  <?php if ($_smarty_tpl->tpl_vars['product_data']->value['is_returnable']!="Y"&&$_smarty_tpl->tpl_vars['runtime']->value['mode']!="add") {?>disabled="disabled"<?php }?> />
    </div>
</div>
</div>
<?php }} ?>
