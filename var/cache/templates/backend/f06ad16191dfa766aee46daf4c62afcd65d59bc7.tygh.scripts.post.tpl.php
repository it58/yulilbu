<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:54:25
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_follow_vendor/hooks/index/scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3828122795d484321b95e97-12605208%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f06ad16191dfa766aee46daf4c62afcd65d59bc7' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_follow_vendor/hooks/index/scripts.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3828122795d484321b95e97-12605208',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d484321b9b3d7_17661418',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d484321b9b3d7_17661418')) {function content_5d484321b9b3d7_17661418($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
(function(_, $) {

    $(document).ready(function() {
        $(_.doc).on('click', '.cm-status-switch', function (e) {
            e.stopImmediatePropagation();

            var jelm = $(e.target),
                productData = $("input[name='product_id']").val(),
                companyData = $('#product_data_company_id').val(),
                companyId = (typeof companyData !== 'undefined') ? companyData : $('#compId_' + jelm.data('caPostId')).val(),
                productId = (typeof productData !== 'undefined') ? productData : $('#prodId_' + jelm.data('caPostId')).val();
            
            $.ceAjax('request', fn_url('tools.update_status'), {
                method: 'post',
                obj: jelm,
                status: jelm.data('caStatus'),
                data: {
                    'table': 'discussion_posts',
                    'product_id': productId,
                    'company_id': companyId,
                    'id_name': 'post_id',
                    'id': jelm.data('caPostId'),
                    'status': jelm.data('caStatus')
                },
                callback: function (data, params) {
                    if (params.obj) {
                        var $ = Tygh.$;
                        var status = (data.return_status) ? data.return_status : params.status;
                        var s_elm = $(params.obj).parents('.cm-statuses:first');
                        s_elm.children('[class^="cm-status-"]').hide();
                        s_elm.children('.cm-status-' + status.toLowerCase()).show();
                    }
                }
            });
        });
    });

}(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>
