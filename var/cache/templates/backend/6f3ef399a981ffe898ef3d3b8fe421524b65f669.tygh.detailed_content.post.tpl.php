<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:41
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_accelerated_pages/hooks/products/detailed_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19466160945d49804d985dc9-86255910%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f3ef399a981ffe898ef3d3b8fe421524b65f669' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_accelerated_pages/hooks/products/detailed_content.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19466160945d49804d985dc9-86255910',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'auth' => 0,
    'product_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d49804d9a3429_37769879',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d49804d9a3429_37769879')) {function content_5d49804d9a3429_37769879($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('sd_accelerated_pages.amp','sd_accelerated_pages.full_description'));
?>
<?php if ($_smarty_tpl->tpl_vars['auth']->value['user_type']=="A") {?>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("sd_accelerated_pages.amp"),'target'=>"#sd_accelerated_pages"), 0);?>

<div id="sd_accelerated_pages" class="in collapse">
    <fieldset>
        <div class="control-group cm-no-hide-input">
            <label class="control-label" for="elm_product_amp_descr"><?php echo $_smarty_tpl->__("sd_accelerated_pages.full_description");?>
:</label>
            <div class="controls">
                <textarea id="elm_product_amp_descr" name="product_data[amp_description]" cols="55" rows="8" class="cm-wysiwyg input-large"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['amp_description'], ENT_QUOTES, 'UTF-8');?>
</textarea>
            </div>
        </div>
    </fieldset>
</div>
<?php }?><?php }} ?>
