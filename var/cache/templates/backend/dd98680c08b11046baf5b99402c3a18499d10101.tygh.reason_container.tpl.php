<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 10:54:53
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/vendor_data_premoderation/views/premoderation/components/reason_container.tpl" */ ?>
<?php /*%%SmartyHeaderCode:730982965d4a4b8dd83508-83888048%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd98680c08b11046baf5b99402c3a18499d10101' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/vendor_data_premoderation/views/premoderation/components/reason_container.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '730982965d4a4b8dd83508-83888048',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'type' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a4b8dd8c924_83304348',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a4b8dd8c924_83304348')) {function content_5d4a4b8dd8c924_83304348($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('reason','notify_vendors_by_email'));
?>
<div class="form-horizontal form-edit">
	<div class="control-group">
	    <label class="control-label" ><?php echo $_smarty_tpl->__("reason");?>
:</label>
	    <div class="controls">
	    	<textarea name="action_reason_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" id="action_reason_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" cols="50" rows="4" class="input-text input-large"></textarea>
	    </div>
	</div>
	
	<div class="control-group cm-toggle-button">
	    <label class="control-label" for="action_notification_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("notify_vendors_by_email");?>
</label>
	    <div class="controls">
	    	<input type="checkbox" name="action_notification_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" id="action_notification_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" value="Y" checked="checked">
	    </div>
	</div>
</div><?php }} ?>
