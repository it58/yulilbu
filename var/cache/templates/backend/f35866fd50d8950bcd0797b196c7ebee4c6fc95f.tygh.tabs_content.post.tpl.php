<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:41
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/ebay/hooks/products/tabs_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18519053675d49804dad3497-31598612%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f35866fd50d8950bcd0797b196c7ebee4c6fc95f' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/ebay/hooks/products/tabs_content.post.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '18519053675d49804dad3497-31598612',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ebay_templates' => 0,
    'template' => 0,
    'product_data' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d49804db27072_82515475',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d49804db27072_82515475')) {function content_5d49804db27072_82515475($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ebay_template','override_price','ebay_price','ebay_package_type','package_type_tooltip','override','override_tooltip','ebay_title','ebay_title_tooltip','ebay_description','ebay_description_tooltip','ebay_templates_not_found'));
?>
<div id="content_ebay" class="cm-hide-save-button hidden">
    <?php if ($_smarty_tpl->tpl_vars['ebay_templates']->value) {?>
        <div id="acc_ebay" class="collapse in">
            <div class="control-group">
                <label class="control-label" for="elm_ebay_template_id"><?php echo $_smarty_tpl->__("ebay_template");?>
:</label>
                <div class="controls">
                    <select class="span3" name="product_data[ebay_template_id]" id="elm_ebay_template_id">
                        <option value="0"><?php echo $_smarty_tpl->__('select');?>
</option>
                        <?php  $_smarty_tpl->tpl_vars["template"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["template"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ebay_templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["template"]->key => $_smarty_tpl->tpl_vars["template"]->value) {
$_smarty_tpl->tpl_vars["template"]->_loop = true;
?>
                            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['template']->value['template_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['ebay_template_id']==$_smarty_tpl->tpl_vars['template']->value['template_id']||(empty($_smarty_tpl->tpl_vars['product_data']->value['ebay_template_id'])&&$_smarty_tpl->tpl_vars['template']->value['use_as_default']=='Y')) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['template']->value['name'], ENT_QUOTES, 'UTF-8');?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="control-group" id="override_price" >
                <label for="elm_override_price" class="control-label"><?php echo $_smarty_tpl->__("override_price");?>
:</label>
                <div class="controls">
                    <input type="hidden" value="N" name="product_data[ebay_override_price]"/>
                    <input type="checkbox" onclick="override_price();" id="elm_override_price" name="product_data[ebay_override_price]" class="cm-toggle-checkbox cm-no-hide-input" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['ebay_override_price']=='Y') {?> checked="checked"<?php }?> value="Y" />
                </div>
            </div>
            <div class="control-group">
                <label id="elm_ebay_price_title" class="control-label<?php if ($_smarty_tpl->tpl_vars['product_data']->value['ebay_override_price']=='Y') {?> cm-required<?php }?>" for="elm_ebay_price"><?php echo $_smarty_tpl->__("ebay_price");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
) :</label>
                <div class="controls">
                    <input type="text" name="product_data[ebay_price]" id="elm_ebay_price" size="10" <?php if (!empty($_smarty_tpl->tpl_vars['product_data']->value['ebay_price'])) {?> value="<?php echo htmlspecialchars(fn_format_price($_smarty_tpl->tpl_vars['product_data']->value['ebay_price'],$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?> value="<?php echo htmlspecialchars(fn_format_price($_smarty_tpl->tpl_vars['product_data']->value['price'],$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
"  <?php }?> class="input-long" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['ebay_override_price']!='Y') {?> disabled="disabled"<?php }?> />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_package_type"><?php echo $_smarty_tpl->__("ebay_package_type");
ob_start();?><?php echo $_smarty_tpl->__("package_type_tooltip");?>
<?php $_tmp5=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_tmp5), 0);?>
:</label>
                <div class="controls">
                    <select class="span3" name="product_data[package_type]" id="elm_package_type">
                        <option <?php if ($_smarty_tpl->tpl_vars['product_data']->value['package_type']=='Letter') {?>selected="selected"<?php }?> value="Letter"><?php echo $_smarty_tpl->__('Letter');?>
</option>
                        <option <?php if ($_smarty_tpl->tpl_vars['product_data']->value['package_type']=='LargeEnvelope') {?>selected="selected"<?php }?> value="LargeEnvelope"><?php echo $_smarty_tpl->__('large_envelope');?>
</option>
                        <option <?php if ($_smarty_tpl->tpl_vars['product_data']->value['package_type']=='PackageThickEnvelope') {?>selected="selected"<?php }?> value="PackageThickEnvelope"><?php echo $_smarty_tpl->__('ebay_package');?>
</option>
                        <option <?php if ($_smarty_tpl->tpl_vars['product_data']->value['package_type']=='ExtraLargePack') {?>selected="selected"<?php }?> value="ExtraLargePack"><?php echo $_smarty_tpl->__('large_package');?>
</option>
                    </select>
                </div>
            </div>
            <div class="control-group" id="override" >
                <label for="elm_override" class="control-label"><?php echo $_smarty_tpl->__("override");
ob_start();?><?php echo $_smarty_tpl->__("override_tooltip");?>
<?php $_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_tmp6), 0);?>
:</label>
                <div class="controls">
                    <input type="hidden" value="N" name="product_data[override]"/>
                    <input type="checkbox" onclick="override();" id="elm_override" name="product_data[override]" class="cm-toggle-checkbox cm-no-hide-input" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']=='Y') {?> checked="checked"<?php }?> value="Y" />
                </div>
            </div>
            <div class="control-group">
                <label for="elm_ebay_title" class="control-label <?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']=='Y') {?>cm-required<?php }?>" id="ebay_title_req"><?php echo $_smarty_tpl->__("ebay_title");
ob_start();?><?php echo $_smarty_tpl->__("ebay_title_tooltip");?>
<?php $_tmp7=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_tmp7), 0);?>
:</label>
                <div class="controls">
                    <input type="text" name="product_data[ebay_title]" id="elm_ebay_title" size="55" <?php if (!empty($_smarty_tpl->tpl_vars['product_data']->value['ebay_title'])) {?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['ebay_title'], ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['product'], ENT_QUOTES, 'UTF-8');?>
" <?php }?> class="input-large cm-no-hide-input" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']=='N'||empty($_smarty_tpl->tpl_vars['product_data']->value['override'])) {?> disabled="disabled" <?php }?>/>
                </div>
            </div>

            <div class="control-group cm-no-hide-input">
                <label class="control-label" for="elm_ebay_full_descr"><?php echo $_smarty_tpl->__("ebay_description");
ob_start();?><?php echo $_smarty_tpl->__("ebay_description_tooltip");?>
<?php $_tmp8=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_tmp8), 0);?>
:</label>
                <div class="controls">
                    <div id="ebay_description_wrapper" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']!="Y") {?>class="disable-overlay-wrap wysiwyg-overlay"<?php }?>>
                        <textarea id="elm_ebay_full_descr" data-name="product_data[ebay_description]"<?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']=="Y") {?> name="product_data[ebay_description]"<?php }?> cols="55" rows="8" class="cm-wysiwyg input-large cm-no-hide-input">
                            <?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']=="Y"||!empty($_smarty_tpl->tpl_vars['product_data']->value['ebay_description'])) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['ebay_description'], ENT_QUOTES, 'UTF-8');?>

                            <?php } else { ?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['full_description'], ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                        </textarea>

                        <div id="elm_ebay_full_descr_overlay" class="disable-overlay<?php if ($_smarty_tpl->tpl_vars['product_data']->value['override']=="Y") {?> hidden<?php }?>"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <?php echo $_smarty_tpl->__("ebay_templates_not_found");?>

    <?php }?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
        function override() {
            var $ = Tygh.$,
                $elm_ebay_full_descr = $("#elm_ebay_full_descr"),
                $ebay_description_wrapper = $("#ebay_description_wrapper"),
                $elm_ebay_full_descr_overlay = $("#elm_ebay_full_descr_overlay");

            if ($("#elm_override").is(":checked")) {
                $("#elm_ebay_title").removeAttr("disabled");
                $("#ebay_title_req").addClass("cm-required");

                $elm_ebay_full_descr.attr("name", $elm_ebay_full_descr.data("name"));
                $ebay_description_wrapper.removeClass("disable-overlay-wrap wysiwyg-overlay");
                $elm_ebay_full_descr_overlay.addClass("hidden");
            } else {
                $ebay_description_wrapper.addClass("disable-overlay-wrap wysiwyg-overlay");
                $elm_ebay_full_descr_overlay.removeClass("hidden");
                $elm_ebay_full_descr.removeAttr("name");

                $("#ebay_title_req").removeClass("cm-required");
                $("#elm_ebay_title").attr("disabled","disabled");
            }
        }

        function override_price() {
            var $ = Tygh.$;

            if ($('#elm_override_price').prop('checked')) {
                $('#elm_ebay_price').prop('disabled', false);
                $('#elm_ebay_price_title').addClass("cm-required");
            } else {
                $('#elm_ebay_price').prop('disabled', true);
                $('#elm_ebay_price_title').removeClass("cm-required");
            }
        }
    <?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div><?php }} ?>
