<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:59:01
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/ebay/hooks/products/advanced_search.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19918477355d484435962764-94152861%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a0d0c9f73b1c7bca9f5f513fc0a78d693598b830' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/ebay/hooks/products/advanced_search.post.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19918477355d484435962764-94152861',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search' => 0,
    'ebay_templates' => 0,
    'key' => 0,
    'template' => 0,
    'ebay_product_statuses' => 0,
    'value' => 0,
    'status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d484435982dc3_04865436',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d484435982dc3_04865436')) {function content_5d484435982dc3_04865436($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ebay_template','any','ebay_product_status','exported_to_ebay','all','revised_after_the_latest_export'));
?>
<div class="row-fluid">
    <div class="group span6 form-horizontal">
        <div class="control-group">
            <label class="control-label" for="elm_ebay_template_id"><?php echo $_smarty_tpl->__("ebay_template");?>
</label>
            <div class="controls">
                <select  name="ebay_template_id" id="elm_ebay_template_id">
                    <option value="0">--</option>
                        <option value="any" <?php if ($_smarty_tpl->tpl_vars['search']->value['ebay_template_id']=='any') {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("any");?>
</option>
                    <?php  $_smarty_tpl->tpl_vars["template"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["template"]->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ebay_templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["template"]->key => $_smarty_tpl->tpl_vars["template"]->value) {
$_smarty_tpl->tpl_vars["template"]->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars["template"]->key;
?>
                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['search']->value['ebay_template_id']==$_smarty_tpl->tpl_vars['key']->value) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['template']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="elm_ebay_status"><?php echo $_smarty_tpl->__("ebay_product_status");?>
</label>
            <div class="controls">
                <select  name="ebay_status" id="elm_ebay_status">
                    <option value="">--</option>
                    <?php  $_smarty_tpl->tpl_vars['status'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['status']->_loop = false;
 $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ebay_product_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['status']->key => $_smarty_tpl->tpl_vars['status']->value) {
$_smarty_tpl->tpl_vars['status']->_loop = true;
 $_smarty_tpl->tpl_vars['value']->value = $_smarty_tpl->tpl_vars['status']->key;
?>
                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['search']->value['ebay_status']==$_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['search']->value['ebay_status']!='') {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['status']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="group span6 form-horizontal">
        <div class="control-group">
            <label class="control-label" for="ebay_change_products"><?php echo $_smarty_tpl->__("exported_to_ebay");?>
</label>
            <div class="controls">
                <select name="ebay_update" id="ebay_change_products">
                    <option value="">--</option>
                    <option value="P" <?php if ($_smarty_tpl->tpl_vars['search']->value['ebay_update']=="P") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("all");?>
</option>
                    <option value="W" <?php if ($_smarty_tpl->tpl_vars['search']->value['ebay_update']=="W") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("revised_after_the_latest_export");?>
</option>
                </select>
            </div>
        </div>
    </div>
</div><?php }} ?>
