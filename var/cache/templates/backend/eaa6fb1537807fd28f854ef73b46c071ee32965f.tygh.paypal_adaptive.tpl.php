<?php /* Smarty version Smarty-3.1.21, created on 2019-08-08 09:10:47
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/paypal_adaptive/views/payments/components/cc_processors/paypal_adaptive.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11892341815d4b84a7299321-16155405%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eaa6fb1537807fd28f854ef73b46c071ee32965f' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/paypal_adaptive/views/payments/components/cc_processors/paypal_adaptive.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '11892341815d4b84a7299321-16155405',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'paypal_currencies' => 0,
    'currency' => 0,
    'processor_params' => 0,
    'addons' => 0,
    'statuses' => 0,
    'k' => 0,
    's' => 0,
    'are_credentials_filled' => 0,
    'payment_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4b84a72f2685_47573826',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4b84a72f2685_47573826')) {function content_5d4b84a72f2685_47573826($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('addons.paypal_adaptive.paypal_workflow','currency','paypal_adaptive_type_payments','addons.paypal_adaptive.payment_type_notice','paypal_adaptive_papallel_payments','paypal_adaptive_chained_payments','paypal_adaptive_payer_fees','paypal_adaptive_fees_eachreceiver','paypal_adaptive_fees_primaryreceiver','primary_email','addons.paypal_adaptive.store_settings','paypal_adaptive_override_with_secondary_currency','paypal_adaptive_pending_payment','addons.paypal_adaptive.paypal_credentials','paypal_api_username','paypal_api_password','paypal_authentication_method','certificate','signature','certificate_filename','signature','test_live_mode','test','live','application_id'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("addons.paypal_adaptive.paypal_workflow"),'target'=>"#ppa_workflow"), 0);?>

<div id="ppa_workflow" class="collapse in">
    <div class="control-group">
        <label class="control-label" for="currency"><?php echo $_smarty_tpl->__("currency");?>
:</label>
        <div class="controls">
            <select name="payment_data[processor_params][currency]" id="currency">
                <?php  $_smarty_tpl->tpl_vars["currency"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["currency"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['paypal_currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["currency"]->key => $_smarty_tpl->tpl_vars["currency"]->value) {
$_smarty_tpl->tpl_vars["currency"]->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['code'], ENT_QUOTES, 'UTF-8');?>
"<?php if (!$_smarty_tpl->tpl_vars['currency']->value['active']) {?> disabled="disabled"<?php }
if ($_smarty_tpl->tpl_vars['processor_params']->value['currency']==$_smarty_tpl->tpl_vars['currency']->value['code']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['name'], ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo $_smarty_tpl->__("paypal_adaptive_type_payments");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("addons.paypal_adaptive.payment_type_notice",array("[url]"=>fn_url("addons.update&addon=paypal_adaptive")))), 0);?>
:</label>
        <div class="controls">
            <label class="radio inline">
                <input class="cm-switch-availability cm-switch-inverse cm-switch-visibility" id="sw_block_chained_settings" type="radio" value="parallel" name="payment_data[processor_params][payment_type]" <?php if ($_smarty_tpl->tpl_vars['addons']->value['paypal_adaptive']['collect_payouts']=="Y"||$_smarty_tpl->tpl_vars['processor_params']->value['payment_type']=="parallel"||!$_smarty_tpl->tpl_vars['processor_params']->value['payment_type']) {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("paypal_adaptive_papallel_payments");?>

            </label>
            <label class="radio inline">
                <input class="cm-switch-availability cm-switch-visibility" id="sw_block_chained_settings" type="radio" value="chained" name="payment_data[processor_params][payment_type]" <?php if ($_smarty_tpl->tpl_vars['addons']->value['paypal_adaptive']['collect_payouts']=="Y") {?> disabled="disabled"<?php } elseif ($_smarty_tpl->tpl_vars['processor_params']->value['payment_type']=="chained") {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("paypal_adaptive_chained_payments");?>

            </label>
        </div>
    </div>

    <div id="block_chained_settings"<?php if ($_smarty_tpl->tpl_vars['addons']->value['paypal_adaptive']['collect_payouts']=="Y"||$_smarty_tpl->tpl_vars['processor_params']->value['payment_type']=="parallel"||!$_smarty_tpl->tpl_vars['processor_params']->value['payment_type']) {?> style="display: none;"<?php }?>>
        <div class="control-group">
            <label class="control-label" for="elm_paypal_fees_payer"><?php echo $_smarty_tpl->__("paypal_adaptive_payer_fees");?>
:</label>
            <div class="controls">
                <select name="payment_data[processor_params][fees_payer]" id="elm_paypal_fees_payer">
                    <option value="EACHRECEIVER" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['fees_payer']=="EACHRECEIVER") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("paypal_adaptive_fees_eachreceiver");?>
</option>
                    <option value="PRIMARYRECEIVER" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['fees_payer']=="PRIMARYRECEIVER") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("paypal_adaptive_fees_primaryreceiver");?>
</option>
                </select>
            </div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="primary_email"><?php echo $_smarty_tpl->__("primary_email");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][primary_email]" id="primary_email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['primary_email'], ENT_QUOTES, 'UTF-8');?>
" class="input-text" />
        </div>
    </div>

    <input type="hidden" name="payment_data[processor_params][in_context]" value="N" />
</div>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("addons.paypal_adaptive.store_settings"),'target'=>"#ppa_store_settings"), 0);?>

<div id="ppa_store_settings" class="collapse in">
    <div class="control-group">
        <label class="control-label" for="user_currency"><?php echo $_smarty_tpl->__("paypal_adaptive_override_with_secondary_currency");?>
:</label>
        <div class="controls">
            <input type="hidden" name="payment_data[processor_params][user_currency]" value="N" />
            <input type="checkbox" id="user_currency" name="payment_data[processor_params][user_currency]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['user_currency']=="Y") {?>checked="checked"<?php }?> value="Y">
        </div>
    </div>

    <div class="control-group">
        <?php $_smarty_tpl->tpl_vars["statuses"] = new Smarty_variable(fn_get_simple_statuses(@constant('STATUSES_ORDER')), null, 0);?>

        <label class="control-label" for="elm_paypal_pending"><?php echo $_smarty_tpl->__("paypal_adaptive_pending_payment");?>
:</label>
        <div class="controls">
            <select name="payment_data[processor_params][statuses][pending_payment]" id="elm_paypal_pending">
                <?php  $_smarty_tpl->tpl_vars["s"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["s"]->_loop = false;
 $_smarty_tpl->tpl_vars["k"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["s"]->key => $_smarty_tpl->tpl_vars["s"]->value) {
$_smarty_tpl->tpl_vars["s"]->_loop = true;
 $_smarty_tpl->tpl_vars["k"]->value = $_smarty_tpl->tpl_vars["s"]->key;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ((isset($_smarty_tpl->tpl_vars['processor_params']->value['statuses']['pending_payment'])&&$_smarty_tpl->tpl_vars['processor_params']->value['statuses']['pending_payment']==$_smarty_tpl->tpl_vars['k']->value)||(!isset($_smarty_tpl->tpl_vars['processor_params']->value['statuses']['pending_payment'])&&$_smarty_tpl->tpl_vars['k']->value=='I')) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['s']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>

<?php $_smarty_tpl->tpl_vars['are_credentials_filled'] = new Smarty_variable($_smarty_tpl->tpl_vars['processor_params']->value['username']&&$_smarty_tpl->tpl_vars['processor_params']->value['password'], null, 0);?>
<?php ob_start();
if ($_smarty_tpl->tpl_vars['are_credentials_filled']->value) {?><?php echo "collapsed";?><?php }
$_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("addons.paypal_adaptive.paypal_credentials"),'meta'=>$_tmp1,'target'=>"#ppa_credentials"), 0);?>

<div id="ppa_credentials" class="collapse <?php if ($_smarty_tpl->tpl_vars['are_credentials_filled']->value) {?>out<?php } else { ?>in<?php }?>">
    <div class="control-group">
        <label class="control-label" for="username"><?php echo $_smarty_tpl->__("paypal_api_username");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][username]" id="username" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['username'], ENT_QUOTES, 'UTF-8');?>
" class="input-text" size="60"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="password"><?php echo $_smarty_tpl->__("paypal_api_password");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][password]" id="password" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['password'], ENT_QUOTES, 'UTF-8');?>
" class="input-text" size="60"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo $_smarty_tpl->__("paypal_authentication_method");?>
:</label>
        <div class="controls">
            <label class="radio inline" for="elm_payment_auth_method_cert">
                <input id="elm_payment_auth_method_cert" type="radio" value="cert" name="payment_data[processor_params][authentication_method]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['authentication_method']=="cert"||!$_smarty_tpl->tpl_vars['processor_params']->value['authentication_method']) {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("certificate");?>

            </label>
            <label class="radio inline" for="elm_payment_auth_method_signature">
                <input id="elm_payment_auth_method_signature" type="radio" value="signature" name="payment_data[processor_params][authentication_method]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['authentication_method']=="signature") {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("signature");?>

            </label>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="certificate_filename"><?php echo $_smarty_tpl->__("certificate_filename");?>
:</label>
        <div class="controls" id="certificate_file">
            <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['certificate_filename']) {?>
                <div class="text-type-value pull-left">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['certificate_filename'], ENT_QUOTES, 'UTF-8');?>

                    <a href="<?php echo htmlspecialchars(fn_url(('payments.delete_certificate?payment_id=').($_smarty_tpl->tpl_vars['payment_id']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax" data-ca-target-id="certificate_file">
                        <i class="icon-remove-sign cm-tooltip hand" title="<?php echo $_smarty_tpl->__('remove');?>
"></i>
                    </a>
                </div>
            <?php }?>

            <div <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['certificate_filename']) {?>class="clear"<?php }?>><?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"payment_certificate[]"), 0);?>
</div>
            <!--certificate_file--></div>
    </div>

    <div class="control-group">
        <label class="control-label" for="api_signature"><?php echo $_smarty_tpl->__("signature");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][signature]" id="api_signature" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['signature'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo $_smarty_tpl->__("test_live_mode");?>
:</label>
        <div class="controls">
            <label class="radio inline">
                <input class="cm-switch-availability cm-switch-inverse cm-switch-visibility" id="sw_block_app_id" type="radio" value="test" name="payment_data[processor_params][mode]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="test"||!$_smarty_tpl->tpl_vars['processor_params']->value['mode']) {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("test");?>

            </label>
            <label class="radio inline">
                <input class="cm-switch-availability cm-switch-visibility" id="sw_block_app_id" type="radio" value="live" name="payment_data[processor_params][mode]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="live") {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("live");?>

            </label>
        </div>
    </div>

    <div id="block_app_id"<?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="test"||!$_smarty_tpl->tpl_vars['processor_params']->value['mode']) {?> style="display: none;"<?php }?>>
        <div class="control-group">
            <label class="control-label cm-required" for="app_id"><?php echo $_smarty_tpl->__("application_id");?>
:</label>
            <div class="controls">
                <input type="text" name="payment_data[processor_params][app_id]" id="app_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['app_id'], ENT_QUOTES, 'UTF-8');?>
" class="input-text" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="test"||!$_smarty_tpl->tpl_vars['processor_params']->value['mode']) {?> disabled="" <?php }?> placeholder="APP-80W284485P519543T"/>
            </div>
        </div>
    </div>
</div>
<?php }} ?>
