<?php /* Smarty version Smarty-3.1.21, created on 2019-08-08 09:11:05
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/paypal/views/payments/components/cc_processors/paypal_pro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3562729445d4b84b93ccd92-07798092%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c9c227f79fee9587ad617347842d98a9ecf4c7a' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/paypal/views/payments/components/cc_processors/paypal_pro.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3562729445d4b84b93ccd92-07798092',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'addons' => 0,
    'paypal_currencies' => 0,
    'currency' => 0,
    'processor_params' => 0,
    'payment_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4b84b9444ab6_01129814',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4b84b9444ab6_01129814')) {function content_5d4b84b9444ab6_01129814($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('paypal.addon_is_disabled_notice','currency','order_prefix','addons.paypal.technical_details','username','password','paypal_authentication_method','certificate','signature','certificate_filename','signature','test_live_mode','test','live','3d_secure','merchant_id','processor_id','transaction_password','transaction_url','read_more_3d_secure'));
?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['paypal']['status']=="D") {?>
    <div class="alert alert-block">
	<p><?php echo $_smarty_tpl->__("paypal.addon_is_disabled_notice");?>
</p>
    </div>
<?php } else { ?>

<div class="control-group">
    <label class="control-label" for="currency"><?php echo $_smarty_tpl->__("currency");?>
:</label>
    <div class="controls">
        <select name="payment_data[processor_params][currency]" id="currency">
            <?php  $_smarty_tpl->tpl_vars["currency"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["currency"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['paypal_currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["currency"]->key => $_smarty_tpl->tpl_vars["currency"]->value) {
$_smarty_tpl->tpl_vars["currency"]->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['code'], ENT_QUOTES, 'UTF-8');?>
"<?php if (!$_smarty_tpl->tpl_vars['currency']->value['active']) {?> disabled="disabled"<?php }
if ($_smarty_tpl->tpl_vars['processor_params']->value['currency']==$_smarty_tpl->tpl_vars['currency']->value['code']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['name'], ENT_QUOTES, 'UTF-8');?>
</option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="order_prefix"><?php echo $_smarty_tpl->__("order_prefix");?>
:</label>
    <div class="controls">
        <input type="text" name="payment_data[processor_params][order_prefix]" id="order_prefix" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['order_prefix'], ENT_QUOTES, 'UTF-8');?>
" >
    </div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("addons.paypal.technical_details"),'target'=>"#section_technical_details"), 0);?>


<div id="section_technical_details">

    <div class="control-group">
        <label class="control-label cm-required" for="username"><?php echo $_smarty_tpl->__("username");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][username]" id="username" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['username'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="password"><?php echo $_smarty_tpl->__("password");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][password]" id="password" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['password'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo $_smarty_tpl->__("paypal_authentication_method");?>
:</label>
        <div class="controls">
            <label class="radio inline" for="elm_payment_auth_method_cert">
                <input id="elm_payment_auth_method_cert" type="radio" value="cert" name="payment_data[processor_params][authentication_method]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['authentication_method']=="cert"||!$_smarty_tpl->tpl_vars['processor_params']->value['authentication_method']) {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("certificate");?>

            </label>
            <label class="radio inline" for="elm_payment_auth_method_signature">
                <input id="elm_payment_auth_method_signature" type="radio" value="signature" name="payment_data[processor_params][authentication_method]" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['authentication_method']=="signature") {?> checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->__("signature");?>

            </label>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="certificate_filename"><?php echo $_smarty_tpl->__("certificate_filename");?>
:</label>
        <div class="controls" id="certificate_file">
            <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['certificate_filename']) {?>
                <div class="text-type-value pull-left">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['certificate_filename'], ENT_QUOTES, 'UTF-8');?>

                    <a href="<?php echo htmlspecialchars(fn_url(('payments.delete_certificate?payment_id=').($_smarty_tpl->tpl_vars['payment_id']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-post" data-ca-target-id="certificate_file">
                        <i class="icon-remove-sign cm-tooltip hand" title="<?php echo $_smarty_tpl->__('remove');?>
"></i>
                    </a>
                </div>
            <?php }?>

            <div <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['certificate_filename']) {?>class="clear"<?php }?>><?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"payment_certificate[]"), 0);?>
</div>
        <!--certificate_file--></div>
    </div>

    <div class="control-group">
        <label class="control-label" for="api_signature"><?php echo $_smarty_tpl->__("signature");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][signature]" id="api_signature" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['signature'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="mode"><?php echo $_smarty_tpl->__("test_live_mode");?>
:</label>
        <div class="controls">
            <select name="payment_data[processor_params][mode]" id="mode">
                <option value="test" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="test") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("test");?>
</option>
                <option value="live" <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="live") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("live");?>
</option>
            </select>
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("3d_secure")), 0);?>


    <div class="control-group">
        <label class="control-label cm-required" for="merchant_id"><?php echo $_smarty_tpl->__("merchant_id");?>
:</label>
            <div class="controls">
                <input type="text" name="payment_data[processor_params][merchant_id]" id="merchant_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['merchant_id'], ENT_QUOTES, 'UTF-8');?>
" >
            </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="processor_id"><?php echo $_smarty_tpl->__("processor_id");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][processor_id]" id="processor_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['processor_id'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="transaction_password"><?php echo $_smarty_tpl->__("transaction_password");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][transaction_password]" id="transaction_password" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['transaction_password'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="transaction_url"><?php echo $_smarty_tpl->__("transaction_url");?>
:</label>
        <div class="controls">
            <input type="text" name="payment_data[processor_params][transaction_url]" id="transaction_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['transaction_url'], ENT_QUOTES, 'UTF-8');?>
" >
        </div>
    </div>

    <p class="description"><a href="https://www.paypal-business.co.uk/3Dsecure.asp" target="_blank"><?php echo $_smarty_tpl->__("read_more_3d_secure");?>
</a></p>
</div>
<?php }?>
<?php }} ?>
