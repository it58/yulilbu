<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:44
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/wk_categories_extented_view/common/category_tree.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5585716755d498050095258-61254022%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47f68b2b9f5617a647cb78a129e8b1fd6e09b3cc' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/wk_categories_extented_view/common/category_tree.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5585716755d498050095258-61254022',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wk_categories_tree' => 0,
    'wk_parent_category_id' => 0,
    'item' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4980500d4a62_85123385',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4980500d4a62_85123385')) {function content_5d4980500d4a62_85123385($_smarty_tpl) {?> <?php if ($_smarty_tpl->tpl_vars['wk_categories_tree']->value) {?>
    <div class="input-group">
        <i class="fa fa-search search-icon"></i>
        <input type="text" name="search" class="category_search form-control" data-parent-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="wk_search_category_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="Search">
    </div>
    <ul id="container_category_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
" >
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key1'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['wk_categories_tree']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key1']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
            <?php if (!empty($_smarty_tpl->tpl_vars['category']->value['category_id'])&&$_smarty_tpl->tpl_vars['category']->value['parent_id']==$_smarty_tpl->tpl_vars['wk_parent_category_id']->value) {?>
                <li id="wk_category_id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category_id'], ENT_QUOTES, 'UTF-8');?>
" data-category-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category_id'], ENT_QUOTES, 'UTF-8');?>
" data-parent-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
"  class="<?php if ($_smarty_tpl->tpl_vars['category']->value['subcategories']||$_smarty_tpl->tpl_vars['category']->value['has_children']) {?>child<?php } else { ?>no_child<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['category']->value['subcategories']||$_smarty_tpl->tpl_vars['category']->value['has_children']) {?><span class="fa fa-caret-right chevron-icon"></span><?php }?></li>
            <?php }?>
            <?php } ?>
        <?php } ?>
    <!--container_category_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
--></ul>
<!--child_categories_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
--><?php }?><?php }} ?>
