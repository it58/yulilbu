<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 10:53:50
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/cp_ebay_import/hooks/companies/manage_tools_list.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10870944415d4a4b4e24da39-79082117%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '646ed77d485ef826a093737e85b64d127c4f7dac' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/cp_ebay_import/hooks/companies/manage_tools_list.pre.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '10870944415d4a4b4e24da39-79082117',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a4b4e278ff2_72271088',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a4b4e278ff2_72271088')) {function content_5d4a4b4e278ff2_72271088($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('allow_to_use_ebay','forbid_to_use_ebay'));
?>
<?php if (!fn_allowed_for("ULTIMATE")&&!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
	<li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("allow_to_use_ebay"),'dispatch'=>"dispatch[companies.allow_accsess_ebay]",'form'=>"companies_form"));?>
</li>
	<li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("forbid_to_use_ebay"),'dispatch'=>"dispatch[companies.forbid_accsess_ebay]",'form'=>"companies_form"));?>
</li>
<?php }?>
<?php }} ?>
