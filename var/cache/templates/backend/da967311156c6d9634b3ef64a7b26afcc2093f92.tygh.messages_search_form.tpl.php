<?php /* Smarty version Smarty-3.1.21, created on 2019-08-09 13:44:28
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/views/messenger/components/messages_search_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16495898985d4d164c24cea7-03850746%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'da967311156c6d9634b3ef64a7b26afcc2093f92' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/views/messenger/components/messages_search_form.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '16495898985d4d164c24cea7-03850746',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_meta' => 0,
    'ticket' => 0,
    'selected_section' => 0,
    'put_request_vars' => 0,
    'extra' => 0,
    'search' => 0,
    'dispatch' => 0,
    'in_popup' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4d164c2631f3_30101198',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4d164c2631f3_30101198')) {function content_5d4d164c2631f3_30101198($_smarty_tpl) {?><?php if (!is_callable('smarty_function_array_to_fields')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.array_to_fields.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('message','period'));
?>
<div class="sidebar-row">
    <form name="messages_search_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="get" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_meta']->value, ENT_QUOTES, 'UTF-8');?>
">
        <?php $_smarty_tpl->_capture_stack[0][] = array("simple_search", null, null); ob_start(); ?>
        <input type="hidden" name="ticket_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" />

        <?php if ($_REQUEST['redirect_url']) {?>
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_REQUEST['redirect_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['selected_section']->value!='') {?>
        <input type="hidden" id="selected_section" name="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_section']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['put_request_vars']->value) {?>
            <?php echo smarty_function_array_to_fields(array('data'=>$_REQUEST,'skip'=>array("callback"),'escape'=>array("data_id")),$_smarty_tpl);?>

        <?php }?>

        <?php echo $_smarty_tpl->tpl_vars['extra']->value;?>

        <div class="sidebar-field">
            <label for="elm_message"><?php echo $_smarty_tpl->__("message");?>
</label>
            <div class="break">
                <input type="text" name="message" id="elm_message" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['message'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

        <?php $_smarty_tpl->_capture_stack[0][] = array("advanced_search", null, null); ob_start(); ?>
        <div class="group form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo $_smarty_tpl->__("period");?>
</label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/period_selector.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('period'=>$_smarty_tpl->tpl_vars['search']->value['period'],'form_name'=>"messages_search_form"), 0);?>

                </div>
            </div>
        </div>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/advanced_search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('simple_search'=>Smarty::$_smarty_vars['capture']['simple_search'],'advanced_search'=>Smarty::$_smarty_vars['capture']['advanced_search'],'dispatch'=>$_smarty_tpl->tpl_vars['dispatch']->value,'view_type'=>"ticket_messages",'in_popup'=>$_smarty_tpl->tpl_vars['in_popup']->value), 0);?>

    </form>

</div><hr><?php }} ?>
