<?php /* Smarty version Smarty-3.1.21, created on 2019-08-11 07:05:02
         compiled from "/home/yulibu/public_html/design/backend/templates/views/upgrade_center/conflicts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2586227675d4f5bae184451-45106700%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e54c80133e0311f76104c58532fb187fc1021bb' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/views/upgrade_center/conflicts.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2586227675d4f5bae184451-45106700',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'package_id' => 0,
    'config' => 0,
    'backups_dir' => 0,
    'package' => 0,
    'file_data' => 0,
    'result_ids' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4f5bae1c7033_83463974',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4f5bae1c7033_83463974')) {function content_5d4f5bae1c7033_83463974($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('local_modifications_message','files','status','files','status','not_checked','checked','checked','not_checked','close'));
?>
<div id="conflicts_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['package_id']->value, ENT_QUOTES, 'UTF-8');?>
">
    <?php $_smarty_tpl->tpl_vars["backups_dir"] = new Smarty_variable(fn_get_rel_dir($_smarty_tpl->tpl_vars['config']->value['dir']['backups']), null, 0);?>
    <p><?php echo $_smarty_tpl->__("local_modifications_message",array("[dir]"=>trim($_smarty_tpl->tpl_vars['backups_dir']->value,"/\\")));?>
</p>
    <?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable("conflicts_content_".((string)$_smarty_tpl->tpl_vars['package_id']->value), null, 0);?>
    <div class="table-responsive-wrapper">
        <table class="table table-condensed table-responsive">
            <thead>
                <tr>
                    <th><?php echo $_smarty_tpl->__("files");?>
</th>
                    <th class="left"><?php echo $_smarty_tpl->__("status");?>
</th>
                </tr>
            </thead>
            <tbody>
                <?php  $_smarty_tpl->tpl_vars['file_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['file_data']->_loop = false;
 $_smarty_tpl->tpl_vars['file_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['package']->value['conflicts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['file_data']->key => $_smarty_tpl->tpl_vars['file_data']->value) {
$_smarty_tpl->tpl_vars['file_data']->_loop = true;
 $_smarty_tpl->tpl_vars['file_id']->value = $_smarty_tpl->tpl_vars['file_data']->key;
?>
                    <tr>
                        <td data-th="<?php echo $_smarty_tpl->__("files");?>
">
                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file_data']->value['file_path'], ENT_QUOTES, 'UTF-8');?>

                        </td>
                        <td width="10%" class="left" data-th="<?php echo $_smarty_tpl->__("status");?>
">
                            <?php if ($_smarty_tpl->tpl_vars['file_data']->value['status']=="C") {?>
                                <div class="btn-group dropleft">
                                    <button class="btn btn-danger btn-small dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->__("not_checked");?>
 <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo htmlspecialchars(fn_url("upgrade_center.resolve_conflict?package_id=".((string)$_smarty_tpl->tpl_vars['package_id']->value)."&file_id=".((string)$_smarty_tpl->tpl_vars['file_id']->value)."&status=R"), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("checked");?>
</a></li>
                                    </ul>
                                </div>
                            <?php } elseif ($_smarty_tpl->tpl_vars['file_data']->value['status']=="R") {?>
                                <div class="btn-group dropleft">
                                    <button class="btn btn-success btn-small dropdown-toggle" data-toggle="dropdown"><?php echo $_smarty_tpl->__("checked");?>
 <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo htmlspecialchars(fn_url("upgrade_center.resolve_conflict?package_id=".((string)$_smarty_tpl->tpl_vars['package_id']->value)."&file_id=".((string)$_smarty_tpl->tpl_vars['file_id']->value)."&status=C"), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("not_checked");?>
</a></li>
                                    </ul>
                                </div>
                            <?php }?>
                            
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <div class="buttons-container">
        <a class="cm-dialog-closer cm-cancel tool-link btn"><?php echo $_smarty_tpl->__("close");?>
</a>
    </div>

<!--conflicts_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['package_id']->value, ENT_QUOTES, 'UTF-8');?>
--></div><?php }} ?>
