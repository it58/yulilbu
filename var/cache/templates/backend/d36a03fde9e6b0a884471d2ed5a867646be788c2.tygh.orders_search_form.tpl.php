<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:42:52
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/ecl_search_improvements/overrides/views/orders/components/orders_search_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11529667935d4983dc96a382-87312957%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd36a03fde9e6b0a884471d2ed5a867646be788c2' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/ecl_search_improvements/overrides/views/orders/components/orders_search_form.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '11529667935d4983dc96a382-87312957',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4983dc98c5c9_00268109',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4983dc98c5c9_00268109')) {function content_5d4983dc98c5c9_00268109($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('phone'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("phone_search", null, null); ob_start(); ?>
<div class="sidebar-field hidden">
    <label for="ecl_phone"><?php echo $_smarty_tpl->__("phone");?>
</label>
    <input type="text" name="phone" id="ecl_phone" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" size="30" />
</div>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
Tygh.$(document).ready(function() {
    if ($("input[name='phone']" ).length > 1) {
        $('#ecl_phone').parent().remove();
    } else {
        $('#ecl_phone').parent().show();
    }
});
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("views/orders/components/../components/orders_search_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dispatch'=>"orders.manage",'extra'=>Smarty::$_smarty_vars['capture']['phone_search']), 0);?>
<?php }} ?>
