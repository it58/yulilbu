<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 16:43:22
         compiled from "/home/yulibu/public_html/design/backend/mail/templates/companies/apply_for_vendor_notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20264669575d494bbaef0797-92319988%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fe3cedf6521022d057b4c0c5b0c4a770ce9f4909' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/mail/templates/companies/apply_for_vendor_notification.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '20264669575d494bbaef0797-92319988',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'company_update_url' => 0,
    'company' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d494bbaf3d091_28163276',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d494bbaf3d091_28163276')) {function content_5d494bbaf3d091_28163276($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('vendor_candidate_notification','company_name','description','account_name','first_name','last_name','email','phone','url','fax','address','city','country','state','zip_postal_code','vendor_candidate_notification','company_name','description','account_name','first_name','last_name','email','phone','url','fax','address','city','country','state','zip_postal_code'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>
<?php echo $_smarty_tpl->__("vendor_candidate_notification",array("[href]"=>$_smarty_tpl->tpl_vars['company_update_url']->value));?>


<br/><br/>

<table>
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:apply_for_vendor_company_name")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:apply_for_vendor_company_name"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("company_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['company'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:apply_for_vendor_company_name"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php if ($_smarty_tpl->tpl_vars['company']->value['company_description']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("description");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['company_description'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['request_account_name']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("account_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['request_account_name'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['admin_firstname']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("first_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['admin_firstname'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['admin_lastname']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("last_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['admin_lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("email");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("phone");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['url']) {?>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("url");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['url'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['fax']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("fax");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['fax'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("address");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['address'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("city");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['city'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("country");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['country'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("state");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['state'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("zip_postal_code");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
</table><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="companies/apply_for_vendor_notification.tpl" id="<?php echo smarty_function_set_id(array('name'=>"companies/apply_for_vendor_notification.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>
<?php echo $_smarty_tpl->__("vendor_candidate_notification",array("[href]"=>$_smarty_tpl->tpl_vars['company_update_url']->value));?>


<br/><br/>

<table>
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:apply_for_vendor_company_name")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:apply_for_vendor_company_name"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("company_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['company'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:apply_for_vendor_company_name"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php if ($_smarty_tpl->tpl_vars['company']->value['company_description']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("description");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['company_description'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['request_account_name']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("account_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['request_account_name'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['admin_firstname']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("first_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['admin_firstname'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['admin_lastname']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("last_name");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['admin_lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("email");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("phone");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['url']) {?>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("url");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['url'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['company']->value['fax']) {?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("fax");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['fax'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <?php }?>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("address");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['address'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("city");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['city'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("country");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['country'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("state");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['state'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
    <tr>
        <td class="form-field-caption" nowrap><?php echo $_smarty_tpl->__("zip_postal_code");?>
:&nbsp;</td>
        <td ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
</td>
    </tr>
</table><?php }?><?php }} ?>
