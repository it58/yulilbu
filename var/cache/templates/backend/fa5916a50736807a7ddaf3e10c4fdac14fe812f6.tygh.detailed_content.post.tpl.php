<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 10:54:38
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_google_analytics/hooks/companies/detailed_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12048053965d4a4b7eeffa26-58136215%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa5916a50736807a7ddaf3e10c4fdac14fe812f6' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_google_analytics/hooks/companies/detailed_content.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12048053965d4a4b7eeffa26-58136215',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'addons' => 0,
    'company_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a4b7ef0bd27_39292107',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a4b7ef0bd27_39292107')) {function content_5d4a4b7ef0bd27_39292107($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('sd_ga_google_analytics_section','sd_ga_tracking_code','sd_ga_tracking_code_tooltip'));
?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("sd_ga_google_analytics_section"),'target'=>"#acc_addon_sd_google_analytics"), 0);?>

    <div id="acc_addon_sd_google_analytics" class="collapse in">
        <div class="control-group" id="condition_ga">
            <label class="control-label" for="sd_google_analytics_vendor_tracking_code"><?php echo $_smarty_tpl->__("sd_ga_tracking_code");?>
 <i class="icon-question-sign cm-tooltip" title="<?php echo $_smarty_tpl->__("sd_ga_tracking_code_tooltip");?>
"></i>:</label> 
            <div class="controls">
                <input type="text" name="company_data[sd_ga_tracking_code]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['sd_ga_tracking_code'], ENT_QUOTES, 'UTF-8');?>
" class="input-text" size="60" />
            </div>
        </div>
    </div>
<?php }?><?php }} ?>
