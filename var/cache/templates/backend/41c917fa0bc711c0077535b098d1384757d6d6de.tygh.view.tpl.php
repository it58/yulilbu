<?php /* Smarty version Smarty-3.1.21, created on 2019-08-09 13:44:28
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/views/messenger/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11539571385d4d164c198655-14721471%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '41c917fa0bc711c0077535b098d1384757d6d6de' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/views/messenger/view.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '11539571385d4d164c198655-14721471',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ticket' => 0,
    'messages' => 0,
    'message' => 0,
    'last_message_id' => 0,
    'message_class' => 0,
    'no_messages_found' => 0,
    'self_images_dir' => 0,
    'attachment_message' => 0,
    'auth' => 0,
    'delete_all_url' => 0,
    'addons' => 0,
    'vendor_image_data' => 0,
    'customer_image_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4d164c22d672_46079531',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4d164c22d672_46079531')) {function content_5d4d164c22d672_46079531($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('no_messages_found_with_recipient','no_messages_found','delete_all_messages','messages'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<?php $_smarty_tpl->tpl_vars['last_message_id'] = new Smarty_variable(0, null, 0);?>

<div class="ticket-messages-block" id="ticket_messages_block">
    <div class="ticket-messages-list" data-ticket-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" id="ticket_messages_list">
        <?php if ($_smarty_tpl->tpl_vars['messages']->value) {?>
            <?php  $_smarty_tpl->tpl_vars['message'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['message']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['message']->key => $_smarty_tpl->tpl_vars['message']->value) {
$_smarty_tpl->tpl_vars['message']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['message']->value['message_id']>$_smarty_tpl->tpl_vars['last_message_id']->value) {?>
                    <?php $_smarty_tpl->tpl_vars['last_message_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['message']->value['message_id'], null, 0);?>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['message']->value['author_type']=="V") {?>
                    <?php $_smarty_tpl->tpl_vars['message_class'] = new Smarty_variable("author-message", null, 0);?>
                <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars['message_class'] = new Smarty_variable("recipient-message has-avatar", null, 0);?>
                <?php }?>

                <div class="ty-sd_messaging_system-message__content <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message_class']->value, ENT_QUOTES, 'UTF-8');?>
 ty-mb-l">
                    <div class="ty-sd_messaging_system-message__author">
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_messaging_system/views/messenger/components/user_image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('message'=>$_smarty_tpl->tpl_vars['message']->value,'ticket'=>$_smarty_tpl->tpl_vars['ticket']->value), 0);?>

                    </div>

                    <div class="ty-sd_messaging_system-all">
                        <div class="ty-sd_messaging_system-name">
                            <?php if ($_smarty_tpl->tpl_vars['message']->value['author_type']=="V"&&$_smarty_tpl->tpl_vars['ticket']->value['vendor_name']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['vendor_name'], ENT_QUOTES, 'UTF-8');?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['message']->value['author_type']!="V"&&$_smarty_tpl->tpl_vars['ticket']->value['customer_name']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['customer_name'], ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <span class="ty-sd_messaging_system-message__date" data-message-timestamp="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['timestamp'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['date'], ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>

                        <div class="ty-sd_messaging_system-message" id="message_text">
                            <div class="ty-sd_messaging_system-message__message <?php if (!empty($_smarty_tpl->tpl_vars['message']->value['need_highlight'])&&$_smarty_tpl->tpl_vars['message']->value['need_highlight']=='Y'&&$_SESSION['auth']['user_type']=='A') {?>text-error<?php }?>">
                                <?php echo preg_replace('!\s+!u', ' ',nl2br($_smarty_tpl->tpl_vars['message']->value['message']));?>

                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>
        <?php } else { ?>
            <?php if ($_smarty_tpl->tpl_vars['ticket']->value['customer_name']) {?>
                <?php $_smarty_tpl->tpl_vars['no_messages_found'] = new Smarty_variable($_smarty_tpl->__("no_messages_found_with_recipient",array("[recipient_name]"=>$_smarty_tpl->tpl_vars['ticket']->value['customer_name'])), null, 0);?>
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars['no_messages_found'] = new Smarty_variable($_smarty_tpl->__("no_messages_found"), null, 0);?>
            <?php }?>

            <p class="ty-no-messages"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_messages_found']->value, ENT_QUOTES, 'UTF-8');?>
</p>
        <?php }?>
    </div>
</div>

<div class="center hidden" id="message_spinner"><?php echo $_smarty_tpl->__('addons.sd_messaging_system.waiting_for_connect');?>
 <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['self_images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/images/spinner.gif" width="20" height="20"/></div>
<div class="ty-discussion-post__buttons buttons-container">
    <form id="new_message_form" class="ty-discussion-post__form">
        <textarea id="new_message_field" name="message_body" rows="4" cols="50" placeholder="<?php echo $_smarty_tpl->__('write_your_message_here');?>
" class="ty-sd_messaging_system-textarea" disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['attachment_message']->value;?>
</textarea>
        <a href="#" onclick="history.back();" class="btn btn-secondary"><?php echo $_smarty_tpl->__('go_back');?>
</a>
        <input type="hidden" name="ticket_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" name="author_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['auth']->value['company_id'], ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" name="last_message_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['last_message_id']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <button id="send_message_but" class="btn btn-primary btn-right ty-btn" type="submit" name="dispatch[messenger.send_message]" onclick="return false;" disabled="disabled"><?php echo $_smarty_tpl->__('send');?>
</button>
    </form>
</div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php if ($_smarty_tpl->tpl_vars['messages']->value) {?>
        <?php $_smarty_tpl->tpl_vars["delete_all_url"] = new Smarty_variable(fn_url("messenger.delete_all_messages&ticket_id=".((string)$_REQUEST['ticket_id'])), null, 0);?>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_gtoup'=>"ture",'but_text'=>$_smarty_tpl->__("delete_all_messages"),'but_role'=>"action",'but_href'=>$_smarty_tpl->tpl_vars['delete_all_url']->value,'but_meta'=>"btn btn-primary cm-confirm"), 0);?>

    <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/saved_search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dispatch'=>"messenger.view",'view_type'=>"ticket_messages",'view_suffix'=>"ticket_id=".((string)$_smarty_tpl->tpl_vars['ticket']->value['ticket_id'])), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_messaging_system/views/messenger/components/messages_search_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dispatch'=>"messenger.view",'form_meta'=>''), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("messages"),'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'tools'=>Smarty::$_smarty_vars['capture']['tools'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons']), 0);?>


<?php $_smarty_tpl->tpl_vars['vendor_image_data'] = new Smarty_variable(fn_image_to_display($_smarty_tpl->tpl_vars['ticket']->value['vendor_image'],@constant('USER_IMAGE_WIDTH'),@constant('USER_IMAGE_HEIGHT')), null, 0);?>
<?php $_smarty_tpl->tpl_vars['customer_image_data'] = new Smarty_variable(fn_image_to_display($_smarty_tpl->tpl_vars['ticket']->value['customer_image'],@constant('USER_IMAGE_WIDTH'),@constant('USER_IMAGE_HEIGHT')), null, 0);?>

<input type="hidden" name="websocket_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['sd_messaging_system']['websocket_url'], ENT_QUOTES, 'UTF-8');?>
">

<div id="sd-messenger-sender-data" class="hidden"
    data-name="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['ticket']->value['vendor_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['company_id'], ENT_QUOTES, 'UTF-8');?>

    data-type="V"
    data-has-image="<?php if ($_smarty_tpl->tpl_vars['vendor_image_data']->value) {?>Y<?php } else { ?>N<?php }?>"
    data-image-path="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['vendor_image_data']->value['image_path'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-image-alt="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['vendor_image_data']->value['alt'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
">
</div>

<div id="sd-messenger-recipient-data" class="hidden"
    data-name="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['ticket']->value['customer_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['customer_id'], ENT_QUOTES, 'UTF-8');?>

    data-type="C"
    data-has-image="<?php if ($_smarty_tpl->tpl_vars['customer_image_data']->value) {?>Y<?php } else { ?>N<?php }?>"
    data-image-path="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['customer_image_data']->value['image_path'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-image-alt="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['customer_image_data']->value['alt'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
">
</div>

<?php echo smarty_function_script(array('src'=>"js/addons/sd_messaging_system/web_socket.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/addons/sd_messaging_system/messenger.js"),$_smarty_tpl);?>

<?php }} ?>
