<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:39
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/wk_categories_extented_view/common/picker.tpl" */ ?>
<?php /*%%SmartyHeaderCode:324992205d49804b9028d4-00284709%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbef8679e0cb5d9b84d59e7d731878aa490afd72' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/wk_categories_extented_view/common/picker.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '324992205d49804b9028d4-00284709',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wk_category_path' => 0,
    'wk_parent_category_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d49804b924870_97031411',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d49804b924870_97031411')) {function content_5d49804b924870_97031411($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('select_a_category','please_select_a_category','please_select_a_leaf_category','category_selected','success'));
?>
<div class="category_container_box <?php if ($_smarty_tpl->tpl_vars['wk_category_path']->value) {?>hidden<?php }?>" id="category_container_box">
    <div class="categories_container" id="child_categories_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
"></div> 
<!--category_container_box--></div>

<?php echo smarty_function_script(array('src'=>"js/addons/wk_category_extended_view/wk_cat.js"),$_smarty_tpl);?>
 
<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
    Tygh.tr('select_a_category', '<?php echo strtr($_smarty_tpl->__("select_a_category"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
    Tygh.tr('please_select_a_category', '<?php echo strtr($_smarty_tpl->__("please_select_a_category"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
    Tygh.tr('please_select_a_leaf_category', '<?php echo strtr($_smarty_tpl->__("please_select_a_leaf_category"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
    Tygh.tr('category_selected', '<?php echo strtr($_smarty_tpl->__("category_selected"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
    Tygh.tr('success', '<?php echo strtr($_smarty_tpl->__("success"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
    <?php if ($_smarty_tpl->tpl_vars['wk_category_path']->value) {?>
        $(document).ready(function(){
            var category_path1 = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_category_path']->value, ENT_QUOTES, 'UTF-8');?>
';
            var categories1 =  category_path1.split('/');
            if(categories1 && categories1.length>0){
                fn_add_subcategory_tree(0,categories1);
            }
        });
    <?php } else { ?>
        fn_add_subcategory_tree(0,{});
    <?php }?>
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php }} ?>
