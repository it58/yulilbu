<?php /* Smarty version Smarty-3.1.21, created on 2019-08-09 11:16:14
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/ecl_search_improvements/hooks/profiles/search_form.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4813449815d4cf38eb908d2-78484203%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cd581d48ee1958a0b1dbb506f49f74342609212e' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/ecl_search_improvements/hooks/profiles/search_form.post.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4813449815d4cf38eb908d2-78484203',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4cf38eb9d073_56949612',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4cf38eb9d073_56949612')) {function content_5d4cf38eb9d073_56949612($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('phone'));
?>
<div class="sidebar-field" id="elm_phone_field">
    <label for="elm_phone_sb"><?php echo $_smarty_tpl->__("phone");?>
</label>
    <div class="break">
        <input type="text" name="phone" id="elm_phone_sb" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" />
    </div>
</div>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
(function(_, $) {
    $(document).ready(function(){
		$('#simple_search').append($('#elm_phone_field'));
        if ($('#elm_phone').length) {
            $('#elm_phone').parent().parent().remove();
        }
    });
}(Tygh, Tygh.$));
//]]>
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php }} ?>
