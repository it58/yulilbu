<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:59:01
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/ecl_search_improvements/hooks/products/advanced_search.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2577102425d48443594d8f1-11252579%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f1826b00742509d06b630e5a320c60d7244e86d1' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/ecl_search_improvements/hooks/products/advanced_search.post.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2577102425d48443594d8f1-11252579',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search' => 0,
    'addons' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48443595f598_75773859',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48443595f598_75773859')) {function content_5d48443595f598_75773859($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('search_by_sku'));
?>
<div class="sidebar-field" id="elm_pcode_field">
    <label><?php echo $_smarty_tpl->__("search_by_sku");?>
</label>
	<input type="text" name="pcode" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['pcode'], ENT_QUOTES, 'UTF-8');?>
" onfocus="this.select();"/>
</div>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
(function(_, $) {
    $(document).ready(function(){
		$('#pcode').parent().parent().remove();
		$('#simple_search').append($('#elm_pcode_field'));
    });
}(Tygh, Tygh.$));
//]]>
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<input id="elm_match_field" type="hidden" name="match" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['ecl_search_improvements']['admin_search_type'], ENT_QUOTES, 'UTF-8');?>
" /><?php }} ?>
