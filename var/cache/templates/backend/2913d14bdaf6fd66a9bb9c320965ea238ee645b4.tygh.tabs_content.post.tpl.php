<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 10:54:39
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_vacation_mode_for_vendors/hooks/companies/tabs_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2053191065d4a4b7f0ed1b2-36507370%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2913d14bdaf6fd66a9bb9c320965ea238ee645b4' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_vacation_mode_for_vendors/hooks/companies/tabs_content.post.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2053191065d4a4b7f0ed1b2-36507370',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'navigation' => 0,
    'company_data' => 0,
    'sd_vmfv_all_timezone_vacation' => 0,
    'timezone_value' => 0,
    'timezone_vacation' => 0,
    'settings' => 0,
    'not_ban_change_history' => 0,
    'sd_vmfv_period_vacation' => 0,
    'month_day_dayweek' => 0,
    'year' => 0,
    'month' => 0,
    'i' => 0,
    'first_day' => 0,
    'day_of_week' => 0,
    'day_dayweek' => 0,
    'dayweek' => 0,
    'currend_date' => 0,
    'day' => 0,
    'sd_vmfv_vacation_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a4b7f12e8d1_90818017',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a4b7f12e8d1_90818017')) {function content_5d4a4b7f12e8d1_90818017($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('sd_vmfv_vendor_in_vacation','sd_vmfv_date_timezone','month_name_','weekday_abr_','the_time_format_must_be_hhmm','work_time_start','ttc_work_time_start','hh_mm','the_time_format_must_be_hhmm','work_time_end','ttc_work_time_end','hh_mm'));
?>
<?php if ($_smarty_tpl->tpl_vars['navigation']->value['tabs']['sd_vacation_mode_for_vendors']) {?>
    <div id="content_sd_vacation_mode_for_vendors" class="hidden">
        <div class="control-group">
            <label class="control-label" for="id_sd_vmfv_vendor_in_vacation"><?php echo $_smarty_tpl->__("sd_vmfv_vendor_in_vacation");?>
:</label>
            <div class="controls">
                <input type="checkbox" name="company_data[vendor_in_vacation]" id="id_sd_vmfv_vendor_in_vacation" <?php if ($_smarty_tpl->tpl_vars['company_data']->value['vendor_in_vacation']=='Y') {?>checked="checked"<?php }?> value="Y"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="id_sd_vmfv_timezone_vacation"><?php echo $_smarty_tpl->__("sd_vmfv_date_timezone");?>
:</label>
            <div class="controls">
            <select name="company_data[timezone_vacation]" id="id_sd_vmfv_timezone_vacation">
                <?php  $_smarty_tpl->tpl_vars["timezone_vacation"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["timezone_vacation"]->_loop = false;
 $_smarty_tpl->tpl_vars["timezone_value"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sd_vmfv_all_timezone_vacation']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["timezone_vacation"]->key => $_smarty_tpl->tpl_vars["timezone_vacation"]->value) {
$_smarty_tpl->tpl_vars["timezone_vacation"]->_loop = true;
 $_smarty_tpl->tpl_vars["timezone_value"]->value = $_smarty_tpl->tpl_vars["timezone_vacation"]->key;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['timezone_value']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['company_data']->value['timezone_vacation'])&&$_smarty_tpl->tpl_vars['timezone_value']->value==$_smarty_tpl->tpl_vars['company_data']->value['timezone_vacation']) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['timezone_vacation']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
            <?php if (!isset($_smarty_tpl->tpl_vars['company_data']->value['timezone_vacation'])) {?>
                <literal>
                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
>
                    var currenDate = new Date();
                    timezoneOffset = -currenDate.getTimezoneOffset() / 60;
                    $("#id_sd_vmfv_timezone_vacation option[value='"+timezoneOffset+"']").attr("selected","selected");
                    <?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                </literal>
                <span class="sd_vmfv_inform">
                    <?php echo $_smarty_tpl->__('sd_vmfv_aute_set_timezone_check');?>

                </span>
            <?php }?>
            </div>
        </div>


        <div id="sd_vmfv_calendars" class ="sd_vmfv_calendars">
        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__('sd_vmfv_weekend')), 0);?>

            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Appearance']['calendar_week_format']=="sunday_first") {?>
                <?php $_smarty_tpl->tpl_vars['first_day'] = new Smarty_variable(0, null, 0);?> 
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars['first_day'] = new Smarty_variable(1, null, 0);?> 
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['not_ban_change_history']->value) {?>
                <?php $_smarty_tpl->tpl_vars['currend_date'] = new Smarty_variable(1, null, 0);?>
                <input type='hidden' name="not_ban_change_history" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['not_ban_change_history']->value, ENT_QUOTES, 'UTF-8');?>
">
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars['currend_date'] = new Smarty_variable(0, null, 0);?>
            <?php }?>

            <?php  $_smarty_tpl->tpl_vars["month_day_dayweek"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["month_day_dayweek"]->_loop = false;
 $_smarty_tpl->tpl_vars["year"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sd_vmfv_period_vacation']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["month_day_dayweek"]->key => $_smarty_tpl->tpl_vars["month_day_dayweek"]->value) {
$_smarty_tpl->tpl_vars["month_day_dayweek"]->_loop = true;
 $_smarty_tpl->tpl_vars["year"]->value = $_smarty_tpl->tpl_vars["month_day_dayweek"]->key;
?>
                <?php  $_smarty_tpl->tpl_vars["day_dayweek"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["day_dayweek"]->_loop = false;
 $_smarty_tpl->tpl_vars["month"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['month_day_dayweek']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["day_dayweek"]->key => $_smarty_tpl->tpl_vars["day_dayweek"]->value) {
$_smarty_tpl->tpl_vars["day_dayweek"]->_loop = true;
 $_smarty_tpl->tpl_vars["month"]->value = $_smarty_tpl->tpl_vars["day_dayweek"]->key;
?>
                    <div id="sd_vmfv_calendar_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['month']->value, ENT_QUOTES, 'UTF-8');?>
" class="sd_vmfv_calendar">
                        <table class="table_calendar_table">
                            <thead class="thead_calendar_table">
                                <tr>
                                    <th colspan=7><?php echo $_smarty_tpl->__("month_name_".((string)$_smarty_tpl->tpl_vars['month']->value));?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');?>
<th>
                                </tr>
                                <tr>
                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 6+1 - (0) : 0-(6)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                    <?php $_smarty_tpl->tpl_vars['dow'] = new Smarty_variable(($_smarty_tpl->tpl_vars['i']->value+$_smarty_tpl->tpl_vars['first_day']->value)%7, null, 0);?> 
                                    <th><?php echo $_smarty_tpl->__("weekday_abr_".((string)$_smarty_tpl->tpl_vars['dow']->value));?>
</th>
                                <?php }} ?>
                                </tr>
                            </thead>
                            <tbody class="tbody_calendar_table">
                                <tr>
                                <?php $_smarty_tpl->tpl_vars['day_of_week'] = new Smarty_variable($_smarty_tpl->tpl_vars['first_day']->value, null, 0);?>
                                <?php while ($_smarty_tpl->tpl_vars['day_of_week']->value!=$_smarty_tpl->tpl_vars['day_dayweek']->value[1]) {?>
                                    <td></td>
                                    <?php $_smarty_tpl->tpl_vars['day_of_week'] = new Smarty_variable(($_smarty_tpl->tpl_vars['day_of_week']->value+1)%7, null, 0);?>
                                <?php }?>
                                <?php  $_smarty_tpl->tpl_vars["dayweek"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["dayweek"]->_loop = false;
 $_smarty_tpl->tpl_vars["day"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['day_dayweek']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["dayweek"]->key => $_smarty_tpl->tpl_vars["dayweek"]->value) {
$_smarty_tpl->tpl_vars["dayweek"]->_loop = true;
 $_smarty_tpl->tpl_vars["day"]->value = $_smarty_tpl->tpl_vars["dayweek"]->key;
?>
                                    <?php if ($_smarty_tpl->tpl_vars['dayweek']->value==$_smarty_tpl->tpl_vars['first_day']->value) {?>
                                        </tr><tr>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['currend_date']->value==0&&$_smarty_tpl->tpl_vars['sd_vmfv_vacation_data']->value['currend_date'][$_smarty_tpl->tpl_vars['year']->value][$_smarty_tpl->tpl_vars['month']->value][$_smarty_tpl->tpl_vars['day']->value]) {?>
                                        <?php $_smarty_tpl->tpl_vars['currend_date'] = new Smarty_variable(1, null, 0);?>
                                    <?php }?>
                                    <td> 
                                        <div id="id_cg_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['month']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8');?>
" class="control-group <?php if ($_smarty_tpl->tpl_vars['currend_date']->value==0) {?> sd_vmfv_control_group_disable <?php }
if ($_smarty_tpl->tpl_vars['sd_vmfv_vacation_data']->value[$_smarty_tpl->tpl_vars['year']->value][$_smarty_tpl->tpl_vars['month']->value][$_smarty_tpl->tpl_vars['day']->value]) {?> sd_vmfv_control_group_checked <?php } else { ?> sd_vmfv_control_group_unchecked <?php }?>">
                                            <label class="control-label" for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['month']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8');?>
</label>
                                            <?php if ($_smarty_tpl->tpl_vars['currend_date']->value) {?>
                                            <div class="controls">
                                                <input id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['month']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8');?>
" control-group="id_cg_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['month']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8');?>
" type="checkbox"  name="dates[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8');?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['month']->value, ENT_QUOTES, 'UTF-8');?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8');?>
]" <?php if ($_smarty_tpl->tpl_vars['sd_vmfv_vacation_data']->value[$_smarty_tpl->tpl_vars['year']->value][$_smarty_tpl->tpl_vars['month']->value][$_smarty_tpl->tpl_vars['day']->value]) {?> checked="checked" <?php }?> value="v">
                                            </div>
                                            <?php }?>
                                        </div>
                                    </td>
                                <?php } ?>
                                </tr>
                            </tbody>
                    </table></div>
                <?php } ?>
            <?php } ?>
        </div>
        
        <div class="control-group">
            <label for="elm_working_hours_from" class="control-label cm-regexp" data-ca-regexp="^([0-1]\d|2[0-3])(:[0-5]\d)$" data-ca-message="<?php echo $_smarty_tpl->__("the_time_format_must_be_hhmm");?>
"><?php echo $_smarty_tpl->__("work_time_start");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("ttc_work_time_start")), 0);?>
:</label>
            <div class="controls">
                <input type="text" name="company_data[working_hours_from]" id="elm_working_hours_from" size="10" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['working_hours_from'], ENT_QUOTES, 'UTF-8');?>
" class="input-small" placeholder="<?php echo $_smarty_tpl->__("hh_mm");?>
"/>
            </div>
        </div>
        <div class="control-group">
            <label for="elm_working_hours_to" class="control-label cm-regexp" data-ca-regexp="^([0-1]\d|2[0-3])(:[0-5]\d)$" data-ca-message="<?php echo $_smarty_tpl->__("the_time_format_must_be_hhmm");?>
"><?php echo $_smarty_tpl->__("work_time_end");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("ttc_work_time_end")), 0);?>
:</label>
            <div class="controls">
                <input type="text" name="company_data[working_hours_to]" id="elm_working_hours_to" size="10" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['working_hours_to'], ENT_QUOTES, 'UTF-8');?>
" class="input-small" placeholder="<?php echo $_smarty_tpl->__("hh_mm");?>
" />
            </div>
        </div>
    </div>

    <literal>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
>
            $(".sd_vmfv_calendars input[type='checkbox']").click(function () {
                var id_cg = $(this).attr("control-group");
                var elm_id_cg = $("#"+id_cg);
                if ($(this).prop("checked")) {
                    elm_id_cg.addClass("sd_vmfv_control_group_checked");
                    elm_id_cg.removeClass("sd_vmfv_control_group_unchecked");
                } else {
                    elm_id_cg.removeClass("sd_vmfv_control_group_checked");
                    elm_id_cg.addClass("sd_vmfv_control_group_unchecked");
                }
            });
        <?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </literal>
<?php }?>
<?php }} ?>
