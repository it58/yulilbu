<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 12:35:41
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/views/messenger/components/tickets_search_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8630344495d4a632daccb72-72535467%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5664adb002a13bb35064773b51316fcc80393bb1' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/sd_messaging_system/views/messenger/components/tickets_search_form.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '8630344495d4a632daccb72-72535467',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_meta' => 0,
    'selected_section' => 0,
    'put_request_vars' => 0,
    'extra' => 0,
    'search' => 0,
    'auth' => 0,
    'runtime' => 0,
    'dispatch' => 0,
    'in_popup' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a632daf2f63_35577957',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a632daf2f63_35577957')) {function content_5d4a632daf2f63_35577957($_smarty_tpl) {?><?php if (!is_callable('smarty_function_array_to_fields')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.array_to_fields.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ticket_id','author','recipient','company','period'));
?>
<div class="sidebar-row">
    <form name="tickets_search_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="get" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_meta']->value, ENT_QUOTES, 'UTF-8');?>
">
        <?php $_smarty_tpl->_capture_stack[0][] = array("simple_search", null, null); ob_start(); ?>
        <?php if ($_REQUEST['redirect_url']) {?>
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_REQUEST['redirect_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['selected_section']->value!='') {?>
        <input type="hidden" id="selected_section" name="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_section']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['put_request_vars']->value) {?>
            <?php echo smarty_function_array_to_fields(array('data'=>$_REQUEST,'skip'=>array("callback"),'escape'=>array("data_id")),$_smarty_tpl);?>

        <?php }?>

        <?php echo $_smarty_tpl->tpl_vars['extra']->value;?>

        <div class="sidebar-field">
            <label for="elm_author"><?php echo $_smarty_tpl->__("ticket_id");?>
</label>
            <div class="break">
                <input type="text" name="ticket_id" id="elm_author_name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <div class="sidebar-field">
            <label for="elm_author"><?php echo $_smarty_tpl->__("author");?>
</label>
            <div class="break">
                <input type="text" name="author" id="elm_author_name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['author'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <div class="sidebar-field">
            <label for="elm_recipient"><?php echo $_smarty_tpl->__("recipient");?>
</label>
            <div class="break">
                <input type="text" name="recipient" id="elm_recipient" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['recipient'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_type']=="A"&&!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
            <div class="sidebar-field">
                <label for="elm_company"><?php echo $_smarty_tpl->__("company");?>
</label>
                <div class="break">
                    <input type="text" name="company" id="elm_company" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['company'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>
        <?php }?>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

        <?php $_smarty_tpl->_capture_stack[0][] = array("advanced_search", null, null); ob_start(); ?>
        <div class="group form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo $_smarty_tpl->__("period");?>
</label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/period_selector.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('period'=>$_smarty_tpl->tpl_vars['search']->value['period'],'form_name'=>"tickets_search_form"), 0);?>

                </div>
            </div>
        </div>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/advanced_search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('simple_search'=>Smarty::$_smarty_vars['capture']['simple_search'],'advanced_search'=>Smarty::$_smarty_vars['capture']['advanced_search'],'dispatch'=>$_smarty_tpl->tpl_vars['dispatch']->value,'view_type'=>"tickets",'in_popup'=>$_smarty_tpl->tpl_vars['in_popup']->value), 0);?>

    </form>

</div><hr>
<?php }} ?>
