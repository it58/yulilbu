<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:59:00
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/ebay/hooks/products/action_buttons.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3822191635d484434babe61-11768506%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '45af298ee94e472ca2d69794a34edc8bc3ff23a3' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/ebay/hooks/products/action_buttons.post.tpl',
      1 => 1565015958,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3822191635d484434babe61-11768506',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d484434bbc117_49489497',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d484434bbc117_49489497')) {function content_5d484434bbc117_49489497($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('export_products_to_ebay','ebay_end_products_on_ebay','ebay_sync_products_status'));
?>
<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'class'=>"cm-process-items cm-ajax cm-comet",'text'=>$_smarty_tpl->__("export_products_to_ebay"),'dispatch'=>"dispatch[ebay.export]",'form'=>"manage_products_form"));?>
</li>
    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'class'=>"cm-process-items cm-ajax cm-comet",'text'=>$_smarty_tpl->__("ebay_end_products_on_ebay"),'dispatch'=>"dispatch[ebay.end_products]",'form'=>"manage_products_form"));?>
</li>
    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'class'=>"cm-process-items cm-ajax cm-comet",'text'=>$_smarty_tpl->__("ebay_sync_products_status"),'dispatch'=>"dispatch[ebay.update_product_status]",'form'=>"manage_products_form"));?>
</li>
<?php }?><?php }} ?>
