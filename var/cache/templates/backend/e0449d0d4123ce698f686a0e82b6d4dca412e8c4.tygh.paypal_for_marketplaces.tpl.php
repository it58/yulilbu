<?php /* Smarty version Smarty-3.1.21, created on 2019-08-08 09:11:09
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/paypal_for_marketplaces/views/payments/components/cc_processors/paypal_for_marketplaces.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11507612355d4b84bd091f95-60521002%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e0449d0d4123ce698f686a0e82b6d4dca412e8c4' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/paypal_for_marketplaces/views/payments/components/cc_processors/paypal_for_marketplaces.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '11507612355d4b84bd091f95-60521002',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'payment_id' => 0,
    'processor_params' => 0,
    'suffix' => 0,
    'currencies' => 0,
    'code' => 0,
    'currency' => 0,
    'source' => 0,
    'shape' => 0,
    'color' => 0,
    'size' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4b84bd105031_38542531',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4b84bd105031_38542531')) {function content_5d4b84bd105031_38542531($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('paypal_for_marketplaces.webhook_notice','paypal_for_marketplaces.settings.account','paypal_for_marketplaces.bn_code','paypal_for_marketplaces.payer_id','paypal_for_marketplaces.client_id','paypal_for_marketplaces.secret','test_live_mode','test','live','currency','paypal_for_markeplaces.settings.funding','paypal_for_marketplaces.source.','paypal_for_markeplaces.settings.style','paypal_for_marketplaces.style.shape','paypal_for_marketplaces.shape.','paypal_for_marketplaces.style.color','paypal_for_marketplaces.color.','paypal_for_marketplaces.style.size','paypal_for_marketplaces.size.'));
?>
<?php $_smarty_tpl->tpl_vars['suffix'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['payment_id']->value)===null||$tmp==='' ? 0 : $tmp), null, 0);?>

<p>
    <?php echo $_smarty_tpl->__("paypal_for_marketplaces.webhook_notice",array("[url]"=>fn_url("paypal_for_marketplaces.webhook","C")));?>

</p>

<hr>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("paypal_for_marketplaces.settings.account")), 0);?>


<input type="hidden"
       name="payment_data[processor_params][is_paypal_for_marketplaces]"
       value="Y"
/>

<input type="hidden"
       name="payment_data[processor_params][access_token]"
       value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['processor_params']->value['access_token'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
/>

<input type="hidden"
       name="payment_data[processor_params][expiry_time]"
       value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['processor_params']->value['expiry_time'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'UTF-8');?>
"
/>

<div class="control-group">
    <label for="elm_bn_code<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label cm-required"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.bn_code");?>
:</label>
    <div class="controls">
        <input type="text"
               name="payment_data[processor_params][bn_code]"
               id="elm_bn_code<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['bn_code'], ENT_QUOTES, 'UTF-8');?>
"
        />
    </div>
</div>

<div class="control-group">
    <label for="elm_payer_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label cm-required"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.payer_id");?>
:</label>
    <div class="controls">
        <input type="text"
               name="payment_data[processor_params][payer_id]"
               id="elm_payer_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['payer_id'], ENT_QUOTES, 'UTF-8');?>
"
        />
    </div>
</div>

<div class="control-group">
    <label for="elm_client_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label cm-required"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.client_id");?>
:</label>
    <div class="controls">
        <input type="text"
               name="payment_data[processor_params][client_id]"
               id="elm_client_id<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['client_id'], ENT_QUOTES, 'UTF-8');?>
"
        />
    </div>
</div>

<div class="control-group">
    <label for="elm_secret<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label cm-required"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.secret");?>
:</label>
    <div class="controls">
        <input type="password"
               name="payment_data[processor_params][secret]"
               id="elm_secret<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['processor_params']->value['secret'], ENT_QUOTES, 'UTF-8');?>
"
        />
    </div>
</div>

<div class="control-group">
    <label for="elm_mode<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label"
    ><?php echo $_smarty_tpl->__("test_live_mode");?>
:</label>
    <div class="controls">
        <select name="payment_data[processor_params][mode]"
                id="elm_mode<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
        >
            <option value="test"
                    <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="test") {?>selected="selected"<?php }?>
            ><?php echo $_smarty_tpl->__("test");?>
</option>
            <option value="live"
                    <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['mode']=="live") {?>selected="selected"<?php }?>
            ><?php echo $_smarty_tpl->__("live");?>
</option>
        </select>
    </div>
</div>

<div class="control-group">
    <label for="elm_currency<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label"
    ><?php echo $_smarty_tpl->__("currency");?>
:</label>
    <div class="controls">
        <select name="payment_data[processor_params][currency]"
                id="elm_currency<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
                data-ca-paypal-for-marketplaces-element="currency"
                data-ca-paypal-for-marketplaces-credit-selector="#elm_funding_credit<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
        >
            <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value) {
$_smarty_tpl->tpl_vars['currency']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['currency']->key;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"
                        <?php if ($_smarty_tpl->tpl_vars['processor_params']->value['currency']==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?>
                ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['description'], ENT_QUOTES, 'UTF-8');?>
</option>
            <?php } ?>
        </select>
    </div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("paypal_for_markeplaces.settings.funding")), 0);?>



<?php  $_smarty_tpl->tpl_vars['source'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['source']->_loop = false;
 $_from = array("card","credit","elv"); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['source']->key => $_smarty_tpl->tpl_vars['source']->value) {
$_smarty_tpl->tpl_vars['source']->_loop = true;
?>
    <div class="control-group">
        <label for="elm_funding_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['source']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
               class="control-label"
        ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.source.".((string)$_smarty_tpl->tpl_vars['source']->value));?>
:</label>
        <div class="controls">
            <input type="hidden"
                   name="payment_data[processor_params][funding][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['source']->value, ENT_QUOTES, 'UTF-8');?>
]"
                   value=""
            />
            <input type="checkbox"
                   name="payment_data[processor_params][funding][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['source']->value, ENT_QUOTES, 'UTF-8');?>
]"
                   id="elm_funding_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['source']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
                   value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['source']->value, ENT_QUOTES, 'UTF-8');?>
"
                   <?php if ((($tmp = @$_smarty_tpl->tpl_vars['processor_params']->value['funding'][$_smarty_tpl->tpl_vars['source']->value])===null||$tmp==='' ? '' : $tmp)) {?>checked="checked"<?php }?>
            />
        </div>
    </div>
<?php } ?>


<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("paypal_for_markeplaces.settings.style")), 0);?>


<div class="control-group">
    <label for="elm_shape<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.style.shape");?>
:</label>
    <div class="controls">
        <select name="payment_data[processor_params][style][shape]"
                id="elm_shape<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
        >
            <?php  $_smarty_tpl->tpl_vars['shape'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['shape']->_loop = false;
 $_from = array("pill","rect"); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['shape']->key => $_smarty_tpl->tpl_vars['shape']->value) {
$_smarty_tpl->tpl_vars['shape']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shape']->value, ENT_QUOTES, 'UTF-8');?>
"
                        <?php if ((($tmp = @$_smarty_tpl->tpl_vars['processor_params']->value['style']['shape'])===null||$tmp==='' ? "pill" : $tmp)==$_smarty_tpl->tpl_vars['shape']->value) {?>selected="selected"<?php }?>
                ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.shape.".((string)$_smarty_tpl->tpl_vars['shape']->value));?>
</option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label for="elm_color<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.style.color");?>
:</label>
    <div class="controls">
        <select name="payment_data[processor_params][style][color]"
                id="elm_color<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
        >
            <?php  $_smarty_tpl->tpl_vars['color'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['color']->_loop = false;
 $_from = array("gold","blue","silver","black"); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['color']->key => $_smarty_tpl->tpl_vars['color']->value) {
$_smarty_tpl->tpl_vars['color']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['color']->value, ENT_QUOTES, 'UTF-8');?>
"
                        <?php if ((($tmp = @$_smarty_tpl->tpl_vars['processor_params']->value['style']['color'])===null||$tmp==='' ? "gold" : $tmp)==$_smarty_tpl->tpl_vars['color']->value) {?>selected="selected"<?php }?>
                ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.color.".((string)$_smarty_tpl->tpl_vars['color']->value));?>
</option>
            <?php } ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label for="elm_size<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
           class="control-label"
    ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.style.size");?>
:</label>
    <div class="controls">
        <select name="payment_data[processor_params][style][size]"
                id="elm_size<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
"
        >
            <?php  $_smarty_tpl->tpl_vars['size'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['size']->_loop = false;
 $_from = array("small","medium","large","responsive"); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['size']->key => $_smarty_tpl->tpl_vars['size']->value) {
$_smarty_tpl->tpl_vars['size']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['size']->value, ENT_QUOTES, 'UTF-8');?>
"
                        <?php if ((($tmp = @$_smarty_tpl->tpl_vars['processor_params']->value['style']['size'])===null||$tmp==='' ? "medium" : $tmp)==$_smarty_tpl->tpl_vars['size']->value) {?>selected="selected"<?php }?>
                ><?php echo $_smarty_tpl->__("paypal_for_marketplaces.size.".((string)$_smarty_tpl->tpl_vars['size']->value));?>
</option>
            <?php } ?>
        </select>
    </div>
</div>
<?php }} ?>
