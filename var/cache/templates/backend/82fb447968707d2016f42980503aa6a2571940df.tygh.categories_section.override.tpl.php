<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 20:27:39
         compiled from "/home/yulibu/public_html/design/backend/templates/addons/wk_categories_extented_view/hooks/products/categories_section.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4206476995d49804b7dc780-76643255%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '82fb447968707d2016f42980503aa6a2571940df' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/addons/wk_categories_extented_view/hooks/products/categories_section.override.tpl',
      1 => 1565015959,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4206476995d49804b7dc780-76643255',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mode' => 0,
    'product_data' => 0,
    'companies_tooltip' => 0,
    'result_ids' => 0,
    'wk_category_path' => 0,
    'wk_parent_category_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d49804b7f5af6_52092496',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d49804b7f5af6_52092496')) {function content_5d49804b7f5af6_52092496($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.hook.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('categories','select_a_category'));
?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"products:categories_section")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"products:categories_section"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable("product_categories", null, 0);?>
    <?php if (fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['mode']->value!="add") {?>
            <?php $_smarty_tpl->tpl_vars['js_action'] = new Smarty_variable("fn_change_vendor_for_product();", null, 0);?>
    <?php }?>
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"companies:product_details_fields")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"companies:product_details_fields"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php echo $_smarty_tpl->getSubTemplate ("views/companies/components/company_field.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name'=>"product_data[company_id]",'id'=>"product_data_company_id",'selected'=>$_smarty_tpl->tpl_vars['product_data']->value['company_id'],'tooltip'=>$_smarty_tpl->tpl_vars['companies_tooltip']->value), 0);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"companies:product_details_fields"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" name="result_ids">
    <div class="control-group" id="product_categories">
        <label for="selected_category_id" class="control-label cm-required"><?php echo $_smarty_tpl->__("categories");?>
</label>
        <div class="controls">
            <input type="hidden" name="product_data[category_ids]" id="selected_category_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['category_ids'][0], ENT_QUOTES, 'UTF-8');?>
">
            <span id="selected_category_title" class="hidden"></span>
            <div class="category_main_container_box">
                <span class="pull-right toggle_category_span" >
                    <a class="toggle_category_box"><?php if ($_smarty_tpl->tpl_vars['wk_category_path']->value) {?>change<?php } else { ?>close<?php }?></a>
                </span>
                <div class="category_title_container">
                    <span id="category_title_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['wk_parent_category_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="select_a_category_title"><?php echo $_smarty_tpl->__("select_a_category");?>
</span>
                </div>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_categories_extented_view/common/picker.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"products:categories_section"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>
