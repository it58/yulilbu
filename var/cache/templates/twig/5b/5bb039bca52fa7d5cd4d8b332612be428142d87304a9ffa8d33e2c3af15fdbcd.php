<?php

/* __string_template__c59faa8e46d68d0b65bf9bf5d5443709634de8a77da0bcf86e9c2bb354e7fe80 */
class __TwigTemplate_de1fd75eb527c8936decc193a0442ecc3efb01e50739fa421c6936cca962425f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ": ";
        echo (isset($context["subject"]) ? $context["subject"] : null);
    }

    public function getTemplateName()
    {
        return "__string_template__c59faa8e46d68d0b65bf9bf5d5443709634de8a77da0bcf86e9c2bb354e7fe80";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ company_name }}: {{ subject }}*/
