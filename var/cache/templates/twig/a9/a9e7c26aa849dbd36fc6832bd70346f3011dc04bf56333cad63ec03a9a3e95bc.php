<?php

/* __string_template__c945e78442bcf3720d4535865202f014bf394ad7d451618413c076ac42a81749 */
class __TwigTemplate_5bf6e38fd0422d4de4669cf6889444122aaaccb74a86cef417a476bced7ead97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "event_company_new", array("[vendor_name]" => $this->getAttribute((isset($context["company_data"]) ? $context["company_data"] : null), "company_name", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__c945e78442bcf3720d4535865202f014bf394ad7d451618413c076ac42a81749";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("event_company_new", {"[vendor_name]" : company_data.company_name}) }}*/
