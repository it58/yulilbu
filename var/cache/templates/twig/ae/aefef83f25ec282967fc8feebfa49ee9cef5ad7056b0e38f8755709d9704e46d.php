<?php

/* __string_template__8b33c59fc1895aba6db42e090d7385e84d374f629d6cb933f9079d8f56724c33 */
class __TwigTemplate_7db7d6892c0b28f10acf5fc3cea064af5dbd52124efa7948ade58dd3990d533f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ": ";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "new_ticket_notification");
    }

    public function getTemplateName()
    {
        return "__string_template__8b33c59fc1895aba6db42e090d7385e84d374f629d6cb933f9079d8f56724c33";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ company_name }}: {{ __("new_ticket_notification") }}*/
