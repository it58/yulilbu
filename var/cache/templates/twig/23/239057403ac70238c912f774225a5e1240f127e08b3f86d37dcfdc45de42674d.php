<?php

/* __string_template__e3f776d62d9af1dc0b52a8f7a39c6987c14ab87e5c7b1778e9cdbe8c6ad22e53 */
class __TwigTemplate_6e9c60fadb0d2e081dda5f971f58b03358a197673f432ec4caa9191043c73373 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "event_company_new", array("[vendor_name]" => $this->getAttribute((isset($context["company_data"]) ? $context["company_data"] : null), "company_name", array())))));
        echo "
            <h4><div style=\"margin-bottom: 15px;\">";
        // line 2
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "dear");
        echo " ";
        if ($this->getAttribute((isset($context["follower"]) ? $context["follower"] : null), "b_firstname", array())) {
            echo " ";
            echo $this->getAttribute((isset($context["follower"]) ? $context["follower"] : null), "b_firstname", array());
            echo ", ";
        } else {
            echo " ";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "follower");
            echo "! ";
        }
        echo "</div></h4>
            ";
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "event_company_we_are_writting");
        echo ": <br /><br />
            ";
        // line 4
        if ((isset($context["image"]) ? $context["image"] : null)) {
            echo "<span style=\"float: left; margin: 0px 15px 15px 0px;\">";
            echo (isset($context["image"]) ? $context["image"] : null);
            echo "</span>";
        }
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, (isset($context["lang_var"]) ? $context["lang_var"] : null), array("[vendor_name]" => (isset($context["company_name"]) ? $context["company_name"] : null), "[url]" => (isset($context["url"]) ? $context["url"] : null)));
        echo "
            ";
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__e3f776d62d9af1dc0b52a8f7a39c6987c14ab87e5c7b1778e9cdbe8c6ad22e53";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 5,  41 => 4,  37 => 3,  23 => 2,  19 => 1,);
    }
}
/* {{ snippet("header", {"title": __("event_company_new", {"[vendor_name]" : company_data.company_name})}) }}*/
/*             <h4><div style="margin-bottom: 15px;">{{ __("dear") }} {% if follower.b_firstname %} {{ follower.b_firstname }}, {% else %} {{ __("follower") }}! {% endif %}</div></h4>*/
/*             {{ __("event_company_we_are_writting") }}: <br /><br />*/
/*             {% if image %}<span style="float: left; margin: 0px 15px 15px 0px;">{{ image }}</span>{% endif %}{{__(lang_var , {"[vendor_name]" : company_name, "[url]" : url}) }}*/
/*             {{ snippet("footer") }}*/
