<?php

/* __string_template__9bf440a979307118a47dc1b90ea381fd26871479998646058f7e4380e892e86c */
class __TwigTemplate_012b3b0f6dbc87f45c753e80edee661b6d47ced4f7dd387d01fdc96c10f36f51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "</td>
</tr>
<tr class=\"message-footer\" style=\"background: #fff;   padding: 20px 30px;    color: #444;    border-top: 1px solid #d2d6de;\">
<td>
<table style=\"width: 100%; margin-left: 30px; margin-top:20px; line-height: 1.8;\" align=\"left\">
  <tr  style=\"text-align: left; font-weight: bold; padding-top: 15px; font-size: 16px; font-family: tahoma;\">
    <td>Yulibu Office</td>
  </tr>
  <tr>
    <td style=\"text-align: left; padding: 10px 30px;\">
      <address style=\"font-style:normal !important;\"><p style=\"margin:0px;font-style:normal!important;\"><h4 style=\"line-height: 0px; margin: 15px 0px 5px 0px;\">Indonesia:</h4>
<span style=\"font-size:10px;\">Jl. Tombro Selatan, Malang 65142 Jawa Timur, Indonesia - Phone: +62 812 345 9069</span></p>
<p style=\"margin:0px;font-style:normal!important\"><h4 style=\"line-height: 0px;     margin: 15px 0px 5px 0px;\">Thailand:</h4>
<span style=\"font-size:10px;\">No 53/8 M.3 T. Yaha, A. Yaha Yala 95120, Thailand - Phone: +66 62 063 4740</span></p>
<p style=\"margin:0px;font-style:normal!important\"><h4 style=\"line-height: 0px;     margin: 15px 0px 5px 0px;\">United States:</h4>
<span style=\"font-size:10px;\">16192 Coastal Highway, Lewes, Delaware 19958, United States - Phone: +1 302-261-5255</span></p></address>
    </td>
  </tr>
<tr>
<td>
</td>
</tr>
<tr>
  <td><span style=\"font-size:10px;\">Email Us : info@yulibu.com - help@yulibu.com</span>
  </td>
</tr>
<tr>
";
        // line 28
        if ($this->getAttribute((isset($context["company_data"]) ? $context["company_data"] : null), "company_name", array())) {
            // line 29
            echo "<td style=\"text-align: left; padding: 10px 30px;\">

<br>
<br>
</td>
</tr>
</table>
</td>
</tr>
";
        }
        // line 39
        echo "</table>
<!-- content-wrapper -->
</td>
</tr>
</table>
<!-- main-wrapper -->
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__9bf440a979307118a47dc1b90ea381fd26871479998646058f7e4380e892e86c";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 39,  50 => 29,  48 => 28,  19 => 1,);
    }
}
/* </td>*/
/* </tr>*/
/* <tr class="message-footer" style="background: #fff;   padding: 20px 30px;    color: #444;    border-top: 1px solid #d2d6de;">*/
/* <td>*/
/* <table style="width: 100%; margin-left: 30px; margin-top:20px; line-height: 1.8;" align="left">*/
/*   <tr  style="text-align: left; font-weight: bold; padding-top: 15px; font-size: 16px; font-family: tahoma;">*/
/*     <td>Yulibu Office</td>*/
/*   </tr>*/
/*   <tr>*/
/*     <td style="text-align: left; padding: 10px 30px;">*/
/*       <address style="font-style:normal !important;"><p style="margin:0px;font-style:normal!important;"><h4 style="line-height: 0px; margin: 15px 0px 5px 0px;">Indonesia:</h4>*/
/* <span style="font-size:10px;">Jl. Tombro Selatan, Malang 65142 Jawa Timur, Indonesia - Phone: +62 812 345 9069</span></p>*/
/* <p style="margin:0px;font-style:normal!important"><h4 style="line-height: 0px;     margin: 15px 0px 5px 0px;">Thailand:</h4>*/
/* <span style="font-size:10px;">No 53/8 M.3 T. Yaha, A. Yaha Yala 95120, Thailand - Phone: +66 62 063 4740</span></p>*/
/* <p style="margin:0px;font-style:normal!important"><h4 style="line-height: 0px;     margin: 15px 0px 5px 0px;">United States:</h4>*/
/* <span style="font-size:10px;">16192 Coastal Highway, Lewes, Delaware 19958, United States - Phone: +1 302-261-5255</span></p></address>*/
/*     </td>*/
/*   </tr>*/
/* <tr>*/
/* <td>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/*   <td><span style="font-size:10px;">Email Us : info@yulibu.com - help@yulibu.com</span>*/
/*   </td>*/
/* </tr>*/
/* <tr>*/
/* {% if company_data.company_name %}*/
/* <td style="text-align: left; padding: 10px 30px;">*/
/* */
/* <br>*/
/* <br>*/
/* </td>*/
/* </tr>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* {% endif %}*/
/* </table>*/
/* <!-- content-wrapper -->*/
/* </td>*/
/* </tr>*/
/* </table>*/
/* <!-- main-wrapper -->*/
/* </body>*/
/* </html>*/
