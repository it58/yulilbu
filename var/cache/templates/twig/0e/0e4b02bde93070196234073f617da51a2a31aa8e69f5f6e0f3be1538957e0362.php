<?php

/* __string_template__6c6cf3c473b0f5ef49344615012ea01700caa8158ceaeb0858be60a361078a1e */
class __TwigTemplate_db17db3b8da8bf9ae371333cf9d3c684b4d47b4a0468d28fce7111aa0649eabd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["status"]) ? $context["status"] : null) == "Y")) {
            echo "    ";
            $context["text_status"] = $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "approved");
        } else {
            echo "    ";
            $context["text_status"] = $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "disapproved");
        }
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ": ";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "products_approval_status_changed", array("[status]" => (isset($context["text_status"]) ? $context["text_status"] : null)));
    }

    public function getTemplateName()
    {
        return "__string_template__6c6cf3c473b0f5ef49344615012ea01700caa8158ceaeb0858be60a361078a1e";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {% if status == "Y" %}    {% set text_status=__("approved") %}{% else %}    {% set text_status=__("disapproved") %}{% endif %}{{ company_name }}: {{ __("products_approval_status_changed", {"[status]": text_status}) }}*/
