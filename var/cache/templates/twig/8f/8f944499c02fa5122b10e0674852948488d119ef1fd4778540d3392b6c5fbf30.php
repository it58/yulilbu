<?php

/* __string_template__00aac8b45759f0d05561f562a1d72813f52a703c5003fc605937a1f62345b5b4 */
class __TwigTemplate_a94bfe7ea55d00a20abd5ae4f13d447a07820668957c79d93fb70f2f26fe5c98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "recover_password_subj")));
        echo "

    ";
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "text_confirm_passwd_recovery");
        echo ":
    <br />
    <br />

    <a href=\"";
        // line 7
        echo (isset($context["url"]) ? $context["url"] : null);
        echo "\">";
        echo $this->env->getExtension('tygh.core')->punyDecodeFilter((isset($context["url"]) ? $context["url"] : null));
        echo "</a>

";
        // line 9
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__00aac8b45759f0d05561f562a1d72813f52a703c5003fc605937a1f62345b5b4";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 9,  31 => 7,  24 => 3,  19 => 1,);
    }
}
/* {{ snippet("header", {"title": __("recover_password_subj") } ) }}*/
/* */
/*     {{ __("text_confirm_passwd_recovery") }}:*/
/*     <br />*/
/*     <br />*/
/* */
/*     <a href="{{ url }}">{{ url|puny_decode }}</a>*/
/* */
/* {{ snippet("footer") }}*/
