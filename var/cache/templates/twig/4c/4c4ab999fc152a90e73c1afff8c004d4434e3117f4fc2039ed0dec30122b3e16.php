<?php

/* __string_template__d3e71739a1a536224b9968791d1d364062fffe17da4de4a92aa091def1456350 */
class __TwigTemplate_26606961427c9d9d74112cddc8da5e61bfba179a732424e1e1d3e0faf178684a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((((((($this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_firstname", array()) || $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_lastname", array())) || $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_address", array())) || $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_address_2", array())) || $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_city", array())) || $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_state_descr", array())) || $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_zipcode", array()))) {
            // line 2
            echo "<h2 style=\"margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #e8e8e8; margin-bottom: 20px;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "bill_to");
            echo "</h2>
<p style=\"margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;\">
    <strong>";
            // line 4
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_firstname", array());
            echo " ";
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_lastname", array());
            echo "</strong>
</p>
<p style=\"margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;\">
    ";
            // line 7
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_address", array());
            echo " <br>";
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_address_2", array());
            echo "
</p>
<p style=\"margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;\">
    ";
            // line 10
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_city", array());
            echo ", ";
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_state_descr", array());
            echo " ";
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_zipcode", array());
            echo "
</p>
<p style=\"margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;\">
    ";
            // line 13
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_country_descr", array());
            echo "
</p>
<p style=\"margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;\">
    <bdi>";
            // line 16
            echo $this->getAttribute((isset($context["u"]) ? $context["u"] : null), "b_phone", array());
            echo "</bdi>
</p>
";
        }
    }

    public function getTemplateName()
    {
        return "__string_template__d3e71739a1a536224b9968791d1d364062fffe17da4de4a92aa091def1456350";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 16,  53 => 13,  43 => 10,  35 => 7,  27 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if u.b_firstname or u.b_lastname or u.b_address or u.b_address_2 or u.b_city or u.b_state_descr or u.b_zipcode %}*/
/* <h2 style="margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #e8e8e8; margin-bottom: 20px;">{{__("bill_to")}}</h2>*/
/* <p style="margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;">*/
/*     <strong>{{u.b_firstname}} {{u.b_lastname}}</strong>*/
/* </p>*/
/* <p style="margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;">*/
/*     {{u.b_address}} <br>{{u.b_address_2}}*/
/* </p>*/
/* <p style="margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;">*/
/*     {{u.b_city}}, {{u.b_state_descr}} {{u.b_zipcode}}*/
/* </p>*/
/* <p style="margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;">*/
/*     {{u.b_country_descr}}*/
/* </p>*/
/* <p style="margin: 0px; padding-bottom: 5px; font-family: Helvetica, Arial, sans-serif;">*/
/*     <bdi>{{u.b_phone}}</bdi>*/
/* </p>*/
/* {% endif %}*/
