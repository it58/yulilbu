<?php

/* __string_template__d84a3e9b0ffcb06fae8f4f5ae34a1c49610ac1bb7f1159fb2b9987c8457c0ada */
class __TwigTemplate_4f72ef1e0ce79d7e8e1e549de74dd45245737f896afb396ef3be7598d67c4db6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["status"]) ? $context["status"] : null) == "Y")) {
            // line 2
            echo "    ";
            $context["text_status"] = $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "approved");
        } else {
            // line 4
            echo "    ";
            $context["text_status"] = $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "disapproved");
        }
        // line 6
        echo "
";
        // line 7
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header");
        echo "

  ";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
        echo ",<br /><br />

";
        // line 11
        if (((isset($context["status"]) ? $context["status"] : null) == "Y")) {
            // line 12
            echo "    ";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "products_approval_status_approved");
            echo "<br />
";
        } else {
            // line 14
            echo "    ";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "products_approval_status_disapproved");
            echo "<br />
";
        }
        // line 16
        echo "
";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 18
            echo "    ";
            echo $this->getAttribute($context["loop"], "index", array());
            echo ") <a href=\"";
            echo $this->getAttribute($context["product"], "url", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "product", array());
            echo "</a><br />
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "
";
        // line 21
        if (((isset($context["status"]) ? $context["status"] : null) == "Y")) {
            // line 22
            echo "    <br />";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "text_shoppers_can_order_products");
            echo "
";
        }
        // line 24
        if ((isset($context["reason"]) ? $context["reason"] : null)) {
            // line 25
            echo "    <p>";
            echo (isset($context["reason"]) ? $context["reason"] : null);
            echo "</p>
";
        }
        // line 27
        echo "
";
        // line 28
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__d84a3e9b0ffcb06fae8f4f5ae34a1c49610ac1bb7f1159fb2b9987c8457c0ada";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 28,  116 => 27,  110 => 25,  108 => 24,  102 => 22,  100 => 21,  97 => 20,  76 => 18,  59 => 17,  56 => 16,  50 => 14,  44 => 12,  42 => 11,  37 => 9,  32 => 7,  29 => 6,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if status == "Y" %}*/
/*     {% set text_status=__("approved") %}*/
/* {% else %}*/
/*     {% set text_status=__("disapproved") %}*/
/* {% endif %}*/
/* */
/* {{ snippet("header") }}*/
/* */
/*   {{ __("hello") }},<br /><br />*/
/* */
/* {% if status == "Y" %}*/
/*     {{ __("products_approval_status_approved") }}<br />*/
/* {% else %}*/
/*     {{ __("products_approval_status_disapproved") }}<br />*/
/* {% endif %}*/
/* */
/* {% for product in products %}*/
/*     {{ loop.index }}) <a href="{{ product.url }}">{{ product.product }}</a><br />*/
/* {% endfor %}*/
/* */
/* {% if status == "Y" %}*/
/*     <br />{{ __("text_shoppers_can_order_products") }}*/
/* {% endif %}*/
/* {% if reason %}*/
/*     <p>{{ reason }}</p>*/
/* {% endif %}*/
/* */
/* {{ snippet("footer") }}*/
