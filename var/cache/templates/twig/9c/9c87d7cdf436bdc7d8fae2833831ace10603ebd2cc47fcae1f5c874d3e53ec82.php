<?php

/* __string_template__77ead6b768dd779da2344fa0a71664e3af7103c6cf138bcf281e00b63a5316c6 */
class __TwigTemplate_bd74c32c5afe4f13fa46ff6cdf695204a2d3d752ccf6d35d1faba4a4ce1b7a96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "new_ticket")));
        echo "

    ";
        // line 3
        if ((isset($context["message_company_name"]) ? $context["message_company_name"] : null)) {
            echo " ";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "dear");
            echo " ";
            echo (isset($context["message_company_name"]) ? $context["message_company_name"] : null);
            echo ", ";
        } else {
            echo " ";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
            echo ", ";
        }
        // line 4
        echo "    <br />
    <br />
    ";
        // line 6
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "you_have_a_new_ticket");
        if ((isset($context["message_user_name"]) ? $context["message_user_name"] : null)) {
            echo " ";
            echo twig_lower_filter($this->env, $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "from"));
            echo " ";
            echo (isset($context["message_user_name"]) ? $context["message_user_name"] : null);
        }
        echo ".
    <br />
    <br />
    <b><a href=\"";
        // line 9
        echo (isset($context["ticket_url"]) ? $context["ticket_url"] : null);
        echo "\">";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "check_ticket");
        echo "</a></b>

";
        // line 11
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__77ead6b768dd779da2344fa0a71664e3af7103c6cf138bcf281e00b63a5316c6";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 11,  52 => 9,  40 => 6,  36 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ snippet("header", {"title": __("new_ticket") } ) }}*/
/* */
/*     {% if message_company_name %} {{ __("dear") }} {{ message_company_name }}, {% else %} {{ __("hello") }}, {% endif %}*/
/*     <br />*/
/*     <br />*/
/*     {{ __("you_have_a_new_ticket") }}{% if message_user_name %} {{__("from")|lower}} {{ message_user_name }}{% endif %}.*/
/*     <br />*/
/*     <br />*/
/*     <b><a href="{{ ticket_url }}">{{__("check_ticket")}}</a></b>*/
/* */
/* {{ snippet("footer") }}*/
