<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 11:04:53
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_facebook_pixel/hooks/checkout/order_confirmation.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3508411975d48fc65df3e73-89826623%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cb81fbf775d64bace51b33bd537e91afc5733b54' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_facebook_pixel/hooks/checkout/order_confirmation.pre.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3508411975d48fc65df3e73-89826623',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'identifiers_facebook_pixel' => 0,
    'item' => 0,
    'childs_order_total' => 0,
    'order_info' => 0,
    'product' => 0,
    'total' => 0,
    'currencies' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48fc65e34e84_56890127',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48fc65e34e84_56890127')) {function content_5d48fc65e34e84_56890127($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/yulibu/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['purchase_facebook_pixel']=='Y'||$_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['add_payment_info_facebook_pixel']=='Y') {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        var pixel_id = '<?php echo htmlspecialchars($_REQUEST['pixel_id'], ENT_QUOTES, 'UTF-8');?>
';
        if (!pixel_id) {
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['identifiers_facebook_pixel']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel']&&$_smarty_tpl->tpl_vars['item']->value['company_id']) {?>
                    var el = document.createElement("iframe");
                    document.body.appendChild(el);
                    el.id = 'iframe';
                    el.style.width = "1px";
                    el.style.height = "1px";
                    el.style.position = 'absolute';
                    el.style.left = '-9999px';
                    el.src = fn_url('checkout.complete?order_id=<?php echo htmlspecialchars($_REQUEST['order_id'], ENT_QUOTES, 'UTF-8');?>
&pixel_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel'], ENT_QUOTES, 'UTF-8');?>
&company_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['company_id'], ENT_QUOTES, 'UTF-8');?>
');
                <?php }?>
            <?php } ?>
        }
    <?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->tpl_vars["total"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['childs_order_total']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['order_info']->value['total'] : $tmp), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['purchase_facebook_pixel']=='Y'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        <?php if (@constant('AJAX_REQUEST')) {?>
            /*ajax_rnd_<?php echo htmlspecialchars(mt_rand(0,10000000), ENT_QUOTES, 'UTF-8');?>
*/
        <?php }?>
        (function(_, $) {
            fbq('track', 'Purchase', {
                <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['account_facebook_pixel']=='business') {?>
                content_type: 'product',
                content_ids: [<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?> <?php if (!$_REQUEST['pixel_id']||$_smarty_tpl->tpl_vars['product']->value['company_id']==$_REQUEST['company_id']) {?> '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
',<?php }
} ?>],
                <?php }?>
                value: <?php echo smarty_function_math(array('equation'=>((string)$_smarty_tpl->tpl_vars['total']->value)." / ".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['order_info']->value['secondary_currency']]['coefficient']),'format'=>"%.".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['order_info']->value['secondary_currency']]['decimals'])."f"),$_smarty_tpl);?>
,
                currency: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['secondary_currency'], ENT_QUOTES, 'UTF-8');?>
'
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['add_payment_info_facebook_pixel']=='Y'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        <?php if (@constant('AJAX_REQUEST')) {?>
            /*ajax_rnd_<?php echo htmlspecialchars(mt_rand(0,10000000), ENT_QUOTES, 'UTF-8');?>
*/
        <?php }?>
        (function(_, $) {
            fbq('track', 'AddPaymentInfo');
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_facebook_pixel/hooks/checkout/order_confirmation.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_facebook_pixel/hooks/checkout/order_confirmation.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['purchase_facebook_pixel']=='Y'||$_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['add_payment_info_facebook_pixel']=='Y') {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        var pixel_id = '<?php echo htmlspecialchars($_REQUEST['pixel_id'], ENT_QUOTES, 'UTF-8');?>
';
        if (!pixel_id) {
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['identifiers_facebook_pixel']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel']&&$_smarty_tpl->tpl_vars['item']->value['company_id']) {?>
                    var el = document.createElement("iframe");
                    document.body.appendChild(el);
                    el.id = 'iframe';
                    el.style.width = "1px";
                    el.style.height = "1px";
                    el.style.position = 'absolute';
                    el.style.left = '-9999px';
                    el.src = fn_url('checkout.complete?order_id=<?php echo htmlspecialchars($_REQUEST['order_id'], ENT_QUOTES, 'UTF-8');?>
&pixel_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel'], ENT_QUOTES, 'UTF-8');?>
&company_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['company_id'], ENT_QUOTES, 'UTF-8');?>
');
                <?php }?>
            <?php } ?>
        }
    <?php echo '</script'; ?>
>
<?php }?>

<?php $_smarty_tpl->tpl_vars["total"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['childs_order_total']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['order_info']->value['total'] : $tmp), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['purchase_facebook_pixel']=='Y'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        <?php if (@constant('AJAX_REQUEST')) {?>
            /*ajax_rnd_<?php echo htmlspecialchars(mt_rand(0,10000000), ENT_QUOTES, 'UTF-8');?>
*/
        <?php }?>
        (function(_, $) {
            fbq('track', 'Purchase', {
                <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['account_facebook_pixel']=='business') {?>
                content_type: 'product',
                content_ids: [<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?> <?php if (!$_REQUEST['pixel_id']||$_smarty_tpl->tpl_vars['product']->value['company_id']==$_REQUEST['company_id']) {?> '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
',<?php }
} ?>],
                <?php }?>
                value: <?php echo smarty_function_math(array('equation'=>((string)$_smarty_tpl->tpl_vars['total']->value)." / ".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['order_info']->value['secondary_currency']]['coefficient']),'format'=>"%.".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['order_info']->value['secondary_currency']]['decimals'])."f"),$_smarty_tpl);?>
,
                currency: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['secondary_currency'], ENT_QUOTES, 'UTF-8');?>
'
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['add_payment_info_facebook_pixel']=='Y'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        <?php if (@constant('AJAX_REQUEST')) {?>
            /*ajax_rnd_<?php echo htmlspecialchars(mt_rand(0,10000000), ENT_QUOTES, 'UTF-8');?>
*/
        <?php }?>
        (function(_, $) {
            fbq('track', 'AddPaymentInfo');
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }?>
<?php }?><?php }} ?>
