<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:53:56
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_google_analytics/hooks/index/scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:982068315d484304847339-87972472%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f1a308217589bf8734d62c004f57a022087704f' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_google_analytics/hooks/index/scripts.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '982068315d484304847339-87972472',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'sd_ga' => 0,
    'product' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4843048b5404_54354559',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4843048b5404_54354559')) {function content_5d4843048b5404_54354559($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo smarty_function_script(array('src'=>"js/addons/sd_google_analytics/sd_google_analytics.js"),$_smarty_tpl);?>




<?php if ($_smarty_tpl->tpl_vars['sd_ga']->value) {?>
<?php echo '<script'; ?>
 type="text/javascript" class="cm-ajax-force">
(function(_, $) {
    $(document).ready(function(){
        if (typeof(ga) != 'undefined') {
            <?php if ($_smarty_tpl->tpl_vars['sd_ga']->value['deleted']) {?>
                var is_delete_tracker = false;
                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sd_ga']->value['deleted']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                    ga('ec:addProduct', {
                        id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
'
                    });
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tracker']) {?>
                        is_delete_tracker = true;
                        delete_tracker = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
';
                        
                        ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:addProduct', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
'
                        });
                    <?php }?>
                <?php } ?>

                ga('ec:setAction', 'remove');
                ga('send', 'event', 'UX', 'click', 'remove to cart'); // Send data using an event.

                if (is_delete_tracker === true) {
                    ga(delete_tracker + '.ec:setAction', 'remove');
                    ga(delete_tracker + '.send', 'event', 'UX', 'click', 'remove to cart');
                }

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['sd_ga']->value['added']) {?>
                var is_added_tracker = false;

                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sd_ga']->value['added']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                    ga('ec:addProduct', {
                        id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        category: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['category'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        brand: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['brand'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        price: <?php echo strtr($_smarty_tpl->tpl_vars['product']->value['price'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
,
                        quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
',
                    });
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tracker']) {?>
                        is_added_tracker = true;
                        added_tracker = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
';
                        
                        ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:addProduct', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
'
                        });
                    <?php }?>
                <?php } ?>

                ga('ec:setAction', 'add');
                ga('send', 'event', 'UX', 'click', 'add to cart'); // Send data using an event.

                if (is_added_tracker === true) {
                    ga(added_tracker + '.ec:setAction', 'add');
                    ga(added_tracker + '.send', 'event', 'UX', 'click', 'add to cart');
                }
            <?php }?>
        }
   });
}(Tygh, Tygh.$));
<?php echo '</script'; ?>
>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_google_analytics/hooks/index/scripts.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_google_analytics/hooks/index/scripts.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo smarty_function_script(array('src'=>"js/addons/sd_google_analytics/sd_google_analytics.js"),$_smarty_tpl);?>




<?php if ($_smarty_tpl->tpl_vars['sd_ga']->value) {?>
<?php echo '<script'; ?>
 type="text/javascript" class="cm-ajax-force">
(function(_, $) {
    $(document).ready(function(){
        if (typeof(ga) != 'undefined') {
            <?php if ($_smarty_tpl->tpl_vars['sd_ga']->value['deleted']) {?>
                var is_delete_tracker = false;
                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sd_ga']->value['deleted']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                    ga('ec:addProduct', {
                        id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
'
                    });
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tracker']) {?>
                        is_delete_tracker = true;
                        delete_tracker = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
';
                        
                        ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:addProduct', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
'
                        });
                    <?php }?>
                <?php } ?>

                ga('ec:setAction', 'remove');
                ga('send', 'event', 'UX', 'click', 'remove to cart'); // Send data using an event.

                if (is_delete_tracker === true) {
                    ga(delete_tracker + '.ec:setAction', 'remove');
                    ga(delete_tracker + '.send', 'event', 'UX', 'click', 'remove to cart');
                }

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['sd_ga']->value['added']) {?>
                var is_added_tracker = false;

                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sd_ga']->value['added']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                    ga('ec:addProduct', {
                        id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        category: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['category'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        brand: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['brand'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        price: <?php echo strtr($_smarty_tpl->tpl_vars['product']->value['price'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
,
                        quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
',
                    });
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tracker']) {?>
                        is_added_tracker = true;
                        added_tracker = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
';
                        
                        ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:addProduct', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            quantity: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
'
                        });
                    <?php }?>
                <?php } ?>

                ga('ec:setAction', 'add');
                ga('send', 'event', 'UX', 'click', 'add to cart'); // Send data using an event.

                if (is_added_tracker === true) {
                    ga(added_tracker + '.ec:setAction', 'add');
                    ga(added_tracker + '.send', 'event', 'UX', 'click', 'add to cart');
                }
            <?php }?>
        }
   });
}(Tygh, Tygh.$));
<?php echo '</script'; ?>
>
<?php }
}?><?php }} ?>
