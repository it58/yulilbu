<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 07:03:27
         compiled from "/home/yulibu/public_html/design/backend/templates/components/bottom_panel/icons/bp-close.svg" */ ?>
<?php /*%%SmartyHeaderCode:6842034295d48c3cf082186-18299747%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7ddfd8a5a094ec448ef12315fc73abecb79b96ef' => 
    array (
      0 => '/home/yulibu/public_html/design/backend/templates/components/bottom_panel/icons/bp-close.svg',
      1 => 1565015960,
      2 => 'backend',
    ),
  ),
  'nocache_hash' => '6842034295d48c3cf082186-18299747',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48c3cf083537_46807798',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48c3cf083537_46807798')) {function content_5d48c3cf083537_46807798($_smarty_tpl) {?><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
    class="bp-close__icon">
    <path
        d="M16.644 8.966l-3.025 3.024 3.025 3.025a1.152 1.152 0 0 1-1.629 1.629l-3.025-3.025-3.024 3.025a1.148 1.148 0 0 1-1.628 0 1.152 1.152 0 0 1 0-1.63l3.024-3.024-3.025-3.024a1.152 1.152 0 0 1 1.629-1.629l3.024 3.025 3.025-3.025a1.152 1.152 0 0 1 1.629 1.629z" />
</svg><?php }} ?>
