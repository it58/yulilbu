<?php /* Smarty version Smarty-3.1.21, created on 2019-08-09 20:31:15
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/views/wallet_system/wallet_transactions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7051879995d4d75a3b676c8-92051898%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '43e3f35900bbae1615596231635e2a18a0fde3c4' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/views/wallet_system/wallet_transactions.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '7051879995d4d75a3b676c8-92051898',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'config' => 0,
    'search' => 0,
    'wallet_transactions' => 0,
    'transaction' => 0,
    'extra_info' => 0,
    'wallet_user_email' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4d75a3c0df43_82981296',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4d75a3c0df43_82981296')) {function content_5d4d75a3c0df43_82981296($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/yulibu/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('credit_debit_id','transaction_type','reference_id','credit','debit','total_cash','timestamp','credit','bytransfer','return_id','order_id','sender','debit','bytransfer','order_id','reciever','transfer','no_data','wallet_transactions','credit_debit_id','transaction_type','reference_id','credit','debit','total_cash','timestamp','credit','bytransfer','return_id','order_id','sender','debit','bytransfer','order_id','reciever','transfer','no_data','wallet_transactions'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ty-wallet-credit cm-pagination cm-history cm-pagination-button">
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="wallet_transaction_form">
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"exicon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"exicon-dummy\"></i>", null, 0);?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <table class="ty-table ty-wallet-credit__table">
            <thead>    
                <tr>
                    <th style="width: 10%"><a ><?php echo $_smarty_tpl->__("credit_debit_id");?>
</a></th>
                    <th style="width: 15%"><?php echo $_smarty_tpl->__("transaction_type");?>
</th>
                    <th style="width: 15%"><?php echo $_smarty_tpl->__("reference_id");?>
</th>
                    <th style="width: 10%"><?php echo $_smarty_tpl->__("credit");?>
</th>
                    <th style="width: 10%"><?php echo $_smarty_tpl->__("debit");?>
</th>
                    <th style="width: 10%"><?php echo $_smarty_tpl->__("total_cash");?>
</th>
                    <th style="width: 15%"><?php echo $_smarty_tpl->__("timestamp");?>
</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars["transaction"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["transaction"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wallet_transactions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["transaction"]->key => $_smarty_tpl->tpl_vars["transaction"]->value) {
$_smarty_tpl->tpl_vars["transaction"]->_loop = true;
?>
                <?php $_smarty_tpl->tpl_vars['extra_info'] = new Smarty_variable(unserialize($_smarty_tpl->tpl_vars['transaction']->value['extra_info']), null, 0);?>
                <?php $_smarty_tpl->tpl_vars['wallet_user_email'] = new Smarty_variable(fn_wallet_system_get_wallet_user_email_id($_smarty_tpl->tpl_vars['transaction']->value['wallet_id']), null, 0);?>
                <tr>
                <?php if (isset($_smarty_tpl->tpl_vars['transaction']->value['credit_id'])) {?>
                    <td><strong>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['credit_id'], ENT_QUOTES, 'UTF-8');?>
</strong></a><?php echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->tpl_vars['transaction']->value['refund_reason']), 0);?>
</td>

                    <td><b><?php echo $_smarty_tpl->__("credit");?>
</b><?php if (isset($_smarty_tpl->tpl_vars['extra_info']->value['sender_email'])) {?>(<?php echo $_smarty_tpl->__("bytransfer");?>
)<?php } else { ?>(<?php ob_start();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source'], ENT_QUOTES, 'UTF-8');?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->__($_tmp1);?>
)<?php }?></td>
                    <?php if (!empty($_smarty_tpl->tpl_vars['transaction']->value['source_id'])) {?>
                    <td><?php if ($_smarty_tpl->tpl_vars['transaction']->value['source']=='refund_rma') {
echo $_smarty_tpl->__("return_id");?>
<a href="?dispatch=rma.details&return_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
">&nbsp;#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
</a><?php } else {
echo $_smarty_tpl->__("order_id");?>
<a href="?dispatch=orders.details&order_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
">&nbsp;#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
</a><?php }?></td>
                    <?php } else { ?>
                    <td><?php if (isset($_smarty_tpl->tpl_vars['extra_info']->value['sender_email'])) {?>
                        <?php echo $_smarty_tpl->__("sender");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_info']->value['sender_email'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['wallet_user_email']->value, ENT_QUOTES, 'UTF-8');?>

                        <?php }?></td>
                    <?php }?>
                    <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['credit_amount']), 0);?>
</td>
                    <td>-</td>
                    <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['total_amount']), 0);?>
</td>
                    <td><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['transaction']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</td>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['transaction']->value['debit_id'])) {?> 
                
                    <td class="row-status"><strong>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['debit_id'], ENT_QUOTES, 'UTF-8');?>
</strong></a><?php if (!empty($_smarty_tpl->tpl_vars['transaction']->value['debit_reason'])) {
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->tpl_vars['transaction']->value['debit_reason']), 0);
}?></td>
                 
                    <td class="row-status"><b><?php echo $_smarty_tpl->__("debit");?>
</b><?php if (!empty($_smarty_tpl->tpl_vars['extra_info']->value['reciever_email'])) {?>&nbsp;(<?php echo $_smarty_tpl->__("bytransfer");?>
)<?php } else {
if (!empty($_smarty_tpl->tpl_vars['transaction']->value['source'])) {?>(<?php ob_start();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source'], ENT_QUOTES, 'UTF-8');?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->__($_tmp2);?>
)<?php }
}?></td>
                    <?php if (!empty($_smarty_tpl->tpl_vars['transaction']->value['order_id'])) {?>
                    <td class="row-status"><?php echo $_smarty_tpl->__("order_id");?>
<a href="?dispatch=orders.details&order_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
">&nbsp;#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
</a></td>
                    <?php } else { ?>
                    <td class="row-status"><?php if (!empty($_smarty_tpl->tpl_vars['extra_info']->value['reciever_email'])) {
echo $_smarty_tpl->__("reciever");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_info']->value['reciever_email'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("transfer");?>
)<?php } else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['wallet_user_email']->value, ENT_QUOTES, 'UTF-8');
}?></td>
                    <?php }?>
                    <td class="row-status">-</td> 
                    <td class="row-status"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['debit_amount']), 0);?>
</td> 
                    <td class="row-status"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['remain_amount']), 0);?>
</td>
                    <td class="row-status"> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['transaction']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</td> 
                <?php }?>       
                </tr>
            <?php }
if (!$_smarty_tpl->tpl_vars["transaction"]->_loop) {
?>
                <tr class="ty-table__no-items">
                    <td colspan="7"><p class="ty-no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </form>
 <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("wallet_transactions");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
</div>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wallet_system/views/wallet_system/wallet_transactions.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wallet_system/views/wallet_system/wallet_transactions.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ty-wallet-credit cm-pagination cm-history cm-pagination-button">
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="wallet_transaction_form">
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"exicon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"exicon-dummy\"></i>", null, 0);?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <table class="ty-table ty-wallet-credit__table">
            <thead>    
                <tr>
                    <th style="width: 10%"><a ><?php echo $_smarty_tpl->__("credit_debit_id");?>
</a></th>
                    <th style="width: 15%"><?php echo $_smarty_tpl->__("transaction_type");?>
</th>
                    <th style="width: 15%"><?php echo $_smarty_tpl->__("reference_id");?>
</th>
                    <th style="width: 10%"><?php echo $_smarty_tpl->__("credit");?>
</th>
                    <th style="width: 10%"><?php echo $_smarty_tpl->__("debit");?>
</th>
                    <th style="width: 10%"><?php echo $_smarty_tpl->__("total_cash");?>
</th>
                    <th style="width: 15%"><?php echo $_smarty_tpl->__("timestamp");?>
</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars["transaction"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["transaction"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wallet_transactions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["transaction"]->key => $_smarty_tpl->tpl_vars["transaction"]->value) {
$_smarty_tpl->tpl_vars["transaction"]->_loop = true;
?>
                <?php $_smarty_tpl->tpl_vars['extra_info'] = new Smarty_variable(unserialize($_smarty_tpl->tpl_vars['transaction']->value['extra_info']), null, 0);?>
                <?php $_smarty_tpl->tpl_vars['wallet_user_email'] = new Smarty_variable(fn_wallet_system_get_wallet_user_email_id($_smarty_tpl->tpl_vars['transaction']->value['wallet_id']), null, 0);?>
                <tr>
                <?php if (isset($_smarty_tpl->tpl_vars['transaction']->value['credit_id'])) {?>
                    <td><strong>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['credit_id'], ENT_QUOTES, 'UTF-8');?>
</strong></a><?php echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->tpl_vars['transaction']->value['refund_reason']), 0);?>
</td>

                    <td><b><?php echo $_smarty_tpl->__("credit");?>
</b><?php if (isset($_smarty_tpl->tpl_vars['extra_info']->value['sender_email'])) {?>(<?php echo $_smarty_tpl->__("bytransfer");?>
)<?php } else { ?>(<?php ob_start();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source'], ENT_QUOTES, 'UTF-8');?>
<?php $_tmp3=ob_get_clean();?><?php echo $_smarty_tpl->__($_tmp3);?>
)<?php }?></td>
                    <?php if (!empty($_smarty_tpl->tpl_vars['transaction']->value['source_id'])) {?>
                    <td><?php if ($_smarty_tpl->tpl_vars['transaction']->value['source']=='refund_rma') {
echo $_smarty_tpl->__("return_id");?>
<a href="?dispatch=rma.details&return_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
">&nbsp;#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
</a><?php } else {
echo $_smarty_tpl->__("order_id");?>
<a href="?dispatch=orders.details&order_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
">&nbsp;#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source_id'], ENT_QUOTES, 'UTF-8');?>
</a><?php }?></td>
                    <?php } else { ?>
                    <td><?php if (isset($_smarty_tpl->tpl_vars['extra_info']->value['sender_email'])) {?>
                        <?php echo $_smarty_tpl->__("sender");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_info']->value['sender_email'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['wallet_user_email']->value, ENT_QUOTES, 'UTF-8');?>

                        <?php }?></td>
                    <?php }?>
                    <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['credit_amount']), 0);?>
</td>
                    <td>-</td>
                    <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['total_amount']), 0);?>
</td>
                    <td><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['transaction']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</td>

                <?php } elseif (isset($_smarty_tpl->tpl_vars['transaction']->value['debit_id'])) {?> 
                
                    <td class="row-status"><strong>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['debit_id'], ENT_QUOTES, 'UTF-8');?>
</strong></a><?php if (!empty($_smarty_tpl->tpl_vars['transaction']->value['debit_reason'])) {
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->tpl_vars['transaction']->value['debit_reason']), 0);
}?></td>
                 
                    <td class="row-status"><b><?php echo $_smarty_tpl->__("debit");?>
</b><?php if (!empty($_smarty_tpl->tpl_vars['extra_info']->value['reciever_email'])) {?>&nbsp;(<?php echo $_smarty_tpl->__("bytransfer");?>
)<?php } else {
if (!empty($_smarty_tpl->tpl_vars['transaction']->value['source'])) {?>(<?php ob_start();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['source'], ENT_QUOTES, 'UTF-8');?>
<?php $_tmp4=ob_get_clean();?><?php echo $_smarty_tpl->__($_tmp4);?>
)<?php }
}?></td>
                    <?php if (!empty($_smarty_tpl->tpl_vars['transaction']->value['order_id'])) {?>
                    <td class="row-status"><?php echo $_smarty_tpl->__("order_id");?>
<a href="?dispatch=orders.details&order_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
">&nbsp;#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['transaction']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
</a></td>
                    <?php } else { ?>
                    <td class="row-status"><?php if (!empty($_smarty_tpl->tpl_vars['extra_info']->value['reciever_email'])) {
echo $_smarty_tpl->__("reciever");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_info']->value['reciever_email'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("transfer");?>
)<?php } else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['wallet_user_email']->value, ENT_QUOTES, 'UTF-8');
}?></td>
                    <?php }?>
                    <td class="row-status">-</td> 
                    <td class="row-status"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['debit_amount']), 0);?>
</td> 
                    <td class="row-status"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['transaction']->value['remain_amount']), 0);?>
</td>
                    <td class="row-status"> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['transaction']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</td> 
                <?php }?>       
                </tr>
            <?php }
if (!$_smarty_tpl->tpl_vars["transaction"]->_loop) {
?>
                <tr class="ty-table__no-items">
                    <td colspan="7"><p class="ty-no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </form>
 <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("wallet_transactions");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
</div>


<?php }?><?php }} ?>
