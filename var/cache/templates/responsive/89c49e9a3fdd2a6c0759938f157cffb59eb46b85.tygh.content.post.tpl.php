<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:53:56
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_google_analytics/hooks/index/content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16237557725d4843044b13e9-42266704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '89c49e9a3fdd2a6c0759938f157cffb59eb46b85' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_google_analytics/hooks/index/content.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '16237557725d4843044b13e9-42266704',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'product' => 0,
    'ga_product_info' => 0,
    'ga_info' => 0,
    'vendor_tracking_code_for_product' => 0,
    'ga_product_promotions' => 0,
    'promotion' => 0,
    'ga_banners_info' => 0,
    'ga_banner_info' => 0,
    'vendor_tracking_code' => 0,
    'snapping_id' => 0,
    'banner_key' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d484304577f39_20357087',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d484304577f39_20357087')) {function content_5d484304577f39_20357087($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['runtime']->value['vendor_id']!=0) {?>
    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1($_smarty_tpl->tpl_vars['runtime']->value['vendor_id'],$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars["ga_product_info"] = new Smarty_variable(sd_MTgzOGUwOWQyZDkzZjU2MWY5MjQ2YzAx(''), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value) {?>
<?php echo '<script'; ?>
 type="text/javascript" class="cm-ajax-force">
    (function(_, $) {
        $(document).ready(function() {
            if (typeof(ga) != 'undefined') {

                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addImpression']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addImpression']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_info']->key => $_smarty_tpl->tpl_vars['ga_info']->value) {
$_smarty_tpl->tpl_vars['ga_info']->_loop = true;
?>
                        ga('ec:addImpression', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['product_id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            category: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['category'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            brand: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['brand'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            list: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['list'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            position: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['position'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'
                        });
                    <?php } ?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addProduct']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addProduct']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_info']->key => $_smarty_tpl->tpl_vars['ga_info']->value) {
$_smarty_tpl->tpl_vars['ga_info']->_loop = true;
?>
                        ga('ec:addProduct', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['product_id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            category: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['category'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            brand: '<?php echo strtr(sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky($_smarty_tpl->tpl_vars['ga_info']->value['brand'],$_smarty_tpl->tpl_vars['product']->value), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        });

                        <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")) {?>
                            <?php $_smarty_tpl->tpl_vars["vendor_tracking_code_for_product"] = new Smarty_variable(sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1($_smarty_tpl->tpl_vars['product']->value['company_id'],$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>

                            <?php if ($_smarty_tpl->tpl_vars['vendor_tracking_code_for_product']->value['tracker']) {?>
                                ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code_for_product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:addProduct', {
                                    id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['product_id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['product'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    category: '<?php echo strtr(sd_NDI3MWFiZWU2NmEwNWNlYTU0ODA5YzNj($_smarty_tpl->tpl_vars['product']->value['product_id']), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    brand: '<?php echo strtr(sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['product']->value), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    variant: '<?php echo strtr(sd_YmRkMjMwZTdlZDRkYTc2NjdmNGZlYmUw($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['product']->value['extra']['product_options']), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    price: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['price'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    coupon: '',
                                    quantity: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['amount'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'
                                });
                            <?php }?>
                        <?php }?>
                    <?php } ?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_product_promotions'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_product_promotions']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_product_promotions']->key => $_smarty_tpl->tpl_vars['ga_product_promotions']->value) {
$_smarty_tpl->tpl_vars['ga_product_promotions']->_loop = true;
?>
                        <?php  $_smarty_tpl->tpl_vars['promotion'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['promotion']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_promotions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['promotion']->key => $_smarty_tpl->tpl_vars['promotion']->value) {
$_smarty_tpl->tpl_vars['promotion']->_loop = true;
?>
                            ga('ec:addPromo', {
                                id: '<?php echo strtr($_smarty_tpl->tpl_vars['promotion']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                name: '<?php echo strtr($_smarty_tpl->tpl_vars['promotion']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                creative: '<?php echo strtr($_smarty_tpl->tpl_vars['promotion']->value['creative'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                position: ''
                            });
                        <?php } ?>
                    <?php } ?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_banners_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banners_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banners_info']->key => $_smarty_tpl->tpl_vars['ga_banners_info']->value) {
$_smarty_tpl->tpl_vars['ga_banners_info']->_loop = true;
?>
                        <?php  $_smarty_tpl->tpl_vars['ga_banner_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banner_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_banners_info']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banner_info']->key => $_smarty_tpl->tpl_vars['ga_banner_info']->value) {
$_smarty_tpl->tpl_vars['ga_banner_info']->_loop = true;
?>
                            ga('ec:addPromo', {
                                id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                creative: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['creative'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                position: ''
                            });
                        <?php } ?>
                    <?php } ?>
                <?php }?>
                ga('ec:setAction', 'detail');
                ga('send', 'event', 'product', 'details', {
                    nonInteraction: true
                });

                <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker']) {?>
                    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:setAction', 'detail');
                    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.send', 'event', 'product', 'detail', {
                        nonInteraction: true
                    });
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_banners_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banners_info']->_loop = false;
 $_smarty_tpl->tpl_vars['snapping_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banners_info']->key => $_smarty_tpl->tpl_vars['ga_banners_info']->value) {
$_smarty_tpl->tpl_vars['ga_banners_info']->_loop = true;
 $_smarty_tpl->tpl_vars['snapping_id']->value = $_smarty_tpl->tpl_vars['ga_banners_info']->key;
?>
                        var parent_elm_slider = $('#banner_slider_' + <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['snapping_id']->value, ENT_QUOTES, 'UTF-8');?>
);
                        if (!parent_elm_slider.length) {
                            var parent_elm_original = $('#banner_original_' + <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['snapping_id']->value, ENT_QUOTES, 'UTF-8');?>
);
                        }
                        var bkey_wisiwyg = bkey_image = 0;
                        var elm = '';
                        <?php  $_smarty_tpl->tpl_vars['ga_banner_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banner_info']->_loop = false;
 $_smarty_tpl->tpl_vars['banner_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ga_banners_info']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banner_info']->key => $_smarty_tpl->tpl_vars['ga_banner_info']->value) {
$_smarty_tpl->tpl_vars['ga_banner_info']->_loop = true;
 $_smarty_tpl->tpl_vars['banner_key']->value = $_smarty_tpl->tpl_vars['ga_banner_info']->key;
?>
                            if (parent_elm_slider.length) {
                                elm = parent_elm_slider.find('.ty-banner__image-item')[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner_key']->value, ENT_QUOTES, 'UTF-8');?>
];
                            } else {
                                <?php if ($_smarty_tpl->tpl_vars['ga_banner_info']->value['wysiwyg']=='Y') {?>
                                    elm = parent_elm_original.find('.ty-wysiwyg-content')[bkey_wisiwyg];
                                    bkey_wisiwyg++;
                                <?php } else { ?>
                                    elm = parent_elm_original.find('.ty-banner__image-wrapper')[bkey_image];
                                    bkey_image++;
                                <?php }?>
                            }
                            $(elm).on('click', 'a', function() {
                                ga('ec:addPromo', {
                                    id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    creative: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['creative'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    position: ''
                                });
                                ga('ec:setAction', 'promo_click');
                                ga('send', 'event', 'Internal Promotions', 'click', '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
                            });
                        <?php } ?>
                    <?php } ?>
                <?php }?>
            }
        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_google_analytics/hooks/index/content.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_google_analytics/hooks/index/content.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['runtime']->value['vendor_id']!=0) {?>
    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1($_smarty_tpl->tpl_vars['runtime']->value['vendor_id'],$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars["ga_product_info"] = new Smarty_variable(sd_MTgzOGUwOWQyZDkzZjU2MWY5MjQ2YzAx(''), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value) {?>
<?php echo '<script'; ?>
 type="text/javascript" class="cm-ajax-force">
    (function(_, $) {
        $(document).ready(function() {
            if (typeof(ga) != 'undefined') {

                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addImpression']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addImpression']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_info']->key => $_smarty_tpl->tpl_vars['ga_info']->value) {
$_smarty_tpl->tpl_vars['ga_info']->_loop = true;
?>
                        ga('ec:addImpression', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['product_id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            category: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['category'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            brand: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['brand'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            list: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['list'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            position: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['position'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'
                        });
                    <?php } ?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addProduct']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addProduct']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_info']->key => $_smarty_tpl->tpl_vars['ga_info']->value) {
$_smarty_tpl->tpl_vars['ga_info']->_loop = true;
?>
                        ga('ec:addProduct', {
                            id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['product_id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            category: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['category'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            brand: '<?php echo strtr(sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky($_smarty_tpl->tpl_vars['ga_info']->value['brand'],$_smarty_tpl->tpl_vars['product']->value), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            variant: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_info']->value['variant'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                        });

                        <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")) {?>
                            <?php $_smarty_tpl->tpl_vars["vendor_tracking_code_for_product"] = new Smarty_variable(sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1($_smarty_tpl->tpl_vars['product']->value['company_id'],$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>

                            <?php if ($_smarty_tpl->tpl_vars['vendor_tracking_code_for_product']->value['tracker']) {?>
                                ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code_for_product']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:addProduct', {
                                    id: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['product_id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    name: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['product'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    category: '<?php echo strtr(sd_NDI3MWFiZWU2NmEwNWNlYTU0ODA5YzNj($_smarty_tpl->tpl_vars['product']->value['product_id']), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    brand: '<?php echo strtr(sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['product']->value), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    variant: '<?php echo strtr(sd_YmRkMjMwZTdlZDRkYTc2NjdmNGZlYmUw($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['product']->value['extra']['product_options']), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    price: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['price'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    coupon: '',
                                    quantity: '<?php echo strtr($_smarty_tpl->tpl_vars['product']->value['amount'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'
                                });
                            <?php }?>
                        <?php }?>
                    <?php } ?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_product_promotions'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_product_promotions']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_product_promotions']->key => $_smarty_tpl->tpl_vars['ga_product_promotions']->value) {
$_smarty_tpl->tpl_vars['ga_product_promotions']->_loop = true;
?>
                        <?php  $_smarty_tpl->tpl_vars['promotion'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['promotion']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_promotions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['promotion']->key => $_smarty_tpl->tpl_vars['promotion']->value) {
$_smarty_tpl->tpl_vars['promotion']->_loop = true;
?>
                            ga('ec:addPromo', {
                                id: '<?php echo strtr($_smarty_tpl->tpl_vars['promotion']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                name: '<?php echo strtr($_smarty_tpl->tpl_vars['promotion']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                creative: '<?php echo strtr($_smarty_tpl->tpl_vars['promotion']->value['creative'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                position: ''
                            });
                        <?php } ?>
                    <?php } ?>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_banners_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banners_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banners_info']->key => $_smarty_tpl->tpl_vars['ga_banners_info']->value) {
$_smarty_tpl->tpl_vars['ga_banners_info']->_loop = true;
?>
                        <?php  $_smarty_tpl->tpl_vars['ga_banner_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banner_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ga_banners_info']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banner_info']->key => $_smarty_tpl->tpl_vars['ga_banner_info']->value) {
$_smarty_tpl->tpl_vars['ga_banner_info']->_loop = true;
?>
                            ga('ec:addPromo', {
                                id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                creative: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['creative'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                position: ''
                            });
                        <?php } ?>
                    <?php } ?>
                <?php }?>
                ga('ec:setAction', 'detail');
                ga('send', 'event', 'product', 'details', {
                    nonInteraction: true
                });

                <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker']) {?>
                    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.ec:setAction', 'detail');
                    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.send', 'event', 'product', 'detail', {
                        nonInteraction: true
                    });
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']) {?>
                    <?php  $_smarty_tpl->tpl_vars['ga_banners_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banners_info']->_loop = false;
 $_smarty_tpl->tpl_vars['snapping_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ga_product_info']->value['addPromo_banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banners_info']->key => $_smarty_tpl->tpl_vars['ga_banners_info']->value) {
$_smarty_tpl->tpl_vars['ga_banners_info']->_loop = true;
 $_smarty_tpl->tpl_vars['snapping_id']->value = $_smarty_tpl->tpl_vars['ga_banners_info']->key;
?>
                        var parent_elm_slider = $('#banner_slider_' + <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['snapping_id']->value, ENT_QUOTES, 'UTF-8');?>
);
                        if (!parent_elm_slider.length) {
                            var parent_elm_original = $('#banner_original_' + <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['snapping_id']->value, ENT_QUOTES, 'UTF-8');?>
);
                        }
                        var bkey_wisiwyg = bkey_image = 0;
                        var elm = '';
                        <?php  $_smarty_tpl->tpl_vars['ga_banner_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ga_banner_info']->_loop = false;
 $_smarty_tpl->tpl_vars['banner_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ga_banners_info']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ga_banner_info']->key => $_smarty_tpl->tpl_vars['ga_banner_info']->value) {
$_smarty_tpl->tpl_vars['ga_banner_info']->_loop = true;
 $_smarty_tpl->tpl_vars['banner_key']->value = $_smarty_tpl->tpl_vars['ga_banner_info']->key;
?>
                            if (parent_elm_slider.length) {
                                elm = parent_elm_slider.find('.ty-banner__image-item')[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner_key']->value, ENT_QUOTES, 'UTF-8');?>
];
                            } else {
                                <?php if ($_smarty_tpl->tpl_vars['ga_banner_info']->value['wysiwyg']=='Y') {?>
                                    elm = parent_elm_original.find('.ty-wysiwyg-content')[bkey_wisiwyg];
                                    bkey_wisiwyg++;
                                <?php } else { ?>
                                    elm = parent_elm_original.find('.ty-banner__image-wrapper')[bkey_image];
                                    bkey_image++;
                                <?php }?>
                            }
                            $(elm).on('click', 'a', function() {
                                ga('ec:addPromo', {
                                    id: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['id'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    name: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    creative: '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['creative'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                    position: ''
                                });
                                ga('ec:setAction', 'promo_click');
                                ga('send', 'event', 'Internal Promotions', 'click', '<?php echo strtr($_smarty_tpl->tpl_vars['ga_banner_info']->value['name'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
                            });
                        <?php } ?>
                    <?php } ?>
                <?php }?>
            }
        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }
}?><?php }} ?>
