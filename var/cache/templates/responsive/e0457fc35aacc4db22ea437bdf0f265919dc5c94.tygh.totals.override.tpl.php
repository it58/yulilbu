<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 11:07:52
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/hooks/orders/totals.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9808109395d48fd18e69518-57188168%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e0457fc35aacc4db22ea437bdf0f265919dc5c94' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/hooks/orders/totals.override.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '9808109395d48fd18e69518-57188168',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'order_info' => 0,
    'use_shipments' => 0,
    'shipping_method' => 0,
    'shipping' => 0,
    'shipments' => 0,
    'shipment' => 0,
    'key' => 0,
    'tax_data' => 0,
    'settings' => 0,
    'take_surcharge_from_vendor' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48fd18f03f53_57795483',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48fd18f03f53_57795483')) {function content_5d48fd18f03f53_57795483($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('payment_method','wallet_payment','shipping_method','tracking_number','tracking_number','carrier','subtotal','shipping_cost','including_discount','order_discount','coupon','taxes','included','tax_exempt','payment_surcharge','total','un_paid_amount','payment_method','wallet_payment','shipping_method','tracking_number','tracking_number','carrier','subtotal','shipping_cost','including_discount','order_discount','coupon','taxes','included','tax_exempt','payment_surcharge','total','un_paid_amount'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:totals")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:totals"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_id']) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("payment_method");?>
:</td>
            <td style="width: 57%" data-ct-orders-summary="summary-payment">
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:totals_payment")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:totals_payment"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['payment'], ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description']) {?>(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:totals_payment"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
if (isset($_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash'])&&$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']!=$_smarty_tpl->tpl_vars['order_info']->value['total']) {?>(<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']-$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']), 0);?>
)<?php }?>
            </td>
        </tr>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash'])) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("wallet_payment");?>
:</td>
            <td style="width: 57%" data-ct-orders-summary="summary-payment">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']), 0);?>
        
            </td>
        </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['shipping']) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("shipping_method");?>
:</td>
            <td data-ct-orders-summary="summary-ship">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:totals_shipping")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:totals_shipping"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['use_shipments']->value) {?>
                <ul>
                    <?php  $_smarty_tpl->tpl_vars["shipping_method"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["shipping_method"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["shipping_method"]->key => $_smarty_tpl->tpl_vars["shipping_method"]->value) {
$_smarty_tpl->tpl_vars["shipping_method"]->_loop = true;
?>
                        <li><?php if ($_smarty_tpl->tpl_vars['shipping_method']->value['shipping']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping_method']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?> – <?php }?></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <?php  $_smarty_tpl->tpl_vars["shipping"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["shipping"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["shipping"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["shipping"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["shipping"]->key => $_smarty_tpl->tpl_vars["shipping"]->value) {
$_smarty_tpl->tpl_vars["shipping"]->_loop = true;
 $_smarty_tpl->tpl_vars["shipping"]->iteration++;
 $_smarty_tpl->tpl_vars["shipping"]->last = $_smarty_tpl->tpl_vars["shipping"]->iteration === $_smarty_tpl->tpl_vars["shipping"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["f_shipp"]['last'] = $_smarty_tpl->tpl_vars["shipping"]->last;
?>

                    <?php if ($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier']&&$_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("tracking_number");?>
: <a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier_info']['tracking_url'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number'], ENT_QUOTES, 'UTF-8');?>
</a>)
                        <?php echo $_smarty_tpl->tpl_vars['shipment']->value['carrier_info']['info'];?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("tracking_number");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number'], ENT_QUOTES, 'UTF-8');?>
)
                        <?php echo $_smarty_tpl->tpl_vars['shipment']->value['carrier_info']['info'];?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("carrier");?>
: <?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier_info']['name'];?>
)
                        <?php echo $_smarty_tpl->tpl_vars['shipment']->value['carrier_info']['info'];?>

                    <?php } else { ?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>

                    <?php }?>
                    <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['f_shipp']['last']) {?><br><?php }?>
                <?php } ?>
            <?php }?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:totals_shipping"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </td>
        </tr>
    <?php }?>

    <tr class="ty-orders-summary__row">
        <td><?php echo $_smarty_tpl->__("subtotal");?>
:&nbsp;</td>
        <td data-ct-orders-summary="summary-subtotal"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_subtotal']), 0);?>
</td>
    </tr>
    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost'])) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("shipping_cost");?>
:&nbsp;</td>
            <td data-ct-orders-summary="summary-shipcost"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost']), 0);?>
</td>
        </tr>
    <?php }?>

    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['discount'])) {?>
    <tr class="ty-orders-summary__row">
        <td class="ty-strong"><?php echo $_smarty_tpl->__("including_discount");?>
:</td>
        <td class="ty-nowrap" data-ct-orders-summary="summary-discount">
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['discount']), 0);?>

        </td>
    </tr>
    <?php }?>

    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount'])) {?>
        <tr class="ty-orders-summary__row">
            <td class="ty-strong"><?php echo $_smarty_tpl->__("order_discount");?>
:</td>
            <td class="ty-nowrap" data-ct-orders-summary="summary-sub-discount">
                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount']), 0);?>

            </td>
        </tr>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['coupons']) {?>
        <?php  $_smarty_tpl->tpl_vars["coupon"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["coupon"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['coupons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["coupon"]->key => $_smarty_tpl->tpl_vars["coupon"]->value) {
$_smarty_tpl->tpl_vars["coupon"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["coupon"]->key;
?>
            <tr class="ty-orders-summary__row">
                <td class="ty-nowrap"><?php echo $_smarty_tpl->__("coupon");?>
:</td>
                <td data-ct-orders-summary="summary-coupons"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
</td>
            </tr>
        <?php } ?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']) {?>
        <tr class="taxes">
            <td><strong><?php echo $_smarty_tpl->__("taxes");?>
:</strong></td>
            <td>&nbsp;</td>
        </tr>
        <?php  $_smarty_tpl->tpl_vars['tax_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax_data']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['taxes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax_data']->key => $_smarty_tpl->tpl_vars['tax_data']->value) {
$_smarty_tpl->tpl_vars['tax_data']->_loop = true;
?>
            <tr class="ty-orders-summary__row">
                <td class="ty-orders-summary__taxes-description">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['description'], ENT_QUOTES, 'UTF-8');?>

                    <?php echo $_smarty_tpl->getSubTemplate ("common/modifier.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('mod_value'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_value'],'mod_type'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_type']), 0);?>

                    <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['price_includes_tax']=="Y"&&($_smarty_tpl->tpl_vars['settings']->value['Appearance']['cart_prices_w_taxes']!="Y"||$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']=="subtotal")) {?>
                        <?php echo $_smarty_tpl->__("included");?>

                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['regnumber']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['regnumber'], ENT_QUOTES, 'UTF-8');?>
)
                    <?php }?>
                </td>
                <td class="ty-orders-summary__taxes-description" data-ct-orders-summary="summary-tax-sub"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['tax_data']->value['tax_subtotal']), 0);?>
</td>
            </tr>
        <?php } ?>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['tax_exempt']=="Y") {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("tax_exempt");?>
</td>
            <td>&nbsp;</td>
        <tr>
    <?php }?>

    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge'])&&!$_smarty_tpl->tpl_vars['take_surcharge_from_vendor']->value) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['order_info']->value['payment_method']['surcharge_title'])===null||$tmp==='' ? $_smarty_tpl->__("payment_surcharge") : $tmp), ENT_QUOTES, 'UTF-8');?>
:&nbsp;</td>
            <td data-ct-orders-summary="summary-surchange"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge']), 0);?>
</td>
        </tr>
    <?php }?>
    <tr class="ty-orders-summary__row">
        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
:&nbsp;</td>
        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']), 0);?>
</td>
    </tr>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_id']==6&&$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']>0) {?>
    <tr class="ty-orders-summary__row">
        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("un_paid_amount");?>
:&nbsp;</td>
        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']-$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']), 0);?>
</td>
    </tr>
    <?php }?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:totals"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wallet_system/hooks/orders/totals.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wallet_system/hooks/orders/totals.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:totals")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:totals"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_id']) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("payment_method");?>
:</td>
            <td style="width: 57%" data-ct-orders-summary="summary-payment">
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:totals_payment")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:totals_payment"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['payment'], ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description']) {?>(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:totals_payment"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
if (isset($_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash'])&&$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']!=$_smarty_tpl->tpl_vars['order_info']->value['total']) {?>(<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']-$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']), 0);?>
)<?php }?>
            </td>
        </tr>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash'])) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("wallet_payment");?>
:</td>
            <td style="width: 57%" data-ct-orders-summary="summary-payment">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']), 0);?>
        
            </td>
        </tr>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['shipping']) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("shipping_method");?>
:</td>
            <td data-ct-orders-summary="summary-ship">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"orders:totals_shipping")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"orders:totals_shipping"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['use_shipments']->value) {?>
                <ul>
                    <?php  $_smarty_tpl->tpl_vars["shipping_method"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["shipping_method"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["shipping_method"]->key => $_smarty_tpl->tpl_vars["shipping_method"]->value) {
$_smarty_tpl->tpl_vars["shipping_method"]->_loop = true;
?>
                        <li><?php if ($_smarty_tpl->tpl_vars['shipping_method']->value['shipping']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping_method']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
 <?php } else { ?> – <?php }?></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <?php  $_smarty_tpl->tpl_vars["shipping"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["shipping"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['shipping']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["shipping"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["shipping"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["shipping"]->key => $_smarty_tpl->tpl_vars["shipping"]->value) {
$_smarty_tpl->tpl_vars["shipping"]->_loop = true;
 $_smarty_tpl->tpl_vars["shipping"]->iteration++;
 $_smarty_tpl->tpl_vars["shipping"]->last = $_smarty_tpl->tpl_vars["shipping"]->iteration === $_smarty_tpl->tpl_vars["shipping"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["f_shipp"]['last'] = $_smarty_tpl->tpl_vars["shipping"]->last;
?>

                    <?php if ($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier']&&$_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("tracking_number");?>
: <a target="_blank" href="<?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier_info']['tracking_url'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number'], ENT_QUOTES, 'UTF-8');?>
</a>)
                        <?php echo $_smarty_tpl->tpl_vars['shipment']->value['carrier_info']['info'];?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("tracking_number");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['tracking_number'], ENT_QUOTES, 'UTF-8');?>
)
                        <?php echo $_smarty_tpl->tpl_vars['shipment']->value['carrier_info']['info'];?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo $_smarty_tpl->__("carrier");?>
: <?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->tpl_vars['shipping']->value['group_key']]['carrier_info']['name'];?>
)
                        <?php echo $_smarty_tpl->tpl_vars['shipment']->value['carrier_info']['info'];?>

                    <?php } else { ?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['shipping'], ENT_QUOTES, 'UTF-8');?>

                    <?php }?>
                    <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['f_shipp']['last']) {?><br><?php }?>
                <?php } ?>
            <?php }?>
            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:totals_shipping"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            </td>
        </tr>
    <?php }?>

    <tr class="ty-orders-summary__row">
        <td><?php echo $_smarty_tpl->__("subtotal");?>
:&nbsp;</td>
        <td data-ct-orders-summary="summary-subtotal"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_subtotal']), 0);?>
</td>
    </tr>
    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost'])) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("shipping_cost");?>
:&nbsp;</td>
            <td data-ct-orders-summary="summary-shipcost"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost']), 0);?>
</td>
        </tr>
    <?php }?>

    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['discount'])) {?>
    <tr class="ty-orders-summary__row">
        <td class="ty-strong"><?php echo $_smarty_tpl->__("including_discount");?>
:</td>
        <td class="ty-nowrap" data-ct-orders-summary="summary-discount">
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['discount']), 0);?>

        </td>
    </tr>
    <?php }?>

    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount'])) {?>
        <tr class="ty-orders-summary__row">
            <td class="ty-strong"><?php echo $_smarty_tpl->__("order_discount");?>
:</td>
            <td class="ty-nowrap" data-ct-orders-summary="summary-sub-discount">
                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount']), 0);?>

            </td>
        </tr>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['coupons']) {?>
        <?php  $_smarty_tpl->tpl_vars["coupon"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["coupon"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['coupons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["coupon"]->key => $_smarty_tpl->tpl_vars["coupon"]->value) {
$_smarty_tpl->tpl_vars["coupon"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["coupon"]->key;
?>
            <tr class="ty-orders-summary__row">
                <td class="ty-nowrap"><?php echo $_smarty_tpl->__("coupon");?>
:</td>
                <td data-ct-orders-summary="summary-coupons"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
</td>
            </tr>
        <?php } ?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']) {?>
        <tr class="taxes">
            <td><strong><?php echo $_smarty_tpl->__("taxes");?>
:</strong></td>
            <td>&nbsp;</td>
        </tr>
        <?php  $_smarty_tpl->tpl_vars['tax_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax_data']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['taxes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax_data']->key => $_smarty_tpl->tpl_vars['tax_data']->value) {
$_smarty_tpl->tpl_vars['tax_data']->_loop = true;
?>
            <tr class="ty-orders-summary__row">
                <td class="ty-orders-summary__taxes-description">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['description'], ENT_QUOTES, 'UTF-8');?>

                    <?php echo $_smarty_tpl->getSubTemplate ("common/modifier.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('mod_value'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_value'],'mod_type'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_type']), 0);?>

                    <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['price_includes_tax']=="Y"&&($_smarty_tpl->tpl_vars['settings']->value['Appearance']['cart_prices_w_taxes']!="Y"||$_smarty_tpl->tpl_vars['settings']->value['General']['tax_calculation']=="subtotal")) {?>
                        <?php echo $_smarty_tpl->__("included");?>

                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['regnumber']) {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['regnumber'], ENT_QUOTES, 'UTF-8');?>
)
                    <?php }?>
                </td>
                <td class="ty-orders-summary__taxes-description" data-ct-orders-summary="summary-tax-sub"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['tax_data']->value['tax_subtotal']), 0);?>
</td>
            </tr>
        <?php } ?>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['tax_exempt']=="Y") {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo $_smarty_tpl->__("tax_exempt");?>
</td>
            <td>&nbsp;</td>
        <tr>
    <?php }?>

    <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge'])&&!$_smarty_tpl->tpl_vars['take_surcharge_from_vendor']->value) {?>
        <tr class="ty-orders-summary__row">
            <td><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['order_info']->value['payment_method']['surcharge_title'])===null||$tmp==='' ? $_smarty_tpl->__("payment_surcharge") : $tmp), ENT_QUOTES, 'UTF-8');?>
:&nbsp;</td>
            <td data-ct-orders-summary="summary-surchange"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge']), 0);?>
</td>
        </tr>
    <?php }?>
    <tr class="ty-orders-summary__row">
        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
:&nbsp;</td>
        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']), 0);?>
</td>
    </tr>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_id']==6&&$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']>0) {?>
    <tr class="ty-orders-summary__row">
        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("un_paid_amount");?>
:&nbsp;</td>
        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']-$_smarty_tpl->tpl_vars['order_info']->value['wallet']['used_cash']), 0);?>
</td>
    </tr>
    <?php }?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"orders:totals"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
}?><?php }} ?>
