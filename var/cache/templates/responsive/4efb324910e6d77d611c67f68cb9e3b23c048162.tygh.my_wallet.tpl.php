<?php /* Smarty version Smarty-3.1.21, created on 2019-08-07 14:40:16
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/views/wallet_system/my_wallet.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5008395325d4a8060cc7e81-29513126%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4efb324910e6d77d611c67f68cb9e3b23c048162' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/views/wallet_system/my_wallet.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5008395325d4a8060cc7e81-29513126',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'total_cash' => 0,
    'current_cash_to_add' => 0,
    'enable_transfer' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4a8060d191e4_62779671',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4a8060d191e4_62779671')) {function content_5d4a8060d191e4_62779671($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('total_amount','enter_amount','add_money','wallet_transactions','wallet_transfer','my_wallet','total_amount','enter_amount','add_money','wallet_transactions','wallet_transfer','my_wallet'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><style type="text/css">
	.ty-wallet-my_wallet{
		margin-left: 35%;
		border: 1px solid grey;
		display: table;
		padding: 30px;
	}
</style>
<div class="ty-wallet-my_wallet">

<center><span><img src="images/wallet/icon.png" width="23" /></span>&nbsp;&nbsp;
<strong style="font-size:20px;"><?php echo $_smarty_tpl->__("total_amount");?>
&nbsp;&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_cash']->value), 0);?>
</strong></center>
<br>
	<form action="<?php echo htmlspecialchars(fn_url("wallet_system.cash_add_wallet"), ENT_QUOTES, 'UTF-8');?>
" method="post" name="my_wallet_add_cash">
		<br><br>
	    <label class="cm-required ty-control-group__title" for="this_wallet_recharge_amount"><?php echo $_smarty_tpl->__("enter_amount");?>
</label>
	    <input type="text" name="wallet_system[recharge_amount]" id="this_wallet_recharge_amount" class="ty-input-text" value="<?php if ($_smarty_tpl->tpl_vars['current_cash_to_add']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['current_cash_to_add']->value, ENT_QUOTES, 'UTF-8');
}?>" style="width:100%">
	    <input type="text" name="wallet_system[total_cash]" id="wallet_total_cash" class="ty-input-text hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['total_cash']->value, ENT_QUOTES, 'UTF-8');?>
" style="width:100%">
	    <br><br>
	    <div style="text-align:center;">
	        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("add_money"),'but_name'=>"wallet_submit"), 0);?>

	    </div>
	</form><br>
<center><strong></strong><a href="?dispatch=wallet_system.wallet_transactions"><?php echo $_smarty_tpl->__("wallet_transactions");?>
</a></strong></center>
<?php if (isset($_smarty_tpl->tpl_vars['enable_transfer']->value)) {?>
<center><strong><a href="?dispatch=wallet_system.wallet_transfer_user_to_user"><?php echo $_smarty_tpl->__("wallet_transfer");?>
</a></strong></center>
<?php }?></div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("my_wallet");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wallet_system/views/wallet_system/my_wallet.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wallet_system/views/wallet_system/my_wallet.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><style type="text/css">
	.ty-wallet-my_wallet{
		margin-left: 35%;
		border: 1px solid grey;
		display: table;
		padding: 30px;
	}
</style>
<div class="ty-wallet-my_wallet">

<center><span><img src="images/wallet/icon.png" width="23" /></span>&nbsp;&nbsp;
<strong style="font-size:20px;"><?php echo $_smarty_tpl->__("total_amount");?>
&nbsp;&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['total_cash']->value), 0);?>
</strong></center>
<br>
	<form action="<?php echo htmlspecialchars(fn_url("wallet_system.cash_add_wallet"), ENT_QUOTES, 'UTF-8');?>
" method="post" name="my_wallet_add_cash">
		<br><br>
	    <label class="cm-required ty-control-group__title" for="this_wallet_recharge_amount"><?php echo $_smarty_tpl->__("enter_amount");?>
</label>
	    <input type="text" name="wallet_system[recharge_amount]" id="this_wallet_recharge_amount" class="ty-input-text" value="<?php if ($_smarty_tpl->tpl_vars['current_cash_to_add']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['current_cash_to_add']->value, ENT_QUOTES, 'UTF-8');
}?>" style="width:100%">
	    <input type="text" name="wallet_system[total_cash]" id="wallet_total_cash" class="ty-input-text hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['total_cash']->value, ENT_QUOTES, 'UTF-8');?>
" style="width:100%">
	    <br><br>
	    <div style="text-align:center;">
	        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("add_money"),'but_name'=>"wallet_submit"), 0);?>

	    </div>
	</form><br>
<center><strong></strong><a href="?dispatch=wallet_system.wallet_transactions"><?php echo $_smarty_tpl->__("wallet_transactions");?>
</a></strong></center>
<?php if (isset($_smarty_tpl->tpl_vars['enable_transfer']->value)) {?>
<center><strong><a href="?dispatch=wallet_system.wallet_transfer_user_to_user"><?php echo $_smarty_tpl->__("wallet_transfer");?>
</a></strong></center>
<?php }?></div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("my_wallet");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
