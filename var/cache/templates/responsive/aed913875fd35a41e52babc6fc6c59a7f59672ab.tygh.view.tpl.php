<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 11:08:13
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_messaging_system/views/messenger/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2218496775d48fd2d746df1-16664264%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aed913875fd35a41e52babc6fc6c59a7f59672ab' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_messaging_system/views/messenger/view.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2218496775d48fd2d746df1-16664264',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'ticket' => 0,
    'messages' => 0,
    'message' => 0,
    'last_message_id' => 0,
    'message_class' => 0,
    'self_images_dir' => 0,
    'auth' => 0,
    'hide_send_message' => 0,
    'attachment_message' => 0,
    'addons' => 0,
    'vendor_image_data' => 0,
    'customer_image_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48fd2d7c9a14_93697716',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48fd2d7c9a14_93697716')) {function content_5d48fd2d7c9a14_93697716($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('no_messages_found_with_recipient','no_messages_found_with_recipient'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars['last_message_id'] = new Smarty_variable(0, null, 0);?>

<div class="ticket-messages-block" id="ticket_messages_block">
    <div class="ticket-messages-list" data-ticket-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" id="ticket_messages_list">
        <?php if ($_smarty_tpl->tpl_vars['messages']->value) {?>
            <?php  $_smarty_tpl->tpl_vars['message'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['message']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['message']->key => $_smarty_tpl->tpl_vars['message']->value) {
$_smarty_tpl->tpl_vars['message']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['message']->value['message_id']>$_smarty_tpl->tpl_vars['last_message_id']->value) {?>
                    <?php $_smarty_tpl->tpl_vars['last_message_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['message']->value['message_id'], null, 0);?>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['message']->value['author_type']!="V") {?>
                    <?php $_smarty_tpl->tpl_vars['message_class'] = new Smarty_variable("author-message has-avatar", null, 0);?>
                <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars['message_class'] = new Smarty_variable("recipient-message", null, 0);?>
                <?php }?>

                <div class="ty-sd_messaging_system-message__content <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message_class']->value, ENT_QUOTES, 'UTF-8');?>
 ty-mb-l">
                    <div class="ty-sd_messaging_system-message__author">
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_messaging_system/views/messenger/components/user_image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('message'=>$_smarty_tpl->tpl_vars['message']->value,'ticket'=>$_smarty_tpl->tpl_vars['ticket']->value), 0);?>

                    </div>

                    <div class="ty-sd_messaging_system-all">
                        <div class="ty-sd_messaging_system-name">
                            <?php if ($_smarty_tpl->tpl_vars['message']->value['author_type']=="V"&&$_smarty_tpl->tpl_vars['ticket']->value['vendor_name']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['vendor_name'], ENT_QUOTES, 'UTF-8');?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['message']->value['author_type']!="V"&&$_smarty_tpl->tpl_vars['ticket']->value['customer_name']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['customer_name'], ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <span class="ty-sd_messaging_system-message__date" data-message-timestamp="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['timestamp'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['date'], ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>

                        <div class="ty-sd_messaging_system-message" id="message_text">
                            <div class="ty-sd_messaging_system-message__message">
                                <?php echo preg_replace('!\s+!u', ' ',nl2br($_smarty_tpl->tpl_vars['message']->value['message']));?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <p class="ty-no-items"><?php echo $_smarty_tpl->__("no_messages_found_with_recipient",array("[recipient_name]"=>$_smarty_tpl->tpl_vars['ticket']->value['vendor_name']));?>
</p>
        <?php }?>
    </div>

</div>

<div class="ty-center hidden" id="message_spinner"><?php echo $_smarty_tpl->__('addons.sd_messaging_system.waiting_for_connect');?>
 <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['self_images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/images/spinner.gif" width="20" height="20"/></div>
<div class="ty-discussion-post__buttons buttons-container">
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" id="new_message_form" class="ty-discussion-post__form">
        <input type="hidden" name="ticket_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" name="author_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['auth']->value['user_id'], ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" name="last_message_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['last_message_id']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <?php if (!$_smarty_tpl->tpl_vars['hide_send_message']->value) {?>
            <textarea id="new_message_field" name="message_body" rows="4" cols="50" placeholder="<?php echo $_smarty_tpl->__('write_your_message_here');?>
" class="ty-sd_messaging_system-textarea" disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['attachment_message']->value;?>
</textarea>
            <a href="<?php echo htmlspecialchars(fn_url("messenger.tickets_list"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn__send-left ty-btn ty-btn__secondary"><?php echo $_smarty_tpl->__('go_back');?>
</a>
            <button id="send_message_but" class="ty-btn__send-right ty-btn__primary ty-btn ty-float-right ty-btn" type="submit" name="dispatch[messenger.send_message]" onclick="return false;" disabled="disabled"><?php echo $_smarty_tpl->__('send');?>
</button>
        <?php } else { ?>
            <div class="ty-center"><?php echo $_smarty_tpl->__('vendor_cannot_receive_messages');?>
</div>
        <?php }?>
    </form>
</div>

<?php $_smarty_tpl->tpl_vars['vendor_image_data'] = new Smarty_variable(fn_image_to_display($_smarty_tpl->tpl_vars['ticket']->value['vendor_image'],@constant('USER_IMAGE_WIDTH'),@constant('USER_IMAGE_HEIGHT')), null, 0);?>
<?php $_smarty_tpl->tpl_vars['customer_image_data'] = new Smarty_variable(fn_image_to_display($_smarty_tpl->tpl_vars['ticket']->value['customer_image'],@constant('USER_IMAGE_WIDTH'),@constant('USER_IMAGE_HEIGHT')), null, 0);?>

<input type="hidden" name="websocket_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['sd_messaging_system']['websocket_url'], ENT_QUOTES, 'UTF-8');?>
">

<div id="sd-messenger-recipient-data" class="hidden"
    data-name="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['ticket']->value['vendor_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['company_id'], ENT_QUOTES, 'UTF-8');?>

    data-type="V"
    data-has-image="<?php if ($_smarty_tpl->tpl_vars['vendor_image_data']->value) {?>Y<?php } else { ?>N<?php }?>"
    data-image-path="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['vendor_image_data']->value['image_path'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-image-alt="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['vendor_image_data']->value['alt'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
">
</div>

<div id="sd-messenger-sender-data" class="hidden"
    data-name="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['ticket']->value['customer_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['customer_id'], ENT_QUOTES, 'UTF-8');?>

    data-type="C"
    data-has-image="<?php if ($_smarty_tpl->tpl_vars['customer_image_data']->value) {?>Y<?php } else { ?>N<?php }?>"
    data-image-path="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['customer_image_data']->value['image_path'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-image-alt="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['customer_image_data']->value['alt'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
">
</div>

<?php echo smarty_function_script(array('src'=>"js/addons/sd_messaging_system/web_socket.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/addons/sd_messaging_system/messenger.js"),$_smarty_tpl);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_messaging_system/views/messenger/view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_messaging_system/views/messenger/view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars['last_message_id'] = new Smarty_variable(0, null, 0);?>

<div class="ticket-messages-block" id="ticket_messages_block">
    <div class="ticket-messages-list" data-ticket-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" id="ticket_messages_list">
        <?php if ($_smarty_tpl->tpl_vars['messages']->value) {?>
            <?php  $_smarty_tpl->tpl_vars['message'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['message']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['message']->key => $_smarty_tpl->tpl_vars['message']->value) {
$_smarty_tpl->tpl_vars['message']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['message']->value['message_id']>$_smarty_tpl->tpl_vars['last_message_id']->value) {?>
                    <?php $_smarty_tpl->tpl_vars['last_message_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['message']->value['message_id'], null, 0);?>
                <?php }?>

                <?php if ($_smarty_tpl->tpl_vars['message']->value['author_type']!="V") {?>
                    <?php $_smarty_tpl->tpl_vars['message_class'] = new Smarty_variable("author-message has-avatar", null, 0);?>
                <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars['message_class'] = new Smarty_variable("recipient-message", null, 0);?>
                <?php }?>

                <div class="ty-sd_messaging_system-message__content <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message_class']->value, ENT_QUOTES, 'UTF-8');?>
 ty-mb-l">
                    <div class="ty-sd_messaging_system-message__author">
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_messaging_system/views/messenger/components/user_image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('message'=>$_smarty_tpl->tpl_vars['message']->value,'ticket'=>$_smarty_tpl->tpl_vars['ticket']->value), 0);?>

                    </div>

                    <div class="ty-sd_messaging_system-all">
                        <div class="ty-sd_messaging_system-name">
                            <?php if ($_smarty_tpl->tpl_vars['message']->value['author_type']=="V"&&$_smarty_tpl->tpl_vars['ticket']->value['vendor_name']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['vendor_name'], ENT_QUOTES, 'UTF-8');?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['message']->value['author_type']!="V"&&$_smarty_tpl->tpl_vars['ticket']->value['customer_name']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['customer_name'], ENT_QUOTES, 'UTF-8');?>

                            <?php }?>
                            <span class="ty-sd_messaging_system-message__date" data-message-timestamp="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['timestamp'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['date'], ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>

                        <div class="ty-sd_messaging_system-message" id="message_text">
                            <div class="ty-sd_messaging_system-message__message">
                                <?php echo preg_replace('!\s+!u', ' ',nl2br($_smarty_tpl->tpl_vars['message']->value['message']));?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <p class="ty-no-items"><?php echo $_smarty_tpl->__("no_messages_found_with_recipient",array("[recipient_name]"=>$_smarty_tpl->tpl_vars['ticket']->value['vendor_name']));?>
</p>
        <?php }?>
    </div>

</div>

<div class="ty-center hidden" id="message_spinner"><?php echo $_smarty_tpl->__('addons.sd_messaging_system.waiting_for_connect');?>
 <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['self_images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/images/spinner.gif" width="20" height="20"/></div>
<div class="ty-discussion-post__buttons buttons-container">
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" id="new_message_form" class="ty-discussion-post__form">
        <input type="hidden" name="ticket_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" name="author_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['auth']->value['user_id'], ENT_QUOTES, 'UTF-8');?>
"/>
        <input type="hidden" name="last_message_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['last_message_id']->value, ENT_QUOTES, 'UTF-8');?>
"/>
        <?php if (!$_smarty_tpl->tpl_vars['hide_send_message']->value) {?>
            <textarea id="new_message_field" name="message_body" rows="4" cols="50" placeholder="<?php echo $_smarty_tpl->__('write_your_message_here');?>
" class="ty-sd_messaging_system-textarea" disabled="disabled"><?php echo $_smarty_tpl->tpl_vars['attachment_message']->value;?>
</textarea>
            <a href="<?php echo htmlspecialchars(fn_url("messenger.tickets_list"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn__send-left ty-btn ty-btn__secondary"><?php echo $_smarty_tpl->__('go_back');?>
</a>
            <button id="send_message_but" class="ty-btn__send-right ty-btn__primary ty-btn ty-float-right ty-btn" type="submit" name="dispatch[messenger.send_message]" onclick="return false;" disabled="disabled"><?php echo $_smarty_tpl->__('send');?>
</button>
        <?php } else { ?>
            <div class="ty-center"><?php echo $_smarty_tpl->__('vendor_cannot_receive_messages');?>
</div>
        <?php }?>
    </form>
</div>

<?php $_smarty_tpl->tpl_vars['vendor_image_data'] = new Smarty_variable(fn_image_to_display($_smarty_tpl->tpl_vars['ticket']->value['vendor_image'],@constant('USER_IMAGE_WIDTH'),@constant('USER_IMAGE_HEIGHT')), null, 0);?>
<?php $_smarty_tpl->tpl_vars['customer_image_data'] = new Smarty_variable(fn_image_to_display($_smarty_tpl->tpl_vars['ticket']->value['customer_image'],@constant('USER_IMAGE_WIDTH'),@constant('USER_IMAGE_HEIGHT')), null, 0);?>

<input type="hidden" name="websocket_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['sd_messaging_system']['websocket_url'], ENT_QUOTES, 'UTF-8');?>
">

<div id="sd-messenger-recipient-data" class="hidden"
    data-name="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['ticket']->value['vendor_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['company_id'], ENT_QUOTES, 'UTF-8');?>

    data-type="V"
    data-has-image="<?php if ($_smarty_tpl->tpl_vars['vendor_image_data']->value) {?>Y<?php } else { ?>N<?php }?>"
    data-image-path="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['vendor_image_data']->value['image_path'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-image-alt="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['vendor_image_data']->value['alt'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
">
</div>

<div id="sd-messenger-sender-data" class="hidden"
    data-name="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['ticket']->value['customer_name'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['customer_id'], ENT_QUOTES, 'UTF-8');?>

    data-type="C"
    data-has-image="<?php if ($_smarty_tpl->tpl_vars['customer_image_data']->value) {?>Y<?php } else { ?>N<?php }?>"
    data-image-path="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['customer_image_data']->value['image_path'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
    data-image-alt="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['customer_image_data']->value['alt'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
">
</div>

<?php echo smarty_function_script(array('src'=>"js/addons/sd_messaging_system/web_socket.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/addons/sd_messaging_system/messenger.js"),$_smarty_tpl);?>

<?php }?><?php }} ?>
