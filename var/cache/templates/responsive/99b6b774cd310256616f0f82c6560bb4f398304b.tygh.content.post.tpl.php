<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:53:56
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_facebook_pixel/hooks/index/content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17024452225d484304584ff9-33562531%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99b6b774cd310256616f0f82c6560bb4f398304b' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_facebook_pixel/hooks/index/content.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '17024452225d484304584ff9-33562531',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'cart_products' => 0,
    'auth' => 0,
    'user_data' => 0,
    'identifiers_facebook_pixel' => 0,
    'item' => 0,
    'product_data' => 0,
    'secondary_currency' => 0,
    'currencies' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4843045e1217_95230269',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4843045e1217_95230269')) {function content_5d4843045e1217_95230269($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_math')) include '/home/yulibu/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['complete_registration_facebook_pixel']=='Y'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>
    <?php echo smarty_function_script(array('src'=>"js/addons/sd_facebook_pixel/func.js"),$_smarty_tpl);?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['initiate_checkout_facebook_pixel']=='Y'&&$_smarty_tpl->tpl_vars['cart_products']->value&&$_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='checkout'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>

    <?php echo '<script'; ?>
 type="text/javascript">
        var pixel_id = '<?php echo htmlspecialchars($_REQUEST['pixel_id'], ENT_QUOTES, 'UTF-8');?>
',
            el,
            Initcount = 0,
            user_id = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['auth']->value['user_id'], ENT_QUOTES, 'UTF-8');?>
',
            user_data = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
';

        var Initiate = function() {
            if (!pixel_id) {
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['identifiers_facebook_pixel']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <?php if ($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel']&&$_smarty_tpl->tpl_vars['item']->value['company_id']) {?>
                        el = document.createElement("iframe");
                        document.body.appendChild(el);
                        el.id = 'iframe';
                        el.style.width = "1px";
                        el.style.height = "1px";
                        el.style.position = 'absolute';
                        el.style.left = '-9999px';
                        el.src = fn_url('checkout.checkout?pixel_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel'], ENT_QUOTES, 'UTF-8');?>
&companyid=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['company_id'], ENT_QUOTES, 'UTF-8');?>
');
                    <?php }?>
                <?php } ?>
            }
            el = $('#checkout_steps').get(0);
            if (typeof el != 'undefined') {
                <?php  $_smarty_tpl->tpl_vars["product_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product_data"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product_data"]->key => $_smarty_tpl->tpl_vars["product_data"]->value) {
$_smarty_tpl->tpl_vars["product_data"]->_loop = true;
?>
                    <?php if (!$_REQUEST['pixel_id']||$_smarty_tpl->tpl_vars['product_data']->value['company_id']==$_REQUEST['companyid']) {?>
                        fbq('track', 'InitiateCheckout', {
                            value: <?php echo smarty_function_math(array('equation'=>((string)$_smarty_tpl->tpl_vars['product_data']->value['price'])." / ".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['secondary_currency']->value]['coefficient']),'format'=>"%.".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['secondary_currency']->value]['decimals'])."f"),$_smarty_tpl);?>
,
                            currency: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['secondary_currency']->value, ENT_QUOTES, 'UTF-8');?>
',
                            content_name: '<?php echo strtr($_smarty_tpl->tpl_vars['product_data']->value['product'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            content_category: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['main_category'], ENT_QUOTES, 'UTF-8');?>
'
                        });
                    <?php }?>
                <?php } ?>
            }
        }
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['track_initiate_checkout']=="only_after_auth") {?>
            $.ceEvent('on', 'ce.formajaxpost_step_two_billing_address', function(response_data,params, response_text) {
                if (response_data !== undefined  && Initcount == 0) {
                    Initiate();
                    Initcount++;
                }
            });

            $.ceEvent('on', 'ce.formajaxpost_step_one_register_form', function(response_data) {
                if (response_data !== undefined && Initcount == 0) {
                    var register_at_checkout = response_data["notifications"];
                    for(key in register_at_checkout) {
                        if (register_at_checkout[key]["type"] == "N") {
                            Initiate();
                            Initcount++;
                        }
                    }
                }
            });

            if (Initcount == 0 && (user_id != 0 || user_data)) {
                Initiate();
                Initcount++;
            }
        <?php } elseif ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['track_initiate_checkout']=="all_stages") {?>
            Initiate();
        <?php }?>
    <?php echo '</script'; ?>
>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_facebook_pixel/hooks/index/content.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_facebook_pixel/hooks/index/content.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['complete_registration_facebook_pixel']=='Y'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>
    <?php echo smarty_function_script(array('src'=>"js/addons/sd_facebook_pixel/func.js"),$_smarty_tpl);?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['initiate_checkout_facebook_pixel']=='Y'&&$_smarty_tpl->tpl_vars['cart_products']->value&&$_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='checkout'&&!empty($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['identifier_facebook_pixel'])) {?>

    <?php echo '<script'; ?>
 type="text/javascript">
        var pixel_id = '<?php echo htmlspecialchars($_REQUEST['pixel_id'], ENT_QUOTES, 'UTF-8');?>
',
            el,
            Initcount = 0,
            user_id = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['auth']->value['user_id'], ENT_QUOTES, 'UTF-8');?>
',
            user_data = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
';

        var Initiate = function() {
            if (!pixel_id) {
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['identifiers_facebook_pixel']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                    <?php if ($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel']&&$_smarty_tpl->tpl_vars['item']->value['company_id']) {?>
                        el = document.createElement("iframe");
                        document.body.appendChild(el);
                        el.id = 'iframe';
                        el.style.width = "1px";
                        el.style.height = "1px";
                        el.style.position = 'absolute';
                        el.style.left = '-9999px';
                        el.src = fn_url('checkout.checkout?pixel_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['identifier_facebook_pixel'], ENT_QUOTES, 'UTF-8');?>
&companyid=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['company_id'], ENT_QUOTES, 'UTF-8');?>
');
                    <?php }?>
                <?php } ?>
            }
            el = $('#checkout_steps').get(0);
            if (typeof el != 'undefined') {
                <?php  $_smarty_tpl->tpl_vars["product_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product_data"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product_data"]->key => $_smarty_tpl->tpl_vars["product_data"]->value) {
$_smarty_tpl->tpl_vars["product_data"]->_loop = true;
?>
                    <?php if (!$_REQUEST['pixel_id']||$_smarty_tpl->tpl_vars['product_data']->value['company_id']==$_REQUEST['companyid']) {?>
                        fbq('track', 'InitiateCheckout', {
                            value: <?php echo smarty_function_math(array('equation'=>((string)$_smarty_tpl->tpl_vars['product_data']->value['price'])." / ".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['secondary_currency']->value]['coefficient']),'format'=>"%.".((string)$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['secondary_currency']->value]['decimals'])."f"),$_smarty_tpl);?>
,
                            currency: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['secondary_currency']->value, ENT_QUOTES, 'UTF-8');?>
',
                            content_name: '<?php echo strtr($_smarty_tpl->tpl_vars['product_data']->value['product'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                            content_category: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['main_category'], ENT_QUOTES, 'UTF-8');?>
'
                        });
                    <?php }?>
                <?php } ?>
            }
        }
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['track_initiate_checkout']=="only_after_auth") {?>
            $.ceEvent('on', 'ce.formajaxpost_step_two_billing_address', function(response_data,params, response_text) {
                if (response_data !== undefined  && Initcount == 0) {
                    Initiate();
                    Initcount++;
                }
            });

            $.ceEvent('on', 'ce.formajaxpost_step_one_register_form', function(response_data) {
                if (response_data !== undefined && Initcount == 0) {
                    var register_at_checkout = response_data["notifications"];
                    for(key in register_at_checkout) {
                        if (register_at_checkout[key]["type"] == "N") {
                            Initiate();
                            Initcount++;
                        }
                    }
                }
            });

            if (Initcount == 0 && (user_id != 0 || user_data)) {
                Initiate();
                Initcount++;
            }
        <?php } elseif ($_smarty_tpl->tpl_vars['addons']->value['sd_facebook_pixel']['track_initiate_checkout']=="all_stages") {?>
            Initiate();
        <?php }?>
    <?php echo '</script'; ?>
>
<?php }?>
<?php }?><?php }} ?>
