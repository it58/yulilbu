<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 07:03:27
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/hooks/index/cart_status.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12420354605d48c3cf573017-94281782%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '21e42e91a664b48effd1f31e2f3225d528bb9a76' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/hooks/index/cart_status.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12420354605d48c3cf573017-94281782',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'block' => 0,
    'wallet' => 0,
    'gift_key' => 0,
    'config' => 0,
    'force_items_deletion' => 0,
    'p' => 0,
    'r_url' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48c3cf59a503_00946685',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48c3cf59a503_00946685')) {function content_5d48c3cf59a503_00946685($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('wallet_recharge','wallet_recharge'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_SESSION['cart']['wallet_system']) {?>
    <?php  $_smarty_tpl->tpl_vars["wallet"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["wallet"]->_loop = false;
 $_smarty_tpl->tpl_vars["gift_key"] = new Smarty_Variable;
 $_from = $_SESSION['cart']['wallet_system']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["wallet"]->key => $_smarty_tpl->tpl_vars["wallet"]->value) {
$_smarty_tpl->tpl_vars["wallet"]->_loop = true;
 $_smarty_tpl->tpl_vars["gift_key"]->value = $_smarty_tpl->tpl_vars["wallet"]->key;
?>
    <li class="ty-cart-items__list-item">
        <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['products_links_type']=="thumb") {?>
        <div class="ty-cart-items__list-item-image">
            <img src="images/wallet/icon.png" width="40" height="40">
        </div>
        <?php }?>
        <div class="ty-cart-items__list-item-desc">
            
                <span><?php echo $_smarty_tpl->__("wallet_recharge");?>
</span>
            
        <p>
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['wallet']->value['recharge_amount'],'span_id'=>"subtotal_gc_".((string)$_smarty_tpl->tpl_vars['gift_key']->value),'class'=>"none"), 0);?>

        </p>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['display_delete_icons']=="Y") {?>
        <?php $_smarty_tpl->tpl_vars["r_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
        <div class="ty-cart-items__list-item-tools cm-cart-item-delete">
            <?php if ((!$_smarty_tpl->tpl_vars['runtime']->value['checkout']||$_smarty_tpl->tpl_vars['force_items_deletion']->value)&&!$_smarty_tpl->tpl_vars['p']->value['extra']['exclude_from_calculate']) {
echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>"wallet_system.clear_cart?redirect_url=".((string)$_smarty_tpl->tpl_vars['r_url']->value),'but_meta'=>"cm-ajax cm-post cm-ajax-full-render",'but_target_id'=>"cart_status*",'but_role'=>"delete",'but_name'=>"delete_cart_item"), 0);
}?>
        </div>
        <?php }?>
    </li>
    <?php } ?>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wallet_system/hooks/index/cart_status.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wallet_system/hooks/index/cart_status.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_SESSION['cart']['wallet_system']) {?>
    <?php  $_smarty_tpl->tpl_vars["wallet"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["wallet"]->_loop = false;
 $_smarty_tpl->tpl_vars["gift_key"] = new Smarty_Variable;
 $_from = $_SESSION['cart']['wallet_system']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["wallet"]->key => $_smarty_tpl->tpl_vars["wallet"]->value) {
$_smarty_tpl->tpl_vars["wallet"]->_loop = true;
 $_smarty_tpl->tpl_vars["gift_key"]->value = $_smarty_tpl->tpl_vars["wallet"]->key;
?>
    <li class="ty-cart-items__list-item">
        <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['products_links_type']=="thumb") {?>
        <div class="ty-cart-items__list-item-image">
            <img src="images/wallet/icon.png" width="40" height="40">
        </div>
        <?php }?>
        <div class="ty-cart-items__list-item-desc">
            
                <span><?php echo $_smarty_tpl->__("wallet_recharge");?>
</span>
            
        <p>
            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['wallet']->value['recharge_amount'],'span_id'=>"subtotal_gc_".((string)$_smarty_tpl->tpl_vars['gift_key']->value),'class'=>"none"), 0);?>

        </p>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['display_delete_icons']=="Y") {?>
        <?php $_smarty_tpl->tpl_vars["r_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
        <div class="ty-cart-items__list-item-tools cm-cart-item-delete">
            <?php if ((!$_smarty_tpl->tpl_vars['runtime']->value['checkout']||$_smarty_tpl->tpl_vars['force_items_deletion']->value)&&!$_smarty_tpl->tpl_vars['p']->value['extra']['exclude_from_calculate']) {
echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>"wallet_system.clear_cart?redirect_url=".((string)$_smarty_tpl->tpl_vars['r_url']->value),'but_meta'=>"cm-ajax cm-post cm-ajax-full-render",'but_target_id'=>"cart_status*",'but_role'=>"delete",'but_name'=>"delete_cart_item"), 0);
}?>
        </div>
        <?php }?>
    </li>
    <?php } ?>
<?php }?>
<?php }?><?php }} ?>
