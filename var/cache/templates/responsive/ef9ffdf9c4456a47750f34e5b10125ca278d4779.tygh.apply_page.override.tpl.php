<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 04:07:42
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/wk_vendor_custom_registration/hooks/vendors/apply_page.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18390653715d489a9ec05d43-28697471%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ef9ffdf9c4456a47750f34e5b10125ca278d4779' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/wk_vendor_custom_registration/hooks/vendors/apply_page.override.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '18390653715d489a9ec05d43-28697471',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'wk_vendor_custom_fields' => 0,
    'sec_section_text' => 0,
    'profile_fields' => 0,
    'ship_to_another' => 0,
    'all_sections' => 0,
    'section_data' => 0,
    'section_id' => 0,
    'extra_fields' => 0,
    'settings' => 0,
    'suffix' => 0,
    'iframe_mode' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d489a9ec62135_05706896',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d489a9ec62135_05706896')) {function content_5d489a9ec62135_05706896($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('apply_for_vendor_account','contact_information','address','vendor_terms_n_conditions_name','vendor_terms_n_conditions','terms_and_conditions_content','vendor_terms_n_conditions_alert','submit','apply_for_vendor_account','contact_information','address','vendor_terms_n_conditions_name','vendor_terms_n_conditions','terms_and_conditions_content','vendor_terms_n_conditions_alert','submit'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ty-company-fields">
    <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <h1 class="ty-mainbox-title"><?php echo $_smarty_tpl->__("apply_for_vendor_account");?>
</h1>

    <div id="apply_for_vendor_account" >
        <form action="<?php echo htmlspecialchars(fn_url("wk_vendor_custom_fields.apply_for_vendor"), ENT_QUOTES, 'UTF-8');?>
" method="post" name="apply_for_vendor_form">

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"O",'title'=>''), 0);?>


                    <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"C",'title'=>$_smarty_tpl->__("contact_information")), 0);?>


                    <?php if ($_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value['S']) {?>
                       <?php $_smarty_tpl->tpl_vars["sec_section_text"] = new Smarty_variable($_smarty_tpl->__("address"), null, 0);?>
                        
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>'S','body_id'=>"sa",'ship_to_another'=>$_smarty_tpl->tpl_vars['ship_to_another']->value,'title'=>$_smarty_tpl->tpl_vars['sec_section_text']->value,'address_flag'=>fn_compare_shipping_billing($_smarty_tpl->tpl_vars['profile_fields']->value),'wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value), 0);?>

                    <?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['all_sections']->value)) {?>
                    <?php  $_smarty_tpl->tpl_vars["section_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["section_data"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['all_sections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["section_data"]->key => $_smarty_tpl->tpl_vars["section_data"]->value) {
$_smarty_tpl->tpl_vars["section_data"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["section_data"]->key;
?>
                        <?php $_smarty_tpl->tpl_vars["section_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['section_data']->value['section_id'], null, 0);?> 
                        
                        <?php if (!empty($_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value[$_smarty_tpl->tpl_vars['section_id']->value])) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>$_smarty_tpl->tpl_vars['section_data']->value['section_id'],'title'=>$_smarty_tpl->tpl_vars['section_data']->value['description'],'wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value,'extra_fields'=>$_smarty_tpl->tpl_vars['extra_fields']->value), 0);?>

                       <?php }?>
                    <?php } ?>
                <?php }?>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"apply_for_vendor_account",'align'=>"left"), 0);?>

            
            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Vendors']['need_agree_with_terms_n_conditions']=="Y") {?>
                <div class="ty-control-group ty-company__terms">
                    <div class="cm-field-container">
                        <label for="id_accept_terms<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-check-agreement"><input type="checkbox" id="id_accept_terms<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" name="accept_terms" value="Y" class="cm-agreement checkbox" <?php if ($_smarty_tpl->tpl_vars['iframe_mode']->value) {?>onclick="fn_check_agreements('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?> /><?php $_smarty_tpl->_capture_stack[0][] = array("terms_link", null, null); ob_start(); ?><a id="sw_terms_and_conditions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-combination ty-dashed-link"><?php echo $_smarty_tpl->__("vendor_terms_n_conditions_name");?>
</a><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
echo $_smarty_tpl->__("vendor_terms_n_conditions",array("[terms_href]"=>Smarty::$_smarty_vars['capture']['terms_link']));?>
</label>

                        <div class="hidden" id="terms_and_conditions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <?php echo $_smarty_tpl->__("terms_and_conditions_content");?>

                        </div>
                    </div>
                    <?php echo '<script'; ?>
 type="text/javascript">
                        (function(_, $) {
                            $.ceFormValidator('registerValidator', {
                                class_name: 'cm-check-agreement',
                                message: '<?php echo strtr($_smarty_tpl->__("vendor_terms_n_conditions_alert"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                func: function(id) {
                                    return $('#' + id).prop('checked');
                                }
                            });     
                        }(Tygh, Tygh.$));
                    <?php echo '</script'; ?>
>
                </div>
            <?php }?>
            <div class="buttons-container">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("submit"),'but_name'=>"dispatch[wk_vendor_custom_fields.apply_for_vendor]",'but_id'=>"but_apply_for_vendor",'but_meta'=>"ty-btn__primary"), 0);?>

            </div>
        </form>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wk_vendor_custom_registration/hooks/vendors/apply_page.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wk_vendor_custom_registration/hooks/vendors/apply_page.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ty-company-fields">
    <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <h1 class="ty-mainbox-title"><?php echo $_smarty_tpl->__("apply_for_vendor_account");?>
</h1>

    <div id="apply_for_vendor_account" >
        <form action="<?php echo htmlspecialchars(fn_url("wk_vendor_custom_fields.apply_for_vendor"), ENT_QUOTES, 'UTF-8');?>
" method="post" name="apply_for_vendor_form">

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"O",'title'=>''), 0);?>


                    <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"C",'title'=>$_smarty_tpl->__("contact_information")), 0);?>


                    <?php if ($_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value['S']) {?>
                       <?php $_smarty_tpl->tpl_vars["sec_section_text"] = new Smarty_variable($_smarty_tpl->__("address"), null, 0);?>
                        
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>'S','body_id'=>"sa",'ship_to_another'=>$_smarty_tpl->tpl_vars['ship_to_another']->value,'title'=>$_smarty_tpl->tpl_vars['sec_section_text']->value,'address_flag'=>fn_compare_shipping_billing($_smarty_tpl->tpl_vars['profile_fields']->value),'wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value), 0);?>

                    <?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['all_sections']->value)) {?>
                    <?php  $_smarty_tpl->tpl_vars["section_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["section_data"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['all_sections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["section_data"]->key => $_smarty_tpl->tpl_vars["section_data"]->value) {
$_smarty_tpl->tpl_vars["section_data"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["section_data"]->key;
?>
                        <?php $_smarty_tpl->tpl_vars["section_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['section_data']->value['section_id'], null, 0);?> 
                        
                        <?php if (!empty($_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value[$_smarty_tpl->tpl_vars['section_id']->value])) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>$_smarty_tpl->tpl_vars['section_data']->value['section_id'],'title'=>$_smarty_tpl->tpl_vars['section_data']->value['description'],'wk_vendor_custom_fields'=>$_smarty_tpl->tpl_vars['wk_vendor_custom_fields']->value,'extra_fields'=>$_smarty_tpl->tpl_vars['extra_fields']->value), 0);?>

                       <?php }?>
                    <?php } ?>
                <?php }?>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/wk_vendor_custom_registration/common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"apply_for_vendor_account",'align'=>"left"), 0);?>

            
            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Vendors']['need_agree_with_terms_n_conditions']=="Y") {?>
                <div class="ty-control-group ty-company__terms">
                    <div class="cm-field-container">
                        <label for="id_accept_terms<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-check-agreement"><input type="checkbox" id="id_accept_terms<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" name="accept_terms" value="Y" class="cm-agreement checkbox" <?php if ($_smarty_tpl->tpl_vars['iframe_mode']->value) {?>onclick="fn_check_agreements('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
');"<?php }?> /><?php $_smarty_tpl->_capture_stack[0][] = array("terms_link", null, null); ob_start(); ?><a id="sw_terms_and_conditions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-combination ty-dashed-link"><?php echo $_smarty_tpl->__("vendor_terms_n_conditions_name");?>
</a><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
echo $_smarty_tpl->__("vendor_terms_n_conditions",array("[terms_href]"=>Smarty::$_smarty_vars['capture']['terms_link']));?>
</label>

                        <div class="hidden" id="terms_and_conditions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <?php echo $_smarty_tpl->__("terms_and_conditions_content");?>

                        </div>
                    </div>
                    <?php echo '<script'; ?>
 type="text/javascript">
                        (function(_, $) {
                            $.ceFormValidator('registerValidator', {
                                class_name: 'cm-check-agreement',
                                message: '<?php echo strtr($_smarty_tpl->__("vendor_terms_n_conditions_alert"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
',
                                func: function(id) {
                                    return $('#' + id).prop('checked');
                                }
                            });     
                        }(Tygh, Tygh.$));
                    <?php echo '</script'; ?>
>
                </div>
            <?php }?>
            <div class="buttons-container">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("submit"),'but_name'=>"dispatch[wk_vendor_custom_fields.apply_for_vendor]",'but_id'=>"but_apply_for_vendor",'but_meta'=>"ty-btn__primary"), 0);?>

            </div>
        </form>
    </div>
</div><?php }?><?php }} ?>
