<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:54:23
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/rees46/hooks/products/add_to_cart.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9513589955d48431f892980-98214466%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3866f3c9356b9d73523d1d7d26da8c6dabcb3819' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/rees46/hooks/products/add_to_cart.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '9513589955d48431f892980-98214466',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'rees46' => 0,
    'product' => 0,
    'rees46_type' => 0,
    'cat' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48431f90ff83_76972593',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48431f90ff83_76972593')) {function content_5d48431f90ff83_76972593($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['rees46']->value&&$_smarty_tpl->tpl_vars['rees46']->value['shop_id']!='') {?>
<?php echo '<script'; ?>
 type="text/javascript">
    <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
        cart_params = {
            <?php if ($_smarty_tpl->tpl_vars['rees46_type']->value) {?>
            recommended_by: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46_type']->value, ENT_QUOTES, 'UTF-8');?>
',
            <?php }?>
            id: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
,
            <?php if ($_smarty_tpl->tpl_vars['product']->value['price']) {?>
            price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
,
            <?php } else { ?>
            price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['base_price'], ENT_QUOTES, 'UTF-8');?>
,
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value['amount']>0) {?>
            stock: true,
            <?php } else { ?>
            stock: false,
            <?php }?>
            categories: [<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_smarty_tpl->tpl_vars['cat_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['category_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['cat']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['cat']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
 $_smarty_tpl->tpl_vars['cat_id']->value = $_smarty_tpl->tpl_vars['cat']->key;
 $_smarty_tpl->tpl_vars['cat']->iteration++;
 $_smarty_tpl->tpl_vars['cat']->last = $_smarty_tpl->tpl_vars['cat']->iteration === $_smarty_tpl->tpl_vars['cat']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['cats']['last'] = $_smarty_tpl->tpl_vars['cat']->last;
?>'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value, ENT_QUOTES, 'UTF-8');?>
'<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['cats']['last']) {?>,<?php }
} ?>],
            name: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
',
            url: '<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
',
            image: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
'
        }

        if ($("#button_cart_ajax<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
").length>0) {
            $("#button_cart_ajax<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
").on ("click", function(){
                r46('track', 'cart', cart_params);
            });
        } else {
            $("<?php if ($_smarty_tpl->tpl_vars['rees46_type']->value) {?>#rees46_recommend_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46_type']->value, ENT_QUOTES, 'UTF-8');
}?> #button_cart_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
").on ("click", function(){
                r46('track', 'cart', cart_params);
            });
        }
    <?php }?>
<?php echo '</script'; ?>
>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/rees46/hooks/products/add_to_cart.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/rees46/hooks/products/add_to_cart.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['rees46']->value&&$_smarty_tpl->tpl_vars['rees46']->value['shop_id']!='') {?>
<?php echo '<script'; ?>
 type="text/javascript">
    <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
        cart_params = {
            <?php if ($_smarty_tpl->tpl_vars['rees46_type']->value) {?>
            recommended_by: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46_type']->value, ENT_QUOTES, 'UTF-8');?>
',
            <?php }?>
            id: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
,
            <?php if ($_smarty_tpl->tpl_vars['product']->value['price']) {?>
            price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
,
            <?php } else { ?>
            price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['base_price'], ENT_QUOTES, 'UTF-8');?>
,
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value['amount']>0) {?>
            stock: true,
            <?php } else { ?>
            stock: false,
            <?php }?>
            categories: [<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_smarty_tpl->tpl_vars['cat_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['category_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['cat']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['cat']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
 $_smarty_tpl->tpl_vars['cat_id']->value = $_smarty_tpl->tpl_vars['cat']->key;
 $_smarty_tpl->tpl_vars['cat']->iteration++;
 $_smarty_tpl->tpl_vars['cat']->last = $_smarty_tpl->tpl_vars['cat']->iteration === $_smarty_tpl->tpl_vars['cat']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['cats']['last'] = $_smarty_tpl->tpl_vars['cat']->last;
?>'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value, ENT_QUOTES, 'UTF-8');?>
'<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['cats']['last']) {?>,<?php }
} ?>],
            name: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
',
            url: '<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
',
            image: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
'
        }

        if ($("#button_cart_ajax<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
").length>0) {
            $("#button_cart_ajax<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
").on ("click", function(){
                r46('track', 'cart', cart_params);
            });
        } else {
            $("<?php if ($_smarty_tpl->tpl_vars['rees46_type']->value) {?>#rees46_recommend_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46_type']->value, ENT_QUOTES, 'UTF-8');
}?> #button_cart_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
").on ("click", function(){
                r46('track', 'cart', cart_params);
            });
        }
    <?php }?>
<?php echo '</script'; ?>
>
<?php }?>
<?php }?><?php }} ?>
