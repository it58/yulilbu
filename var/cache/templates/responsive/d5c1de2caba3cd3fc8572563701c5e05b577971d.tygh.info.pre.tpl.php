<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 11:07:52
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_shipping_by_product/hooks/orders/info.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12823275425d48fd18d72938-44911508%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd5c1de2caba3cd3fc8572563701c5e05b577971d' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_shipping_by_product/hooks/orders/info.pre.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12823275425d48fd18d72938-44911508',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'order_info' => 0,
    'product' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48fd18d90f94_11471304',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48fd18d90f94_11471304')) {function content_5d48fd18d90f94_11471304($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('product','shipping_method','shipping_cost','delivery_time','product','shipping_method','shipping_cost','delivery_time'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['order_info']->value['products']) {?>
    <table class="ty-orders-detail__table ty-table ty-mb-s">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->__("product");?>
</th>
                <th><?php echo $_smarty_tpl->__("shipping_method");?>
</th>
                <th><?php echo $_smarty_tpl->__("shipping_cost");?>
</th>
                <th><?php echo $_smarty_tpl->__("delivery_time");?>
</th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
            <tr>
                <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
</td>
                <td><?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['shipping_id']) {
echo htmlspecialchars(fn_get_shipping_name($_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['shipping_id']), ENT_QUOTES, 'UTF-8');
} else { ?>-<?php }?></td>
                <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['shipping_cost']), 0);?>
</td>
                <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['delivery_time'], ENT_QUOTES, 'UTF-8');?>
</td>
            </tr>
        <?php } ?>
    </table>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_shipping_by_product/hooks/orders/info.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_shipping_by_product/hooks/orders/info.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['order_info']->value['products']) {?>
    <table class="ty-orders-detail__table ty-table ty-mb-s">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->__("product");?>
</th>
                <th><?php echo $_smarty_tpl->__("shipping_method");?>
</th>
                <th><?php echo $_smarty_tpl->__("shipping_cost");?>
</th>
                <th><?php echo $_smarty_tpl->__("delivery_time");?>
</th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
            <tr>
                <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
</td>
                <td><?php if ($_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['shipping_id']) {
echo htmlspecialchars(fn_get_shipping_name($_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['shipping_id']), ENT_QUOTES, 'UTF-8');
} else { ?>-<?php }?></td>
                <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['shipping_cost']), 0);?>
</td>
                <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['extra']['shipping_info']['delivery_time'], ENT_QUOTES, 'UTF-8');?>
</td>
            </tr>
        <?php } ?>
    </table>
<?php }
}?><?php }} ?>
