<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 11:07:52
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_buyer_reviews/hooks/orders/product_info.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4207425505d48fd18af4541-04134528%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6074b574bd959acb3c83b271ba0ea3803a772ccd' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_buyer_reviews/hooks/orders/product_info.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4207425505d48fd18af4541-04134528',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'product' => 0,
    'config' => 0,
    'rating' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48fd18b12e60_17341905',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48fd18b12e60_17341905')) {function content_5d48fd18b12e60_17341905($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('write_review','write_review','write_review','write_review'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['product']->value['extra']['discussion']['type']&&$_smarty_tpl->tpl_vars['product']->value['extra']['discussion']['type']!='D'&&!$_smarty_tpl->tpl_vars['product']->value['extra']['discussion']['reviews_amount']) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/discussion/views/discussion/components/new_post.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('new_post_title'=>$_smarty_tpl->__("write_review"),'obj_id'=>$_smarty_tpl->tpl_vars['product']->value['product_id'],'discussion'=>$_smarty_tpl->tpl_vars['product']->value['extra']['discussion'],'post_redirect_url'=>$_smarty_tpl->tpl_vars['config']->value['current_url']), 0);?>

    <div class="ty-discussion__rating-wrapper ty-mt-s" id="average_rating_product_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
">
        <?php $_smarty_tpl->tpl_vars['rating'] = new Smarty_variable("rating_".((string)$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['rating']->value];?>

        <a class="ty-discussion__review-write cm-dialog-opener cm-dialog-auto-size" data-ca-target-id="new_post_dialog_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
" rel="nofollow"><?php echo $_smarty_tpl->__("write_review");?>
</a>
    <!--average_rating_product_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_buyer_reviews/hooks/orders/product_info.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_buyer_reviews/hooks/orders/product_info.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['product']->value['extra']['discussion']['type']&&$_smarty_tpl->tpl_vars['product']->value['extra']['discussion']['type']!='D'&&!$_smarty_tpl->tpl_vars['product']->value['extra']['discussion']['reviews_amount']) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/discussion/views/discussion/components/new_post.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('new_post_title'=>$_smarty_tpl->__("write_review"),'obj_id'=>$_smarty_tpl->tpl_vars['product']->value['product_id'],'discussion'=>$_smarty_tpl->tpl_vars['product']->value['extra']['discussion'],'post_redirect_url'=>$_smarty_tpl->tpl_vars['config']->value['current_url']), 0);?>

    <div class="ty-discussion__rating-wrapper ty-mt-s" id="average_rating_product_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
">
        <?php $_smarty_tpl->tpl_vars['rating'] = new Smarty_variable("rating_".((string)$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['rating']->value];?>

        <a class="ty-discussion__review-write cm-dialog-opener cm-dialog-auto-size" data-ca-target-id="new_post_dialog_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
" rel="nofollow"><?php echo $_smarty_tpl->__("write_review");?>
</a>
    <!--average_rating_product_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
<?php }
}?><?php }} ?>
