<?php /* Smarty version Smarty-3.1.21, created on 2019-08-08 07:28:33
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_messaging_system/views/messenger/tickets_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:507940375d4b6cb1424086-77983047%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a3aadb3898df3053ef6cb722555e94c1615e7a50' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_messaging_system/views/messenger/tickets_list.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '507940375d4b6cb1424086-77983047',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'tickets' => 0,
    'ticket' => 0,
    'addons' => 0,
    'message_length' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4b6cb1483763_88316574',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4b6cb1483763_88316574')) {function content_5d4b6cb1483763_88316574($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/yulibu/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/yulibu/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('new','text_no_tickets','messages','new','text_no_tickets','messages'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><ul class="ty-sd_messaging__list ty-sd_messaging__list-linked ty-sd_messaging__list-bordered ty-sd_messaging__message">
<?php  $_smarty_tpl->tpl_vars['ticket'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ticket']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tickets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ticket']->key => $_smarty_tpl->tpl_vars['ticket']->value) {
$_smarty_tpl->tpl_vars['ticket']->_loop = true;
?>
    <li class="ty-sd_messaging">
        <a href="<?php echo htmlspecialchars(fn_url(('messenger.view&ticket_id=').($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'])), ENT_QUOTES, 'UTF-8');?>
" id="ticket_id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" class="ty-sd_messaging__wrapper ty-sd_messaging__ticket <?php if ($_smarty_tpl->tpl_vars['ticket']->value['last_message']&&$_smarty_tpl->tpl_vars['ticket']->value['status']==@constant('TICKET_STATUS_NEW')) {?>ty-sd_messaging__ticket-new<?php }?>">
            <div class="ty-sd_messaging_system-recipient-user__image">
                <?php if ($_smarty_tpl->tpl_vars['ticket']->value['vendor_image']) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('images'=>$_smarty_tpl->tpl_vars['ticket']->value['vendor_image'],'image_height'=>@constant('USER_IMAGE_HEIGHT'),'image_width'=>@constant('USER_IMAGE_HEIGHT'),'image_type'=>"user_img"), 0);?>

                <?php }?>
            </div>

            <div class="ty-sd_messaging__body">
                <h3 class="ty-sd_messaging_system-recipient-user__name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['last_message_name'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['ticket']->value['vendor_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['vendor_name'], ENT_QUOTES, 'UTF-8');
}?>
                    <span class="ty-sd_messaging_system-last-message__date dotted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['last_message_date'], ENT_QUOTES, 'UTF-8');?>
</span>
                    <?php if ($_smarty_tpl->tpl_vars['ticket']->value['last_message']&&$_smarty_tpl->tpl_vars['ticket']->value['status']==@constant('TICKET_STATUS_NEW')) {?>
                        <span class="ty-sd_messaging_system-last-message__date new-message dotted"><?php echo $_smarty_tpl->__("new");
if ($_smarty_tpl->tpl_vars['ticket']->value['new_messages_amount']) {?>&nbsp;(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['new_messages_amount'], ENT_QUOTES, 'UTF-8');?>
)<?php }?></span>
                    <?php }?>
                </h3>
                <p class="ty-sd_messaging_system-last-message__message">
                    <?php echo smarty_function_math(array('equation'=>"x+y",'x'=>$_smarty_tpl->tpl_vars['addons']->value['sd_messaging_system']['trimmed_messages_length'],'y'=>@constant('TRIPLE_DOT_LENGHT'),'assign'=>"message_length"),$_smarty_tpl);?>

                    <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['ticket']->value['last_message']),$_smarty_tpl->tpl_vars['message_length']->value);?>

                </p>
            </div>
        </a>
    </li>
<?php }
if (!$_smarty_tpl->tpl_vars['ticket']->_loop) {
?>
    <p class="ty-no-items"><?php echo $_smarty_tpl->__("text_no_tickets");?>
</p>
<?php } ?>
</ul>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("messages");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_messaging_system/views/messenger/tickets_list.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_messaging_system/views/messenger/tickets_list.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><ul class="ty-sd_messaging__list ty-sd_messaging__list-linked ty-sd_messaging__list-bordered ty-sd_messaging__message">
<?php  $_smarty_tpl->tpl_vars['ticket'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ticket']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tickets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ticket']->key => $_smarty_tpl->tpl_vars['ticket']->value) {
$_smarty_tpl->tpl_vars['ticket']->_loop = true;
?>
    <li class="ty-sd_messaging">
        <a href="<?php echo htmlspecialchars(fn_url(('messenger.view&ticket_id=').($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'])), ENT_QUOTES, 'UTF-8');?>
" id="ticket_id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['ticket_id'], ENT_QUOTES, 'UTF-8');?>
" class="ty-sd_messaging__wrapper ty-sd_messaging__ticket <?php if ($_smarty_tpl->tpl_vars['ticket']->value['last_message']&&$_smarty_tpl->tpl_vars['ticket']->value['status']==@constant('TICKET_STATUS_NEW')) {?>ty-sd_messaging__ticket-new<?php }?>">
            <div class="ty-sd_messaging_system-recipient-user__image">
                <?php if ($_smarty_tpl->tpl_vars['ticket']->value['vendor_image']) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('images'=>$_smarty_tpl->tpl_vars['ticket']->value['vendor_image'],'image_height'=>@constant('USER_IMAGE_HEIGHT'),'image_width'=>@constant('USER_IMAGE_HEIGHT'),'image_type'=>"user_img"), 0);?>

                <?php }?>
            </div>

            <div class="ty-sd_messaging__body">
                <h3 class="ty-sd_messaging_system-recipient-user__name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['last_message_name'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['ticket']->value['vendor_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['vendor_name'], ENT_QUOTES, 'UTF-8');
}?>
                    <span class="ty-sd_messaging_system-last-message__date dotted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['last_message_date'], ENT_QUOTES, 'UTF-8');?>
</span>
                    <?php if ($_smarty_tpl->tpl_vars['ticket']->value['last_message']&&$_smarty_tpl->tpl_vars['ticket']->value['status']==@constant('TICKET_STATUS_NEW')) {?>
                        <span class="ty-sd_messaging_system-last-message__date new-message dotted"><?php echo $_smarty_tpl->__("new");
if ($_smarty_tpl->tpl_vars['ticket']->value['new_messages_amount']) {?>&nbsp;(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ticket']->value['new_messages_amount'], ENT_QUOTES, 'UTF-8');?>
)<?php }?></span>
                    <?php }?>
                </h3>
                <p class="ty-sd_messaging_system-last-message__message">
                    <?php echo smarty_function_math(array('equation'=>"x+y",'x'=>$_smarty_tpl->tpl_vars['addons']->value['sd_messaging_system']['trimmed_messages_length'],'y'=>@constant('TRIPLE_DOT_LENGHT'),'assign'=>"message_length"),$_smarty_tpl);?>

                    <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['ticket']->value['last_message']),$_smarty_tpl->tpl_vars['message_length']->value);?>

                </p>
            </div>
        </a>
    </li>
<?php }
if (!$_smarty_tpl->tpl_vars['ticket']->_loop) {
?>
    <p class="ty-no-items"><?php echo $_smarty_tpl->__("text_no_tickets");?>
</p>
<?php } ?>
</ul>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("messages");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?><?php }} ?>
