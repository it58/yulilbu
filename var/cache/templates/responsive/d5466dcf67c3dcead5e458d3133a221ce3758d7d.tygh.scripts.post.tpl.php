<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:53:56
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_messaging_system/hooks/index/scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7711204535d4843047ff724-15629255%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd5466dcf67c3dcead5e458d3133a221ce3758d7d' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_messaging_system/hooks/index/scripts.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '7711204535d4843047ff724-15629255',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4843048287b2_39757509',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4843048287b2_39757509')) {function content_5d4843048287b2_39757509($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('addons.sd_messaging_system.reload_page','addons.sd_messaging_system.reload_page'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><?php echo '<script'; ?>
 type="text/javascript">
(function(_, $) {
    var controller = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['runtime']->value['controller'], ENT_QUOTES, 'UTF-8');?>
';
    var mode = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['runtime']->value['mode'], ENT_QUOTES, 'UTF-8');?>
';
    var theme = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['theme_name'], ENT_QUOTES, 'UTF-8');?>
';

    if (controller == 'messenger' && mode == 'view') {
        $(window).resize(function() {
            if ($(window).height() > 800) {
                $('.tygh-header .row-fluid').first().show();
            } else {
                $('.tygh-header .row-fluid').first().hide();
            }
        });
        $('.sd-lists-popup').css('display', 'none');
    }

    if (theme == 'passion') {
        var $style = "<style>.ty-sd_messaging .ty-sd_messaging__ticket-new { background: #dd2884; } .recipient-message .ty-sd_messaging_system-all { background-color: #fff; } .recipient-message .ty-sd_messaging_system-all:before { background-color: #fff; } .ty-sd_messaging_system-name, .ty-sd_messaging_system-message__message { color:inherit !important; } .ty-sd_messaging__list-bordered > li { border-color:#dadada !important; }</style>";

        $($style).appendTo("head");
    }

    if (theme == 'kids_theme') {
        var $style = "<style>.ty-sd_messaging .ty-sd_messaging__ticket-new { background: #00cad3; } .author-message .ty-sd_messaging_system-all:before { background-color:#00cad3 !important; } .ty-sd_messaging_system-all { background-color: #00cad3 !important; } .recipient-message .ty-sd_messaging_system-message__message { color: white; } .recipient-message .ty-sd_messaging_system-name { color: white; } .recipient-message .ty-sd_messaging_system-all:before { background-color:#00cad3; }</style>";

        $($style).appendTo("head");
    }

    if (theme == 'american') {
        var $style = "<style>.ty-discussion-post__buttons.buttons-container { background: #f4f4f4; }</style>";

        $($style).appendTo("head");
    }

    _.tr({
        'addons.sd_messaging_system.reload_page': '<?php echo strtr($_smarty_tpl->__("addons.sd_messaging_system.reload_page"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'
    });

}(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_messaging_system/hooks/index/scripts.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_messaging_system/hooks/index/scripts.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><?php echo '<script'; ?>
 type="text/javascript">
(function(_, $) {
    var controller = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['runtime']->value['controller'], ENT_QUOTES, 'UTF-8');?>
';
    var mode = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['runtime']->value['mode'], ENT_QUOTES, 'UTF-8');?>
';
    var theme = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['theme_name'], ENT_QUOTES, 'UTF-8');?>
';

    if (controller == 'messenger' && mode == 'view') {
        $(window).resize(function() {
            if ($(window).height() > 800) {
                $('.tygh-header .row-fluid').first().show();
            } else {
                $('.tygh-header .row-fluid').first().hide();
            }
        });
        $('.sd-lists-popup').css('display', 'none');
    }

    if (theme == 'passion') {
        var $style = "<style>.ty-sd_messaging .ty-sd_messaging__ticket-new { background: #dd2884; } .recipient-message .ty-sd_messaging_system-all { background-color: #fff; } .recipient-message .ty-sd_messaging_system-all:before { background-color: #fff; } .ty-sd_messaging_system-name, .ty-sd_messaging_system-message__message { color:inherit !important; } .ty-sd_messaging__list-bordered > li { border-color:#dadada !important; }</style>";

        $($style).appendTo("head");
    }

    if (theme == 'kids_theme') {
        var $style = "<style>.ty-sd_messaging .ty-sd_messaging__ticket-new { background: #00cad3; } .author-message .ty-sd_messaging_system-all:before { background-color:#00cad3 !important; } .ty-sd_messaging_system-all { background-color: #00cad3 !important; } .recipient-message .ty-sd_messaging_system-message__message { color: white; } .recipient-message .ty-sd_messaging_system-name { color: white; } .recipient-message .ty-sd_messaging_system-all:before { background-color:#00cad3; }</style>";

        $($style).appendTo("head");
    }

    if (theme == 'american') {
        var $style = "<style>.ty-discussion-post__buttons.buttons-container { background: #f4f4f4; }</style>";

        $($style).appendTo("head");
    }

    _.tr({
        'addons.sd_messaging_system.reload_page': '<?php echo strtr($_smarty_tpl->__("addons.sd_messaging_system.reload_page"), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'
    });

}(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php }?><?php }} ?>
