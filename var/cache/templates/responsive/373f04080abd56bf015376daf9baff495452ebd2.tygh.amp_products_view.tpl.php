<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:54:07
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_accelerated_pages/views/products/amp_products_view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12590932995d48430f16ff75-83542149%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '373f04080abd56bf015376daf9baff495452ebd2' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_accelerated_pages/views/products/amp_products_view.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12590932995d48430f16ff75-83542149',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'language_direction' => 0,
    'product' => 0,
    'meta_description' => 0,
    'location_data' => 0,
    'meta_keywords' => 0,
    'ga_tracking_id' => 0,
    'ldelim' => 0,
    'rdelim' => 0,
    'amp_iframe' => 0,
    'amp_youtube' => 0,
    'logos' => 0,
    'amp_logo' => 0,
    'settings' => 0,
    'search' => 0,
    'search_title' => 0,
    'images_dir' => 0,
    'image' => 0,
    'obj_id' => 0,
    'show_discount_label' => 0,
    'show_price_values' => 0,
    'obj_prefix' => 0,
    'show_list_discount' => 0,
    'show_sku' => 0,
    'show_sku_label' => 0,
    'show_product_amount' => 0,
    'product_amount' => 0,
    'auth' => 0,
    'details_page' => 0,
    'but_id' => 0,
    'but_role' => 0,
    'tabs' => 0,
    'tab' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48430f3d71d1_80614591',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48430f3d71d1_80614591')) {function content_5d48430f3d71d1_80614591($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/yulibu/public_html/app/functions/smarty_plugins/modifier.enum.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('search','search_products','search','search','save_discount','old_price','list_price','you_save','you_save','sku','items','on_backorder','in_stock','out_of_stock_products','in_stock','on_backorder','out_of_stock_products','buy_now','sd_accelerated_pages.view_detailes','sign_in_to_buy','text_login_to_add_to_cart','search','search_products','search','search','save_discount','old_price','list_price','you_save','you_save','sku','items','on_backorder','in_stock','out_of_stock_products','in_stock','on_backorder','out_of_stock_products','buy_now','sd_accelerated_pages.view_detailes','sign_in_to_buy','text_login_to_add_to_cart'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><!DOCTYPE html>
<html amp lang="<?php echo htmlspecialchars(@constant('CART_LANGUAGE'), ENT_QUOTES, 'UTF-8');?>
" dir="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
">
<head>
    <meta charset="utf-8">
    <title><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product']->value['page_title'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product']->value['product'] : $tmp), ENT_QUOTES, 'UTF-8');?>
</title>
    <link rel="canonical" href="<?php echo htmlspecialchars(fn_url("index.php?dispatch=products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width, minimum-scale=1" />
    <meta name="description" content="<?php echo htmlspecialchars((($tmp = @html_entity_decode($_smarty_tpl->tpl_vars['meta_description']->value,@constant('ENT_COMPAT'),"UTF-8"))===null||$tmp==='' ? $_smarty_tpl->tpl_vars['location_data']->value['meta_description'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
    <meta name="keywords" content="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['meta_keywords']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['location_data']->value['meta_keywords'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
    <?php if ($_smarty_tpl->tpl_vars['ga_tracking_id']->value) {?>
        <?php echo '<script'; ?>
 async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"><?php echo '</script'; ?>
>
    <?php }?>
    <style amp-boilerplate>body<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-webkit-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-moz-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-ms-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-o-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
</style><noscript><style amp-boilerplate>body<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
</style></noscript>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_accelerated_pages/common/styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo '<script'; ?>
 async src="https://cdn.ampproject.org/v0.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"><?php echo '</script'; ?>
>

    <?php if ($_smarty_tpl->tpl_vars['amp_iframe']->value) {?>
    <?php echo '<script'; ?>
 async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"><?php echo '</script'; ?>
>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['amp_youtube']->value) {?>
    <?php echo '<script'; ?>
 async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"><?php echo '</script'; ?>
>
    <?php }?>
</head>
<body>
    <div class="content">
        <div class="ty-header">
            <div class="ty-logo-container">
                <?php $_smarty_tpl->tpl_vars["amp_logo"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['logos']->value['amp'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['logos']->value['theme'] : $tmp), null, 0);?>
                <a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['alt'], ENT_QUOTES, 'UTF-8');?>
">
                    <amp-img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['image_path'], ENT_QUOTES, 'UTF-8');?>
"
                        width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['image_x'], ENT_QUOTES, 'UTF-8');?>
"
                        height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['image_y'], ENT_QUOTES, 'UTF-8');?>
"
                        alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['alt'], ENT_QUOTES, 'UTF-8');?>
"
                        class="ty-logo-container__image">
                    </amp-img>
                </a>
            </div>
            <div class="ty-search-block">
                <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" name="search_form" method="GET" target="_top">
                    <input type="hidden" name="subcats" value="Y" />
                    <input type="hidden" name="pcode_from_q" value="Y" />
                    <input type="hidden" name="pshort" value="Y" />
                    <input type="hidden" name="pfull" value="Y" />
                    <input type="hidden" name="pname" value="Y" />
                    <input type="hidden" name="pkeywords" value="Y" />
                    <input type="hidden" name="search_performed" value="Y" />

                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['search_objects']) {?>
                        <?php $_smarty_tpl->tpl_vars["search_title"] = new Smarty_variable($_smarty_tpl->__("search"), null, 0);?>
                    <?php } else { ?>
                        <?php $_smarty_tpl->tpl_vars["search_title"] = new Smarty_variable($_smarty_tpl->__("search_products"), null, 0);?>
                    <?php }?>
                    <input type="text" name="q" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['q'], ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_title']->value, ENT_QUOTES, 'UTF-8');?>
" id="search_input<?php echo htmlspecialchars(Smarty::$_smarty_vars['capture']['search_input_id'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_title']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-search-block__input cm-hint" />
                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['search_objects']) {?>
                        <button title="<?php echo $_smarty_tpl->__("search");?>
" class="ty-search-magnifier" type="submit"><amp-img layout="responsive" width="24" height="24" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/addons/sd_accelerated_pages/search.png"></amp-img></button>
                        <input type="hidden" name="dispatch" value="search.results" />
                    <?php } else { ?>
                        <button title="<?php echo $_smarty_tpl->__("search");?>
" class="ty-search-magnifier" type="submit"><amp-img layout="responsive" width="24" height="24" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/addons/sd_accelerated_pages/search.png"></amp-img></button>
                        <input type="hidden" name="dispatch" value="products.search" />
                    <?php }?>
                </form>
            </div>
        </div>
        <div class="ty-main-image">
            <amp-carousel type="slides" layout="responsive" width="360" height="240">
                <amp-img layout="responsive" height="600" width="1200" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
"></amp-img>
                <?php  $_smarty_tpl->tpl_vars["image"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["image"]->_loop = false;
 $_smarty_tpl->tpl_vars["pair_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['image_pairs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["image"]->key => $_smarty_tpl->tpl_vars["image"]->value) {
$_smarty_tpl->tpl_vars["image"]->_loop = true;
 $_smarty_tpl->tpl_vars["pair_id"]->value = $_smarty_tpl->tpl_vars["image"]->key;
?>
                    <amp-img layout="responsive" height="600" width="1200" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
"></amp-img>
                <?php } ?>
            </amp-carousel>
        </div>
        <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>

        <div class="ty-product-block">
            <div class="ty-product-main-info">
                <?php $_smarty_tpl->tpl_vars["form_open"] = new Smarty_variable("form_open_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>

                <h1 class="ty-product-block-title"><bdi><?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>
</bdi></h1>

                <?php if ($_smarty_tpl->tpl_vars['show_discount_label']->value&&($_smarty_tpl->tpl_vars['product']->value['discount_prc']||$_smarty_tpl->tpl_vars['product']->value['list_discount_prc'])&&$_smarty_tpl->tpl_vars['show_price_values']->value) {?>
                <span class="ty-discount-label cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="discount_label_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <span class="ty-discount-label__item" id="line_prc_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><span class="ty-discount-label__value" id="prc_discount_value_label_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("save_discount");?>
 <?php if ($_smarty_tpl->tpl_vars['product']->value['discount']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discount_prc'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['list_discount_prc'], ENT_QUOTES, 'UTF-8');
}?>%</span></span>
                </span>
                <?php }?>

                <div class="ty-product-brand">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_features_short_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('features'=>$_smarty_tpl->tpl_vars['product']->value['header_features']), 0);?>

                </div>

                <span class="ty-old-price" id="old_price_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                <?php if ($_smarty_tpl->tpl_vars['product']->value['discount']) {?>
                    <span class="ty-list-price ty-nowrap" id="line_old_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("old_price");?>
: <span class="ty-strike"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['product']->value['original_price'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product']->value['base_price'] : $tmp),'span_id'=>"old_price_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-list-price ty-nowrap"), 0);?>
</span></span>
                <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['list_discount']) {?>
                    <span class="ty-list-price ty-nowrap" id="line_list_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><span class="list-price-label"><?php echo $_smarty_tpl->__("list_price");?>
:</span> <span class="ty-strike"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['list_price'],'span_id'=>"list_price_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-list-price ty-nowrap"), 0);?>
</span></span>
                <?php }?>
                </span>

                <div class="prices-container price-wrap">
                    <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['price'])||$_smarty_tpl->tpl_vars['product']->value['zero_price_action']=='P') {?>
                    <span class="ty-price" id="line_discounted_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price'],'span_id'=>"discounted_price_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-price-num",'live_editor_name'=>"product:price:".((string)$_smarty_tpl->tpl_vars['product']->value['product_id']),'live_editor_phrase'=>$_smarty_tpl->tpl_vars['product']->value['base_price']), 0);?>

                    </span>
                    <?php }?>
                </div>

                <span class="ty-product-discount" id="line_discount_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <input type="hidden" name="appearance[show_price_values]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_price_values']->value, ENT_QUOTES, 'UTF-8');?>
" />
                    <input type="hidden" name="appearance[show_list_discount]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_list_discount']->value, ENT_QUOTES, 'UTF-8');?>
" />
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['discount']) {?>
                        <span class="ty-list-price ty-save-price ty-nowrap" id="line_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("you_save");?>
: <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['discount'],'span_id'=>"discount_value_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-list-price ty-nowrap"), 0);?>
&nbsp;(<span id="prc_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-list-price ty-nowrap"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discount_prc'], ENT_QUOTES, 'UTF-8');?>
</span>%)</span>
                    <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['list_discount']) {?>
                        <span class="ty-list-price ty-save-price ty-nowrap" id="line_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"> <?php echo $_smarty_tpl->__("you_save");?>
: <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['list_discount'],'span_id'=>"discount_value_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value)), 0);?>
&nbsp;(<span id="prc_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['list_discount_prc'], ENT_QUOTES, 'UTF-8');?>
</span>%)</span>
                    <?php }?>
                </span>

                <div class="ty-product-block__sku">
                    <div class="ty-control-group ty-sku-item cm-hidden-wrapper<?php if (!$_smarty_tpl->tpl_vars['product']->value['product_code']) {?> hidden<?php }?> cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="sku_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <input type="hidden" name="appearance[show_sku]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_sku']->value, ENT_QUOTES, 'UTF-8');?>
" />
                        <?php if ($_smarty_tpl->tpl_vars['show_sku_label']->value) {?>
                            <label class="ty-control-group__label" id="sku_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("sku");?>
:</label>
                        <?php }?>
                        <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</span>
                    </div>
                </div>

                <?php if ($_smarty_tpl->tpl_vars['product']->value['is_edp']!="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y") {?>
                    <?php $_smarty_tpl->tpl_vars["product_amount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['inventory_amount'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product']->value['amount'] : $tmp), null, 0);?>
                    <div class="ty-product-availability stock-wrap" id="product_amount_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <input type="hidden" name="appearance[show_product_amount]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_product_amount']->value, ENT_QUOTES, 'UTF-8');?>
" />
                        <?php if (!$_smarty_tpl->tpl_vars['product']->value['hide_stock_info']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Appearance']['in_stock_field']=="Y") {?>
                                <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty'])&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y") {?>
                                    <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty']||$_smarty_tpl->tpl_vars['product']->value['out_of_stock_actions']==smarty_modifier_enum("OutOfStockActions::BUY_IN_ADVANCE"))&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y") {?>
                                        <div class="ty-control-group product-list-field">
                                            <span id="qty_in_stock_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-qty-in-stock ty-control-group__item">
                                                <?php if ($_smarty_tpl->tpl_vars['product_amount']->value>0) {?>
                                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_amount']->value, ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo $_smarty_tpl->__("items");?>

                                                <?php } else { ?>
                                                    <?php echo $_smarty_tpl->__("on_backorder");?>

                                                <?php }?>
                                            </span>
                                        </div>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']!="Y") {?>
                                        <div class="ty-control-group product-list-field">
                                                <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("in_stock");?>
:</label>
                                            <span class="ty-qty-out-of-stock ty-control-group__item"><?php echo $_smarty_tpl->__("out_of_stock_products");?>
</span>
                                        </div>
                                    <?php }?>
                                <?php }?>
                            <?php } else { ?>
                                <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty']||$_smarty_tpl->tpl_vars['product']->value['tracking']==smarty_modifier_enum("ProductTracking::DO_NOT_TRACK"))&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']!="Y"||$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&($_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']=="Y"||$_smarty_tpl->tpl_vars['product']->value['out_of_stock_actions']==smarty_modifier_enum("OutOfStockActions::BUY_IN_ADVANCE"))) {?>
                                    <div class="ty-control-group product-list-field">
                                        <span class="ty-qty-in-stock ty-control-group__item" id="in_stock_info_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                            <?php if ($_smarty_tpl->tpl_vars['product_amount']->value>0) {?>
                                                <?php echo $_smarty_tpl->__("in_stock");?>

                                            <?php } else { ?>
                                                <?php echo $_smarty_tpl->__("on_backorder");?>

                                            <?php }?>
                                        </span>
                                    </div>
                                <?php } elseif ($_smarty_tpl->tpl_vars['product_amount']->value<=0||$_smarty_tpl->tpl_vars['product_amount']->value<$_smarty_tpl->tpl_vars['product']->value['min_qty']&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']!="Y") {?>
                                    <div class="ty-control-group product-list-field">
                                        <span class="ty-qty-out-of-stock ty-control-group__item" id="out_of_stock_info_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("out_of_stock_products");?>
</span>
                                    </div>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['allow_anonymous_shopping']=="allow_shopping"||$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
                    <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty']&&$_smarty_tpl->tpl_vars['product']->value['price']>0)&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"||$_smarty_tpl->tpl_vars['details_page']->value) {?>
                        <div class="ty-product-block__button">
                            <a class="ty-btn" href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['obj_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("buy_now");?>
</a>
                        </div>
                    <?php } else { ?>
                        <div class="ty-product-block__button">
                            <a class="ty-btn" href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['obj_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("sd_accelerated_pages.view_detailes");?>
</a>
                        </div>
                    <?php }?>
                <?php } else { ?>
                    <?php ob_start();
echo htmlspecialchars(fn_url("auth.login_form"), ENT_QUOTES, 'UTF-8');
$_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_id'=>$_smarty_tpl->tpl_vars['but_id']->value,'but_text'=>$_smarty_tpl->__("sign_in_to_buy"),'but_href'=>$_tmp1,'but_role'=>(($tmp = @$_smarty_tpl->tpl_vars['but_role']->value)===null||$tmp==='' ? "text" : $tmp),'but_name'=>''), 0);?>

                    <p><?php echo $_smarty_tpl->__("text_login_to_add_to_cart");?>
</p>
                <?php }?>
            </div>

            <div class="ty-detailed-info">
                <amp-accordion disable-session-states>
                    <?php  $_smarty_tpl->tpl_vars["tab"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["tab"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["tab"]->key => $_smarty_tpl->tpl_vars["tab"]->value) {
$_smarty_tpl->tpl_vars["tab"]->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['tab']->value['html_id']=="description"&&$_smarty_tpl->tpl_vars['product']->value['amp_description']) {?>
                            <section expanded class="ty-section-description">
                                <h4 class="ty-product-block__description-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h4>
                                <div class="accordion-description">
                                    <?php echo $_smarty_tpl->tpl_vars['product']->value['amp_description'];?>

                                </div>
                            </section>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['tab']->value['html_id']=="features"&&$_smarty_tpl->tpl_vars['product']->value['product_features']) {?>
                            <section expanded class="ty-section-features">
                                <h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h4>
                                <div class="accordion-description cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="product_features_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                    <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_features.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_features'=>$_smarty_tpl->tpl_vars['product']->value['product_features']), 0);?>

                                </div>
                            </section>
                        <?php }?>
                    <?php } ?>
                </amp-accordion>
            </div>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_accelerated_pages/common/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_accelerated_pages/views/products/amp_products_view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_accelerated_pages/views/products/amp_products_view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><!DOCTYPE html>
<html amp lang="<?php echo htmlspecialchars(@constant('CART_LANGUAGE'), ENT_QUOTES, 'UTF-8');?>
" dir="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
">
<head>
    <meta charset="utf-8">
    <title><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product']->value['page_title'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product']->value['product'] : $tmp), ENT_QUOTES, 'UTF-8');?>
</title>
    <link rel="canonical" href="<?php echo htmlspecialchars(fn_url("index.php?dispatch=products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width, minimum-scale=1" />
    <meta name="description" content="<?php echo htmlspecialchars((($tmp = @html_entity_decode($_smarty_tpl->tpl_vars['meta_description']->value,@constant('ENT_COMPAT'),"UTF-8"))===null||$tmp==='' ? $_smarty_tpl->tpl_vars['location_data']->value['meta_description'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
    <meta name="keywords" content="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['meta_keywords']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['location_data']->value['meta_keywords'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
    <?php if ($_smarty_tpl->tpl_vars['ga_tracking_id']->value) {?>
        <?php echo '<script'; ?>
 async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"><?php echo '</script'; ?>
>
    <?php }?>
    <style amp-boilerplate>body<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-webkit-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-moz-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-ms-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@-o-keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
@keyframes -amp-start<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
from<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:hidden<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
to<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
visibility:visible<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
</style><noscript><style amp-boilerplate>body<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ldelim']->value, ENT_QUOTES, 'UTF-8');?>
-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rdelim']->value, ENT_QUOTES, 'UTF-8');?>
</style></noscript>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_accelerated_pages/common/styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo '<script'; ?>
 async src="https://cdn.ampproject.org/v0.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"><?php echo '</script'; ?>
>

    <?php if ($_smarty_tpl->tpl_vars['amp_iframe']->value) {?>
    <?php echo '<script'; ?>
 async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"><?php echo '</script'; ?>
>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['amp_youtube']->value) {?>
    <?php echo '<script'; ?>
 async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"><?php echo '</script'; ?>
>
    <?php }?>
</head>
<body>
    <div class="content">
        <div class="ty-header">
            <div class="ty-logo-container">
                <?php $_smarty_tpl->tpl_vars["amp_logo"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['logos']->value['amp'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['logos']->value['theme'] : $tmp), null, 0);?>
                <a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['alt'], ENT_QUOTES, 'UTF-8');?>
">
                    <amp-img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['image_path'], ENT_QUOTES, 'UTF-8');?>
"
                        width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['image_x'], ENT_QUOTES, 'UTF-8');?>
"
                        height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['image_y'], ENT_QUOTES, 'UTF-8');?>
"
                        alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['amp_logo']->value['image']['alt'], ENT_QUOTES, 'UTF-8');?>
"
                        class="ty-logo-container__image">
                    </amp-img>
                </a>
            </div>
            <div class="ty-search-block">
                <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" name="search_form" method="GET" target="_top">
                    <input type="hidden" name="subcats" value="Y" />
                    <input type="hidden" name="pcode_from_q" value="Y" />
                    <input type="hidden" name="pshort" value="Y" />
                    <input type="hidden" name="pfull" value="Y" />
                    <input type="hidden" name="pname" value="Y" />
                    <input type="hidden" name="pkeywords" value="Y" />
                    <input type="hidden" name="search_performed" value="Y" />

                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['search_objects']) {?>
                        <?php $_smarty_tpl->tpl_vars["search_title"] = new Smarty_variable($_smarty_tpl->__("search"), null, 0);?>
                    <?php } else { ?>
                        <?php $_smarty_tpl->tpl_vars["search_title"] = new Smarty_variable($_smarty_tpl->__("search_products"), null, 0);?>
                    <?php }?>
                    <input type="text" name="q" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['q'], ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_title']->value, ENT_QUOTES, 'UTF-8');?>
" id="search_input<?php echo htmlspecialchars(Smarty::$_smarty_vars['capture']['search_input_id'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_title']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-search-block__input cm-hint" />
                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['search_objects']) {?>
                        <button title="<?php echo $_smarty_tpl->__("search");?>
" class="ty-search-magnifier" type="submit"><amp-img layout="responsive" width="24" height="24" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/addons/sd_accelerated_pages/search.png"></amp-img></button>
                        <input type="hidden" name="dispatch" value="search.results" />
                    <?php } else { ?>
                        <button title="<?php echo $_smarty_tpl->__("search");?>
" class="ty-search-magnifier" type="submit"><amp-img layout="responsive" width="24" height="24" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['images_dir']->value, ENT_QUOTES, 'UTF-8');?>
/addons/sd_accelerated_pages/search.png"></amp-img></button>
                        <input type="hidden" name="dispatch" value="products.search" />
                    <?php }?>
                </form>
            </div>
        </div>
        <div class="ty-main-image">
            <amp-carousel type="slides" layout="responsive" width="360" height="240">
                <amp-img layout="responsive" height="600" width="1200" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
"></amp-img>
                <?php  $_smarty_tpl->tpl_vars["image"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["image"]->_loop = false;
 $_smarty_tpl->tpl_vars["pair_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['image_pairs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["image"]->key => $_smarty_tpl->tpl_vars["image"]->value) {
$_smarty_tpl->tpl_vars["image"]->_loop = true;
 $_smarty_tpl->tpl_vars["pair_id"]->value = $_smarty_tpl->tpl_vars["image"]->key;
?>
                    <amp-img layout="responsive" height="600" width="1200" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
"></amp-img>
                <?php } ?>
            </amp-carousel>
        </div>
        <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>

        <div class="ty-product-block">
            <div class="ty-product-main-info">
                <?php $_smarty_tpl->tpl_vars["form_open"] = new Smarty_variable("form_open_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>

                <h1 class="ty-product-block-title"><bdi><?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>
</bdi></h1>

                <?php if ($_smarty_tpl->tpl_vars['show_discount_label']->value&&($_smarty_tpl->tpl_vars['product']->value['discount_prc']||$_smarty_tpl->tpl_vars['product']->value['list_discount_prc'])&&$_smarty_tpl->tpl_vars['show_price_values']->value) {?>
                <span class="ty-discount-label cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="discount_label_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <span class="ty-discount-label__item" id="line_prc_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><span class="ty-discount-label__value" id="prc_discount_value_label_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("save_discount");?>
 <?php if ($_smarty_tpl->tpl_vars['product']->value['discount']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discount_prc'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['list_discount_prc'], ENT_QUOTES, 'UTF-8');
}?>%</span></span>
                </span>
                <?php }?>

                <div class="ty-product-brand">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_features_short_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('features'=>$_smarty_tpl->tpl_vars['product']->value['header_features']), 0);?>

                </div>

                <span class="ty-old-price" id="old_price_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                <?php if ($_smarty_tpl->tpl_vars['product']->value['discount']) {?>
                    <span class="ty-list-price ty-nowrap" id="line_old_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("old_price");?>
: <span class="ty-strike"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['product']->value['original_price'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product']->value['base_price'] : $tmp),'span_id'=>"old_price_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-list-price ty-nowrap"), 0);?>
</span></span>
                <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['list_discount']) {?>
                    <span class="ty-list-price ty-nowrap" id="line_list_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><span class="list-price-label"><?php echo $_smarty_tpl->__("list_price");?>
:</span> <span class="ty-strike"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['list_price'],'span_id'=>"list_price_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-list-price ty-nowrap"), 0);?>
</span></span>
                <?php }?>
                </span>

                <div class="prices-container price-wrap">
                    <?php if (floatval($_smarty_tpl->tpl_vars['product']->value['price'])||$_smarty_tpl->tpl_vars['product']->value['zero_price_action']=='P') {?>
                    <span class="ty-price" id="line_discounted_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price'],'span_id'=>"discounted_price_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-price-num",'live_editor_name'=>"product:price:".((string)$_smarty_tpl->tpl_vars['product']->value['product_id']),'live_editor_phrase'=>$_smarty_tpl->tpl_vars['product']->value['base_price']), 0);?>

                    </span>
                    <?php }?>
                </div>

                <span class="ty-product-discount" id="line_discount_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <input type="hidden" name="appearance[show_price_values]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_price_values']->value, ENT_QUOTES, 'UTF-8');?>
" />
                    <input type="hidden" name="appearance[show_list_discount]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_list_discount']->value, ENT_QUOTES, 'UTF-8');?>
" />
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['discount']) {?>
                        <span class="ty-list-price ty-save-price ty-nowrap" id="line_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("you_save");?>
: <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['discount'],'span_id'=>"discount_value_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value),'class'=>"ty-list-price ty-nowrap"), 0);?>
&nbsp;(<span id="prc_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-list-price ty-nowrap"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discount_prc'], ENT_QUOTES, 'UTF-8');?>
</span>%)</span>
                    <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['list_discount']) {?>
                        <span class="ty-list-price ty-save-price ty-nowrap" id="line_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"> <?php echo $_smarty_tpl->__("you_save");?>
: <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['list_discount'],'span_id'=>"discount_value_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value)), 0);?>
&nbsp;(<span id="prc_discount_value_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['list_discount_prc'], ENT_QUOTES, 'UTF-8');?>
</span>%)</span>
                    <?php }?>
                </span>

                <div class="ty-product-block__sku">
                    <div class="ty-control-group ty-sku-item cm-hidden-wrapper<?php if (!$_smarty_tpl->tpl_vars['product']->value['product_code']) {?> hidden<?php }?> cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="sku_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <input type="hidden" name="appearance[show_sku]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_sku']->value, ENT_QUOTES, 'UTF-8');?>
" />
                        <?php if ($_smarty_tpl->tpl_vars['show_sku_label']->value) {?>
                            <label class="ty-control-group__label" id="sku_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("sku");?>
:</label>
                        <?php }?>
                        <span class="ty-control-group__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</span>
                    </div>
                </div>

                <?php if ($_smarty_tpl->tpl_vars['product']->value['is_edp']!="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y") {?>
                    <?php $_smarty_tpl->tpl_vars["product_amount"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['product']->value['inventory_amount'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product']->value['amount'] : $tmp), null, 0);?>
                    <div class="ty-product-availability stock-wrap" id="product_amount_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <input type="hidden" name="appearance[show_product_amount]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_product_amount']->value, ENT_QUOTES, 'UTF-8');?>
" />
                        <?php if (!$_smarty_tpl->tpl_vars['product']->value['hide_stock_info']) {?>
                            <?php if ($_smarty_tpl->tpl_vars['settings']->value['Appearance']['in_stock_field']=="Y") {?>
                                <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty'])&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y") {?>
                                    <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty']||$_smarty_tpl->tpl_vars['product']->value['out_of_stock_actions']==smarty_modifier_enum("OutOfStockActions::BUY_IN_ADVANCE"))&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y") {?>
                                        <div class="ty-control-group product-list-field">
                                            <span id="qty_in_stock_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-qty-in-stock ty-control-group__item">
                                                <?php if ($_smarty_tpl->tpl_vars['product_amount']->value>0) {?>
                                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_amount']->value, ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo $_smarty_tpl->__("items");?>

                                                <?php } else { ?>
                                                    <?php echo $_smarty_tpl->__("on_backorder");?>

                                                <?php }?>
                                            </span>
                                        </div>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']!="Y") {?>
                                        <div class="ty-control-group product-list-field">
                                                <label class="ty-control-group__label"><?php echo $_smarty_tpl->__("in_stock");?>
:</label>
                                            <span class="ty-qty-out-of-stock ty-control-group__item"><?php echo $_smarty_tpl->__("out_of_stock_products");?>
</span>
                                        </div>
                                    <?php }?>
                                <?php }?>
                            <?php } else { ?>
                                <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty']||$_smarty_tpl->tpl_vars['product']->value['tracking']==smarty_modifier_enum("ProductTracking::DO_NOT_TRACK"))&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']!="Y"||$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&($_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']=="Y"||$_smarty_tpl->tpl_vars['product']->value['out_of_stock_actions']==smarty_modifier_enum("OutOfStockActions::BUY_IN_ADVANCE"))) {?>
                                    <div class="ty-control-group product-list-field">
                                        <span class="ty-qty-in-stock ty-control-group__item" id="in_stock_info_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                            <?php if ($_smarty_tpl->tpl_vars['product_amount']->value>0) {?>
                                                <?php echo $_smarty_tpl->__("in_stock");?>

                                            <?php } else { ?>
                                                <?php echo $_smarty_tpl->__("on_backorder");?>

                                            <?php }?>
                                        </span>
                                    </div>
                                <?php } elseif ($_smarty_tpl->tpl_vars['product_amount']->value<=0||$_smarty_tpl->tpl_vars['product_amount']->value<$_smarty_tpl->tpl_vars['product']->value['min_qty']&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"&&$_smarty_tpl->tpl_vars['settings']->value['General']['allow_negative_amount']!="Y") {?>
                                    <div class="ty-control-group product-list-field">
                                        <span class="ty-qty-out-of-stock ty-control-group__item" id="out_of_stock_info_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("out_of_stock_products");?>
</span>
                                    </div>
                                <?php }?>
                            <?php }?>
                        <?php }?>
                    </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['allow_anonymous_shopping']=="allow_shopping"||$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
                    <?php if (($_smarty_tpl->tpl_vars['product_amount']->value>0&&$_smarty_tpl->tpl_vars['product_amount']->value>=$_smarty_tpl->tpl_vars['product']->value['min_qty']&&$_smarty_tpl->tpl_vars['product']->value['price']>0)&&$_smarty_tpl->tpl_vars['settings']->value['General']['inventory_tracking']=="Y"||$_smarty_tpl->tpl_vars['details_page']->value) {?>
                        <div class="ty-product-block__button">
                            <a class="ty-btn" href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['obj_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("buy_now");?>
</a>
                        </div>
                    <?php } else { ?>
                        <div class="ty-product-block__button">
                            <a class="ty-btn" href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['obj_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("sd_accelerated_pages.view_detailes");?>
</a>
                        </div>
                    <?php }?>
                <?php } else { ?>
                    <?php ob_start();
echo htmlspecialchars(fn_url("auth.login_form"), ENT_QUOTES, 'UTF-8');
$_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_id'=>$_smarty_tpl->tpl_vars['but_id']->value,'but_text'=>$_smarty_tpl->__("sign_in_to_buy"),'but_href'=>$_tmp2,'but_role'=>(($tmp = @$_smarty_tpl->tpl_vars['but_role']->value)===null||$tmp==='' ? "text" : $tmp),'but_name'=>''), 0);?>

                    <p><?php echo $_smarty_tpl->__("text_login_to_add_to_cart");?>
</p>
                <?php }?>
            </div>

            <div class="ty-detailed-info">
                <amp-accordion disable-session-states>
                    <?php  $_smarty_tpl->tpl_vars["tab"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["tab"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["tab"]->key => $_smarty_tpl->tpl_vars["tab"]->value) {
$_smarty_tpl->tpl_vars["tab"]->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['tab']->value['html_id']=="description"&&$_smarty_tpl->tpl_vars['product']->value['amp_description']) {?>
                            <section expanded class="ty-section-description">
                                <h4 class="ty-product-block__description-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h4>
                                <div class="accordion-description">
                                    <?php echo $_smarty_tpl->tpl_vars['product']->value['amp_description'];?>

                                </div>
                            </section>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['tab']->value['html_id']=="features"&&$_smarty_tpl->tpl_vars['product']->value['product_features']) {?>
                            <section expanded class="ty-section-features">
                                <h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h4>
                                <div class="accordion-description cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="product_features_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                    <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_features.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_features'=>$_smarty_tpl->tpl_vars['product']->value['product_features']), 0);?>

                                </div>
                            </section>
                        <?php }?>
                    <?php } ?>
                </amp-accordion>
            </div>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/sd_accelerated_pages/common/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html>
<?php }?><?php }} ?>
