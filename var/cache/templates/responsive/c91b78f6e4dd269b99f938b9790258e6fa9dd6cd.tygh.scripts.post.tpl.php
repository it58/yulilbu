<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:53:56
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/rees46/hooks/index/scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19734583335d4843048bdc40-51768485%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c91b78f6e4dd269b99f938b9790258e6fa9dd6cd' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/rees46/hooks/index/scripts.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19734583335d4843048bdc40-51768485',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'rees46' => 0,
    'auth' => 0,
    'short_user_data' => 0,
    'product' => 0,
    '_cart_products' => 0,
    'cart_product' => 0,
    'rees46_type' => 0,
    'cat' => 0,
    'search' => 0,
    'category_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48430494df09_14530995',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48430494df09_14530995')) {function content_5d48430494df09_14530995($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['rees46']->value&&$_smarty_tpl->tpl_vars['rees46']->value['shop_id']!='') {?>
<?php echo '<script'; ?>
 type="text/javascript">

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/\[(\d+)\]/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

window._r46 = {
    init: function() {
        (function(r){
            window.r46=window.r46||function(){
                (r46.q=r46.q||[]).push(arguments);
            };
            var s=document.getElementsByTagName(r)[0],rs=document.createElement(r);
            rs.async=1;
            rs.src='//cdn.rees46.com/v3.js';
            s.parentNode.insertBefore(rs,s);
        })('script');
        r46('init', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46']->value['shop_id'], ENT_QUOTES, 'UTF-8');?>
');

        <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']>0) {?>
            <?php $_smarty_tpl->tpl_vars['short_user_data'] = new Smarty_variable(fn_get_user_short_info($_smarty_tpl->tpl_vars['auth']->value['user_id']), null, 0);?>
            r46('profile', 'set', { "id":<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['auth']->value['user_id'])===null||$tmp==='' ? 'null' : $tmp), ENT_QUOTES, 'UTF-8');?>
, "email":"<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['short_user_data']->value['email'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
" });
        <?php }?>
    },
    view: function() {
        <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
            document.currentProductId = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
;
        <?php }?>

        <?php $_smarty_tpl->tpl_vars["_cart_products"] = new Smarty_variable($_SESSION['cart']['products'], null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['_cart_products']->value) {?>
            <?php $_smarty_tpl->tpl_vars['_cart_products'] = new Smarty_variable(array_reverse($_smarty_tpl->tpl_vars['_cart_products']->value,true), null, 0);?>
            var ids = [];
            <?php  $_smarty_tpl->tpl_vars["cart_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cart_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cart_product"]->key => $_smarty_tpl->tpl_vars["cart_product"]->value) {
$_smarty_tpl->tpl_vars["cart_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["cart_product"]->key;
?>
                ids.push(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart_product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
);
            <?php } ?>
            document.currentCart = ids;
        <?php } else { ?>
            document.currentCart = [];
        <?php }?>

        <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')) {?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
                params = {
                    <?php if ($_smarty_tpl->tpl_vars['rees46_type']->value) {?>
                    recommended_by: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46_type']->value, ENT_QUOTES, 'UTF-8');?>
',
                    <?php }?>
                    id: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
,
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['price']) {?>
                    price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
,
                    <?php } else { ?>
                    price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['base_price'], ENT_QUOTES, 'UTF-8');?>
,
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['amount']>0) {?>
                    stock: true,
                    <?php } else { ?>
                    stock: false,
                    <?php }?>
                    categories: [<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_smarty_tpl->tpl_vars['cat_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['category_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['cat']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['cat']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
 $_smarty_tpl->tpl_vars['cat_id']->value = $_smarty_tpl->tpl_vars['cat']->key;
 $_smarty_tpl->tpl_vars['cat']->iteration++;
 $_smarty_tpl->tpl_vars['cat']->last = $_smarty_tpl->tpl_vars['cat']->iteration === $_smarty_tpl->tpl_vars['cat']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['cats']['last'] = $_smarty_tpl->tpl_vars['cat']->last;
?>'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value, ENT_QUOTES, 'UTF-8');?>
'<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['cats']['last']) {?>,<?php }
} ?>],
                    name: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
',
                    url: '<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
',
                    image: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
'
                }
                r46('track', 'view', params);
            <?php }?>
        <?php }?>
    },
    search: function() {
        <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='search')) {?>
            var search_query = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['q'], ENT_QUOTES, 'UTF-8');?>
';
            if(search_query) {
                r46('track', 'search', search_query);
            }
        <?php }?>
    },
    category: function() {
        <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='categories'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')&&$_smarty_tpl->tpl_vars['category_data']->value['category_id']>0) {?>
        r46('track','category', <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['category_id'], ENT_QUOTES, 'UTF-8');?>
);
        <?php }?>
    },
    recommend: function() {        
        var rees46_blocks = $('.rees46'), i = 0;
        var rees46_block_render = function () {
            var recommenderBlock = $(this);
            var recommenderType = recommenderBlock.attr('data-type');
            var recommenderCount = recommenderBlock.attr('data-count');
            var recommenderTitle = recommenderBlock.attr('data-title');
            var categoryId = recommenderBlock.attr('data-category');
            var recommenderOrientation = recommenderBlock.attr('data-orientation');

            if (recommenderType) {
            // Skip see_also if cart is empty
                if(recommenderType == 'see_also' && ( document.currentCart == null || document.currentCart.length == 0 ) ) {
                    return;
                }

                r46('recommend', recommenderType, {
                    <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='search')) {?>
                    search_query: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['q'], ENT_QUOTES, 'UTF-8');?>
',
                    <?php }?>
                    category: categoryId,
                    item: document.currentProductId,
                    cart: document.currentCart
                }, function(ids){
                    if (ids.length == 0) {
                        rees46_next_render();
                        return;
                    }
                    if (recommenderCount <= ids.length) {
                    //Стандартные заголовки
                        var recommender_titles = {
                            interesting: 'Вам это будет интересно',
                            also_bought: 'С этим также покупают',
                            similar: 'Похожие товары',
                            popular: 'Популярные товары',
                            see_also: 'Посмотрите также',
                            recently_viewed: 'Вы недавно смотрели',
                            buying_now: 'Сейчас покупают',
                            search: 'Искавшие это также купили'
                        };

                        //Отправляем запрос
                        $.ceAjax('request', fn_url("rees46.get_info"), {
                            data: {
                                product_ids: ids,
                                recommended_by: recommenderType,
                                result_ids: recommenderBlock.attr('id'),
                                title: recommenderTitle ? recommenderTitle : recommender_titles[recommenderType],
                                count: recommenderCount,
                                orientation: recommenderOrientation
                            }, callback: function () { //Находим все ссылки
                                recommenderBlock.find('a').each(function () {
                                    this.href += (this.href.match(/\?/) ? '&' : '?') + 'recommended_by=' + recommenderType
                                });
                                rees46_next_render();
                            }
                        });
                    } else {
                        rees46_next_render();
                    };
                });
            };
        };
        var rees46_next_render = function() {
            if( i < rees46_blocks.length ) {
                rees46_block_render.apply(rees46_blocks.eq(i));
                i++;
            }
        };
        rees46_next_render();
    }
}
_r46.init();
_r46.view();
_r46.search();
_r46.category();

var getRecommend = function () {
    var pageIsReady = setInterval(function() {
        if (typeof jQuery != 'undefined') {
            clearInterval(pageIsReady);
            _r46.recommend();
        }
    }, 100);
}
   
var isREES46InitializationStarted = false;
if ( document.readyState === "complete" ) {
    isREES46InitializationStarted = true;
    getRecommend();
} else if ( document.addEventListener ) {
    document.addEventListener( "DOMContentLoaded", function(){
        if(isREES46InitializationStarted == false) {
            getRecommend();
            isREES46InitializationStarted = true;
        }
    });
    window.addEventListener( "load", function(){
        if(isREES46InitializationStarted == false) {
            getRecommend();
            isREES46InitializationStarted = true;
        }
    });
} else {
    window.attachEvent( "onload", function(){
        if(isREES46InitializationStarted == false) {
            getRecommend();
            isREES46InitializationStarted = true;
        }
    });
}
<?php echo '</script'; ?>
>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/rees46/hooks/index/scripts.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/rees46/hooks/index/scripts.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['rees46']->value&&$_smarty_tpl->tpl_vars['rees46']->value['shop_id']!='') {?>
<?php echo '<script'; ?>
 type="text/javascript">

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/\[(\d+)\]/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

window._r46 = {
    init: function() {
        (function(r){
            window.r46=window.r46||function(){
                (r46.q=r46.q||[]).push(arguments);
            };
            var s=document.getElementsByTagName(r)[0],rs=document.createElement(r);
            rs.async=1;
            rs.src='//cdn.rees46.com/v3.js';
            s.parentNode.insertBefore(rs,s);
        })('script');
        r46('init', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46']->value['shop_id'], ENT_QUOTES, 'UTF-8');?>
');

        <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']>0) {?>
            <?php $_smarty_tpl->tpl_vars['short_user_data'] = new Smarty_variable(fn_get_user_short_info($_smarty_tpl->tpl_vars['auth']->value['user_id']), null, 0);?>
            r46('profile', 'set', { "id":<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['auth']->value['user_id'])===null||$tmp==='' ? 'null' : $tmp), ENT_QUOTES, 'UTF-8');?>
, "email":"<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['short_user_data']->value['email'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
" });
        <?php }?>
    },
    view: function() {
        <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
            document.currentProductId = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
;
        <?php }?>

        <?php $_smarty_tpl->tpl_vars["_cart_products"] = new Smarty_variable($_SESSION['cart']['products'], null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['_cart_products']->value) {?>
            <?php $_smarty_tpl->tpl_vars['_cart_products'] = new Smarty_variable(array_reverse($_smarty_tpl->tpl_vars['_cart_products']->value,true), null, 0);?>
            var ids = [];
            <?php  $_smarty_tpl->tpl_vars["cart_product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cart_product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_cart_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cart_product"]->key => $_smarty_tpl->tpl_vars["cart_product"]->value) {
$_smarty_tpl->tpl_vars["cart_product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["cart_product"]->key;
?>
                ids.push(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart_product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
);
            <?php } ?>
            document.currentCart = ids;
        <?php } else { ?>
            document.currentCart = [];
        <?php }?>

        <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')) {?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
                params = {
                    <?php if ($_smarty_tpl->tpl_vars['rees46_type']->value) {?>
                    recommended_by: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rees46_type']->value, ENT_QUOTES, 'UTF-8');?>
',
                    <?php }?>
                    id: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
,
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['price']) {?>
                    price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
,
                    <?php } else { ?>
                    price: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['base_price'], ENT_QUOTES, 'UTF-8');?>
,
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['amount']>0) {?>
                    stock: true,
                    <?php } else { ?>
                    stock: false,
                    <?php }?>
                    categories: [<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_smarty_tpl->tpl_vars['cat_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product']->value['category_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['cat']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['cat']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
 $_smarty_tpl->tpl_vars['cat_id']->value = $_smarty_tpl->tpl_vars['cat']->key;
 $_smarty_tpl->tpl_vars['cat']->iteration++;
 $_smarty_tpl->tpl_vars['cat']->last = $_smarty_tpl->tpl_vars['cat']->iteration === $_smarty_tpl->tpl_vars['cat']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['cats']['last'] = $_smarty_tpl->tpl_vars['cat']->last;
?>'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value, ENT_QUOTES, 'UTF-8');?>
'<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['cats']['last']) {?>,<?php }
} ?>],
                    name: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
',
                    url: '<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
',
                    image: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['image_path'], ENT_QUOTES, 'UTF-8');?>
'
                }
                r46('track', 'view', params);
            <?php }?>
        <?php }?>
    },
    search: function() {
        <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='search')) {?>
            var search_query = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['q'], ENT_QUOTES, 'UTF-8');?>
';
            if(search_query) {
                r46('track', 'search', search_query);
            }
        <?php }?>
    },
    category: function() {
        <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='categories'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')&&$_smarty_tpl->tpl_vars['category_data']->value['category_id']>0) {?>
        r46('track','category', <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['category_id'], ENT_QUOTES, 'UTF-8');?>
);
        <?php }?>
    },
    recommend: function() {        
        var rees46_blocks = $('.rees46'), i = 0;
        var rees46_block_render = function () {
            var recommenderBlock = $(this);
            var recommenderType = recommenderBlock.attr('data-type');
            var recommenderCount = recommenderBlock.attr('data-count');
            var recommenderTitle = recommenderBlock.attr('data-title');
            var categoryId = recommenderBlock.attr('data-category');
            var recommenderOrientation = recommenderBlock.attr('data-orientation');

            if (recommenderType) {
            // Skip see_also if cart is empty
                if(recommenderType == 'see_also' && ( document.currentCart == null || document.currentCart.length == 0 ) ) {
                    return;
                }

                r46('recommend', recommenderType, {
                    <?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='search')) {?>
                    search_query: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['q'], ENT_QUOTES, 'UTF-8');?>
',
                    <?php }?>
                    category: categoryId,
                    item: document.currentProductId,
                    cart: document.currentCart
                }, function(ids){
                    if (ids.length == 0) {
                        rees46_next_render();
                        return;
                    }
                    if (recommenderCount <= ids.length) {
                    //Стандартные заголовки
                        var recommender_titles = {
                            interesting: 'Вам это будет интересно',
                            also_bought: 'С этим также покупают',
                            similar: 'Похожие товары',
                            popular: 'Популярные товары',
                            see_also: 'Посмотрите также',
                            recently_viewed: 'Вы недавно смотрели',
                            buying_now: 'Сейчас покупают',
                            search: 'Искавшие это также купили'
                        };

                        //Отправляем запрос
                        $.ceAjax('request', fn_url("rees46.get_info"), {
                            data: {
                                product_ids: ids,
                                recommended_by: recommenderType,
                                result_ids: recommenderBlock.attr('id'),
                                title: recommenderTitle ? recommenderTitle : recommender_titles[recommenderType],
                                count: recommenderCount,
                                orientation: recommenderOrientation
                            }, callback: function () { //Находим все ссылки
                                recommenderBlock.find('a').each(function () {
                                    this.href += (this.href.match(/\?/) ? '&' : '?') + 'recommended_by=' + recommenderType
                                });
                                rees46_next_render();
                            }
                        });
                    } else {
                        rees46_next_render();
                    };
                });
            };
        };
        var rees46_next_render = function() {
            if( i < rees46_blocks.length ) {
                rees46_block_render.apply(rees46_blocks.eq(i));
                i++;
            }
        };
        rees46_next_render();
    }
}
_r46.init();
_r46.view();
_r46.search();
_r46.category();

var getRecommend = function () {
    var pageIsReady = setInterval(function() {
        if (typeof jQuery != 'undefined') {
            clearInterval(pageIsReady);
            _r46.recommend();
        }
    }, 100);
}
   
var isREES46InitializationStarted = false;
if ( document.readyState === "complete" ) {
    isREES46InitializationStarted = true;
    getRecommend();
} else if ( document.addEventListener ) {
    document.addEventListener( "DOMContentLoaded", function(){
        if(isREES46InitializationStarted == false) {
            getRecommend();
            isREES46InitializationStarted = true;
        }
    });
    window.addEventListener( "load", function(){
        if(isREES46InitializationStarted == false) {
            getRecommend();
            isREES46InitializationStarted = true;
        }
    });
} else {
    window.attachEvent( "onload", function(){
        if(isREES46InitializationStarted == false) {
            getRecommend();
            isREES46InitializationStarted = true;
        }
    });
}
<?php echo '</script'; ?>
>
<?php }?>
<?php }?><?php }} ?>
