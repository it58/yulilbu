<?php /* Smarty version Smarty-3.1.21, created on 2019-08-05 21:53:53
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_google_analytics/hooks/index/head_scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21341845485d484301f25a85-08382427%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34333a87651fa7dda19d4bfb946f70af4832b6c0' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_google_analytics/hooks/index/head_scripts.post.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '21341845485d484301f25a85-08382427',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
    'addons' => 0,
    'product' => 0,
    'cart' => 0,
    'orders_info' => 0,
    'sd_ga_params' => 0,
    'coverage_user_id' => 0,
    'config' => 0,
    'vendor_tracking_code' => 0,
    'tracking_code' => 0,
    'domains' => 0,
    'domain' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48430205ba25_44168463',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48430205ba25_44168463')) {function content_5d48430205ba25_44168463($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
    <?php $_smarty_tpl->tpl_vars["coverage_user_id"] = new Smarty_variable(sd_ZjhiOTcwYjc0OTc4YTY4N2E4YmEzMGEz(''), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['runtime']->value['vendor_id']!=0) {?>
    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1($_smarty_tpl->tpl_vars['runtime']->value['vendor_id'],$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['cart']->value['products']) {?>

    

    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_OGNhNDA5OTUzOWI3YmU0YjAyZmIzN2Vm($_smarty_tpl->tpl_vars['cart']->value), null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['orders_info']->value&&!$_smarty_tpl->tpl_vars['cart']->value['products']) {?>

    

    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_ZjljYzBlMjY3Zjk4MjczOGY1MDIwMDJh($_smarty_tpl->tpl_vars['orders_info']->value), null, 0);?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['enable_cross_domain_tracking']=='Y') {?>
    <?php $_smarty_tpl->tpl_vars['domains'] = new Smarty_variable(explode(", ",$_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['domain_id']), null, 0);?>
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript" data-no-defer>
(function(i,s,o,g,r,a,m){
    i['GoogleAnalyticsObject']=r;
    i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)
    },i[r].l=1*new Date();
    a=s.createElement(o), m=s.getElementsByTagName(o)[0];
    a.async=1;
    a.src=g;
    m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['tracking_code'], ENT_QUOTES, 'UTF-8');?>
', 'auto');

ga('set', JSON.parse('<?php echo strtr($_smarty_tpl->tpl_vars['sd_ga_params']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'));

ga('require', 'ec');
<?php if ($_smarty_tpl->tpl_vars['coverage_user_id']->value['ga_user_id_law']&&$_smarty_tpl->tpl_vars['coverage_user_id']->value['validator_data']) {?>
    ga('set', 'userId', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['coverage_user_id']->value['validator_data'], ENT_QUOTES, 'UTF-8');?>
');
<?php }?>
ga('send', 'pageview', '<?php echo strtr(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url'],'C','rel'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
<?php if ($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['ga_code']) {?>
    ga('create', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['ga_code'], ENT_QUOTES, 'UTF-8');?>
', 'auto', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
');
    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.require', 'ec');
    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.send', 'pageview', '<?php echo strtr(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url'],'C','rel'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
<?php } elseif ($_smarty_tpl->tpl_vars['vendor_tracking_code']->value) {?>
    <?php  $_smarty_tpl->tpl_vars['tracking_code'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tracking_code']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vendor_tracking_code']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tracking_code']->key => $_smarty_tpl->tpl_vars['tracking_code']->value) {
$_smarty_tpl->tpl_vars['tracking_code']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['tracking_code']->value['ga_code']) {?>
            ga('create', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['ga_code'], ENT_QUOTES, 'UTF-8');?>
', 'auto', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
');
            ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.require', 'ec');
            ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.send', 'pageview', '<?php echo strtr(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url'],'C','rel'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
        <?php }?>
    <?php } ?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['enable_cross_domain_tracking']=='Y') {?>
    ga('require', 'linker');
    ga('linker:autoLink', ['<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_host'], ENT_QUOTES, 'UTF-8');?>
', <?php  $_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['domain']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['domains']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['domain']->key => $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
?>'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
',<?php } ?>]);
<?php }?>
<?php echo '</script'; ?>
>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_google_analytics/hooks/index/head_scripts.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_google_analytics/hooks/index/head_scripts.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
    <?php $_smarty_tpl->tpl_vars["coverage_user_id"] = new Smarty_variable(sd_ZjhiOTcwYjc0OTc4YTY4N2E4YmEzMGEz(''), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['runtime']->value['vendor_id']!=0) {?>
    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1($_smarty_tpl->tpl_vars['runtime']->value['vendor_id'],$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['cart']->value['products']) {?>

    

    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_OGNhNDA5OTUzOWI3YmU0YjAyZmIzN2Vm($_smarty_tpl->tpl_vars['cart']->value), null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['ga_vendors_tracking']=='Y'&&fn_allowed_for("MULTIVENDOR")&&$_smarty_tpl->tpl_vars['orders_info']->value&&!$_smarty_tpl->tpl_vars['cart']->value['products']) {?>

    

    <?php $_smarty_tpl->tpl_vars["vendor_tracking_code"] = new Smarty_variable(sd_ZjljYzBlMjY3Zjk4MjczOGY1MDIwMDJh($_smarty_tpl->tpl_vars['orders_info']->value), null, 0);?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['enable_cross_domain_tracking']=='Y') {?>
    <?php $_smarty_tpl->tpl_vars['domains'] = new Smarty_variable(explode(", ",$_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['domain_id']), null, 0);?>
<?php }?>

<?php echo '<script'; ?>
 type="text/javascript" data-no-defer>
(function(i,s,o,g,r,a,m){
    i['GoogleAnalyticsObject']=r;
    i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)
    },i[r].l=1*new Date();
    a=s.createElement(o), m=s.getElementsByTagName(o)[0];
    a.async=1;
    a.src=g;
    m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['tracking_code'], ENT_QUOTES, 'UTF-8');?>
', 'auto');

ga('set', JSON.parse('<?php echo strtr($_smarty_tpl->tpl_vars['sd_ga_params']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
'));

ga('require', 'ec');
<?php if ($_smarty_tpl->tpl_vars['coverage_user_id']->value['ga_user_id_law']&&$_smarty_tpl->tpl_vars['coverage_user_id']->value['validator_data']) {?>
    ga('set', 'userId', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['coverage_user_id']->value['validator_data'], ENT_QUOTES, 'UTF-8');?>
');
<?php }?>
ga('send', 'pageview', '<?php echo strtr(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url'],'C','rel'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
<?php if ($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['ga_code']) {?>
    ga('create', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['ga_code'], ENT_QUOTES, 'UTF-8');?>
', 'auto', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
');
    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.require', 'ec');
    ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vendor_tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.send', 'pageview', '<?php echo strtr(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url'],'C','rel'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
<?php } elseif ($_smarty_tpl->tpl_vars['vendor_tracking_code']->value) {?>
    <?php  $_smarty_tpl->tpl_vars['tracking_code'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tracking_code']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vendor_tracking_code']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tracking_code']->key => $_smarty_tpl->tpl_vars['tracking_code']->value) {
$_smarty_tpl->tpl_vars['tracking_code']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['tracking_code']->value['ga_code']) {?>
            ga('create', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['ga_code'], ENT_QUOTES, 'UTF-8');?>
', 'auto', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
');
            ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.require', 'ec');
            ga('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tracking_code']->value['tracker'], ENT_QUOTES, 'UTF-8');?>
.send', 'pageview', '<?php echo strtr(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url'],'C','rel'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
        <?php }?>
    <?php } ?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['sd_google_analytics']['enable_cross_domain_tracking']=='Y') {?>
    ga('require', 'linker');
    ga('linker:autoLink', ['<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_host'], ENT_QUOTES, 'UTF-8');?>
', <?php  $_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['domain']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['domains']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['domain']->key => $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
?>'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['domain']->value, ENT_QUOTES, 'UTF-8');?>
',<?php } ?>]);
<?php }?>
<?php echo '</script'; ?>
>
<?php }?><?php }} ?>
