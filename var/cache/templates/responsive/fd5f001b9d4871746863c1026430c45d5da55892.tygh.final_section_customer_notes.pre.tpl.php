<?php /* Smarty version Smarty-3.1.21, created on 2019-08-06 09:40:09
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/hooks/checkout/final_section_customer_notes.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10185114585d48e88941e823-76140085%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fd5f001b9d4871746863c1026430c45d5da55892' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/wallet_system/hooks/checkout/final_section_customer_notes.pre.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '10185114585d48e88941e823-76140085',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'show_wallet' => 0,
    'cart' => 0,
    'config' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d48e88944d913_75745995',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d48e88944d913_75745995')) {function content_5d48e88944d913_75745995($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('user_wallet','available_wallet_cash','applied_wallet_cash','use_wallet','user_wallet','available_wallet_cash','applied_wallet_cash','use_wallet'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (isset($_smarty_tpl->tpl_vars['show_wallet']->value)) {?>
<?php if (isset($_smarty_tpl->tpl_vars['cart']->value['wallet']['used_cash'])&&empty($_smarty_tpl->tpl_vars['cart']->value['total'])) {?>
<div>
	<h3 class="ty-step__title-active clearfix">
	    <span class="ty-step__title-left"><img src="images/wallet/icon.png" width="20"></span>
	    <span class="ty-step__title-txt">&nbsp;&nbsp;<?php echo $_smarty_tpl->__("user_wallet");?>
</span>
	</h3>
    <span style="font-size:18px"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->__("available_wallet_cash");?>
:&nbsp;<b><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['cart']->value['wallet']['current_cash']), 0);?>
</b></span>

    <?php if (isset($_smarty_tpl->tpl_vars['cart']->value['wallet']['used_cash'])) {?>
    
    <span style="color:green">&nbsp;&nbsp;&nbsp;&nbsp;<i class="ty-icon-ok" ></i><?php echo $_smarty_tpl->__("applied_wallet_cash");?>
:&nbsp;<b><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['cart']->value['wallet']['used_cash']), 0);?>
</b>&nbsp;<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['customer_index'], ENT_QUOTES, 'UTF-8');?>
?dispatch=wallet_system.remove_wallet_cash" class="cm-ajax cm-ajax-force cm-ajax-full-render" data-ca-target-id="checkout_*"><i title="Remove" class="ty-icon-cancel-circle" style="color:red; font-size:16px"></i></a></span>
    <?php } else { ?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['customer_index'], ENT_QUOTES, 'UTF-8');?>
?dispatch=wallet_system.apply_wallet_cash" class="ty-btn ty-btn__secondary cm-ajax cm-ajax-force cm-ajax-full-render" data-ca-target-id="checkout_*"><?php echo $_smarty_tpl->__("use_wallet");?>
</a>
    <?php }?>
    <br>
    <br>
</div>
<div class="ty-checkout-buttons ty-checkout-buttons__submit-order"></div>
<?php }?>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wallet_system/hooks/checkout/final_section_customer_notes.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wallet_system/hooks/checkout/final_section_customer_notes.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (isset($_smarty_tpl->tpl_vars['show_wallet']->value)) {?>
<?php if (isset($_smarty_tpl->tpl_vars['cart']->value['wallet']['used_cash'])&&empty($_smarty_tpl->tpl_vars['cart']->value['total'])) {?>
<div>
	<h3 class="ty-step__title-active clearfix">
	    <span class="ty-step__title-left"><img src="images/wallet/icon.png" width="20"></span>
	    <span class="ty-step__title-txt">&nbsp;&nbsp;<?php echo $_smarty_tpl->__("user_wallet");?>
</span>
	</h3>
    <span style="font-size:18px"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->__("available_wallet_cash");?>
:&nbsp;<b><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['cart']->value['wallet']['current_cash']), 0);?>
</b></span>

    <?php if (isset($_smarty_tpl->tpl_vars['cart']->value['wallet']['used_cash'])) {?>
    
    <span style="color:green">&nbsp;&nbsp;&nbsp;&nbsp;<i class="ty-icon-ok" ></i><?php echo $_smarty_tpl->__("applied_wallet_cash");?>
:&nbsp;<b><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['cart']->value['wallet']['used_cash']), 0);?>
</b>&nbsp;<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['customer_index'], ENT_QUOTES, 'UTF-8');?>
?dispatch=wallet_system.remove_wallet_cash" class="cm-ajax cm-ajax-force cm-ajax-full-render" data-ca-target-id="checkout_*"><i title="Remove" class="ty-icon-cancel-circle" style="color:red; font-size:16px"></i></a></span>
    <?php } else { ?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['customer_index'], ENT_QUOTES, 'UTF-8');?>
?dispatch=wallet_system.apply_wallet_cash" class="ty-btn ty-btn__secondary cm-ajax cm-ajax-force cm-ajax-full-render" data-ca-target-id="checkout_*"><?php echo $_smarty_tpl->__("use_wallet");?>
</a>
    <?php }?>
    <br>
    <br>
</div>
<div class="ty-checkout-buttons ty-checkout-buttons__submit-order"></div>
<?php }?>
<?php }
}?><?php }} ?>
