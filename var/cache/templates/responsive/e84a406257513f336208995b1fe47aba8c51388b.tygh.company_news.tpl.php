<?php /* Smarty version Smarty-3.1.21, created on 2019-08-09 19:35:35
         compiled from "/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_follow_vendor/views/follower/components/company_news.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9889168375d4d6897a73b44-19289022%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e84a406257513f336208995b1fe47aba8c51388b' => 
    array (
      0 => '/home/yulibu/public_html/design/themes/responsive/templates/addons/sd_follow_vendor/views/follower/components/company_news.tpl',
      1 => 1565015960,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '9889168375d4d6897a73b44-19289022',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'vendor_news' => 0,
    'one' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5d4d6897a945c2_47686453',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d4d6897a945c2_47686453')) {function content_5d4d6897a945c2_47686453($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/yulibu/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_hook')) include '/home/yulibu/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/yulibu/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('vendors_news','by','no_items','vendors_news','by','no_items'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('class'=>"ty-companies_news-header",'title'=>$_smarty_tpl->__("vendors_news")), 0);?>


<?php if ($_smarty_tpl->tpl_vars['vendor_news']->value) {?>
    <ul>
        <?php  $_smarty_tpl->tpl_vars["one"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["one"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vendor_news']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["one"]->key => $_smarty_tpl->tpl_vars["one"]->value) {
$_smarty_tpl->tpl_vars["one"]->_loop = true;
?>
            <div class="ty-news__item">
                <div class="ty-news__date"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['one']->value['published_on'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>
</div>
                <div class="ty-news__author"><?php echo $_smarty_tpl->__("by");?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['one']->value['company'], ENT_QUOTES, 'UTF-8');?>
</div>
                <div class="ty-news__description">
                    <span><?php echo $_smarty_tpl->__($_smarty_tpl->tpl_vars['one']->value['lang_var'],array("[vendor_name]"=>$_smarty_tpl->tpl_vars['one']->value['company'],"[url]"=>$_smarty_tpl->tpl_vars['one']->value['url']));?>
</span>
                </div>
            </div>
        <?php } ?>
    </ul>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"follower:vendor_news")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"follower:vendor_news"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"follower:vendor_news"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php } else { ?>
    <p class="ty-no-items"><?php echo $_smarty_tpl->__("no_items");?>
</p>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/sd_follow_vendor/views/follower/components/company_news.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/sd_follow_vendor/views/follower/components/company_news.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('class'=>"ty-companies_news-header",'title'=>$_smarty_tpl->__("vendors_news")), 0);?>


<?php if ($_smarty_tpl->tpl_vars['vendor_news']->value) {?>
    <ul>
        <?php  $_smarty_tpl->tpl_vars["one"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["one"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vendor_news']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["one"]->key => $_smarty_tpl->tpl_vars["one"]->value) {
$_smarty_tpl->tpl_vars["one"]->_loop = true;
?>
            <div class="ty-news__item">
                <div class="ty-news__date"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['one']->value['published_on'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>
</div>
                <div class="ty-news__author"><?php echo $_smarty_tpl->__("by");?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['one']->value['company'], ENT_QUOTES, 'UTF-8');?>
</div>
                <div class="ty-news__description">
                    <span><?php echo $_smarty_tpl->__($_smarty_tpl->tpl_vars['one']->value['lang_var'],array("[vendor_name]"=>$_smarty_tpl->tpl_vars['one']->value['company'],"[url]"=>$_smarty_tpl->tpl_vars['one']->value['url']));?>
</span>
                </div>
            </div>
        <?php } ?>
    </ul>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"follower:vendor_news")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"follower:vendor_news"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"follower:vendor_news"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php } else { ?>
    <p class="ty-no-items"><?php echo $_smarty_tpl->__("no_items");?>
</p>
<?php }?>
<?php }?><?php }} ?>
