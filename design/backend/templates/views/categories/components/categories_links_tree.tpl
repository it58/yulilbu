<ul>
{foreach from=$categories_tree item=category}
    {$shift = 14 * $category.level|default:0}
    {capture name="category_subtitle"}
        {if $category.company_categories}
            {$expanded = $category.company_id == $category_data.company_id}
            {$comb_id = "comp_`$category.company_id`"}
            <strong class="row-status">{$category.category}</strong>
        {else}
            {$expanded = $category.category_id|in_array:$runtime.active_category_ids}
            {$comb_id = "cat_`$category.category_id`"}
            {if "MULTIVENDOR"|fn_allowed_for && $category.disabled}
                {$category.category}
            {else}
<<<<<<< HEAD
                <a class="row-status {if $category.status == "N"} manage-root-item-disabled{/if}{if !$category.subcategories} normal{/if}" href="{"categories.update?category_id=`$category.category_id`"|fn_url}"{if !$category.subcategories} style="padding-{$direction}: 14px;"{/if} >{$category.category}</a>
            {/if}
        {/if}
    {/capture}
    <li {if $category.active}class="active"{/if} style="padding-{$direction}: {$shift}px;">
=======
                <a class="row-status {if $category.status == "N"} manage-root-item-disabled{/if}{if !$category.subcategories} normal{/if}" href="{"categories.update?category_id=`$category.category_id`"|fn_url}"{if !$category.subcategories} style="padding-left: 14px;"{/if} >{$category.category}</a>
            {/if}
        {/if}
    {/capture}
    <li {if $category.active}class="active"{/if} style="padding-left: {$shift}px;">
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
    {strip}
        <div class="link">
            {if $category.subcategories}
                <span alt="{__("expand_sublist_of_items")}" title="{__("expand_sublist_of_items")}" id="on_{$comb_id}" class="cm-combination{if $expanded} hidden{/if}" ><span class="icon-caret-right"> </span></span>
                <span alt="{__("collapse_sublist_of_items")}" title="{__("collapse_sublist_of_items")}" id="off_{$comb_id}" class="cm-combination{if !$expanded} hidden{/if}" ><span class="icon-caret-down"> </span></span>
            {/if}
            {$smarty.capture.category_subtitle nofilter}
        </div>
    {/strip}
    </li>
    {if $category.subcategories}
        <li class="{if !$expanded} hidden{/if}" id="{$comb_id}">
            {if $category.subcategories}
<<<<<<< HEAD
            {include file="views/categories/components/categories_links_tree.tpl"
                categories_tree=$category.subcategories
                direction=$direction
            }
=======
            {include file="views/categories/components/categories_links_tree.tpl" categories_tree=$category.subcategories}
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
            {/if}
        <!--{$comb_id}--></li>
    {/if}
{/foreach}
</ul>