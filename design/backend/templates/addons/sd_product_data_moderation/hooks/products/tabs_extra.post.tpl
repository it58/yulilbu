<div id="content_product_data_moderation_changes" class="hidden">
    {script src="js/addons/sd_product_data_moderation/func.js"}
    <form action="{""|fn_url}" method="post" name="product_data_moderation_form">
        <input type="hidden" name="product_id" value="{$product_data.product_id}">

        {if $product_data_moderation && $auth.user_type != 'V'}
            {capture name="disapprove_selected"}
                {include file="addons/sd_product_data_moderation/views/moderation/components/reason_container.tpl" type="declined"}
                <div class="buttons-container">
                    {include file="buttons/save_cancel.tpl" but_text=__("product_data_moderation_disapprove") but_name="dispatch[products.m_decline]" cancel_action="close" but_meta="cm-process-items"}
                </div>
            {/capture}
            {include file="common/popupbox.tpl" id="disapprove_selected" text=__("product_data_moderation_disapprove") content=$smarty.capture.disapprove_selected link_text=__("product_data_moderation_disapprove")}
                <div class="pull-right shift-left">
                    {include file="buttons/button.tpl"  but_role="submit-link" but_name="dispatch[products.m_approve]" but_text=__("product_data_moderation_approve") but_target_form="manage_products_form" but_meta="cm-process-items"}
                    {include file="buttons/button.tpl" but_role="submit-link" but_target_id="content_disapprove_selected" but_text=__("product_data_moderation_disapprove") but_meta="cm-process-items cm-dialog-opener" but_target_form="product_data_moderation_form"}
                </div>
        {/if}

        {if $product_data_moderation}
            <table class="table table-middle">
                <thead>
                    <tr>
                        <th>
                            <div name="plus_minus" id="on_st" alt="{__("expand_collapse_list")}" title="{__("expand_collapse_list")}" class="hand hidden cm-combinations-products icon-caret-right"></div><div name="minus_plus" id="off_st" alt="{__("expand_collapse_list")}" title="{__("expand_collapse_list")}" class="hand cm-combinations-products icon-caret-down"></div>
                        </th>
                        <th class="left" width="5%">{include file="common/check_items.tpl"}</th>
                        <th width="20%">{__("product_name")}</th>
                        <th width="10%">{__("language")}</th>
                        <th width="15%">{__("date")}</th>
                        <th width="10%">&nbsp;</th>
                    </tr>
                </thead>
            <tbody>
                {foreach from=$product_data_moderation item=product key=product_key}
                    <tr>
                        <td width="2%">
                            <span id="on_extra_products_{$product_key}" alt="{__("expand_collapse_list")}" title="{__("expand_collapse_list")}" class="hand hidden cm-combination-products"><span class="icon-caret-right"></span></span>
                            <span id="off_extra_products_{$product_key}" alt="{__("expand_collapse_list")}" title="{__("expand_collapse_list")}" class="hand cm-combination-products"><span class="icon-caret-down"></span></span>
                        </td>
                        <td class="left">
                            <input type="checkbox" name="product_moderation_ids[]" value="{$product.product_id}" class="cm-item" onClick="product_checked('{$product_key}')" id="input_product_{$product_key}"/>
                            <input type="checkbox" name="lang_code[]" value="{$product.lang_code}" class="cm-item hidden" id="input_lang_code_{$product_key}"/>
                        </td>
                        <td>
                            {$product.old_product_name nofilter}
                        </td>
                        <td>
                            <i class="flag flag-{$languages[$product.lang_code].country_code|lower}"></i>{$product.lang_code|strtoupper}
                        </td>
                        <td>
                            {$product.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
                        </td>
                        <td>
                            {if $auth.user_type != 'V'}
                                {capture name="disapprove"}
                                    {include file="addons/sd_product_data_moderation/views/moderation/components/approval_popup.tpl" name="approval_data[`$product_key`]" status="N" product_id=$product.product_id company_id=$product.company_id lang_code=$product.lang_code}
                                    <div class="buttons-container">
                                        {include file="buttons/save_cancel.tpl" but_text=__("product_data_moderation_disapprove") but_name="dispatch[products.products_approval.disapprove.`$product_key`]" cancel_action="close"}
                                    </div>
                                {/capture}

                                <a data-ca-dispatch="dispatch[products.products_approval.approve.{$product_key}]" class="cm-submit icon-thumbs-up cm-tooltip" title="{__("product_data_moderation_approve")}"></a>
                                {include file="addons/sd_product_data_moderation/views/moderation/components/popupbox.tpl" id="disapprove_`$product_key`" text="{__("product_data_moderation_disapprove")} \"`$product.old_product_name`\"" content=$smarty.capture.disapprove link_text="{__("product_data_moderation_disapprove")}" act="edit" link_class="icon-thumbs-down cm-tooltip"}
                            {/if}
                        </td>
                    </tr>
                    <tr id="extra_products_{$product_key}">
                        <td colspan="7">
                            <table class="table table-middle">
                            <thead>
                                    <tr>
                                        <th width="10%">&nbsp;</th>
                                        <th width="45%" class="center">{__("before_changes")}</th>
                                        <th width="45%" class="center">{__("after_changes")}</th>
                                    </tr>
                                </thead>
                            <tbody>
                                {if $product.product_name_diff}
                                    <tr>
                                        <td width="10%">{__("product_name")}</td>
                                        <td width="45%" class="center">{$product.old_product_name nofilter}</td>
                                        <td width="45%" class="center">{$product.new_product_name nofilter}</td>
                                    </tr>
                                {/if}
                                {if $product.product_full_description_diff}
                                    <tr>
                                        <td width="10%">{__("product_full_description")}</td>
                                        <td width="45%" class="sd_product_data_moderation-td_descr"><div class="sd_product_data_moderation-table_descr">{$product.old_product_full_description nofilter}</div></td>
                                        <td width="45%" class="sd_product_data_moderation-td_descr"><div class="sd_product_data_moderation-table_descr">{$product.new_product_full_description nofilter}</div></td>
                                    </tr>
                                {/if}
                            </tbody>
                            </table>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}
    </form>
</div>
