{assign var="method" value=$method|default:"POST"}
{$popup_params = " id=\"opener_`$id`\" data-ca-target-id=\"content_`$id`\""}
{if !$content}
    {$popup_params = "`$popup_params`  data-ca-dialog-title=\"`$text|replace:'"':''`\""}
{/if}
{if ($runtime.action && $runtime.action|fn_check_view_permissions:$method) || (!$runtime.action && $content|fn_check_html_view_permissions:$method)}
    {if $act == "edit"}
        {assign var="_href" value=$href|fn_url}
        {assign var="default_link_text" value=__("edit")}
        {if !$_href|fn_check_view_permissions}
            {assign var="_link_text" value=__("view")}
            {assign var="default_link_text" value=__("view")}
        {/if}
        <a {if $edit_onclick}onclick="{$edit_onclick}"{/if} class="hand {if !$no_icon_link}{if $update_controller == "addons"}icon-cog{/if}{if $icon}{$icon}{/if}{/if} {if !$is_promo}cm-dialog-opener{/if}{if $is_promo}cm-promo-popup{/if} {if $_href && !$is_promo} {$opener_ajax_class|default:'cm-ajax'}{/if}{if $link_class} {$link_class}{/if}" {if $_href && !$is_promo} href="{$_href}"{/if} {$popup_params nofilter} title="{$link_text|default:__("edit")}" {if $drop_left}data-placement="left"{/if}>{if $icon}<i class="{$icon}"></i>{/if}</a>
    {/if}

    {if $content}
    <div class="hidden {if "ULTIMATE"|fn_allowed_for}ufa{/if}" title="{$text}" id="content_{$id}">
        {$content nofilter}
    <!--content_{$id}--></div>
    {/if}

{else}{*
skipped {$text}
*}{/if}
