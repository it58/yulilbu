<div class="form-horizontal">
    <div class="control-group">
        <label class="control-label" for="{$name}">{__("reason")}:</label>
        <div class="controls">
            <input type="hidden" name="{$name}[company_id]" value="{$company_id}" />
            <input type="hidden" name="{$name}[product_id]" value="{$product_id}" />
            <input type="hidden" name="{$name}[status]" value="{$status}" />
            <input type="hidden" name="{$name}[lang_code]" value="{$lang_code}" />
            <textarea name="{$name}[reason_{$status}]" id="{$name}" cols="50" rows="4" class="input-text"></textarea>
        </div>
    </div>
</div>