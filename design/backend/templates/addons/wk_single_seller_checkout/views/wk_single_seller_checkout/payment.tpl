{if $auth.user_type=="A"}
{capture name="mainbox"}
<div class="items-container cm-sortable" data-ca-sortable-table="payments" data-ca-sortable-id-name="payment_id" id="payments_list">
{if $payments}
<table class="table table-middle table-objects">
    <thead>
            <tr>
                <th>{__("payment_method")}</th>
                <th>{__("description")}</th>
                <th>{__("surcharge")}</th>
                <th>{__("status")}</th>
            </tr>
    </thead>
    <tbody>
        {foreach from=$payments item=payment name="pf"}
            <tr>
                <td><a>{$payment.payment}</a></td>
                <td>{$payment.description}</td>
                <td>{$payment.p_surcharge}% + {include file="common/price.tpl" value=$payment.a_surcharge}</td>
                <td>{include file="common/select_popup.tpl" id=$payment.payment_id status=$payment.status hidden="" update_controller="wk_single_seller_checkout" notify=false}</td>
            </tr>
        {/foreach}
    </tbody>
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
<!--payments_list--></div>
{/capture}

{include file="common/mainbox.tpl" title=__("payment_methods") content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}
{/if}