{if $auth.user_type=="A"}
{if $payments AND $addons.wk_single_seller_checkout.enable_payment_action eq 'Y'}
<div id="content_payment_method">
<table class="table">
    <thead>
            <tr>
                <th>{__("payment_method")}</th>
                <th>{__("description")}</th>
                <th>{__("surcharge")}</th>
                <th>{__("status")}</th>
            </tr>
    </thead>
    <tbody>
        {foreach from=$payments item=payment name="pf"}
            <tr {if $payment.status eq 'D'}class="cm-row-status-d"{else}class="cm-row-status-a"{/if}>
                <td>{$payment.payment}</td>
                <td>{$payment.description}</td>
                <td>{$payment.p_surcharge}% + {include file="common/price.tpl" value=$payment.a_surcharge}</td>
                <td>
                    {assign var=restricted_payments value=fn_get_restricted_payment_method_tpl()}
                    {if $payment.payment_id|in_array:$restricted_payments}
                        {if $payment.status eq 'A'}
                        {__("active")}
                        {else}
                        {__("disabled")}
                        {/if}
                    {else}
                    {assign var=temp_id value="`$company_data.company_id`-`$payment.payment_id`"}
                        {include file="common/select_popup.tpl" id=$temp_id status=$payment.status hidden="" update_controller="wk_single_seller_checkout" notify=false}
                    {/if} 
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
</div>
{/if}