{if $navigation.tabs.sd_vacation_mode_for_vendors}
    <div id="content_sd_vacation_mode_for_vendors" class="hidden">
        <div class="control-group">
            <label class="control-label" for="id_sd_vmfv_vendor_in_vacation">{__("sd_vmfv_vendor_in_vacation")}:</label>
            <div class="controls">
                <input type="checkbox" name="company_data[vendor_in_vacation]" id="id_sd_vmfv_vendor_in_vacation" {if $company_data.vendor_in_vacation == 'Y'}checked="checked"{/if} value="Y"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="id_sd_vmfv_timezone_vacation">{__("sd_vmfv_date_timezone")}:</label>
            <div class="controls">
            <select name="company_data[timezone_vacation]" id="id_sd_vmfv_timezone_vacation">
                {foreach from=$sd_vmfv_all_timezone_vacation item="timezone_vacation" key="timezone_value"}
                    <option value="{$timezone_value}" {if isset($company_data.timezone_vacation) && $timezone_value == $company_data.timezone_vacation}selected="selected"{/if}>{$timezone_vacation}</option>
                {/foreach}
            </select>
            {if !isset($company_data.timezone_vacation)}
                <literal>
                    <script>
                    var currenDate = new Date();
                    timezoneOffset = -currenDate.getTimezoneOffset() / 60;
                    $("#id_sd_vmfv_timezone_vacation option[value='"+timezoneOffset+"']").attr("selected","selected");
                    </script>
                </literal>
                <span class="sd_vmfv_inform">
                    {__('sd_vmfv_aute_set_timezone_check')}
                </span>
            {/if}
            </div>
        </div>


        <div id="sd_vmfv_calendars" class ="sd_vmfv_calendars">
        {include file="common/subheader.tpl" title=__('sd_vmfv_weekend')}
            {if $settings.Appearance.calendar_week_format == "sunday_first"}
                {assign var=first_day value=0} 
            {else}
                {assign var=first_day value=1} 
            {/if}
            {if $not_ban_change_history}
                {assign var=currend_date value=1}
                <input type='hidden' name="not_ban_change_history" value="{$not_ban_change_history}">
            {else}
                {assign var=currend_date value=0}
            {/if}

            {foreach from=$sd_vmfv_period_vacation item="month_day_dayweek" key="year"}
                {foreach from=$month_day_dayweek item="day_dayweek" key="month"}
                    <div id="sd_vmfv_calendar_{$year}_{$month}" class="sd_vmfv_calendar">
                        <table class="table_calendar_table">
                            <thead class="thead_calendar_table">
                                <tr>
                                    <th colspan=7>{__("month_name_$month")}&nbsp;{$year}<th>
                                </tr>
                                <tr>
                                {for $i = 0 to 6}
                                    {assign var=dow value=($i + $first_day) % 7} 
                                    <th>{__("weekday_abr_$dow")}</th>
                                {/for}
                                </tr>
                            </thead>
                            <tbody class="tbody_calendar_table">
                                <tr>
                                {$day_of_week = $first_day}
                                {while $day_of_week != $day_dayweek[1]}
                                    <td></td>
                                    {$day_of_week = ($day_of_week + 1) % 7}
                                {/while}
                                {foreach from=$day_dayweek item="dayweek" key="day"}
                                    {if $dayweek == $first_day}
                                        </tr><tr>
                                    {/if}
                                    {if $currend_date==0 && $sd_vmfv_vacation_data['currend_date'][$year][$month][$day]}
                                        {$currend_date = 1}
                                    {/if}
                                    <td> 
                                        <div id="id_cg_{$year}{$month}{$day}" class="control-group {if $currend_date==0} sd_vmfv_control_group_disable {/if}{if $sd_vmfv_vacation_data[$year][$month][$day]} sd_vmfv_control_group_checked {else} sd_vmfv_control_group_unchecked {/if}">
                                            <label class="control-label" for="id_{$year}{$month}{$day}">{$day}</label>
                                            {if $currend_date}
                                            <div class="controls">
                                                <input id="id_{$year}{$month}{$day}" control-group="id_cg_{$year}{$month}{$day}" type="checkbox"  name="dates[{$year}][{$month}][{$day}]" {if $sd_vmfv_vacation_data[$year][$month][$day]} checked="checked" {/if} value="v">
                                            </div>
                                            {/if}
                                        </div>
                                    </td>
                                {/foreach}
                                </tr>
                            </tbody>
                    </table></div>
                {/foreach}
            {/foreach}
        </div>
        
        <div class="control-group">
            <label for="elm_working_hours_from" class="control-label cm-regexp" data-ca-regexp="^([0-1]\d|2[0-3])(:[0-5]\d)$" data-ca-message="{__("the_time_format_must_be_hhmm")}">{__("work_time_start")}:</label>
            <div class="controls">
                <input type="text" name="company_data[working_hours_from]" id="elm_working_hours_from" size="10" value="{$company_data.working_hours_from}" class="input-small" placeholder="{__("hh_mm")}"/>
            </div>
        </div>
        <div class="control-group">
            <label for="elm_working_hours_to" class="control-label cm-regexp" data-ca-regexp="^([0-1]\d|2[0-3])(:[0-5]\d)$" data-ca-message="{__("the_time_format_must_be_hhmm")}">{__("work_time_end")}:</label>
            <div class="controls">
                <input type="text" name="company_data[working_hours_to]" id="elm_working_hours_to" size="10" value="{$company_data.working_hours_to}" class="input-small" placeholder="{__("hh_mm")}" />
            </div>
        </div>
    </div>

    <literal>
        <script>
            $(".sd_vmfv_calendars input[type='checkbox']").click(function () {
                var id_cg = $(this).attr("control-group");
                var elm_id_cg = $("#"+id_cg);
                if ($(this).prop("checked")) {
                    elm_id_cg.addClass("sd_vmfv_control_group_checked");
                    elm_id_cg.removeClass("sd_vmfv_control_group_unchecked");
                } else {
                    elm_id_cg.removeClass("sd_vmfv_control_group_checked");
                    elm_id_cg.addClass("sd_vmfv_control_group_unchecked");
                }
            });
        </script>
    </literal>
{/if}
