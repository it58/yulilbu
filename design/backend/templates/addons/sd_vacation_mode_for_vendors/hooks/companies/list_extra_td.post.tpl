<td class="row-status">
    {if $sd_vmfv_company_vacation[$company.company_id]}
        {if $sd_vmfv_company_vacation[$company.company_id].vendor_in_vacation == 'Y' || $sd_vmfv_company_vacation[$company.company_id].vendor_in_vacation_today == 'Y'}
            {__('sd_vmfv_in_vacation')}
        {else}
            {__('sd_vmfv_not_in_vacation')}
        {/if}
    {else}
        {__('unknown')}
    {/if}
</td>