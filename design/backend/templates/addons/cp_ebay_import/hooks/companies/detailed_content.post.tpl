{if !"ULTIMATE"|fn_allowed_for && !$runtime.company_id}
	{include file="common/subheader.tpl" title=__("allow_ebay_to_vendors") target="#acc_addon_allow_ebay"}

	<div id="acc_addon_allow_ebay" class="collapsed in">
		<div class="control-group">
			<label class="control-label" for="allow_ebay">{__("allow_ebay_to_vendors")}:</label>
			<input type="hidden" name="company_data[allow_ebay]" value="N" />
			<div class="controls">
				<input id="allow_ebay" type="checkbox" name="company_data[allow_ebay]" value="Y" {if $company_data.allow_ebay == "Y"}checked="checked"{/if} />
			</div>
		</div>
	</div>
{/if}