{if !"ULTIMATE"|fn_allowed_for && !$runtime.company_id}
	<li>{btn type="list" text=__("allow_to_use_ebay") dispatch="dispatch[companies.allow_accsess_ebay]" form="companies_form"}</li>
	<li>{btn type="list" text=__("forbid_to_use_ebay") dispatch="dispatch[companies.forbid_accsess_ebay]" form="companies_form"}</li>
{/if}
