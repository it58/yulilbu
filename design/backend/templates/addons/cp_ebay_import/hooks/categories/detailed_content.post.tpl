{if !$hide_title}
	{include file="common/subheader.tpl" title=__("import_from_ebay") target="#acc_addon_ebay_import"}
{/if}
<div id="acc_addon_ebay_import" class="collapsed in">
	<div class="control-group {if $share_dont_hide}cm-no-hide-input{/if}">
		<label class="control-label" for="elm_ebay_category">{__("category_for_ebay_products")}:</label>
		<input type="hidden" name="category_data[cp_ebay_category]" value="N" />
		<input type="hidden" name="category_data[old_ebay_category]" value="{$category_data.cp_ebay_category}" />
		<div class="controls">
			<input id="elm_ebay_category" type="checkbox" name="category_data[cp_ebay_category]" value="Y" {if $category_data.cp_ebay_category == "Y"}checked="checked"{/if} />
		</div>
	</div>
</div>