{capture name="mainbox"}
<div>
	<label for="elm_processing" class="control-label">Processing....</label>
</div>
<div>
	<label for="number_of_get_listings" class="control-label">{__("number_of_get_listings")} : {$count_items}</label>
</div>
{/capture}

{include file="common/mainbox.tpl" title=__("process_of_getting_listings") content=$smarty.capture.mainbox }