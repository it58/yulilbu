{capture name="mainbox"}
<div>
	<label for="elm_processing" class="control-label">Processing....</label>
</div>
<div>
	<label for="number_of_updated_products" class="control-label">{__("number_of_updated_products")} : {$process.updated}</label>
</div>
<div>
	<label for="number_of_new_products" class="control-label">{__("number_of_new_products")} : {$process.new_products}</label>
</div>
<div>
	<label for="number_of_imported_listings" class="control-label">{__("number_of_imported_listings")} : {$process.imported}</label>
</div>
{/capture}

{include file="common/mainbox.tpl" title=__("import_from_ebay") content=$smarty.capture.mainbox }