{if $in_popup}
    <div class="adv-search">
    <div class="group">
{else}
    <div class="sidebar-row">
    <h6>{__("search")}</h6>
{/if}
{if $page_part}
    {assign var="_page_part" value="#`$page_part`"}
{/if}

<form action="{""|fn_url}{$_page_part}" name="{$product_search_form_prefix}search_form" method="get" class="cm-disable-empty {$form_meta}">
<input type="hidden" name="type" value="{$search_type|default:"simple"}" autofocus="autofocus" />
{if $smarty.request.redirect_url}
    <input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}
{if $selected_section != ""}
    <input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
{/if}
<input type="hidden" name="pcode_from_q" value="Y" />

{if $put_request_vars}
    {array_to_fields data=$smarty.request skip=["callback"]}
{/if}

{$extra nofilter}
{capture name="simple_search"}
    <div class="sidebar-field">
        <label>{__("find_results_with")}</label>
        <input type="text" name="q" size="20" value="{$search.q}" />
    </div>
     <div class="sidebar-field">
        <label>{__("country")}</label>
        <input type="text" name="country_code" size="20" value="{$search.country_code}" />
    </div>
{/capture}
{include file="addons/cp_ebay_import/views/cp_ebay_import/components/ebay_advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="products" in_popup=$in_popup}

</form>
{if $in_popup}
    </div></div>
{else}
    </div><hr>
{/if}