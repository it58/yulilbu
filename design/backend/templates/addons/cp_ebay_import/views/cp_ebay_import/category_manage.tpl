
{capture name="mainbox"}
	<form action="{""|fn_url}" method="post" name="ebay_categories_form" id="ebay_categories_form" class="form-horizontal form-edit">
		<div class="control-group cm-required" >
			<label class="control-label" for="ebay_country_for_cat">{__("ebay_contries_cat")}:</label>
			<div class="controls">
				<select class="span5" name="ebay_country_get_cat" id="ebay_country_for_cat" >
					<option value="0">US (http://www.ebay.com)</option>
					<option value="3">UK (http://www.ebay.ch)</option>
					<option value="15">Australia (http://www.ebay.com.au)</option>
					<option value="16">Austria (http://www.ebay.at)</option>
					<option value="123">Belgium Dutch (http://www.ebay.be)</option>
					<option value="23">Belgium French (http://www.ebay.be)</option>
					<option value="2">Canada (http://www.ebay.ca)</option>
					<option value="210">Canada French (http://www.ebay.ca)</option>
					<option value="71">France (http://www.ebay.fr)</option>
					<option value="77">Germany (http://www.ebay.de)</option>
					<option value="201">HongKong (http://www.ebay.com.hk)</option>
					<option value="203">India (http://www.ebay.in)</option>
					<option value="205">Ireland (http://www.ebay.ie)</option>
					<option value="101">Italy (http://www.ebay.it)</option>
					<option value="207">Malaysia (http://www.ebay.com.my)</option>
					<option value="146">Netherlands (http://www.ebay.nl)</option>
					<option value="211">Philippines (http://www.ebay.ph)</option>
					<option value="212">Poland (http://www.ebay.pl)</option>
					<option value="215">Russia (http://www.ebay.com)</option>
					<option value="216">Singapore (http://www.ebay.com.sg)</option>
					<option value="186">Spain (http://www.ebay.es)</option>
					<option value="193">Switzerland (http://www.ebay.co.uk)</option>
				</select>
			</div>
		</div>
		<div class="control-group">
				<label class="control-label" for="ebay_dev_key">{__("ebay_dev_key")}{include file="common/tooltip.tpl" tooltip={__("ebay_dev_key_descrip")} params="ty-subheader__tooltip"}:</label>
				<div class="controls">
					<input type="text" name="ebay_data[ebay_dev_key]" id="ebay_dev_key" size="25" value="{$cp_ebay_data_key.ebay_dev_key}" class="input-short" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="ebay_api_key">{__("ebay_api_key")}{include file="common/tooltip.tpl" tooltip={__("ebay_api_key_descrip")} params="ty-subheader__tooltip"}:</label>
				<div class="controls">
					<input type="text" name="ebay_data[ebay_api_key]" id="ebay_api_key" size="25" value="{$cp_ebay_data_key.ebay_api_key}" class="input-short" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="ebay_cert_key">{__("ebay_cert_key")}{include file="common/tooltip.tpl" tooltip={__("ebay_cert_key_descrip")} params="ty-subheader__tooltip"}:</label>
				<div class="controls">
					<input type="text" name="ebay_data[ebay_cert_key]" id="ebay_cert_key" size="25" value="{$cp_ebay_data_key.ebay_cert_key}" class="input-short" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="ebay_user_token">{__("ebay_user_token")}{include file="common/tooltip.tpl" tooltip={__("ebay_user_token_descrip")} params="ty-subheader__tooltip"}:</label>
				<div class="controls">
					<input type="text" name="ebay_data[ebay_user_token]" id="ebay_user_token" size="25" value="{$cp_ebay_data_key.ebay_user_token}" class="input-large" />
				</div>
			</div>
		<div class="control-group">
			<label class="control-label" for="ebay_category_lvl">{__("ebay_category_lvl")}{include file="common/tooltip.tpl" tooltip={__("ebay_category_lvl_descrip")} params="ty-subheader__tooltip"}:</label>
			<div class="controls">
				<input type="text" name="lvl_limit" id="ebay_category_lvl" size="25" value="0" class="input-small" />
			</div>
		</div>
		<div class="buttons-container" id="get_categories_but">
			<div class="controls">
				{include file="buttons/button.tpl" but_text=__("get_ebay_categories") but_meta="cm-confirm cm-post cp-get-cats-button" but_name="dispatch[cp_ebay_import.get_categories]" but_role="submit" but_target_form="ebay_categories_form"}&nbsp;
				{include file="buttons/button.tpl" but_text=__("delete_ebay_categories") but_meta="cm-confirm cm-post cp-get-not-req-button" but_name="dispatch[cp_ebay_import.delete_ebay_categories]" but_role="submit" but_target_form="ebay_categories_form"}
			</div>
		</div><br />
		<div class="buttons-container">
			<label class="control-label" for="ebay_global_layout">{__("global_layout")}</label>
			<div class="controls">
				<select name="global_mapping" id="ebay_global_layout">
					<option value="0">- {__("category_unmapped")} -</option>
					{foreach from=0|fn_get_plain_categories_tree:false item="cat" name="categories"}
						<option value="{$cat.category_id}" {if $cat.disabled}disabled="disabled"{/if}>{$cat.category|escape|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;" nofilter}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div class="buttons-container" id="global_mapping_but">
			<div class="controls">
				{include file="buttons/button.tpl" but_text=__("global_mapping") but_meta="cm-post cp-get-not-req-button" but_name="dispatch[cp_ebay_import.global_mapping]" but_role="submit" but_target_form="ebay_categories_form"}
			</div>
		</div>
		{include file="common/pagination.tpl" save_current_page=true save_current_url=true}
		{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
		{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
		{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}
		{if $ebay_data}
			<table class="table table-middle">
			<thead>
			<tr>
				<th width="1%">
					{include file="common/check_items.tpl"}
				</th>
				<th width="2%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=ebay_cat_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("ebay_category_name")}{if $search.sort_by == "ebay_cat_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="45%" class="nowrap left">
					<a class="cm-ajax" href="{"`$c_url`&sort_by=ebay_cat_path&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("ebay_category_path")}{if $search.sort_by == "ebay_cat_path"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="2%" class="nowrap left">
					<a class="cm-ajax" href="{"`$c_url`&sort_by=country_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("country")}{if $search.sort_by == "country_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="13%" class="nowrap center">
					<a class="cm-ajax" href="{"`$c_url`&sort_by=category_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("cart_category_path")}{if $search.sort_by == "category_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
			</tr>
			</thead>
			{foreach from=$ebay_data item=ebay_cat}
			<tr>
				<td>
					<input type="hidden" name="ebay_country[{$ebay_cat.ebay_category_id}]" value="{$ebay_cat.country_name}" />
					<input name="ebay_category_ids[]" type="checkbox" value="{$ebay_cat.ebay_category_id}" {if $ebay_cat.ebay_category_id == "Y"}checked="checked"{/if} class="cm-item" /></td>
				<td class="left">
					<span>{$ebay_cat.ebay_cat_name}</span>
				</td>
				<td class="left">
					<span>{$ebay_cat.ebay_cat_path}</span>
				</td>
				<td class="left">
					<span>{$ebay_cat.country_name}</span>
				</td>
				<td class="right">
					<span>
						<select name="link_with_cat[{$ebay_cat.ebay_category_id}]" id="elm_category_parent_id">
							<option value="0" {if $ebay_cat.category_id == "0"}selected="selected"{/if}>- {__("category_unmapped")} -</option>
							{foreach from=0|fn_get_plain_categories_tree:false item="cat" name="categories"}
								<option value="{$cat.category_id}" {if $cat.disabled}disabled="disabled"{/if} {if $ebay_cat.category_id == $cat.category_id}selected="selected"{/if}>{$cat.category|escape|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;" nofilter}</option>
							{/foreach}
						</select>
					</span>
				</td>   
			</tr>
			{/foreach}
			</table>
		{else}
			<p class="no-items">{__("no_data")}</p>
		{/if}
		{include file="common/pagination.tpl"}
		{if $ebay_data}
			{capture name="buttons"}
				{include file="buttons/save.tpl" but_meta="cp-get-not-req-button" but_name="dispatch[cp_ebay_import.category_manage]" but_role="submit-button" but_target_form="ebay_categories_form"}
			{/capture}
		{/if}
	</form>
{/capture}
{capture name="sidebar"}
	{include file="addons/cp_ebay_import/views/cp_ebay_import/components/ebay_products_search.tpl" dispatch="cp_ebay_import.category_manage" view_type="products"}
{/capture}

<script language="javascript">
	$.ceEvent('on', 'ce.commoninit', function(context) {
		context.find('.cp-get-cats-button').click(function() {
			$('label[for=ebay_dev_key]').addClass('cm-required');
			$('label[for=ebay_api_key]').addClass('cm-required');
			$('label[for=ebay_cert_key]').addClass('cm-required');
			$('label[for=ebay_user_token]').addClass('cm-required');
		});
		context.find('.cp-get-not-req-button').click(function() {
			$('label[for=ebay_dev_key]').removeClass('cm-required');
			$('label[for=ebay_api_key]').removeClass('cm-required');
			$('label[for=ebay_cert_key]').removeClass('cm-required');
			$('label[for=ebay_user_token]').removeClass('cm-required');
		});
	});
</script>

{include file="common/mainbox.tpl" title=__("ebay_category_manage") content=$smarty.capture.mainbox tools=$smarty.capture.tools buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar}