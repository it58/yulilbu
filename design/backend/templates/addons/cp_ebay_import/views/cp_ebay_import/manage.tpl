
{capture name="mainbox"}
{if $ebay_data.company_id || !"MULTIVENDOR"|fn_allowed_for}
	<form action="{""|fn_url}" method="post" name="ebay_manage_form" id="ebay_manage_form" class="form-horizontal form-edit">
		<input type="hidden" name="ebay_data[company_id]" value="{$ebay_data.company_id}" />
			<div id="content_details">
				
				<div class="control-group">
					<label class="control-label" for="ebay_dev_key">{__("ebay_dev_key")}{include file="common/tooltip.tpl" tooltip={__("ebay_dev_key_descrip")} params="ty-subheader__tooltip"}:</label>
					<div class="controls">
						<input type="text" name="ebay_data[ebay_dev_key]" id="ebay_dev_key" size="25" value="{$cp_ebay_data_key.ebay_dev_key}" class="input-short" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="ebay_api_key">{__("ebay_api_key")}{include file="common/tooltip.tpl" tooltip={__("ebay_api_key_descrip")} params="ty-subheader__tooltip"}:</label>
					<div class="controls">
						<input type="text" name="ebay_data[ebay_api_key]" id="ebay_api_key" size="25" value="{$cp_ebay_data_key.ebay_api_key}" class="input-short" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="ebay_cert_key">{__("ebay_cert_key")}{include file="common/tooltip.tpl" tooltip={__("ebay_cert_key_descrip")} params="ty-subheader__tooltip"}:</label>
					<div class="controls">
						<input type="text" name="ebay_data[ebay_cert_key]" id="ebay_cert_key" size="25" value="{$cp_ebay_data_key.ebay_cert_key}" class="input-short" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="ebay_user_token">{__("ebay_user_token")}{include file="common/tooltip.tpl" tooltip={__("ebay_user_token_descrip")} params="ty-subheader__tooltip"}:</label>
					<div class="controls">
						<input type="text" name="ebay_data[ebay_user_token]" id="ebay_user_token" size="25" value="{$cp_ebay_data_key.ebay_user_token}" class="input-large" />
					</div>
				</div>
				{if $ebay_data.allow_ebay && $ebay_data.allow_ebay == "Y"}
					<span class="control-group">
						<div class="controls">
							<span>{__("if_you_want_to_get_listings_ebay")}</span><br />
						</div>
					</span>
					<div class="control-group">
						<label class="control-label" for="elm_date_holder_from">{__("ebay_start_time")}:</label>
						<div class="controls">
						<input type="hidden" name="ebay_data[from_date]" value="0" />
						{include file="common/calendar.tpl" date_id="elm_date_holder_from" date_name="ebay_data[from_date]" date_val=$ebay_data.from_date|default:$smarty.const.TIME start_year=$settings.Company.company_start_year extra=$smarty.capture.calendar_disable}
						</div>
					</div>
				
					<div class="control-group">
						<label class="control-label" for="elm_date_holder_to">{__("ebay_end_time")}:</label>
						<div class="controls">
						<input type="hidden" name="ebay_data[to_date]" value="0" />
						{include file="common/calendar.tpl" date_id="elm_date_holder_to" date_name="ebay_data[to_date]" date_val=$ebay_data.to_date|default:$smarty.const.TIME start_year=$settings.Company.company_start_year extra=$smarty.capture.calendar_disable}
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="ebay_seller">{__("ebay_seller_to_import")}{include file="common/tooltip.tpl" tooltip={__("ebay_seller_descrip")} params="ty-subheader__tooltip"}:</label>
						<div class="controls">
							<input type="text" name="ebay_data[ebay_seller]" id="ebay_seller" size="25" value="" class="input-short" />
						</div>
					</div>
					<div class="control-group cm-required" >
						<label class="control-label" for="ebay_country_for_cat">{__("ebay_contries_cat")}:</label>
						<div class="controls">
							<select class="span5" name="ebay_data[ebay_country]" id="ebay_country_for_cat" >
								<option value="0">US (http://www.ebay.com)</option>
								<option value="3">UK (http://www.ebay.ch)</option>
								<option value="15">Australia (http://www.ebay.com.au)</option>
								<option value="16">Austria (http://www.ebay.at)</option>
								<option value="123">Belgium Dutch (http://www.ebay.be)</option>
								<option value="23">Belgium French (http://www.ebay.be)</option>
								<option value="2">Canada (http://www.ebay.ca)</option>
								<option value="210">Canada French (http://www.ebay.ca)</option>
								<option value="71">France (http://www.ebay.fr)</option>
								<option value="77">Germany (http://www.ebay.de)</option>
								<option value="201">HongKong (http://www.ebay.com.hk)</option>
								<option value="203">India (http://www.ebay.in)</option>
								<option value="205">Ireland (http://www.ebay.ie)</option>
								<option value="101">Italy (http://www.ebay.it)</option>
								<option value="207">Malaysia (http://www.ebay.com.my)</option>
								<option value="146">Netherlands (http://www.ebay.nl)</option>
								<option value="211">Philippines (http://www.ebay.ph)</option>
								<option value="212">Poland (http://www.ebay.pl)</option>
								<option value="215">Russia (http://www.ebay.com)</option>
								<option value="216">Singapore (http://www.ebay.com.sg)</option>
								<option value="186">Spain (http://www.ebay.es)</option>
								<option value="193">Switzerland (http://www.ebay.co.uk)</option>
							</select>
						</div>
					</div>
					<div class="buttons-container" id="get_listings_but">
						<div class="controls">
							{include file="buttons/button.tpl" but_text=__("get_ebay_listings") but_meta="cm-confirm cm-post cp-get-listings-button" but_name="dispatch[cp_ebay_import.get_listings]" but_role="submit" but_target_form="ebay_manage_form"}
						</div>
					</div>
					<div class="control-group" >
						<label class="control-label" for="ebay_seller_for_delete">{__("ebay_sellers_listings")}:</label>
						<div class="controls">
							<select class="span5" name="ebay_seller_for_delete" id="ebay_seller_for_delete" >
							{foreach from=$ebay_data.ebay_sellers key = "k" item = "seller"} 
								<option value="{$seller}" {if $seller == $ebay_data.ebay_sellers} selected="selected"{/if}>{$seller}</option>
							{/foreach}
							</select>
						</div>
					</div>
					<div class="buttons-container" id="delete_listings_but">
						<div class="controls">
							{include file="buttons/button.tpl" but_text=__("delete_ebay_listings") but_meta="cm-confirm cm-post cp-get-not-req-button" but_name="dispatch[cp_ebay_import.delete_seller_listings]" but_role="submit" but_target_form="ebay_manage_form"}
						</div>
					</div>
					<script language="javascript">
						$.ceEvent('on', 'ce.commoninit', function(context) {
							context.find('.cp-get-listings-button').click(function() {
								$('label[for=ebay_dev_key]').addClass('cm-required');
								$('label[for=ebay_api_key]').addClass('cm-required');
								$('label[for=ebay_cert_key]').addClass('cm-required');
								$('label[for=ebay_user_token]').addClass('cm-required');
							});
							context.find('.cp-get-not-req-button').click(function() {
								$('label[for=ebay_dev_key]').removeClass('cm-required');
								$('label[for=ebay_api_key]').removeClass('cm-required');
								$('label[for=ebay_cert_key]').removeClass('cm-required');
								$('label[for=ebay_user_token]').removeClass('cm-required');
							});
							var  ebay_seller = $('#ebay_seller').val();
							if (!ebay_seller) {
								$('#get_listings_but').hide();
							}
							context.find('#ebay_seller').on('keyup', function() {
								var check_length = $('#ebay_seller').val();
								var amount = check_length.length;
								if (amount > 0) {
									$('#get_listings_but').show();
								} else {
									$('#get_listings_but').hide();
								}
							});
						});
					</script>
				{/if}
			</div>
			{*
		{capture name="buttons"}
			{include file="buttons/save.tpl" but_name="dispatch[cp_ebay_import.manage]" but_meta="cp-get-not-req-button" but_role="submit-button" but_target_form="ebay_manage_form"}
		{/capture}
		*}
	</form>
	{if $ebay_data.allow_ebay && $ebay_data.allow_ebay == "Y"}
		<form action="{""|fn_url}" method="post" name="ebay_listings_form" id="ebay_listings_form" class="form-horizontal form-edit">
			<input type="hidden" name="company_id" value="{$ebay_data.company_id}" />
			{include file="common/pagination.tpl" save_current_page=true save_current_url=true}
			{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
			{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
			{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}
			{if $all_listings}
				<table class="table table-middle">
				<thead>
				<tr>
					<th width="1%">
						{include file="common/check_items.tpl"}
						<a class="cm-ajax" href="{"`$c_url`&sort_by=select_list&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("ebay_sort")}{if $search.sort_by == "select_list"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
					</th>
					<th width="2%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=ebay_sellers&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("ebay_seller_to_import")}{if $search.sort_by == "ebay_sellers"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
					<th width="25%" class="nowrap center">
						<a class="cm-ajax" href="{"`$c_url`&sort_by=product&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("product")}{if $search.sort_by == "product"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
					<th width="1%" class="nowrap left">
					<a class="cm-ajax" href="{"`$c_url`&sort_by=country_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("country")}{if $search.sort_by == "country_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
					<th width="1%" class="nowrap center">
						<a class="cm-ajax" href="{"`$c_url`&sort_by=import_status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("import_status")}{if $search.sort_by == "import_status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
					<th width="2%" class="center"><a class="cm-ajax" href="{"`$c_url`&sort_by=price&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("price")}{if $search.sort_by == "price"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
					<th width="2%" class="center nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=amount&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("quantity")}{if $search.sort_by == "amount"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				</tr>
				</thead>
				{foreach from=$all_listings item=listing}
				<tr>
					<td>
						<input type="hidden" name="ebay_country[{$listing.ebay_id}]" value="{$listing.country_name}" />
						<input type="hidden" name="ebay_listing_not_sel[]" value="{$listing.ebay_id}" />
						<input name="ebay_listing_ids[]" type="checkbox" value="{$listing.ebay_id}" {if $listing.select_list == "Y"}checked="checked"{/if} class="cm-item" /></td>
					<td class="left">
						<span>{$listing.ebay_seller}</span>
					</td>
					<td>
						<a class="row-status" href="{$listing.ebay_url}">{$listing.product|truncate:70 nofilter}</a>
					</td>
					<td class="left">
						<span>{$listing.country_name}</span>
					</td>
					<td class="center">
						<span>{$listing.import_status}</span>
					</td>   
					<td class="center">
						<span>{include file="common/price.tpl" value=$listing.price span_id="price_`$listing.ebay_id`" class="price nowrap"}</span>
					</td>
					<td class="center">
						<span>{$listing.amount}</span>
					</td>
				</tr>
				{/foreach}
				</table>
			{else}
				<p class="no-items">{__("no_data")}</p>
			{/if}
			{capture name="adv_buttons"}
				{capture name="tools_list"}
                    {if $ebay_data.count_queue && $ebay_data.count_queue > 0}
                        {assign var="text" value="{__("import_selected_from_ebay")} (`$ebay_data.count_queue`)"}
                    {else}
                        {assign var="text" value=__("import_selected_from_ebay")}
                    {/if}
					{*
					<li>{btn type="list" text=$text href="cp_ebay_import.import_from_ebay?company_id=`$ebay_data.company_id`"} </li>
					*}
					<li>{btn type="list" text=$text dispatch="dispatch[cp_ebay_import.first_import_from_ebay]" id="go_import_link" form="ebay_manage_form"} </li>
					<li>{btn type="list" text=__("save_selected_listings") dispatch="dispatch[cp_ebay_import.save_selected]" form="ebay_listings_form"}</li>
					<li>{btn type="list" text=__("unselect_all_selected_listings") dispatch="dispatch[cp_ebay_import.unselec_selected]" form="ebay_listings_form"}</li>
					<li>{btn type="list" text=__("add_all_to_import_queue") href="cp_ebay_import.add_all_to_queue?company_id=`$ebay_data.company_id`&status=Y"}</li>
					<li>{btn type="list" text=__("remove_all_from_import_queue") href="cp_ebay_import.add_all_to_queue?company_id=`$ebay_data.company_id`&status=N"}</li>
				{/capture}
				{dropdown content=$smarty.capture.tools_list}
			{/capture}
			{include file="common/pagination.tpl"}
		</form>
		<script language="javascript">
			$.ceEvent('on', 'ce.commoninit', function(context) {
				context.find('#go_import_link').click(function() {
					$('label[for=ebay_dev_key]').addClass('cm-required');
					$('label[for=ebay_api_key]').addClass('cm-required');
					$('label[for=ebay_cert_key]').addClass('cm-required');
					$('label[for=ebay_user_token]').addClass('cm-required');
				});
			});
		</script>
	{/if}
{else}
	{if !$runtime.forced_company_id} 
		{include file="common/select_company.tpl" hide_title=true select_id="company_select" assign="mb"}
		{$smarty.capture.mainbox nofilter}
	{/if}
	<p class="no-items">{__("no_data")}</p>
{/if}
{/capture}
{if $ebay_data.company_id}
	{capture name="sidebar"}
		{include file="addons/cp_ebay_import/views/cp_ebay_import/components/ebay_products_search.tpl" dispatch="cp_ebay_import.manage" view_type="products"}
	{/capture}
{/if}
{include file="common/mainbox.tpl" title=__("import_from_ebay") content=$smarty.capture.mainbox tools=$smarty.capture.tools buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar}