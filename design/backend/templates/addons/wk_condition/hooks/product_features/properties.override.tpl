
{if $is_group || $feature.feature_type == "ProductFeatures::GROUP"|enum}
  <div class="control-group">
            <label class="control-label" for="elm_feature_show_on_product_{$id}">{__("feature_show_on_product")}</label>
            <div class="controls">
            <input type="hidden" name="feature_data[show_on_product]" value="N" />
            <input id="elm_feature_show_on_product_{$id}" type="checkbox" name="feature_data[show_on_product]" value="Y"  data-ca-display-id="OnCatalog" {if $feature.show_on_product == "Y"}checked="checked"{/if} {if $feature.parent_id && $group_features[$feature.parent_id].show_on_product == "Y"}disabled="disabled"{/if} />
            </div>
        </div>
{/if}        