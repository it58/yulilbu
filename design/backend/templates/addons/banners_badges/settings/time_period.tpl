{if $badges_sales_data}
	{assign var="start_date" value="{$badges_sales_data['start_date']}"}
	{assign var="end_date" value="{$badges_sales_data['end_date']}"}	
{else}
	{assign var="start_date" value="$smarty.const.TIME"}
	{assign var="end_date" value="$smarty.const.TIME"}
{/if}

<div class="control-group setting-wide">
    <label class="control-label">{__("sales_period")}{include file="common/tooltip.tpl" tooltip={__("wk_sales_period_tooltip")}}:</label>
    <div class="controls">
    {include file="common/calendar.tpl" date_name="badges_sales_period[start_date]" date_val=$start_date date_id="start_date"} - 
    {include file="common/calendar.tpl" date_name="badges_sales_period[end_date]" date_val=$end_date date_id="end_date"}
    </div>
</div>