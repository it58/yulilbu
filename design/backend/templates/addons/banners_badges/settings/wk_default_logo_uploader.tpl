
<div class="control-group hidden">
    <label class="control-label" for="elm_default_vendor_logo">{__("default_vendor_logo")}{include file="common/tooltip.tpl" tooltip={__("wk_default_vendor_logo_tooltip")}}:</label>
    <div class="controls">
        {include file="common/attach_images.tpl" image_name="default_vendor_logo" image_object_type="default_vendor_logo" image_pair=$default_vendor_logo_data no_thumbnail=true}
    </div>
</div>
