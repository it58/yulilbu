<div class="control-group control-group1">
      <label class="control-label for="elm_review_mail_text_guide"">{__("user_guide")}:{include file="common/tooltip.tpl" tooltip={__("user_guide_tooltip")}}</label>
          <div class="controls">            
              <textarea readonly id="elm_review_mail_text_guide" name="product_data[g_review_mail_text_guide]" rows="10" cols="55" class="input-large">{__("wk_banners_badges_user_guide")}
                 </textarea>
          </div>
      </div>  
<div id="wk_badges_vendor_congrates">
    <div class="control-group control-group1">
      <label class="control-label">{__("wk_badges_vendor_congrates_sub")}:{include file="common/tooltip.tpl" tooltip={__("wk_badges_vendor_congrates_sub_tooltip")}}</label>  
      <div class="controls">
        <input name="wk_banners_badges[upload_mail_sub]" value="{$wk_banners_badges_mail_settings_data.upload_mail_sub}"/>
      </div>
    </div> 
    <div class="control-group control-group1">
      <label class="control-label">{__("wk_badges_vendor_congrates_mail_text")}:{include file="common/tooltip.tpl" tooltip={__("wk_badges_vendor_congrates_mail_text_tooltip")}}</label>  
      <div class="controls">
        <textarea cols="55" rows="20" name="wk_banners_badges[upload_mail_text]" class="cm-wysiwyg">{if !empty($wk_banners_badges_mail_settings_data.upload_mail_text)}{$wk_banners_badges_mail_settings_data.upload_mail_text}{/if}
        </textarea>
        <p>{include file="buttons/button.tpl" but_text=__("preview") but_name="dispatch[seller_badges.preview_html_upload_request]" but_meta="cm-new-window"}</p>
      </div>
    </div> 
</div>