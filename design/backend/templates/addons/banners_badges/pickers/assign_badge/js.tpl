{if $badge_id == "0"}
    {assign var="badge_name" value=$default_name}
{else}
    {assign var="badge" value=$badge_name}
{/if}
<tr {if !$clone}id="{$holder}_{$badge_id}" {/if}class="cm-js-item{if $clone} cm-clone hidden{/if}">

    <td>
        {if $hidden_field}<input type="hidden" name="{$input_name}[]" value="{$badge_id}" size="3" class="input-micro"{if $clone} disabled="disabled"{/if} />{/if}
        <a href="{"seller_badges.update?id={$badge_id}"|fn_url}">{$badge_name}</a>    
    </td>
   
    <td>
        {capture name="tools_list"}           
            {if !$hide_delete_button && !$view_only}
          
                <li><a onclick="Tygh.$.cePicker('delete_js_item', '{$holder}', '{$badge_id}', 'b'); return false;">{__("delete")}</a></li>
                
            {/if}
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>