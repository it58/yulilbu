<div id="content_badges">
{if $badges_assign_list}
<div class="table-wrapper">
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th class="nowrap hidden" ><a href="{"`$c_url`&sort_by=priority&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("company_id")}{if $search.sort_by == "priority"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="20%">{__("badges_image")}</th> 
    <th width="20%">{__("badges_name")}</th>
    <th width="20%">{__("assign_on")}</th>
    <th>&nbsp;</th>
</tr>
</thead>
{foreach from=$badges_assign_list item=badges_data}
<tr class="cm-row-status-{$badges_data.status|lower}">
    <td class="hidden">
    	<input type="text" name="badges_data[company_id]" value="{$badges_data.company_id}" class="input-mini input-hidden">
    </td>
    <td >
       {include file="common/image.tpl" image=$badges_data.main_pair.icon image_id=$badges_data.main_pair.image_id image_width=50}
    </td>
    <td>
        {if $auth.user_type == 'A'}
            <a class="row-status" href="{"seller_badges.update?id=`$badges_data.badge_id`"|fn_url}">{$badges_data.badge_name|truncate:40 nofilter}</a>
        {else}
            {$badges_data.badge_name|truncate:40 nofilter}
        {/if}
    </td>
    <td>
        {$badges_data.assign_time|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
    </td>
    <td  class="nowrap">
        <div class="hidden-tools">
			{capture name="tools_list"}
				<li>{btn type="list" text=__("remove") class="cm-confirm cm-post" href="companies.b_delete?id=`$badges_data.id`&company_id=`$id`"}</li>
            {/capture}
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
</div>