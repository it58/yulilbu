{include file="views/profiles/components/profiles_scripts.tpl"}

{capture name="mainbox"}

	{capture name="tabsbox"}

		{** /Item menu section **}
		<form class="form-horizontal form-edit {$form_class} {if !fn_check_view_permissions("seller_badges.update", "POST")}cm-hide-inputs{/if} {if !$id}cm-comet cm-disable-check-changes{/if}" action="{""|fn_url}" method="post" id="seller_badges_update_form" enctype="multipart/form-data" name="seller_badges_update_form"> {* company update form *}
			{* class=""*}
			<input type="hidden" name="fake" value="1" />
			<input type="hidden" name="selected_section" id="selected_section" value="{$smarty.request.selected_section}" />
			<input type="hidden" name="id" value="{$id}" />
			{** General info section **}
			<div id="content_detailed" class="hidden"> {* content detailed *}
				{include file="common/subheader.tpl" title=__("seller_badges_information") target="#seller_badges_general_info"}
					<div id="seller_badges_general_info">
					<fieldset>
						<div class="control-group">
						 	{include file="views/companies/components/company_field.tpl" name="product_data[company_id]" id="product_data_company_id" selected=$product_data.company_id tooltip=$companies_tip js_action=$js_action}
						</div>
						<div class="control-group">
							{include file="addons/banners_badges/pickers/assign_badge/picker.tpl" data_id="badge_share" input_name="seller_badges_data[badge_id]" view_mode="list" multiple=true show_add_button=true hidden_field=true no_item_text=$no_item_text}
						</div>
					</fieldset>
					</div>
			</div>
			{* /content detailed *}
			{** /General info section **}
		</form>
		{* /product update form *}
	{/capture}
	{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name="companies" active_tab=$smarty.request.selected_section track=true}

{/capture}
{** Form submit section **}
{capture name="buttons"}   
	{include file="buttons/button.tpl" but_text=__("assign") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[assign_badge.add]" but_target_form="seller_badges_update_form"}
{/capture}
{** /Form submit section **}
	{include file="common/mainbox.tpl" title=__("assign_badges") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}

