{if $in_popup}
    <div class="adv-search">
    <div class="group">
{else}
    <div class="sidebar-row">
    <h6>{__("search")}</h6>
{/if}
<form name="user_search_form" action="{""|fn_url}" method="get" class="{$form_meta}">

{if $smarty.request.redirect_url}
<input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}

{if $selected_section != ""}
<input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
{/if}

{if $search.user_type}
<input type="hidden" name="user_type" value="{$search.user_type}" />
{/if}

{if $put_request_vars}
{foreach from=$smarty.request key="k" item="v"}
{if $v && $k != "callback"}
<input type="hidden" name="{$k}" value="{$v}" />
{/if}
{/foreach}
{/if}

{capture name="simple_search"}
{$extra nofilter}

<div class="sidebar-field">
    <label for="elm_name">{__("badge_name")}</label>
    <div class="break">
        <input type="text" name="badge_name" id="elm_name" value="{$search.badge_name}" />
    </div>
</div>
<div class="sidebar-field">
    <label for="elm_name">{__("company_name")}</label>
    <div class="break">
        <input type="text" name="company_name" id="elm_name" value="{$search.company_name}" />
    </div>
</div>
{/capture}



{include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="users" in_popup=$in_popup}

</form>

{if $in_popup}
</div></div>
{else}
</div><hr>
{/if}
<style>
#adv_search_opener
{
	display: none;
}
</style>