{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="badges_log_form" class="form-horizontal">
{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}
{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}
{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{if $badges_assign_list}
<div class="table-responsive-wrapper">
<table width="100%" class="table table-middle table-responsive">
<thead>
<tr>
    <th width="1%">
        {include file="common/check_items.tpl"}
    </th>
    <th class="nowrap hidden"><a href="{"`$c_url`&sort_by=priority&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("company_id")}{if $search.sort_by == "priority"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>

    <th width="20%">{__("company_name")}</th>
    <th width="20%">{__("badges_name")}</th>
    <th width="20%">{__("badges_image")}</th> 
    <th width="20%">{__("assign_on")}</th>
    <th>&nbsp;</th>
</tr>
</thead>
{foreach from=$badges_assign_list item=badges_data}
<tr class="cm-row-status-{$badges_data.status|lower}">
    <td class="mobile-hide">
        <input name="assignment_ids[]" type="checkbox" value="{$badges_data.id}" class="cm-item" />
    </td>
    <td class="hidden">
    	<input type="text" name="badges_data[company_id]" value="{$badges_data.company_id}" class="input-mini input-hidden">
    </td>
    <td data-th='{__("company_name")}'>
        <a class="row-status" href="{"companies.update?id=`$badges_data.company_id`"|fn_url}">{$badges_data.company_name|truncate:40 nofilter}</a>
    </td>
    <td data-th='{__("badges_name")}'>
        <a class="row-status" href="{"seller_badges.update?id=`$badges_data.badge_id`"|fn_url}">{$badges_data.badge_name|truncate:40 nofilter}</a>
    </td>
    <td data-th='{__("badges_image")}'>
       {include file="common/image.tpl" image=$badges_data.main_pair.icon image_id=$badges_data.main_pair.image_id image_width=50}
    </td>
    <td data-th='{__("assign_on")}'>
        {$badges_data.assign_time|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
    </td>
    <td  class="nowrap" data-th='{__("tools")}'>
        <div class="hidden-tools">
			{capture name="tools_list"}
				<li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="seller_assign.delete?id=`$badges_data.id`"}</li>
            {/capture}
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{capture name="buttons"}
    {capture name="tools_list"}
            {if $badges_assign_list}
                <li>{btn type="delete_selected" dispatch="dispatch[seller_assign.m_delete]" form="badges_log_form"}</li>
            {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
<div class="clearfix">
    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>
</form>
{/capture}

{capture name="sidebar"}
    {include file="addons/banners_badges/views/seller_assign/users_search_form.tpl" dispatch="seller_assign.manage"}
{/capture}

{include file="common/mainbox.tpl" title=__("list_of_assignment") content=$smarty.capture.mainbox title_extra=$smarty.capture.title_extra adv_buttons=$smarty.capture.adv_buttons select_languages=true buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar content_id="badges_log_form"}