{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="badges_log_form" class="form-horizontal">
{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}
{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}
{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{if $badges_list}
<div class="table-responsive-wrapper">
<table width="100%" class="table table-middle table-responsive">
<thead>
<tr>
    <th width="1%">
        {include file="common/check_items.tpl"}
    </th>
    <th width="4%">{__("id")}</th>
    <th class="nowrap hidden" ><a href="{"`$c_url`&sort_by=priority&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("badges_id")}{if $search.sort_by == "priority"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>

    <th width="12%">{__("badges_image")}</th>
    <th width="12%">{__("badges_name")}</th>
    <th width="15%">{__("desc")}</th>
    
    <th width="10%">{__("created_on")}</th>
    <th width="5%">&nbsp;</th>

    <th width="10%"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
</tr>
</thead>
{foreach from=$badges_list item=badges_data}
<tr class="cm-row-status-{$badges_data.status|lower}">
    <td class="mobile-hide">
        <input name="badges_ids[]" type="checkbox" value="{$badges_data.id}" class="cm-item" />
    </td>
    <td data-th='{__("id")}'>
        <a class="row-status" href="{"seller_badges.update?id=`$badges_data.id`"|fn_url}">{$badges_data.id}</a>
    </td>
    <td class="hidden">
    	<input type="text" name="badges_data[{$badges_data.id}][priority]" value="{$badges_data.priority}" class="input-mini input-hidden">
    </td>
    <td data-th='{__("badges_image")}'>
       {include file="common/image.tpl" image=$badges_data.main_pair.icon image_id=$badges_data.main_pair.image_id image_width=50 href="seller_badges.update?id=`$badges_data.id`"|fn_url}
        
    </td>
    <td data-th='{__("badges_name")}'>
        <a class="row-status" href="{"seller_badges.update?id=`$badges_data.id`"|fn_url}">{$badges_data.badge_name|truncate:40 nofilter}</a>
    </td>
    <td data-th='{__("desc")}'>
        {$badges_data.desc|truncate:40 nofilter}
    </td>
    <td data-th='{__("created_on")}'>
        {$badges_data.created_on|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
    </td>
    <td  class="nowrap" data-th='{__("tools")}'>
        <div class="hidden-tools">
			{capture name="tools_list"}
				<li>{btn type="list" text=__("edit") href="seller_badges.update?id=`$badges_data.id`"}</li>
				<li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="seller_badges.delete?id=`$badges_data.id`"}</li>
            {/capture}
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
	<td class="nowrap" data-th='{__("status")}'>
        {include file="common/select_popup.tpl" popup_additional_class="dropleft" display=$status_display id=$badges_data.id status=$badges_data.status hidden=false object_id_name="id" table="seller_badges" update_controller="seller_badges"}
	</td>
</tr>
{/foreach}
</table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{capture name="buttons"}
    {capture name="tools_list"}
            {if $badges_list}
                <li>{btn type="delete_selected" dispatch="dispatch[seller_badges.m_delete]" form="badges_log_form"}</li>
            {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
<div class="clearfix">
    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>
{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="seller_badges.add" prefix="top" hide_tools=true title=__("add_seller_badges") icon="icon-plus"}
{/capture}
</form>
{/capture}
{capture name="sidebar"}
    {include file="addons/banners_badges/views/seller_badges/users_search_form.tpl" dispatch="seller_badges.manage"}
{/capture}
{include file="common/mainbox.tpl" title=__("list_of_badges") content=$smarty.capture.mainbox title_extra=$smarty.capture.title_extra adv_buttons=$smarty.capture.adv_buttons select_languages=true buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar content_id="badges_log_form"}