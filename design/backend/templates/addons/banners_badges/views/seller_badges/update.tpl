{if $seller_badges_data.id}
		{assign var="id" value=$seller_badges_data.id}
{else}
		{assign var="id" value=0}
{/if}

{include file="views/profiles/components/profiles_scripts.tpl"}

{capture name="mainbox"}

	{capture name="tabsbox"}

		{** /Item menu section **}
		<form class="form-horizontal form-edit {$form_class} {if !fn_check_view_permissions("seller_badges.update", "POST")}cm-hide-inputs{/if} {if !$id}cm-comet cm-disable-check-changes{/if}" action="{""|fn_url}" method="post" id="seller_badges_update_form" enctype="multipart/form-data" name="seller_badges_update_form"> {* company update form *}
			{* class=""*}
			<input type="hidden" name="fake" value="1" />
			<input type="hidden" name="selected_section" id="selected_section" value="{$smarty.request.selected_section}" />
			<input type="hidden" name="id" value="{$id}" />
			{** General info section **}
			<div id="content_detailed" class="hidden"> 
			{* content detailed *}
				<fieldset>
					{include file="common/subheader.tpl" title=__("seller_badges_information") target="#seller_badges_general_info"}
					<div id="seller_badges_general_info">
						<div class="control-group">
							<label class="control-label cm-required cm-value-decimal" for="elm_badge_name">{__("badge_name")}</label>
							<div class="controls">
								<input type="text" class="input-large" name="seller_badges_data[badge_name]" id="elm_badge_name" size="32" {if $seller_badges_data} value="{$seller_badges_data.badge_name}"{/if} placeholder="Enter Badge Name" />
							</div>
						</div>

						<div class="control-group">
							{include file="common/select_status.tpl" input_name="seller_badges_data[status]" id="seller_badges_data_status" obj=$seller_badges_data}
						</div>
						<div class="control-group">
							<label class="control-label cm-required cm-integer" for="elm_badge_priority">{__("badge_priority")}:{include file="common/tooltip.tpl" tooltip=__("tooltip_elm_badge_priority")}</label>
							<div class="controls">
								<input type="text" name="seller_badges_data[priority]" id="elm_badge_priority" size="4" value="{if $seller_badges_data}{$seller_badges_data.priority}{else}0{/if}" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label cm-required cm-integer" for="elm_badge_no_of_seller">{__("badge_no_of_seller")}:{include file="common/tooltip.tpl" tooltip=__("tooltip_elm_badge_no_of_seller")}</label>
							<div class="controls">
								<input type="text" name="seller_badges_data[no_of_seller]" id="elm_badge_no_of_seller" size="4" value="{if $seller_badges_data}{$seller_badges_data.total_seller}{else}1{/if}" />
							</div>
						</div>
						<div class="control-group">
							<fieldset>	 
							<div class="control-group">
							    <label class="control-label" for="elm_badge_description">{__("description")}:</label>
							    <div class="controls">
							        <textarea id="elm_badge_description" name="seller_badges_data[badge_description]" cols="55" rows="8" class="input-large">{$seller_badges_data.desc}</textarea>
							    </div>
							</div>
							</fieldset>
						</div>
						<div class="control-group" id="seller_badge_graphic">
							<label class="control-label cm-required" for="type_banners_badges_main">{__("badge_image")}{include file="common/tooltip.tpl" tooltip=__("tooltip_seller_badge_graphic")}</label>
							<div class="controls" id="type_banners_badges_main">
							{include file="common/attach_images.tpl" image_name="banners_badges_main" image_object_type="seller_badges" image_pair=$seller_badges_data.main_pair image_object_id=$id no_detailed=true hide_titles=true}
							<div class="hidden image-err-box">
								<p style="color: #B94A48;">{__("wk_please_select_an_image")}</p>
							</div> 
							</div>
						</div>
						<div class="control-group" id="seller_badge_url">
							<label class="control-label" for="elm_badge_url">{__("url")}:{include file="common/tooltip.tpl" tooltip=__("tooltip_elm_badge_url")}</label>
							<div class="controls">
								<input type="text" name="seller_badges_data[url]" id="elm_badge_url" value="{$seller_badges_data.url}" size="25" class="input-large"/>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			{* /content detailed *}
			{** /General info section **}	
			{* products details *}
			<div id="content_select_vendor" class="hidden">
				{include file="pickers/companies/picker.tpl" data_id="share" input_name="seller_badges_data[company_id]" item_ids=$seller_badges_data.manual.company_id no_js=true positions=false view_mode="list" hide_edit_button=true view_only=$runtime.company_id multiple=true hidden_field=true show_add_button=true no_item_text=$no_item_text}
			</div>
			<div id="content_badges_condition" class="hidden">
				{include file="addons/banners_badges/views/seller_badges/components/group.tpl" prefix="badges_data[conditions]" group=$badges_data.conditions root=true no_ids=true hide_add_buttons=!$allow_save}
			</div>
			{if $runtime.mode == 'update'}
			<div id="content_auto_assign" class="hidden">
				{include file="addons/banners_badges/views/seller_badges/components/auto_assign.tpl"}
			</div>
			{/if}
			{* /products details *}
		</form>
		{* /product update form *}
	{/capture}

	{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name="companies" active_tab=$smarty.request.selected_section track=true}

{/capture}
{** Form submit section **}

{capture name="buttons"}   
	{if $id}
		{include file="buttons/save_cancel.tpl" but_name="dispatch[seller_badges.update]" but_target_form="seller_badges_update_form" save=$id}
	{else}
		{include file="buttons/save_cancel.tpl" but_name="dispatch[seller_badges.add]" but_target_form="seller_badges_update_form" but_meta="cm-comet"}
	{/if}
{/capture}
{** /Form submit section **}

{if $id}
	{include file="common/mainbox.tpl" title="{__("editing_badges")}" content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons}
{else}
	{include file="common/mainbox.tpl" title=__("new_badges") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
{/if}
<script type="text/javascript">
	(function(_, $) {
	$.ceEvent('on', 'ce.formpre_seller_badges_update_form', function(form, elm) {
        var val_value = form.find('[name="file_banners_badges_main_image_icon[0]"]').val();
        var val_value2 = form.find('[name="banners_badges_main_image_data[0][pair_id]"]').val();
        var val_value3 = form.find('[class="no-image"]');
        var count = val_value3.length;
        
        if(val_value == 'banners_badges_main' && (val_value2 == "null" || val_value2 == "undefined" || val_value2 == "" || (count != 0)) )
        {	
        	$(".image-err-box").show();
        	return false;
        }	
        	
        else
        {
        	$(".image-err-box").hide();
        	return true;
        }
    });
		
	})(Tygh,Tygh.$);
</script>