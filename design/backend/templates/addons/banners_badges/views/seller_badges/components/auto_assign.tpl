<div class="table-wrapper">
<table width="100%" class="table table-middle">
	<thead>
	<tr>
	    <th width="100%">{__("name")}</th>
	    <th>&nbsp;</th>
	</tr>
	</thead>
		{if $seller_badges_data.auto.company_id|@count}
			<tbody>
				{foreach from=$seller_badges_data.auto.company_id item=company_id}
					{assign var="compny_name" value=$company_id|fn_get_company_name}
					<tr>
						<td>
						<a href="{"companies.update?company_id={$company_id}"|fn_url}">{$compny_name}</a>
					</td>
					<td></td>
					</tr>
				{/foreach}
			</tbody>
		{else}
			<tbody>
		        <tr class="no-items">
		            <td><p>{$no_item_text|default:__("no_items") nofilter}</p></td>
		        </tr>
	        </tbody>
		{/if}
</table>
</div>