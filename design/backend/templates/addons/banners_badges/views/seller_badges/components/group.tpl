
<div>
    <label class="control-label" for="elm_badge_autoenable">{__("auto_enable")}:{include file="common/tooltip.tpl" tooltip=__("tooltip_elm_badge_autoenable")}
    </label>
    <div class="controls">
        <label class="checkbox">
            <input type="checkbox" name="seller_badges_data[badge_autoenable]" id="elm_badge_autoenable" {if $seller_badges_data.auto_enable == 'Y'} checked="checked" {/if} value="Y"/>
        </label>
    </div>
</div>
<div>
    {if $seller_badges_data.condition_count == 1}
    <p style="margin-top: 15px;"> 
        {__('you_can_delete_condition_after_disable_auto_assign')} 
    </p>
    {/if}
</div>
<div id="content_detailed"> {* content detailed *}
    <fieldset>
        {include file="common/subheader.tpl" title=__("seller_badges_condition") target="#content_badge_condition"}
        <div id="content_badge_condition">
            <div class="table-wrapper">
                <table class="table table-middle" width="100%">
                <thead class="cm-first-sibling">
                <tr>
                    <th width="25%">{__("condition")}{include file="common/tooltip.tpl" tooltip=__("wk_banners_badges_conditions_tooltip")}</th>
                    <th width="25%">{__("criteria")}</th>
                    <th width="20%">{__("value")}</th>
                    <th width="15%">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
            {if $seller_badges_data.condition_count}

                {foreach from=$seller_badges_data['conditions'] key=k item=conditions}
                <tr class="{cycle values="table-row , " reset=1}{$no_hide_input_if_shared_product}" id="box_add_badge_condition_previous">
                    <td width="25%">
                        <select class="span3" name="seller_badges_data[pre_criteria][{$conditions.id}]">
                            <option value="O" {if $conditions.criteria == "O"}selected="selected"{/if}>{__("no_of_orders")}</option>
                            <option value="P" {if $conditions.criteria == "P"}selected="selected"{/if}>{__("no_of_products")}</option>
                            <option value="S" {if $conditions.criteria == "S"}selected="selected"{/if}>{__("sales_amount")}</option>
                            <option value="VR" {if $conditions.criteria == "VR"}selected="selected"{/if}>{__("no_of_vendor_review")}</option>
                            <option value="VPR" {if $conditions.criteria == "VPR"}selected="selected"{/if}>{__("no_of_vendor_products_review")}</option>
                        </select>
                    <td width="25%">
                        <select class="span3" name="seller_badges_data[pre_condition][{$conditions.id}]">
                            <option value="EQ" {if $conditions.condition == "EQ"}selected="selected"{/if}>{__("equal")}</option>
                            <option value="LTE" {if $conditions.condition == "LTE"}selected="selected"{/if}>{__("equal_or_less")}</option>
                            <option value="GTE" {if $conditions.condition == "GTE"}selected="selected"{/if}>{__("equal_or_greater")}</option>
                            <option value="LT" {if $conditions.condition == "LT"}selected="selected"{/if}>{__("less")}</option>
                            <option value="GT" {if $conditions.condition == "GT"}selected="selected"{/if}>{__("greater")}</option>
                        </select>
                    </td>
                    <td width="20%">
                        <input type="text" name="seller_badges_data[pre_amount][{$conditions.id}]" value="{if $conditions}{$conditions.amount}{else}0{/if}" class="input-small" />
                    </td>
                    
                        <td width="15%" class="right">
                        {if ($seller_badges_data.condition_count > 1 )|| ($seller_badges_data.auto_enable == 'N')}
                        <div class="hidden-tools">
                            {capture name="tools_list"}
                                <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="seller_badges.c_delete?id=`$conditions.id`&badge_id=`$id`"}</li>
                            {/capture}
                            {dropdown content=$smarty.capture.tools_list}
                        </div>
                    {/if}
                        </td>
                </tr>
                {/foreach}
            </tbody>
                </table>
            </div>
            <div class="table-wrapper">
                <table class="table table-middle" width="100%">
                <thead class="cm-first-sibling">
                <tr style="display: none;">
                    <th width="25%">{__("")}</th>
                    <th width="25%">{__("")}</th>
                    <th width="20%">{__("")}</th>
                    <th width="15%">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <tr><td class="no_items" style="font-weight:bold; ">{__("add_new_conditions_here")}</td></tr>
                {/if}
                {math equation="x+1" x=$_key|default:0 assign="new_key"}
                <tr class="{cycle values="table-row , " reset=1}{$no_hide_input_if_shared_product}" id="box_add_badge_condition">
                    <td width="25%">
                        <select class="span3" name="seller_badges_data[criteria][{$new_key}]">
                            <option value="">{__("---")}</option>
                            <option value="O">{__("no_of_orders")}</option>
                            <option value="P">{__("no_of_products")}</option>
                            <option value="S">{__("sales_amount")}</option>
                            <option value="VR">{__("no_of_vendor_review")}</option>
                            <option value="VPR">{__("no_of_vendor_products_review")}</option>
                        </select>
                    <td width="25%">
                        <select class="span3" name="seller_badges_data[condition][{$new_key}]">
                            <option value="">{__("---")}</option>
                            <option value="EQ">{__("equal")}</option>
                            <option value="LTE">{__("equal_or_less")}</option>
                            <option value="GTE">{__("equal_or_greater")}</option>
                            <option value="LT">{__("less")}</option>
                            <option value="GT">{__("greater")}</option>
                        </select>
                    </td>
                    <td width="20%">
                        <input type="text" name="seller_badges_data[amount][{$new_key}]" value="{if $cond}{$cond.amount}{else}0{/if}" class="input-small" />
                    </td>
                    <td width="15%" class="right">
                        {include file="buttons/multiple_buttons.tpl" item_id="add_badge_condition"}
                    </td>
                </tr>
                </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>