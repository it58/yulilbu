{if !$hide_title}
{include file="common/subheader.tpl" title=__("banners_badges") target="#acc_addon_banners_badges"}
{/if}
<div id="acc_addon_banners_badges" class="collapsed in">
<div class="control-group {if $share_dont_hide}cm-no-hide-input{/if}">
	<div class="control-group" id="vendor_banner_graphic">
		<label class="control-label">{__("banner_image")}{include file="common/tooltip.tpl" tooltip=__("tooltip_vendor_banner_graphic")}</label>
		<div class="controls">
				{include file="common/attach_images.tpl" image_name="banners_badges_main" image_object_type="vendor_banner" image_pair=$banners_badges_data image_object_id=$company_id no_detailed=true hide_titles=true}
		</div> 
	</div>
	<div class="control-group" id="banners_badges_url">
		<label class="control-label" for="elm_banners_badges_url">{__("url")}:{include file="common/tooltip.tpl" tooltip=__("tooltip_elm_banners_badges_url")}</label>
		<div class="controls">
			<input type="text" name="{$object_name}[banners_badges_url]" id="elm_banners_badges_url" value="{$banners_badges_data.url}" size="25" class="input-large"/>
		</div>
	</div>
</div>
</div>
