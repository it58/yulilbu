{if $wk_categories_tree}
    {foreach from=$wk_categories_tree item=item key=key1}
        {foreach from=$item item=category key=key}
        {if !empty($category.category_id) && $category.parent_id == $wk_parent_category_id}
            <li id="wk_category_id_{$category.category_id}" data-category-id="{$category.category_id}" data-parent-id="{$wk_parent_category_id}" class="{if $category.has_children || $category.subcategories}child{else}no_child{/if}">{$category.category}{if $category.has_children || $category.subcategories}<span class="fa fa-caret-right chevron-icon"></span>{/if}</li>
        {/if}
        {/foreach}
    {/foreach}    
{/if}