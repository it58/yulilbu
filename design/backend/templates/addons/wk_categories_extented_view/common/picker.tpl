<div class="category_container_box {if $wk_category_path}hidden{/if}" id="category_container_box">
    <div class="categories_container" id="child_categories_{$wk_parent_category_id}"></div> 
<!--category_container_box--></div>

{script src="js/addons/wk_category_extended_view/wk_cat.js"} 
<script type="text/javascript">
    Tygh.tr('select_a_category', '{__("select_a_category")|escape:"javascript"}');
    Tygh.tr('please_select_a_category', '{__("please_select_a_category")|escape:"javascript"}');
    Tygh.tr('please_select_a_leaf_category', '{__("please_select_a_leaf_category")|escape:"javascript"}');
    Tygh.tr('category_selected', '{__("category_selected")|escape:"javascript"}');
    Tygh.tr('success', '{__("success")|escape:"javascript"}');
    {if $wk_category_path}
        $(document).ready(function(){
            var category_path1 = '{$wk_category_path}';
            var categories1 =  category_path1.split('/');
            if(categories1 && categories1.length>0){
                fn_add_subcategory_tree(0,categories1);
            }
        });
    {else}
        fn_add_subcategory_tree(0,{});
    {/if}
</script>

