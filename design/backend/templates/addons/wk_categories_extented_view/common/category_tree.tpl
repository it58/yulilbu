 {if $wk_categories_tree}
    <div class="input-group">
        <i class="fa fa-search search-icon"></i>
        <input type="text" name="search" class="category_search form-control" data-parent-id="{$wk_parent_category_id}" id="wk_search_category_{$wk_parent_category_id}" placeholder="Search">
    </div>
    <ul id="container_category_list_{$wk_parent_category_id}" >
        {foreach from=$wk_categories_tree item=item key=key1}
            {foreach from=$item item=category key=key}
            {if !empty($category.category_id) && $category.parent_id == $wk_parent_category_id}
                <li id="wk_category_id_{$category.category_id}" data-category-id="{$category.category_id}" data-parent-id="{$wk_parent_category_id}"  class="{if $category.subcategories || $category.has_children}child{else}no_child{/if}">{$category.category}{if $category.subcategories || $category.has_children}<span class="fa fa-caret-right chevron-icon"></span>{/if}</li>
            {/if}
            {/foreach}
        {/foreach}
    <!--container_category_list_{$wk_parent_category_id}--></ul>
<!--child_categories_{$wk_parent_category_id}-->{/if}