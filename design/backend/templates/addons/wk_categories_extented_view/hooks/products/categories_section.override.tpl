{hook name="products:categories_section"}
    {$result_ids = "product_categories"}
    {if "MULTIVENDOR"|fn_allowed_for && $mode != "add"}
            {$js_action = "fn_change_vendor_for_product();"}
    {/if}
    {hook name="companies:product_details_fields"}
    {include file="views/companies/components/company_field.tpl"
        name="product_data[company_id]"
        id="product_data_company_id"
        selected=$product_data.company_id
        tooltip=$companies_tooltip
    }
    {/hook}
    <input type="hidden" value="{$result_ids}" name="result_ids">
    <div class="control-group" id="product_categories">
        <label for="selected_category_id" class="control-label cm-required">{__("categories")}</label>
        <div class="controls">
            <input type="hidden" name="product_data[category_ids]" id="selected_category_id" value="{$product_data.category_ids.0}">
            <span id="selected_category_title" class="hidden"></span>
            <div class="category_main_container_box">
                <span class="pull-right toggle_category_span" >
                    <a class="toggle_category_box">{if $wk_category_path}change{else}close{/if}</a>
                </span>
                <div class="category_title_container">
                    <span id="category_title_{$wk_parent_category_id}" class="select_a_category_title">{__("select_a_category")}</span>
                </div>
                {include file="addons/wk_categories_extented_view/common/picker.tpl"}
            </div>
        </div>
    </div>
{/hook}