<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<style>
.category_container_box{
    margin:5px;
}
.category_main_container_box{
    border:1px solid gray;
    border-radius:2px;
    padding:5px;
    margin:5px;
    box-shadow: 0 0 2px gray inset;
}
.category_title_container{
    padding:5px;
}
.select_a_category_title{
    font-size:25px;
    color:gray;
}
.categories_container{
    border:1px solid gray;
    border-radius:3px;
    margin:5px;
    width:230px;
    display:inline-block;
}
.icon_root{
    vertical-align: top;
    height: 260px;
    color:grey;
    font-size: 20px !important;
    box-sizing: border-box;
    padding-top: 120px;
}
.icon_title{
    color:grey;
    font-size: 14px !important;
}
.icon_title .title,.parent_title{
    color:black;
    font-size: 20px !important;
    margin:3px;
}
.categories_container .category_search{
    margin:5px;
}
.toggle_category_span{
    margin-top:5px;
    margin-right:5px;
}
.categories_container ul li{
    margin-bottom:3px;
    padding:5px;
    padding-right:15px;
    cursor:pointer;
}
.categories_container ul{
    height:230px;
    overflow:auto;
    list-style-type: none;
    position:relative;
    margin:0px;
}
.categories_container ul li:last-child{
    border:none;
    margin-bottom:0px;
}

.chevron-icon{
    position:absolute;
    right:5px;
    color:grey;
    font-size: 15px !important;
    margin-top:3px;
}
.search-icon{
    position:absolute;
    color:gray;
    right:15px;
    margin-top:14px;
}
.categories_container ul li a{
    color:#404040;
}
.categories_container ul li:hover{
    background-color:#6790BE;
    color:white;
}
.active{
    background-color:#6790BE;
    color:white;
} 

div .input-group{
    position:relative;
}

.input-group .form-control:focus {
    box-shadow:none;
    -webkit-box-shadow:none; 
    border-color:#cccccc; 
}
</style>