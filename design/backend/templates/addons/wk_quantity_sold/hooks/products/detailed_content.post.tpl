{if $runtime.company_id == 0}
{include file="common/subheader.tpl" title=__("wk_quantity_sold_header") target="#wk_quantity_sold_header"}
<div id="wk_quantity_sold_header" class="collapse in">    
<div class="control-group">
    <label class="control-label" for="wk_quantity_sold_val">{__("wk_quantity_sold_val")}:{include file="common/tooltip.tpl" tooltip=__("wk_quantity_sold_val_help")}</label>
        <div class="controls">
            <input type="text" {if $runtime.company_id != 0} readonly {/if} id="wk_quantity_sold_val" name="product_data[wk_quantity_sold_val]" value="{$product_data.wk_quantity_sold_val|default:"0"}" class="input-large cm-value-integer" size="5" />
        </div>
    </div>           
</div>
{/if}
