{** conversations section **}

{capture name="mainbox"}
{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}
{if $conversations}
    <form action="{""|fn_url}" method="post" name="conversations_form">
    <table class="table table-middle cp-conversations-table">
        <thead>
            <tr>
                <th width="1%" class="left">{include file="common/check_items.tpl"}</th>
                <th  width="10%">&nbsp;</th>
                <th  width="10%">{__('recipient')}</th>
                <th width="49%">{__("conversation")}</th>
                <th width="20%">{__("messages")}</th>
                <th width="15%">{__('date')}</th>  
                <th width="10%" class="right">&nbsp;</th>
            </tr>
        </thead>
        {foreach from=$conversations item=conversation}
            <tr class="cp-external-click" data-ca-external-click-id="conversations_opener_{$conversation.conversation_id}">
                <td class="left">
                    <input type="checkbox" name="conversation_ids[]" value="{$conversation.conversation_id}" class="cm-item" />
                </td>
                <td>
                    {if $conversation.last_message.user_image}
                        {include file="common/image.tpl" image=$conversation.last_message.user_image image_width=40 image_height=40}
                    {else}
                        <div class="cp-conversation-no-avatar"><i class="cp-icon-user"></i></div>
                    {/if}
                </td>
                <td>
                    <a href="{"profiles.update&profile_id=`$conversation.last_message.user_id`"|fn_url}">{$conversation.last_message.user_id|fn_get_user_name}</a>
                </td>
                <td class="nowrap row-status">
                    <div class="cp-subject {if $conversation.read == 'N'}unread{/if}">
                        <a href="{"conversations.update&conversation_id=`$conversation.conversation_id`"|fn_url}" id="conversations_opener_{$conversation.conversation_id}">
                            {$conversation.subject}
                        </a>
                </div>
                    <div class="cp-last-message">{$conversation.last_message.message}</div>
                </td>
                <td>
                    <a class="badge" href="{"conversations.update&conversation_id=`$conversation.conversation_id`&selected_section=messages"|fn_url}">{$conversation.messages_amount}</a>
                </td>
                <td width="15%">
                    {$conversation.last_message.humanized_time}
                    {if $conversation.last_message.user_id == $auth.user_id}
                        <a href="{"conversations.update&conversation_id=`$conversation.conversation_id`"|fn_url}" class="cp-reply-link"><i class="cp-icon-reply"></i></a>
                    {/if}
                </td>
                <td>
                    {capture name="tools_list"}
                        <li>{btn type="list" text=__("edit") href="conversations.update?conversation_id=`$conversation.conversation_id`"}</li>
                        <li>{btn type="list" class="cm-confirm" text=__("delete") href="conversations.delete?conversation_id=`$conversation.conversation_id`" method="POST"}</li>
                    {/capture}
                    <div class="hidden-tools">
                        {dropdown content=$smarty.capture.tools_list}
                    </div>
                </td>
            </tr>
        {/foreach}
    </table>
    </form>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $conversations}
            <li>{btn type="delete_selected" dispatch="dispatch[conversations.m_delete]" form="conversations_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
    <a class="btn btn-primary cm-dialog-opener cm-ajax" href="{"conversations.new"|fn_url}" data-ca-dialog-title="{__('new_conversation')}" data-ca-target-id="compose_new_message">
        <i class="cp-icon-edit"></i>{__('compose')}
    </a>
{/capture}

{capture name="sidebar"}
    <div class="sidebar-row">
        <h6>{__('search')}</h6>
        <form action="{""|fn_url}" method="get">
            <input type="hidden" name="dispatch" value="conversations.manage">
            <div class="sidebar-field">
                <label>{__('find_text')}</label>
                <input type="text" name="q" size="20" value="{$smarty.request.q}">
            </div>
            <div class="sidebar-field">
                <label>{__('recipient')}</label>
                {include file="pickers/users/picker.tpl" input_name=recipient_ids item_ids=$smarty.request.recipient_ids view_mode="mixed"}
            </div>
            <input class="btn " type="submit" name="dispatch[conversations.manage]" value="{__('search')}">
        </form>
    </div>
{/capture}
    
{/capture}
{include file="common/mainbox.tpl" title=__("conversations") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar select_languages=true}

{** ad section **}