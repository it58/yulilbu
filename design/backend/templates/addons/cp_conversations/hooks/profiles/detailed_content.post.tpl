<div class="control-group">
    <label class="control-label">{__("image")}:</label>
    <div class="controls">
        {include file="common/attach_images.tpl" image_name="user_image" image_object_type="user_image" image_pair=$user_data.user_image no_thumbnail=true}
    </div>
</div>