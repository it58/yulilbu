
{include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section="O" title='' wk_vendor_custom_fields=$wk_vendor_custom_fields extra_fields=$extra_fields}

{if "ULTIMATE"|fn_allowed_for}
{hook name="companies:storefronts"}
    <div class="control-group">
        <label for="elm_company_storefront" class="control-label cm-required">{__("storefront_url")}:</label>
        <div class="controls">
        {if $runtime.company_id}
            http://{$company_data.storefront|unpuny}
        {else}
            <input type="text" name="company_data[storefront]" id="elm_company_storefront" size="32" value="{$company_data.storefront|unpuny}" class="input-large" placeholder="http://" />
        {/if}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_company_secure_storefront">{__("secure_storefront_url")}:</label>
        <div class="controls">
        {if $runtime.company_id}
            https://{$company_data.secure_storefront|unpuny}
        {else}
            <input type="text" name="company_data[secure_storefront]" id="elm_company_secure_storefront" size="32" value="{$company_data.secure_storefront|unpuny}" class="input-large" placeholder="https://" />
        {/if}
        </div>
    </div>
{/hook}

{hook name="companies:storefronts_design"}
    {if $id}
    {include file="common/subheader.tpl" title=__("design")}

    <div class="control-group">
        <label class="control-label">{__("store_theme")}:</label>
        <div class="controls">
            <p>{$theme_info.title}: {$current_style.name}</p>
            <a href="{"themes.manage?switch_company_id=`$id`"|fn_url}">{__("goto_theme_configuration")}</a>
        </div>
    </div>
    {else}
        {* TODO: Make theme selector *}
        <input type="hidden" value="responsive" name="company_data[theme_name]">
    {/if}
{/hook}

{/if}

{if "MULTIVENDOR"|fn_allowed_for}
    {if !$runtime.company_id}
        {include file="common/select_status.tpl" input_name="company_data[status]" id="company_data" obj=$company_data items_status="companies"|fn_get_predefined_statuses:$company_data.status}
    {else}
        <div class="control-group">
            <label class="control-label">{__("status")}:</label>
            <div class="controls">
                <label class="radio"><input type="radio" checked="checked" />{if $company_data.status == "A"}{__("active")}{elseif $company_data.status == "P"}{__("pending")}{elseif $company_data.status == "N"}{__("new")}{elseif $company_data.status == "D"}{__("disabled")}{/if}</label>
            </div>
        </div>
    {/if}

  
{/if}


{if !$id}
    {literal}
    <script type="text/javascript">
    function fn_toggle_required_fields()
    {
        var $ = Tygh.$;
        var checked = $('#company_description_vendor_admin').prop('checked');

        $('#company_description_username').prop('disabled', !checked);
        $('#company_description_first_name').prop('disabled', !checked);
        $('#company_description_last_name').prop('disabled', !checked);

        $('.cm-profile-field').each(function(index){
            $('#' + Tygh.$(this).prop('for')).prop('disabled', !checked);
        });
    }

    function fn_switch_store_settings(elm)
    {
        jelm = Tygh.$(elm);
        var close = true;
        if (jelm.val() != 'all' && jelm.val() != '' && jelm.val() != 0) {
            close = false;
        }
        
        Tygh.$('#clone_settings_container').toggleBy(close);
    }

    function fn_check_dependence(object, enabled)
    {
        if (enabled) {
            Tygh.$('.cm-dependence-' + object).prop('checked', 'checked').prop('readonly', true).on('click', function(e) {
                return false
            });
        } else {
            Tygh.$('.cm-dependence-' + object).prop('readonly', false).off('click');
        }
    }
    </script>
    {/literal}

    {if !"ULTIMATE"|fn_allowed_for}
        <div class="control-group">
            <label class="control-label" for="elm_company_vendor_admin">{__("create_administrator_account")}:</label>
            <div class="controls">
                <label class="checkbox">
                    <input type="checkbox" name="company_data[is_create_vendor_admin]" id="elm_company_vendor_admin" checked="checked" value="Y" onchange="fn_toggle_required_fields();" />
                </label>
            </div>
        </div>
    {/if}
{/if}

{if "MULTIVENDOR"|fn_allowed_for}
    {hook name="companies:contact_information"}
    {if !$id}
        {include file="views/profiles/components/profile_fields.tpl" section="C" title=__("contact_information")}
    {else}
        {include file="common/subheader.tpl" title=__("contact_information")}
    {/if}
    {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section="C" title=''}

    {/hook}
    {hook name="companies:shipping_address"}
        {if !$id}
            {include file="views/profiles/components/profile_fields.tpl" section="B" title=__("shipping_address") shipping_flag=false}
        {else}
            {include file="common/subheader.tpl" title=__("shipping_address")}
        {/if}
        {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section='S' body_id='' ship_to_another=true title='' address_flag=$profile_fields|fn_compare_shipping_billing ship_to_another=$ship_to_another wk_vendor_custom_fields=$wk_vendor_custom_fields extra_fields=$extra_fields}
    {/hook}
{/if}
{if !empty($all_sections)}
    {foreach from=$all_sections key="key" item="section_data"}
        {assign var="section_id" value=$section_data.section_id} 
        {if !empty($wk_vendor_custom_fields.$section_id)}
            {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section=$section_data.section_id title=$section_data.description wk_vendor_custom_fields=$wk_vendor_custom_fields extra_fields=$extra_fields}
       {/if}
    {/foreach}
{/if}
{if "ULTIMATE"|fn_allowed_for}
    {include file="common/subheader.tpl" title="{__("settings")}: {__("company")}" }
    
    {foreach from=$company_settings key="field_id" item="item"}
        {include file="common/settings_fields.tpl" item=$item section="Company" html_id="field_`$section`_`$item.name`_`$item.object_id`" html_name="update[`$item.object_id`]"}
    {/foreach}
{/if}

          


                    