
{capture name="mainbox"}
<form  action="{""|fn_url}" method="post" name="edit_custom_section" class="form-horizontal form-edit  cm-disable-empty-files" enctype="multipart/form-data" id="add_section_type_form" >
{include file="common/subheader.tpl" title=__("information") target="#edit_information"}
<div id="edit_information">
     <input id="elm_section_description" class="input-large" type="hidden" name="section_info[section_id]" value="{$section_id}" />
    <div class="control-group">
        <label for="elm_section_description" class="control-label cm-required">{__("section_name")}:</label>
        <div class="controls">
        <input id="elm_section_description" class="input-large" type="text" name="section_info[section_description]" value="{$description}" />
        </div>
    </div>
</div>   
</form>
{capture name="buttons"}
    {include file="buttons/save.tpl" but_name="dispatch[wk_vendor_custom_fields.edit_custom_sections]" but_role="submit-link" but_target_form="edit_custom_section"}
{/capture}
{/capture}
{include file="common/mainbox.tpl" title=__("edit_custom_section") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}