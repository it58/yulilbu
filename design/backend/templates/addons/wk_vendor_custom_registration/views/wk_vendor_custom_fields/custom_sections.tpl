{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="wk_vendor_custom_sections" class="form-horizontal form-edit  {$hide_inputs}">
{if $all_sections}
    <table width="100%" class="table table-middle">
    <thead>
        <tr>
            <th width="1%" class="left">
                {include file="common/check_items.tpl"}</th>
            <th>{__("section_id")}</th>
            <th>{__("section_name")}</th>
            <th></th>
        </tr>
    </thead>
    {foreach from=$all_sections key="key" item="section_data"}
    <tr>
        <td><input type="checkbox" name="section_ids[]" value="{$section_data.section_id}" class="cm-item" /></td>
        
        <td> <a href="{"wk_vendor_custom_fields.edit_custom_sections?section_id=`$section_data.section_id`"|fn_url}">{$section_data.section_id}</a></td>
        <td><a href="{"wk_vendor_custom_fields.edit_custom_sections?section_id=`$section_data.section_id`"|fn_url}">{$section_data.description}</td>
        <td class="nowrap">
            <div class="hidden-tools">
                {capture name="tools_list"}
                    <li>{btn type="list" text=__("edit") href="wk_vendor_custom_fields.edit_custom_sections?section_id=`$section_data.section_id`"}</li>
                    <li>{btn type="list" text=__("delete") href="wk_vendor_custom_fields.delete_custom_section?section_id=`$section_data.section_id`" method="POST"}</li>

                {/capture}
                {dropdown content=$smarty.capture.tools_list}
            </div>
        </td> 
    </tr>
    {/foreach}
</table>
{else}
    <p class="no-items">{__("nodata")}</p>
{/if}
</form>
{/capture}
{capture name="add_section_type"}
    {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/section_type.tpl"}
{/capture}
{capture name="adv_buttons"}
     {include file="common/popupbox.tpl" id="add_section_type" text=__("add_section_type") title=__("add_section_type") content=$smarty.capture.add_section_type act="general" link_class="cm-dialog-auto-size" icon="icon-plus" link_text=""}
{/capture}
{capture name="buttons"}
    {capture name="tools_items"}
        {hook name="companies:manage_tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[wk_vendor_custom_fields.custom_section_delete]" form="wk_vendor_custom_sections"}</li>
        {/hook}
    {/capture}
    {dropdown content=$smarty.capture.tools_items}
{/capture}
{include file="common/mainbox.tpl" title={__('new_wk_vendor_custom_section')} content=$smarty.capture.mainbox  buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons select_languages=true}
