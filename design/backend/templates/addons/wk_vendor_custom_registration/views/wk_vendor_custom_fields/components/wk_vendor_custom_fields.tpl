
{if $wk_vendor_custom_fields.$section}
{if !$nothing_extra}
    {include file="common/subheader.tpl" title=$title}
{/if}
{foreach from=$wk_vendor_custom_fields.$section item=field}
{if $field.field_name}
    {assign var="data_name" value="company_data"}
    {assign var="data_id" value=$field.field_name}
    {assign var="value" value=$company_data.$data_id}
{else}
    {assign var="data_name" value="company_data[fields]"}
    {assign var="data_id" value=$field.field_id}
    {assign var="value" value=$company_data.fields.$data_id}
{/if}
{foreach from=$extra_fields key=wk_field_id item =field_value}
    {if $wk_field_id== $field.field_id}
     {assign var="value" value=$field_value}
    {/if}
{/foreach}
{foreach from=$company_data key=wk_field_name item=field_value}
     {if $wk_field_name== $field.field_name}
     {assign var="value" value=$field_value}
    {/if}
{/foreach}
{if $field.field_id!=2}
    <div class="control-group">
        <label for="{$id_prefix}elm_{$field.field_id}" class="control-label cm-profile-field {if $field.required == "Y"}cm-required{/if}{if $field.field_type == "P"} cm-phone{/if}{if $field.field_type == "Z"} cm-zipcode{/if}{if $field.field_type == "E"} cm-email{/if} {if $field.field_type == "Z"}{if $section == "S"}cm-location-shipping{else}cm-location-billing{/if}{/if}">{$field.description}:</label>

        <div class="controls">
        {if $field.field_type == "A"}  {* State selectbox *}

            {$_country = $settings.General.default_country}
            {$_state = $value|default:$settings.General.default_state}

            <select class="cm-state {if $section == "S"}cm-location-shipping{else}cm-location-billing{/if}" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" {$disabled_param nofilter}>
                <option value="">- {__("select_state")} -</option>
                {if $states && $states.$_country}
                    {foreach from=$states.$_country item=state}
                        <option {if $_state == $state.code}selected="selected"{/if} value="{$state.code}">{$state.state}</option>
                    {/foreach}
                {/if}
            </select><input type="text" id="elm_{$field.field_id}_d" name="{$data_name}[{$data_id}]" size="32" maxlength="64" value="{$_state}" disabled="disabled" class="cm-state {if $section == "S"}cm-location-shipping{else}cm-location-billing{/if} input-large hidden cm-skip-avail-switch" />

        {elseif $field.field_type == "O"}  {* Countries selectbox *}
            {assign var="_country" value=$value|default:$settings.General.default_country}
            <select id="{$id_prefix}elm_{$field.field_id}" class="cm-country {if $section == "S"}cm-location-shipping{else}cm-location-billing{/if}" name="{$data_name}[{$data_id}]" {$disabled_param nofilter}>
                {hook name="profiles:country_selectbox_items"}
                <option value="">- {__("select_country")} -</option>
                {foreach from=$countries item="country" key="code"}
                <option {if $_country == $code}selected="selected"{/if} value="{$code}">{$country}</option>
                {/foreach}
                {/hook}
            </select>

        {elseif $field.field_type == "C"}  {* Checkbox *}
            <input type="hidden" name="{$data_name}[{$data_id}]" value="N" {$disabled_param nofilter} />
            <label class="checkbox">
            <input type="checkbox" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" value="Y" {if $value == "Y"}checked="checked"{/if} {$disabled_param nofilter} /></label>

        {elseif $field.field_type == "T"}  {* Textarea *}
         
            <textarea class="input-large" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" cols="32" rows="3" {$disabled_param nofilter}>{$value}</textarea>
            

        {elseif $field.field_type == "D"}  {* Date *}
            {include file="common/calendar.tpl" date_id="elm_`$field.field_id`" date_name="`$data_name`[`$data_id`]" date_val=$value extra=$disabled_param}

        {elseif $field.field_type == "S"}  {* Selectbox *}
            {if $field.field_id==3}
                 <select name="company_data[lang_code]" id="elm_company_language">
                {foreach from=$languages item="language" key="lang_code"}
                    <option value="{$lang_code}" {if $lang_code == $company_data.lang_code}selected="selected"{/if}>{$language.name}</option>
                {/foreach}
                </select>
            {else}
                <select id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" {$disabled_param nofilter}>
                    {if $field.required != "Y"}
                    <option value="">--</option>
                    {/if}
                    {foreach from=$field.values key=k item=v}
                    <option {if $value == $k}selected="selected"{/if} value="{$k}">{$v}</option>
                    {/foreach}
                </select>
            {/if}
        {elseif $field.field_type == "R"}  {* Radiogroup *}
            <div class="select-field">
            {foreach from=$field.values key=k item=v name="rfe"}
            <input class="radio" type="radio" id="{$id_prefix}elm_{$field.field_id}_{$k}" name="{$data_name}[{$data_id}]" value="{$k}" {if (!$value && $smarty.foreach.rfe.first) || $value == $k}checked="checked"{/if} {$disabled_param nofilter} /><label for="{$id_prefix}elm_{$field.field_id}_{$k}">{$v}</label>
            {/foreach}
            </div>

        {elseif $field.field_type == "N"}  {* Address type *}
            <input class="radio valign {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if}" type="radio" id="{$id_prefix}elm_{$field.field_id}_residential" name="{$data_name}[{$data_id}]" value="residential" {if !$value || $value == "residential"}checked="checked"{/if} {if !$skip_field}{$disabled_param nofilter}{/if} /><span class="radio">{__("address_residential")}</span>
            <input class="radio valign {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if}" type="radio" id="{$id_prefix}elm_{$field.field_id}_commercial" name="{$data_name}[{$data_id}]" value="commercial" {if $value == "commercial"}checked="checked"{/if} {if !$skip_field}{$disabled_param nofilter}{/if} /><span class="radio">{__("address_commercial")}</span>

        {else}  {* Simple input *}
            <input type="text" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" size="32" value="{$value}" class="input-large" {$disabled_param nofilter} />
        {/if}
        </div>
    </div>
{/if}
{/foreach}
{if $body_id}
</div>
{/if}

{/if}