
<form  action="{""|fn_url}" method="post" name="add_section_type_form" class="form-horizontal form-edit  cm-disable-empty-files" enctype="multipart/form-data" id="add_section_type_form">
<div style="margin:0 auto;display:table;padding:10px 80px 0px 0px;margin-top:5%; background:#EFEFEF;">
    <div class="control-group">
        <label for="elm_section_description" class="control-label cm-required">{__("section_name")}:</label>
        <div class="controls">
        <input id="elm_section_description" class="input-large" type="text" name="section_info[section_description]" value="{$single_section_data.0.description}" />
        </div>
    </div>
    <div class="control-group">
         <div class="controls display_none right" id="debit_but">
            {include file="buttons/button.tpl" but_meta="cm-tab-tools" but_text=__("add") but_role="submit-link" but_target_form="add_section_type_form" but_name="dispatch[wk_vendor_custom_fields.custom_sections]" save=true}
        </div>  
    </div> 
</div>   
</form>

