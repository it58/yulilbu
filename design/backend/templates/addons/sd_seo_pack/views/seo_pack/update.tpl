{script src="js/tygh/email_templates.js"}

{assign var="return_url" value=$config.current_url}
{assign var="return_url_escape" value=$return_url|escape:"url"}
{assign var="r_url" value=$return_url_escape|urlencode}

{capture name="mainbox"}

{capture name="tabsbox"}

<form action="{""|fn_url}" method="post" enctype="multipart/form-data" name="seo_pack_form" class="form-horizontal {if ""|fn_check_form_permissions} cm-hide-inputs{/if}">

    <input type="hidden" name="selected_section" id="selected_section" value="{$smarty.request.selected_section}" />
    <input type="hidden" name="result_ids" value="preview_dialog" />
    {if $pattern_data.pattern_id}
        <input type="hidden" name="pattern_data[pattern_id]" value="{$pattern_data.pattern_id}" />
    {/if}
    <input type="hidden" name="pattern_data[type]" value="{$type}" />

    <div id="content_general">


        <div class="control-group">
            <label for="elm_pattern_name" class="cm-required control-label">{__("addons.sd_seo_pack.pattern_name")}:</label>
            <div class="controls">
                <input id="elm_pattern_name" type="text" name="pattern_data[name]" value="{$pattern_data.name}" class="span9 cm-focus">
            </div>
        </div>

        {include file="common/select_status.tpl" input_name="pattern_data[status]" id="elm_pattern_status" obj=$pattern_data hidden=false}

        {if "MULTIVENDOR"|fn_allowed_for && $mode != "add"}
            {assign var="zero_company_id_name_lang_var" value="none"}
            {assign var="js_action" value="fn_change_vendor_for_product(elm);"}
        {/if}

        {if "ULTIMATE"|fn_allowed_for}
            {assign var="companies_tooltip" value=__("addons.sd_seo_pack.text_ult_product_store_field_tooltip")}
        {/if}

        {include file="views/companies/components/company_field.tpl"
            name="pattern_data[company_id]"
            id="product_data_company_id"
            zero_company_id_name_lang_var=$zero_company_id_name_lang_var
            selected=$pattern_data.company_id
            tooltip=$companies_tooltip
            js_action=$js_action
        }

        <div class="control-group">
            <label class="control-label" for="elm_page_title">{__("page_title")}:</label>
            <div class="controls">
                <textarea id="elm_page_title" name="pattern_data[page_title]" cols="55" rows="3" cols="55" rows="8" class="span9 cm-emltpl-set-active">{$pattern_data.page_title}</textarea>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="elm_meta_description">{__("meta_description")}:</label>
            <div class="controls">
                <textarea id="elm_meta_description" name="pattern_data[meta_description]" cols="3" rows="3" cols="55" rows="8" class="span9 cm-emltpl-set-active">{$pattern_data.meta_description}</textarea>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="elm_meta_keywords">{__("meta_keywords")}:</label>
            <div class="controls">
                <textarea id="elm_meta_keywords" name="pattern_data[meta_keywords]" cols="55" rows="4" cols="55" rows="8" class="span9 cm-emltpl-set-active">{$pattern_data.meta_keywords}</textarea>
            </div>
        </div>

        {if $type != $smarty.const.SD_SEO_PACK_TYPE_PAGE}
            <div class="control-group">
                <label class="control-label" for="elm_alt_img">{__("addons.sd_seo_pack.alt_img")}:</label>
                <div class="controls">
                    <textarea id="elm_alt_img" name="pattern_data[alt_img]" cols="55" rows="3" cols="55" rows="8" class="span9 cm-emltpl-set-active">{$pattern_data.alt_img}</textarea>
                </div>
            </div>
        {/if}
        {if $addons.seo.status == 'A'}
            <div class="control-group">
                <label class="control-label" for="elm_seo_name">{__("seo_name")}:</label>
                <div class="controls">
                    <textarea id="elm_seo_name" name="pattern_data[seo_name]" cols="55" rows="3" cols="55" rows="8" class="span9 cm-emltpl-set-active">{$pattern_data.seo_name}</textarea>
                </div>
            </div>
        {/if}
    </div>

    {if $pattern_data.pattern_id}
        <div id="content_update">
            <div class="control-group">
                <label class="control-label" for="elm_rewriting">{__("addons.sd_seo_pack.rewriting")}:</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="hidden" name="pattern_data[rewriting]" value="N" />
                        <input type="checkbox" name="pattern_data[rewriting]" id="elm_rewriting" value="Y" {if $pattern_data.rewriting == "Y"}checked="checked"{/if}/>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_upd_page_title">{__("addons.sd_seo_pack.upd_page_title")}:</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="hidden" name="pattern_data[upd_page_title]" value="N" />
                        <input type="checkbox" name="pattern_data[upd_page_title]" id="elm_upd_page_title" value="Y" {if $pattern_data.upd_page_title == "Y"}checked="checked"{/if}/>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_upd_meta_description">{__("addons.sd_seo_pack.upd_meta_description")}:</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="hidden" name="pattern_data[upd_meta_description]" value="N" />
                        <input type="checkbox" name="pattern_data[upd_meta_description]" id="elm_upd_meta_description" value="Y" {if $pattern_data.upd_meta_description == "Y"}checked="checked"{/if}/>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_upd_meta_keywords">{__("addons.sd_seo_pack.upd_meta_keywords")}:</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="hidden" name="pattern_data[upd_meta_keywords]" value="N" />
                        <input type="checkbox" name="pattern_data[upd_meta_keywords]" id="elm_upd_meta_keywords" value="Y" {if $pattern_data.upd_meta_keywords == "Y"}checked="checked"{/if}/>
                    </label>
                </div>
            </div>
            {if $type != $smarty.const.SD_SEO_PACK_TYPE_PAGE}
                <div class="control-group">
                    <label class="control-label" for="elm_upd_alt_img">{__("addons.sd_seo_pack.upd_alt_img")}:</label>
                    <div class="controls">
                        <label class="checkbox">
                            <input type="hidden" name="pattern_data[upd_alt_img]" value="N" />
                            <input type="checkbox" name="pattern_data[upd_alt_img]" id="elm_upd_alt_img" value="Y" {if $pattern_data.upd_alt_img == "Y"}checked="checked"{/if}/>
                        </label>
                    </div>
                </div>
            {/if}
            {if $addons.seo.status == 'A'}
                <div class="control-group">
                    <label class="control-label">{__("addons.sd_seo_pack.upd_seo_name")}:</label>
                    <div class="controls">
                        <label class="checkbox">
                            <input type="hidden" name="pattern_data[upd_seo_name]" value="N" />
                            <input type="checkbox" class="cm-switch-availability cm-switch-visibility" name="pattern_data[upd_seo_name]" id="sw_elm_upd_seo_name" value="Y" {if $pattern_data.upd_seo_name == "Y"}checked="checked"{/if}/>
                        </label>
                    </div>
                </div>
                <div id="elm_upd_seo_name" {if $pattern_data.upd_seo_name != "Y"}class="hidden"{/if}>
                    <div class="control-group">
                        <label class="control-label" for="elm_seo_create_redirect">{__("seo.create_redirect")}:</label>
                        <div class="controls">
                            <input type="hidden" name="pattern_data[seo_create_redirect]" value="0" />
                            <input type="checkbox" name="pattern_data[seo_create_redirect]" value="1" {if $pattern_data.seo_create_redirect == "Y"}checked="checked"{/if} />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="elm_seo_name_symbols">{__("addons.sd_seo_pack.seo_name_symbols")}{include file="common/tooltip.tpl" tooltip={__("addons.sd_seo_pack.seo_name_symbols_tooltip")}}:</label>
                        <div class="controls">
                            <input type="text" name="pattern_data[seo_name_symbols]" id="elm_seo_name_symbols" size="20" maxlength="32"  value="{$pattern_data.seo_name_symbols}" class="cm-value-integer" />
                        </div>
                    </div>
                </div>
            {/if}
            {if $type == $smarty.const.SD_SEO_PACK_TYPE_PRODUCT}
                {if !$is_vendor}
                    <div class="control-group" id="pattern_categories">
                        {math equation="rand()" assign="rnd"}
                        <label for="ccategories_{$rnd}_ids" class="control-label">{__("categories")}</label>
                        <div class="controls">
                            {include file="pickers/categories/picker.tpl" 
                                hide_input=true 
                                company_ids=$pattern_data.company_id 
                                rnd=$rnd 
                                data_id="categories" 
                                input_name="pattern_data[category_ids]" 
                                item_ids=$pattern_data.category_ids|default:"" 
                                hide_link=true 
                                hide_delete_button=true 
                                display_input_id="category_ids" 
                                disable_no_item_text=true 
                                view_mode="list" 
                                but_meta="btn" 
                                show_active_path=true}
                        </div>
                    <!--pattern_categories--></div>
                {/if}
                <div class="control-group" id="pattern_products">
                    {math equation="rand()" assign="rnd"}
                    <label for="pattern_products" class="control-label">{__("products")}</label>
                    <div class="controls">
                        {include file="pickers/products/picker.tpl"
                            input_name="pattern_data[product_ids]"
                            data_id="pattern_products"
                            item_ids=$pattern_data.product_ids
                            type="links"}
                    </div>
                <!--pattern_products--></div>
            {elseif $type == $smarty.const.SD_SEO_PACK_TYPE_CATEGORY}
                <div class="control-group" id="pattern_categories">
                    {math equation="rand()" assign="rnd"}
                    <label for="ccategories_{$rnd}_ids" class="control-label">{__("categories")}</label>
                    <div class="controls">
                        {include file="pickers/categories/picker.tpl" 
                            hide_input=true 
                            company_ids=$pattern_data.company_id 
                            rnd=$rnd 
                            data_id="categories" 
                            input_name="pattern_data[category_ids]" 
                            item_ids=$pattern_data.category_ids|default:"" 
                            hide_link=true 
                            hide_delete_button=true 
                            display_input_id="category_ids" 
                            disable_no_item_text=true 
                            view_mode="list" 
                            but_meta="btn" 
                            show_active_path=true}
                    </div>
                <!--pattern_categories--></div>
            {else}
                <div class="control-group" id="pattern_pages">
                    {math equation="rand()" assign="rnd"}
                    <label for="pattern_pages" class="control-label">{__("pages")}</label>
                    <div class="controls seo-pack">
                        {include file="pickers/pages/picker.tpl"
                            input_name="pattern_data[page_ids]"
                            data_id="pattern_pages"
                            item_ids=$pattern_data.page_ids
                            multiple=1 
                            type="links"
                            extra_url="&full_search=1&get_tree=multi_level"}
                    </div>
                <!--pattern_pages--></div>
            {/if}
        </div>
    {/if}
</form>

{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}

{capture name="sidebar"}
    <div class="sidebar-row">
        <h6>{__("variables")}</h6>
        <ul class="nav nav-list">
            {foreach from=$variables item="variable"}
                <li><span class="cm-emltpl-insert-variable label hand" data-ca-template-value="{$variable}">{$variable|truncate:35}</span></li>
            {/foreach}
        </ul>
    </div>
{/capture}

{if $pattern_data.pattern_id}
    {capture name="mainbox_title"}
        {"{__("addons.sd_seo_pack.editing_pattern")}: `$pattern_data.name`"|strip_tags}
    {/capture}
    {capture name="buttons"}

        {if $pattern_data.pattern_id}
            {capture name="tools_list"}
                <li>{btn type="list" text=__("addons.sd_seo_pack.generate")  class="cm-confirm" href="seo_pack.generate?pattern_id=`$pattern_data.pattern_id`&lang_code=`$smarty.const.DESCR_SL`&return_url=`$r_url`"}</li>
                <li class="divider"></li>
                <li>{btn type="list" text=__("clone")  href="seo_pack.clone?pattern_ids=`$pattern_data.pattern_id`" method="POST"}</li>
                <li>{btn type="list" text=__("delete") class="cm-confirm" href="seo_pack.delete?pattern_ids=`$pattern_data.pattern_id`" method="POST"}</li>
            {/capture}
            {dropdown content=$smarty.capture.tools_list}
        {/if}

        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_name="dispatch[seo_pack.update]" but_target_form="seo_pack_form" save=1}
    {/capture}
{else}
    {capture name="mainbox_title"}
        {__("add_template")}
    {/capture}
    {capture name="buttons"}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_name="dispatch[seo_pack.add]" but_target_form="seo_pack_form"}
{/capture}
{/if}

{include file="common/mainbox.tpl"
    title=$smarty.capture.mainbox_title
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    sidebar=$smarty.capture.sidebar
    sidebar_position="left"
    select_languages=true
}
