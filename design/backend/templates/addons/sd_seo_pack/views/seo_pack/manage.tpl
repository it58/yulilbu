{notes title=__("notice")}
    <p>{__("addons.sd_seo_pack.export_cron_hint")}:<br />
        <span>php /path/to/cart/{""|fn_url:"A":"rel"} --dispatch=seo_pack_cron.apply --cron_password=CRON_PASSWORD</span>
    </p>
{/notes}
{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="seo_patterns_form" class="{if ""|fn_check_form_permissions} cm-hide-inputs{/if}">
        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
        {assign var="r_url" value=$config.current_url|urlencode}
        {assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
        {assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

        <input type="hidden" name="return_url" var="{$r_url}">
        
        {if $patterns}
            <table class="table table-middle">
                <thead>
                <tr>
                    <th width="1%">
                        {include file="common/check_items.tpl"}
                    </th>
                    <th width="20%">
                        <a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                    </th>
                    <th width="10%">
                        <a class="cm-ajax" href="{"`$c_url`&sort_by=type&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("addons.sd_seo_pack.type")}{if $search.sort_by == "type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                    </th>
                    <th width="9%">&nbsp;</th>
                    <th width="50%">
                        {__("addons.sd_seo_pack.location")}
                    </th>
                    <th width="10%" class="right">
                        <a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                    </th>
                </tr>
                </thead>
                <tbody>
                    {foreach from=$patterns item=pattern}

                        <tr class="cm-row-status-{$pattern.status|lower}">
                            <td>
                                <input name="pattern_ids[]" type="checkbox" value="{$pattern.pattern_id}" class="cm-item" /></td>
                            <td>
                                <a class="row-status" href="{"seo_pack.update?pattern_id=`$pattern.pattern_id`"|fn_url}">{$pattern.name}</a>
                                {include file="views/companies/components/company_name.tpl" object=$pattern}
                            <td>
                                <span class="row-status">{__("addons.sd_seo_pack.pattern_`$pattern.type`")}</span>
                            </td>

                            <td class="right">
                                <div class="hidden-tools">
                                {capture name="tools_list"}
                                    <li>{btn type="list" text=__("addons.sd_seo_pack.generate") class="cm-confirm" href="seo_pack.generate?pattern_id=`$pattern.pattern_id`&lang_code=`$smarty.const.DESCR_SL`&return_url=`$r_url`"}</li>
                                    <li class="divider"></li>
                                    <li>{btn type="list" text=__("edit") href="seo_pack.update?pattern_id=`$pattern.pattern_id`&return_url=`$r_url`"}</li>
                                    <li>{btn type="list" text=__("clone") href="seo_pack.clone?pattern_ids=`$pattern.pattern_id`" method="POST"}</li>
                                    <li>{btn type="list" text=__("delete") class="cm-confirm" href="seo_pack.delete?pattern_ids=`$pattern.pattern_id`&return_url=`$r_url`" method="POST"}</li>
                                {/capture}
                                {dropdown content=$smarty.capture.tools_list}
                                </div>
                            </td>
                            <td>
                                {if $pattern.product_names}
                                    <h6>{__("products")}</h6>
                                    {foreach from=$pattern.product_names key=product_id item=product_name name=product_names}
                                        <a class="row-status" href="{"products.update?product_id=`$product_id`"|fn_url}">{$product_name}</a>{if !$smarty.foreach.product_names.last}, {/if}
                                    {/foreach}
                                {/if}
                                {if $pattern.category_names}
                                    <h6>{__("categories")}</h6>
                                    {foreach from=$pattern.category_names key=category_id item=category_name name=category_names}
                                        <a class="row-status"  href="{"categories.update&category_id=`$category_id`"|fn_url}">{$category_name}</a>{if !$smarty.foreach.category_names.last}, {/if}
                                    {/foreach}
                                {/if}
                                {if $pattern.page_names}
                                    <h6>{__("pages")}</h6>
                                    {foreach from=$pattern.page_names key=page_id item=page_name name=page_names}
                                        <a class="row-status"  href="{"pages.update&page_id=`$page_id`"|fn_url}">{$page_name}</a>{if !$smarty.foreach.page_names.last}, {/if}
                                    {/foreach}
                                {/if}
                            </td>
                            <td class="nowrap right">
                                {include file="common/select_popup.tpl" popup_additional_class="dropleft" display="" id=$pattern.pattern_id status=$pattern.status object_id_name="pattern_id" table="sd_seo_pack_patterns"}
                            </td>
                        </tr>

                    {/foreach}
                </tbody>
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

        {capture name="buttons"}
            {capture name="tools_list"}
                {if $patterns}
                    <li>{btn type="list" text=__("clone_selected") dispatch="dispatch[seo_pack.m_clone]" form="seo_patterns_form"}</li>
                    <li>{btn type="delete_selected" dispatch="dispatch[seo_pack.m_delete]" form="seo_patterns_form"}</li>
                {/if}
            {/capture}
            {dropdown content=$smarty.capture.tools_list}
        {/capture}

        {capture name="adv_buttons"}
            {capture name="tools_list"}
                <li>{btn type="list" text=__("addons.sd_seo_pack.add_pattern_P") href="seo_pack.add?type=P"}</li>
                {if !$is_vendor}
                    <li>{btn type="list" text=__("addons.sd_seo_pack.add_pattern_C") href="seo_pack.add?type=C"}</li>
                {/if}
                <li>{btn type="list" text=__("addons.sd_seo_pack.add_pattern_PA") href="seo_pack.add?type=PA"}</li>
            {/capture}
            {dropdown content=$smarty.capture.tools_list icon="icon-plus" no_caret=true placement="right"}
        {/capture}

    </form>
{/capture}
{include file="common/mainbox.tpl"
    title=__("addons.sd_seo_pack.patterns")
    content=$smarty.capture.mainbox
    tools=$smarty.capture.tools
    select_languages=true
    buttons=$smarty.capture.buttons
    adv_buttons=$smarty.capture.adv_buttons
}