{if $order_info.otp_data.phone}
<div class="control-group shift-top">
    <div class="control-label">
        {include file="common/subheader.tpl" title=__("otp_information")}
    </div>
</div>
<div class="control-group">
    <div class="control-label">{__("phone")}</div>
    <div class="controls">
        {$order_info.otp_data.phone}
    </div>
</div>
{/if}