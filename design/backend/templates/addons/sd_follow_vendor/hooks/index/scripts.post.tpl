<script type="text/javascript">
(function(_, $) {

    $(document).ready(function() {
        $(_.doc).on('click', '.cm-status-switch', function (e) {
            e.stopImmediatePropagation();

            var jelm = $(e.target),
                productData = $("input[name='product_id']").val(),
                companyData = $('#product_data_company_id').val(),
                companyId = (typeof companyData !== 'undefined') ? companyData : $('#compId_' + jelm.data('caPostId')).val(),
                productId = (typeof productData !== 'undefined') ? productData : $('#prodId_' + jelm.data('caPostId')).val();
            
            $.ceAjax('request', fn_url('tools.update_status'), {
                method: 'post',
                obj: jelm,
                status: jelm.data('caStatus'),
                data: {
                    'table': 'discussion_posts',
                    'product_id': productId,
                    'company_id': companyId,
                    'id_name': 'post_id',
                    'id': jelm.data('caPostId'),
                    'status': jelm.data('caStatus')
                },
                callback: function (data, params) {
                    if (params.obj) {
                        var $ = Tygh.$;
                        var status = (data.return_status) ? data.return_status : params.status;
                        var s_elm = $(params.obj).parents('.cm-statuses:first');
                        s_elm.children('[class^="cm-status-"]').hide();
                        s_elm.children('.cm-status-' + status.toLowerCase()).show();
                    }
                }
            });
        });
    });

}(Tygh, Tygh.$));
</script>