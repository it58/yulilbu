{capture name="mainbox"}
    <div id="content_vendor_events" class="table-responsive-wrapper">
        <form action="{""|fn_url}" method="post" name="follower">

            <table class="table table-middle table-responsive">
                <thead class="cm-first-sibling">
                    <tr>
                        <th class="left" width="1%">{include file="common/check_items.tpl"}</th>
                        <th width="20%">{__("event_name")}</th>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$events item=event}
                    {assign var="e_id" value=$event.event_id}

                    <tr id="container_vendor_events">
                        <td class="left" data-th="{__("show")}">
                            <input type="hidden" name="event_list[{$e_id}]" value="0" />
                            <input type="checkbox" class="cm-item" {if $event.is_selected}checked="checked"{/if} value="1" name="event_list[{$e_id}]" />
                        </td>
                        <td data-th="{__("event_name")}">{__($event.lang_var)}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>

            {capture name="buttons"}
                {include file="buttons/save.tpl" but_name="dispatch[follower.update]" but_role="submit-link" but_target_form="follower"}
            {/capture}
        </form>
    </div>
{/capture}

{include file="common/mainbox.tpl" title=__("event_list") buttons=$smarty.capture.buttons content=$smarty.capture.mainbox}
