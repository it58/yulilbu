{capture name="mainbox"}
<form action="{""|fn_url}" method="post" id="forbidden_words_form" class="form-horizontal form-edit cm-disable-empty-files" name="forbidden_words_form" enctype="multipart/form-data">
    <table class="table table-middle forbidden-words-table">
        <thead class="cm-first-sibling">
            <tr>
                <th>{__("addons.sd_messaging_system.forbidden_word")}</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$forbidden_words item="word" key="_key"}
                <tr class="cm-row-item">
                    <td>
                        <input type="text" name="forbidden_words[{$_key}]" value="{$word}" />
                    </td>
                    <td class="nowrap right">
                        {include file="buttons/clone_delete.tpl" microformats="cm-delete-row" no_confirm=true}
                    </td>
                </tr>
            {/foreach}
            {math equation="x+1" x=$_key|default:0 assign="new_key"}
            <tr class="{cycle values="table-row , " reset=1}" id="box_add_forbidden_word">
                <td width="50%">
                    <input type="text" name="forbidden_words[{$new_key}]" value=""/>
                </td>
                <td class="right">
                    {include file="buttons/multiple_buttons.tpl" item_id="add_forbidden_word"}
                </td>
            </tr>
        </tbody>
    </table>
    {capture name="buttons"}
        {include file="buttons/save.tpl" but_name="dispatch[messenger.update_forbidden_words]" but_role="submit-link" but_target_form="forbidden_words_form"}
    {/capture}
</form>
{/capture}

{include file="common/mainbox.tpl" title=__("messenger_forbidden_words") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}