{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="tickets_list_form" class="{if ""|fn_check_form_permissions} cm-hide-inputs{/if}">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

{if $tickets}

<table class="table table-middle">
<thead>
    <tr>
        <th width="1%">
            {include file="common/check_items.tpl"}
        </th>
        <th width="9%">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=ticket_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("ticket_id")}{if $search.sort_by == "ticket_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>
        <th width="10%" class="nowrap center">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=author&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("author")}{if $search.sort_by == "author"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>
        <th width="10%" class="nowrap center">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=recipient&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("recipient")}{if $search.sort_by == "recipient"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>
        <th width="35%">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=last_message&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("last_message")}{if $search.sort_by == "last_message"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>
        <th width="10%">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=last_message_date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("last_message_date")}{if $search.sort_by == "last_message_date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>
        <th width="10%">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=amount&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("messages_amount")}{if $search.sort_by == "amount"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>
        {if !$runtime.company_id}<th width="10%">
            <a class="cm-ajax" href="{"`$c_url`&sort_by=conpany_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("company")}{if $search.sort_by == "company"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
        </th>{/if}
        <th width="5%">&nbsp;</th>
        <th width="5%">&nbsp;</th>
    </tr>
</thead>

{foreach from=$tickets item=ticket}

    {assign var="allow_save" value=$promotion|fn_allow_save_object:"promotions"}

    {if $allow_save}
        {assign var="link_text" value=__("edit")}
        {assign var="additional_class" value="cm-no-hide-input"}
        {assign var="status_display" value=""}
    {else}
        {assign var="link_text" value=__("view")}
        {assign var="additional_class" value="cm-hide-inputs"}
        {assign var="status_display" value="text"}
    {/if}

<tr class="cm-row-status-{$promotion.status|lower} {$additional_class} {if $ticket.last_message && $ticket.status == $smarty.const.TICKET_STATUS_NEW}ty-sd_messaging__ticket-new{/if}">
    <td>
        <input name="ticket_ids[]" type="checkbox" value="{$ticket.ticket_id}" class="cm-item" /></td>
    <td>
        <a class="row-status" href="{"messenger.view?ticket_id=`$ticket.ticket_id`"|fn_url}">#{$ticket.ticket_id}</a>
    <td class="center">
        {if $ticket.author_type == $smarty.const.AUTHOR_TYPE_VENDOR}
            <a class="row-status" href="{"companies.update&company_id=`$ticket.author_id`"|fn_url}">{$ticket.author_name}</a>
        {else}
            <a class="row-status" href="{"profiles.update&user_id=`$ticket.author_id`"|fn_url}">{$ticket.author_name}</a>
        {/if}
    </td>
    <td class="center">
        {if $ticket.author_type == $smarty.const.AUTHOR_TYPE_VENDOR}
            <a class="row-status" href="{"profiles.update&user_id=`$ticket.recipient_id`"|fn_url}">{$ticket.recipient_name}</a>
        {else}
            <a class="row-status" href="{"companies.update&company_id=`$ticket.recipient_id`"|fn_url}">{$ticket.recipient_name}</a>
        {/if}
    </td>
    <td>
        {math equation="x+y" x=$addons.sd_messaging_system.trimmed_messages_length y=$smarty.const.TRIPLE_DOT_LENGHT assign="message_length"}
        <span class="row-status ty-last-message">{$ticket.last_message|strip_tags|truncate:$message_length nofilter}</span>
    </td>
    <td>
        <span class="row-status">{$ticket.last_message_date}</span>
    </td>
    <td>
        <span class="row-status">{$ticket.amount}</span>
    </td>
    {if !$runtime.company_id}<td>
        <a class="row-status" href="{"companies.update&company_id=`$ticket.company_id`"|fn_url}">{$ticket.company}</a>
    </td>{/if}
    <td>{if !empty($ticket.need_highlight) && $ticket.need_highlight == 'Y' && $smarty.session.auth.user_type == 'A'}<span class="icon-exclamation-sign text-error cm-tooltip" title="{__('addons.sd_messaging_system.highlighted_ticket_tooltip')}"></span>{/if}</td>
    <td class="nowrap">
        {capture name="tools_list"}
            <li>{btn type="list" text=__("delete") class="cm-confirm" href="messenger.delete_ticket?ticket_id=`$ticket.ticket_id`" method="POST"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl"}

</form>
{/capture}

{capture name="buttons"}
    {if $tickets}
        {capture name="tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[messenger.m_delete_tickets]" form="tickets_list_form"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
{/capture}

{capture name="sidebar"}
    {include file="common/saved_search.tpl" dispatch="messenger.tickets_list" view_type="tickets"}
    {include file="addons/sd_messaging_system/views/messenger/components/tickets_search_form.tpl" dispatch="messenger.tickets_list" form_meta=""}
{/capture}

{include file="common/mainbox.tpl" title=__("messages") content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar tools=$smarty.capture.tools buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}
