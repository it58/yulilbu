<div class="sidebar-row">
    <form name="messages_search_form" action="{""|fn_url}" method="get" class="{$form_meta}">
        {capture name="simple_search"}
        <input type="hidden" name="ticket_id" value="{$ticket.ticket_id}" />

        {if $smarty.request.redirect_url}
        <input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
        {/if}

        {if $selected_section != ""}
        <input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
        {/if}

        {if $put_request_vars}
            {array_to_fields data=$smarty.request skip=["callback"] escape=["data_id"]}
        {/if}

        {$extra nofilter}
        <div class="sidebar-field">
            <label for="elm_message">{__("message")}</label>
            <div class="break">
                <input type="text" name="message" id="elm_message" value="{$search.message}" />
            </div>
        </div>
        {/capture}

        {capture name="advanced_search"}
        <div class="group form-horizontal">
            <div class="control-group">
                <label class="control-label">{__("period")}</label>
                <div class="controls">
                    {include file="common/period_selector.tpl" period=$search.period form_name="messages_search_form"}
                </div>
            </div>
        </div>
        {/capture}

        {include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="ticket_messages" in_popup=$in_popup}
    </form>

</div><hr>