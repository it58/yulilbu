<div class="sidebar-row">
    <form name="tickets_search_form" action="{""|fn_url}" method="get" class="{$form_meta}">
        {capture name="simple_search"}
        {if $smarty.request.redirect_url}
        <input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
        {/if}

        {if $selected_section != ""}
        <input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
        {/if}

        {if $put_request_vars}
            {array_to_fields data=$smarty.request skip=["callback"] escape=["data_id"]}
        {/if}

        {$extra nofilter}
        <div class="sidebar-field">
            <label for="elm_author">{__("ticket_id")}</label>
            <div class="break">
                <input type="text" name="ticket_id" id="elm_author_name" value="{$search.ticket_id}" />
            </div>
        </div>
        <div class="sidebar-field">
            <label for="elm_author">{__("author")}</label>
            <div class="break">
                <input type="text" name="author" id="elm_author_name" value="{$search.author}" />
            </div>
        </div>
        <div class="sidebar-field">
            <label for="elm_recipient">{__("recipient")}</label>
            <div class="break">
                <input type="text" name="recipient" id="elm_recipient" value="{$search.recipient}" />
            </div>
        </div>
        {if $auth.user_type == "A" && !$runtime.company_id}
            <div class="sidebar-field">
                <label for="elm_company">{__("company")}</label>
                <div class="break">
                    <input type="text" name="company" id="elm_company" value="{$search.company}" />
                </div>
            </div>
        {/if}
        {/capture}

        {capture name="advanced_search"}
        <div class="group form-horizontal">
            <div class="control-group">
                <label class="control-label">{__("period")}</label>
                <div class="controls">
                    {include file="common/period_selector.tpl" period=$search.period form_name="tickets_search_form"}
                </div>
            </div>
        </div>
        {/capture}

        {include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="tickets" in_popup=$in_popup}
    </form>

</div><hr>
