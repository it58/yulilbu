{capture name="mainbox"}

{assign var=last_message_id value=0}

<div class="ticket-messages-block" id="ticket_messages_block">
    <div class="ticket-messages-list" data-ticket-id="{$ticket.ticket_id}" id="ticket_messages_list">
        {if $messages}
            {foreach from=$messages item=message}
                {if $message.message_id > $last_message_id}
                    {assign var=last_message_id value=$message.message_id}
                {/if}

                {if $message.author_type == "V"}
                    {assign var=message_class value="author-message"}
                {else}
                    {assign var=message_class value="recipient-message has-avatar"}
                {/if}

                <div class="ty-sd_messaging_system-message__content {$message_class} ty-mb-l">
                    <div class="ty-sd_messaging_system-message__author">
                        {include file="addons/sd_messaging_system/views/messenger/components/user_image.tpl" message=$message ticket=$ticket}
                    </div>

                    <div class="ty-sd_messaging_system-all">
                        <div class="ty-sd_messaging_system-name">
                            {if $message.author_type == "V" && $ticket.vendor_name}
                                {$ticket.vendor_name}
                            {elseif $message.author_type != "V" && $ticket.customer_name}
                                {$ticket.customer_name}
                            {/if}
                            <span class="ty-sd_messaging_system-message__date" data-message-timestamp="{$message.timestamp}">{$message.date}</span>
                        </div>

                        <div class="ty-sd_messaging_system-message" id="message_text">
                            <div class="ty-sd_messaging_system-message__message {if !empty($message.need_highlight) && $message.need_highlight == 'Y' && $smarty.session.auth.user_type == 'A'}text-error{/if}">
                                {$message.message|nl2br|strip nofilter}
                            </div>
                        </div>
                    </div>

                </div>
            {/foreach}
        {else}
            {if $ticket.customer_name}
                {assign var=no_messages_found value=__("no_messages_found_with_recipient", ["[recipient_name]" => $ticket.customer_name])}
            {else}
                {assign var=no_messages_found value=__("no_messages_found")}
            {/if}

            <p class="ty-no-messages">{$no_messages_found}</p>
        {/if}
    </div>
</div>

<div class="center hidden" id="message_spinner">{__('addons.sd_messaging_system.waiting_for_connect')} <img src="{$self_images_dir}/images/spinner.gif" width="20" height="20"/></div>
<div class="ty-discussion-post__buttons buttons-container">
    <form id="new_message_form" class="ty-discussion-post__form">
        <textarea id="new_message_field" name="message_body" rows="4" cols="50" placeholder="{__('write_your_message_here')}" class="ty-sd_messaging_system-textarea" disabled="disabled">{$attachment_message nofilter}</textarea>
        <a href="#" onclick="history.back();" class="btn btn-secondary">{__('go_back')}</a>
        <input type="hidden" name="ticket_id" value="{$ticket.ticket_id}"/>
        <input type="hidden" name="author_id" value="{$auth.company_id}"/>
        <input type="hidden" name="last_message_id" value="{$last_message_id}"/>
        <button id="send_message_but" class="btn btn-primary btn-right ty-btn" type="submit" name="dispatch[messenger.send_message]" onclick="return false;" disabled="disabled">{__('send')}</button>
    </form>
</div>

{/capture}

{capture name="buttons"}
    {if $messages}
        {assign var="delete_all_url" value=fn_url("messenger.delete_all_messages&ticket_id=`$smarty.request.ticket_id`")}
        {include file="buttons/button.tpl" but_gtoup="ture" but_text=__("delete_all_messages") but_role="action" but_href=$delete_all_url but_meta="btn btn-primary cm-confirm"}
    {/if}
{/capture}

{capture name="sidebar"}
    {include file="common/saved_search.tpl" dispatch="messenger.view" view_type="ticket_messages" view_suffix="ticket_id=`$ticket.ticket_id`"}
    {include file="addons/sd_messaging_system/views/messenger/components/messages_search_form.tpl" dispatch="messenger.view" form_meta=""}
{/capture}

{include file="common/mainbox.tpl" title=__("messages") sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox tools=$smarty.capture.tools buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}

{assign var=vendor_image_data value=$ticket.vendor_image|fn_image_to_display:$smarty.const.USER_IMAGE_WIDTH:$smarty.const.USER_IMAGE_HEIGHT}
{assign var=customer_image_data value=$ticket.customer_image|fn_image_to_display:$smarty.const.USER_IMAGE_WIDTH:$smarty.const.USER_IMAGE_HEIGHT}

<input type="hidden" name="websocket_url" value="{$addons.sd_messaging_system.websocket_url}">

<div id="sd-messenger-sender-data" class="hidden"
    data-name="{$ticket.vendor_name|default:""}"
    data-id={$ticket.company_id}
    data-type="V"
    data-has-image="{if $vendor_image_data}Y{else}N{/if}"
    data-image-path="{$vendor_image_data.image_path|default:""}"
    data-image-alt="{$vendor_image_data.alt|default:""}">
</div>

<div id="sd-messenger-recipient-data" class="hidden"
    data-name="{$ticket.customer_name|default:""}"
    data-id={$ticket.customer_id}
    data-type="C"
    data-has-image="{if $customer_image_data}Y{else}N{/if}"
    data-image-path="{$customer_image_data.image_path|default:""}"
    data-image-alt="{$customer_image_data.alt|default:""}">
</div>

{script src="js/addons/sd_messaging_system/web_socket.js"}
{script src="js/addons/sd_messaging_system/messenger.js"}
