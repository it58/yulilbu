<div class="control-group setting-wide">
    <label for="text" class="control-label">{__(autocheck_messages_time)}&nbsp;<a class="cm-tooltip" title="{__(tooltips_autocheck_messages_time)}"><i class="icon-question-sign"></i></a></label>
    <div class="controls">
        <input type="text" size="4" id="addon_option_sd_messaging_system_autocheck_messages_time" name="addon_data[other_options][autocheck_messages_time]" value="{$addons.sd_messaging_system.autocheck_messages_time}" class="input-micro" />&nbsp;{__("seconds")}
    </div>
</div>