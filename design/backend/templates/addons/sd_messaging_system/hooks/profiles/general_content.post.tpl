{if $smarty.request.user_type != 'C' && $user_data.is_root != 'Y'}
    <div class="control-group">
        <label for="elm_messenger_permission" class="control-label">{__("messenger_permission")}:</label>
        <div class="controls">
            <input type="hidden" name="user_data[messenger_permission]" value="N" />
            <input type="checkbox" name="user_data[messenger_permission]" value="Y" {if $user_data.messenger_permission == "Y"}checked="checked"{/if} {if $auth.is_root != 'Y'}disabled="disabled"{/if} />
        </div>
    </div>
{/if}