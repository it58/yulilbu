{if $addons.vendor_plans.status == 'A'}
    {include file="common/subheader.tpl" title=__("messenger_system.messenger_for_vendor") target="#messenger_vendor_setting"}
    <div id="messenger_vendor_setting" class="in collapse">
        <fieldset>

        {if !$runtime.company_id}
            <div class="control-group">
                <label class="control-label cm-required">{__("status")}:</label>
                <div class="controls">
                    <label class="radio inline" for="company_data_messenger_{$obj_id|default:0}_a"><input type="radio" name="company_data[messenger]" id="{$id}_{$obj_id|default:0}_a" {if $company_data.messenger == "A"}checked="checked"{/if} value="A" />{__("active")}</label>
                    <label class="radio inline" for="company_data_messenger_{$obj_id|default:0}_d"><input type="radio" name="company_data[messenger]" id="{$id}_{$obj_id|default:0}_d" {if $company_data.messenger == "D"}checked="checked"{/if} value="D" />{__("disabled")}</label>
                </div>
            </div>
        {else}
            <div class="control-group">
                <label class="control-label">{__("status")}:</label>
                <div class="controls">
                    <label class="radio"><input type="radio" checked="checked" />{if $company_data.messenger == "A"}{__("active")}{elseif $company_data.messenger == "D"}{__("disabled")}{/if}</label>
                </div>
            </div>
        {/if}

        </fieldset>
    </div>
{/if}