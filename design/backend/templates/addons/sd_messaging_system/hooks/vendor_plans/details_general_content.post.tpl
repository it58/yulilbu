<div class="control-group">
    <label class="control-label" for="elm_messenger_{$id}">{__("messenger_system.messenger_for_plans")}:</label>
    <div class="controls">
        <input type="hidden" name="plan_data[messenger]" value="N" />
        <input type="checkbox" id="elm_messenger_{$id}" name="plan_data[messenger]" size="10" value="Y"{if $plan.messenger == "Y"} checked="checked"{/if} />
    </div>
</div>
