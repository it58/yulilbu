<script>
    (function (_, $) {
        _.tr({
            'addons.sd_messaging_system.reload_page': '{__("addons.sd_messaging_system.reload_page")|escape:"javascript"}'
        });
    }(Tygh, Tygh.$));
</script>
