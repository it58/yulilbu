{if $order_info.payment_id && $order_info|fn_sd_stripe_pay_by_strip_bitcoin}
    <div class="control-group shift-top">
        <div class="control-label">
            {include file="common/subheader.tpl" title=__('sd_stripe_order_on_check')}
        </div>
    </div>
    {if $order_info.payment_info.bitcoin_amount_received < $order_info.payment_info.bitcoin_amount}
        {literal}
        <script>
            function sd_stripe_add_to_check(order_id) {
                $.ceAjax('request', fn_url("orders.add_to_check"), {
                    data: {
                        order_id: order_id,
                        result_ids: 'id_info_on_check_bitcoin',
                    },
                    method: 'get',
                });
            }
        </script>
        {/literal}
        {include file="addons/sd_stripe/views/orders/components/info_on_check_bitcoin.tpl" info_on_check_bitcoin=$order_info.info_on_check_bitcoin }
    {else}
        <div class="control-group">
            {__('sd_strip_order_was_check')}
        </div>
    {/if}
{/if}