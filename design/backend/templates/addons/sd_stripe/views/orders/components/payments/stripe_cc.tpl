{assign var="count_enable" value=0}
{if $cart.active_stripe_tab.{$cart.payment_id}}
    {assign var="active_stripe_tab" value=$cart.active_stripe_tab.{$cart.payment_id}}
{/if}

{if $payment_method.processor_params.enable_card == 'Y'}
    {assign var="count_enable" value=($count_enable+1)}
    {if !$active_stripe_tab}
        {assign var="active_stripe_tab" value='stripe'}
    {/if}
{/if}
{if $payment_method.processor_params.enable_bitcoin == 'Y'}
    {assign var="count_enable" value=($count_enable+1)}
    {if !$active_stripe_tab}
        {assign var="active_stripe_tab" value='bitcoin'}
    {/if}
{/if}
{if $payment_method.processor_params.enable_alipay == 'Y'}
    {assign var="count_enable" value=($count_enable+1)}
    {if !$active_stripe_tab}
        {assign var="active_stripe_tab" value='alipay'}
    {/if}
{/if}

{if $count_enable > 1}
    <div class="control-group">
        <label class="control-label" for="payment_methods">{__('type')}</label>
        <div class="controls">
        <select name="active_stripe_tab" id="id_active_stripe_tab"
            class="cm-submit cm-ajax cm-skip-validation"
            data-ca-dispatch="dispatch[order_management.active_stripe_tab]">
                {if $payment_method.processor_params.enable_card == 'Y'}
                    <option id="stripe_cart" value="stripe" {if $active_stripe_tab == 'stripe'}selected="selected"{/if} >
                        {__('credit_card')}
                    </option>
                {/if}
                {if $payment_method.processor_params.enable_bitcoin == 'Y'}
                    <option id="stripe_bitcoin" value="bitcoin" {if $active_stripe_tab == 'bitcoin'}selected="selected"{/if} >
                        {__('bitcoin')}
                    </option>
                {/if}
                {if $payment_method.processor_params.enable_alipay == 'Y'}
                    <option id="stripe_alipay" value="alipay" {if $active_stripe_tab == 'alipay'}selected="selected"{/if} >
                        {__('alipay')}
                    </option>
                {/if}
        </select>
        </div>
    </div>
{/if}

<div id="stripe_tabs" class="cm-tabs-content cm-j-content-disable-convertation tabs-content clearfix">
    {if $active_stripe_tab == 'stripe'}
        <div id="stripe_block">
        <input type="hidden" name="payment_info[stripe_type_flag]" value="stripe"/>
        {if $user_stripe_credit_cards}
        <div class="stripe-payment-tpl">
            <ul>
                {foreach from=$user_stripe_credit_cards item="card" name="credit_cards"}
                    <li>
                        <label class="ty-form-builder__radio-label">
                            <input id="stripe_card_{$card.id}" class="ty-form-builder__radio radio stripe-cm-select-credit-card" type="radio" name="payment_info[card_id]" value="{$card.id}" {if $smarty.foreach.credit_cards.first}checked="checked"{/if} />
                            <span>{$card.brand}&nbsp;<em>....</em>{$card.last4}</span>
                        </label>
                    </li>
                {/foreach}

                <li>
                    <label class="ty-form-builder__radio-label">
                        <input id="stripe_card_new" class="radio valign stripe-cm-select-credit-card" type="radio" name="payment_info[card_id]" value="" />
                        {__('use_new_credit_card')}
                    </label>
                </li>
            </ul>

            <div id="stripe_new_cc" class="hidden">
            </div>
        </div>
        {else}
            <div class="clearfix" id="stripe_block">
            {if $card_id}
                {assign var="id_suffix" value="`$card_id`"}
            {else}
                {assign var="id_suffix" value=""}
            {/if}

            {if $smarty.request.stripeToken && $smarty.request.stripeEmail}
                <input type="hidden" name="payment_info[stripeToken]" value="{$smarty.request.stripeToken}"/>
                <input type="hidden" name="payment_info[stripeEmail]" value="{$smarty.request.stripeEmail}"/>
            {/if}
            <input type="hidden" name="payment_info[tab_id]" value="{$tab_id}"/>

        {/if}
    <!--stripe_block--></div>
    {elseif $active_stripe_tab == 'bitcoin'}

        <div class="clearfix" id="bitcoin_block">
            {if $card_id}
                {assign var="id_suffix" value="`$card_id`"}
            {else}
                {assign var="id_suffix" value=""}
            {/if}

            <input type="hidden" name="payment_info[stripe_type_flag]" value="bitcoin"/>
            <div class="cm-cc_form_{$id_suffix}">
                <div class="control-group">
                    <label for="id_bitcoin_email_{$id_suffix}" class="control-label cm-required cm-email">{__("email")}</label>
                    <div class="controls">
                        <input type="text" id="id_bitcoin_email_{$id_suffix}" name="payment_info[bitcoin_email]" size="20" value= {if $cart.user_data.email}"{$cart.user_data.email}"{else}"{__("enter_email")}"{/if} class="{if !$user_info.email}cm-hint{/if} input-big" />
                    </div>
                </div>
            </div>
            <div class="sd-stripe-info-about-check">
                <b>{__('attention')}</b>&nbsp;{__('sd_stripe_info_about_check')}
            </div>
        <!--bitcoin_block--></div>
    {elseif $active_stripe_tab == 'alipay'}
        <div class="clearfix" id="alipay_block">
            {if $card_id}
                {assign var="id_suffix" value="`$card_id`"}
            {else}
                {assign var="id_suffix" value=""}
            {/if}

            {if $smarty.request.stripeToken && $smarty.request.stripeEmail}
                <input type="hidden" name="payment_info[stripeToken]" value="{$smarty.request.stripeToken}"/>
                <input type="hidden" name="payment_info[stripeEmail]" value="{$smarty.request.stripeEmail}"/>
            {/if}
            <input type="hidden" name="payment_info[stripe_type_flag]" value="alipay"/>
            <input type="hidden" name="payment_info[tab_id]" value="{$tab_id}"/>
        <!--alipay_block--></div>
    {/if}
<!--stripe_tabs--></div>