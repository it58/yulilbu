<div id="id_info_on_check_bitcoin">
    {if $info_on_check_bitcoin}
        <div class="control-group">
            <div class="control-label">
                {__('sd_stripe_have_to_check')}
            </div>
            <div class="controls">
                {$info_on_check_bitcoin.have_to_check}
            </div>
        </div>
    {else}
        <div class="control-group">
            <a class="btn" onclick="sd_stripe_add_to_check({if $order_info.parent_order_id}{$order_info.parent_order_id}{else}{$order_info.order_id}{/if})">{__('sd_stripe_to_check')}</a>
        </div>
    {/if}
<!--id_info_on_check_bitcoin--></div>