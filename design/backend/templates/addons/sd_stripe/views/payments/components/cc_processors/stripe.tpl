<div class="sd-stripe-payment-settings">

    {$currencies=fn_sd_stripe_get_currencies()}

    {if !empty($currencies)}
        <div class="control-group">
            <label class="control-label" for="currency">{__("currency")}:</label>

            <div class="controls">
                <select name="payment_data[processor_params][currency]" id="currency">
                    {foreach from=$currencies item="currency"}
                        <option value="{$currency}"{if $processor_params.currency == $currency} selected="selected"{/if}>{__("currency_code_`$currency|lower`")}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    {/if}

    <div class="control-group">
        <label class="control-label" for="id_enable_card">&nbsp;{__('enable_card')}{include file="common/tooltip.tpl" tooltip=__('enable_card_tooltip')}:</label>
        <div class="controls">
            <input type="hidden" name="payment_data[processor_params][enable_card]" value="N">
            <input type="checkbox" name="payment_data[processor_params][enable_card]" id="id_enable_card" value="Y" {if $processor_params.enable_card == "Y"}checked="checked"{/if}>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="id_enable_bitcoin">{__('enable_bitcoin')}{include file="common/tooltip.tpl" tooltip=__('enable_bitcoin_tooltip')}:</label>
        <div class="controls">
            <input type="hidden" name="payment_data[processor_params][enable_bitcoin]" value="N">
            <input type="checkbox" name="payment_data[processor_params][enable_bitcoin]" id="id_enable_bitcoin" value="Y" {if $processor_params.enable_bitcoin == "Y"}checked="checked"{/if}>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="id_enable_alipay">{__('enable_alipay')}{include file="common/tooltip.tpl" tooltip=__('enable_alipay_tooltip')}:</label>

        <div class="controls">
            <input type="hidden" name="payment_data[processor_params][enable_alipay]" value="N">
            <input type="checkbox" name="payment_data[processor_params][enable_alipay]" id="id_enable_alipay" value="Y" {if $processor_params.enable_alipay == "Y"}checked="checked"{/if}>
            <p class="description">
                <small>{__("alipay_payment_note")}</small>
            </p>
        </div>
    </div>
</div>

