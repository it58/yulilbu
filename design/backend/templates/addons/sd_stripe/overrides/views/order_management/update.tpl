{assign var="result_ids" value="om_ajax_*,actions_panel,stripe_tabs"}
{script src="js/tygh/order_management.js"}
{script src="js/tygh/exceptions.js"}

<div class="hidden">
    {$users_shared_force = false}
    {if "ULTIMATE"|fn_allowed_for}
        {if $settings.Stores.share_users == "Y"}
            {$users_shared_force = true}
        {/if}
    {/if}
    {include file="views/order_management/components/customer_info_update.tpl"}
</div>

<form action="{""|fn_url}" method="post" class="form-table" name="om_cart_form" enctype="multipart/form-data">
{$ORDER_MANAGEMENT}
<input type="hidden" name="result_ids" value="{$result_ids}" />

{capture name="sidebar"}
    {if $cart.order_id || $cart.user_data}
        {assign var="is_edit" value=true}
    {/if}
    <div id="om_ajax_customer_info">
        {* Issuer info*}
        {*include file="views/order_management/components/issuer_info.tpl" user_data=$cart.issuer_data*}
        {* Customer info *}
        {include file="views/order_management/components/profiles_info.tpl" user_data=$cart.user_data location="O" is_edit=$is_edit}
    <!--om_ajax_customer_info--></div>
{/capture}

{capture name="mainbox"}

<div class="row-fluid orders-wrap">
    <div class="span8">
        <div  class="cm-om-totals" id="om_ajax_update_totals">
        {if $is_empty_cart}
        <label class="hidden cm-required" for="products_required">{__("products_required")}</label>
        <input type="hidden" id="products_required" name="products_required" value="" />
        {/if}

        {* Products *}
        {include file="views/order_management/components/products.tpl"}
        <hr>
        <div class="row-fluid">
            <div class="span6">
            {* Discounts *}
            {include file="views/order_management/components/discounts.tpl"}
            {hook name="order_management:totals_extra"}
            {/hook}
            </div>

            <div class="span6">
            {* Totals *}
            {include file="views/order_management/components/totals.tpl"}
            </div>
        </div>
        <!--om_ajax_update_totals--></div>

        <div class="note clearfix">
            <div class="span6">
                <label for="customer_notes">{__("customer_notes")}</label>
                <textarea class="span12" name="customer_notes" id="customer_notes" cols="40" rows="5">{$cart.notes}</textarea>
            </div>
            <div class="span6">
                <label for="order_details">{__("staff_only_notes")}</label>
                <textarea class="span12" name="update_order[details]" id="order_details" cols="40" rows="5">{$cart.details}</textarea>
            </div>
        </div>

        <div class="clearfix">
            {$notify_customer_status = false}
            {$notify_department_status = false}
            {$notify_vendor_status = false}

            {hook name="order_management:notify_checkboxes"}
                <div class="control-group">
                    <label for="notify_user" class="checkbox">{__("notify_customer")}
                    <input type="checkbox" class="" {if $notify_customer_status == true} checked="checked" {/if} name="notify_user" id="notify_user" value="Y" /></label>
                </div>
                <div class="control-group">
                    <label for="notify_department" class="checkbox">{__("notify_orders_department")}
                    <input type="checkbox" class="" {if $notify_department_status == true} checked="checked" {/if} name="notify_department" id="notify_department" value="Y" /></label>
                </div>
                {if fn_allowed_for("MULTIVENDOR")}
                <div class="control-group">
                    <label for="notify_vendor" class="checkbox">{__("notify_vendor")}
                    <input type="checkbox" class="" {if $notify_vendor_status == true} checked="checked" {/if} name="notify_vendor" id="notify_vendor" value="Y" /></label>
                </div>
                {/if}
            {/hook}
        </div>
    </div>

    <div class="span4">
        <div class="well orders-right-pane form-horizontal">
            {* Status *}
            <div class="statuses">
                {include file="views/order_management/components/status.tpl"}
            </div>
            {* Payment method *}
            <div class="payments" id="om_ajax_update_payment">
                {include file="views/order_management/components/payment_method.tpl"}
            <!--om_ajax_update_payment--></div>

            {* Shipping method*}
            <div class="shippings" id="om_ajax_update_shipping">
                {include file="views/order_management/components/shipping_method.tpl"}
            <!--om_ajax_update_shipping--></div>
        </div>
    </div>
</div>

{/capture}

{capture name="buttons"}
{* Order buttons *}
    {if $cart.order_id == ""}
        {$_but_text = __("create")}
        {$but_text_ = __("create_process_payment")}
        {$_title = __("create_new_order")}
    {else}
        {$_but_text = __("save")}
        {$but_text_ = __("save_process_payment")}
        {$_title = "{__("editing_order")}: #`$cart.order_id`"}
        {$but_check_filter = "label:not(#om_ajax_update_payment)"}
        {include file="buttons/button.tpl" but_text=__("cancel") but_role="action" but_href="orders.details?order_id=`$cart.order_id`"}
    {/if}

    {include file="buttons/button.tpl" but_text=$_but_text but_name="dispatch[order_management.place_order.save]" but_role="button_main"}

    {if $smarty.request.active_stripe_tab}
        {assign var="active_stripe_tab" value=$smarty.request.active_stripe_tab}
    {/if}

    {if isset($payment_method.processor_params.enable_card) && $payment_method.processor_params.enable_card == 'Y'}
        {assign var="count_enable" value=($count_enable+1)}
        {if !$active_stripe_tab}
            {assign var="active_stripe_tab" value='alipay'}
        {/if}
    {/if}
    {if isset($payment_method.processor_params.enable_bitcoin) && $payment_method.processor_params.enable_bitcoin == 'Y'}
        {assign var="count_enable" value=($count_enable+1)}
        {if !$active_stripe_tab}
            {assign var="active_stripe_tab" value='bitcoin'}
        {/if}
    {/if}
    {if isset($payment_method.processor_params.enable_alipay) && $payment_method.processor_params.enable_alipay == 'Y'}
        {assign var="count_enable" value=($count_enable+1)}
        {if !$active_stripe_tab}
            {assign var="active_stripe_tab" value='alipay'}
        {/if}
    {/if}

    {assign var="payment_id" value=$cart.payment_id}

    {if $smarty.request.stripeToken && $smarty.request.stripeEmail}
        {assign var="alipay_account_flag" value=true}
    {/if}

    {if isset($payment_method.processor_params.enable_alipay) && $payment_method.processor_params.enable_alipay == 'Y' && ($active_stripe_tab == 'alipay' || $cart.active_stripe_tab.$payment_id == 'alipay') && !$alipay_account_flag}
        {include file="buttons/button.tpl" but_id="alipay_account_button" but_onclick="$.fn.open_stripe_form()" but_text=$but_text_ but_meta="btn-primary" but_type="button" but_check_filter=""}
    {else}
        {include file="buttons/button.tpl" but_id="alipay_submit_button" but_text=$but_text_ but_name="dispatch[order_management.place_order]" but_role="button_main" but_check_filter=""}
    {/if}

    {if $alipay_account_flag == true}
        <script>
            $(document).ready(function() {
                $("#alipay_submit_button").click();
            });
        </script>
    {/if}
{/capture}

{capture name="mainbox_title"}
    {if $cart.order_id == ""}
        {__("add_new_order")}
    {else}

        {__("editing_order")} #{$cart.order_id} <span class="f-middle">{__("total")}: <span>{include file="common/price.tpl" value=$cart.total}</span>{if $cart.company_id}, {$cart.company_id|fn_get_company_name}{/if}</span>

        <span class="f-small">
        /{if $cart.company_id}{$cart.company_id|fn_get_company_name}){/if}
        {if $status_settings.appearance_type == "I" && $cart.doc_ids[$status_settings.appearance_type]}
        ({__("invoice")} #{$cart.doc_ids[$status_settings.appearance_type]})
        {elseif $status_settings.appearance_type == "C" && $cart.doc_ids[$status_settings.appearance_type]}
        ({__("credit_memo")} #{$cart.doc_ids[$status_settings.appearance_type]})
        {/if}
        {__("by")} {if $cart.user_data.user_id}{/if}{$cart.user_data.firstname} {$cart.user_data.lastname} {if $cart.user_data.user_id}{/if}
        / {$cart.order_timestamp|date_format:"`$settings.Appearance.date_format`"}, {$cart.order_timestamp|date_format:"`$settings.Appearance.time_format`"}
        </span>

    {/if}
{/capture}

<div id="order_update">
{include file="common/mainbox.tpl" title=$smarty.capture.mainbox_title sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons sidebar_position="left"}
<!--order_update--></div>

</form>

<form id="alipay_form" class="cm-processed-form" {if $cart.order_id}action="{"order_management.place_order"|fn_url}"{else}action="{"order_management.add"|fn_url}"{/if} method="POST">
    {$company_data = $cart.payment_method_data.company_id|fn_get_company_data}
    <script data-no-defer
      src="//checkout.stripe.com/checkout.js" class="stripe-button hidden"
      data-key="{$addons.sd_stripe.publishable_key}"
      data-amount="{$cart.total * 100}"
      data-name="Test company"
      data-description="{$cart.amount} {if $cart.amount > 1}widgets{else}widget{/if} ({$cart.total|default:"0.00"|fn_format_price:$primary_currency:null:false} {$primary_currency})"
      data-locale="en"
      data-label="{__('submit_my_order')}"
      data-currency="usd"
      data-alipay="true">
    </script>
</form>