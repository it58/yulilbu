<div class="sidebar-row">
<h6>{__("exchange_rate")}</h6>
<ul class="unstyled currencies-rate" id="currencies_stock_exchange_">
    {foreach from=$currencies key=c_code item=v}
        <li>{$c_code}/{$smarty.const.CART_PRIMARY_CURRENCY}<span class="pull-right muted">{$c_code|fn_adv_curr_get_live_rate:$smarty.const.CART_PRIMARY_CURRENCY}</span></li>
    {/foreach}
</ul>
{if $addons.wk_advanced_currency_exchange.exchange_rate eq 'free_converter'}
    <a href="https://free.currencyconverterapi.com/" class="" target="_blank" title="Free currency converter">Free Currency Converter</a>
{elseif $addons.wk_advanced_currency_exchange.exchange_rate eq 'fixer_converter'}
    <a href="https://fixer.io/documentation#latestrates" class="" target="_blank" title="Fixer.io">Fixer.io</a>
{elseif $addons.wk_advanced_currency_exchange.exchange_rate eq 'paypal_converter'}
    <a href="https://developer.paypal.com/docs/classic/api/adaptive-payments/ConvertCurrency_API_Operation/" class="" target="_blank" title="Paypal Currency Conversion">Paypal</a>
{elseif $addons.wk_advanced_currency_exchange.exchange_rate eq 'oanda_converter'}
    <a href="http://developer.oanda.com/exchange-rates-api/" class="" target="_blank" title="OANDA Currency Conversion">OANDA</a>
{elseif $addons.wk_advanced_currency_exchange.exchange_rate eq 'ecb_converter'}
    <a href="http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html#dev" class="" target="_blank" title="European Central Bank Currency Conversion">European Central Bank</a>
{/if}
</div>