 {if $id}
    <div class="control-group">
        <label class="control-label" for="elm_auto_update_{$id}">{__("auto_update")}:{include file="common/tooltip.tpl" tooltip={__("auto_update_tooltip")}}</label>
        <div class="controls">
            <input type="checkbox" name="currency_data[auto_update]" value="Y" {if $currency.auto_update == "Y"}checked="checked"{/if} id="elm_auto_update_{$id}">
        </div>
    </div>
    
    <div class="control-group">
    	<label class="control-label" for="elm_currency_modifier_{$id}">{__("modifier_rate")}:{include file="common/tooltip.tpl" tooltip={__("modifier_rate_tooltip")}}</label>
    	<div class="controls">
    		<select name="currency_data[modifier_sign]" class="span1">
        		<option value="A" {if $currency.modifier_sign == "A"}selected="selected"{/if}> + </option>
        		<option value="M" {if $currency.modifier_sign == "M"}selected="selected"{/if}> - </option>
    		</select>
			<input type="text" name="currency_data[modifier_rate]" id="elm_currency_modifier_{$id}" value="{$currency.modifier_rate|default:'0.00000'}"  class="input-small" />
    		<select name="currency_data[modifier_type]" class="span1">
        		<option value="A" {if $currency.modifier_type == "A"}selected="selected"{/if}>{$currency.symbol}</option>
        		<option value="P" {if $currency.modifier_type == "P"}selected="selected"{/if}>%</option>
    		</select>
    	</div>
	</div>

	<div class="control-group">
    	<label class="control-label" for="elm_current_exchange_rate_{$id}">{__("current_exchange_rate")}:<br><small>{$currency.fetch_time|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</small>{include file="common/tooltip.tpl" tooltip={__("current_exchange_rate_tooltip")}}</label>
    	<div class="controls">
    		<input type="text" class="input-small" name="currency_data[current_exchange_rate]" id="elm_current_exchange_rate_{$id}" value="{$currency.current_exchange_rate}" readonly/>
            &nbsp;&nbsp;
			<label class="checkbox" for="usergroup_to_subcats">{__("update")}
                <input id="usergroup_to_subcats" type="checkbox" name="currency_data[update_current_exchange_rate]" value="Y">
            </label>
    	</div>
	</div>
{/if}