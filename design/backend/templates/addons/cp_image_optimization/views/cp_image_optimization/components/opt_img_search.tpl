{if $in_popup}
    <div class="adv-search">
    <div class="group">
{else}
    <div class="sidebar-row">
    <h6>{__("search")}</h6>
{/if}
{if $page_part}
    {assign var="_page_part" value="#`$page_part`"}
{/if}

<form action="{""|fn_url}{$_page_part}" name="{$product_search_form_prefix}search_form" method="get" class="cm-disable-empty {$form_meta}">
<input type="hidden" name="type" value="simple" autofocus="autofocus" />
{if $smarty.request.redirect_url}
    <input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}
{*
{if $put_request_vars}
    {array_to_fields data=$smarty.request skip=["callback"]}
{/if}
*}
{$extra nofilter}
{capture name="simple_search"}
    <div class="sidebar-field">
        <label>{__("image")}</label>
        <input type="text" name="s_image" size="20" value="{$search.s_image}" />
    </div>
    <div class="sidebar-field">
        <label>{__("size")}, Mb</label>
        <input type="text" name="s_size_from" size="3" value="{$search.s_size_from}" class="input-small"/> - 
        <input type="text" name="s_size_to" size="10" value="{$search.s_size_to}" class="input-small" />
    </div>
    <div class="sidebar-field">
        <label>{__("cp_extension")}</label>
		{if $search.search_ext}
			<select name="s_ext" class="input-small">
				<option value="" {if $ext_type == $search.s_ext}selected="selected"{/if}>{__("all")}</option>
				{foreach from=$search.search_ext item="ext_type"}
					<option value="{$ext_type}" {if $ext_type == $search.s_ext}selected="selected"{/if}>{$ext_type}</option>
				{/foreach}
			</select>
		{else}
			<input type="text" name="s_ext" size="3" value="{$search.s_ext}" class="input-small"/> 
		{/if}
    </div>
    <div class="sidebar-field">
        <label>{__("type")}</label>
		{if $search.search_type}
			<select name="s_type" class="input-small">
				<option value="" {if $img_type == $search.s_type}selected="selected"{/if}>{__("all")}</option>
				{foreach from=$search.search_type item="img_type"}
					<option value="{$img_type}" {if $img_type == $search.s_type}selected="selected"{/if}>{$img_type}</option>
				{/foreach}
			</select>
		{else}
			<input type="text" name="s_type" size="20" value="{$search.s_type}" /> 
		{/if}
    </div>
{/capture}
{include file="addons/cp_image_optimization/views/cp_image_optimization/components/opt_img_advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="images" in_popup=$in_popup}


</form>
<div class="sidebar-field hidden">
	<a href="{"cp_image_optimization.list_find_new"|fn_url}" class="btn">{__("find_new_images")}</a>
</div>
{if $in_popup}
    </div></div>
{else}
    </div><hr>
{/if}
