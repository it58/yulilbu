{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="cp_image_list" class="{if ""|fn_check_form_permissions} cm-hide-inputs{/if}" id="cp_image_list" >

{include file="addons/cp_image_optimization/views/cp_image_optimization/components/cp_pagination.tpl" save_current_page=true save_current_url=true}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

{if $image_list}
	<table class="table table-middle">
		<thead>
			<tr>
				<th width="1%">
					{include file="common/check_items.tpl"}
				</th>
				<th width="5%">{__("image")}</th>
				<th width="40%"><a class="cm-ajax" href="{"`$c_url`&sort_by=image_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("name")}{if $search.sort_by == "image_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="13%" class="center"><a class="cm-ajax" href="{"`$c_url`&sort_by=object_type&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("type")}{if $search.sort_by == "object_type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="10%" class="center"><a class="cm-ajax" href="{"`$c_url`&sort_by=image_size&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("size")}{if $search.sort_by == "image_size"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="5%">&nbsp;</th>
				<th width="2%" class="center"><a class="cm-ajax" href="{"`$c_url`&sort_by=image_ext&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("cp_extension")}{if $search.sort_by == "image_ext"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
				<th width="15%" class="center"><a class="cm-ajax" href="{"`$c_url`&sort_by=optimization&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("status")}{if $search.sort_by == "optimization"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th width="5%">{__("cp_opt_info")}</th>
			</tr>
		</thead>

		{foreach from=$image_list item="cp_image"}
			{assign var="holder_link" value=""}
			{if $cp_image.type && $cp_image.object_type}
				{if $cp_image.object_type == "blog"}
					{assign var="holder_link" value="pages.update&page_id=`$cp_image.object_id`&come_from=B&selected_section=addons"}
				{elseif $cp_image.object_type == "feature_variant"}
					{assign var="cp_allow_conv" value="Y"}
					{assign var="holder_link" value="product_features.update&feature_id=`$cp_image.feature_id`&selected_section=tab_variants_`$cp_image.feature_id`"}
				{elseif $cp_image.object_type == "product"}
					{assign var="cp_allow_conv" value="Y"}
					{if $cp_image.type == "M"}
						{assign var="holder_link" value="products.update&product_id=`$cp_image.object_id`"}
					{else}
						{assign var="holder_link" value="products.update&product_id=`$cp_image.object_id`&selected_section=images"}
					{/if}
				{elseif $cp_image.object_type == "category"}
					{assign var="cp_allow_conv" value="Y"}
					{assign var="holder_link" value="categories.update&category_id=`$cp_image.object_id`"}
				{elseif $cp_image.object_type == "shipping"}
					{assign var="cp_allow_conv" value="Y"}
					{assign var="holder_link" value="shippings.update&shipping_id=`$cp_image.object_id`"}
				{elseif $cp_image.object_type == "payment"}
					{assign var="cp_allow_conv" value="Y"}
					{assign var="holder_link" value="payments.manage"}
				{/if}
			{/if}
			<tr>
				<td>
					<input name="cp_image_ids[]" type="checkbox" value="{$cp_image.image_id}" class="cm-item" />
				</td>
				<td class="center">
					{if $holder_link}
						{if $cp_image.cp_image_data && $cp_image.object_type != "thumbnail"}
							{*
							{include file="common/image.tpl" image=$cp_image.cp_image_data image_width=40 href="$holder_link"|fn_url}
							*}
							<a href="{"$holder_link"|fn_url}">
                                <img src="{$cp_image.image_path}" width="40" alt="" />
                            </a>
						{elseif $cp_image.image_path}
							<a href="{"$holder_link"|fn_url}">
								<img src="{$cp_image.image_path}" width="40" alt="" />
							</a>
						{else}
							---
						{/if}
					{else}
						{if $cp_image.cp_image_data && $cp_image.object_type != "thumbnail"}
                            {*
							{include file="common/image.tpl" image=$cp_image.cp_image_data image_width=40 href=""}
							*}
							<img src="{$cp_image.image_path}" width="40" alt="" />
						{elseif $cp_image.image_path}
							<img src="{$cp_image.image_path}" width="40" alt="" />
						{else}
							---
						{/if}
					{/if}
				</td>
				<td>
					{if $cp_image.image_name}
						{if $holder_link}
							<a href="{"$holder_link"|fn_url}">
								{$cp_image.image_name}
							</a>
						{else}
							{$cp_image.image_name}
						{/if}
					{else}
						---
					{/if}
				</td>
				<td class="center">
					{if $cp_image.object_type}
						{if $cp_image.object_type == "blog"}
							{__("blog")}
						{elseif $cp_image.object_type == "feature_variant"}
							{__("feature")} {__("variant")}
						{elseif $cp_image.object_type == "product"}
							{__("product")}{if $cp_image.type == "M"} (M){elseif $cp_image.type == "A"} (A){/if}
						{elseif $cp_image.object_type == "category"}
							{__("category")}
						{elseif $cp_image.object_type == "promo"}
							{*
							{__("cp_other")}
							*}
							{$cp_image.object_type}
						{elseif $cp_image.object_type == "logos"}
							{__("logos")}
						{elseif $cp_image.object_type == "shipping"}
							{__("shipping")}
						{elseif $cp_image.object_type == "payment"}
							{__("payment")}
						{else}
							{$cp_image.object_type}
						{/if}
					{else}
						-----
					{/if}
				</td>
				<td class="center">
					{if $cp_image.image_size}
						{$cp_image.image_size} Mb
					{else}
						-
					{/if}
				</td> 
				<td class="center">
					<div class="hidden-tools">
					{*
					<a id="cp_upload_new_img_{$cp_image.image_id}" data-img-id="{$cp_image.image_id}" data-img-name="{$cp_image.image_name}" data-img-dir="{$cp_image.dir}" data-img-ext="{$cp_image.image_ext}" data-ca-target-id="cp_upload_form" class="cm-dialog-opener cm-dialog-auto-size btn cp-upload-but">{__("upload")}</a>
					*}
					{capture name="tools_list"}
						{if $opt_choosen && $opt_choosen == "S"}
                            <li>{btn type="list" text=__("cp_opt_by_lossy") href="cp_image_optimization.optim_few?cp_image_ids=`$cp_image.image_id`&compressor=L"}</li>
                            <li>{btn type="list" text=__("cp_opt_by_glossy") href="cp_image_optimization.optim_few?cp_image_ids=`$cp_image.image_id`&compressor=G"}</li>
                            <li>{btn type="list" text=__("cp_opt_by_lossless") href="cp_image_optimization.optim_few?cp_image_ids=`$cp_image.image_id`&compressor=S"}</li>
                        {else}
                            <li>{btn type="list" text=__("cp_optimizate") href="cp_image_optimization.optim_few?cp_image_ids=`$cp_image.image_id`"}</li>
                        {/if}
                        {*
						<li><a id="cp_upload_new_img_{$cp_image.image_id}" data-img-id="{$cp_image.image_id}" data-img-name="{$cp_image.image_name}" data-img-dir="{$cp_image.dir}" data-img-ext="{$cp_image.image_ext}" data-ca-target-id="cp_upload_form" class="cm-dialog-opener cm-dialog-auto-size cp-upload-but">{__("upload")}</a></li>
						*}
						{if $cp_image.cp_image_data}
							<li>{btn type="list" text=__("download") href="cp_image_optimization.download_img?img_href=`$cp_image.cp_image_data.image_path`"}</li>
						{else}
							<li>{btn type="list" text=__("download") href="cp_image_optimization.download_img?img_href=`$cp_image.image_path`"}</li>
						{/if}
						{*
						{if $cp_image.image_ext == "png" && $cp_image.object_id && $cp_allow_conv}
							<li>{btn type="list" text=__("cp_convert_to_jpg") href="cp_image_optimization.convert_to_jpg?img_dir=`$cp_image.dir`&image_name=`$cp_image.image_name`&object_id=`$cp_image.object_id`&object_type=`$cp_image.object_type`&type=`$cp_image.type`&strore_image_id=`$cp_image.strore_image_id`&fl_path=`$cp_image.cp_image_data.image_path`&img_pair_id=`$cp_image.img_pair_id`"}</li>
						{/if}
						*}
					{/capture}
					{dropdown content=$smarty.capture.tools_list}
					</div>
				</td>
				<td class="center">
					{if $cp_image.image_ext}
						{$cp_image.image_ext}
					{else}
						-
					{/if}
				</td>
				<td class="center">
					{if $cp_image.optimization == "N"}
						{__("not_optimazed")}
					{elseif $cp_image.optimization == "O"}
						{__("cp_optimazed")}
					{elseif $cp_image.optimization == "E"}
						{__("error")}
					{/if}
				</td>
				<td>
                    {if $cp_image.optimization == "O"}
                        {$cp_image.opt_info nofilter}
                    {else}
                        -
                    {/if}
				</td>
			</tr>
		{/foreach}
	</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{include file="addons/cp_image_optimization/views/cp_image_optimization/components/cp_pagination.tpl"}
{capture name="buttons"}
    {capture name="tools_list"}
		{if $image_list}
            {if $opt_choosen && $opt_choosen == "S"}
                <li>{btn type="list" text=__("optimaze_selected_by_lossy") dispatch="dispatch[cp_image_optimization.optim_few..L]" form="cp_image_list"}</li>
                <li>{btn type="list" text=__("optimaze_selected_by_glossy") dispatch="dispatch[cp_image_optimization.optim_few..G]" form="cp_image_list"}</li>
                <li>{btn type="list" text=__("optimaze_selected_by_lossless") dispatch="dispatch[cp_image_optimization.optim_few..S]" form="cp_image_list"}</li>
			{else}
                <li>{btn type="list" text=__("optimaze_selected") dispatch="dispatch[cp_image_optimization.optim_few]" form="cp_image_list"}</li>
			{/if}
		{/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
</form>
<div id="cp_upload_form" class="hidden" title="{__("cp_upload_image")}">
	<form action="{""|fn_url}" method="post" name="cp_image_list_upload_new" id="cp_image_list_upload_new" enctype="multipart/form-data" >
		<input type="hidden" id="data_img_id" name="image_data[image_id]" value="" />
		<input type="hidden" id="data_img_name" name="image_data[image_name]" value="" />
		<input type="hidden" id="data_img_dir" name="image_data[dir]" value="" />
		<input type="hidden" id="data_img_exten" name="image_data[ext]" value="" />
		{include file="common/attach_images.tpl" image_name="new_image" hide_titles=true no_detailed=true hide_alt=true image_object_type="new" image_type="X" hide_server=true}
		<div class="cp-image-add-form">
			{include file="buttons/button.tpl" but_text=__("upload") but_role="submit" but_name="dispatch[cp_image_optimization.add_new_img]" but_target_form="cp_image_list_upload_new" image_pair=""}
		</div>
	</form>
</div>
{capture name="sidebar"}
	{include file="addons/cp_image_optimization/views/cp_image_optimization/components/opt_img_search.tpl" dispatch="cp_image_optimization.image_list" view_type="images"}
{/capture}
{/capture}
{script src="js/addons/cp_image_optimization/new_upld_form.js"}
{include file="common/mainbox.tpl" title=__("cp_image_list") content=$smarty.capture.mainbox tools=$smarty.capture.tools sidebar=$smarty.capture.sidebar buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}


