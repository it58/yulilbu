{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="image_optimization_manage_form" id="image_optimization_manage_form" class="form-horizontal form-edit">
   <p>{$cron_info nofilter}</p>
    <div id="content_details">
        <div class="control-group">
            <div class="controls">
                <table class="table table-middle">
                    <thead>
                        <th>{__("name")}</th>
                        <th>{__("value")}</th>
                        <th>{__("action")}</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{__("total_save_space")}</td>
                            <td>{$get_all.total_save} Mb</td>
                            <td> - </td>
                        </tr>
                        <tr>
                            <td>{__("number_of_all_images")}</td>
                            <td>{$get_all.number_of_all_images}</td>
                            <td> - </td>
                        </tr>
                        <tr>
                            <td>{__("number_of_opt_images")}</td>
                            <td>{$get_all.number_of_opt_images}</td>
                            <td> - </td>
                        </tr>
                        <tr>
                            <td>{__("number_of_errors_images")}{include file="common/tooltip.tpl" tooltip={__("descr_of_error_img")} params="ty-subheader__tooltip"}</td>
                            <td>{$get_all.number_of_errors_images}</td>
                            <td><a class="btn cm-confirm cm-post" href="{"cp_image_optimization.optimize_errors"|fn_url}">{__("try_optimize_errors")}</a></td>
                        </tr>
                        <tr>
                            <td>{__("number_of_not_opt_images")}</td>
                            <td>{$get_all.number_of_not_opt_images}</td>
                            <td>{*<a class="btn cm-confirm cm-post" href="{"cp_image_optimization.optimize_new"|fn_url}" >{__("optimize_new_images")}</a>*} - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {*
        <div class="control-group">
            <label for="elm_statastica" class="control-label">{__("total_save_space")}:</label>
            <div class="controls">
                {$get_all.total_save} Mb
            </div>
        </div>
        *}
        <div class="control-group">
            <label class="control-label" for="elm_choose_optimizere">{__("choose_optimizere")}:</label>
            <div class="controls">
                <select class="input-short" name="choosen_optimizerer" id="elm_choose_optimizere">
                    <option value="S" {if $get_all.choosen_optimizerer == "S"}selected="selected"{/if}>{__("shortpixel_img_opt")}</option>
                    <option value="R" {if $get_all.choosen_optimizerer == "R"}selected="selected"{/if}>{__("resmush_it")}</option>
                    <option value="B" {if $get_all.choosen_optimizerer == "B"}selected="selected"{/if}>{__("both_optimizers")}</option>
                 </select>
            </div>
        </div>
         <div class="control-group">
			<div class="controls">{__("for_both_optimizers")}</div>
        </div>
        {if $scan_folders}
            <div class="control-group">
                <label class="control-label" for="elm_scan_folders">{__("cp_io_folders_for_scan")}:</label>
                <div class="controls">
                    <select class="input-short" name="cp_folders[]" id="elm_scan_folders" multiple=true>
                        {foreach from=$scan_folders item="sc_fold"}
                            <option value="{$sc_fold}" {if in_array($sc_fold,$get_all.folders)}selected="selected"{/if}>{$sc_fold}</option>
                        {/foreach}
                    </select>
                    {*
                    <p><strong>{__("cp_io_thumbs_notice_text")}</strong></p>
                    *}
                </div>
            </div>
        {/if}
        <div class="control-group">
            <label for="elm_img_quality" class="control-label">{__("img_quality")}{include file="common/tooltip.tpl" tooltip={__("for_good_quality")} params="ty-subheader__tooltip"}:</label>
            <div class="controls">
                <input type="text" name="img_quality" id="elm_img_quality" size="25" value="{$get_all.img_quality|default:"90"}" class="input-short" />
            </div>
        </div>
        <div class="control-group">
            <label for="elm_shortpixel_wait" class="control-label">{__("cp_io_shortpixel_wait")}{include file="common/tooltip.tpl" tooltip={__("cp_io_shortpixel_wait_tooltip")} params="ty-subheader__tooltip"}:</label>
            <div class="controls">
                <input type="text" name="shortpixel_wait" id="elm_shortpixel_wait" size="25" value="{$get_all.shortpixel_wait|default:"6"}" class="input-short" />
            </div>
        </div>
        <div class="control-group">
            <label for="elm_cron_value" class="control-label">{__("cp_io_cron_total_opt")}{include file="common/tooltip.tpl" tooltip={__("cp_io_cron_total_opt_tooltip")} params="ty-subheader__tooltip"}</label>
            <div class="controls">
                <input type="text" name="cron_value" id="elm_cron_value" size="25" value="{$get_all.cron_value|default:"0"}" class="input-short" />
            </div>
        </div>
        <div class="control-group">
            <label for="elm_cron_pass" class="control-label">{__("cp_io_cron_pass")}</label>
            <div class="controls">
                <input type="text" name="cron_pass" id="elm_cron_value" size="25" value="{$get_all.cron_pass|default:"cron_pass"}" class="input-short" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="elm_compressor">{__("cp_sp_compressor")}:</label>
            <div class="controls">
                <select class="input-short" name="compressor" id="elm_compressor">
                    <option value="L" {if $get_all.compressor == "L"}selected="selected"{/if}>{__("cp_lossy")}</option>
                    <option value="G" {if $get_all.compressor == "G"}selected="selected"{/if}>{__("cp_glossy")}</option>
                    <option value="S" {if $get_all.compressor == "S"}selected="selected"{/if}>{__("cp_lossless")}</option>
                 </select>
                <br />{__("cp_compression_info_text")}
            </div>
        </div>
    {*
        <div class="control-group">
            <label for="elm_number_of_all_images" class="control-label">{__("number_of_all_images")}:</label>
            <div class="controls">
                {$get_all.number_of_all_images}
            </div>
        </div>
        <div class="control-group">
            <label for="elm_number_of_opt_images" class="control-label">{__("number_of_opt_images")}:</label>
            <div class="controls">
                {$get_all.number_of_opt_images}
            </div>
        </div>
        <div class="control-group">
            <label for="elm_number_of_errors_images" class="control-label">{__("number_of_errors_images")}{include file="common/tooltip.tpl" tooltip={__("descr_of_error_img")} params="ty-subheader__tooltip"}:</label>
            <div class="controls">
                {$get_all.number_of_errors_images} 
                {include file="buttons/button.tpl" but_text=__("try_optimize_errors") but_meta="cm-confirm cm-post" but_name="dispatch[cp_image_optimization.optimize_errors]" but_role="submit" but_target_form="image_optimization_manage_form"}
            </div>
        </div>
        <div class="buttons-container">
            <div class="controls">
                {include file="buttons/button.tpl" but_text=__("optimize_all") but_meta="cm-confirm cm-post" but_name="dispatch[cp_image_optimization.optimize_all]" but_role="submit" but_target_form="image_optimization_manage_form"}
            </div>
        </div>
        <div class="control-group">
            <label for="elm_number_of_not_opt_images" class="control-label">{__("number_of_not_opt_images")}:</label>
            <div class="controls" id="find_new_img">
                {$get_all.number_of_not_opt_images}
                <span id="check_but">{btn type="list" text=__("find_new_images") dispatch="dispatch[cp_image_optimization.find_new]"}</span>
            </div>
        </div>
        <div class="buttons-container">
            <div class="controls">
                {include file="buttons/button.tpl" but_text=__("optimize_new_images") but_meta="cm-confirm cm-post" but_role="submit" but_name="dispatch[cp_image_optimization.optimize_new]" but_target_form="image_optimization_manage_form"}
            </div>
        </div>
    *}
        {include file="common/subheader.tpl" title=__("shortpixel_img_opt")}
        <div class="control-group">
            <label for="elm_api_key_for_opt" class="control-label">{__("api_key")}:</label>
            <div class="controls">
                <input type="text" name="api_key_for_opt" id="elm_api_key_for_opt" size="25" value="{$get_all.api_key_for_opt}" class="input-short" />
            </div>
        </div>
    </div>
    {capture name="buttons"}
    {*
        {capture name="tools_list"}
            <li><a href="{"cp_image_optimization.find_new"|fn_url}" class="cm-confirm cm-post">{__("find_new_images")}</a></li>
            <li class="divider"></li>
            <li><a href="{"cp_image_optimization.optimize_all"|fn_url}" class="cm-confirm cm-post">{__("optimize_all")}</a></li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    *}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[cp_image_optimization.save_api_key]" hide_first_button=false hide_second_button=true but_target_form="image_optimization_manage_form" save=1}
    {/capture}
</form>
{/capture}



{include file="common/mainbox.tpl" title=__("image_optimization") content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=true buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons }



