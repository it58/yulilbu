{capture name="mainbox"}
<div>
	<label for="elm_processing" class="control-label">Processing....</label>
</div>
<div>
	<label for="elm_number_of_optimazed_images" class="control-label">{__("number_of_opt_images")} : {$process.optimizated}</label>
</div>

<div>
	<label for="elm_amount_of_save_space" class="control-label">{__("save_space")} : {$saving}Mb</label>
</div>
{/capture}

{include file="common/mainbox.tpl" title=__("image_optimization") content=$smarty.capture.mainbox }