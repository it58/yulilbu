{include file="views/categories/../categories/manage.tpl"}

{if !("MULTIVENDOR"|fn_allowed_for && $runtime.company_id)}
<li id="export_selected_categories" class="hidden">{btn type="list" text=__("export_selected_categories") dispatch="dispatch[categories.export_range]" form="category_tree_form"}</li>
<li id="export_selected_categories_and_subcategories" class="hidden">{btn type="list" text=__("export_selected_categories_and_subcategories") dispatch="dispatch[categories.export_range.subcats]" form="category_tree_form"}</li>
{/if}
<li id="export_products_from_categories" class="hidden">{btn type="list" text=__("export_products_from_categories") dispatch="dispatch[categories.export_category_products]" form="category_tree_form"}</li>
<li id="export_products_from_categories_and_subcategories" class="hidden">{btn type="list" text=__("export_products_from_categories_and_subcategories") dispatch="dispatch[categories.export_category_products.subcategories]" form="category_tree_form"}</li>

<script type="text/javascript">
(function(_, $) {
    $(document).ready(function(){
        $('#export_selected_categories').show();
        $('#export_selected_categories_and_subcategories').show();
        $('#export_products_from_categories').show();
        $('#export_products_from_categories_and_subcategories').show();
        $('.actions .dropdown-menu').append($('#export_selected_categories')).append($('#export_selected_categories_and_subcategories')).append($('#export_products_from_categories')).append($('#export_products_from_categories_and_subcategories'));
    });
}(Tygh, Tygh.$));
</script>