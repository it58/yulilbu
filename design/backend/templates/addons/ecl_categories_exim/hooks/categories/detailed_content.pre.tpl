<div class="control-group" id="container_category_code">
    <label class="control-label" for="elm_category_code">{__("category_code")}:</label>
    <div class="controls">
        <input type="text" name="category_data[category_code]" id="elm_category_code" size="20" maxlength="32"  value="{$category_data.category_code}" class="input-long" />
    </div>
</div>

<script type="text/javascript">
(function(_, $) {
    $(document).ready(function(){
        $('#acc_information .control-group:first').after($('#container_category_code'));
        if ($.trim($('#content_addons').html()) == '') {
            $('#addons').hide();
        }
    });
}(Tygh, Tygh.$));
</script>