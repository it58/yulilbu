<form>
 <div class="control-group">
 <label class="control-label">{__(enable_autofill)}:{include file="common/tooltip.tpl" tooltip=__("enter_product_code")}</label>
	<div class="controls">
	<input type="hidden" name="order_cancelation_data[check]" value="N" />
	<input type="checkbox" name="order_cancelation_data[check]" id="elm_check" value="Y" {if {$order_cancelation_data.check}==Y}checked{/if} class="checkbox cm-item cm-item-status-{$product.status|lower}"  />
	</div>
</div>
<div id="acc_options" class="autofill collapse in">
    <div class="control-group">
	<label class="control-label">{__(allow_settings)}:{include file="common/tooltip.tpl" tooltip=__("allow_product_settings")}</label>
		<div class="controls">
		<input type="hidden" name="order_cancelation_data[allow]" value="N" />
			<input type="checkbox" name="order_cancelation_data[allow]" value="Y" {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.allow}==Y}checked{/if}{/if} class="checkbox cm-item cm-item-status-{$product.status|lower}"  />
		</div>
	</div>
	<div class="control-group">
	<label class="control-label">{__(allow_settings_inventory)}:{include file="common/tooltip.tpl" tooltip=__("product_combination")}</label>
		<div class="controls">
		<input type="hidden" name="order_cancelation_data[option_combination]" value="N" />
			<input type="checkbox" name="order_cancelation_data[option_combination]" value="Y" {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.option_combination}==Y}checked{/if}{/if} class="checkbox cm-item cm-item-status-{$product.status|lower}"  />
		</div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="prefix">{__(prefix_in_sku)}:{include file="common/tooltip.tpl" tooltip=__("enter_prefix_in_code")}</label>
		<div class="controls">
			<select id="prefix" name='order_cancelation_data[prefix]' >
				<option {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='Random'}selected{/if}{/if}>{__(random)}</option>
				<option {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='User Entry'}selected{/if}{/if}>{__(user_entry)}</option>
				<option {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='Product ID'}selected{/if}{/if}>{__(product_id)}</option>
			</select>
		</div>
		<label class="user_prefix control-label" for="prefix" {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='User Entry'}style='display:block'{/if}{else}style='display:none'{/if}>{__(enter_prefix)}:</label>
		<div class="controls">
		<input type ='text' id='pre1' class='user_prefix' name='order_cancelation_data[enter_prefix]' placeholder ='Enter Prefix' {if {$order_cancelation_data.check}==Y}{if {$smarty.request.prefix}=='User Entry' || {$order_cancelation_data.prefix}=='User Entry'}style='display:block'{/if} {else}style='display:none'{/if}{if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='User Entry'}value={$order_cancelation_data.enter_prefix}{/if}{/if} >
		</div>

		
		
	</div>

	<div class="control-group {$promo_class}">
		<label  class="control-label" for="middle" >{__(middle_in_sku)}:{include file="common/tooltip.tpl" tooltip=__("enter_middle_in_code")}</label>
		<div class="controls">
			<select  id="middle" name='order_cancelation_data[middle]'>
				<option  {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.middle}=='Random'}selected{/if}{/if}>{__(random)}</option>
				<option  {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.middle}=='User Entry'}selected{/if}{/if}>{__(user_entry)}</option>
				<option  {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.middle}=='Product ID'}selected{/if}{/if}>{__(product_id)}</option>
			</select>
		</div>
		<label class="user_middle control-label" for="prefix" {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='User Entry'}style='display:block'{/if}{else}style='display:none'{/if}>{__(enter_middle)}:</label>
		<div class="controls">
		<input type ='text' class='user_middle' name='order_cancelation_data[enter_middle]' placeholder ='Enter Middle' {if {$order_cancelation_data.check}==Y}{if {$smarty.request.middle}=='User Entry' || {$order_cancelation_data.middle}=='User Entry'}style='display:block'{/if}{else}style='display:none'{/if} {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.middle}=='User Entry'}value={$order_cancelation_data.enter_middle}{/if}{/if}>
		</div>
			
		
	</div>

	<div class="control-group {$promo_class}">
		<label  class="control-label" for="suffix">{__(suffix_in_sku)}:{include file="common/tooltip.tpl" tooltip=__("enter_suffix_in_code")}</label>
		<div class="controls">
			<select id="suffix" name='order_cancelation_data[suffix]' >
				<option {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.suffix}=='Random'}selected{/if}{/if}>{__(random)}</option>
				<option {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.suffix}=='User Entry'}selected{/if}{/if}>{__(user_entry)}</option>
				<option {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.suffix}=='Product ID'}selected{/if}{/if}>{__(product_id)}</option>
			</select>
		</div>
		<label class="user_suffix control-label" for="prefix" {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.prefix}=='User Entry'}style='display:block'{/if}{else}style='display:none'{/if}>{__(enter_suffix)}:</label>
		<div class="controls">
		<input type ='text' class='user_suffix' name='order_cancelation_data[enter_suffix]'  placeholder ='Enter Suffix' {if {$order_cancelation_data.check}==Y}{if {$smarty.request.suffix}=='User Entry' || {$order_cancelation_data.suffix}=='User Entry'}style='display:block'{/if}{else}style='display:none'{/if} {if {$order_cancelation_data.check}==Y}{if {$order_cancelation_data.suffix}=='User Entry'}value={$order_cancelation_data.enter_suffix}{/if}{/if}>	
		</div>	
	</div>
</div>
<div class="control-group {$promo_class}">
<label class="control-label">{__(show_products_with_same_sku)}:{include file="common/tooltip.tpl" tooltip=__("list_of_duplicate_products")}</label>
<div class="controls">
	{include file="buttons/button.tpl" but_text=__("show_existing") but_name="dispatch[addons.show_existing]"}
	</div>
</div>
</form>
{if isset($smarty.request.show_existing)}
{if $products}

{include file="common/subheader.tpl" title={__("exist_products_with_same_sku")} target="#acc_package_information"}
<div id="acc_package_information" class="collapse in">
<table width="100%" class="table">
<thead>
<tr>
    <th width="10%"><a class="cm-ajax" href="http://192.168.1.29/~saurabhbhardwaj/cs-cart2/cscart2_v4.3.4/admin.php?dispatch=product_package_builder.manage&amp;sort_by=product&amp;sort_order=desc" data-ca-target-id=pagination_contents>{__(product_id)}<i class="exicon-desc"></i></a></th>
    <th width="10%"><a class="cm-ajax" href="http://192.168.1.29/~saurabhbhardwaj/cs-cart2/cscart2_v4.3.4/admin.php?dispatch=product_package_builder.manage&amp;sort_by=product&amp;sort_order=desc" data-ca-target-id=pagination_contents>{__(product_name)}<i class="exicon-desc"></i></a></th>

    <th width="10%"><a class="cm-ajax" href="http://192.168.1.29/~saurabhbhardwaj/cs-cart2/cscart2_v4.3.4/admin.php?dispatch=product_package_builder.manage&amp;sort_by=total_products&amp;sort_order=desc" data-ca-target-id=pagination_contents>{__(old_sku)}<i class="exicon-dummy"></i></a></th>
    
    <th width="10%"><a class="cm-ajax" href="http://192.168.1.29/~saurabhbhardwaj/cs-cart2/cscart2_v4.3.4/admin.php?dispatch=product_package_builder.manage&amp;sort_by=total_price&amp;sort_order=desc" data-ca-target-id=pagination_contents>{__(new_sku)}<i class="exicon-dummy"></i></a></th>
</tr>
</thead>
{foreach from=$products item=product}
{assign var=random value=rand()}
<tr>
	<td>
{$product.product_id}
	</td>
	<td>
		<a class="row-status" href="{"products.update?product_id=`$product.product_id`"|fn_url}">
		
    {$product.product_name|truncate:20 nofilter}
    </a>
	</td>
	<td>
		
    {$product.product_code}

	</td>
	<td>
	{if {$smarty.request.check}==Y}
	<label class="{if {$smarty.request.show_existing==Y}}cm-no-hide-input{else}cm-required{/if}" for={$product.product_id}></label>
		<input type="text" id={$product.product_id} name="product_data[{$product.product_id}][new_sku]" class='new_sku' value={$product.prefix}{$product.middle}{$product.suffix}>
	{else}
	<label class="{if {$smarty.request.show_existing==Y}}cm-no-hide-input{else}cm-required{/if}" for={$product.product_id}></label>
	<input type="text" id={$product.product_id} name="product_data[{$product.product_id}][new_sku]">
	{/if}
	</td>
</tr>
{/foreach}
</table>
</div>
{else}
{include file="common/subheader.tpl" title={__("exist_products_with_same_sku")} target="#no_data"}
    <div id="no_data" class="collapse in" ><p class='no-items'>{__("no_data")}</p></div>
{/if}
{/if}
<script type="text/javascript">
	jQuery(document).ready(function($) {
   $('#elm_check').change(function(){

       if ($(this).is(':checked'))
       {
       	$('.autofill').show();
       }
       else 
       {
       	$('.new_sku').val('');
       	$('.autofill').hide();
      }
   }).change();

   $('#prefix').change(function(){
	if ($('#prefix').val() == '{__(user_entry)}') 
	{
		
		
		  $('.user_prefix').show();
    } 
    else {
		  $('.user_prefix').hide();
		}
	});

    $('#middle').change(function(){
   if ($('#middle').val() == '{__(user_entry)}') {
          $('.user_middle').show();
	        } else {
	          $('.user_middle').hide();
	        }
    });

   $('#suffix').change(function(){
   if ($('#suffix').val() == '{__(user_entry)}') {
          $('.user_suffix').show();
	        } else {
	          $('.user_suffix').hide();
	        }
    });
});
</script>
