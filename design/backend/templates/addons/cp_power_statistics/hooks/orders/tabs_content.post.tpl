{if $transitions}
<div id="content_conversion_statistics">
    <table width="100%" class="table table-middle first-transition">
    <thead>
    <tr>
		<th class="left" width="60px">{__("unique_id")}</th>
		<th class="left" width="280px">{__("from_url")}</th>
		<th class="left" width="70px">{__("from_domain")}</th>
		<th class="left" width="280px">{__("to_url")}</th>
		<th class="left" width="90px">{__("time")}</th>    
    </tr>
    </thead>

    {foreach from=$transitions item="transition"}

    <tr class="cm-row-status {if $transition.first_transition == 'Y'}first-transition{/if}">
	<td>
	     {if $transition.unique_id != 0}<a href="{"?dispatch=conversion_statistics.manage&unique_id=`$transition.unique_id|urlencode`"|fn_url}">{$transition.unique_id}</a>{if $transition.country_code}<div><i class="flag flag-{$transition.country_code|strtolower}"></i><a href="{"?dispatch=conversion_statistics.manage&country_code=`$transition.country_code|urlencode`"|fn_url}">{$countries[$transition.country_code]}</a></div>{/if}{else}-{__("empty")}-{/if}
	</td>
	<td>
	    <div class="overflow-auto-280"><a href="{"?dispatch=conversion_statistics.manage&all_from_url=`$transition.from_url|urlencode`"|fn_url}" class="nowrap">{if $transition.from_url == "direct_transition"}{__("direct_transition")}{else}{$transition.from_url}{/if}</a></div>
	    {if $transition.from_url != "direct_transition"}<div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="{$transition.from_url|fn_url}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>{/if}
	</td>
	<td>
	    <a href="{"?dispatch=conversion_statistics.manage&all_from_domain=`$transition.from_domain|urlencode`"|fn_url}" class="nowrap">{if $transition.from_domain == "direct_transition"}{__("direct_transition")}{else}{$transition.from_domain}{/if}</a>
	    {if $transition.from_domain != "direct_transition"}<div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="http://{$transition.from_domain}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>{/if}</td>
	<td>
	    <div class="overflow-auto-280"><a href="{"?dispatch=conversion_statistics.manage&all_to_url=`$transition.to_url|urlencode`"|fn_url}" class="nowrap">{$transition.to_url}</a></div>
	    <div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="http://{$transition.to_url}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>
	</td>
	<td>
	     {$transition.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
	</td>	
    </tr>
    {/foreach}
    </table>  
<!--content_transitions--></div>
{/if}