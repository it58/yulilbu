{script src="js/addons/cp_power_statistics/swfobject.js"}

{capture name="mainbox"}
    <div id="content_{$reports_group}">    
        {include file="addons/cp_power_statistics/views/cp_extended_statistics/components/reports/`$reports_group`.tpl" report_data=$statistics_data}
    <!--content_{$reports_group}--></div>
{/capture}

{capture name="sidebar"}
    {include file="common/saved_search.tpl" dispatch="cp_extended_statistics.report" view_type="statistics"}
    {include file="addons/cp_power_statistics/views/cp_extended_statistics/components/search_form.tpl" key=$runtime.action dispatch="cp_extended_statistics.report"}
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
        <li>{btn type="list" class="cm-confirm" text=__("remove_statistics") href="cp_extended_statistics.delete"}</li>
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

{include file="common/mainbox.tpl" title="{__("cp_extended_statistics")}: {__($reports_group)}" content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar buttons=$smarty.capture.buttons}

