{capture name="table_chart"}

{include file="common/pagination.tpl" div_id="product_favorites_pagination_content"}

{if $report_data && $report_data.data}
    <table width="100%" class="table">
    <thead>
        <tr>
            <th>{__("date")}</th>
            <th class="right">{__("product_favorites")}</th>
        </tr>
    </thead>
    {foreach from=$report_data.data key="date" item="stat"}
    <tr>
        <td>
            {if $statistics_period == $smarty.const.STAT_PERIOD_DAY}
                {$stat.timestamp|fn_date_format:"`$settings.Appearance.date_format`"}
            {elseif $statistics_period == $smarty.const.STAT_PERIOD_HOUR}
                {$stat.timestamp|fn_date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
            {/if}
        </td>
        <td class="right">{$stat.favorites}</td>
    </tr>
    {/foreach}
    </table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id="product_favorites_pagination_content"}

{/capture}

{if $report_data.data}
    {$applicable_charts = "line"}
{else}
    {$applicable_charts = ""}
{/if}
{include file="addons/cp_power_statistics/views/statistics/components/select_charts.tpl" chart_table=$smarty.capture.table_chart chart_type=$chart_type applicable_charts=$applicable_charts}