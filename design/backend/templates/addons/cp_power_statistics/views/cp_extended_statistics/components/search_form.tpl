<div class="sidebar-row">
    <h6>{__("search")}</h6>
<form action="{""|fn_url}" name="{$key}_filter_form" method="get">
{capture name="simple_search"}
<input type="hidden" name="report" value="{$report_data.report}" />
<input type="hidden" name="reports_group" value="{$reports_group}" />
<input type="hidden" name="selected_section" value="{$report_data.report}" />
{$extra nofilter}

{include file="common/period_selector.tpl" period=$search.period extra="" display="form"}
{/capture}

{capture name="advanced_search"}
    <div class="row-fluid">
        <div class="span6 group form-horizontal">
        </div>
    </div>
{/capture}

{include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="statistics"}

</form>
</div>
