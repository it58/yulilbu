{capture name="table_chart"}

{include file="common/pagination.tpl" div_id="traffic_source_pagination_content"}

{if $report_data && $report_data.data}
    <table width="100%" class="table">
    <thead>
        <tr>
            <th>{__("traffic_source")}</th>
            <th class="right">{__("views")}</th>
        </tr>
    </thead>
    {foreach from=$report_data.data key="date" item="stat"}
    <tr>
        <td>{$stat.label}</td>
        <td class="right">{$stat.count}</td>
    </tr>
    {/foreach}
    </table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id="traffic_source_pagination_content"}

{/capture}

{$applicable_charts = ""}
{include file="addons/cp_power_statistics/views/statistics/components/select_charts.tpl" chart_table=$smarty.capture.table_chart chart_type=$chart_type applicable_charts=$applicable_charts}