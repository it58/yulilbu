{capture name="table_chart"}

{include file="common/pagination.tpl" div_id="most_favorites_pagination_content"}

{if $report_data && $report_data.data}
    <table width="100%" class="table">
    <thead>
        <tr>
            <th>{__("product")}</th>
            <th class="right">{__("most_favorites")}</th>
        </tr>
    </thead>
    {foreach from=$report_data.data key="date" item="stat"}
    <tr>
        <td><a href="{"products.update&product_id=`$stat.product_id`"|fn_url}">{$stat.product}</a>
        </td>
        <td class="right">{$stat.favorites}</td>
    </tr>
    {/foreach}
    </table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id="most_favorites_pagination_content"}

{/capture}

{$applicable_charts = ""}
{include file="addons/cp_power_statistics/views/statistics/components/select_charts.tpl" chart_table=$smarty.capture.table_chart chart_type=$chart_type applicable_charts=$applicable_charts}