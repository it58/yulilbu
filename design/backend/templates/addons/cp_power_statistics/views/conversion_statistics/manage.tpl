{capture name="mainbox"}
{capture name="sidebar"}
    {*{include file="common/saved_search.tpl" dispatch="conversion_statistics.manage" view_type="orders"}*}
    {include file="addons/cp_power_statistics/views/conversion_statistics/components/transition_search_form.tpl" dispatch="conversion_statistics.manage"}
{/capture}

<form action="{""|fn_url}" method="post" name="manage_first_transition_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}
{if $transitions}
<div id="content_transitions">
    <table width="100%" class="table table-middle first-transition">
    <thead>
    <tr>
		<th class="left" width="35px">{include file="common/check_items.tpl" check_statuses=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}</th>
		<th class="left" width="60px"><a class="cm-ajax" href="{"`$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("order_id")}{if $search.sort_by == "order_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="60px"><a class="cm-ajax" href="{"`$c_url`&sort_by=unique_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("unique_id")}{if $search.sort_by == "unique_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="190px"><a class="cm-ajax" href="{"`$c_url`&sort_by=from_url&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("from_url")}{if $search.sort_by == "from_url"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="70px"><a class="cm-ajax" href="{"`$c_url`&sort_by=from_domain&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("from_domain")}{if $search.sort_by == "from_domain"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="190px"><a class="cm-ajax" href="{"`$c_url`&sort_by=to_url&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("to_url")}{if $search.sort_by == "to_url"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th>&nbsp;</th>
		<th class="left" width="90px"><a class="cm-ajax" href="{"`$c_url`&sort_by=timestamp&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("time")}{if $search.sort_by == "timestamp"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>    
    </tr>
    </thead>

    {foreach from=$transitions item="transition"}

    <tr class="cm-row-status {if $transition.first_transition == 'Y'}first-transition{/if}">
    <td class="left">
        <input type="checkbox" name="url_ids[]" value="{$transition.url_id}" class="cm-item cm-item-status-{$transition.status|lower}" />
    </td>
    <td>
    {if $transition.order_id != 0}
	     <a href="{"?dispatch=conversion_statistics.manage&all_order_id=`$transition.order_id`"|fn_url}" class="underlined">{__("order")} #{$transition.order_id}</a>
	     {assign var="status" value=$transition.status}
	      <span>{$order_status_descr.$status}</span>
	     <span>{$transition.order_timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</span>
	     <div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="{"orders.details?order_id=`$transition.order_id`"|fn_url}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>
	  {else} <a href="{"?dispatch=conversion_statistics.manage&all_no_order=no_order"|fn_url}" class="underlined">{__("no_order")}</a>
	  {/if}   
		</td>
	<td>
	     {if $transition.unique_id != 0}<a href="{"?dispatch=conversion_statistics.manage&unique_id=`$transition.unique_id|urlencode`"|fn_url}">{$transition.unique_id}</a>{if $transition.country_code}<div><i class="flag flag-{$transition.country_code|strtolower}"></i><a href="{"?dispatch=conversion_statistics.manage&country_code=`$transition.country_code|urlencode`"|fn_url}">{$countries[$transition.country_code]}</a></div>{/if}{else}-{__("empty")}-{/if}
	     <a href="{"?dispatch=conversion_statistics.manage&client_ip=`$transition.client_ip|urlencode`"|fn_url}">{$transition.client_ip|long2ip}</a>
	</td>
	<td>
	    <div class="overflow-auto-190"><a href="{"?dispatch=conversion_statistics.manage&all_from_url=`$transition.from_url|urlencode`"|fn_url}" class="nowrap">{if $transition.from_url == "direct_transition"}{__("direct_transition")}{else}{$transition.from_url}{/if}</a></div>
	    {if $transition.from_url != "direct_transition"}<div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="{$transition.from_url|fn_url}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>{/if}
	</td>
	<td>
	    <a href="{"?dispatch=conversion_statistics.manage&all_from_domain=`$transition.from_domain|urlencode`"|fn_url}" class="nowrap">{if $transition.from_domain == "direct_transition"}{__("direct_transition")}{else}{$transition.from_domain}{/if}</a>
	    {if $transition.from_domain != "direct_transition"}<div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="http://{$transition.from_domain}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>{/if}</td>
	<td>
	    <div class="overflow-auto-190"><a href="{"?dispatch=conversion_statistics.manage&all_to_url=`$transition.to_url|urlencode`"|fn_url}" class="nowrap">{$transition.to_url}</a></div>
	    <div class="text-left">{include file="buttons/button.tpl" but_role="text" but_href="http://{$transition.to_url}" but_text=__("go") but_meta="btn text-center hidden-tools" but_target="_blank"}</div>
	</td>
	<td width="5%" class="center">
        {capture name="tools_items"}
            {hook name="orders:list_extra_links"}
                {assign var="current_redirect_url" value=$config.current_url|escape:url}
                <li>{btn type="list" href="conversion_statistics.delete?url_id=`$transition.url_id`&redirect_url=`$current_redirect_url`" class="cm-confirm" text={__("delete")}}</li>
            {/hook}
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_items}
        </div>
    </td>
	<td>
	     {$transition.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
	</td>	
    </tr>
    
    {/foreach}
    </table>  
<!--content_transitions--></div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{if $transitions}
    <div class="statistic clearfix" id="transitions_total">
        <table class="pull-right ">
            {if $total_pages > 1 && $search.page != "full_list"}
                <tr>
                    <td>&nbsp;</td>
                    <td width="100px">{__("for_this_page_orders")}:</td>
                </tr>
                <tr>
                    <td>{__("gross_total")}:</td>
                    <td>{include file="common/price.tpl" value=$display_totals.gross_total}</td>
                </tr>
                {if !$incompleted_view}
                    <tr>
                        <td>{__("totally_paid")}:</td>
                        <td>{include file="common/price.tpl" value=$display_totals.totally_paid}</td>
                    </tr>
                {/if}
                <hr />
                <tr>
                    <td>{__("for_all_found_orders")}:</td>
                </tr>
            {/if}
            <tr>
                <td>{__("gross_total")}:</td>
                <td>{include file="common/price.tpl" value=$totals.gross_total}</td>
            </tr>

                {if !$incompleted_view}
                    <tr>
                        <td><h4>{__("totally_paid")}:</h4></td>
                        <td class="price">{include file="common/price.tpl" value=$totals.totally_paid}</td>
                    </tr>
                {/if}
        </table>
    <!--transitions_total--></div>
{/if}

<div class="clearfix">
	{include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>
</form>
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $transitions}
                <li>{btn type="delete_selected" dispatch="dispatch[conversion_statistics.m_delete]" form="manage_first_transition_form"}</li>
        {/if}
        {hook name="orders:list_tools"}
        {/hook}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

{include file="common/mainbox.tpl" title=__("conversion_statistics") sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="manage_first_transitions"}