{if $in_popup}
    <div class="adv-search">
    <div class="group">
{else}
    <div class="sidebar-row">
    <h6>{__("search")}</h6>
{/if}

<form action="{""|fn_url}" name="transition_search_form" method="get" class="{$form_meta}">
{capture name="simple_search"}

{if $smarty.request.redirect_url}
<input type="hidden" name="redirect_url" value="{$smarty.request.redirect_url}" />
{/if}
{if $selected_section != ""}
<input type="hidden" id="selected_section" name="selected_section" value="{$selected_section}" />
{/if}

{$extra nofilter}
<div class="sidebar-field">
    <label for="order_id">{__("order_id")}</label>
    <input type="text" name="order_id" id="order_id" value="{$search.order_id}" size="10"/>
</div>
<div class="sidebar-field">
    <label for="unique_id">{__("unique_id")}</label>
    <input type="text" name="unique_id" id="unique_id" value="{$search.unique_id}" size="10"/>
</div>

<div class="sidebar-field">
         <label class="checkbox" for="only_with_order">
            <input type="hidden" name="only_with_order" value="N" />
            <input type="checkbox" value="Y" {if $search.only_with_order == "Y"}checked="checked"{/if} name="only_with_order" id="only_with_order"/>{__("only_with_order")}
        </label>
</div>

<div class="sidebar-field">
         <label class="checkbox" for="only_first_transition">
            <input type="hidden" name="only_first_transition" value="N" />
            <input type="checkbox" value="Y" {if $search.only_first_transition == "Y"}checked="checked"{/if} name="only_first_transition" id="only_first_transition"/>{__("only_first_transition")}
        </label>
</div>
{/capture}

{capture name="advanced_search"}
<div class="row-fluid"> 
<div class="group span7 form-horizontal">
	<div class="control-group">
    <label for="match">{__("find_results_with")}</label>
    <select name="match" id="match" class="valign">
        <option {if $search.match == "any"}selected="selected"{/if} value="any">{__("any_words")}</option>
        <option {if $search.match == "all"}selected="selected"{/if} value="all">{__("all_words")}</option>
        <option {if $search.match == "exact"}selected="selected"{/if} value="exact">{__("exact_phrase")}</option>
    </select>
    <input type="text" name="q" size="38" value="{$search.q}" class="input-text-large valign" />
</div>

<div class="control-group">
    <label>{__("search_in")}</label>
    <div class="select-field">
        <label class="checkbox" for="qfrom_url">
            <input type="hidden" name="qfrom_url" value="N" />
            <input type="checkbox" value="Y" {if $search.qfrom_url == "Y"}checked="checked"{/if} name="qfrom_url" id="qfrom_url"/>{__("from_url")}
        </label>

        <label class="checkbox" for="qfrom_domain">
            <input type="checkbox" value="Y" {if $search.qfrom_domain == "Y"}checked="checked"{/if} name="qfrom_domain" id="qfrom_domain"/>{__("from_domain")}
        </label>

        <label class="checkbox" for="qto_url">
            <input type="checkbox" value="Y" {if $search.qto_url == "Y"}checked="checked"{/if} name="qto_url" id="qto_url" />{__("to_url")}
        </label>
    </div>
</div>
</div> 
</div>
<div class="group form-horizontal">
<div class="control-group">
    <label class="control-label">{__("order_period")}</label>
    <div class="controls">
        {include file="common/period_selector.tpl" period=$search.period form_name="order_search_form"}
    </div>
</div>
</div>

<div class="group form-horizontal">
<div class="control-group">
    <label class="control-label">{__("transition_period")}</label>
    <div class="controls">
        {include file="common/period_selector.tpl" period=$search.tr_period form_name="transition_search_form" prefix="tr_"}
    </div>
</div>
</div>

<div class="group">
{if $incompleted_view}
    <input type="hidden" name="status" value="{$smarty.const.STATUS_INCOMPLETED_ORDER}" />
{else}
<div class="control-group">
    <label class="control-label">{__("order_status")}</label>
    <div class="controls checkbox-list">
        {include file="common/status.tpl" status=$search.status display="checkboxes" name="status" columns=5}
    </div>
</div>
{/if}
</div>

<div class="group">
    <div class="control-group">
        <label class="checkbox" for="a_uid"><input type="checkbox" name="admin_user_id" id="a_uid" value="{$auth.user_id}" {if $search.admin_user_id}checked="checked"{/if} />{__("new_orders")}</label>
    </div>
</div>


{/capture}

{include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="orders" in_popup=$in_popup}

</form>

{if $in_popup}
    </div></div>
{else}
    </div><hr>
{/if}