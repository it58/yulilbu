<div class="power-statistics"> 
    <table class="table">
    <tr>    
    <th>{__("users_online")|ucfirst}</th>
    <td class="right">{$users_online_count}</td>    
    </tr>    
    </table>
  
  {if $country_users}
    <table class="table">
    <tr><th colspan="2">{__("location")}</th></tr>  
    {foreach from=$country_users item="country"}
    <tr>    
    <td><i class="flag flag-{$country.country_code|strtolower}"></i>{$countries[$country.country_code|strtoupper]|default:__("unknown")}</td>
    <td class="right">{$country.users_count}</td>    
    </tr>    
    {/foreach}
    </table>
  {/if}
  
  {if $browser_users}
    <br/>
    <table class="table">
    <tr><th colspan="2">{__("browser")}</th></tr>
    {foreach from=$browser_users item="browser"}
    <tr>    
    <td><img src="{$images_dir}/addons/cp_power_statistics/browsers/{$browser.browser|default:'unknown'}.png">&nbsp;{$browser.browser|fn_get_browser_name|default:__("unknown")}</td>
    <td class="right">{$browser.users_count}</td>    
    </tr>    
    {/foreach}
    </table> 
  {/if}
</div>