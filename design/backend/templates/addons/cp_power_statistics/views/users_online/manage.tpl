{capture name="mainbox"}
{capture name="sidebar"}
    {include file="addons/cp_power_statistics/views/users_online/components/get_info.tpl"}
{/capture}

<form action="{""|fn_url}" method="post" name="manage_first_transition_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}
{if $users_online}
<div id="content_users_online">
    <table width="100%" class="table table-middle first-transition">
    <thead>
    <tr>
		<th class="left" width="100px"><a class="cm-ajax" href="{"`$c_url`&sort_by=client_ip&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("client_ip")}{if $search.sort_by == "client_ip"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="140px"><a class="cm-ajax" href="{"`$c_url`&sort_by=country_code&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("country")}{if $search.sort_by == "country_code"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="140px"><a class="cm-ajax" href="{"`$c_url`&sort_by=browser&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("browser")}{if $search.sort_by == "browser"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="270px"><a class="cm-ajax" href="{"`$c_url`&sort_by=current_url&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("current_url")}{if $search.sort_by == "current_url"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>		
		<th class="left" width="270px"><a class="cm-ajax" href="{"`$c_url`&sort_by=from_url&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("from_url")}{if $search.sort_by == "from_url"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>				
    </tr>
    </thead>

    {foreach from=$users_online item="user_data"}
    <tr>
      <td>{$user_data.client_ip|long2ip}</td>
      <td><i class="flag flag-{$user_data.country_code|strtolower}"></i>{$countries[$user_data.country_code|strtoupper]|default:__("unknown")}</td>
      <td><img src="{$images_dir}/addons/cp_power_statistics/browsers/{$user_data.browser|default:'unknown'}.png">&nbsp;{$user_data.browser|fn_get_browser_name|default:__("unknown")}</td>
      <td><div class="overflow-auto-270 nowrap">{$user_data.current_url}</div></td>      
      <td><div class="overflow-auto-270 nowrap">{$user_data.from_url}</div></td>       
    </tr>
    
    {/foreach}
    </table>  
<!--content_users_online--></div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

<div class="clearfix">
	{include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>
</form>
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $transitions}
                <li>{btn type="delete_selected" dispatch="dispatch[conversion_statistics.m_delete]" form="manage_first_transition_form"}</li>
        {/if}
        {hook name="orders:list_tools"}
        {/hook}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

{include file="common/mainbox.tpl" title=__("users_online")|ucfirst sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="manage_first_transitions"}