<div id="content_power_statistic" class="hidden">

<div class="control-group">
<label class="control-label" for="global_popularity">{__("global_popularity")}:</label>
<div class="controls">
  <label class="checkbox">
  <input type="hidden" name="product_data[global_popularity]" value="N" />
  <input id="global_popularity" type="checkbox" name="product_data[global_popularity]" value="Y" {if $product_data.global_popularity == "Y"}checked="checked"{/if}/>
  </label>
</div>

<label class="control-label" for="global_total_plus">{__("global_total_plus")}:</label>
<div class="controls">
  <label>
  <input type="text" id="global_total_plus" class="input-small" value="{$product_data.global_total_plus|default:0}" name="product_data[global_total_plus]" />
  </label>
</div>
</div>

{assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}

<table class="table">
<tr><th colspan="{$order_status_descr|count}"><span class="ps-label">{__("orders")}</span>
  <select class="ps-select" name="product_data[show_orders]">
      <option value="N" {if $product_data.show_orders == "N"}selected="selected"{/if}>{__("hide")}</option>
      <option value="H" {if $product_data.show_orders == "H"}selected="selected"{/if}>{__("header")}</option>
      <option value="B" {if $product_data.show_orders == "B"}selected="selected"{/if}>{__("bottom")}</option>
      <option value="I" {if $product_data.show_orders == "I"}selected="selected"{/if}>{__("under_image")}</option>    
      <option value="O" {if $product_data.show_orders == "O"}selected="selected"{/if}>{__("after_options")}</option>    
      <option value="T" {if $product_data.show_orders == "T"}selected="selected"{/if}>{__("tab")}</option>    
  </select>
  <select class="ps-select" name="product_data[orders_view]">
      <option value="L" {if $product_data.orders_view == "L"}selected="selected"{/if}>{__("list")}</option>
      <option value="T" {if $product_data.orders_view == "T"}selected="selected"{/if}>{__("table")}</option>
  </select>
  <span class="ps-label">{__("show_header")}:<span class="ps-content"><input type="hidden" name="product_data[show_orders_header]" value="N" />
  <input type="checkbox" name="product_data[show_orders_header]" value="Y" {if $product_data.show_orders_header == "Y"}checked="checked"{/if}/></span></span>
  <span class="ps-delim"></span><span class="ps-label">{__("show_only_total")}:<span class="ps-content"><input type="hidden" name="product_data[show_only_total_orders]" value="N" />
  <input type="checkbox" name="product_data[show_only_total_orders]" value="Y" {if $product_data.show_only_total_orders == "Y"}checked="checked"{/if}/></span></span>
  <span class="ps-delim"></span>
  <span class="ps-label">{__("total_plus")}:&nbsp;</span><input type="text" class="input-small" value="{$product_data.total_plus|default:0}" name="product_data[total_plus]" />
</th></tr>
<tr>
{foreach from=$order_status_descr item="order"}
<td class="center">
  {$order}
</td>
{/foreach}
</tr>
<tr>
{foreach from=$order_status_descr key="order_key" item="order"}
<td class="center">
  <a href="{"orders.manage&p_ids=`$product_data.product_id`&status=`$order_key`"|fn_url}">{$orders[$order_key]['count']|default:0}</a>
   <span class="ps-label"><span class="ps-content"><input type="checkbox" name="product_data[show_orders_items][]" value="{$order_key}" {if $order_key|in_array:$product_data.show_orders_items}checked="checked"{/if}/></span></span>
</td>
{/foreach}
</tr>
</table>

<table class="table">
<tr><th colspan="5"><span class="ps-label">{__("popularity")}</span>
  <select class="ps-select" name="product_data[show_popularity]">
      <option value="N" {if $product_data.show_popularity == "N"}selected="selected"{/if}>{__("hide")}</option>
      <option value="H" {if $product_data.show_popularity == "H"}selected="selected"{/if}>{__("header")}</option>
      <option value="B" {if $product_data.show_popularity == "B"}selected="selected"{/if}>{__("bottom")}</option>
      <option value="I" {if $product_data.show_popularity == "I"}selected="selected"{/if}>{__("under_image")}</option>    
      <option value="O" {if $product_data.show_popularity == "O"}selected="selected"{/if}>{__("after_options")}</option>    
      <option value="T" {if $product_data.show_popularity == "T"}selected="selected"{/if}>{__("tab")}</option>    
  </select>
  <select class="ps-select" name="product_data[popularity_view]">
      <option value="L" {if $product_data.popularity_view == "L"}selected="selected"{/if}>{__("list")}</option>
      <option value="T" {if $product_data.popularity_view == "T"}selected="selected"{/if}>{__("table")}</option>
  </select>
  <span class="ps-label">{__("show_header")}:<span class="ps-content"><input type="hidden" name="product_data[show_popularity_header]" value="N" />
  <input type="checkbox" name="product_data[show_popularity_header]" value="Y" {if $product_data.show_popularity_header == "Y"}checked="checked"{/if}/></span></span>
  <span class="ps-delim"></span><span class="ps-label">{__("show_only_total")}:<span class="ps-content"><input type="hidden" name="product_data[show_only_total_popularity]" value="N" />
  <input type="checkbox" name="product_data[show_only_total_popularity]" value="Y" {if $product_data.show_only_total_popularity == "Y"}checked="checked"{/if}/></span></span>  
</th></tr>
<tr>
<td class="center">
  {__("viewed")}
</td>
<td class="center">
  {__("added")|ucfirst}
</td>
<td class="center">
  {__("deleted")}
</td>
<td class="center">
  {__("bought")}
</td>
<td class="center">
  {__("total")}
</td>
</tr>
<tr>
<td class="center">
    
  <input type="hidden" name="popularity[product_id]" value="{$product_data.product_id}" />
  
  <input type="text" name="popularity[viewed]" value="{$ps_popularity.viewed|default:0}" class="input-small" />
  <input type="hidden" name="product_data[show_viewed]" value="N" />
  <input type="checkbox" name="product_data[show_viewed]" value="Y" {if $product_data.show_viewed == "Y"}checked="checked"{/if}/> 
</td>
<td class="center">
  <input type="text" name="popularity[added]" value="{$ps_popularity.added|default:0}" class="input-small" />
  <input type="hidden" name="product_data[show_added]" value="N" />
  <input type="checkbox" name="product_data[show_added]" value="Y" {if $product_data.show_added == "Y"}checked="checked"{/if}/>   
</td>
<td class="center">
  <input type="text" name="popularity[deleted]" value="{$ps_popularity.deleted|default:0}" class="input-small" />
  <input type="hidden" name="product_data[show_deleted]" value="N" />
  <input type="checkbox" name="product_data[show_deleted]" value="Y" {if $product_data.show_deleted == "Y"}checked="checked"{/if}/>   
</td>
<td class="center">
  <input type="text" name="popularity[bought]" value="{$ps_popularity.bought|default:0}" class="input-small" />
  <input type="hidden" name="product_data[show_bought]" value="N" />
  <input type="checkbox" name="product_data[show_bought]" value="Y" {if $product_data.show_bought == "Y"}checked="checked"{/if}/>  
</td>
<td class="center">
  <input type="text" name="popularity[total]" value="{$ps_popularity.total|default:0}" class="input-small" />
  <input type="hidden" name="product_data[show_total]" value="N" />
  <input type="checkbox" name="product_data[show_total]" value="Y" {if $product_data.show_total == "Y"}checked="checked"{/if}/>  
</td>
</tr>
</table>

</div>