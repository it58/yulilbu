<div id="content_referers" class="hidden">

{assign var="search" value=$ref_search}

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id="pagination_contents_ref" search=$search}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents_ref"}

{if $referers}
<div id="content_referers_data">
    <table width="100%" class="table table-middle first-transition">
    <thead>
    <tr>
		<th class="left" width="100px"><a class="cm-ajax" href="{"`$c_url`&sort_by=client_ip&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("client_ip")}{if $search.sort_by == "client_ip"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="140px"><a class="cm-ajax" href="{"`$c_url`&sort_by=country_code&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("country")}{if $search.sort_by == "country_code"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="140px"><a class="cm-ajax" href="{"`$c_url`&sort_by=browser&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("browser")}{if $search.sort_by == "browser"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
		<th class="left" width="540px"><a class="cm-ajax" href="{"`$c_url`&sort_by=referer&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("referer")}{if $search.sort_by == "referer"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>		
		<th class="left" width="140px"><a class="cm-ajax" href="{"`$c_url`&sort_by=timestamp&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("time")}{if $search.sort_by == "timestamp"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>				
    </tr>
    </thead>

    {foreach from=$referers item="ref_data"}
    <tr>
      <td>{$ref_data.client_ip|long2ip}</td>
      <td><i class="flag flag-{$ref_data.country_code|strtolower}"></i>{$countries[$ref_data.country_code|strtoupper]|default:__("unknown")}</td>
      <td><img src="{$images_dir}/addons/cp_power_statistics/browsers/{$ref_data.browser|default:'unknown'}.png">&nbsp;{$ref_data.browser|fn_get_browser_name|default:__("unknown")}</td>
      <td><div class="overflow-auto-540 nowrap">{$ref_data.referer}</div></td>      
      <td>{$ref_data.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>       
    </tr>
    
    {/foreach}
    </table>  
<!--content_referers_data--></div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

<div class="clearfix">
	{include file="common/pagination.tpl" div_id="pagination_contents_ref" search=$ref_search}
</div>

</div>