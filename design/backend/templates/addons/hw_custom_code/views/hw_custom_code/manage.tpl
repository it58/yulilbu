{capture name="mainbox"}

        <form id='form' action="{""|fn_url}" method="post" name="custom_code_form" class="form-horizontal form-edit" enctype="multipart/form-data">
            <input type="hidden" name="fake" value="1" />

                    <div class="control-group ">
                        <label for="elm_head" class="control-label"> </label>
                        <div class="controls">
                            <input type="hidden" value="N" name="custom_code[all_languages]">
                            <label for="elm_all_languages" class="checkbox inline">
                            {__('apply_to_all_languages')}<input type="checkbox" value="Y"{if $custom_code.all_languages=='Y'} checked="checked"{/if} id="elm_all_languages" name="custom_code[all_languages]">
                                        </label>
                        </div>
                    </div>


                {include file="common/subheader.tpl" title=__("hw_cc_head") target="#acc_head"}
                <div id="acc_head" class="collapse in">

                    <div class="control-group ">
                        <label for="elm_head" class="control-label">{__('hw_cc_code')}:</label>
                        <div class="controls">
                            <textarea class="input-large" rows="2" cols="55" id="elm_head" name="custom_code[head]">{$custom_code.head nofilter}</textarea>
                        </div>
                    </div>

                </div>

                {include file="common/subheader.tpl" title=__("hw_cc_styles") target="#acc_styles"}
                <div id="acc_styles" class="collapse in">

                    <div id="content_hw_cc_styles">
                            <div class="control-group ">
                                <label for="elm_styles" class="control-label">{__('hw_cc_code')}:</label>
                                <div class="controls">
                                    <textarea class="input-large" rows="2" cols="55" id="elm_styles" name="custom_code[styles]">{$custom_code.styles nofilter}</textarea>
                                </div>
                            </div>
                    </div>

                </div>

                {include file="common/subheader.tpl" title=__("hw_cc_scripts") target="#acc_scripts"}
                <div id="acc_scripts" class="collapse in">                

                    <div id="content_hw_cc_scripts">
                            <div class="control-group ">
                                <label for="elm_scripts" class="control-label">{__('hw_cc_code')}:</label>
                                <div class="controls">
                                    <textarea class="input-large" rows="2" cols="55" id="elm_scripts" name="custom_code[scripts]">{$custom_code.scripts nofilter}</textarea>
                                </div>
                            </div>
                    </div>

                </div>

                {include file="common/subheader.tpl" title=__("hw_cc_footer") target="#acc_footer"}
                <div id="acc_footer" class="collapse in">     

                    <div id="content_hw_cc_footer">
                            <div class="control-group ">
                                <label for="elm_footer" class="control-label">{__('hw_cc_code')}:</label>
                                <div class="controls">
                                    <textarea class="input-large" rows="2" cols="55" id="elm_footer" name="custom_code[footer]">{$custom_code.footer nofilter}</textarea>
                                </div>
                            </div>
                    </div>

                </div>                    

        {capture name="buttons"}
                {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="custom_code_form" but_name="dispatch[hw_custom_code.update]" hide_second_button=true but_text=__('save')}
        {/capture}
        </form>

{literal}
<script>
$(function(){
        $("#form").find("textarea").each(function () {
            $(this).on('keyup', function () {
                fn_hw_custom_code_height($(this));
            });
            fn_hw_custom_code_height($(this));
        });
});

function fn_hw_custom_code_height($obj){
    $obj.css('height', (Math.max($obj.val().split("\n").length,3)*21)+"px" );
}

</script>
{/literal}
{/capture}

{include file="common/mainbox.tpl"
    title=__("hw_custom_code")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
     select_languages=true
}