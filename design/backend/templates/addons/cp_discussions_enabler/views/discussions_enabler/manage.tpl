{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="discussions_enabler_form" class="form-horizontal form-edit ">
<input type="hidden" name="fake" value="1" />

{include file="common/subheader.tpl" title=__("cp_mass_update") target="#acc_mass_update"}
<div id="acc_mass_update" class="collapse in">
    <div class="control-group">
        <label class="control-label cm-required" for="discussion_item">{__("cp_item_type")}</label>
        <div class="controls">
            <select name="discussions_settings[item_type]" id="discussion_item" onchange=fn_check_discussions_variants($(this).val());>
                <option value="" selected="selected">{__("please_select_one")}</option>
                {if $runtime.company_id && "MULTIVENDOR"|fn_allowed_for}
                    <option value="P">{__("products")}</option>
                    {if $addons.blog.status == "A"} 
                         <option value="B">{__("blog")}</option>
                    {/if}
                    <option value="A">{__("pages")}</option>                    
                    <option value="O">{__("orders")}</option>
                {else}
                    <option value="P">{__("products")}</option>
                    {if $addons.blog.status == "A"} 
                         <option value="B">{__("blog")}</option>
                    {/if}
                    <option value="A">{__("pages")}</option>
                    <option value="C">{__("categories")}</option>
                    <option value="O">{__("orders")}</option>
                {/if}
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label cm-required" for="discussion_type">{__("cp_discussions_type")}</label>
        <div class="controls">
            <select name="discussions_settings[discussion]" id="discussion_type">
                <option value="" selected="selected">{__("please_select_one")}</option>
                <option value="B">{__("communication")} {__("and")} {__("rating")}</option>
                <option value="C">{__("communication")}</option>
                <option value="R">{__("rating")}</option>
                <option value="D">{__("disabled")}</option>
            </select>
         </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="cp_only_empty">{__("cp_only_empty")}</label>
        <div class="controls">
            <input type="hidden" value="N" name="discussions_settings[cp_only_empty]"/>
            <input type="checkbox" value="Y" name="discussions_settings[cp_only_empty]" id="cp_only_empty"/>
        </div>
    </div>
</div>
<script>
    function fn_check_discussions_variants(value) {
        if (value == 'O') {
            $("#discussion_type option[value='R']").attr("disabled", "disabled");
            $("#discussion_type option[value='B']").attr("disabled", "disabled");
        } else {
            $("#discussion_type option[value='R']").removeAttr("disabled");
            $("#discussion_type option[value='B']").removeAttr("disabled");
        }
    }
</script>
{include file="buttons/save.tpl" but_name="dispatch[discussions_enabler.mass_update]" but_role="submit-link" but_target_form="discussions_enabler_form"} 
</form>

<form action="{""|fn_url}" method="post" name="discussions_preset_form" class="form-horizontal form-edit ">
<input type="hidden" name="fake" value="1" />

{include file="common/subheader.tpl" title=__("cp_preset_configuration") target="#acc_preset_configuration"}
<div id="acc_preset_configuration" class="collapse in">
   
   {assign var="items" value=""|fn_get_discussions_items}

   {foreach from=$items item="item" key="key"}
        <div class="control-group">
            <label class="control-label" for="discussion_type_{$key}">{$item}</label>
            <div class="controls">
                <select  name="discussion_preset[{$key}]" id="discussion_type_{$key}">
                    {if !$discussion_preset}
                         <option selected="selected" value="">{__("cp_not_set")}</option>
                    {/if}
                    {if $key != "O"}
                    <option {if $discussions_preset.$key == "B"}selected="selected"{/if} value="B">{__("communication")} {__("and")} {__("rating")}</option>
                    {/if}
                    <option {if $discussions_preset.$key == "C"}selected="selected"{/if} value="C">{__("communication")}</option>
                    {if $key != "O"}
                    <option {if $discussions_preset.$key == "R"}selected="selected"{/if} value="R">{__("rating")}</option>
                    {/if}
                    <option {if $discussions_preset.$key == "D"}selected="selected"{/if} value="D">{__("disabled")}</option>
                </select>
            </div>
        </div>
   {/foreach}
</div>

{include file="buttons/save.tpl" but_name="dispatch[discussions_enabler.save_preset]" but_role="submit-link" but_target_form="discussions_preset_form"}
</form>

{/capture}

{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox}
