<div id="sd_seo_jsonld_logo_uploader" class="in collapse{if !$runtime.company_id && !fn_allowed_for('MULTIVENDOR') && !$runtime.simple_ultimate} disable-overlay-wrap{/if}">
    {if !$runtime.company_id && !fn_allowed_for('MULTIVENDOR') && !$runtime.simple_ultimate}
    <div class="disable-overlay" id="sd_seo_jsonld_logo_disable_overlay"></div>
    {/if}
    <div class="control-group">
        <label class="control-label" for="elm_sd_seo_jsonld_logo">{__('sd_seo_jsonld.company_logo')} <a class="cm-tooltip" title="{__('sd_seo_jsonld.ttc_company_logo')}"><i class="icon-question-sign"></i></a>:</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="jsonld_company_logo" image_object_type="jsonld_company_logo" image_pair=$company_logo no_thumbnail=true}
            {if fn_allowed_for("ULTIMATE") && !$runtime.company_id}
            <div class="right update-for-all">
                {include file="buttons/update_for_all.tpl" display=true object_id="settings" name="settings[sd_seo_jsonld_logo_update_all_vendors]" hide_element="sd_seo_jsonld_logo_uploader"}
            </div>
            {/if}
        </div>
    </div>
</div>
<script type="text/javascript">
    Tygh.$(document).ready(function(){
    var $ = Tygh.$;
    $('.cm-update-for-all-icon[data-ca-hide-id=sd_seo_jsonld_logo_uploader]').on('click', function() {
        $('#sd_seo_jsonld_logo_uploader').toggleClass('disable-overlay-wrap');
        $('#sd_seo_jsonld_logo_disable_overlay').toggleClass('disable-overlay');
    });
});
</script>
