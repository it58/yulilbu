<div class="control-group {$no_hide_input_if_shared_product}" id="product_categories">
{math equation="rand()" assign="rnd"}
{if $smarty.request.category_id}
    {assign var="request_category_id" value=","|explode:$smarty.request.category_id}
{else}
    {assign var="request_category_id" value=""}
{/if}
<label for="ccategories_{$rnd}_ids" class="control-label cm-required">{__("categories")}</label>
<div class="controls">
    {include file="pickers/categories/picker.tpl" hide_input=$product_data.shared_product company_ids=$product_data.company_id rnd=$rnd data_id="categories" input_name="wkimporter_cat_addon[category_ids]" radio_input_name="wkimporter_cat_addon[main_category]" main_category=$wkimporter_settings.general.wkimporter_cat_addon.main_category item_ids=$wkimporter_settings.general.wkimporter_cat_addon.category_ids|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="category_ids" disable_no_item_text=true view_mode="list" but_meta="btn" show_active_path=true}
</div>
<!--product_categories--></div>