<!-- This pattern was overridden by sd_shipping_by_product add-on -->
{hook name="order_management:shipping_method"}
<div class="control-group">
    <div class="control-label">
        <h4 class="subheader">{__("shipping_method")}</h4>
    </div>
</div>
{if $product_groups}
    {foreach from=$product_groups key=group_key item=group}
        <div class="control-group">
            {foreach from=$group.products item="product" key="item_id"}
                <label class="control-label">
                    {if $product.product}
                        {$product.product nofilter}
                    {else}
                        {$product.product_id|fn_get_product_name}
                    {/if}
                </label>
                {if $group.shippings && !$group.shipping_no_required}
                    {if $product.extra.shipping_info.shipping_id}
                        <div class="controls">
                            {$product.extra.shipping_info.shipping_id|fn_get_shipping_name} ({$product.extra.shipping_info.delivery_time}) - {include file="common/price.tpl" value=$product.extra.shipping_info.shipping_cost}
                        </div>
                    {/if}
                {elseif $group.shipping_no_required}
                    {__("no_shipping_required")}
                {else}
                    {__("text_no_shipping_methods")}
                    {assign var="is_empty_rates" value="Y"}
                {/if}
            {/foreach}
        </div>
    {/foreach}
{else}
    <span class="text-error">{__("text_no_shipping_methods")}</span>
{/if}
{/hook}