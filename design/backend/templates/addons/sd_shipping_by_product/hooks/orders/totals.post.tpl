{if $order_info.products}
    <h4 class="subheader">{__("addons.sd_shipping_by_product.shipping_detailed_information")}</h4>
    <div class="table-responsive-wrapper">
        <table class="table table-middle table-responsive ty-mb-s">
            <thead>
                <tr>
                    <th>{__("product")}</th>
                    <th>{__("shipping_method")}</th>
                    <th>{__("shipping_cost")}</th>
                    <th>{__("delivery_time")}</th>
                </tr>
            </thead>
            {foreach from=$order_info.products item="product"}
                <tr>
                    <td data-th="{__("product")}">{$product.product}</td>
                    <td data-th="{__("shipping_method")}">{if $product.extra.shipping_info.shipping_id}{$product.extra.shipping_info.shipping_id|fn_get_shipping_name}{else}-{/if}</td>
                    <td data-th="{__("shipping_cost")}">{include file="common/price.tpl" value=$product.extra.shipping_info.shipping_cost}</td>
                    <td data-th="{__("delivery_time")}">{$product.extra.shipping_info.delivery_time}</td>
                </tr>
            {/foreach}
        </table>
    </div>
{/if}