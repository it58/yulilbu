<hr>
{include file="common/subheader.tpl" title=__("addons.sd_shipping_by_product.product_shipping") target="#acc_shipping_details"}
<div id="acc_shipping_details" class="collapse in">
    <div class="control-group">
        <label class="control-label cm-required" for="elm_shipping_method">{__("shipping_method")}:</label>
        <div class="controls">
            <select class="span5" id="elm_shipping_method" name="product_data[shipping_id]">
                <option value="">{__('addons.sd_shipping_by_product.please_select_shipping')}</option>
                {foreach from=$shipping_methods key="shipping_id" item="shipping_name"}
                    <option {if $product_data.shipping_id == $shipping_id}selected="selected"{/if} value="{$shipping_id}">{$shipping_name}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label cm-required" for="elm_shipping_cost">{__("shipping_cost")}:</label>
        <div class="controls">
            <input type="text" name="product_data[shipping_cost]" id="elm_shipping_cost" size="10" value="{$product_data.shipping_cost}" class="input-small" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label cm-required" for="elm_delivery_time">{__("delivery_time")}:</label>
        <div class="controls">
            <select class="span5" id="elm_delivery_time" name="product_data[delivery_time]">
                {foreach from=$delivery_time_list key="delivery_time_id" item="delivery_time"}
                    <option {if $product_data.delivery_time == $delivery_time_id}selected="selected"{/if} value="{$delivery_time_id}">{$delivery_time}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div id="available_countries">
        {include file="common/double_selectboxes.tpl"
            title=__("addons.sd_shipping_by_product.available_countries")
            first_name="product_data[available_countries]"
            first_data=$product_data.available_countries
            second_name="all_countries"
            second_data=$all_available_countries}
    </div>
</div>