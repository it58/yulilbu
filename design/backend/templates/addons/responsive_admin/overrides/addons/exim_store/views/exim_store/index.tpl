{capture name="mainbox"}

<div id="import_store">
    {if $step == 1}
    <div id="import_step_1">
        {include file="common/subheader.tpl" title=__("es_select_store_to_import") target="#store_import_step_1"}
        <form action="{""|fn_url}" method="GET" name="import_step_1" enctype="multipart/form-data" class="form-horizontal">
           <div id="store_import_step_1" class="in collapse">
               <fieldset>
                   <div class="control-group ">
                       <label class="control-label">{__("es_local_store")} <a class="cm-tooltip" title="{__("es_local_store_tooltip")}"><i class="icon-question-sign"></i></a>:</label>
                       <div class="controls">
                           <input type="text" name="store_data[path]" id="store_data_path" size="44" value="{$store_data.path}" {if $step != 1}disabled{/if}>
                               <input type="submit" class="btn" name="dispatch[exim_store.index.step_1]" value="{__("es_local_store_validate_path")}">
                           </span>
                       </div>
                   </div>
                   <br>
               </fieldset>
           </div>
        </form>
    </div>
    {/if}
    {if $step == 2}
    <div id="import_step_2">
        {include file="common/subheader.tpl" title=__("es_enter_settings") target="#store_import_step_2"}
        <form action="{""|fn_url}" method="POST" name="import_step_2" class=" cm-comet cm-ajax form-horizontal" enctype="multipart/form-data">
            <div id="store_import_step_2" class="in collapse">
            <fieldset>
                {if "ULTIMATE"|fn_allowed_for}
                    <div class="control-group">
                        <label for="store_data_storefront" class="control-label cm-required">{__("es_storefront_url")}:</label>
                            <div class="controls">
                                <input type="text" name="store_data[storefront]" id="store_data_storefront" size="44" value="{$store_data.storefront}" class="input-text"/>
                            </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="store_data_secure_storefront">{__("es_secure_storefront_url")}:</label>
                            <div class="controls">
                                <input type="text" name="store_data[secure_storefront]" id="store_data_secure_storefront" size="44" value="{$store_data.secure_storefront}" class="input-text"/>
                            </div>
                    </div>
                {/if}

                <div class="control-group cm-required">
                    <label for="store_data_db_host" class="control-label cm-required">{__("es_db_host")}:</label>
                    <div class="controls">
                        <input type="text" name="store_data[db_host]" id="store_data_db_host" size="44" value="{$store_data.db_host}" class="input-text " />
                    </div>
                </div>
                <div class="control-group cm-required">
                    <label for="store_data_db_name" class="control-label cm-required">{__("es_db_name")}:</label>
                    <div class="controls">
                        <input type="text" name="store_data[db_name]" id="store_data_db_name" size="44" value="{$store_data.db_name}" class="input-text "/>
                    </div>
                </div>
                <div class="control-group cm-required">
                    <label for="store_data_db_user" class="control-label cm-required">{__("es_db_user")}:</label>
                    <div class="controls">
                        <input type="text" name="store_data[db_user]" id="store_data_db_user" size="44" value="{$store_data.db_user}" class="input-text "/>
                    </div>
                </div>
                <div class="control-group cm-required">
                    <label for="store_data_db_password" class="control-label cm-required">{__("es_db_password")}:</label>
                    <div class="controls">
                        <input type="text" name="store_data[db_password]" id="store_data_db_password" size="44" value="{$store_data.db_password}" class="input-text " />
                    </div>
                </div>
                <div class="control-group cm-required">
                    <label for="store_data_table_prefix" class="control-label cm-required">{__("es_table_prefix")}:</label>
                   <div class="controls">
                        <input type="text" name="store_data[table_prefix]" id="store_data_table_prefix" size="44" value="{$store_data.table_prefix}" class="input-text "/>
                   </div>
                </div>
            </fieldset>
            </div>

            {capture name="buttons"}
                {btn type="text" text=__("cancel") href="exim_store.index"}
                {btn type="list" dispatch="dispatch[exim_store.index.step_2]" text=__("import") form="import_step_2"}
            {/capture}

        </form>
    </div>
    {/if}
<!--import_store--></div>

{/capture}
{include file="common/mainbox.tpl" title=__("es_exim_store") content=$smarty.capture.mainbox title_extra=$smarty.capture.title_extra buttons=$smarty.capture.buttons}