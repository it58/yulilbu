<div class="control-group">
	<label class="control-label" for="ab_merchant_id">{__("login")}:</label>
	<div class="controls">
		<input type="text" name="payment_data[processor_params][login]" id="ab_merchant_id" value="{$processor_params.login}" class="input-text" size="60" />
	</div>
</div>

<div class="control-group">
    <label class="control-label" for="ab_shared_secret">{__("shared_secret")}:</label>
    <div class="controls">
        <input type="password" name="payment_data[processor_params][shared_secret]" id="ab_shared_secret" value="{$processor_params.shared_secret}"   size="60">
    </div>
</div>

<div class="control-group">
	<label class="control-label" for="ab_currency">{__("currency")}:</label>
	<div class="controls">
		<select name="payment_data[processor_params][currency]" id="ab_currency">
			<option value="EUR"{if $processor_params.currency == "EUR"} selected="selected"{/if}>{__("currency_code_eur")}</option>
			<option value="USD"{if $processor_params.currency == "USD"} selected="selected"{/if}>{__("currency_code_usd")}</option>
		</select>
	</div>
</div>