{include file="common/letter_header.tpl"}

{if $message_company_name} {__("dear")} {$message_company_name}, {else} {__("hello")}, {/if}<br /><br />

{__("you_have_a_new_message")}{if $message_user_name} {__("from")|lower} {$message_user_name}{/if}.<br /><br />

<b><a href="{$ticket_url}">{__("check_message")}</a></b>

{include file="common/letter_footer.tpl"}