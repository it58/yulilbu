{include file="common/letter_header.tpl"}

{__('sd_vmfv_vendor_vacation_notification_to_admin')}

{if $from_vacation}
    <br><br>
    {__('sd_vmfv_vendor_return_vacation_notification_to_admin')}<br>
    {foreach from=$from_vacation item="vacation_id"}
        {if $vendors.$vacation_id}
            {$vendors.$vacation_id.company}({$vendors.$vacation_id.email})<br>
        {else}
            {__('sd_vmfv_unknown_vendor')}:{$vacation_id}<br>
        {/if}
    {/foreach}
{/if}

{if $goto_vacation}
    <br><br>
    {__('sd_vmfv_vendor_go_to_vacation_notification_to_admin')}<br>
    {foreach from=$goto_vacation item="vacation_id"}
        {if $vendors.$vacation_id}
            {$vendors.$vacation_id.company}({$vendors.$vacation_id.email})<br>
        {else}
            {__('sd_vmfv_unknown_vendor')}:{$vacation_id}<br>
        {/if}
    {/foreach}
{/if}

{include file="common/letter_footer.tpl"}