{** block-description:vendor_banner_block **}
{if $vendor_banner_data.icon}
<div>
{if $vendor_banner_data}	
		<div class="ty-banner__image-wrapper">
		<div style="text-align:center;">
			{if $vendor_banner_data.url != ""}<a href="{$vendor_banner_data.url|fn_url}"> {/if}
			{include file="common/image.tpl" images=$vendor_banner_data image_auto_size=true}
		{if $vendor_banner_data.url != ""}</a>{/if}
		</div>
		</div>	
{/if}
</div>
{/if}