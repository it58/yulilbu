
{if !$smarty.request.extra}
<script type="text/javascript">
(function(_, $) {
    _.tr('text_items_added', '{__("text_items_added")|escape:"javascript"}');

    $.ceEvent('on', 'ce.formpost_badges_form', function(frm, elm) {

        var badges = {};

        if ($('input.cm-item:checked', frm).length > 0) {
            $('input.cm-item:checked', frm).each( function() {
                var id = $(this).val();
                var item = $('#badge_' + id).text();
                badges[id] = $('#badge_' + id).text();
              
            });
            {literal}
            $.cePicker('add_js_item', frm.data('caResultId'), badges, 'b', {
                '{badge_id}': '%id',
                '{badge_name}': '%item'
            });
            {/literal}

            $.ceNotification('show', {
                type: 'N', 
                title: _.tr('notice'), 
                message: _.tr('text_items_added'), 
                message_state: 'I'
            });
        }

        return false;
    });

}(Tygh, Tygh.$));
</script>
{/if}
</head>
<form action="{$smarty.request.extra|fn_url}" data-ca-result-id="{$smarty.request.data_id}" method="post" name="badges_form">
{if $badges}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th>
        {include file="common/check_items.tpl"}</th>
    <th>{__("badges")}</th>
</tr>
</thead>
{foreach from=$badges item=badge}
<tr>
    <td>
        <input type="checkbox" name="{$smarty.request.checkbox_name|default:"badges_ids"}[]" value="{$badge.id}" class="cm-item" /></td>
    <td id="badge_{$badge.id}" width="100%">{$badge.badge_name}</td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{if $badges}
<div class="buttons-container">
    {include file="buttons/add_close.tpl" but_text=__("add") but_close_text=__("badges_and_close") is_js=$smarty.request.extra|fn_is_empty}
</div>
{/if}

</form>
