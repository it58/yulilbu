<style type="text/css">
    .badge_slide{
    border: 1px solid #ddd;
    float: right;
    margin: 15px 15px 20px 25px;
    overflow: hidden;
    text-align: center;
    width: 200px;
    }
   
</style>
{hook name="companies:catalog"}

{assign var="title" value=__("all_vendors")}

{include file="common/pagination.tpl"}

{include file="views/companies/components/sorting.tpl"}

{if $companies}

{foreach from=$companies item=company key=key name="companies"}
{assign var="obj_id" value=$company.company_id}
{assign var="badges" value=$company.company_id|fn_banners_badges_get_seller_badges}

{assign var="obj_id_prefix" value="`$obj_prefix``$obj_id`"}
{include file="common/company_data.tpl" company=$company show_name=true show_descr=true show_rating=true show_logo=true}
<div class="ty-companies">
    <div class="ty-companies__img">
        {assign var="capture_name" value="logo_`$obj_id`"}
        {$smarty.capture.$capture_name nofilter}
        
        {assign var="rating" value="rating_$obj_id"}
        {$smarty.capture.$rating nofilter}
    </div>
    
    <div class="ty-companies__info">
        <div>
            {assign var="company_name" value="name_`$obj_id`"}
            {$smarty.capture.$company_name nofilter}
        </div>
        <div style="display: inline-block;">
            {assign var="company_descr" value="company_descr_`$obj_id`"}
             {$smarty.capture.$company_descr nofilter}
        </div>     
    </div>
    
    {if $badges}
        <div class="badge_slide">
            <p style="color: red">{__("seller_badges")}</p>
            <div id="badges_slider_{$obj_id}" class="owl-carousel">
                {foreach from=$badges item="badge" key="key"}
                    <div style="text-align: center;">
                        {if $badge.main_pair.image_id}
                            {if $badge.url != ""}<a class="banner__link" href="{$badge.url|fn_url}">{/if}
                                {include file="common/image.tpl" images=$badge.main_pair image_width=200}
                                {$badge.name nofilter}
                            {if $badge.url != ""}</a>{/if}
                       
                        {/if}
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}
</div>
<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var slider = context.find('#badges_slider_{$obj_id}');
        if (slider.length) {
            slider.owlCarousel({
                direction: '{$language_direction}',
                items: 1,
                singleItem : true,
                slideSpeed: 200,
                autoPlay: '{3 * 1000|default:false}',
                stopOnHover: true,
                pagination: true
            });
        }
    });
}(Tygh, Tygh.$));
</script>
{/foreach}

{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}

{include file="common/pagination.tpl"}

{capture name="mainbox_title"}{$title}{/capture}

{/hook}