{capture name="wk_quantity_sold_`$obj_prefix``$obj_id`"}
	{if $runtime.mode!= 'compare'}
		{if $product.quantity_sold}
            <span class="wk-product-sold-label">
                <span class="wk-product-label-item">
                    <span>{if $product.quantity_sold > '10000'}{$product.quantity_sold|substr:0:2}K+{else if $product.quantity_sold > '1000'}{$product.quantity_sold|substr:0:1}K+{else}{$product.quantity_sold}{/if} {__("sold")}</span>
                </span>
            </span>
        {/if}
	{/if}
{/capture}
{if $no_capture}
	{assign var="capture_name" value="wk_quantity_sold_`$obj_prefix``$obj_id`"}
	{$smarty.capture.$capture_name nofilter}
{/if}
