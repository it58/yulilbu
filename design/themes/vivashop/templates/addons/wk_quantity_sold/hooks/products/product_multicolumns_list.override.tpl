<div class="vs-grid vs-grid-table">
    <div class="preview-image">
        <div class="ty-center scroll-image animate scale">
            {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}
            {$smarty.capture.$discount_label nofilter}
            {**}
                {assign var="wk_quantity_sold" value="wk_quantity_sold_`$obj_prefix``$obj_id`"}
                {$smarty.capture.$wk_quantity_sold nofilter}
            {**}          
            {include file="views/products/components/product_icon.tpl" product=$product show_vs_gallery=true}

        </div>
    </div>

     <div class="title-wrapper">
        {if $item_number == "Y"}<span class="item-number">{$cur_number}.&nbsp;</span>{math equation="num + 1" num=$cur_number assign="cur_number"}{/if}
        {assign var="name" value="name_$obj_id"}
		<div class="title-wrapper-inner"><bdi>{$smarty.capture.$name nofilter}</bdi></div>
    </div>
    <div class="price-wrapper {if $settings.Appearance.show_prices_taxed_clean=="Y"}vs-grid-tax{/if}">
        <div class="clearfix prices-container">

            <div class="clearfix">
                <div class="ty-float-left">
                    {assign var="price" value="price_`$obj_id`"}
                    {$smarty.capture.$price nofilter}<br/>
                </div>
                            
                <div class="ty-float-right">
                {assign var="old_price" value="old_price_`$obj_id`"}
                {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}
                </div>

            </div>
            {assign var="clean_price" value="clean_price_`$obj_id`"}
            {$smarty.capture.$clean_price nofilter}
                        

            {assign var="list_discount" value="list_discount_`$obj_id`"}
            {$smarty.capture.$list_discount nofilter}


        </div>
    </div>
    <div class="product-description">
        <div class="rating-buttons clearfix">
            <div class="ty-float-left vs-rating">
                {assign var="rating" value="rating_$obj_id"}
                {$smarty.capture.$rating nofilter}
            </div>
            <div class="ty-float-right vs-buttons clearfix">
                {if $show_add_to_cart}
                    {if $settings.Appearance.enable_quick_view == 'Y'}
                        {include file="views/products/components/quick_view_link.tpl" quick_nav_ids=$quick_nav_ids}
                    {/if}
                    {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                    {$smarty.capture.$add_to_cart nofilter}
                {/if}
            </div>
        </div>
    </div>                
</div>