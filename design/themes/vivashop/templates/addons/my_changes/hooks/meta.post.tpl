{if $runtime.controller == 'products' && $runtime.mode == 'view'}
<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="{"products.view?product_id=`$product.product_id`"|fn_url}" />
<meta name="twitter:title" content="{$product.product}" />
<meta name="twitter:description" content="{$product.meta_description|default:$product.short_description|html_entity_decode}" />
<meta name="twitter:image" content="{$product.main_pair.detailed.https_image_path}" />
{/if}
{/if}