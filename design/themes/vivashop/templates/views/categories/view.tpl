{hook name="categories:view"}

<div id="category_products_{$block.block_id}">
{$depth="/"|explode:$category_data.id_path|count}

<div class="vs-category vs-category-level-{$depth}">

<div class="clearfix vs-category-header">
    <div class="vs-category-descr vs-categ-banner">
    {if ($category_data.description && $category_data.description != "") || $runtime.customization_mode.live_editor}
        <div class="ty-wysiwyg-content ty-mb-s vs-categ-descr-box" {live_edit name="category:description:{$category_data.category_id}"}>{$category_data.description nofilter}</div>
    {/if}
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth;
        //banner
        var banner=$(".category-banner").detach();
        $(".vs-category-descr").prepend(banner);
        {if $depth==1}
            banner.show();
        {/if}

        //scroller
        var scroller=$(".vs-scroller-category-product").detach();

        if (scroller.length){
            scroller.addClass("ty-float-right");
            $(".vs-category-header").append(scroller);
        }

        {if $depth==1}
            if (scroller.length){
                $(".vs-category-descr").addClass("ty-float-left");
                if (windowWidth > 1247){
                    var header_w=$(".vs-category-header").width();
                    var scroller_w=scroller.outerWidth(true);
                    $(".vs-category-descr").css("width",header_w-scroller_w);
                }else{
                    $(".vs-category-descr").css("width","646px");
                }
                scroller.show();
            }
        {/if}
        
        //disabled scroller buttons
        var buttons=$(".subcateg-scroller .owl-controls");
        var buttons_status=buttons.css('display');

        if(buttons_status != "block"){
            $(".owl-buttons div",buttons).addClass("vs-disabled");
            buttons.show();
        }
    });
}(Tygh, Tygh.$));
//]]>
</script>

{if $products}
{assign var="layouts" value=""|fn_get_products_views:false:0}
{if $category_data.product_columns}
    {assign var="product_columns" value=$category_data.product_columns}
{else}
    {assign var="product_columns" value=$settings.Appearance.columns_in_products_list}
{/if}

{if $layouts.$selected_layout.template}
    {include file="`$layouts.$selected_layout.template`" columns=$product_columns}
{/if}

{elseif !$subcategories || $show_no_products_block}
<p class="ty-no-items cm-pagination-container">{__("text_no_products")}</p>
{else}
<div class="cm-pagination-container clear-both"></div>
{/if}
</div>
<!--category_products_{$block.block_id}--></div>

{capture name="mainbox_title"}<span {live_edit name="category:category:{$category_data.category_id}"}>{$category_data.category}</span>{/capture}
{/hook}