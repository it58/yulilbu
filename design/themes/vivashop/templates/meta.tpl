{hook name="index:meta"}
{if $display_base_href}
<base href="{$config.current_location}/" />
{/if}

<meta http-equiv="Content-Type" content="text/html; charset={$smarty.const.CHARSET}" data-ca-mode="{$store_trigger}" data-vs-ver="2.18.471"/>

<meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width" />

{hook name="index:meta_description"}
<meta name="description" content="{$meta_description|html_entity_decode:$smarty.const.ENT_COMPAT:"UTF-8"|default:$location_data.meta_description}" />
{/hook}
<meta name="keywords" content="{$meta_keywords|default:$location_data.meta_keywords}" />
{/hook}
{$location_data.custom_html nofilter}
<link rel="apple-touch-icon" href="{$logos.favicon.image.image_path|fn_query_remove:'t'}">
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:url" content="{"products.view?product_id=`$product.product_id`"|fn_url}" />
<meta name="twitter:title" content="{$product.product}" />
<meta name="twitter:description" content="{$product.meta_description|default:$product.short_description|html_entity_decode}" />
<meta name="twitter:image" content="{$product.main_pair.detailed.https_image_path}" />
