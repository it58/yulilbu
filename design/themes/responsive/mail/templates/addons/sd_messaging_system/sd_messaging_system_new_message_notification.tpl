{include file="common/letter_header.tpl"}

{if $user_data.firstname} {__("dear")} {$user_data.firstname}, {else} {__("hello")}, {/if}<br /><br />

{__("you_have_a_new_message")}{if $message_company_name} {__("from")|lower} {$message_company_name}{/if}.<br /><br />

<b><a href="{$ticket_url}">{__("check_message")}</a></b>

{include file="common/letter_footer.tpl"}