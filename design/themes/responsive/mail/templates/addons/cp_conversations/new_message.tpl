{include file="common/letter_header.tpl"}

<h3>{__('you_have_new_undread_message')}</h3>

{__('use_following_link_to_check_your_inbox')} <a href="{$link}">{__('inbox_link')}</a>

{include file="common/letter_footer.tpl"}