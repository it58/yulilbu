{include file="common/letter_header.tpl"}

{__("dear")} {__("follower")|fn_sd_mb_strtolower},<br /><br />

{__("event_company_we_are_writting")} {__($lang_var, ["[vendor_name]" => $company_name, "[url]" => $url])|fn_sd_mb_strtolower}

{include file="common/letter_footer.tpl"}
