{include file="common/letter_header.tpl"}

{__("dear")} {$wallet_data.user_name},<br /><br />

{__("your_wallet_debited_with_amount")}<b>{include file="common/price.tpl" value=$wallet_data.amount}</b>{if !empty($wallet_data.order_id)}{__("regardeing_order_no")}: <b>{$wallet_data.order_id}</b>{/if}
<br /><br />
<small>
<strong>{__("debit_wallet_amount")} </strong>: {include file="common/price.tpl" value=$wallet_data.amount}<br /><br />
<strong>{__("total_wallet_amount")}</strong> : {include file="common/price.tpl" value=$wallet_data.total_cash}<br /><br /></small>
{include file="common/letter_footer.tpl"}