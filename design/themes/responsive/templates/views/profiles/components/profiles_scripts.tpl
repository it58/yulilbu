<script type="text/javascript">
(function(_, $) {

    /* Do not put this code to document.ready, because it should be
       initialized first
    */
    $.ceRebuildStates('init', {
<<<<<<< HEAD
        default_country: '{$settings.Checkout.default_country|escape:javascript}',
=======
        default_country: '{$settings.General.default_country|escape:javascript}',
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
        states: {$states|json_encode nofilter}
    });


    {literal}
    $.ceFormValidator('setZipcode', {
        US: {
            regexp: /^(\d{5})(-\d{4})?$/,
            format: '01342 (01342-5678)'
        },
        CA: {
            regexp: /^(\w{3} ?\w{3})$/,
            format: 'K1A OB1 (K1AOB1)'
        },
        RU: {
            regexp: /^(\d{6})?$/,
            format: '123456'
        }
    });
    {/literal}

}(Tygh, Tygh.$));
</script>
