
{if $addons.cp_ajax_update.disable_button_delete == 'Y' && $addons.cp_ajax_update.disable_button_backspace == 'Y'}
{literal}
    <script>
        $(document).ready(function() {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                if (e.keyCode != 8 && e.keyCode != 46) {
                    setTimeout(function () { $('#button_cart').click(); }, 1000);
                }
            });
        });
        $(document).ajaxComplete(function(event,request, settings) {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                if (e.keyCode != 8 && e.keyCode != 46) {
                    setTimeout(function () { $('#button_cart').click(); }, 1000);
                }
            });
            var url = settings.url;
            if (url.indexOf("products.options") >= 0) {
                $('#button_cart').click();
            }
        }); 
    </script>
{/literal}
{/if}
{if $addons.cp_ajax_update.disable_button_delete == 'N' && $addons.cp_ajax_update.disable_button_backspace == 'Y'}
{literal}
    <script>
        $(document).ready(function() {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                if (e.keyCode != 8) {
                    setTimeout(function () { $('#button_cart').click(); }, 1000);
                }
            });
        });
        $(document).ajaxComplete(function(event,request, settings) {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                if (e.keyCode != 8) {
                    setTimeout(function () { $('#button_cart').click(); }, 1000);
                }
            });
            var url = settings.url;
            if (url.indexOf("products.options") >= 0) {
                $('#button_cart').click();
            }
        }); 
    </script>
{/literal}
{/if}
{if $addons.cp_ajax_update.disable_button_delete == 'Y' && $addons.cp_ajax_update.disable_button_backspace == 'N'}
{literal}
    <script>
        $(document).ready(function() {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                if (e.keyCode != 46) {
                    setTimeout(function () { $('#button_cart').click(); }, 1000);
                }
            });
        });
        $(document).ajaxComplete(function(event,request, settings) {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                if (e.keyCode != 46) {
                    setTimeout(function () { $('#button_cart').click(); }, 1000);
                }
            });
            var url = settings.url;
            if (url.indexOf("products.options") >= 0) {
                $('#button_cart').click();
            }
        }); 
    </script>
{/literal}
{/if}
{if $addons.cp_ajax_update.disable_button_delete == 'N' && $addons.cp_ajax_update.disable_button_backspace == 'N'}
{literal}
    <script>
        $(document).ready(function() {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
        });
        $(document).ajaxComplete(function(event,request, settings) {
            $("form[name='checkout_form']").addClass("cm-ajax");
            $("form[name='checkout_form']").addClass("cm-ajax-full-render");
            
            $(".ty-value-changer__increase").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".ty-value-changer__decrease").click(function() {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            $(".cm-amount").bind('keyup', function(e) {
                setTimeout(function () { $('#button_cart').click(); }, 1000);
            });
            var url = settings.url;
            if (url.indexOf("products.options") >= 0) {
                $('#button_cart').click();
            }
        }); 
    </script>
{/literal}
{/if}

