{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{if $search.sort_order == "asc"}
    {assign var="sort_sign" value="<i class=\"ty-icon-down-dir\"></i>"}
{else}
    {assign var="sort_sign" value="<i class=\"ty-icon-up-dir\"></i>"}
{/if}

{include file="common/pagination.tpl"}
<table class="ty-table">
<thead>
    <tr>
        <th class="ty-follower-list__name"><a class="cm-ajax" href="{"`$c_url`&sort_by=firstname&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("first_name")} {__("and")} {__("last_name")|lower}</a>{if $search.sort_by == "firstname"}{$sort_sign nofilter}{/if}</th>
        <th class="ty-follower-list__date"><a class="cm-ajax" href="{"`$c_url`&sort_by=followed_on&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("date")}</a>{if $search.sort_by == "followed_on"}{$sort_sign nofilter}{/if}</th>
        <th class="ty-follower-list__email">{__("email")}</th>
    </tr>
</thead>

{foreach from=$followers item="user"}
<tr>
    <td>{$user.firstname|default:" - "} {$user.lastname|default:" - "}</td>
    <td>{$user.followed_on|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
    <td><a href="mailto:{$user.email}">{$user.email}</a></td>
</tr>
{foreachelse}
<tr class="ty-table__no-items">
    <td colspan="3"><p class="ty-no-items">{__("no_items")}</p></td>
</tr>
{/foreach}

</table>
{include file="common/pagination.tpl"}
{** / followers description section **}

{capture name="mainbox_title"}{__("my_followers")}{/capture}
