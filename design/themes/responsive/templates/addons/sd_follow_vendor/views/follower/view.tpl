{include file="common/pagination.tpl"}

<h1 class="ty-mainbox-title ty-my-followers">{__('following_vendors')}</h1>

{if $auth.user_type == V}
    <div class="ty-companies__my-followers">
        {include file="buttons/button.tpl" 
            but_text=__("my_followers")
            but_href="follower.list"
            but_role="text"
            but_meta="ty-btn__primary ty-vendor-followers"}
    </div>
{/if}

{include file="addons/sd_follow_vendor/views/follower/components/sorting.tpl"}

<form action="{""|fn_url}" method="post" name="follower">
{if $companies}
{foreach from=$companies item=company key=key name="companies"}
    {assign var="obj_id" value=$company.company_id}
    {assign var="obj_id_prefix" value="`$obj_prefix``$obj_id`"}

    {include file="common/company_data.tpl" company=$company show_name=true show_descr=true show_rating=true show_logo=true}
    <div class="ty-companies ty-companies-follow">
        <div class="ty-companies__check">
            <label>
                <input type="hidden" name="reporters[{$company.company_id}]" value="0" />
                <input type="checkbox" name="reporters[{$company.company_id}]" value="1" {if $company.is_selected}checked="checked"{/if} class="checkbox" />
            </label>
        </div>

        <div class="ty-companies__img">
            {assign var="capture_name" value="logo_`$obj_id`"}
            {$smarty.capture.$capture_name nofilter}

            {assign var="rating" value="rating_$obj_id"}
            {$smarty.capture.$rating nofilter}
        </div>
        
        <div class="ty-companies__info">
            {assign var="company_name" value="name_`$obj_id`"}
            {$smarty.capture.$company_name nofilter}

            <div>
                {assign var="company_descr" value="company_descr_`$obj_id`"}
                {$smarty.capture.$company_descr nofilter}
            </div>
        </div>

        <div class="ty-companies__unfollow">
            {include file="buttons/button.tpl" 
                but_text=__("remove_from_follow_list")
                but_href="follower.delete?company_id=`$company.company_id`"
                but_role="text"
                but_meta="ty-btn__primary cm-post ty-follow-vendor"}
        </div>
    </div>
{/foreach}
    {include file="buttons/save.tpl" 
        but_name="dispatch[follower.update]" 
        but_role="submit-link" 
        but_target_form="follower"
        but_meta="ty-btn__secondary ty-follow-update-news"}
</form>

{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}

{include file="addons/sd_follow_vendor/views/follower/components/company_news.tpl"}
{include file="common/pagination.tpl"}
