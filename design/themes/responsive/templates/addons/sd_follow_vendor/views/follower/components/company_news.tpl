{include file="common/subheader.tpl" class="ty-companies_news-header" title=__("vendors_news")}

{if $vendor_news}
    <ul>
        {foreach from=$vendor_news item="one"}
            <div class="ty-news__item">
                <div class="ty-news__date">{$one.published_on|date_format:"`$settings.Appearance.date_format`"}</div>
                <div class="ty-news__author">{__("by")} {$one.company}</div>
                <div class="ty-news__description">
                    <span>{__($one.lang_var, ["[vendor_name]" => $one.company, "[url]" => $one.url])}</span>
                </div>
            </div>
        {/foreach}
    </ul>

{hook name="follower:vendor_news"}{/hook}

{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}
