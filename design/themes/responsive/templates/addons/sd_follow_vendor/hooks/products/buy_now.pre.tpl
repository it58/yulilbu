{$c_url = $redirect_url|default:$config.current_url|escape:url}
{if $auth.user_id}
    {if $is_subscribed}
        {include file="buttons/button.tpl"
            but_text=__("remove_from_follow_list")
            but_href="follower.delete?company_id=`$product.company_id`&amp;redirect_url=`$c_url`"
            but_role="text"
            but_meta="ty-btn__text cm-post ty-follow-vendor"}
    {else}
        {include file="buttons/button.tpl"
            but_text=__("add_to_follow_list")
            but_href="follower.add?company_id=`$product.company_id`&amp;redirect_url=`$c_url`"
            but_role="text"
            but_meta="ty-btn__text cm-post ty-follow-vendor"}
    {/if}
{/if}