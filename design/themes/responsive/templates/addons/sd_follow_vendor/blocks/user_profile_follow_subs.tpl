{** block-description:block_user_profile_follow_subs **}

{assign var="block_id" value=$block.snapping_id}

{if $auth.user_id == $user_data.user_id}
<div class="ty-sd_user_profile-section">
    <div class="ty-sd_user_profile__body ty-sd_user_profile-profile-module clear">
        <div>
            <a href="{'follower.view'|fn_url}"><h3>{__("vendors_news")}</h3></a>
            <p class="wrap"></p>
            {hook name="user_profile:subs"}
                {if $vendor_news}
                    <ul>
                        {foreach from=$vendor_news item="one"}
                            <div class="ty-news__item">
                                <div class="ty-news__date">{$one.published_on|date_format:"`$settings.Appearance.date_format`"}</div>
                                <div class="ty-news__author">{__("by")} {$one.company}</div>
                                <div class="ty-news__description">
                                    <span>{__($one.lang_var, ["[vendor_name]" => $one.company, "[url]" => $one.url])}</span>
                                </div>
                            </div>
                        {/foreach}
                    </ul>
                {else}
                    <p class="ty-no-items">{__("no_items")}</p>
                {/if}
            {/hook}
        </div>
    </div>
</div>
{/if}