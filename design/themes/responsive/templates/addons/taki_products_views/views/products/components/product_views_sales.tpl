<div class="taki-grid-products-data">
	{if $product.sales_amount}
		<span class="taki-product-sales">
			<i class="fa fa-tags" aria-hidden="true"></i><span class="tk-value">{$product.sales_amount}</span>
		</span>
	{/if}
	{if $product.taki_views}
		<span class="taki-product-views">
			<i class="fa fa-eye" aria-hidden="true"></i><span class="tk-value">{$product.taki_views}</span>
		</span>
	{/if}
</div>