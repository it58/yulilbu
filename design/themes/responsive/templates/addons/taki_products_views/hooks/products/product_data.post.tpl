<!-- taki-cat-v4 - using capture in product_data -->
{capture name="taki_views_sales_`$obj_id`"}
	{include file="addons/taki_products_views/views/products/components/product_views_sales.tpl" product=$product}
{/capture}


{*
	how to use :
	{assign var="taki_views_sales" value="taki_views_sales_$obj_id"}
	{$smarty.capture.$taki_views_sales nofilter}
*}