{if $addons.sd_messaging_system.show_btn_message_to_vendor_company_page == "Y" && $company_data.company_id && !($auth.user_type == 'V' && $auth.company_id == $company_data.company_id) && ($company_data.company_id|fn_check_company_messenger_access)}
    {include file="buttons/button.tpl"
        but_meta="text-button ty-sd_messaging_system__create-ticket ty-btn__text"
        but_text=__('send_message_to_vendor')
        but_role="act"
        but_href=fn_url("messenger.create_ticket&company_id=`$company_data.company_id`")
        but_icon="sd_messaging_system__icon"
    }
{/if}