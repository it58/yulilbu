{if !$hide_form && $addons.sd_messaging_system.show_btn_message_to_vendor_product_page == "Y" && $product.product_id && $product.company_id && !($auth.user_type == 'V' && $auth.company_id == $product.company_id) && ($product.company_id|fn_check_company_messenger_access)}

    {if $addons.sd_messaging_system.show_btn_text == "icon_text"}
        {assign var="but_text" value=__('send_message_to_vendor')}
        {assign var="but_icon" value="sd_messaging_system__icon"}
    {else if $addons.sd_messaging_system.show_btn_text == "icon"}
        {assign var="but_text" value=""}
        {assign var="but_icon" value="sd_messaging_system__icon icon-only"}
    {else}
        {assign var="but_text" value=__('send_message_to_vendor')}
        {assign var="but_icon" value=""}
    {/if}

    {include file="buttons/button.tpl"
        but_meta="ty-btn__big ty-sd_messaging_system__create-ticket ty-btn__`$addons.sd_messaging_system.btn_color`"
        but_text="`$but_text`"
        but_role="act"
        but_href=fn_url("messenger.create_ticket&company_id=`$product.company_id`&product_id=`$product.product_id`")
        but_icon="`$but_icon`"
    }

{/if}


