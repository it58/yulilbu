{if $auth.user_id && !($auth.user_type == 'V' && !fn_sd_messaging_system_check_messenger_permission($auth.user_id))}
    <li class="ty-account-info__item ty-dropdown-box__item ty-account-info__item--messenger"><a class="ty-account-info__a underlined" href="{"messenger.tickets_list"|fn_url}" rel="nofollow" >{__("messages")}&nbsp;({$new_messages_amount})</a></li>
{/if}
