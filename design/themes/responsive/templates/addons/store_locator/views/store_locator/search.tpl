<<<<<<< HEAD
{if $shipping.service_params.display}
    {$display_type = $shipping.service_params.display}
{else}
    {$display_type = "ML"}
{/if}

{if $display_type != "L"}
    {$display_pickup_map = true}
{/if}

<form action="{"store_locator.search"|fn_url}" id="store_locator_search_form">
    <input type="hidden" name="result_ids" value="store_locator_search_controls,store_locator_location,store_locator_search_block_*" />
    <input type="hidden" name="full_render" value="Y" />

    <div id="store_locator_search_controls" class="store-locator__location--wrapper">
        <h2 class="store-locator__step-title">{__("store_locator.stores_and_pickup_points")}</h2>

        {if $store_locations}
            <div class="store-locator__location store-locator__location--city sl-search-control">
                <label class="store-locator__select-label" for="store_locator_search_city">{__("city")}:</label>
                <select name="sl_search[city]" id="store_locator_search_city" class="store-locator__select">
                    <option value="">{__("all")}</option>
                    {foreach $cities as $city}
                        <option value="{$city}" {if $sl_search.city === $city}selected{/if}>{$city}</option>
                    {/foreach}
                </select>
            </div>
        {/if}

    <!--store_locator_search_controls--></div>

<div id="store_locator_location">

    {if $store_locations}
        <div class="store-locator">
            {include file="addons/store_locator/components/map_and_list_store.tpl" store_locations=$store_locations group_key=0 store_locations_count=$store_locations_count}
        </div>
    {else}
        <p class="ty-no-items">{__("no_data")}</p>
    {/if}
<!--store_locator_location--></div>
{script src="js/addons/store_locator/pickup_search.js"}
{script src="js/addons/store_locator/map.js"}
</form>
=======
{assign var="map_provider" value=$addons.store_locator.map_provider}
{assign var="map_provider_api" value="`$map_provider`_map_api"}
{assign var="map_customer_templates" value="C"|fn_get_store_locator_map_templates}
{assign var="map_container" value="map_canvas"}

{if $store_locations}
    {if $map_customer_templates && $map_customer_templates.$map_provider}
        {include file=$map_customer_templates.$map_provider}
    {/if}

    <div class="ty-store-location">
        <div class="ty-store-location__map-wrapper" id="{$map_container}"></div>
        <div class="ty-wysiwyg-content ty-store-location__locations-wrapper" id="stores_list_box">
            {foreach from=$store_locations item=loc key=num}
                <div class="ty-store-location__item" id="loc_{$loc.store_location_id}">
                    <h3 class="ty-store-location__item-title">{$loc.name}</h3>
                    
                    <span class="ty-store-location__item-desc">{$loc.description nofilter}</span>

                    {if $loc.city || $loc.country_title}
                        <span class="ty-store-location__item-country">{if $loc.city}{$loc.city}, {/if}{$loc.country_title}</span>
                    {/if}
                    
                    <div class="ty-store-location__item-view">
                        {include file="buttons/button.tpl" but_role="text" but_meta="cm-sl-map-view-location ty-btn__tertiary" but_scroll="#map_canvas" but_text=__("view_on_map") but_extra="data-ca-latitude={$loc.latitude} data-ca-longitude={$loc.longitude}"}
                    </div>
                </div>
                {if $store_locations|count > 1}
                    <hr />
                {/if}
            {/foreach}

            {if $store_locations|count > 1}
                <div class="ty-store-location__item ty-store-location__item-all_stores">
                    <h3 class="ty-store-location__item-title">{__("all_stores")}</h3>
                    <div class="ty-store-location__item-view">{include file="buttons/button.tpl" but_scroll="#map_canvas" but_role="text" but_meta="cm-sl-map-view-locations ty-btn__tertiary" but_text=__("view_on_map")}</div>
                </div>
            {/if}
        </div>
    </div>
{else}
    <p class="ty-no-items">{__("no_data")}</p>
{/if}

{capture name="mainbox_title"}{__("store_locator")}{/capture}
>>>>>>> 98366e48fbf45a4d3d9a67b3baa6f46cdd3ff868
