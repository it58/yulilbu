{if $product.details_layout == 'bigpicture_template'}
	<style type="text/css">
	.badge_name {
		margin-top: 10px;
	}
	.badge_slide{
		color: #798d8e;
		padding: 21px 0 0;
    	top:136px;
    	margin-left: 5%;
    	width: 200px;
    	text-align: center;
	}
@media screen and (max-width: 768px){
 	.badge_slide{
 		position: static;
 	}
}
</style>
{else}
<style type="text/css">
	.badge_name {
		margin-top: 10px;
	}
	.badge_slide{
		color: #798d8e;
		padding: 21px 0 0;
    	position: absolute;
    	top:155px;
    	right: 0;
    	width: 200px;
    	text-align: center;
	}
@media screen and (max-width: 768px){
 	.badge_slide{
 		position: static;
 	}
}
</style>
{/if}
{assign var="badges" value=$product.badges}

{if $badges|@count}
<div class="badge_slide">
	<p style="color: red">{__("seller_badges")}</p>
	<div id="badges_slider_{$block.snapping_id}" class="owl-carousel">
		{foreach from=$badges item="badge" key="key"}
			{if $badge.main_pair.image_id}
			<div style="text-align: center;">
			{if $badge.url != ""}<a class="banner__link" href="{$badge.url|fn_url}">{/if}
			{include file="common/image.tpl" images=$badge.main_pair class="ty-banner__image" image_width=200}
			{if $badge.url != ""}</a>{/if}			
			<div class="badge_name">
			{$badge.name nofilter}
			</div>
			</div>
			{/if}
		{/foreach}
	</div>
</div>
<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var slider = context.find('#badges_slider_{$block.snapping_id}');
        if (slider.length) {
            slider.owlCarousel({
                direction: '{$language_direction}',
                items: 1,
                singleItem : true,
                slideSpeed: 200,
                autoPlay: '{3 * 1000|default:false}',
                stopOnHover: true,
                pagination: true
            });
        }
    });
}(Tygh, Tygh.$));
</script>
{/if}
