<style type="text/css">
	.badge_slide{
	border: 1px solid #ddd;
    float: left;
    margin-left: 10%;
    overflow: hidden;
    text-align: center;
    width: 200px;
	}
    @media screen and (max-width: 1064px){
    .badge_slide{
    margin-left:0%;
    margin-right: 100%;
    }
</style>
{hook name="companies:view"}
{assign var="badges" value=$company_data.company_id|fn_banners_badges_get_seller_badges}
{script src="js/lib/owlcarousel/owl.carousel.min.js"}
{assign var="obj_id" value=$company_data.company_id}
{assign var="obj_id_prefix" value="`$obj_prefix``$obj_id`"}
    {include file="common/company_data.tpl" company=$company_data show_name=true show_descr=true show_rating=true show_logo=true hide_links=true}
    <div class="ty-company-detail clearfix">

        <div id="block_company_{$company_data.company_id}" class="clearfix">
            <h1 class="ty-mainbox-title">{$company_data.company}</h1>

            <div class="ty-company-detail__top-links clearfix">
                {hook name="companies:top_links"}
                    <div class="ty-company-detail__view-products" id="company_products">
                        <a href="{"companies.products?company_id=`$company_data.company_id`"|fn_url}">{__("view_vendor_products")}
                            ({$company_data.total_products} {__("items")})</a>
                    </div>
                {/hook}
            </div>
            <div class="ty-company-detail__info">
                <div class="ty-company-detail__logo">
                    {assign var="capture_name" value="logo_`$obj_id`"}
                    {$smarty.capture.$capture_name nofilter}
                </div>
                <div class="ty-company-detail__info-list ty-company-detail_info-first">
                    <h5 class="ty-company-detail__info-title">{__("contact_information")}</h5>
                    {if $company_data.email}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("email")}:</label>
                            <span><a href="mailto:{$company_data.email}">{$company_data.email}</a></span>
                        </div>
                    {/if}
                    {if $company_data.phone}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("phone")}:</label>
                            <span>{$company_data.phone}</span>
                        </div>
                    {/if}
                    {if $company_data.fax}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("fax")}:</label>
                            <span>{$company_data.fax}</span>
                        </div>
                    {/if}
                    {if $company_data.url}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("website")}:</label>
                            <span><a href="{$company_data.url}">{$company_data.url}</a></span>
                        </div>
                    {/if}
                </div>
                <div class="ty-company-detail__info-list">
                    <h5 class="ty-company-detail__info-title">{__("shipping_address")}</h5>

                    <div class="ty-company-detail__control-group">
                        <span>{$company_data.address}</span>
                    </div>
                    <div class="ty-company-detail__control-group">
                        <span>{$company_data.city}
                            , {$company_data.state|fn_get_state_name:$company_data.country} {$company_data.zipcode}</span>
                    </div>
                    <div class="ty-company-detail__control-group">
                        <span>{$company_data.country|fn_get_country_name}</span>
                    </div>
                </div>
                {if $badges}
        	  	<div class="ty-company-detail__info-list badge_slide">
            	  	<p style="color: red" class="ty-company-detail__info-title">{__("seller_badges")}</p>
                    <div id="badges_slider_{$block.snapping_id}" class="owl-carousel">

            			{foreach from=$badges item="badge" key="key"}
    			            <div style="text-align: center;">
    			                {if $badge.main_pair.image_id}
    			                    {if $badge.url != ""}<a class="banner__link" href="{$badge.url|fn_url}">{/if}
    			                     {include file="common/image.tpl" images=$badge.main_pair class="ty-banner__image" image_width=200}
    			                        {$badge.name nofilter}
    			                    {if $badge.url != ""}</a>{/if}
    			                {else}
    			                    <div class="ty-wysiwyg-content">
    			                        {$badge.name nofilter}
    			                    </div>
    			                {/if}
    			            </div>
    			        {/foreach}
        			</div>
    			</div>
                {/if}
            </div>
        </div>
        {capture name="tabsbox"}
            <div id="content_description"
                 class="{if $selected_section && $selected_section != "description"}hidden{/if}">
                {if $company_data.company_description}
                    <div class="ty-wysiwyg-content">
                        {$company_data.company_description nofilter}
                    </div>
                {/if}
            </div>
            {hook name="companies:tabs"}
            {/hook}

        {/capture}
    </div>
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section}

{/hook}

<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var slider = context.find('#badges_slider_{$block.snapping_id}');
        if (slider.length) {
            slider.owlCarousel({
                direction: '{$language_direction}',
                items: 1,
                singleItem : true,
                slideSpeed: 200,
                autoPlay: '{3 * 1000|default:false}',
                stopOnHover: true,
                pagination: true
            });
        }
    });
}(Tygh, Tygh.$));
</script>
