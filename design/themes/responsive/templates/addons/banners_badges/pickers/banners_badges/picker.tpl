{assign var="data_id" value=$data_id|default:"companies_list"}
{if !$rnd}{math equation="rand()" assign="rnd"}{/if}
{assign var="data_id" value="`$data_id`_`$rnd`"}
{assign var="view_mode" value=$view_mode|default:"mixed"}
{assign var="start_pos" value=$start_pos|default:0}


{script src="js/tygh/picker.js"}
{include file="views/profiles/components/profiles_scripts.tpl"}
{if $show_add_button}
    {if $multiple}
        {assign var="_but_text" value=$but_text|default:__("add_badges")}
        {assign var="_but_role" value="add"}
    {/if}
    <div class="pull-right">
    {include file="buttons/button.tpl" but_id="opener_picker_`$data_id`" but_href="assign_badge.picker?display=`$display`&picker_for=`$picker_for`&extra=`$extra_var`&checkbox_name=`$checkbox_name`&root=`$default_name`&except_id=`$except_id`&data_id=`$data_id``$extra_url`"|fn_url but_text=$_but_text but_role=$_but_role but_target_id="content_`$data_id`" but_meta="cm-dialog-opener btn" but_icon="icon-plus"}
    </div>
{/if}

{if !$extra_var && $view_mode != "button"}
    {if $multiple}
        <table width="100%" class="table table-middle">
        <thead>
        <tr>
            <th width="100%">{__("name")}</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody id="{$data_id}"{if !$item_ids} class="hidden"{/if}>
    {else}
        <div id="{$data_id}" class="{if $multiple && !$item_ids}hidden{elseif !$multiple}{if $view_mode != "list"}cm-display-radio{/if}{/if} choose-category">
    {/if}
    {if $multiple}
        <tr class="hidden">
            <td colspan="{if $positions}3{else}2{/if}">
    {/if}
            <input id="{if $input_id}{$input_id}{else}m{$data_id}_ids{/if}" type="hidden" class="cm-picker-value" name="{$input_name}" value="{if $item_ids|is_array}{assign var="_item_ids" value=$item_ids}{","|implode:$_item_ids}{else}{$item_ids}{/if}" {$extra} />
    {if $multiple}
            </td>
        </tr>
    {/if}
        {if $multiple}

            {include file="addons/banners_badges/pickers/assign_badge/js.tpl" clone=true badge_id="`$ldelim`badge_id`$rdelim`" badge_name="`$ldelim`badge_name`$rdelim`" holder=$data_id clone=true input_name=$input_name}
        {/if}
    {if $multiple}
        </tbody>
        <tbody id="{$data_id}_no_item"{if $item_ids} class="hidden"{/if}>
        <tr class="no-items">
            <td colspan="{if $positions}3{else}2{/if}"><p>{$no_item_text|default:__("no_items") nofilter}</p></td>
        </tr>
        </tbody>
    </table>
    {else}</div>{/if}
{/if}
