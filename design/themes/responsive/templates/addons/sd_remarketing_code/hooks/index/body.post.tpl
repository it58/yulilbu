{if $addons.sd_remarketing_code.track_code}
    <script type="text/javascript" >
        var google_tag_params = {
            {$remarketing_values.id_var|escape:javascript nofilter}: {$remarketing_values.dest_id nofilter},
            {if $remarketing_values.category_name && !empty($remarketing_values.category_var)}{$remarketing_values.category_var nofilter}:{$remarketing_values.category_name nofilter},{/if}
            {$remarketing_values.pagetype_var|escape:javascript nofilter}: "{$remarketing_values.page_type|escape:javascript nofilter}",
            {if $remarketing_values.totalvalue}{$remarketing_values.totalvalue_var|escape:javascript nofilter}: {$remarketing_values.totalvalue nofilter}{/if}
        };
        /* <![CDATA[ */
        var google_conversion_id = {$addons.sd_remarketing_code.track_code};
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <div class="hidden">
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
    </div>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/{$addons.sd_remarketing_code.track_code}/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
{/if}