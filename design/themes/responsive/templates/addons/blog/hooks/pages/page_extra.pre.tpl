
{if $page.page_type == $smarty.const.PAGE_TYPE_BLOG}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="banner-box">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
		  <div class="item active">
			<img src="/design/themes/responsive/images/slider-image.jpg" alt=""/>
			<div class="caption-text">
				<span class="catname">Technology</span>
				<h1>Harman Kando Onxy Studio Mini Reviews & Experinces</h1>
				<span class="author-name">July 23, 2018 <b>by</b> Brian Paul</span>
			</div>
		  </div>
		  <div class="item">
			<img src="/design/themes/responsive/images/slider-image.jpg" alt=""/>
			<div class="caption-text">
				<span class="catname">Technology</span>
				<h1>Harman Kando Onxy Studio Mini Reviews & Experinces</h1>
				<span class="author-name">July 23, 2018 <b>by</b> Brian Paul</span>
			</div>
		  </div>
		  <div class="item">
		   <img src="/design/themes/responsive/images/slider-image.jpg" alt=""/>
		   <div class="caption-text">
				<span class="catname">Technology</span>
				<h1>Harman Kando Onxy Studio Mini Reviews & Experinces</h1>
				<span class="author-name">July 23, 2018 <b>by</b> Brian Paul</span>
			</div>
		  </div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
		</a>
	  </div>
  </div>
{if $subpages}

    {capture name="mainbox_title"}{/capture}
	<div class="category-list">
		<ul>
			<li class="active"><a href="#">All</a></li>
			<li><a href="#">Business</a></li>
			<li><a href="#">Technology</a></li>
			<li><a href="#">Fashion</a></li>
			<li><a href="#">Lifestyle</a></li>
			<li><a href="#">Entertainment</a></li>
			<li><a href="#">Others</a></li>
		</ul>
	</div>	
    <div class="ty-blog blogpost-list">
        {include file="common/pagination.tpl"}

        {foreach from=$subpages item="subpage"}
            <div class="ty-blog__item ty-column3">
                {if $subpage.main_pair}
					<a href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}">
						<div class="ty-blog__img-block">
							{include file="common/image.tpl" obj_id=$subpage.page_id images=$subpage.main_pair}
						</div>
					</a>
				{else}
					<a href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}">
						<div class="ty-blog__img-block">
							<img src="/design/themes/responsive/images/no-image.jpg" alt=""/>
						</div>
					</a>	
                {/if}
				<a class="category-name" href="#">Business, Lifestyle</>
				<a href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}">
                    <h2 class="ty-blog__post-title">
                        {$subpage.page|strip_tags|truncate:60 nofilter}
                    </h2>
                </a>
				<div class="ty-blog__date">{$subpage.timestamp|date_format:"`$settings.Appearance.date_format`"}</div>
				<div class="ty-blog__author">{__("by")} {$subpage.author}</div>
               <!-- <div class="ty-blog__description">
                    <div class="ty-wysiwyg-content">
                        <div>{*$subpage.spoiler nofilter*}{$subpage.spoiler|strip_tags|truncate:250 nofilter}</div>
                    </div>
                   <div class="ty-blog__read-more ty-mt-l">
                        <a class="ty-btn ty-btn__secondary" href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}">{__("blog.read_more")}</a>
                    </div>
                </div>-->
            </div>
        {/foreach}

        {include file="common/pagination.tpl"}
    </div>

{/if}

{if $page.description}
    {capture name="mainbox_title"}<span class="ty-blog__post-title" {live_edit name="page:page:{$page.page_id}"}>{$page.page}</span>{/capture}
{/if}

{/if}