{** block-description:blog.recent_posts_grid **}

{if $items}

{foreach from=$items item="page"}
            <div class="ty-blog-recent-posts-scroller__item">

                <div class="ty-blog-recent-posts-scroller__img-block">
                    <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}">
                        {include file="common/image.tpl" obj_id=$page.page_id images=$page.main_pair}
                    </a>
                </div>

                <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}">{$page.page}</a>

                <div class="ty-blog__date">{$page.timestamp|date_format:"`$settings.Appearance.date_format`"}</div>

            </div>
        {/foreach}

{/if}