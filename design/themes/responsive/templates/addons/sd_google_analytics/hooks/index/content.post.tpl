{if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $runtime.vendor_id != 0}
    {assign var="vendor_tracking_code" value=$runtime.vendor_id|sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1:$product.product_id}
{/if}

{assign var="ga_product_info" value=""|sd_MTgzOGUwOWQyZDkzZjU2MWY5MjQ2YzAx}
{if $ga_product_info}
<script type="text/javascript" class="cm-ajax-force">
    (function(_, $) {
        $(document).ready(function() {
            if (typeof(ga) != 'undefined') {

                {if $ga_product_info.addImpression}
                    {foreach from=$ga_product_info.addImpression item=ga_info}
                        ga('ec:addImpression', {
                            id: '{$ga_info.product_id|escape:javascript nofilter}',
                            name: '{$ga_info.name|escape:javascript nofilter}',
                            category: '{$ga_info.category|escape:javascript nofilter}',
                            brand: '{$ga_info.brand|escape:javascript nofilter}',
                            variant: '{$ga_info.variant|escape:javascript nofilter}',
                            list: '{$ga_info.list|escape:javascript nofilter}',
                            position: '{$ga_info.position|escape:javascript nofilter}'
                        });
                    {/foreach}
                {/if}
                {if $ga_product_info.addProduct}
                    {foreach from=$ga_product_info.addProduct item=ga_info}
                        ga('ec:addProduct', {
                            id: '{$ga_info.product_id|escape:javascript nofilter}',
                            name: '{$ga_info.name|escape:javascript nofilter}',
                            category: '{$ga_info.category|escape:javascript nofilter}',
                            brand: '{$ga_info.brand|sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky:$product|escape:javascript nofilter}',
                            variant: '{$ga_info.variant|escape:javascript nofilter}',
                        });

                        {if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for}
                            {assign var="vendor_tracking_code_for_product" value=$product.company_id|sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1:$product.product_id}

                            {if $vendor_tracking_code_for_product.tracker}
                                ga('{$vendor_tracking_code_for_product.tracker}.ec:addProduct', {
                                    id: '{$product.product_id|escape:javascript nofilter}',
                                    name: '{$product.product|escape:javascript nofilter}',
                                    category: '{$product.product_id|sd_NDI3MWFiZWU2NmEwNWNlYTU0ODA5YzNj|escape:javascript nofilter}',
                                    brand: '{$product.product_id|sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky:$product|escape:javascript nofilter}',
                                    variant: '{$product.product_id|sd_YmRkMjMwZTdlZDRkYTc2NjdmNGZlYmUw:$product.extra.product_options|escape:javascript nofilter}',
                                    price: '{$product.price|escape:javascript nofilter}',
                                    coupon: '',
                                    quantity: '{$product.amount|escape:javascript nofilter}'
                                });
                            {/if}
                        {/if}
                    {/foreach}
                {/if}
                {if $ga_product_info.addPromo}
                    {foreach from=$ga_product_info.addPromo item=ga_product_promotions}
                        {foreach from=$ga_product_promotions item=promotion}
                            ga('ec:addPromo', {
                                id: '{$promotion.id|escape:javascript nofilter}',
                                name: '{$promotion.name|escape:javascript nofilter}',
                                creative: '{$promotion.creative|escape:javascript nofilter}',
                                position: ''
                            });
                        {/foreach}
                    {/foreach}
                {/if}
                {if $ga_product_info.addPromo_banners}
                    {foreach from=$ga_product_info.addPromo_banners item=ga_banners_info}
                        {foreach from=$ga_banners_info item=ga_banner_info}
                            ga('ec:addPromo', {
                                id: '{$ga_banner_info.id|escape:javascript nofilter}',
                                name: '{$ga_banner_info.name|escape:javascript nofilter}',
                                creative: '{$ga_banner_info.creative|escape:javascript nofilter}',
                                position: ''
                            });
                        {/foreach}
                    {/foreach}
                {/if}
                ga('ec:setAction', 'detail');
                ga('send', 'event', 'product', 'details', {
                    nonInteraction: true
                });

                {if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $vendor_tracking_code.tracker}
                    ga('{$vendor_tracking_code.tracker}.ec:setAction', 'detail');
                    ga('{$vendor_tracking_code.tracker}.send', 'event', 'product', 'detail', {
                        nonInteraction: true
                    });
                {/if}

                {if $ga_product_info.addPromo_banners}
                    {foreach from=$ga_product_info.addPromo_banners item=ga_banners_info key=snapping_id}
                        var parent_elm_slider = $('#banner_slider_' + {$snapping_id});
                        if (!parent_elm_slider.length) {
                            var parent_elm_original = $('#banner_original_' + {$snapping_id});
                        }
                        var bkey_wisiwyg = bkey_image = 0;
                        var elm = '';
                        {foreach from=$ga_banners_info item=ga_banner_info key=banner_key}
                            if (parent_elm_slider.length) {
                                elm = parent_elm_slider.find('.ty-banner__image-item')[{$banner_key}];
                            } else {
                                {if $ga_banner_info.wysiwyg == 'Y'}
                                    elm = parent_elm_original.find('.ty-wysiwyg-content')[bkey_wisiwyg];
                                    bkey_wisiwyg++;
                                {else}
                                    elm = parent_elm_original.find('.ty-banner__image-wrapper')[bkey_image];
                                    bkey_image++;
                                {/if}
                            }
                            $(elm).on('click', 'a', function() {
                                ga('ec:addPromo', {
                                    id: '{$ga_banner_info.id|escape:javascript nofilter}',
                                    name: '{$ga_banner_info.name|escape:javascript nofilter}',
                                    creative: '{$ga_banner_info.creative|escape:javascript nofilter}',
                                    position: ''
                                });
                                ga('ec:setAction', 'promo_click');
                                ga('send', 'event', 'Internal Promotions', 'click', '{$ga_banner_info.name|escape:javascript nofilter}');
                            });
                        {/foreach}
                    {/foreach}
                {/if}
            }
        });
    }(Tygh, Tygh.$));
    </script>
{/if}