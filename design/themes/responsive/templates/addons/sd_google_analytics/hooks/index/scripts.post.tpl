{script src="js/addons/sd_google_analytics/sd_google_analytics.js"}

{* adds the add-to-cart and remove-from-cart events to the cart page (dispatch=checkout.cart) *}

{if $sd_ga}
<script type="text/javascript" class="cm-ajax-force">
(function(_, $) {
    $(document).ready(function(){
        if (typeof(ga) != 'undefined') {
            {if $sd_ga.deleted}
                var is_delete_tracker = false;
                {foreach from=$sd_ga.deleted item='product'}
                    ga('ec:addProduct', {
                        id: '{$product.id|escape:javascript nofilter}',
                        name: '{$product.name|escape:javascript nofilter}',
                        variant: '{$product.variant|escape:javascript nofilter}',
                        quantity: '{$product.quantity}'
                    });
                    {if $product.tracker}
                        is_delete_tracker = true;
                        delete_tracker = '{$product.tracker}';
                        
                        ga('{$product.tracker}.ec:addProduct', {
                            id: '{$product.id|escape:javascript nofilter}',
                            name: '{$product.name|escape:javascript nofilter}',
                            variant: '{$product.variant|escape:javascript nofilter}',
                            quantity: '{$product.quantity}'
                        });
                    {/if}
                {/foreach}

                ga('ec:setAction', 'remove');
                ga('send', 'event', 'UX', 'click', 'remove to cart'); // Send data using an event.

                if (is_delete_tracker === true) {
                    ga(delete_tracker + '.ec:setAction', 'remove');
                    ga(delete_tracker + '.send', 'event', 'UX', 'click', 'remove to cart');
                }

            {/if}
            {if $sd_ga.added}
                var is_added_tracker = false;

                {foreach from=$sd_ga.added item='product'}
                    ga('ec:addProduct', {
                        id: '{$product.id|escape:javascript nofilter}',
                        name: '{$product.name|escape:javascript nofilter}',
                        category: '{$product.category|escape:javascript nofilter}',
                        brand: '{$product.brand|escape:javascript nofilter}',
                        variant: '{$product.variant|escape:javascript nofilter}',
                        price: {$product.price|escape:javascript nofilter},
                        quantity: '{$product.quantity}',
                    });
                    {if $product.tracker}
                        is_added_tracker = true;
                        added_tracker = '{$product.tracker}';
                        
                        ga('{$product.tracker}.ec:addProduct', {
                            id: '{$product.id|escape:javascript nofilter}',
                            name: '{$product.name|escape:javascript nofilter}',
                            variant: '{$product.variant|escape:javascript nofilter}',
                            quantity: '{$product.quantity}'
                        });
                    {/if}
                {/foreach}

                ga('ec:setAction', 'add');
                ga('send', 'event', 'UX', 'click', 'add to cart'); // Send data using an event.

                if (is_added_tracker === true) {
                    ga(added_tracker + '.ec:setAction', 'add');
                    ga(added_tracker + '.send', 'event', 'UX', 'click', 'add to cart');
                }
            {/if}
        }
   });
}(Tygh, Tygh.$));
</script>
{/if}