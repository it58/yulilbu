{if $runtime.controller == 'checkout'}
{if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $cart}

    {* tracking vendor ga code if isset the $orders_info variable (dispatch=checkout.complete) *}

    {assign var="vendor_tracking_code" value=$cart|sd_OGNhNDA5OTUzOWI3YmU0YjAyZmIzN2Vm}
{else}
    {assign var="vendor_tracking_code" value=""}
{/if}

<script type="text/javascript">
(function(_, $) {
    function fn_sd_ga_vendor_tracker_action(vendor_tracking_code, option_name, current_step)
    {
        if (vendor_tracking_code && option_name && current_step) {
            var vendor_tracking_code = JSON.parse(vendor_tracking_code);

            $.each(vendor_tracking_code, function(key, vendor_tracker) {
                ga(vendor_tracker['tracker'] + '.ec:setAction','checkout', {
                    step: current_step,
                    option: option_name
                });
            });
        }
    }

    $(document).ready(function(){
        if (typeof(ga) != 'undefined') {
            {if $settings.Checkout.display_payment_step == 'N'}
                $(document).on('click', '.cm-checkout-place-order', function() {
                    $.ceEvent('on', 'ce.formpost_' + $(this).parents('form').prop('name'), function(form, elm) {
                        var shipping_names = JSON.parse('{$shipping_names|escape:json nofilter}');
                        var chosen_shippings = $.grep(form.serializeArray(), function(field) {
                            return field.name.search(/shipping_ids/) != -1;
                        });
                        var option_name = chosen_shippings.map(function(shipping) {
                            return shipping_names[shipping.value];
                        }, []).join(', ');

                        ga('ec:setAction','checkout', {
                            step: 3,
                            option: option_name,
                        });

                        {if $vendor_tracking_code}
                            var tracking_code = '{$vendor_tracking_code|json_encode|escape:javascript nofilter}';

                            fn_sd_ga_vendor_tracker_action(tracking_code, option_name, 3);
                        {/if}
                    });
                });
            {/if}
        }
   });
}(Tygh, Tygh.$));
</script>
{/if}
