{* send product_data to call_request js ceEvent *}

<input type="hidden" name="product_data[{$obj_id}][name]" value="{$product.product}" />
<input type="hidden" name="product_data[{$obj_id}][product_code]" value="{$product.product_code}" />
<input type="hidden" name="product_data[{$obj_id}][category]" value="{$product.product_id|sd_NDI3MWFiZWU2NmEwNWNlYTU0ODA5YzNj}"/>
<input type="hidden" name="product_data[{$obj_id}][brand]" value="{$product.product_id|sd_ZTJjMDk0ZDJiNzNkOTNlMjMyNDUwMjky:$product}"/>
<input type="hidden" name="product_data[{$obj_id}][variant]" value="{$product.product_id|sd_YmRkMjMwZTdlZDRkYTc2NjdmNGZlYmUw:$product.selected_options}"/>
{if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for}
	{assign var="tracker" value=$product.company_id|sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1:$product.product_id}
	{if $tracker.ga_code}
		<input type="hidden" name="product_data[{$obj_id}][ga_code]" value="{$tracker.ga_code}"/>
		<input type="hidden" name="product_data[{$obj_id}][tracker]" value="{$tracker.tracker}"/>
	{/if}
{/if}