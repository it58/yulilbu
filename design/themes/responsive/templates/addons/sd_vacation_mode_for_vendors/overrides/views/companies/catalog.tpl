<!-- override sd_vacation_mode_for_vendors -->
{hook name="companies:catalog"}

{assign var="title" value=__("all_vendors")}

{include file="common/pagination.tpl"}

{include file="views/companies/components/sorting.tpl"}

{if $companies}

{foreach from=$companies item=company key=key name="companies"}
{assign var="obj_id" value=$company.company_id}
{assign var="obj_id_prefix" value="`$obj_prefix``$obj_id`"}
{include file="common/company_data.tpl" company=$company show_name=true show_descr=true show_rating=true show_logo=true}
<div class="ty-companies">
    <div class="ty-companies__img">
        {assign var="capture_name" value="logo_`$obj_id`"}
        {$smarty.capture.$capture_name nofilter}
        
        {assign var="rating" value="rating_$obj_id"}
        {$smarty.capture.$rating nofilter}
    </div>
    
    <div class="ty-companies__info">
        {assign var="company_name" value="name_`$obj_id`"}
        {$smarty.capture.$company_name nofilter}

        <div>
            {assign var="company_descr" value="company_descr_`$obj_id`"}
            {$smarty.capture.$company_descr nofilter}
        </div>
        <div>   
            <br/>
            {if $company.vendor_in_vacation == 'Y' || $company.vendor_in_vacation_to_day == 'Y'}
                <label class="sd-vmfv-vacation-weekend">{__('sd_vmfv_vendor_in_vacation_weekend')}</label>
            {/if}

            {if $company.vendor_is_not_working_hours == 'Y' && $company.vendor_in_vacation != 'Y' && $company.vendor_in_vacation_to_day != 'Y'}
                <label class="sd-vmfv-vacation-weekend">{__('vendor_is_not_working',['[working_hours]' => $company.working_hours_from|cat:' - '|cat:$company.working_hours_to])} {__('sd_vmfv_date_timezone'|cat:$company.timezone_vacation)}</label>
            {/if}
        </div>    
    </div>
</div>  
{/foreach}

{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}

{include file="common/pagination.tpl"}

{capture name="mainbox_title"}{$title}{/capture}

{/hook}