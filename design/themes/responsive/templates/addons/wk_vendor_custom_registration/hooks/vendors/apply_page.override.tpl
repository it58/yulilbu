<div class="ty-company-fields">
    {include file="views/profiles/components/profiles_scripts.tpl"}

    <h1 class="ty-mainbox-title">{__("apply_for_vendor_account")}</h1>

    <div id="apply_for_vendor_account" >
        <form action="{"wk_vendor_custom_fields.apply_for_vendor"|fn_url}" method="post" name="apply_for_vendor_form">

                    {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section="O" title=''}

                    {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section="C" title=__("contact_information")}

                    {if $wk_vendor_custom_fields.S}
                       {assign var="sec_section_text" value=__("address")}
                        
                        {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section='S' body_id="sa" ship_to_another=true title=$sec_section_text address_flag=$profile_fields|fn_compare_shipping_billing ship_to_another=$ship_to_another wk_vendor_custom_fields=$wk_vendor_custom_fields}
                    {/if}
                {if !empty($all_sections)}
                    {foreach from=$all_sections key="key" item="section_data"}
                        {assign var="section_id" value=$section_data.section_id} 
                        
                        {if !empty($wk_vendor_custom_fields.$section_id)}
                            {include file="addons/wk_vendor_custom_registration/views/wk_vendor_custom_fields/components/wk_vendor_custom_fields.tpl" section=$section_data.section_id title=$section_data.description wk_vendor_custom_fields=$wk_vendor_custom_fields extra_fields=$extra_fields}
                       {/if}
                    {/foreach}
                {/if}
            {include file="addons/wk_vendor_custom_registration/common/image_verification.tpl" option="apply_for_vendor_account" align="left"}
            
            {if $settings.Vendors.need_agree_with_terms_n_conditions == "Y"}
                <div class="ty-control-group ty-company__terms">
                    <div class="cm-field-container">
                        {strip}
                        <label for="id_accept_terms{$suffix}" class="cm-check-agreement">
                            <input type="checkbox" id="id_accept_terms{$suffix}" name="accept_terms" value="Y" class="cm-agreement checkbox" {if $iframe_mode}onclick="fn_check_agreements('{$suffix}');"{/if} />
                            {capture name="terms_link"}
                                <a id="sw_terms_and_conditions_{$suffix}" class="cm-combination ty-dashed-link">
                                    {__("vendor_terms_n_conditions_name")}
                                </a>
                            {/capture}
                            {__("vendor_terms_n_conditions", ["[terms_href]" => $smarty.capture.terms_link])}
                        </label>
                        {/strip}

                        <div class="hidden" id="terms_and_conditions_{$suffix}">
                            {__("terms_and_conditions_content") nofilter}
                        </div>
                    </div>
                    <script type="text/javascript">
                        (function(_, $) {
                            $.ceFormValidator('registerValidator', {
                                class_name: 'cm-check-agreement',
                                message: '{__("vendor_terms_n_conditions_alert")|escape:javascript}',
                                func: function(id) {
                                    return $('#' + id).prop('checked');
                                }
                            });     
                        }(Tygh, Tygh.$));
                    </script>
                </div>
            {/if}
            <div class="buttons-container">
                {include file="buttons/button.tpl" but_text=__("submit") but_name="dispatch[wk_vendor_custom_fields.apply_for_vendor]" but_id="but_apply_for_vendor" but_meta="ty-btn__primary"}
            </div>
        </form>
    </div>
</div>