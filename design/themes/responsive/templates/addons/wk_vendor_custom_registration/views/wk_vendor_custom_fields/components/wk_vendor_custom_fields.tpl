{assign var="flag_check" value=1}
{foreach from=$wk_vendor_custom_fields.$section item=field name="wk_vendor_custom_fields"}		
		
		 {if $flag_check==1}
            {include file="common/subheader.tpl" title=$title}
         {/if}	
		{if $field.field_name}
		    {assign var="data_name" value="user_data"}
		    {assign var="data_id" value=$field.field_name}
		    {assign var="value" value=$user_data.$data_id}
		{else}
		    {assign var="data_name" value="user_data[fields]"}
		    {assign var="data_id" value=$field.field_id}
		    {assign var="value" value=$user_data.fields.$data_id}
		{/if}

		{assign var="skip_field" value=false}
		{if $section == "S" || $section == "B"}
		    {if $section == "S"}
		        {assign var="_to" value="B"}
		    {else}
		        {assign var="_to" value="S"}
		    {/if}
		    {if !$wk_vendor_custom_fields.$_to[$field.matching_id]}
		        {assign var="skip_field" value=true}
		    {/if}
		{/if}
		<div class="ty-control-group ty-profile-field__item ty-{$field.class}">
		    {if $pref_field_name != $field.description || $field.required == "Y"}
		        <label for="{$id_prefix}elm_{$field.field_id}" class="ty-control-group__title cm-profile-field {if $field.required == "Y"}cm-required{/if}{if $field.field_type == "P"} cm-phone{/if}{if $field.field_type == "Z"} cm-zipcode{/if}{if $field.field_type == "E"} cm-email{/if} {if $field.field_type == "Z"}{if $section == "S"}cm-location-shipping{else}cm-location-billing{/if}{/if}">{if $field.description}{$field.description}{else}{$field.field_id|fn_wk_vendor_custom_fields_get_default_lang_description}{/if}</label>
		    {/if}

		    {if $field.field_type == "A"}  {* State selectbox *}
		        {$_country = $settings.General.default_country}
		        {$_state = $value|default:$settings.General.default_state}

		        <select {if $field.autocomplete_type}x-autocompletetype="{$field.autocomplete_type}"{/if} id="{$id_prefix}elm_{$field.field_id}" class="ty-profile-field__select-state cm-state {if $section == "S"}cm-location-shipping{else}cm-location-billing{/if} {if !$skip_field}{$_class}{/if}" name="{$data_name}[{$data_id}]" {if !$skip_field}{$disabled_param nofilter}{/if}>
		            <option value="">- {__("select_state")} -</option>
		            {if $states && $states.$_country}
		                {foreach from=$states.$_country item=state}
		                    <option {if $_state == $state.code}selected="selected"{/if} value="{$state.code}">{$state.state}</option>
		                {/foreach}
		            {/if}
		        </select><input {if $field.autocomplete_type}x-autocompletetype="{$field.autocomplete_type}"{/if} type="text" id="elm_{$field.field_id}_d" name="{$data_name}[{$data_id}]" size="32" maxlength="64" value="{$_state}" disabled="disabled" class="cm-state {if $section == "S"}cm-location-shipping{else}cm-location-billing{/if} ty-input-text hidden {if $_class}disabled{/if}"/>

		    {elseif $field.field_type == "O"}  {* Countries selectbox *}
		        {assign var="_country" value=$value|default:$settings.General.default_country}
		        <select {if $field.autocomplete_type}x-autocompletetype="{$field.autocomplete_type}"{/if} id="{$id_prefix}elm_{$field.field_id}" class="ty-profile-field__select-country cm-country {if $section == "S"}cm-location-shipping{else}cm-location-billing{/if} {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if}" name="{$data_name}[{$data_id}]" {if !$skip_field}{$disabled_param nofilter}{/if}>
		            
		            <option value="">- {__("select_country")} -</option>
		            {foreach from=$countries item="country" key="code"}
		            <option {if $_country == $code}selected="selected"{/if} value="{$code}">{$country}</option>
		            {/foreach}
		          
		        </select>
		    
		    {elseif $field.field_type == "C"}  {* Checkbox *}
		        <input type="hidden" name="{$data_name}[{$data_id}]" value="N" {if !$skip_field}{$disabled_param nofilter}{/if} />
		        <input type="checkbox" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" value="Y" {if $value == "Y"}checked="checked"{/if} class="checkbox {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if}" {if !$skip_field}{$disabled_param nofilter}{/if} />

		    {elseif $field.field_type == "T"}  {* Textarea *}
		        <textarea {if $field.autocomplete_type}x-autocompletetype="{$field.autocomplete_type}"{/if} class="ty-input-textarea-long  {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if}" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" cols="55" rows="5" {if !$skip_field}{$disabled_param nofilter}{/if}>{$value}</textarea>
		    
		    {elseif $field.field_type == "D"}  {* Date *}
		        {if !$skip_field}
		            {include file="common/calendar.tpl" date_id="`$id_prefix`elm_`$field.field_id`" date_name="`$data_name`[`$data_id`]" date_val=$value extra=$disabled_param}
		        {else}
		            {include file="common/calendar.tpl" date_id="`$id_prefix`elm_`$field.field_id`" date_name="`$data_name`[`$data_id`]" date_val=$value}
		        {/if}

		    {elseif $field.field_type == "S"}  {* Selectbox *}
			    {if $field.field_id==3}
	                 <select name="company_data[lang_code]" id="elm_company_language">
	                {foreach from=$languages item="language" key="lang_code"}
	                    <option value="{$lang_code}" {if $lang_code == $company_data.lang_code}selected="selected"{/if}>{$language.name}</option>
	                {/foreach}
	                </select>
	            {else}
			        <select {if $field.autocomplete_type}x-autocompletetype="{$field.autocomplete_type}"{/if} id="{$id_prefix}elm_{$field.field_id}" class="ty-profile-field__select {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if}" name="{$data_name}[{$data_id}]" {if !$skip_field}{$disabled_param nofilter}{/if}>
			            {if $field.required != "Y"}
			            <option value="">--</option>
			            {/if}
			            {foreach from=$field.values key=k item=v}
			            <option {if $value == $k}selected="selected"{/if} value="{$k}">{$v}</option>
			            {/foreach}
			        </select>
			    {/if}
		    {elseif $field.field_type == "R"}  {* Radiogroup *}
		        <div id="{$id_prefix}elm_{$field.field_id}">
		            {foreach from=$field.values key=k item=v name="rfe"}
		            <input class="radio {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if} {$id_prefix}elm_{$field.field_id}" type="radio" id="{$id_prefix}elm_{$field.field_id}_{$k}" name="{$data_name}[{$data_id}]" value="{$k}" {if (!$value && $smarty.foreach.rfe.first) || $value == $k}checked="checked"{/if} {if !$skip_field}{$disabled_param nofilter}{/if} /><span class="radio">{$v}</span>
		            {/foreach}
		        </div>

		    {elseif $field.field_type == "N"}  {* Address type *}
		        <input class="radio {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if} {$id_prefix}elm_{$field.field_id}" type="radio" id="{$id_prefix}elm_{$field.field_id}_residential" name="{$data_name}[{$data_id}]" value="residential" {if !$value || $value == "residential"}checked="checked"{/if} {if !$skip_field}{$disabled_param nofilter}{/if} /><span class="radio">{__("address_residential")}</span>
		        <input class="radio {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if} {$id_prefix}elm_{$field.field_id}" type="radio" id="{$id_prefix}elm_{$field.field_id}_commercial" name="{$data_name}[{$data_id}]" value="commercial" {if $value == "commercial"}checked="checked"{/if} {if !$skip_field}{$disabled_param nofilter}{/if} /><span class="radio">{__("address_commercial")}</span>

		    {else}  {* Simple input *}

		        <input {if $field.autocomplete_type}x-autocompletetype="{$field.autocomplete_type}"{/if} type="text" id="{$id_prefix}elm_{$field.field_id}" name="{$data_name}[{$data_id}]" size="32" value="{$value}" class="ty-input-text {if !$skip_field}{$_class}{else}cm-skip-avail-switch{/if} {if $smarty.foreach.wk_vendor_custom_fields.index == 0} cm-focus{/if}" {if !$skip_field}{$disabled_param nofilter}{/if} />
		    {/if}

		    {assign var="pref_field_name" value=$field.description}
		</div>
		{$flag_check=0}
{/foreach}