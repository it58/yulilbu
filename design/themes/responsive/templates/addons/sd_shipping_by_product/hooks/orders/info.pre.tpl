{if $order_info.products}
    <table class="ty-orders-detail__table ty-table ty-mb-s">
        <thead>
            <tr>
                <th>{__("product")}</th>
                <th>{__("shipping_method")}</th>
                <th>{__("shipping_cost")}</th>
                <th>{__("delivery_time")}</th>
            </tr>
        </thead>
        {foreach from=$order_info.products item="product"}
            <tr>
                <td>{$product.product}</td>
                <td>{if $product.extra.shipping_info.shipping_id}{$product.extra.shipping_info.shipping_id|fn_get_shipping_name}{else}-{/if}</td>
                <td>{include file="common/price.tpl" value=$product.extra.shipping_info.shipping_cost}</td>
                <td>{$product.extra.shipping_info.delivery_time}</td>
            </tr>
        {/foreach}
    </table>
{/if}