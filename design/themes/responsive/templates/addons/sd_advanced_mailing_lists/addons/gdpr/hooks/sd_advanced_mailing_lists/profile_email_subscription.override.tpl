{$show_agreement = false}

{capture name="agreement_contents"}
    {include file="addons/gdpr/componentes/agreement_checkbox.tpl"
        type="advanced_mailing_subscribe"
        meta="cm-gdpr-advanced_mailing-agreement"
        suffix=$tab_id
    }
{/capture}

{if $show_agreement}
    {$smarty.capture.agreement_contents nofilter}

    <script type="text/javascript">
        (function(_, $) {
            $.ceEvent('on', 'ce.gdpr_agreement_accepted', function ($item, context) {
                var checked = $item.prop('checked');
                var $subscription_items = $item.closest('.sd-mailing-lists-page__block').find('input:checkbox').not($item);

                if (checked) {
                    $subscription_items.prop('disabled', false);
                } else {
                    $subscription_items.prop('disabled', true).prop('checked', false);
                }
            });

            $.ceEvent('on', 'ce.commoninit', function(context) {
                $(context).find('.sd-mailing-lists-page__block').find('input:checkbox').not('.cm-gdpr-advanced_mailing-agreement, input:checked').prop('disabled', true);
            });
        }(Tygh, Tygh.$));
    </script>
{/if}
