{if $user_data.mailchimp_lists}
    <div class="sd-mailing-lists-page__block">
        {capture name="mailing_lists"}
            {hook name="sd_advanced_mailing_lists:checkout_email_subscription"}
            {if count($user_data.mailchimp_lists) > 1}
                <p>{__("addons.sd_advanced_mailing_lists.text_signup_for_subscriptions")}</p>
            {/if}
            <div class="sd-mailing-lists-page__wrapper">
                {foreach from=$user_data.mailchimp_lists item=list}
                    <div class="sd-mailing-lists-page__item">
                        <label>
                        <input id="register_subscribe_{$list.list_id}" type="checkbox" name="advanced_mailing_lists[]" value="{$list.list_id}" class="checkbox cm-mailing-lists-subscribe" />
                            {if count($user_data.mailchimp_lists) > 1}
                                {$list.name}
                            {else}
                                {__("addons.sd_advanced_mailing_lists.text_signup_for_subscriptions")}
                            {/if}
                        </label>
                    </div>
                {/foreach}
            </div>
            {/hook}
        {/capture}
        {include file="common/subheader.tpl" title=__("sd_signup_for_subscriptions")}

        {$smarty.capture.mailing_lists nofilter}
    </div>
{/if}
