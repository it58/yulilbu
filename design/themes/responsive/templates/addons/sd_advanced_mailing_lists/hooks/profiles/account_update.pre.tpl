{if $advanced_mailing_lists}
    <div class="sd-mailing-lists-page__block">
        {capture name="mailing_lists"}
            {hook name="sd_advanced_mailing_lists:profile_email_subscription"}
            {if count($advanced_mailing_lists) > 1}
                <p>{__("addons.sd_advanced_mailing_lists.text_signup_for_subscriptions")}</p>
            {/if}
            {/hook}
            <div class="sd-mailing-lists-page__wrapper">
                {foreach $advanced_mailing_lists as $list}
                    <div class="sd-mailing-lists-page__item">
                        <label for="register_subscribe_{$list.list_id}">
                            <input id="register_subscribe_{$list.list_id}" type="checkbox" name="advanced_mailing_lists[]" value="{$list.list_id}" class="checkbox" {if $list.unsubscribe == "Y"}checked="checked"{/if}/>
                            {if count($advanced_mailing_lists) > 1}
                                {$list.name}
                            {else}
                                {__("addons.sd_advanced_mailing_lists.text_signup_for_subscriptions")}
                            {/if}
                        </label>
                    </div>
                {/foreach}
            </div>
        {/capture}
        {include file="common/subheader.tpl" title=__("sd_signup_for_subscriptions")}

        {$smarty.capture.mailing_lists nofilter}
    </div>
{/if}
