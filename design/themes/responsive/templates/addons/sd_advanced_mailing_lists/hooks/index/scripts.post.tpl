{script src="js/addons/sd_advanced_mailing_lists/scripts.js"}

<script type="text/javascript">
    (function(_, $) {
        _.tr({
            incorrect_email: '{__("incorrect_email")|escape:"javascript"}'
        });
    }(Tygh, Tygh.$));
</script>