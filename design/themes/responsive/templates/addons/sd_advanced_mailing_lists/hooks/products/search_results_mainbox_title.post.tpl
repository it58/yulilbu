{capture name="mainbox_title"}
    <span class="ty-mainbox-title__left">{$_title}</span><span class="ty-mainbox-title__right" id="products_search_total_found_{$block.block_id}">{$title_extra nofilter}<!--products_search_total_found_{$block.block_id}--></span><br>
    <div class="sd-subscribe-text-button">
        {include file="addons/sd_advanced_mailing_lists/common/follow_vendor.tpl" company_id=$company_id}
    </div>
{/capture}