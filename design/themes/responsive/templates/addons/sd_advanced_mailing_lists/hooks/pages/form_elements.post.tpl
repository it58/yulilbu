{if !$last}
    {assign var="last" value=$page.form.elements|end}
{/if}
{if $advanced_mailing_lists && $smarty.request.sent != "Y" && $element_id == $last.element_id}
    <div class="sd-mailing-lists-page__block">
        {capture name="mailing_lists"}
            {if count($advanced_mailing_lists) > 1}
                <p>{__("addons.sd_advanced_mailing_lists.text_signup_for_subscriptions_on_page")}</p>
            {/if}
            <div class="sd-mailing-lists-page__wrapper">
                {foreach from=$advanced_mailing_lists item=list}
                    <div class="sd-mailing-lists-page__item">
                        <label for="form_subscribe_{$list.list_id}">
                            <input id="form_subscribe_{$list.list_id}" type="checkbox" name="advanced_mailing_lists[]" value="{$list.list_id}" class="checkbox" />
                            {if count($advanced_mailing_lists) > 1}
                                {$list.name}
                            {else}
                                {__("addons.sd_advanced_mailing_lists.text_signup_for_subscriptions_on_page")}
                            {/if}
                        </label>
                    </div>
                {/foreach}
            </div>
        {/capture}
        {include file="common/subheader.tpl" title=__("sd_signup_for_subscriptions")}

        {$smarty.capture.mailing_lists nofilter}
    </div>
{/if}