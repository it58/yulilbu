<div id="add_description_popup">
    <form  action="{""|fn_url}" method="post" name="add_email_form" id="add_email_form" class="{if $hide_inputs}cm-hide-inputs{/if}">
        <input type="hidden" name="company_id" value="{$company_id}" id="company_id">
        <input type="hidden" name="redirect_url" value="{$c_url}">
        <div class="ty-control-group">
            <label class="ty-control-group__title cm-email cm-required" for="elm_email">{__("email")}:</label>
            <input type="text" id="elm_email" name="email" class="ty-input-text-full" size="50">
        </div>

        {include file="common/image_verification.tpl" option="sd_advanced_mailing_lists"}

        <div class="buttons-container buttons-container-picker">
            {include file="buttons/button.tpl" but_name="dispatch[advanced_mailing_lists.follow_vendor]" but_text=__("subscribe") but_role="submit" but_meta="ty-btn__primary ty-btn__big ty-btn"}
        </div>
    </form>
</div>