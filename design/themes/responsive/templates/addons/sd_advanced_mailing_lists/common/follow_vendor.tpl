{if !empty($company_id)}
    {$mailchimp_lists=sd_ODBlN2UzZTc1MWZmNzIyYTY4Zjg5MzA4($company_id)}
    {if $mailchimp_lists}
        {assign var="c_url" value=$config.current_url|urlencode}
        {assign var="but_text" value=$but_text|default:__("addons.sd_advanced_mailing_lists.subscribe")}
        {assign var="but_meta" value=$but_meta|default:""}

        {if $auth.user_id}
            <a class="cm-post {$but_meta}" href="{"advanced_mailing_lists.follow_vendor?company_id=`$company_id`&redirect_url=`$c_url`"|fn_url}"><span>{$but_text}</span></a>
        {else}
            <a class="cm-dialog-opener cm-dialog-auto-size {$but_meta}" href="{"advanced_mailing_lists.follow_vendor?company_id=`$company_id`&c_url=`$c_url`"|fn_url}" data-ca-target-id="open_id_ajax" data-ca-dialog-title="{$but_text}"><span>{$but_text}</span></a>
        {/if}
    {/if}
{/if}