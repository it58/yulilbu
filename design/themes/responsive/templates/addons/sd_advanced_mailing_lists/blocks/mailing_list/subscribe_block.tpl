{** block-description:subscribe_block_template **}
{if $addons.sd_advanced_mailing_lists}
    {$mailchimp_lists=sd_ZTE3MmI1OTA5YmMyODQ2YWI5ODdlY2Vi($block.block_id)}
    {if $mailchimp_lists}
        {$talign=$block.properties.text_align}
        <div id="subscribe_form_block_{$block.block_id}"
             class="sd-mailing-list sd-subscribe-default
             {if $talign == 'L'}left{/if}">
            <form action="{""|fn_url}" method="post" name="sd_mailchimp_subscribe_form" id="subscribe_form_{$block.block_id}">
            <input type ="hidden" name="redirect_url" value="{$config.current_url}" />
                <div class="sd-subscribe-default__image-wrapper">
                    {include file="addons/sd_advanced_mailing_lists/blocks/mailing_list/components/image.tpl" class='sd-subscribe-default__image'}
                </div>
                <div class="sd-subscribe-default__content-wrapper">
                    {if $block.content.title}<h3 class="title">{$block.content.title nofilter}</h3>{/if}
                    {if $block.content.subtitle}<div class="subtitle">{$block.content.subtitle nofilter}</div>{/if}
                </div>
                <div class="sd-subscribe-default__content-wrapper">
                    <div class="ty-control-group ty-input-append">
                        <label class="cm-required-subscribe-label hidden" for="subscr_email{$block.block_id}"></label>
                        <input type="text" name="mailchimp_subscribe_email" id="subscr_email{$block.block_id}" size="20" placeholder="{$block.content.mail_input|default:__("enter_email")}" class="cm-hint ty-input-text sd-mailchimp"/>
                        {foreach from=$mailchimp_lists item=list}
                            <input type="hidden" name="advanced_mailing_lists[]" value="{$list.list_id}" />
                        {/foreach}
                        <input type="hidden" name="subscribe_block_id" value="{$block.block_id}" />
                        {include file="buttons/button.tpl" but_role="act" but_name="dispatch[advanced_mailing_lists.subscribe]" but_meta="ty-btn-subscribe" but_text=$block.content.subscribe_mail_button|default:__("subscribe") alt=$block.content.subscribe_mail_button|default:__("subscribe")}
                        <p><a class="cm-submit" data-ca-dispatch="dispatch[advanced_mailing_lists.unsubscribe_confirmation]">{$block.content.unsubscribe_mail_button}</a></p>
                    </div>
                </div>
                {if $block.content.text}
                    <div class="sd-subscribe-default__content-wrapper">
                        <div class="text">{$block.content.text nofilter}</div>
                    </div>
                {/if}
            </form>
        </div>
    {/if}
{/if}
