{** block-description:subscribe_block_template_fixed **}

{$mailchimp_lists=sd_ZTE3MmI1OTA5YmMyODQ2YWI5ODdlY2Vi($block.block_id, true)}
{if !empty($mailchimp_lists) && (!$smarty.session.subscribe_appeared.{$block.block_id} || $block.properties.write_to_the_session != 'Y') && !$smarty.session.user_subscribed.{$block.block_id}}
    {$cstyle=$block.properties.content_style}
    {$shmobile=$block.properties.show_on_mobile}
    {$position=$block.properties.position_on_page}
    {if $position == 'C'}
        <div id="fixed_bgr" class="{if !$block.properties.show_after_time}dark-background{if $shmobile == 'N'} hidden-phone{/if}{/if}"></div>
    {/if}
    <div id="subscribe_form_block_{$block.block_id}"
         class="sd-mailing-list sd-subscribe-fixed
         {if $block.properties.show_after_time}hidden{/if}
         {if $cstyle == 'RD'}rounded{/if}
         {if $shmobile == 'N'}hidden-phone{/if}
         {if $position == 'R'}on-right{elseif $position == 'C'}in-center{else}on-left{/if}">
        <form action="{""|fn_url}" method="post" name="sd_mailchimp_subscribe_form" id="subscribe_form_{$block.block_id}">
            <input type ="hidden" name="redirect_url" value="{$config.current_url}" />
            <span class="btn-close" id="close_popup_{$block.block_id}"></span>
            <div class="sd-subscribe-fixed__image-wrapper">
                {include file="addons/sd_advanced_mailing_lists/blocks/mailing_list/components/image.tpl" class='sd-subscribe-fixed__image'}
            </div>
            <div class="sd-subscribe-fixed__content-wrapper">
                {if $block.content.title}<h3 class="title">{$block.content.title nofilter}</h3>{/if}
                {if $block.content.subtitle}<div class="subtitle">{$block.content.subtitle nofilter}</div>{/if}
            </div>
            <div class="sd-subscribe-fixed__content-wrapper">
                <div class="ty-control-group ty-input-append">
                    <label class="cm-required-subscribe-label hidden" for="popup_subscr_email{$block.block_id}"></label>
                    <input type="text" name="mailchimp_subscribe_email" id="popup_subscr_email{$block.block_id}" size="20" placeholder="{$block.content.mail_input|default:__("enter_email")}" class="cm-hint ty-input-text sd-mailchimp"/>
                    {foreach from=$mailchimp_lists item=list}
                        <input type="hidden" name="advanced_mailing_lists[]" value="{$list.list_id}" />
                    {/foreach}
                    <input type="hidden" name="popup_block_id" value="{$block.block_id}" />
                    {include file="buttons/button.tpl" but_role="act" but_name="dispatch[advanced_mailing_lists.subscribe]" but_meta="ty-btn-subscribe" but_text=$block.content.subscribe_mail_button|default:__("subscribe") alt=$block.content.subscribe_mail_button|default:__("subscribe")}
                    <p><a class="" data-ca-dispatch="dispatch[advanced_mailing_lists.unsubscribe_confirmation]">{$block.content.unsubscribe_mail_button}</a></p>
                </div>
            </div>
            {if $block.content.text}
                <div class="sd-subscribe-fixed__content-wrapper">
                    <div class="text">{$block.content.text nofilter}</div>
                </div>
            {/if}
        </form>
    </div>

    {if (!$smarty.session.subscribe_appeared.{$block.block_id} || $block.properties.write_to_the_session != 'Y') && !$smarty.session.user_subscribed.{$block.block_id}}
        <script type="text/javascript">
            (function(_, $) {
                $.ceEvent('one', 'ce.commoninit', function(context) {
                    $(window).load(function(){
                        fn_write_appeare_to_session({$block.block_id});
                    });
                });
            }(Tygh, Tygh.$));
        </script>
    {/if}

    {if $block.properties.show_after_time}
        <script type="text/javascript">
            (function(_, $) {
                $(document).ready(function() {
                    var time = '{$block.properties.show_after_time}' - 0;
                    setTimeout(function(){
                        $("#subscribe_form_block_" + {$block.block_id}).removeClass('hidden');
                        {if $position == 'C'}
                            $("#fixed_bgr").addClass('dark-background');
                            {if $shmobile == 'N'}
                                $("#fixed_bgr").addClass('hidden-phone');
                            {/if}
                        {/if}
                    }, time * 1000)
                });
            }(Tygh, Tygh.$));
        </script>
    {/if}

    {if $block.properties.hide_after_scroll}
        <script type="text/javascript">
            (function(_, $) {
                $(window).scroll(function () {
                    var scrollPos = Math.ceil($(this).scrollTop() + $(this).height()),
                        percent = '{$block.properties.hide_after_scroll}' - 0
                        pageHeigh = $('html').height();

                    if (scrollPos >= Math.ceil((percent * pageHeigh) / 100)) {
                        $("#subscribe_form_block_" + {$block.block_id}).addClass('hidden');
                        {if $position == 'C'}
                            $("#fixed_bgr").removeClass('dark-background');
                        {/if}
                    }
                });
            }(Tygh, Tygh.$));
        </script>
    {/if}
{/if}
