<!-- BEGIN GCR Badge Code -->
<script src="https://apis.google.com/js/platform.js?onload=renderBadge"
  async defer>
</script>

<script>
    (function(_, $) {
        window.renderBadge = function() {
            var ratingBadgeContainer = document.createElement("div");
            document.body.appendChild(ratingBadgeContainer);
            window.gapi.load('ratingbadge', function() {
                window.gapi.ratingbadge.render(
                ratingBadgeContainer, {
                    // REQUIRED
                    "merchant_id": {$addons.sd_google_customer_reviews.google_merchant_id},
                    // OPTIONAL
                    "position": '{$block.properties.position}'
                });           
            });
        }
    }(Tygh, Tygh.$));
</script>
<!-- END GCR Badge Code -->

<!-- BEGIN GCR Language Code -->
<script>
    window.___gcfg = {
        lang: '{$block.lang_code}'
    };
</script>
<!-- END GCR Language Code -->