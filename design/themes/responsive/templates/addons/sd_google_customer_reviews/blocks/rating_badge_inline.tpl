<div class='ty-inline-rating-badge'>
    <!-- BEGIN GCR Badge Code -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>​

    <g:ratingbadge merchant_id={$addons.sd_google_customer_reviews.google_merchant_id}></g:ratingbadge>​
    <!-- END GCR Badge Code -->

    <!-- BEGIN GCR Language Code -->
    <script>
        window.___gcfg = {
            lang: '{$block.lang_code}'
        };
    </script>
    <!-- END GCR Language Code -->
</div>