{if $addons.sd_google_customer_reviews.google_enable_order_confirmation == 'Y' &&  $gcr_showed_count <= $gcr_display_count}
    <!-- BEGIN GCR Opt-in Module Code -->
    <script src="https://apis.google.com/js/platform.js?onload=renderOptIn"
    async defer>
    </script>

    <script>
        (function(_, $) {
            window.renderOptIn = function() {
                window.gapi.load('surveyoptin', function() {
                window.gapi.surveyoptin.render(
                    {
                    // REQUIRED
                    "merchant_id": {$addons.sd_google_customer_reviews.google_merchant_id},
                    "order_id": '{$order_info.order_id}',
                    "email": '{$order_info.email}',
                    "delivery_country": '{$order_info.s_country}',
                    "estimated_delivery_date": '{$order_info|fn_sd_google_customer_reviews_order_ship_date:"est_delivery_date"}',
                    // OPTIONAL
                    "opt_in_style": '{$addons.sd_google_customer_reviews.google_opt_in_style}'
                    });
                });
            }
        }(Tygh, Tygh.$));
    </script>
    <!-- END GCR Opt-in Module Code -->
    
    <!-- BEGIN GCR Language Code -->
    <script>
        window.___gcfg = {
            lang: '{$cart_language}'
        };
    </script>
    <!-- END GCR Language Code -->
{/if}