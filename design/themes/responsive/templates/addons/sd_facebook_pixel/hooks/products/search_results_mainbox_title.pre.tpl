{if $addons.sd_facebook_pixel.search_facebook_pixel == 'Y' && !empty($addons.sd_facebook_pixel.identifier_facebook_pixel)}
    <script type="text/javascript">
        {if $smarty.const.AJAX_REQUEST}
            /*ajax_rnd_{0|mt_rand:10000000}*/
        {/if}
        (function(_, $) {
            fbq('track', 'Search');
        }(Tygh, Tygh.$));
    </script>
{/if}
