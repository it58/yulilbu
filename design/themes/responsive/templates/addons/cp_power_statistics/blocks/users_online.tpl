{if $block.properties.extended_information != 'none'}
  {$quick_view_url = "users_online.get_info?extended_information=`$block.properties.extended_information`&view_extinfo=`$block.properties.view_extended_information`"}
  <a class="cm-dialog-opener cm-dialog-auto-size" data-ca-view-id="user_online_{$block.block_id}" data-ca-target-id="user_online_{$block.block_id}" href="{$quick_view_url|fn_url}" data-ca-dialog-title="{__("users_online")|ucfirst}" rel="nofollow">
{/if}

<span class="ps-users-online-block">
{if $block.properties.users_online_view == 'icon' || $block.properties.users_online_view == 'icon_text'}<img src="{$images_dir}/addons/cp_power_statistics/users_online_24.png" class="ps-users-online" />{/if}{if $block.properties.users_online_view == 'text' || $block.properties.users_online_view == 'icon_text'}&nbsp;{__("users_online")}:{/if}&nbsp;{$users_online}

{if $block.properties.show_countries == 'Y'}
  {assign var="country_users" value=""|fn_ps_get_country_users}
  {foreach from=$country_users item="country"}
  {if $country.country_code}<i class="ps-inline ty-flag ty-flag-{$country.country_code|strtolower}"></i>{$country.users_count}{/if}
  {/foreach}
{/if}

{if $block.properties.show_browsers == 'Y'}
  {assign var="browser_users" value=""|fn_ps_get_browser_users}  
  {foreach from=$browser_users item="browser"}
  <img src="{$images_dir}/addons/cp_power_statistics/browsers/{$browser.browser|default:'unknown'}.png" class="ps-browser">&nbsp;{$browser.users_count}
  {/foreach}
{/if}
</span>

{if $block.properties.extended_information == 'Y'}
  </a>
{/if}