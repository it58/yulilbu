<div class="power-statistics"> 

{if $country_users}
  {if $view_extinfo == 'vertically'}
      <table class="ty-table ps-ty-table">
      {foreach from=$country_users item="country"}
      <tr>      
      <td width="70%"><i class="ty-flag ty-flag-{$country.country_code|strtolower}"></i>{$countries[$country.country_code|strtoupper]|default:__("unknown")}</td>
      <td width="30%" class="ty-center">{$country.users_count}</td>
      </tr>      
      {/foreach}
      </table>
  {else}
      <table class="ty-table ps-ty-table">
      {*<tr><th colspan="{$country_users|count}">{__("location")}</th></tr>*}
      <tr>
      {foreach from=$country_users item="country"}
      <td><i class="ty-flag ty-flag-{$country.country_code|strtolower}"></i>{$countries[$country.country_code|strtoupper]|default:__("unknown")}</td>
      {/foreach}
      </tr>
      <tr>
      {foreach from=$country_users item="country"}
      <td class="ty-center">{$country.users_count}</td>
      {/foreach}
      </tr>
      </table>
  {/if}
{/if}

{if $browser_users}
  {if $view_extinfo == 'vertically'}
    <table class="ty-table ps-ty-table">
    {foreach from=$browser_users item="browser"}
    <tr>    
    <td width="70%"><img src="{$images_dir}/addons/cp_power_statistics/browsers/{$browser.browser|default:'unknown'}.png">&nbsp;{$browser.browser|fn_get_browser_name|default:__("unknown")}</td>
    <td width="30%" class="ty-center">{$browser.users_count}</td>    
    </tr>    
    {/foreach}
    </table> 
  {else}
    <table class="ty-table ps-ty-table">
    <tr>
    {foreach from=$browser_users item="browser"}
    <td><img src="{$images_dir}/addons/cp_power_statistics/browsers/{$browser.browser|default:'unknown'}.png">&nbsp;{$browser.browser|fn_get_browser_name|default:__("unknown")}</td>
    {/foreach}
    </tr>
    <tr>
    {foreach from=$browser_users item="browser"}
    <td class="ty-center">{$browser.users_count}</td>
    {/foreach}
    </tr>
    </table>  
  {/if}
{/if}
</div>