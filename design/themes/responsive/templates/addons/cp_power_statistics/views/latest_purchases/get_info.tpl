<div class="power-statistics"> 

{if $products}
  {assign var="layouts" value=""|fn_get_products_views:false:0}
  {if $category_data.product_columns}
      {assign var="product_columns" value=$category_data.product_columns}
  {else}
      {assign var="product_columns" value=$settings.Appearance.columns_in_products_list}
  {/if}
  
  {if $layouts.$selected_layout.template}
      {include file="`$layouts.$selected_layout.template`" columns=$product_columns id="pagination_content_`$block_id`" no_sorting=true}
  {/if}

{else}
  <p class="ty-no-items cm-pagination-container">{__("text_no_products")}</p>
{/if}

</div>