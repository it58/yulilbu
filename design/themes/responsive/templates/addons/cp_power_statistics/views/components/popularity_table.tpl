{if $product.show_only_total_popularity == "Y"}

  <table class="ty-table ps-initial-width ps-no-to-margin ps-popularity-table">
    <tr>
    <th class="ty-center">{__("ps_popularity")}{include file="common/tooltip.tpl" tooltip=__("ps_popularity_descr")}</th>
    </tr>
    <tr>
    <td class="ty-center">{$ps_popularity.total}</td>
    </tr>
  </table>
  
{else}
  <table class="ty-table ps-initial-width ps-no-to-margin ps-popularity-table">
  {assign var="ps_td" value="th"}
    
  {if $product.show_popularity_header == "Y"}
    <tr>
      <th colspan="{$ps_count_rows_popularity}">{__("ps_popularity")}</th>
    </tr>
    {assign var="ps_td" value="td"}
  {/if}

  <tr>
  {if $product.show_viewed == "Y"}
    <{$ps_td} class="ty-center">
      {__("ps_viewed")}
    </{$ps_td}>
  {/if}
  {if $product.show_added == "Y"}
    <{$ps_td} class="ty-center">
      {__("ps_added")|ucfirst}
    </{$ps_td}>
  {/if}
  {if $product.show_deleted == "Y"}
    <{$ps_td} class="ty-center">
      {__("ps_deleted")}
    </{$ps_td}>
  {/if}
  {if $product.show_bought == "Y"}
    <{$ps_td} class="ty-center">
      {__("ps_bought")}
    </{$ps_td}>
  {/if}
  {if $product.show_total == "Y"}
    <{$ps_td} class="ty-center">
      {__("ps_popularity")}
    </{$ps_td}>
  {/if}
  </tr>
  <tr>
  {if $product.show_viewed == "Y"}
    <td class="ty-center">
      {$ps_popularity.viewed}
    </td>
  {/if}
  {if $product.show_added == "Y"}
    <td class="ty-center">
      {$ps_popularity.added}
    </td>
  {/if}
  {if $product.show_deleted == "Y"}
    <td class="ty-center">
      {$ps_popularity.deleted}
    </td>
  {/if}
  {if $product.show_bought == "Y"}
    <td class="ty-center">
      {$ps_popularity.bought}
    </td>
  {/if}
  {if $product.show_total == "Y"}
    <td class="ty-center">
      {$ps_popularity.total}
    </td>
  {/if}
  </tr>
  </table>
{/if}