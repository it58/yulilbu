{if $product.show_only_total_popularity == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_popularity")}{include file="common/tooltip.tpl" tooltip=__("ps_popularity_descr")}:</label>
      <label class="ty-product-options__box option-items"><div class="cm-field-container">{$ps_popularity.total}</div></label>
    </div>    
{else}
  {if $product.show_popularity_header == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_popularity")}</label>
      <label class="ty-product-options__box option-items">&nbsp;</label>      
    </div>
  {/if}

  {if $product.show_viewed == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_viewed")}:</label>
      <label class="ty-product-options__box option-items">{$ps_popularity.viewed}</label>
    </div>
  {/if}

  {if $product.show_added == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_added")|ucfirst}:</label>
      <label class="ty-product-options__box option-items">{$ps_popularity.added}</label>
    </div>
  {/if}

  {if $product.show_deleted == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_deleted")}:</label>
      <label class="ty-product-options__box option-items">{$ps_popularity.deleted}</label>
    </div>
  {/if}

  {if $product.show_bought == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_bought")}:</label>
      <label class="ty-product-options__box option-items">{$ps_popularity.bought}</label>
    </div>
  {/if}

  {if $product.show_total == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_total")}{include file="common/tooltip.tpl" tooltip=__("ps_popularity_descr")}:</label>
      <label class="ty-product-options__box option-items">{$ps_popularity.total}</label>
    </div>
  {/if}
{/if}