{include file="common/letter_header.tpl"}
<h4>{__("digest_subj", ["[period]" => $period])}</h4>
<div style="margin-bottom: 15px;">{if $follower.b_firstname} {__("dear")} {$follower.b_firstname}! {else} {__("hello")}! {/if}</div>

{foreach from=$event_data item="event" key="event_key"}
    <div style="clear: both; border-bottom: 1px solid #edeff1; padding: 15px 0;">
        <div style="display: inline-block; margin:3px 8px 6px 0px; color: #777777; font-weight: normal; font-size: 12px;">{$event.published_on}</div>
        <div style="display: inline-block; margin:3px 0 8px 0; padding-left: 15px; border-left: 1px solid #adadad; color: #777777; ">{__("by")} {$event.company}</div>
        <div style="margin-left: -15px; margin-bottom: 5px;">{if $event.image}<span style="float: left; margin: 0px 15px 15px 0px;"> {$event.image nofilter}</span>{/if}{__($event.lang_var, ["[vendor_name]" => $event.company, "[url]" => $event.url])}</div>
    </div>
{/foreach}

<br />
{include file="common/letter_footer.tpl"}