{include file="common/letter_header.tpl"}

{__("dear")} {$data.company},<br /><br />

{__("new_subscriber_text", ["[customer_email]" => $data.customer_email, "[url]" => $url|puny_decode])}
