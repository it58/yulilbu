{include file="common/letter_header.tpl"}

{__("dear")} {__("follower")|sd_MDVkZjNiMWVjMGJkM2I2YjI3MzBhZWI4},<br /><br />

{__("event_company_we_are_writting")} {if $image}<span style="float: left; margin: 0px 15px 15px 0px;"> {$image nofilter}</span>{/if}{__($lang_var, ["[vendor_name]" => $company_name, "[url]" => $url])|sd_MDVkZjNiMWVjMGJkM2I2YjI3MzBhZWI4}
<br />
{include file="common/letter_footer.tpl"}
