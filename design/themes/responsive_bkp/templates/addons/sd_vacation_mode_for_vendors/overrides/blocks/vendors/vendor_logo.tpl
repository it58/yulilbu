{** block-description:block_vendor_logo **}

<div class="logo-container">
    <img src="{$vendor_info.logos.theme.image.image_path}" width="{$vendor_info.logos.theme.image.image_x}" height="{$vendor_info.logos.theme.image.image_y}" alt="{$vendor_info.logos.theme.image.alt}" class="logo">
</div>

{if $vendor_info.vendor_in_vacation == 'Y' || $vendor_info.vendor_in_vacation_to_day == 'Y'}
    <label class="sd-vmfv-vacation-weekend">{__('sd_vmfv_vendor_in_vacation_weekend')}</label>
{/if}

{if $vendor_info.vendor_is_not_working_hours == 'Y' && $vendor_info.vendor_in_vacation != 'Y' && $vendor_info.vendor_in_vacation_to_day != 'Y'}
    <label class="sd-vmfv-vacation-weekend">{__('vendor_is_not_working',['[working_hours]' => $vendor_info.working_hours_from|cat:' - '|cat:$vendor_info.working_hours_to])} {__('sd_vmfv_date_timezone'|cat:$vendor_info.timezone_vacation)}</label>
{/if}