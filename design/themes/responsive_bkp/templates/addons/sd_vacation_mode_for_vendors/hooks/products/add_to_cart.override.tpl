{if $product.vendor_in_vacation == 'Y'}
    <div class="sd_vmfv__vendor_in_vacation_message">
        {__('sd_vmfv_vendor_in_vacation_message')}
    </div>
{/if}
{if $product.is_not_working_hours == 'Y' && $product.vendor_in_vacation != 'Y'}
    <div class="sd_vmfv__vendor_in_vacation_message">	
        {__('sd_vmfv_vendor_is_not_working',['[working_hours]' => $product.working_hours_from|cat:' - '|cat:$product.working_hours_to])} {__('sd_vmfv_date_timezone'|cat:$product.timezone_vacation)}
    </div>
{/if}