<!-- This pattern was overridden by sd_shipping_by_product add-on -->
{hook name="checkout:shipping_rates"}
<div id="shipping_rates_list">
    {$show_total = true}
    {foreach from=$product_groups key="group_key" item=group name="spg"}
        {* Products list *}
        <ul class="ty-shipping-options__products">
            {foreach from=$group.products item="product" key="item_id"}
                {if !(($product.is_edp == 'Y' && $product.edp_shipping != 'Y') || $product.free_shipping == 'Y')}
                    <li class="ty-shipping-options__vendor-name">
                        {if $product.product}
                            {$product.product nofilter}
                        {else}
                            {$product.product_id|fn_get_product_name}
                        {/if}
                        {* Shippings list *}
                        {$shipping_id = $cart.chosen_shipping_by_product.$group_key.$item_id}
                        {if $shipping_id && $group.shippings && !$group.all_edp_free_shipping && !$group.shipping_no_required}
                            {$shipping = $group.shippings.$shipping_id}
                            {if $shipping}
                                {if $shipping.product_delivery_time_text || $shipping.service_delivery_time}
                                    {assign var="delivery_time" value="(`$shipping.product_delivery_time_text|default:$shipping.service_delivery_time`)"}
                                {else}
                                    {assign var="delivery_time" value=""}
                                {/if}

                                {if $shipping.rate}
                                    {capture assign="rate"}{include file="common/price.tpl" value=$shipping.rate}{/capture}
                                    {if $shipping.inc_tax}
                                        {assign var="rate" value="`$rate` ("}
                                        {if $shipping.taxed_price && $shipping.taxed_price != $shipping.rate}
                                            {capture assign="tax"}{include file="common/price.tpl" value=$shipping.taxed_price class="ty-nowrap"}{/capture}
                                            {assign var="rate" value="`$rate``$tax` "}
                                        {/if}
                                        {assign var="inc_tax_lang" value=__('inc_tax')}
                                        {assign var="rate" value="`$rate``$inc_tax_lang`)"}
                                    {/if}
                                {elseif fn_is_lang_var_exists("free_shipping")}
                                    {assign var="rate" value=__("free_shipping") }
                                {else}
                                    {assign var="rate" value="" }
                                {/if}

                                {hook name="checkout:shipping_method"}
                                    <div class="ty-shipping-options__method">
                                        <div class="ty-shipping-options__group">
                                            <label for="sh_{$group_key}_{$shipping.shipping_id}" class="ty-valign ty-shipping-options__title">
                                                <bdi>
                                                    {if $shipping.image}
                                                        <div class="ty-shipping-options__image">
                                                            {include file="common/image.tpl" obj_id=$shipping_id images=$shipping.image class="ty-shipping-options__image"}
                                                        </div>
                                                    {/if}

                                                    {$shipping.shipping} {$delivery_time}
                                                    {if $rate} {$rate nofilter}{/if}
                                               </bdi>
                                            </label>
                                        </div>
                                    </div>
                                    {if $shipping.description}
                                        <div class="ty-checkout__shipping-tips">
                                            <p>{$shipping.description nofilter}</p>
                                        </div>
                                    {/if}
                                {/hook}
                            {else}
                                <p class="ty-error-text">
                                    {__("text_no_shipping_methods")}
                                    {$show_total = false}
                                </p>
                            {/if}
                        {else}
                            {if $group.all_free_shipping}
                                 <p>{__("free_shipping")}</p>
                            {elseif $group.all_edp_free_shipping || $group.shipping_no_required }
                                <p>{__("no_shipping_required")}</p>
                                {$show_total = false}
                            {else}
                                <p class="ty-error-text">
                                    {__("text_no_shipping_methods")}
                                    {$show_total = false}
                                </p>
                            {/if}
                        {/if}
                    </li>
                {/if}
            {/foreach}
        </ul>
    {foreachelse}
        <p>
            {if !$cart.shipping_required}
                {__("no_shipping_required")}
            {elseif $cart.free_shipping}
                {__("free_shipping")}
            {/if}
        </p>
        {$show_total = false}
    {/foreach}
    {if $show_total}
        <p class="ty-shipping-options__total">{__("total")}:&nbsp;{include file="common/price.tpl" value=$cart.display_shipping_cost class="ty-price"}</p>
    {/if}
<!--shipping_rates_list--></div>
{/hook}