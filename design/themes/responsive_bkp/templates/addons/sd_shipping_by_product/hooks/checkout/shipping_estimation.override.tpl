<!-- This pattern was overridden by sd_shipping_by_product add-on -->
{hook name="checkout:shipping_estimation"}

{foreach from=$product_groups key=group_key item=group name="s"}
    <ul>
        {foreach from=$group.products item="product" key="item_id"}
            <li>
                <strong>
                {if $product.product}
                    {$product.product nofilter}
                {else}
                    {$product.product_id|fn_get_product_name}
                {/if}
                </strong>
                {$shipping_id = $cart.chosen_shipping_by_product.$group_key.$item_id}
                {if $shipping_id && $group.shippings && !$group.all_edp_free_shipping && !$group.shipping_no_required}
                    {$shipping = $group.shippings.$shipping_id}
                    {if $shipping}
                        {if $shipping.product_delivery_time_text || $shipping.service_delivery_time}
                            {assign var="delivery_time" value="(`$shipping.product_delivery_time_text|default:$shipping.service_delivery_time`)"}
                        {else}
                            {assign var="delivery_time" value=""}
                        {/if}

                        {hook name="checkout:shipping_estimation_method"}
                        {if $shipping.rate}
                            {capture assign="rate"}{include file="common/price.tpl" value=$shipping.rate}{/capture}
                            {if $shipping.inc_tax}
                                {assign var="rate" value="`$rate` ("}
                                {if $shipping.taxed_price && $shipping.taxed_price != $shipping.rate}
                                    {capture assign="tax"}{include file="common/price.tpl" value=$shipping.taxed_price class="ty-nowrap"}{/capture}
                                    {assign var="rate" value="`$rate``$tax` "}
                                {/if}
                                {assign var="inc_tax_lang" value=__('inc_tax')}
                                {assign var="rate" value="`$rate``$inc_tax_lang`)"}
                            {/if}
                        {elseif fn_is_lang_var_exists("free_shipping")}
                            {assign var="rate" value=__("free_shipping") }
                        {else}
                            {assign var="rate" value="" }
                        {/if}
                        <p>
                            <label for="sh_{$group_key}_{$shipping.shipping_id}{$id_suffix}"
                                class="ty-valign">
                                {$shipping.shipping} {$delivery_time}
                                {if $rate} {$rate nofilter}{/if}
                            </label>
                        </p>
                        {/hook}
                    {else}
                        <p class="ty-error-text">
                            {__("text_no_shipping_methods")}
                        </p>
                    {/if}
                {else}
                    {if $group.all_free_shipping}
                         <p>{__("free_shipping")}</p>
                    {elseif $group.all_edp_free_shipping || $group.shipping_no_required }
                        <p>{__("no_shipping_required")}</p>
                    {else}
                        <p class="ty-error-text">
                            {__("text_no_shipping_methods")}
                        </p>
                    {/if}
                {/if}
            </li>
        {/foreach}
    </ul>
{/foreach}

<div id="shipping_estimation_total{$id_suffix}">
    <p><strong>{__("total")}:</strong>&nbsp;{include file="common/price.tpl" value=$cart.display_shipping_cost class="ty-price"}</p>
<!--shipping_estimation_total{$id_suffix}--></div>

{/hook}