{$quick_view_url = "latest_purchases.get_info?extended_information=`$block.properties.extended_information`&view_extinfo=`$block.properties.view_extended_information`&block_id=`$block.block_id`&last_days=`$block.properties.last_days`&products_limit=`$block.properties.products_limit`"}
<a class="cm-dialog-opener cm-dialog-auto-size" data-ca-view-id="latest_purchases_{$block.block_id}" data-ca-target-id="latest_purchases_{$block.block_id}" href="{$quick_view_url|fn_url}" data-ca-dialog-title="{__("latest_purchases")|ucfirst}" rel="nofollow">
<span class="ps-lp-online-block">
<img src="{$images_dir}/addons/cp_power_statistics/latest_purchases_24_24.png" class="ps-users-online ps-img-width" />&nbsp;{__("ps_latest_purchases")}
</span>
</a>