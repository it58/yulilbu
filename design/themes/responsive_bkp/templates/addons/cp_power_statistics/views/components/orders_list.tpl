{assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}

{if $product.show_only_total_orders == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("ps_orders")}:</label>
      <label class="ty-product-options__box option-items"><div class="cm-field-container">{$total_orders}</div></label>
    </div>
{else}

  {if $product.show_orders_header == "Y"}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{__("orders")}</label>
      <label class="ty-product-options__box option-items">&nbsp;</label>      
    </div> 
  {/if}
  
  {foreach from=$order_status_descr key="order_key" item="order"}
    {if $order_key|in_array:$product.show_orders_items}
    <div>
      <label class="ty-control-group__label ty-product-options__item-label">{$order}</label>
      <label class="ty-product-options__box option-items">{$ps_orders[$order_key]['count']|default:0}</label>
    </div>
    {/if}
  {/foreach}

{/if}