<br/>
{assign var="order_status_descr" value=$smarty.const.STATUSES_ORDER|fn_get_simple_statuses:$get_additional_statuses:true}

{if $product.show_only_total_orders == "Y"}
  <table class="ty-table ps-initial-width ps-no-to-margin ps-popularity-table">
    <tr><th class="ty-center">{__("ps_orders")}</th></tr>
    <tr><td class="ty-center">{$total_orders}</td></tr>    
  </table>
{else}
  {assign var="ps_td" value="th"}
  <table class="ty-table ps-initial-width ps-no-to-margin ps-popularity-table">
  {if $product.show_orders_header == "Y"}
    <tr><th colspan="{$order_status_descr|count}">{__("ps_orders")}</th></tr>
    {assign var="ps_td" value="td"}  
  {/if}
  <tr>
  {foreach from=$order_status_descr key="order_key" item="order"}
  {if $order_key|in_array:$product.show_orders_items}
  <{$ps_td} class="ty-center">
    {$order}
  </{$ps_td}>
  {/if}
  {/foreach}
  </tr>
  <tr>
  {foreach from=$order_status_descr key="order_key" item="order"}
  {if $order_key|in_array:$product.show_orders_items}
  <td class="ty-center">
    {$ps_orders[$order_key]['count']|default:0}
  </td>
  {/if}
  {/foreach}
  </tr>
  </table>
{/if}