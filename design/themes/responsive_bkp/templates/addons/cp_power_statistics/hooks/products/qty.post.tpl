{if $product.show_popularity == "B"}
  <br/>
  {if $product.popularity_view == "T"}
    {include file="addons/cp_power_statistics/views/components/popularity_table.tpl"}
  {elseif $product.popularity_view == "L"}
    {include file="addons/cp_power_statistics/views/components/popularity_list.tpl"}
  {/if}
{/if}

{if $product.show_orders == "B"}
  {if $product.orders_view == "T"}
    {include file="addons/cp_power_statistics/views/components/orders_table.tpl"}
  {elseif $product.orders_view == "L"}
    {include file="addons/cp_power_statistics/views/components/orders_list.tpl"}
  {/if}
{/if}