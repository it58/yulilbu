{** block-description:badge_carousel **}
{if $runtime.controller == 'companies' && $runtime.mode == 'products'}
    {assign var="badges" value=$runtime.vendor_id|fn_banners_badges_get_seller_badges}
       {if $badges}
        <div style="text-align: center">
            <p style="color: red">{__("seller_badges")}</p>
            <div id="badges_slider_{$block.snapping_id}" class="owl-carousel"> 
            {foreach from=$badges item="badge" key="key"}
                <div style="text-align: center;">
                    {if $badge.main_pair.image_id}
                        {if $badge.url != ""}<a class="banner__link" href="{$badge.url|fn_url}">{/if}
                            {include file="common/image.tpl" images=$badge.main_pair class="ty-banner__image" }
                            <p>{$badge.name nofilter}</p>
                        {if $badge.url != ""}</a>{/if}
                    {else}
                        <div class="ty-wysiwyg-content">
                            {$badge.name nofilter}
                        </div>
                    {/if}
                </div>
            {/foreach}
            </div>
        </div>
       {/if}
{/if}

<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var slider = context.find('#badges_slider_{$block.snapping_id}');
        if (slider.length) {
            slider.owlCarousel({
                direction: '{$language_direction}',
                items: 1,
                singleItem : true,
                slideSpeed: {$block.properties.speed|default:400},
                autoPlay: '{$block.properties.delay * 1000|default:false}',
                stopOnHover: true,
                {if $block.properties.navigation == "N"}
                    pagination: false
                {/if}
                {if $block.properties.navigation == "D"}
                    pagination: true
                {/if}
                {if $block.properties.navigation == "P"}
                    pagination: true,
                    paginationNumbers: true
                {/if}
                {if $block.properties.navigation == "A"}
                    pagination: false,
                    navigation: true,
                    navigationText: ['{__("prev_page")}', '{__("next")}']
                {/if}
            });
        }
    });
}(Tygh, Tygh.$));
</script>
