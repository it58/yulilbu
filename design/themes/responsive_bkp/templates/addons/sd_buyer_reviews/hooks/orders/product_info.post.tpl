{if $product.extra.discussion.type && $product.extra.discussion.type != 'D' && !$product.extra.discussion.reviews_amount}
    {include file="addons/discussion/views/discussion/components/new_post.tpl" new_post_title=__("write_review") obj_id=$product.product_id discussion=$product.extra.discussion post_redirect_url=$config.current_url}
    <div class="ty-discussion__rating-wrapper ty-mt-s" id="average_rating_product_{$product.product_id}">
        {$rating = "rating_`$product.product_id`"}{$smarty.capture.$rating nofilter}
        <a class="ty-discussion__review-write cm-dialog-opener cm-dialog-auto-size" data-ca-target-id="new_post_dialog_{$product.product_id}" rel="nofollow">{__("write_review")}</a>
    <!--average_rating_product_{$product.product_id}--></div>
{/if}