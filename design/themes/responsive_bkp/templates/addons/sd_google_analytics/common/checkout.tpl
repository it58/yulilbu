{if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $cart}
    {assign var="vendor_tracking_code" value=$cart|sd_OGNhNDA5OTUzOWI3YmU0YjAyZmIzN2Vm}
{else}
    {assign var="vendor_tracking_code" value=""}
{/if}

<script type="text/javascript" class="cm-ajax-force">
(function(_, $) {
    $(document).ready(function() {
        if (typeof(ga) != 'undefined') {
            {if $cart.user_data.user_id}
                {if $smarty.const.TIME == $cart.user_data.last_login}
                    {if $cart.user_data.last_login == $cart.user_data.timestamp}
                        var option_name = '{"register"|sd_OTQyMzExN2I5NzIwYzQwNjQ3MTEzZjM5|escape:javascript nofilter}';

                        ga('ec:setAction','checkout', {
                            step: 1,
                            option: option_name
                        });

                        {if $vendor_tracking_code}
                            var tracking_code = '{$vendor_tracking_code|json_encode|escape:javascript nofilter}';

                            fn_sd_ga_vendor_tracker_action(tracking_code, option_name, 1);
                        {/if}
                    {else}
                        var option_name = '{"login"|sd_OTQyMzExN2I5NzIwYzQwNjQ3MTEzZjM5|escape:javascript nofilter}';

                        ga('ec:setAction','checkout', {
                            step: 1,
                            option: option_name
                        });

                        {if $vendor_tracking_code}
                            var tracking_code = '{$vendor_tracking_code|json_encode|escape:javascript nofilter}';

                            fn_sd_ga_vendor_tracker_action(tracking_code, option_name, 1);
                        {/if}
                    {/if}
                {/if}
            {elseif $smarty.request.edit_step == 'step_two' && $cart.guest_checkout}
                var option_name = '{"checkout_as_guest"|sd_OTQyMzExN2I5NzIwYzQwNjQ3MTEzZjM5|escape:javascript nofilter}';

                ga('ec:setAction','checkout', {
                    step: 1,
                    option: option_name
                });

                {if $vendor_tracking_code}
                    var tracking_code = '{$vendor_tracking_code|json_encode|escape:javascript nofilter}';

                    fn_sd_ga_vendor_tracker_action(tracking_code, option_name, 1);
                {/if}
            {/if}
            {if $smarty.request.edit_step == 'step_four' && ($settings.Checkout.display_shipping_step || fn_allowed_for('MULTIVENDOR'))}
                {assign var="shipping_name" value=$cart|sd_ZmI2NDU3Y2Y2YTVlNTM0NDBhOGUyZDQ3}
                {if $shipping_name}
                    var option_name = '{$shipping_name|escape:javascript nofilter}';

                    ga('ec:setAction','checkout', {
                        step: 3,
                        option: option_name
                    });

                    {if $vendor_tracking_code}
                        var tracking_code = '{$vendor_tracking_code|json_encode|escape:javascript nofilter}';

                        fn_sd_ga_vendor_tracker_action(tracking_code, option_name, 3);
                    {/if}
                {/if}
            {/if}

            ga('ec:setAction','checkout', {
                step: {if $step}{$step}{else}1{/if}
            });
            ga('send', 'event', 'checkout', 'step: {$step}');
            
            {if $vendor_tracking_code}
                {foreach from=$vendor_tracking_code item=tracking_code}
                    ga('{$tracking_code.tracker}.ec:setAction','checkout', {
                        step: {if $step}{$step}{else}1{/if}
                    });
                    ga('{$tracking_code.tracker}.send', 'event', 'checkout', 'step: {$step}');
                {/foreach}
            {/if}
        }
    });

    function fn_sd_ga_vendor_tracker_action(vendor_tracking_code, option_name, current_step) 
    {
        if (vendor_tracking_code && option_name && current_step) {
            var vendor_tracking_code = JSON.parse(vendor_tracking_code);

            $.each(vendor_tracking_code, function(key, vendor_tracker) {
                ga(vendor_tracker['tracker'] + '.ec:setAction','checkout', {
                    step: current_step,
                    option: option_name
                });
            });
        }
    }

}(Tygh, Tygh.$));
</script>