{if $auth.user_id}
    {assign var="coverage_user_id" value=""|sd_ZjhiOTcwYjc0OTc4YTY4N2E4YmEzMGEz}
{/if}

{if $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $runtime.vendor_id != 0}
    {assign var="vendor_tracking_code" value=$runtime.vendor_id|sd_OTljNmFkOTViOTRhMWEzMDE0YmYzMDI1:$product.product_id}
{elseif $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $cart.products}

    {* tracking vendor ga code if isset the $cart variable (dispatch=checkout.cart, dispatch=checkout.checkout) *}

    {assign var="vendor_tracking_code" value=$cart|sd_OGNhNDA5OTUzOWI3YmU0YjAyZmIzN2Vm}
{elseif $addons.sd_google_analytics.ga_vendors_tracking == 'Y' && "MULTIVENDOR"|fn_allowed_for && $orders_info && !$cart.products}

    {* tracking vendor ga code if isset the $orders_info variable (dispatch=checkout.complete) *}

    {assign var="vendor_tracking_code" value=$orders_info|sd_ZjljYzBlMjY3Zjk4MjczOGY1MDIwMDJh}
{/if}
{if $addons.sd_google_analytics.enable_cross_domain_tracking == 'Y'}
    {assign var=domains value=", "|explode:$addons.sd_google_analytics.domain_id}
{/if}

<script type="text/javascript" data-no-defer>
(function(i,s,o,g,r,a,m){
    i['GoogleAnalyticsObject']=r;
    i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)
    },i[r].l=1*new Date();
    a=s.createElement(o), m=s.getElementsByTagName(o)[0];
    a.async=1;
    a.src=g;
    m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '{$addons.sd_google_analytics.tracking_code}', 'auto');

ga('set', JSON.parse('{$sd_ga_params|escape:javascript nofilter}'));

ga('require', 'ec');
{if $coverage_user_id.ga_user_id_law && $coverage_user_id.validator_data}
    ga('set', 'userId', '{$coverage_user_id.validator_data}');
{/if}
ga('send', 'pageview', '{$config.current_url|fn_url:'C':'rel'|escape:javascript nofilter}');
{if $vendor_tracking_code.ga_code}
    ga('create', '{$vendor_tracking_code.ga_code}', 'auto', '{$vendor_tracking_code.tracker}');
    ga('{$vendor_tracking_code.tracker}.require', 'ec');
    ga('{$vendor_tracking_code.tracker}.send', 'pageview', '{$config.current_url|fn_url:'C':'rel'|escape:javascript nofilter}');
{elseif $vendor_tracking_code}
    {foreach from=$vendor_tracking_code item=tracking_code}
        {if $tracking_code.ga_code}
            ga('create', '{$tracking_code.ga_code}', 'auto', '{$tracking_code.tracker}');
            ga('{$tracking_code.tracker}.require', 'ec');
            ga('{$tracking_code.tracker}.send', 'pageview', '{$config.current_url|fn_url:'C':'rel'|escape:javascript nofilter}');
        {/if}
    {/foreach}
{/if}
{if $addons.sd_google_analytics.enable_cross_domain_tracking == 'Y'}
    ga('require', 'linker');
    ga('linker:autoLink', ['{$config.current_host}', {foreach from=$domains item=domain}'{$domain}',{/foreach}]);
{/if}
</script>
