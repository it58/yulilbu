{include file="common/subheader.tpl" class="ty-companies_news-header" title=__("vendors_news")}

{if $vendor_news}
    <ul>
        {foreach from=$vendor_news item="one"}
            <div class="ty-news__item{if $one.main_pair} clearfix{/if}">
                <div class="ty-news__date">{$one.published_on|date_format:"`$settings.Appearance.date_format`"}</div>
                <div class="ty-news__author">{__("by")} {$one.company}</div>
                <div class="ty-news__description">
                    {if $one.main_pair && $one.product_id}
                        <div class="ty-product-list__image">
                            <a href="{"products.view?product_id=`$one.product_id`"|fn_url}">
                                {include file="common/image.tpl" 
                                    image_width=$settings.Thumbnails.product_lists_thumbnail_width 
                                    image_height=$settings.Thumbnails.product_lists_thumbnail_height 
                                    obj_id="$one.product_id"
                                    images=$one.main_pair}
                            </a>
                        </div>
                    {/if}
                    <span>{__($one.lang_var, ["[vendor_name]" => $one.company, "[url]" => $one.url])}</span>
                </div>
            </div>
        {/foreach}
    </ul>

{hook name="follower:vendor_news"}{/hook}

{else}
    <p class="ty-no-items">{__("no_items")}</p>
{/if}
