<div>
    {assign var="is_subscribed" value=$auth.user_id|sd_YWIxNWEwY2M4YzQzNjJhYmU0Y2U5Y2E2:$company_data.company_id}
    {$c_url = $redirect_url|default:$config.current_url|escape:url}
    {if $auth.user_id}
        {if $is_subscribed}
            <a class="cm-post ty-follow-vendor" href="{"follower.delete?company_id=`$company_data.company_id`&redirect_url=`$c_url`"|fn_url}" data-ca-target-id="account_info*">{__("remove_from_follow_list")}</a>
        {else}
            <a class="cm-post ty-follow-vendor" href="{"follower.add?company_id=`$company_data.company_id`&redirect_url=`$c_url`"|fn_url}" data-ca-target-id="account_info*">{__("add_to_follow_list")}</a>
        {/if}
    {/if}
</div>