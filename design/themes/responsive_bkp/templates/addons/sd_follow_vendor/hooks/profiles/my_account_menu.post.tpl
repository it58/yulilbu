{if $auth.user_id}
    <li class="ty-account-info__item ty-dropdown-box__item ty-account-info__item--follow-vendor"><a class="ty-account-info__a" href="{"follower.view"|fn_url}" rel="nofollow">{__("following")}{if $num_of_following > 0} ({$num_of_following}){/if}</a></li>
{/if}