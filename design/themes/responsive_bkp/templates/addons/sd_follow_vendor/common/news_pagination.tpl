{assign var="id" value=$id|default:"pagination_news"}

{assign var="news_pagination" value=$search_news|sd_MzhkNDkzYTgyNjQ3Mjg3ZmFhMjlkZTYz}

{if $smarty.capture.pagination_open != "Y"}
    <div class="ty-pagination-container cm-pagination-container_news" id="{$id}">
    {if $save_current_page}
        <input type="hidden" name="news_page" value="{$search_news.news_page|default:$smarty.request.news_page}" />
    {/if}
    {if $save_current_url}
        <input type="hidden" name="redirect_url" value="{$config.current_url}" />
    {/if}
{/if}

{if $news_pagination.total_pages > 1}
    {if $settings.Appearance.top_pagination == "Y" && $smarty.capture.pagination_open != "Y" || $smarty.capture.pagination_open == "Y"}
    {assign var="c_url" value=$config.current_url|fn_query_remove:"news_page"}

    {if !$config.tweaks.disable_dhtml || $force_ajax}
        {assign var="ajax_class" value="cm-ajax"}
    {/if}

    {if $smarty.capture.pagination_open == "Y"}
    <div class="ty-pagination__bottom">
    {/if}
    <div class="ty-pagination">
        {if $news_pagination.prev_range}
            <a data-ca-scroll=".cm-pagination-container_news" href="{"`$c_url`&news_page=`$news_pagination.prev_range``$extra_url`"|fn_url}" data-ca-page="{$news_pagination.prev_range}" class="cm-history hidden-phone ty-pagination__item ty-pagination__range {$ajax_class}" data-ca-target-id="{$id}">{$news_pagination.prev_range_from} - {$news_pagination.prev_range_to}</a>
        {/if}
        <a data-ca-scroll=".cm-pagination-container_news" class="ty-pagination__item ty-pagination__btn {if $news_pagination.prev_page}ty-pagination__prev cm-history {$ajax_class}{/if}" {if $news_pagination.prev_page}href="{"`$c_url`&news_page=`$news_pagination.prev_page`"|fn_url}" data-ca-page="{$news_pagination.prev_page}" data-ca-target-id="{$id}"{/if}><i class="ty-pagination__text-arrow"></i>&nbsp;<span class="ty-pagination__text">{__("prev_page")}</span></a>

        <div class="ty-pagination__items">
            {foreach from=$news_pagination.navi_pages item="pg"}
                {if $pg != $news_pagination.current_page}
                    <a data-ca-scroll=".cm-pagination-container_news" href="{"`$c_url`&news_page=`$pg``$extra_url`"|fn_url}" data-ca-page="{$pg}" class="cm-history ty-pagination__item {$ajax_class}" data-ca-target-id="{$id}">{$pg}</a>
                {else}
                    <span class="ty-pagination__selected">{$pg}</span>
                {/if}
            {/foreach}
        </div>

        <a data-ca-scroll=".cm-pagination-container_news" class="ty-pagination__item ty-pagination__btn {if $news_pagination.next_page}ty-pagination__next cm-history {$ajax_class}{/if} ty-pagination__right-arrow" {if $news_pagination.next_page}href="{"`$c_url`&news_page=`$news_pagination.next_page``$extra_url`"|fn_url}" data-ca-page="{$news_pagination.next_page}" data-ca-target-id="{$id}"{/if}><span class="ty-pagination__text">{__("next")}</span>&nbsp;<i class="ty-pagination__text-arrow"></i></a>

        {if $news_pagination.next_range}
            <a data-ca-scroll=".cm-pagination-container_news" href="{"`$c_url`&news_page=`$news_pagination.next_range``$extra_url`"|fn_url}" data-ca-page="{$news_pagination.next_range}" class="cm-history ty-pagination__item hidden-phone ty-pagination__range {$ajax_class}" data-ca-target-id="{$id}">{$news_pagination.next_range_from} - {$news_pagination.next_range_to}</a>
        {/if}
    </div>
    {if $smarty.capture.pagination_open == "Y"}
        </div>
    {/if}
    {else}
        <div><a data-ca-scroll=".cm-pagination-container_news" href="" data-ca-page="{$pg}" data-ca-target-id="{$id}" class="hidden"></a></div>
    {/if}
{/if}

{if $smarty.capture.pagination_open == "Y"}
    <!--{$id}--></div>
    {capture name="pagination_open"}N{/capture}
{elseif $smarty.capture.pagination_open != "Y"}
    {capture name="pagination_open"}Y{/capture}
{/if}