{scripts}
{/scripts}
{if $ga_tracking_id}
    <amp-analytics type='googleanalytics'>
        <script type='application/json'>
            {
                "vars": {
                    "account": "{$ga_tracking_id}"
                },
                "triggers": {
                    "trackPageview": {
                        "on": "visible",
                        "request": "pageview"
                    },
                    "trackEvent": {
                        "selector": ".ty-product-block__button .ty-btn",
                        "on": "click",
                        "request": "event",
                        "vars": {
                            "eventCategory": "buy",
                            "eventAction": "buy-click"
                        }
                    }
                }
            }
        </script>
    </amp-analytics>
{/if}