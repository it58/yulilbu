<!DOCTYPE html>
<html amp lang="{$smarty.const.CART_LANGUAGE}" dir="{$language_direction}">
<head>
    <meta charset="utf-8">
    <title>{$product.page_title|default:$product.product}</title>
    <link rel="canonical" href="{"index.php?dispatch=products.view&product_id=`$product.product_id`"|fn_url}">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width, minimum-scale=1" />
    <meta name="description" content="{$meta_description|html_entity_decode:$smarty.const.ENT_COMPAT:"UTF-8"|default:$location_data.meta_description}" />
    <meta name="keywords" content="{$meta_keywords|default:$location_data.meta_keywords}" />
    {if $ga_tracking_id}
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    {/if}
    <style amp-boilerplate>body{$ldelim}-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both{$rdelim}@-webkit-keyframes -amp-start{$ldelim}from{$ldelim}visibility:hidden{$rdelim}to{$ldelim}visibility:visible{$rdelim}{$rdelim}@-moz-keyframes -amp-start{$ldelim}from{$ldelim}visibility:hidden{$rdelim}to{$ldelim}visibility:visible{$rdelim}{$rdelim}@-ms-keyframes -amp-start{$ldelim}from{$ldelim}visibility:hidden{$rdelim}to{$ldelim}visibility:visible{$rdelim}{$rdelim}@-o-keyframes -amp-start{$ldelim}from{$ldelim}visibility:hidden{$rdelim}to{$ldelim}visibility:visible{$rdelim}{$rdelim}@keyframes -amp-start{$ldelim}from{$ldelim}visibility:hidden{$rdelim}to{$ldelim}visibility:visible{$rdelim}{$rdelim}</style><noscript><style amp-boilerplate>body{$ldelim}-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none{$rdelim}</style></noscript>
    {include file="addons/sd_accelerated_pages/common/styles.tpl"}
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

    {if $amp_iframe}
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    {/if}

    {if $amp_youtube}
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
    {/if}
</head>
<body>
    <div class="content">
        <div class="ty-header">
            <div class="ty-logo-container">
                {assign var="amp_logo" value=$logos.amp|default:$logos.theme}
                <a href="{""|fn_url}" title="{$amp_logo.image.alt}">
                    <amp-img src="{$amp_logo.image.image_path}"
                        width="{$amp_logo.image.image_x}"
                        height="{$amp_logo.image.image_y}"
                        alt="{$amp_logo.image.alt}"
                        class="ty-logo-container__image">
                    </amp-img>
                </a>
            </div>
            <div class="ty-search-block">
                <form action="{""|fn_url}" name="search_form" method="GET" target="_top">
                    <input type="hidden" name="subcats" value="Y" />
                    <input type="hidden" name="pcode_from_q" value="Y" />
                    <input type="hidden" name="pshort" value="Y" />
                    <input type="hidden" name="pfull" value="Y" />
                    <input type="hidden" name="pname" value="Y" />
                    <input type="hidden" name="pkeywords" value="Y" />
                    <input type="hidden" name="search_performed" value="Y" />

                    {if $settings.General.search_objects}
                        {assign var="search_title" value=__("search")}
                    {else}
                        {assign var="search_title" value=__("search_products")}
                    {/if}
                    <input type="text" name="q" value="{$search.q}" placeholder="{$search_title}" id="search_input{$smarty.capture.search_input_id}" title="{$search_title}" class="ty-search-block__input cm-hint" />
                    {if $settings.General.search_objects}
                        <button title="{__("search")}" class="ty-search-magnifier" type="submit"><amp-img layout="responsive" width="24" height="24" src="{$images_dir}/addons/sd_accelerated_pages/search.png"></amp-img></button>
                        <input type="hidden" name="dispatch" value="search.results" />
                    {else}
                        <button title="{__("search")}" class="ty-search-magnifier" type="submit"><amp-img layout="responsive" width="24" height="24" src="{$images_dir}/addons/sd_accelerated_pages/search.png"></amp-img></button>
                        <input type="hidden" name="dispatch" value="products.search" />
                    {/if}
                </form>
            </div>
        </div>
        <div class="ty-main-image">
            <amp-carousel type="slides" layout="responsive" width="360" height="240">
                <amp-img layout="responsive" height="600" width="1200" src="{$product.main_pair.detailed.image_path}"></amp-img>
                {foreach from=$product.image_pairs item="image" key="pair_id"}
                    <amp-img layout="responsive" height="600" width="1200" src="{$image.detailed.image_path}"></amp-img>
                {/foreach}
            </amp-carousel>
        </div>
        {assign var="obj_id" value=$product.product_id}

        <div class="ty-product-block">
            <div class="ty-product-main-info">
                {assign var="form_open" value="form_open_`$obj_id`"}

                <h1 class="ty-product-block-title"><bdi>{$product.product nofilter}</bdi></h1>

                {if $show_discount_label && ($product.discount_prc || $product.list_discount_prc) && $show_price_values}
                <span class="ty-discount-label cm-reload-{$obj_id}" id="discount_label_update_{$obj_id}">
                    <span class="ty-discount-label__item" id="line_prc_discount_value_{$obj_id}"><span class="ty-discount-label__value" id="prc_discount_value_label_{$obj_id}">{__("save_discount")} {if $product.discount}{$product.discount_prc}{else}{$product.list_discount_prc}{/if}%</span></span>
                </span>
                {/if}

                <div class="ty-product-brand">
                    {include file="views/products/components/product_features_short_list.tpl" features=$product.header_features}
                </div>

                <span class="ty-old-price" id="old_price_update_{$obj_id}">
                {if $product.discount}
                    <span class="ty-list-price ty-nowrap" id="line_old_price_{$obj_id}">{__("old_price")}: <span class="ty-strike">{include file="common/price.tpl" value=$product.original_price|default:$product.base_price span_id="old_price_`$obj_prefix``$obj_id`" class="ty-list-price ty-nowrap"}</span></span>
                {elseif $product.list_discount}
                    <span class="ty-list-price ty-nowrap" id="line_list_price_{$obj_id}"><span class="list-price-label">{__("list_price")}:</span> <span class="ty-strike">{include file="common/price.tpl" value=$product.list_price span_id="list_price_`$obj_prefix``$obj_id`" class="ty-list-price ty-nowrap"}</span></span>
                {/if}
                </span>

                <div class="prices-container price-wrap">
                    {if $product.price|floatval || $product.zero_price_action == 'P'}
                    <span class="ty-price" id="line_discounted_price_{$obj_id}">
                        {include file="common/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="ty-price-num" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}
                    </span>
                    {/if}
                </div>

                <span class="ty-product-discount" id="line_discount_update_{$obj_id}">
                    <input type="hidden" name="appearance[show_price_values]" value="{$show_price_values}" />
                    <input type="hidden" name="appearance[show_list_discount]" value="{$show_list_discount}" />
                    {if $product.discount}
                        <span class="ty-list-price ty-save-price ty-nowrap" id="line_discount_value_{$obj_id}">{__("you_save")}: {include file="common/price.tpl" value=$product.discount span_id="discount_value_`$obj_prefix``$obj_id`" class="ty-list-price ty-nowrap"}&nbsp;(<span id="prc_discount_value_{$obj_id}" class="ty-list-price ty-nowrap">{$product.discount_prc}</span>%)</span>
                    {elseif $product.list_discount}
                        <span class="ty-list-price ty-save-price ty-nowrap" id="line_discount_value_{$obj_id}"> {__("you_save")}: {include file="common/price.tpl" value=$product.list_discount span_id="discount_value_`$obj_prefix``$obj_id`"}&nbsp;(<span id="prc_discount_value_{$obj_id}">{$product.list_discount_prc}</span>%)</span>
                    {/if}
                </span>

                <div class="ty-product-block__sku">
                    <div class="ty-control-group ty-sku-item cm-hidden-wrapper{if !$product.product_code} hidden{/if} cm-reload-{$obj_id}" id="sku_update_{$obj_id}">
                        <input type="hidden" name="appearance[show_sku]" value="{$show_sku}" />
                        {if $show_sku_label}
                            <label class="ty-control-group__label" id="sku_{$obj_id}">{__("sku")}:</label>
                        {/if}
                        <span class="ty-control-group__item">{$product.product_code}</span>
                    </div>
                </div>

                {if $product.is_edp != "Y" && $settings.General.inventory_tracking == "Y"}
                    {assign var="product_amount" value=$product.inventory_amount|default:$product.amount}
                    <div class="ty-product-availability stock-wrap" id="product_amount_update_{$obj_id}">
                        <input type="hidden" name="appearance[show_product_amount]" value="{$show_product_amount}" />
                        {if !$product.hide_stock_info}
                            {if $settings.Appearance.in_stock_field == "Y"}
                                {if ($product_amount > 0 && $product_amount >= $product.min_qty) && $settings.General.inventory_tracking == "Y"}
                                    {if (
                                            $product_amount > 0
                                            && $product_amount >= $product.min_qty
                                            || $product.out_of_stock_actions == "OutOfStockActions::BUY_IN_ADVANCE"|enum
                                        )
                                        && $settings.General.inventory_tracking == "Y"
                                    }
                                        <div class="ty-control-group product-list-field">
                                            <span id="qty_in_stock_{$obj_id}" class="ty-qty-in-stock ty-control-group__item">
                                                {if $product_amount > 0}
                                                    {$product_amount}&nbsp;{__("items")}
                                                {else}
                                                    {__("on_backorder")}
                                                {/if}
                                            </span>
                                        </div>
                                    {elseif $settings.General.inventory_tracking == "Y" && $settings.General.allow_negative_amount != "Y"}
                                        <div class="ty-control-group product-list-field">
                                                <label class="ty-control-group__label">{__("in_stock")}:</label>
                                            <span class="ty-qty-out-of-stock ty-control-group__item">{__("out_of_stock_products")}</span>
                                        </div>
                                    {/if}
                                {/if}
                            {else}
                                {if (
                                        $product_amount > 0
                                        && $product_amount >= $product.min_qty
                                        || $product.tracking == "ProductTracking::DO_NOT_TRACK"|enum
                                    )
                                    && $settings.General.inventory_tracking == "Y"
                                    && $settings.General.allow_negative_amount != "Y"
                                    || $settings.General.inventory_tracking == "Y"
                                    && (
                                        $settings.General.allow_negative_amount == "Y"
                                        || $product.out_of_stock_actions == "OutOfStockActions::BUY_IN_ADVANCE"|enum
                                    )
                                }
                                    <div class="ty-control-group product-list-field">
                                        <span class="ty-qty-in-stock ty-control-group__item" id="in_stock_info_{$obj_id}">
                                            {if $product_amount > 0}
                                                {__("in_stock")}
                                            {else}
                                                {__("on_backorder")}
                                            {/if}
                                        </span>
                                    </div>
                                {elseif $product_amount <= 0 || $product_amount < $product.min_qty
                                    && $settings.General.inventory_tracking == "Y"
                                    && $settings.General.allow_negative_amount != "Y"
                                }
                                    <div class="ty-control-group product-list-field">
                                        <span class="ty-qty-out-of-stock ty-control-group__item" id="out_of_stock_info_{$obj_id}">{__("out_of_stock_products")}</span>
                                    </div>
                                {/if}
                            {/if}
                        {/if}
                    </div>
                {/if}
                {if $settings.General.allow_anonymous_shopping == "allow_shopping" || $auth.user_id}
                    {if ($product_amount > 0 && $product_amount >= $product.min_qty && $product.price > 0) && $settings.General.inventory_tracking == "Y" || $details_page}
                        <div class="ty-product-block__button">
                            <a class="ty-btn" href="{"products.view?product_id=`$obj_id`"|fn_url}">{__("buy_now")}</a>
                        </div>
                    {else}
                        <div class="ty-product-block__button">
                            <a class="ty-btn" href="{"products.view?product_id=`$obj_id`"|fn_url}">{__("sd_accelerated_pages.view_detailes")}</a>
                        </div>
                    {/if}
                {else}
                    {include file="buttons/button.tpl" but_id=$but_id but_text=__("sign_in_to_buy") but_href="{"auth.login_form"|fn_url}" but_role=$but_role|default:"text" but_name=""}
                    <p>{__("text_login_to_add_to_cart")}</p>
                {/if}
            </div>

            <div class="ty-detailed-info">
                <amp-accordion disable-session-states>
                    {foreach from=$tabs item="tab"}
                        {if $tab.html_id == "description" && $product.amp_description}
                            <section expanded class="ty-section-description">
                                <h4 class="ty-product-block__description-title">{$tab.name}</h4>
                                <div class="accordion-description">
                                    {$product.amp_description nofilter}
                                </div>
                            </section>
                        {/if}
                        {if $tab.html_id == "features" && $product.product_features}
                            <section expanded class="ty-section-features">
                                <h4>{$tab.name}</h4>
                                <div class="accordion-description cm-reload-{$obj_id}" id="product_features_update_{$obj_id}">
                                    {include file="views/products/components/product_features.tpl" product_features=$product.product_features}
                                </div>
                            </section>
                        {/if}
                    {/foreach}
                </amp-accordion>
            </div>
        </div>
    </div>
    {include file="addons/sd_accelerated_pages/common/scripts.tpl"}
</body>
</html>
