<script type="text/javascript">
(function(_, $) {
    var controller = '{$runtime.controller}';
    var mode = '{$runtime.mode}';
    var theme = '{$settings.theme_name}';

    if (controller == 'messenger' && mode == 'view') {
        $(window).resize(function() {
            if ($(window).height() > 800) {
                $('.tygh-header .row-fluid').first().show();
            } else {
                $('.tygh-header .row-fluid').first().hide();
            }
        });
        $('.sd-lists-popup').css('display', 'none');
    }

    if (theme == 'passion') {
        var $style = "<style>.ty-sd_messaging .ty-sd_messaging__ticket-new { background: #dd2884; } .recipient-message .ty-sd_messaging_system-all { background-color: #fff; } .recipient-message .ty-sd_messaging_system-all:before { background-color: #fff; } .ty-sd_messaging_system-name, .ty-sd_messaging_system-message__message { color:inherit !important; } .ty-sd_messaging__list-bordered > li { border-color:#dadada !important; }</style>";

        $($style).appendTo("head");
    }

    if (theme == 'kids_theme') {
        var $style = "<style>.ty-sd_messaging .ty-sd_messaging__ticket-new { background: #00cad3; } .author-message .ty-sd_messaging_system-all:before { background-color:#00cad3 !important; } .ty-sd_messaging_system-all { background-color: #00cad3 !important; } .recipient-message .ty-sd_messaging_system-message__message { color: white; } .recipient-message .ty-sd_messaging_system-name { color: white; } .recipient-message .ty-sd_messaging_system-all:before { background-color:#00cad3; }</style>";

        $($style).appendTo("head");
    }

    if (theme == 'american') {
        var $style = "<style>.ty-discussion-post__buttons.buttons-container { background: #f4f4f4; }</style>";

        $($style).appendTo("head");
    }

    _.tr({
        'addons.sd_messaging_system.reload_page': '{__("addons.sd_messaging_system.reload_page")|escape:"javascript"}'
    });

}(Tygh, Tygh.$));
</script>