{assign var=last_message_id value=0}

<div class="ticket-messages-block" id="ticket_messages_block">
    <div class="ticket-messages-list" data-ticket-id="{$ticket.ticket_id}" id="ticket_messages_list">
        {if $messages}
            {foreach from=$messages item=message}
                {if $message.message_id > $last_message_id}
                    {assign var=last_message_id value=$message.message_id}
                {/if}

                {if $message.author_type != "V"}
                    {assign var=message_class value="author-message has-avatar"}
                {else}
                    {assign var=message_class value="recipient-message"}
                {/if}

                <div class="ty-sd_messaging_system-message__content {$message_class} ty-mb-l">
                    <div class="ty-sd_messaging_system-message__author">
                        {include file="addons/sd_messaging_system/views/messenger/components/user_image.tpl" message=$message ticket=$ticket}
                    </div>

                    <div class="ty-sd_messaging_system-all">
                        <div class="ty-sd_messaging_system-name">
                            {if $message.author_type == "V" && $ticket.vendor_name}
                                {$ticket.vendor_name}
                            {elseif $message.author_type != "V" && $ticket.customer_name}
                                {$ticket.customer_name}
                            {/if}
                            <span class="ty-sd_messaging_system-message__date" data-message-timestamp="{$message.timestamp}">{$message.date}</span>
                        </div>

                        <div class="ty-sd_messaging_system-message" id="message_text">
                            <div class="ty-sd_messaging_system-message__message">
                                {$message.message|nl2br|strip nofilter}
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}
        {else}
            <p class="ty-no-items">{__("no_messages_found_with_recipient", ["[recipient_name]" => $ticket.vendor_name])}</p>
        {/if}
    </div>

</div>

<div class="ty-center hidden" id="message_spinner">{__('addons.sd_messaging_system.waiting_for_connect')} <img src="{$self_images_dir}/images/spinner.gif" width="20" height="20"/></div>
<div class="ty-discussion-post__buttons buttons-container">
    <form action="{""|fn_url}" method="post" id="new_message_form" class="ty-discussion-post__form">
        <input type="hidden" name="ticket_id" value="{$ticket.ticket_id}"/>
        <input type="hidden" name="author_id" value="{$auth.user_id}"/>
        <input type="hidden" name="last_message_id" value="{$last_message_id}"/>
        {if !$hide_send_message}
            <textarea id="new_message_field" name="message_body" rows="4" cols="50" placeholder="{__('write_your_message_here')}" class="ty-sd_messaging_system-textarea" disabled="disabled">{$attachment_message nofilter}</textarea>
            <a href="{"messenger.tickets_list"|fn_url}" class="ty-btn__send-left ty-btn ty-btn__secondary">{__('go_back')}</a>
            <button id="send_message_but" class="ty-btn__send-right ty-btn__primary ty-btn ty-float-right ty-btn" type="submit" name="dispatch[messenger.send_message]" onclick="return false;" disabled="disabled">{__('send')}</button>
        {else}
            <div class="ty-center">{__('vendor_cannot_receive_messages')}</div>
        {/if}
    </form>
</div>

{assign var=vendor_image_data value=$ticket.vendor_image|fn_image_to_display:$smarty.const.USER_IMAGE_WIDTH:$smarty.const.USER_IMAGE_HEIGHT}
{assign var=customer_image_data value=$ticket.customer_image|fn_image_to_display:$smarty.const.USER_IMAGE_WIDTH:$smarty.const.USER_IMAGE_HEIGHT}

<input type="hidden" name="websocket_url" value="{$addons.sd_messaging_system.websocket_url}">

<div id="sd-messenger-recipient-data" class="hidden"
    data-name="{$ticket.vendor_name|default:""}"
    data-id={$ticket.company_id}
    data-type="V"
    data-has-image="{if $vendor_image_data}Y{else}N{/if}"
    data-image-path="{$vendor_image_data.image_path|default:""}"
    data-image-alt="{$vendor_image_data.alt|default:""}">
</div>

<div id="sd-messenger-sender-data" class="hidden"
    data-name="{$ticket.customer_name|default:""}"
    data-id={$ticket.customer_id}
    data-type="C"
    data-has-image="{if $customer_image_data}Y{else}N{/if}"
    data-image-path="{$customer_image_data.image_path|default:""}"
    data-image-alt="{$customer_image_data.alt|default:""}">
</div>

{script src="js/addons/sd_messaging_system/web_socket.js"}
{script src="js/addons/sd_messaging_system/messenger.js"}
