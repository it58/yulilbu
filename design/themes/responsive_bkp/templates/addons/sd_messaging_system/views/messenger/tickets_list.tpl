<ul class="ty-sd_messaging__list ty-sd_messaging__list-linked ty-sd_messaging__list-bordered ty-sd_messaging__message">
{foreach from=$tickets item=ticket}
    <li class="ty-sd_messaging">
        <a href="{fn_url('messenger.view&ticket_id='|cat:$ticket.ticket_id)}" id="ticket_id_{$ticket.ticket_id}" class="ty-sd_messaging__wrapper ty-sd_messaging__ticket {if $ticket.last_message && $ticket.status == $smarty.const.TICKET_STATUS_NEW}ty-sd_messaging__ticket-new{/if}">
            <div class="ty-sd_messaging_system-recipient-user__image">
                {if $ticket.vendor_image}
                    {include file="common/image.tpl" images=$ticket.vendor_image image_height=$smarty.const.USER_IMAGE_HEIGHT image_width=$smarty.const.USER_IMAGE_HEIGHT image_type="user_img"}
                {/if}
            </div>

            <div class="ty-sd_messaging__body">
                <h3 class="ty-sd_messaging_system-recipient-user__name">{$ticket.last_message_name}{if $ticket.vendor_name}{$ticket.vendor_name}{/if}
                    <span class="ty-sd_messaging_system-last-message__date dotted">{$ticket.last_message_date}</span>
                    {if $ticket.last_message && $ticket.status == $smarty.const.TICKET_STATUS_NEW}
                        <span class="ty-sd_messaging_system-last-message__date new-message dotted">{__("new")}{if $ticket.new_messages_amount}&nbsp;({$ticket.new_messages_amount}){/if}</span>
                    {/if}
                </h3>
                <p class="ty-sd_messaging_system-last-message__message">
                    {math equation="x+y" x=$addons.sd_messaging_system.trimmed_messages_length y=$smarty.const.TRIPLE_DOT_LENGHT assign="message_length"}
                    {$ticket.last_message|strip_tags|truncate:$message_length nofilter}
                </p>
            </div>
        </a>
    </li>
{foreachelse}
    <p class="ty-no-items">{__("text_no_tickets")}</p>
{/foreach}
</ul>

{capture name="mainbox_title"}{__("messages")}{/capture}
