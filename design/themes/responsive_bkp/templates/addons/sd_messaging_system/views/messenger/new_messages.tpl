{if $messages}

    {assign var=original_last_message_id value=$last_message_id}

    <div id="new_messages_block_{$last_message_id}">
    {foreach from=$messages item=message}
        {if $message.message_id > $last_message_id}
            {assign var=last_message_id value=$message.message_id}
        {/if}

        {if $message.author_type != "V"} 
            {if $ticket.customer_image}
                {assign var=message_class value="author-message has-avatar"}
            {/if}
            {assign var=message_class value="author-message"}
        {else}
            {assign var=message_class value="recipient-message"}
        {/if}

        <div class="ty-sd_messaging_system-message__content {$message_class} ty-mb-l">
            <div class="ty-sd_messaging_system-message__author">
                {include file="addons/sd_messaging_system/views/messenger/components/user_image.tpl" message=$message ticket=$ticket}
            </div>

            <div class="ty-sd_messaging_system-all">
                <div class="ty-sd_messaging_system-name">
                    {if $message.author_type == "V" && $ticket.vendor_name}
                        {$ticket.vendor_name}
                    {elseif $message.author_type != "V" && $ticket.customer_name}
                        {$ticket.customer_name}
                    {/if}
                </div>

                <div class="ty-sd_messaging_system-message" data-message-id="{$message.message_id}" id="message_{$message.message_id}">
                    <div class="ty-sd_messaging_system-message__message">
                        {$message.message|escape|nl2br nofilter}
                    </div>
                </div>
            </div>
        </div>
    {/foreach}

    <div id="new_messages_block_{$last_message_id}">
        <div id="last_message_id" data-last-message-id="{$last_message_id}"></div>
    <!--new_messages_block_{$last_message_id}--></div>
    <!--new_messages_block_{$original_last_message_id}--></div>
{/if}
